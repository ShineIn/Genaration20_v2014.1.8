﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Inspection.Views
{
    /// <summary>
    /// UCSql.xaml 的交互逻辑
    /// </summary>
    public partial class UCSql : UserControl
    {
        SqlFrm sqlFrm;
        public UCSql()
        {
            InitializeComponent();
            //if (sqlFrm == null) sqlFrm = new SqlFrm();       
            Loaded += UCSql_Loaded;
           
        }

        private void UCSql_Loaded(object sender, RoutedEventArgs e)
        {
            if (sqlFrm != null) 
                sqlFrm.Close();
            sqlFrm = new SqlFrm();
            mainPanel.Controls.Add(sqlFrm);
            sqlFrm.Dock = System.Windows.Forms.DockStyle.Fill;
            sqlFrm.AllowShowTitle = false;
            sqlFrm.Show();
        }
    }
}
