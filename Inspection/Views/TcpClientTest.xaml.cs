﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Threading;
using LogSv;
using SuperSimpleTcp;

namespace Inspection.Views
{
    /// <summary>
    /// TcpClientTest.xaml 的交互逻辑
    /// </summary>
    public partial class TcpClientTest : UserControl
    {
        public TcpClientTest()
        {
            InitializeComponent();
        }

        public static DependencyProperty ServerIPProperty = DependencyProperty.Register("ServerIP", typeof(string), typeof(TcpClientTest), new FrameworkPropertyMetadata("127.0.0.1", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, ProChanged));
        public static DependencyProperty PortProperty = DependencyProperty.Register("Port", typeof(int), typeof(TcpClientTest), new FrameworkPropertyMetadata(5000, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, ProChanged));
        public static DependencyProperty ClientProperty = DependencyProperty.Register("Client", typeof(SimpleTcpClient), typeof(TcpClientTest), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, ProChanged));

        /// <summary>
        /// IP地址
        /// </summary>
        public string ServerIP
        {
            get { return (string)GetValue(ServerIPProperty); }
            set { SetValue(ServerIPProperty, value); }
        }

        /// <summary>
        /// 端口号
        /// </summary>
        public int Port
        {
            get { return (int)GetValue(PortProperty); }
            set { SetValue(PortProperty, value); }
        }

        /// <summary>
        /// 发送的内容
        /// </summary>
        public string SendContent
        {
            get { return _sendContent; }
            set { _sendContent = value; }
        }
        private string _sendContent = string.Empty;

        /// <summary>
        /// TCP客户端
        /// </summary>
        public SimpleTcpClient Client
        {
            get { return (SimpleTcpClient)GetValue(ClientProperty); }
            set { SetValue(ClientProperty, value); }
        }

        private static void ProChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            TcpClientTest sendor = (TcpClientTest)d;
            if (null == sendor) return;
            bool isConnected = sendor.Client != null && sendor.Client.IsConnected;
            string nIp = string.Empty;
            int port = -1;
            if (e.Property == ServerIPProperty)
            {
                nIp = (string)e.NewValue;
                port = sendor.Port;
                //sendor.ConnectClient(nIp, port);
            }
            else if (e.Property == PortProperty)
            {
                nIp = sendor.ServerIP;
                port = (int)e.NewValue;

                //sendor.ConnectClient(nIp, port);
            }
            else if (e.Property == ClientProperty)
            {
                SimpleTcpClient oldclient = e.OldValue as SimpleTcpClient;
                SimpleTcpClient nclient = e.NewValue as SimpleTcpClient;
                if (null != oldclient && null != oldclient.Events)
                {
                    oldclient.Events.DataReceived -= sendor.TcpClient_DataReceived;
                    oldclient.Events.Connected -= sendor.Events_Connected;
                    oldclient.Events.Disconnected -= sendor.Events_Disconnected;
                }
                if (null != nclient && null != nclient.Events)
                {
                    nclient.Events.DataReceived += sendor.TcpClient_DataReceived;
                    nclient.Events.Connected += sendor.Events_Connected;
                    nclient.Events.Disconnected += sendor.Events_Disconnected;
                    string strPort = nclient.ServerIpPort;
                    string[] arry = strPort.Split(':');
                    if (null == arry || arry.Length != 2) return;
                    sendor.ServerIP = arry[0];
                    sendor.Port = int.Parse(arry[1]);
                }
            }
        }

        private bool ConnectClient(string strIP, int port)
        {
            if (!ClsFunction.IsIpAdress(strIP))
            {
                recieveContent.Text += "IP格式设置错误,请设置正确的IP地址.....";
                return false;
            }
            if (null != Client && Client.IsConnected)
            {
                if (Client.ServerIpPort == $"{strIP}:{port}") return true;
                Client.Disconnect();
            }
            Client = new SimpleTcpClient(ServerIP, Port);
            Client.Events.DataReceived += TcpClient_DataReceived;
            Client.Events.Connected += Events_Connected;
            Client.Events.Disconnected += Events_Disconnected;
            Client.Connect();
            if (!Client.IsConnected)
            {
                recieveContent.Text += $"与服务器{ServerIP}端口={Port}通讯连接失败!\r\n";
                return false;
            }
            return true;
        }

        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {
            ConnectClient(ServerIP, Port);
        }

        private void Events_Disconnected(object sender, ConnectionEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                recieveContent.Text += $"与服务器{ServerIP}端口={Port}断开连接\r\n";
            });
        }

        private void Events_Connected(object sender, ConnectionEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                recieveContent.Text += $"与服务器{ServerIP}端口={Port}连接成功\r\n";
            });
        }

        private void btnDisConnect_Click(object sender, RoutedEventArgs e)
        {
            if (null != Client && Client.IsConnected)
                Client.Disconnect();
            Client = null;
        }

        private void btnSend_Click(object sender, RoutedEventArgs e)
        {
            if (null != Client && Client.IsConnected && !string.IsNullOrEmpty(_sendContent))
                Client.Send(_sendContent);
        }

        private void TcpClient_DataReceived(object sender, DataReceivedEventArgs e)
        {
            Dispatcher.Invoke(() => { });
            Dispatcher.Invoke(() =>
            {
                if (IsLoaded)
                {
                    string data = Encoding.UTF8.GetString(e.Data.Array);
                    recieveContent.Text += $"{data}\r\n";
                }
            });
        }

        private void clar_Click(object sender, RoutedEventArgs e)
        {
            recieveContent.Clear();
        }
    }
}
