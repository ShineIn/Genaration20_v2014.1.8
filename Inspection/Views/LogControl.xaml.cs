﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Inspection.Views
{
    /// <summary>
    /// LogControl.xaml 的交互逻辑
    /// </summary>
    public partial class LogControl : UserControl
    {
        LogMessage logWindow;
        public LogControl()
        {
            InitializeComponent();
            logWindow = new LogMessage();
        }

        private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (logWindow != null) 
            {
                logWindow.Close();
            }
            logWindow = new LogMessage();
            logWindow.WindowStartupLocation = WindowStartupLocation.Manual;
            logWindow.Width = 500;
            logWindow.Height = 700;
            logWindow.Left = 1400;
            logWindow.Top = 100;
            logWindow.Show();
            logWindow.Topmost = true;
            logWindow.Activate();
            logWindow.WindowState = WindowState.Normal;
        }
    }
}
