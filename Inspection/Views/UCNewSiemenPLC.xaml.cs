﻿using S7.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Inspection.Views
{
    /// <summary>
    /// Interaction logic for UCNewSiemenPLC.xaml
    /// </summary>
    public partial class UCNewSiemenPLC : UserControl
    {
        public UCNewSiemenPLC()
        {
            InitializeComponent();
        }

        public static DependencyProperty PLCModuleProperty = DependencyProperty.Register("PLCModule", typeof(Plc), typeof(UCNewSiemenPLC), new PropertyMetadata(null));

        public Plc PLCModule
        {
            get { return (Plc)GetValue(PLCModuleProperty); }
            set { SetValue(PLCModuleProperty, value); }
        }
    }
}
