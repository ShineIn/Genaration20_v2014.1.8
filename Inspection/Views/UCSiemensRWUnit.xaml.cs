﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using S7.Net;
using Inspection.Class;

namespace Inspection.Views
{
    /// <summary>
    /// Interaction logic for UCSicmensRWUnit.xaml
    /// </summary>
    public partial class UCSiemensRWUnit : UserControl, INotifyPropertyChanged
    {
        public UCSiemensRWUnit()
        {
            InitializeComponent();
        }

        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        public static DependencyProperty PLCModuleProperty = DependencyProperty.Register("PLCModule", typeof(Plc), typeof(UCSiemensRWUnit), new PropertyMetadata(null));

        public Plc PLCModule
        {
            get { return (Plc)GetValue(PLCModuleProperty); }
            set { SetValue(PLCModuleProperty, value); }
        }

        /// <summary>
        /// DB块地址
        /// </summary>
        public string DBAddress
        {
            get { return _dbAddress; }
            set { _dbAddress = value; OnPropertyChanged(); }
        }
        private string _dbAddress = string.Empty;


        /// <summary>
        /// 读写的值
        /// </summary>
        public object RWValue
        {
            get { return _rWValue; }
            set { _rWValue = value; OnPropertyChanged(); }
        }
        private object _rWValue = null;

        /// <summary>
        /// 读写的值
        /// </summary>
        public Type OperateType
        {
            get { return _operateType; }
            set { _operateType = value; OnPropertyChanged(); }
        }
        private Type _operateType = typeof(int);

        private void btnReadInt_Click(object sender, RoutedEventArgs e)
        {
            if (null == PLCModule || !PLCModule.IsConnected) return;
            if (!_dbAddress.Contains("DB"))
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"设定的读取地址错误，地址类型只能为DB类型，请设置正确的地址", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                return;
            }
            if (PLCModule.ReadValue(_dbAddress, _operateType, out dynamic reValue))
            {
                RWValue = reValue;
                Infrastructure.LogServiceHelper.Info($"读取PLC地址{reValue}中的值为");
            }
        }

        private void btnWriteInt_Click(object sender, RoutedEventArgs e)
        {
            if (null == PLCModule || !PLCModule.IsConnected) return;
            if (!_dbAddress.Contains("DB"))
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"设定的写入地址错误，地址类型只能为DB类型，请设置正确的地址", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                return;
            }
            if (PLCModule.WriteValue(_dbAddress, _rWValue))
            {
                Infrastructure.LogServiceHelper.Info($"写入地址{_dbAddress}中的{_rWValue}成功");
            }
            else
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"写入失败........", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                Infrastructure.LogServiceHelper.Info($"向PLC地址{_dbAddress}中写入值失败...");
            }
        }
    }

    public class ValueConvert : MarkupExtension, IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (null == values || values.Length != 2 || values[0] == DependencyProperty.UnsetValue || values[0] == null) return string.Empty;
            if (values[1] != DependencyProperty.UnsetValue && values[1] != null)
            {
                type = (Type)values[1];
            }
            return values[0].ToString();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            object tempValue = null;
            if (null != type)
            {
                TypeConverter converter = TypeDescriptor.GetConverter(type);
                tempValue = converter.ConvertFromString(value.ToString());
            }
            return new object[] { tempValue, null };
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        private Type type;
    }
}
