﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using HslCommunication;
using HslCommunication.Profinet.Melsec;
using HslCommunication.Profinet.Siemens;
using Inspection.Class;
using Inspection.Helper;
namespace Inspection.Views
{
    /// <summary>
    /// PlcUI.xaml 的交互逻辑
    /// </summary>
    public partial class ucSimensPLC : UserControl
    {
        public ucSimensPLC()
        {
            InitializeComponent();         
            
        }
        public static S71500 siemensS7Net ;                                                         //PLC连接对象

        private string plcIP = "10.66.81.24";
        public string PlcIP
        {
            get => plcIP;
            set => plcIP = value;
        }   

        #region【初始化PLC】
        public void InitPlc()
        {
            if (GlobalParame.PLC != null) { siemensS7Net =new S71500( GlobalParame.PLC); }
            else
            {
                siemensS7Net = new S71500(S7.Net.CpuType.S71500, plcIP, 0, 0);          //PLC连接对象
                siemensS7Net.ConnectPLC();
                GlobalParame.PLC=siemensS7Net.Plc;
            }
        }
        #endregion

        #region【与PLC断开连接】
        public static void DisconnectPLC()
        {
            siemensS7Net.DisConnect();
        }
        #endregion

        #region【与PLC建立连接】
        public static bool ConnectPLC()
        {
            bool connect = siemensS7Net.ConnectPLC();                                            //连接PLC
            if (connect)
            {
                Global.ModulePara.PlcState = true;                                                           //PLC连接状态 true
                return true;
            }
            else
            {
                Global.ModulePara.PlcState = false;

               // Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"PLC连接失败.....", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                return false;
            }
        }
        #endregion

        private void btnReadBit_Click(object sender, RoutedEventArgs e)
        {
            if (!(tbxBitAdd.Text.Contains("DB") && tbxBitAdd.Text.Contains(".")))
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"设定的读取地址错误，地址类型只能为DB类型，请设置正确的读取地址", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                return;
            }
            bool readVal = (bool)siemensS7Net.Read(tbxBitAdd.Text, DataType.BOOL);
            tbxBitVal.Text = readVal.ToString();
            Infrastructure.LogServiceHelper.Info($"读取地址{tbxBitAdd.Text}中的值位为：{tbxBitVal}");

        }

        private void btnWriteBit_Click(object sender, RoutedEventArgs e)
        {
            if (!(tbxBitAdd.Text.Contains("DB") && tbxBitAdd.Text.Contains(".")))
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"设定的写入地址错误，地址类型只能为DB类型，请设置正确的地址", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                return;
            }
            bool bWrite = false;
            if (tbxBitVal.Text != "0")
                bWrite = true;
            else
                bWrite = false;
            bool retVal = siemensS7Net.Write(tbxBitAdd.Text, bWrite);
            if (retVal)           
                Infrastructure.LogServiceHelper.Info($"写入地址{tbxBitAdd}中的值为{tbxBitVal.Text}");
            else
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"写入失败........", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                Infrastructure.LogServiceHelper.Info($"读取地址{tbxBitAdd.Text}中的值失败.....");
            }
        }

        private void btnReadWord_Click(object sender, RoutedEventArgs e)
        {
            if (!tbxWordAdd.Text.Contains("DB"))
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"设定的读取地址错误，地址类型只能为DB类型，请设置正确的地址", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                return;
            }
            ushort retVal = (ushort)siemensS7Net.Read(tbxWordAdd.Text, DataType.INT);
            tbxWordVal.Text = retVal.ToString();
            Infrastructure.LogServiceHelper.Info($"读取地址{tbxWordAdd.Text}中的值为{tbxWordVal.Text}");
        }

        private void btnWriteWord_Click(object sender, RoutedEventArgs e)
        {
            if (!tbxWordAdd.Text.Contains("DB"))
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"设定的写入地址错误，地址类型只能为DB类型，请设置正确的地址", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                return;
            }
            short writeVal = short.Parse(tbxWordVal.Text);
            bool retVal = siemensS7Net.Write(tbxWordAdd.Text, writeVal);
            if (retVal)
            {
                Infrastructure.LogServiceHelper.Info($"向PLC地址{tbxWordAdd.Text}中写入值为{tbxWordVal.Text}");
            }
            else
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"写入失败........", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                Infrastructure.LogServiceHelper.Info($"向PLC地址{tbxWordAdd.Text}中写入值失败...");
            }
        }

        private void btnReadFloat_Click(object sender, RoutedEventArgs e)
        {
            if (!tbxFloatAdd.Text.Contains("DB"))
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"设定的读取地址错误，地址类型只能为DB类型，请设置正确的地址", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                return;
            }
            float retVal = (float)siemensS7Net.Read(tbxFloatAdd.Text,DataType.REAL);
            tbxFloatVal.Text = retVal.ToString();
            Infrastructure.LogServiceHelper.Info($"读取PLC地址{tbxFloatAdd.Text}中的值为");
        }

        private void btnWriteFloat_Click(object sender, RoutedEventArgs e)
        {
            if (!tbxFloatAdd.Text.Contains("DB"))
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"设定的写入地址错误，地址类型只能为DB类型，请设置正确的地址", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                return;
            }
            float writeVal = float.Parse(tbxFloatVal.Text);
            bool retVal = siemensS7Net.Write(tbxFloatAdd.Text,writeVal);
            if (retVal)
            {
                Infrastructure.LogServiceHelper.Info($"写入地址{tbxFloatAdd.Text}中的{tbxFloatVal.Text}成功");
            }
            else
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"写入失败........", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                Infrastructure.LogServiceHelper.Info($"向PLC地址{tbxFloatAdd.Text}中写入值失败...");
            }
        }

        private void btnReadInt_Click(object sender, RoutedEventArgs e)
        {
            if (!tbxIntAdd.Text.Contains("DB"))
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"设定的读取地址错误，地址类型只能为DB类型，请设置正确的地址", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                return;
            }
            short retVal = (short)siemensS7Net.Read(tbxIntAdd.Text, DataType.INT);
            tbxIntVal.Text = retVal.ToString();
            Infrastructure.LogServiceHelper.Info($"读取PLC地址{tbxIntAdd.Text}中的值为");
        }

        private void btnWriteInt_Click(object sender, RoutedEventArgs e)
        {
            if (!tbxIntAdd.Text.Contains("DB"))
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"设定的写入地址错误，地址类型只能为DB类型，请设置正确的地址", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                return;
            }
            int writeVal = int.Parse(tbxIntVal.Text);
            bool retVal = siemensS7Net.Write(tbxIntAdd.Text, writeVal);
            if (retVal)
            {
                Infrastructure.LogServiceHelper.Info($"写入地址{tbxIntAdd.Text}中的{tbxIntVal.Text}成功");
            }
            else
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"写入失败........", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                Infrastructure.LogServiceHelper.Info($"向PLC地址{tbxIntAdd.Text}中写入值失败...");
            }
        }

        private void btnReadString_Click(object sender, RoutedEventArgs e)
        {
            if (!tbxStringAdd.Text.Contains("DB"))
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"设定的读取地址错误，地址类型只能为DB类型，请设置正确的地址", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                return;
            }
            string retVal = (string)siemensS7Net.Read(tbxStringAdd.Text, DataType.STRING);
            tbxStringVal.Text = retVal.ToString();
            Infrastructure.LogServiceHelper.Info($"读取PLC地址{tbxStringVal.Text}中的值为");
        }

        private void btnWriteString_Click(object sender, RoutedEventArgs e)
        {
            if (!tbxStringAdd.Text.Contains("DB"))
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"设定的写入地址错误，地址类型只能为DB类型，请设置正确的地址", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                return;
            }
            int writeVal = int.Parse(tbxStringVal.Text);
            bool retVal = siemensS7Net.Write(tbxStringAdd.Text, writeVal);
            if (retVal)
            {
                Infrastructure.LogServiceHelper.Info($"写入地址{tbxStringAdd.Text}中的{tbxStringVal.Text}成功");
            }
            else
            {
                Xceed.Wpf.Toolkit.MessageBox.Show(Application.Current.MainWindow, $"写入失败........", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Error, null);
                Infrastructure.LogServiceHelper.Info($"向PLC地址{tbxStringAdd.Text}中写入值失败...");
            }
        }
    }
}
