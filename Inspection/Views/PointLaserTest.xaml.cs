﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Threading;
using LogSv;
using Inspection.Class;
using Inspection.Helper;
namespace Inspection.Views
{
    /// <summary>
    /// TcpClientTest.xaml 的交互逻辑
    /// </summary>
    public partial class PointLaserTest : UserControl
    {
        KMeasureHeight PLaser;
        public PointLaserTest()
        {
            InitializeComponent();
            if (GlobalParame.PLaser == null)
            {
                if (!ClsFunction.IsIpAdress(tbxIpAddress.Text))
                {
                    Log.log.Write("IP格式设置错误,请设置正确的IP地址.....", System.Drawing.Color.Black);
                    return;
                }
                PLaser = new KMeasureHeight(tbxIpAddress.Text, (int)numPort.Value, true);
            }
            else
            {
                PLaser = GlobalParame.PLaser;
            }
        }

        
     

        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {            
            if (!ClsFunction.IsIpAdress(tbxIpAddress.Text))
            {
                Log.log.Write("IP格式设置错误,请设置正确的IP地址.....", System.Drawing.Color.Black);
                return;
            }
            else
            {
                if (PLaser.ConnectTcpClient(tbxIpAddress.Text, (int)numPort.Value))
                {                                   
                    Log.log.Write("与服务器通讯连接成功!", System.Drawing.Color.Black);
                    btnConnect.Background = new SolidColorBrush(Colors.Green);
                }
                else
                {                
                    Log.log.Write("与服务器通讯连接失败.......", System.Drawing.Color.Black);
                    btnConnect.Background = new SolidColorBrush(Colors.Red);
                }
            }
        }

        private void btnDisConnect_Click(object sender, RoutedEventArgs e)
        {
            PLaser.DisConnect();
        }             

        private void btnSend_Click(object sender, RoutedEventArgs e)
        {
            PLaser.SendCommend(sendContent.Text);
        }

        private void recieveContent_Scroll(object sender, System.Windows.Controls.Primitives.ScrollEventArgs e)
        {

        }

        private void clar_Click(object sender, RoutedEventArgs e)
        {
            recieveContent.Clear();
        }

        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            recieveContent.Text = PLaser.reciceMsg;
        }
    }
}
