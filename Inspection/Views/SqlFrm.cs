﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Sunny.UI;

namespace Inspection
{
    public partial class SqlFrm :UIForm
    {
       
        public SqlFrm()
        {
            InitializeComponent();          
            // 设定包括Header和所有单元格的列宽自动调整 【Add on 20200226】
            dataGridView1 .AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            // 设定包括Header和所有单元格的行高自动调整 
            dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
            this.TopLevel = false;
            this.TopMost = false;          
        }
       
        private void btn_Conn_Click(object sender, EventArgs e)
        {
           MySql.Data.MySqlClient.MySqlConnection conn  = SysPara.mySql.ConnectionSQL();
            if (conn!=null)
                btn_Conn.BackColor = Color.Green;
            else
                btn_Conn.BackColor = Color.Red;
        }

        private void btn_Read_Click(object sender, EventArgs e)
        {
            if (cbx_TableName.SelectedItem == null) return;
            string TabName = cbx_TableName.SelectedItem.ToString();
            DataTable Dt = new DataTable();
            SysPara.mySql.BatchReadData(TabName, ref Dt);
            dataGridView1.DataSource = Dt;
        }

        private void btn_ExporttToCSV_Click(object sender, EventArgs e)
        {
            SysPara.mySql.SQL_ExportToCSV(dataGridView1);
        }

        private void SqlFrm_Load(object sender, EventArgs e)
        {
            List<string> tableList = null;
            if (SysPara.IsSqlOpen)
            {
                SysPara.mySql.IterateTables(ref tableList);
                for (int i = 0; i < tableList.Count; i++)
                {
                    cbx_TableName.Items.Add(tableList[i].ToString());
                }
            }
        }

        private void btn_Find_Click(object sender, EventArgs e)
        {
            string Condition = string.Format("产品ID='{0}'", tbx_Code.Text);
            DataTable Dt = new DataTable();
            string tabName = "tab_2023_7_19";
            SysPara.mySql.ReadData(tabName, Condition, ref Dt);
            dataGridView1.DataSource = Dt;
        }

        protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                this.btn_Find.PerformClick();
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void tbx_StartDate_Click(object sender, EventArgs e)
        {
            start_monthCalendar.Visible = true;
        }

        private void tbx_EndDate_Click(object sender, EventArgs e)
        {
            end_monthCalendar.Visible = true;
        }

        private void Calendar_DateSelected(object sender, DateRangeEventArgs e)
        {
            string calendarName = ((MonthCalendar)sender).Name.ToString();
            switch (calendarName)
            {
                case "start_monthCalendar":
                    start_monthCalendar.Visible = false;
                    break;
                case "end_monthCalendar":
                    tbx_EndDate.Text = end_monthCalendar.SelectionStart.ToShortDateString();
                    break;
            }          
        }

        UIStyle myStyle;

        private void Calendar_DateChanged(object sender, DateRangeEventArgs e)
        {
            string calendarName = ((MonthCalendar)sender).Name.ToString();         
            switch (calendarName)
            {
                case "start_monthCalendar":             
                    tbx_StartDate.Text = start_monthCalendar.SelectionStart.ToShortDateString();
                    start_monthCalendar.Visible = false;
                    break;
                case "end_monthCalendar":               
                    if (Convert.ToDateTime(start_monthCalendar.SelectionStart.ToShortDateString()) > Convert.ToDateTime(end_monthCalendar.SelectionStart.ToShortDateString()))
                    {
                        myStyle = UIStyle.Red;                    
                        UIMessageDialog.ShowWarningDialog(this,"您选择的结束日期小于开始日期，请重新设置结束日期!", myStyle);
                        tbx_EndDate.Clear();
                        return;
                    }
                    tbx_EndDate.Text = end_monthCalendar.SelectionStart.ToShortDateString();
                    end_monthCalendar.Visible = false;
                    break;
            }
        }
    }
}
