﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Threading;
using LogSv;
using Inspection.Helper;
using HslCommunication.ModBus;
using HslCommunication;
namespace Inspection.Views
{
    /// <summary>
    /// TcpClientTest.xaml 的交互逻辑
    /// </summary>
    public partial class UVLightTest : UserControl
    {
        UVLight_ModbusTcp uvLight;
        public UVLightTest()
        {
            InitializeComponent();
            if (uvLight == null)
            {
                uvLight = new UVLight_ModbusTcp();
            }
            else
            {
                uvLight = GlobalParame.UVLight;
            }
        }





        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {
            if (!ClsFunction.IsIpAdress(tbxIpAddress.Text))
            {
                Log.log.Write("IP格式设置错误,请设置正确的IP地址.....", System.Drawing.Color.Black);
                return;
            }
            else
            {
                uvLight.Connect(tbxIpAddress.Text, Convert.ToString((int)numPort.Value));
                if (uvLight.Connected)
                {
                    Log.log.Write("与UVLight控制器通讯连接成功!", System.Drawing.Color.Black);
                }
                else
                {
                    Log.log.Write("与UVLight控制器通讯连接失败.......", System.Drawing.Color.Black);
                }
            }
        }

        private void btnDisConnect_Click(object sender, RoutedEventArgs e)
        {
            uvLight.DisConnect();
        }



        private void recieveContent_Scroll(object sender, System.Windows.Controls.Primitives.ScrollEventArgs e)
        {

        }

        private void clar_Click(object sender, RoutedEventArgs e)
        {
            recieveContent_Power.Clear();
        }

        private void btnSetPowerLevel_Click(object sender, RoutedEventArgs e)
        {
            if (uvLight.Connected)
            {
                if (Channel.Value == null) { Log.log.Write("未设置通道号", System.Drawing.Color.Red);return; }
                //设置功率10%-100%
                uvLight.UVLight_SetPowerLevel((int)(Channel.Value), (int)(PowerLevel.Value));
            }
        }

        private void btnSetPowerRange_Click(object sender, RoutedEventArgs e)
        {
            if (uvLight.Connected)
            {
                if (Channel.Value == null) { Log.log.Write("未设置通道号", System.Drawing.Color.Red); return; }
                //设置功率范围
                uvLight.UVLight_SetOpticalPowerRange((int)(Channel.Value), (int)(Low.Value), (int)(Hight.Value));
            }
        }

        private void btnGetPowerLevel_Click(object sender, RoutedEventArgs e)
        {
            if (uvLight.Connected)
            {
                if (Channel.Value == null) { Log.log.Write("未设置通道号", System.Drawing.Color.Red); return; }
                //读通道1 UV灯功率
                recieveContent_Power.Text = uvLight.UVLight_GetOpticalPower((int)(Channel.Value)).ToString();
            }
           
        }

        private void clearStrength_Click(object sender, RoutedEventArgs e)
        {
            recieveContent_Strength.Clear();
        }

        private void btnGetStrengthLevel_Click(object sender, RoutedEventArgs e)
        {
            if (uvLight.Connected)
            {
                if (Channel.Value == null) { Log.log.Write("未设置通道号", System.Drawing.Color.Red); return; }
                //读通道1 UV灯强度
                recieveContent_Strength.Text = uvLight.UVLight_GetStrength((int)(Channel.Value)).ToString();
            }
        }

        private void clearTemperature_Click(object sender, RoutedEventArgs e)
        {
            recieveContent_Temperature.Clear();
        }

        private void btnGetTemperatureLevel_Click(object sender, RoutedEventArgs e)
        {
            if (uvLight.Connected)
            {
                if (Channel.Value == null) { Log.log.Write("未设置通道号", System.Drawing.Color.Red); return; }
                //读通道1 UV灯温度（光头温度）
                recieveContent_Temperature.Text = uvLight.UVLight_GetTemperature((int)(Channel.Value)).ToString();
            }
        }

        private void btnTurnOn_Click(object sender, RoutedEventArgs e)
        {
            if (uvLight.Connected)
            {
                if (Channel.Value == null) { Log.log.Write("未设置通道号", System.Drawing.Color.Red); return; }
                //打开通道1 UV灯
                uvLight.UVLight_TurnOn((int)(Channel.Value));
            }
        }

        private void btnTurnOff_Click(object sender, RoutedEventArgs e)
        {
            if (uvLight.Connected)
            {
                if (Channel.Value == null) { Log.log.Write("未设置通道号", System.Drawing.Color.Red); return; }
                //关闭通道1 UV灯
                uvLight.UVLight_TurnOff((int)(Channel.Value));
            }
        }
    }
}
