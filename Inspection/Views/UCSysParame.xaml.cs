﻿using Infrastructure;
using Inspection.Helper;
using S7.Net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Inspection.Views
{
    /// <summary>
    /// Interaction logic for UCSysParame.xaml
    /// </summary>
    public partial class UCSysParame : UserControl
    {
        public UCSysParame()
        {
            InitializeComponent();
        }

        public static DependencyProperty SystemParameProperty = DependencyProperty.Register("SystemParame", typeof(SysInitialParame), typeof(UCSysParame), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        /// <summary>
        /// IP地址
        /// </summary>
        public SysInitialParame SystemParame
        {
            get { return (SysInitialParame)GetValue(SystemParameProperty); }
            set { SetValue(SystemParameProperty, value); }
        }

        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            if (null == SystemParame) return;      
            try
            {
                if (null == GlobalParame.DCCKTCPClient || !GlobalParame.DCCKTCPClient.IsConnected)
                {
                    GlobalParame.DCCKTCPClient = new SuperSimpleTcp.SimpleTcpClient("127.0.0.1", SystemParame.DCTcpPortNum);
                    GlobalParame.DCCKTCPClient.Connect();
                }
                if (null == GlobalParame.RightSlotDCCKTCPClient || !GlobalParame.RightSlotDCCKTCPClient.IsConnected)
                {
                    GlobalParame.RightSlotDCCKTCPClient = new SuperSimpleTcp.SimpleTcpClient("127.0.0.1", SystemParame.RightSlotDCTcpPortNum);
                    GlobalParame.RightSlotDCCKTCPClient.Connect();
                }
                if (null == GlobalParame.HeightLaser || !GlobalParame.HeightLaser.IsConnected)
                {
                    GlobalParame.HeightLaser = new SuperSimpleTcp.SimpleTcpClient(SystemParame.LaserIP, SystemParame.LaserPortNum);
                    GlobalParame.HeightLaser.Connect();
                }
                if (null == GlobalParame.RightHeightLaser || !GlobalParame.RightHeightLaser.IsConnected)
                {
                    GlobalParame.RightHeightLaser = new SuperSimpleTcp.SimpleTcpClient(SystemParame.RightLaserIP, SystemParame.RightLaserPortNum);
                    GlobalParame.RightHeightLaser.Connect();
                }
                if (null == GlobalParame.PLC || !GlobalParame.PLC.IsConnected)
                {
                    GlobalParame.PLC = new S7.Net.Plc(CpuType.S71500, SystemParame.PLCIP, 0, 0);
                    GlobalParame.PLC.Open();
                }              
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            SystemParame.Save();
        }
    }
}
