﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Threading;
using LogSv;
using Inspection.Helper;
using Color = System.Drawing.Color;
using System.Threading;

namespace Inspection.Views
{
    /// <summary>
    /// TcpClientTest.xaml 的交互逻辑
    /// </summary>
    public partial class LaserTest : UserControl
    {
        public LaserTest()
        {
            InitializeComponent();
            if (GlobalParame.Laser == null)
            {
                GlobalParame.Laser = new LaserInstance();      
                
            }
        }



     

        private void btnConnect_Click(object sender, RoutedEventArgs e)
        {            
            if (!ClsFunction.IsIpAdress(tbxIpAddress.Text))
            {
                Log.log.Write("IP格式设置错误,请设置正确的IP地址.....", System.Drawing.Color.Black);
                return;
            }
            else
            {
                GlobalParame.HeightLaser.Disconnect();
                GlobalParame.HeightLaser.Dispose();
                GlobalParame.RightHeightLaser.Disconnect();
                GlobalParame.RightHeightLaser.Dispose();
                if (GlobalParame.Laser.Connect(tbxIpAddress.Text, (int)numPort.Value))
                {                                   
                    Log.log.Write("与服务器通讯连接成功!", System.Drawing.Color.Black);
                    btnConnect.Background = new SolidColorBrush(Colors.Green);
                }
                else
                {                
                    Log.log.Write("与服务器通讯连接失败.......", System.Drawing.Color.Black);
                    btnConnect.Background = new SolidColorBrush(Colors.Red);
                }
            }
        }

        private void btnDisConnect_Click(object sender, RoutedEventArgs e)
        {
            GlobalParame.Laser.DisConnect();                                
        }             

        private void btnSend_Click(object sender, RoutedEventArgs e)
        {
            GlobalParame.Laser.Trigger();
            Thread.Sleep(100);
            if (string.IsNullOrEmpty(GlobalParame.Laser.RecevicedMsg)) return;
            var arr = GlobalParame.Laser.RecevicedMsg.Split(',');
            double.TryParse(arr[1], out double val);
            recieveContent.Text = (val * 0.001).ToString();
        }


        private void clar_Click(object sender, RoutedEventArgs e)
        {
            recieveContent.Clear();
        }

        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            recieveContent.Text = GlobalParame.Laser.RecevicedMsg;
        }

        private void btnSwicthProgram_Click(object sender, RoutedEventArgs e)
        {
            GlobalParame.Laser.SwitchProgram(Convert.ToInt16(ProgramNo.Text));
        }
    }
}
