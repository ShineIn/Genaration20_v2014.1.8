﻿using Inspection.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ModuleData;
using ModuleData.Views;
using MaterialDesignThemes.Wpf;

namespace Inspection.Views
{
    /// <summary>
    /// UC_OperatInformation.xaml 的交互逻辑
    /// </summary>
    public partial class UC_OperatInformation : UserControl
    {
        public UC_OperatInformation()
        {
            InitializeComponent();

        }

        public static DependencyProperty SubModuleProperty =
          DependencyProperty.Register("SubModule", typeof(GlueProject), typeof(UC_OperatInformation), new FrameworkPropertyMetadata(new GlueProject(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public GlueProject SubModule
        {
            get { return (GlueProject)GetValue(SubModuleProperty); }
            set { SetValue(SubModuleProperty, value); }
        }
        private void 点胶复检结果_Click(object sender, RoutedEventArgs e)
        {
           WinCheckResult result = new WinCheckResult();
            result.Title = "点胶复检结果";
            result.ShowDialog();
        }
    }
}
