﻿using Inspection.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Inspection.Views
{
    /// <summary>
    /// UCMainTain.xaml 的交互逻辑
    /// </summary>
    public partial class UCMainTain : UserControl, INotifyPropertyChanged
    {
        public UCMainTain()
        {
            InitializeComponent();
            ShowContent = uCPLC;
            Binding binding = new Binding(".") { Source = GlobalParame.PLC};
            siemens.SetBinding(UCNewSiemenPLC.PLCModuleProperty,binding);
             binding = new Binding(".") { Source = GlobalParame.DCCKTCPClient };
            ucTcp.SetBinding(TcpClientTest.ClientProperty, binding);
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        public FrameworkElement ShowContent
        {
            get { return showContent; }
            set { showContent = value;OnPropertyChanged(); }
        }
        private FrameworkElement showContent;



        private void PLC_Click(object sender, RoutedEventArgs e)
        {
            ShowContent = uCPLC;
            uCPLC.InitPlc();
        }
    
        private readonly ucSimensPLC uCPLC = new ucSimensPLC();
        private readonly UCSql ucSql = new UCSql();
        private readonly TcpClientTest ucTcp = new TcpClientTest();
        private readonly LaserTest laser = new LaserTest();
        private readonly PointLaserTest pointLaser = new PointLaserTest();  
        private readonly UVLightTest uVLightTest = new UVLightTest();
        private readonly UCNewSiemenPLC siemens=new UCNewSiemenPLC();

        private void MySql_Click(object sender, RoutedEventArgs e)
        {
            ShowContent = ucSql;
        }

        private void TcpTest_Click(object sender, RoutedEventArgs e)
        {
            ShowContent = ucTcp;
        }

        private void LaserTest_Click(object sender, RoutedEventArgs e)
        {
            ShowContent = laser;
        }


        private void UVLightTest_Click(object sender, RoutedEventArgs e)
        {
            ShowContent = uVLightTest;
        }

        private void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            ShowContent = siemens;
        }
    }
}
