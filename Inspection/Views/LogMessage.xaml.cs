﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using G4.GlueEditor;
using LogSv;

namespace Inspection.Views
{
    /// <summary>
    /// LogMesage.xaml 的交互逻辑
    /// </summary>
    public partial class LogMessage : System.Windows.Window
    {
        public LogMessage()
        {
            InitializeComponent();
            panelLog.Controls.Add(Log.log);
            Log.log.Dock = System.Windows.Forms.DockStyle.Fill;
            Loaded += LogMesage_Loaded;
        }     

        private void LogMesage_Loaded(object sender, RoutedEventArgs e)
        {
            Log.log.Show();
        }     
    }
}
