﻿using Inspection.Class;
using ModuleData;
using ModuleData.Views;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Inspection.Views
{
    /// <summary>
    /// UC_WorkFlow.xaml 的交互逻辑
    /// </summary>
    public partial class UC_WorkFlow : UserControl
    {
        public UC_WorkFlow()
        {
            InitializeComponent();
        }

        public static DependencyProperty SubModuleProperty =
            DependencyProperty.Register("SubModule", typeof(ModuleRunStatus), typeof(UC_WorkFlow), new FrameworkPropertyMetadata(new ModuleRunStatus(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public ModuleRunStatus SubModule
        {
            get { return (ModuleRunStatus)GetValue(SubModuleProperty); }
            set { SetValue(SubModuleProperty, value); }
        }
    
    }

    public class BoolStatusConv : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue || null == value || !(value is bool brun)) return "空闲";
            return brun ? "运行中" : "空闲";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
           return this;
        }
    }
}
