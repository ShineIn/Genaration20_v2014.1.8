﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;
using System.Threading.Tasks;
using System.Windows.Forms;
using LogSv;
using HslCommunication;
using Inspection.Helper;
using HslCommunication.Profinet.Inovance;
using FormCamera;

namespace Inspection
{
    public partial class FormPLC : UserControl
    {
        public FormPLC()
        {
            InitializeComponent();
        }

        private string plcIP = "192.168.1.120";
        public string PlcIP
        {
            get => plcIP;
            set => plcIP = value;
        }

        private int plcPort = 502;
        public int PlcPort
        {
            get => plcPort;
            set => plcPort = value;
        }      

        private void FormPLC_Load(object sender, EventArgs e)
        {          
            if (!Global.ModulePara.PlcState)
            {
                Log.log.Write("PLC连接未打开,需要重新连接....", Color.Black);
                if (ClsFunction.IsIpAdress(PlcIP))
                {
                    Log.log.Write("IP地址设置错误,请设置正确的IP地址", Color.Black);
                    Global.ModulePara.PlcState = false;
                }
                else
                {
                    GlobalParame.PLC.IpAddress = PlcIP;
                    GlobalParame.PLC.Port = plcPort;
                    GlobalParame.PLC.ConnectClose();
                    GlobalParame.PLC.IsStringReverse = false;
                   OperateResult plcConnect = GlobalParame.PLC.ConnectServer();
                    if (plcConnect.IsSuccess)
                    {
                        Log.log.Write("PLC连接成功！", Color.Black);
                    }
                    else
                    {
                        Log.log.Write("PLC连接失败...", Color.Yellow);
                    }
                }
            }
        }

        private void btn_ReadBitAddress_Click(object sender, EventArgs e)
        {
           string address = tbx_BitAddress.Text.Trim();
            if (address != "")
            {
                OperateResult<bool[]> readVal = GlobalParame.PLC.ReadBool(address, 1);
                if (readVal.IsSuccess)
                {
                    Log.log.Write(string.Format("软元件{0}值读取成功！", address), Color.Black);
                    tbx_bitValue.Text = readVal.Content[0].ToString();
                    btn_ReadBitAddress.BackColor = Color.Green;
                }
                else
                {
                    Log.log.Write(string.Format("软元件{0}值读取失败...", address), Color.Black);
                    btn_ReadBitAddress.BackColor = Color.Red;
                }
            }
            else
            {
                Log.log.Write("软元件地址为空....",Color.Black);
                Interaction.MsgBox("软元件地址为空，请输入软元件地址再进行读取...",MsgBoxStyle.Exclamation);
            }
        }

        private void btn_WriteBitAddress_Click(object sender, EventArgs e)
        {
            string address = tbx_BitAddress.Text.Trim();
            bool writeValue = false;
            bool bConvert = bool.TryParse(tbx_bitValue.Text.Trim(),out writeValue);                      
            if (address != ""&& bConvert)
            {
                OperateResult bWriteVal = GlobalParame.PLC.Write(address, writeValue);
                if (bWriteVal.IsSuccess)
                {
                    btn_WriteBitAddress.BackColor = Color.Green;
                    Log.log.Write(string.Format("写入地址{0}值{1}成功！",address,writeValue),Color.Black);
                }
                else
                {
                    btn_WriteBitAddress.BackColor = Color.Red;
                    Log.log.Write("写入失败！", Color.Red);
                }
            }
            else
            {
                Log.log.Write("软元件地址或写入值为空....", Color.Black);
                Interaction.MsgBox("软元件地址或写入值为空，请输入软元件地址再写入...", MsgBoxStyle.Exclamation);
            }
        }

        private void btn_ReadWordAddress_Click(object sender, EventArgs e)
        {
            string address = tbx_WordAddress.Text.Trim();
            if (address != "")
            {
                OperateResult<Int16> readVal = GlobalParame.PLC.ReadInt16(address);
                if (readVal.IsSuccess)
                {
                    btn_ReadWordAddress.BackColor = Color.Green;
                    Log.log.Write($"读取软元件:{address}中的值成功！", Color.Black);
                    tbx_WordVal.Text = readVal.Content.ToString();
                }
                else
                {
                    btn_ReadWordAddress.BackColor = Color.Red;
                    Log.log.Write($"读取软元件:{address}中的值失败....", Color.Black);
                }
            }
            else
            {
                Log.log.Write("软元件地址为空....", Color.Black);
                Interaction.MsgBox("软元件地址为空，请输入软元件地址再写入...", MsgBoxStyle.Exclamation);
            }
        }

        private void btn_WriteWordAddress_Click(object sender, EventArgs e)
        {
            string address = tbx_WordAddress.Text.Trim();
            short writeVal = 0;
            bool bConvert = short.TryParse(tbx_WordVal.Text.Trim(),out writeVal);
            if (address!="" && bConvert)
            {
                OperateResult bRet = GlobalParame.PLC.Write(address, writeVal);
                if (bRet.IsSuccess)
                {
                    Log.log.Write($"写入地址:{address}的中的值:{writeVal}成功！", Color.Black);
                    btn_WriteWordAddress.BackColor = Color.Green;
                }
                else
                {
                    Log.log.Write($"写入地址:{address}的中的值:{writeVal}失败....", Color.Black);
                    btn_WriteWordAddress.BackColor = Color.Red;
                }
            }
            else
            {
                Log.log.Write("软元件或写入值为空....", Color.Black);
                Interaction.MsgBox("软元件地址或写入值为空，请输入软元件地址或写入值再写入...", MsgBoxStyle.Exclamation);
            }
        }

        private void btn_ReadFloatAddress_Click(object sender, EventArgs e)
        {
            string address = tbx_floatAddress.Text.Trim();           
            string address2 = string.Empty;           
            float floatValue;
            if (address == "" || !address.Contains("D"))
            {
                Xceed.Wpf.Toolkit.MessageBox.Show("输入地址错误，请输入正确的PLC地址");
                return;
            }
            OperateResult<byte[]> retBytes = GlobalParame.PLC.Read(address, 2);
            if (retBytes.IsSuccess)
            {
                if (!ClsFunction.BytesToFloat(retBytes.Content, out floatValue))
                {
                    Log.log.Write($"读取软元件地址{address}失败....", Color.Red);
                    Xceed.Wpf.Toolkit.MessageBox.Show($"读取软元件地址{address}失败...");
                }
                Log.log.Write($"读取软元件地址{address}成功,且其值为{floatValue}！", Color.Red);
                nud_floatValue.Value = (decimal)floatValue;                  //将读取的字节数转换为浮点数
            }
            else
            {
                Log.log.Write($"读取软元件地址{address}失败....", Color.Red);
                Xceed.Wpf.Toolkit.MessageBox.Show($"读取软元件地址{address}失败...");
                btn_ReadFloatAddress.BackColor = Color.Red;
            }
        }

        private void btn_WriteFloatAddress_Click(object sender, EventArgs e)
        {
            string address = tbx_floatAddress.Text.Trim();
            float fVal = (float)nud_floatValue.Value;
            Int16[] writeValue;
            ClsFunction.FloatToInt(fVal,out writeValue);                    //向PLC中写入浮点数          
           
            string[] address2Array = null;
            string address2 = string.Empty;
            int iAddressNo = 0;           
            if (!address.Contains("D"))
            {
                Xceed.Wpf.Toolkit.MessageBox.Show("输入地址错误，请输入正确的PLC地址");
                return;
            }

            address2Array = address.Split('D');
            if (!int.TryParse(address2Array[1], out iAddressNo))
            {
                Xceed.Wpf.Toolkit.MessageBox.Show("输入地址错误，请输入正确的PLC地址");
                return;
            }

            address2 = "D" + (iAddressNo + 1).ToString();

            if (address!="" )
            {
                if (GlobalParame.PLC.Write(address, writeValue).IsSuccess) //&& GlobalParame.PLC.Write(address2, (Int16)writeValue[1]).IsSuccess)
                {
                    btn_WriteFloatAddress.BackColor = Color.Green;
                    Log.log.Write($"写入软元件地址{address}中浮点数{fVal}成功！", Color.Black);
                }
                else
                {
                    btn_WriteFloatAddress.BackColor = Color.Red;
                    Log.log.Write($"写入软元件地址{address}失败....", Color.Black);
                    Interaction.MsgBox($"写入软元件地址{address}失败!");
                }
            }
            else
            {
                Log.log.Write("软元件地址或写入值为空，请设置好软元件地址值及写入值之后再进行写入....",Color.Black);
            }
        }

        private void btn_ReadStrAddress_Click(object sender, EventArgs e)
        {
            string address = tbx_StrAddress.Text.Trim();
            ushort length = (ushort)nud_AddLenght.Value;
            if (address != "")
            {
                OperateResult<byte[]> retByte = GlobalParame.PLC.Read(address, length);
                if (retByte.IsSuccess)
                {
                    btn_ReadStrAddress.BackColor = Color.Green;                 
                    tbx_StrValue.Text = System.Text.Encoding.Default.GetString(retByte.Content);
                    Log.log.Write($"写入软元件地址:{address}中的字符串:{tbx_StrAddress.Text}成功！", Color.Black);
                }
                else
                {
                    btn_ReadStrAddress.BackColor = Color.Red;
                    Log.log.Write($"写入软元件地址:{address}失败....", Color.Black);
                    Interaction.MsgBox($"写入软元件地址:{address}失败!");
                }
            }
            else
            {
                Log.log.Write("软元件地址或写入值为空，请设置好软元件地址值及写入值之后再进行写入....", Color.Black);
            }
        }

        private void btn_WriteStrAddress_Click(object sender, EventArgs e)
        {
            string address = tbx_StrAddress.Text.Trim();
            string writeVal = tbx_StrValue.Text.Trim();
            if (address != "")
            {
                if (GlobalParame.PLC.Write(address, writeVal).IsSuccess)
                {
                    btn_ReadStrAddress.BackColor = Color.Green;
                    Log.log.Write($"写入软元件地址:{address}中的字符串:{writeVal}成功！", Color.Black);
                }
                else
                {
                    btn_ReadStrAddress.BackColor = Color.Red;
                    Log.log.Write($"写入软元件地址:{address}失败....", Color.Black);
                    Interaction.MsgBox($"写入软元件地址:{address}失败!");
                }
            }
            else
            {
                Log.log.Write("软元件地址或写入值为空，请设置好软元件地址值及写入值之后再进行写入....", Color.Black);
            }
        }
    }
}
