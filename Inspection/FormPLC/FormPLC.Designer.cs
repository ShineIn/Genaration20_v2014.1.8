﻿namespace Inspection
{
    partial class FormPLC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbx_bitValue = new System.Windows.Forms.TextBox();
            this.tbx_BitAddress = new System.Windows.Forms.TextBox();
            this.btn_WriteBitAddress = new System.Windows.Forms.Button();
            this.btn_ReadBitAddress = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tbx_WordVal = new System.Windows.Forms.TextBox();
            this.btn_WriteWordAddress = new System.Windows.Forms.Button();
            this.tbx_WordAddress = new System.Windows.Forms.TextBox();
            this.btn_ReadWordAddress = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.nud_floatValue = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbx_floatAddress = new System.Windows.Forms.TextBox();
            this.btn_WriteFloatAddress = new System.Windows.Forms.Button();
            this.btn_ReadFloatAddress = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.nud_AddLenght = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbx_StrValue = new System.Windows.Forms.TextBox();
            this.tbx_StrAddress = new System.Windows.Forms.TextBox();
            this.btn_WriteStrAddress = new System.Windows.Forms.Button();
            this.btn_ReadStrAddress = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_floatValue)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_AddLenght)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 74);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "读写值";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 31);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "软元件地址";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 31);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 15);
            this.label3.TabIndex = 0;
            this.label3.Text = "软元件地址";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 74);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 15);
            this.label4.TabIndex = 0;
            this.label4.Text = "读写值";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Silver;
            this.groupBox1.Controls.Add(this.tbx_bitValue);
            this.groupBox1.Controls.Add(this.tbx_BitAddress);
            this.groupBox1.Controls.Add(this.btn_WriteBitAddress);
            this.groupBox1.Controls.Add(this.btn_ReadBitAddress);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox1.Location = new System.Drawing.Point(6, 5);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox1.Size = new System.Drawing.Size(277, 104);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "读写位元件";
            // 
            // tbx_bitValue
            // 
            this.tbx_bitValue.Location = new System.Drawing.Point(94, 70);
            this.tbx_bitValue.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbx_bitValue.Name = "tbx_bitValue";
            this.tbx_bitValue.Size = new System.Drawing.Size(84, 24);
            this.tbx_bitValue.TabIndex = 2;
            // 
            // tbx_BitAddress
            // 
            this.tbx_BitAddress.Location = new System.Drawing.Point(94, 27);
            this.tbx_BitAddress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbx_BitAddress.Name = "tbx_BitAddress";
            this.tbx_BitAddress.Size = new System.Drawing.Size(84, 24);
            this.tbx_BitAddress.TabIndex = 2;
            // 
            // btn_WriteBitAddress
            // 
            this.btn_WriteBitAddress.Location = new System.Drawing.Point(196, 62);
            this.btn_WriteBitAddress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_WriteBitAddress.Name = "btn_WriteBitAddress";
            this.btn_WriteBitAddress.Size = new System.Drawing.Size(62, 34);
            this.btn_WriteBitAddress.TabIndex = 1;
            this.btn_WriteBitAddress.Text = "写入";
            this.btn_WriteBitAddress.UseVisualStyleBackColor = true;
            this.btn_WriteBitAddress.Click += new System.EventHandler(this.btn_WriteBitAddress_Click);
            // 
            // btn_ReadBitAddress
            // 
            this.btn_ReadBitAddress.Location = new System.Drawing.Point(196, 22);
            this.btn_ReadBitAddress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_ReadBitAddress.Name = "btn_ReadBitAddress";
            this.btn_ReadBitAddress.Size = new System.Drawing.Size(62, 34);
            this.btn_ReadBitAddress.TabIndex = 1;
            this.btn_ReadBitAddress.Text = "读取";
            this.btn_ReadBitAddress.UseVisualStyleBackColor = true;
            this.btn_ReadBitAddress.Click += new System.EventHandler(this.btn_ReadBitAddress_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Silver;
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.tbx_WordVal);
            this.groupBox2.Controls.Add(this.btn_WriteWordAddress);
            this.groupBox2.Controls.Add(this.tbx_WordAddress);
            this.groupBox2.Controls.Add(this.btn_ReadWordAddress);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox2.Location = new System.Drawing.Point(307, 9);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox2.Size = new System.Drawing.Size(274, 100);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "读写字元件";
            // 
            // tbx_WordVal
            // 
            this.tbx_WordVal.Location = new System.Drawing.Point(98, 70);
            this.tbx_WordVal.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbx_WordVal.Name = "tbx_WordVal";
            this.tbx_WordVal.Size = new System.Drawing.Size(84, 24);
            this.tbx_WordVal.TabIndex = 2;
            // 
            // btn_WriteWordAddress
            // 
            this.btn_WriteWordAddress.Location = new System.Drawing.Point(196, 62);
            this.btn_WriteWordAddress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_WriteWordAddress.Name = "btn_WriteWordAddress";
            this.btn_WriteWordAddress.Size = new System.Drawing.Size(62, 34);
            this.btn_WriteWordAddress.TabIndex = 1;
            this.btn_WriteWordAddress.Text = "写入";
            this.btn_WriteWordAddress.UseVisualStyleBackColor = true;
            this.btn_WriteWordAddress.Click += new System.EventHandler(this.btn_WriteWordAddress_Click);
            // 
            // tbx_WordAddress
            // 
            this.tbx_WordAddress.Location = new System.Drawing.Point(98, 27);
            this.tbx_WordAddress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbx_WordAddress.Name = "tbx_WordAddress";
            this.tbx_WordAddress.Size = new System.Drawing.Size(84, 24);
            this.tbx_WordAddress.TabIndex = 2;
            // 
            // btn_ReadWordAddress
            // 
            this.btn_ReadWordAddress.Location = new System.Drawing.Point(196, 22);
            this.btn_ReadWordAddress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_ReadWordAddress.Name = "btn_ReadWordAddress";
            this.btn_ReadWordAddress.Size = new System.Drawing.Size(62, 34);
            this.btn_ReadWordAddress.TabIndex = 1;
            this.btn_ReadWordAddress.Text = "读取";
            this.btn_ReadWordAddress.UseVisualStyleBackColor = true;
            this.btn_ReadWordAddress.Click += new System.EventHandler(this.btn_ReadWordAddress_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Silver;
            this.groupBox3.Controls.Add(this.nud_floatValue);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.tbx_floatAddress);
            this.groupBox3.Controls.Add(this.btn_WriteFloatAddress);
            this.groupBox3.Controls.Add(this.btn_ReadFloatAddress);
            this.groupBox3.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox3.Location = new System.Drawing.Point(6, 123);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox3.Size = new System.Drawing.Size(277, 106);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "读写浮点数";
            // 
            // nud_floatValue
            // 
            this.nud_floatValue.DecimalPlaces = 3;
            this.nud_floatValue.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.nud_floatValue.Location = new System.Drawing.Point(94, 70);
            this.nud_floatValue.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.nud_floatValue.Maximum = new decimal(new int[] {
            -1486618625,
            232830643,
            0,
            0});
            this.nud_floatValue.Minimum = new decimal(new int[] {
            -1486618625,
            232830643,
            0,
            -2147483648});
            this.nud_floatValue.Name = "nud_floatValue";
            this.nud_floatValue.Size = new System.Drawing.Size(83, 24);
            this.nud_floatValue.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 27);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 15);
            this.label6.TabIndex = 0;
            this.label6.Text = "软元件地址";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 74);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 15);
            this.label7.TabIndex = 0;
            this.label7.Text = "读写值";
            // 
            // tbx_floatAddress
            // 
            this.tbx_floatAddress.Location = new System.Drawing.Point(94, 23);
            this.tbx_floatAddress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbx_floatAddress.Name = "tbx_floatAddress";
            this.tbx_floatAddress.Size = new System.Drawing.Size(84, 24);
            this.tbx_floatAddress.TabIndex = 2;
            // 
            // btn_WriteFloatAddress
            // 
            this.btn_WriteFloatAddress.Location = new System.Drawing.Point(196, 64);
            this.btn_WriteFloatAddress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_WriteFloatAddress.Name = "btn_WriteFloatAddress";
            this.btn_WriteFloatAddress.Size = new System.Drawing.Size(62, 34);
            this.btn_WriteFloatAddress.TabIndex = 1;
            this.btn_WriteFloatAddress.Text = "写入";
            this.btn_WriteFloatAddress.UseVisualStyleBackColor = true;
            this.btn_WriteFloatAddress.Click += new System.EventHandler(this.btn_WriteFloatAddress_Click);
            // 
            // btn_ReadFloatAddress
            // 
            this.btn_ReadFloatAddress.Location = new System.Drawing.Point(196, 18);
            this.btn_ReadFloatAddress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_ReadFloatAddress.Name = "btn_ReadFloatAddress";
            this.btn_ReadFloatAddress.Size = new System.Drawing.Size(62, 34);
            this.btn_ReadFloatAddress.TabIndex = 1;
            this.btn_ReadFloatAddress.Text = "读取";
            this.btn_ReadFloatAddress.UseVisualStyleBackColor = true;
            this.btn_ReadFloatAddress.Click += new System.EventHandler(this.btn_ReadFloatAddress_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Silver;
            this.groupBox4.Controls.Add(this.nud_AddLenght);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.tbx_StrValue);
            this.groupBox4.Controls.Add(this.tbx_StrAddress);
            this.groupBox4.Controls.Add(this.btn_WriteStrAddress);
            this.groupBox4.Controls.Add(this.btn_ReadStrAddress);
            this.groupBox4.Font = new System.Drawing.Font("宋体", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.groupBox4.Location = new System.Drawing.Point(307, 123);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.groupBox4.Size = new System.Drawing.Size(274, 106);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "读写字符串";
            // 
            // nud_AddLenght
            // 
            this.nud_AddLenght.Location = new System.Drawing.Point(98, 50);
            this.nud_AddLenght.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.nud_AddLenght.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.nud_AddLenght.Name = "nud_AddLenght";
            this.nud_AddLenght.Size = new System.Drawing.Size(83, 24);
            this.nud_AddLenght.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 27);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 15);
            this.label5.TabIndex = 0;
            this.label5.Text = "软元件地址";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 54);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 15);
            this.label9.TabIndex = 0;
            this.label9.Text = "寄存器个数";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 80);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 15);
            this.label8.TabIndex = 0;
            this.label8.Text = "读写值";
            // 
            // tbx_StrValue
            // 
            this.tbx_StrValue.Location = new System.Drawing.Point(98, 76);
            this.tbx_StrValue.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbx_StrValue.Name = "tbx_StrValue";
            this.tbx_StrValue.Size = new System.Drawing.Size(84, 24);
            this.tbx_StrValue.TabIndex = 2;
            // 
            // tbx_StrAddress
            // 
            this.tbx_StrAddress.Location = new System.Drawing.Point(98, 23);
            this.tbx_StrAddress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.tbx_StrAddress.Name = "tbx_StrAddress";
            this.tbx_StrAddress.Size = new System.Drawing.Size(84, 24);
            this.tbx_StrAddress.TabIndex = 2;
            // 
            // btn_WriteStrAddress
            // 
            this.btn_WriteStrAddress.Location = new System.Drawing.Point(196, 69);
            this.btn_WriteStrAddress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_WriteStrAddress.Name = "btn_WriteStrAddress";
            this.btn_WriteStrAddress.Size = new System.Drawing.Size(62, 34);
            this.btn_WriteStrAddress.TabIndex = 1;
            this.btn_WriteStrAddress.Text = "写入";
            this.btn_WriteStrAddress.UseVisualStyleBackColor = true;
            this.btn_WriteStrAddress.Click += new System.EventHandler(this.btn_WriteStrAddress_Click);
            // 
            // btn_ReadStrAddress
            // 
            this.btn_ReadStrAddress.Location = new System.Drawing.Point(196, 18);
            this.btn_ReadStrAddress.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btn_ReadStrAddress.Name = "btn_ReadStrAddress";
            this.btn_ReadStrAddress.Size = new System.Drawing.Size(62, 34);
            this.btn_ReadStrAddress.TabIndex = 1;
            this.btn_ReadStrAddress.Text = "读取";
            this.btn_ReadStrAddress.UseVisualStyleBackColor = true;
            this.btn_ReadStrAddress.Click += new System.EventHandler(this.btn_ReadStrAddress_Click);
            // 
            // FormPLC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "FormPLC";
            this.Size = new System.Drawing.Size(591, 238);
            this.Load += new System.EventHandler(this.FormPLC_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_floatValue)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nud_AddLenght)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbx_bitValue;
        private System.Windows.Forms.TextBox tbx_BitAddress;
        private System.Windows.Forms.Button btn_WriteBitAddress;
        private System.Windows.Forms.Button btn_ReadBitAddress;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tbx_WordVal;
        private System.Windows.Forms.Button btn_WriteWordAddress;
        private System.Windows.Forms.TextBox tbx_WordAddress;
        private System.Windows.Forms.Button btn_ReadWordAddress;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbx_floatAddress;
        private System.Windows.Forms.Button btn_WriteFloatAddress;
        private System.Windows.Forms.Button btn_ReadFloatAddress;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbx_StrValue;
        private System.Windows.Forms.TextBox tbx_StrAddress;
        private System.Windows.Forms.Button btn_WriteStrAddress;
        private System.Windows.Forms.Button btn_ReadStrAddress;
        private System.Windows.Forms.NumericUpDown nud_floatValue;
        private System.Windows.Forms.NumericUpDown nud_AddLenght;
        private System.Windows.Forms.Label label9;
    }
}