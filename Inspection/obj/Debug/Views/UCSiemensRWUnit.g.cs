﻿#pragma checksum "..\..\..\Views\UCSiemensRWUnit.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "88464498AD822FF767749E905BD9DACBD0E93E981A596033FC8F807DE8CA3141"
//------------------------------------------------------------------------------
// <auto-generated>
//     此代码由工具生成。
//     运行时版本:4.0.30319.42000
//
//     对此文件的更改可能会导致不正确的行为，并且如果
//     重新生成代码，这些更改将会丢失。
// </auto-generated>
//------------------------------------------------------------------------------

using Inspection.Views;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Inspection.Views {
    
    
    /// <summary>
    /// UCSiemensRWUnit
    /// </summary>
    public partial class UCSiemensRWUnit : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        
        #line 8 "..\..\..\Views\UCSiemensRWUnit.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Inspection.Views.UCSiemensRWUnit unit;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\Views\UCSiemensRWUnit.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnReadInt;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\Views\UCSiemensRWUnit.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnWriteInt;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Inspection;component/views/ucsiemensrwunit.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Views\UCSiemensRWUnit.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.unit = ((Inspection.Views.UCSiemensRWUnit)(target));
            return;
            case 2:
            this.btnReadInt = ((System.Windows.Controls.Button)(target));
            
            #line 25 "..\..\..\Views\UCSiemensRWUnit.xaml"
            this.btnReadInt.Click += new System.Windows.RoutedEventHandler(this.btnReadInt_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.btnWriteInt = ((System.Windows.Controls.Button)(target));
            
            #line 26 "..\..\..\Views\UCSiemensRWUnit.xaml"
            this.btnWriteInt.Click += new System.Windows.RoutedEventHandler(this.btnWriteInt_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

