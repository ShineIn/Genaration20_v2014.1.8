﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using System.Xml;
using Infrastructure;
using Inspection.Views;
using MaterialDesignThemes.Wpf;
using System.Collections.ObjectModel;
using System.IO;
using Xceed.Wpf.Toolkit;
using LogSv;
using Sunny.UI;
using Microsoft.Xaml.Behaviors;
using UserManagement;
using System.Dynamic;
using System.Windows.Controls.Primitives;
using Microsoft.VisualBasic;
using System.Windows.Threading;
using System.Data;
using System.Threading;
using Color = System.Drawing.Color;
using HslCommunication.Profinet.Inovance;
using Inspection.Helper;
using HslCommunication;
using System.Windows.Forms;
using VUControl;
using System.Data.Common;
using ModuleData;
using System.Diagnostics;
using ModuleData.ViewModels;
using ModuleData20;
using S7.Net;
using ModuleData20.ViewModels;
using Microsoft.Win32;
using Googo.Manul;
using System.Linq.Expressions;

namespace Inspection
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : System.Windows.Window, INotifyPropertyChanged
    {
        public static SuperSimpleTcp.SimpleTcpClient DCCKTCPClient;//德创心跳
        bool bDCCKHeartBeat = false;
        int disConnectTime = 0;
        DispatcherTimer TimerSpy, TimerRefreshUI, TimerMonitor;
        private Thread ThreadHeartBeat = null;                                                     //心跳线程
        frmConfirm confirmFrm = new frmConfirm();
        public MainWindow()
        {
            InitializeComponent();
            DataContext = model;
            AddCommand();
            

            TimeSpan timerRefreshSpan = new TimeSpan(0, 0, 0, 1, 0);
            InitTimer(ref TimerRefreshUI, timerRefreshSpan);

            TimeSpan timerSpanMonitor = new TimeSpan(0, 0, 0, 0, 100);
            InitTimer(ref TimerMonitor, timerSpanMonitor);
            LoadAllParme();
            //StationTwoIntial parame = model.ProjectInfor.MountingProject.InitParm;
            SysInitialParame parame = model.ProjectInfor.InitProject;
            if (null == parame) return;
            GlobalParame.SysParame = parame;
            TimeSpan timerSpan = new TimeSpan(0, 0, parame.DCHBTickInterval);
            InitTimer(ref TimerSpy, timerSpan);
            CheckVPProg();
            GlobalParame.DCCKTCPClient = new SuperSimpleTcp.SimpleTcpClient("127.0.0.1", parame.DCTcpPortNum);
            GlobalParame.RightSlotDCCKTCPClient = new SuperSimpleTcp.SimpleTcpClient("127.0.0.1", parame.RightSlotDCTcpPortNum);
            GlobalParame.HeightLaser = new SuperSimpleTcp.SimpleTcpClient(parame.LaserIP, parame.LaserPortNum);
            GlobalParame.RightHeightLaser = new SuperSimpleTcp.SimpleTcpClient(parame.RightLaserIP, parame.RightLaserPortNum);
            GlobalParame.PLC = new S7.Net.Plc(CpuType.S71500, parame.PLCIP, 0, 0);
            DCCKTCPClient = new SuperSimpleTcp.SimpleTcpClient("127.0.0.1", parame.DCHBTcpPortNum);
            if (TcpObjectConnect(DCCKTCPClient) && DCCKTCPClient.IsConnected)
            {
                DCCKTCPClient.Events.DataReceived += Alg_DataReceived;
            }
            TcpObjectConnect(GlobalParame.HeightLaser);
            TcpObjectConnect(GlobalParame.RightHeightLaser);
            TcpObjectConnect(GlobalParame.DCCKTCPClient);
            TcpObjectConnect(GlobalParame.RightSlotDCCKTCPClient);
            //GlobalParame.HeightLaser.Connect();
            //GlobalParame.RightHeightLaser.Connect();
            //GlobalParame.PLC.Open();

            try
            {
                GlobalParame.PLC.Open();

            }
            catch (Exception ex)
            {
                Log.log.Write("PLC连接失败", Color.Red);
                Debug.WriteLine(ex);
            }

            InitialFlow = initial;
            Glue1Flow = leftautoglue;
            Glue2Flow = rightautoglue;
            MountingFlow = mountingFlow;

            Loaded += MainWindow_Loaded;

        }

        private bool TcpObjectConnect(SuperSimpleTcp.SimpleTcpClient obj)
        {
            try
            {
                obj.Connect();
                return true;
            }
            catch (Exception)
            {
                Log.log.Write($"{obj.ServerIpPort}连接失败", Color.Red); return false;
            }
        }

        public void InitDgv_AlarmTable(VUDatagridView dataGridView, ref List<Tuple<int, string, string, string, string>> alarmDgvDatas)
        {
            for (int i = 0; i < dataGridView.myDgv.Rows.Count; i++)
            {
                int code = 0;
                if (int.TryParse(dataGridView.GetDataArrayByIndex(i)[0], out code))
                {
                    Tuple<int, string, string, string, string> dgvData = new Tuple<int, string, string, string, string>
                    (code, dataGridView.GetDataArrayByIndex(i)[1], dataGridView.GetDataArrayByIndex(i)[2], dataGridView.GetDataArrayByIndex(i)[3], dataGridView.GetDataArrayByIndex(i)[4]);
                    alarmDgvDatas.Add(dgvData);
                }
                else
                {
                    return;
                }
            }
        }

        private void LoadAllParme()
        {
            string strErr = string.Empty;
            //右边RX贴装参数
            MountingProject Mountingproject = XmlSerializerHelper.Load<MountingProject>(SysPara.SysMountingRXParamPath, ref strErr, false);
            if (null != Mountingproject)
                model.ProjectInfor.MountingProject = Mountingproject;          

            //左点胶RX参数（默认加载RX）
            GlueProject LGlueproject = XmlSerializerHelper.Load<GlueProject>(SysPara.SysLGlueBaseParamPath, ref strErr, false);
            GlueModel LGlueModel = XmlSerializerHelper.Load<GlueModel>(SysPara.SysLGlueRXParamPath, ref strErr, false);
            model.ModelName = "RX";
            if (null != LGlueproject)
                model.ProjectInfor.MountingProject.LeftGlue = LGlueproject;
            if (null != LGlueModel)
                model.ProjectInfor.MountingProject.LeftGlue.GlueExcuteModel = LGlueModel;
            //右边点胶参数
            GlueProject RGlueproject = XmlSerializerHelper.Load<GlueProject>(SysPara.SysRGlueParamPath, ref strErr, false);
            if (null != RGlueproject)
                model.ProjectInfor.MountingProject.RightGlue = RGlueproject;
            //初始化参数
            SysInitialParame Initproject = XmlSerializerHelper.Load<SysInitialParame>(SysPara.SysInitParamPath, ref strErr, false);
            if (null != Initproject)
                model.ProjectInfor.InitProject = Initproject;
        }



        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (model.SettingPara == null)
                model.SettingPara = new ClsParam();
            TimerSpy.Tick += new EventHandler(timerSpy_Tick);
            //TimerSpy.Start();
            CreateAlarmDatagrid();
            LoadConfig();
            ClsFunction.ModuleName = "FlowRun";
            ServoOn();
            #region 初始化贴装报警
            List<Tuple<int, string, string, string, string>> MountingAlarm = new List<Tuple<int, string, string, string, string>>();
            InitDgv_AlarmTable(dgv_MountingHardAlarm, ref MountingAlarm);
            GlobalParame.MountingAlarmFrm = new ShowAlarm.WinShowAlarm(MountingAlarm);
            GlobalParame.MountingAlarmFrm.PLC = GlobalParame.PLC;
            #endregion

            #region 初始化点胶1报警
            List<Tuple<int, string, string, string, string>> Glue1Alarm = new List<Tuple<int, string, string, string, string>>();
            InitDgv_AlarmTable(dgv_Motor1HardAlarm, ref Glue1Alarm);
            GlobalParame.Glue1AlarmFrm = new ShowAlarm.WinShowAlarm(Glue1Alarm);
            GlobalParame.Glue1AlarmFrm.PLC = GlobalParame.PLC;
            #endregion

            #region 初始化点胶2报警
            List<Tuple<int, string, string, string, string>> Glue2Alarm = new List<Tuple<int, string, string, string, string>>();
            InitDgv_AlarmTable(dgv_DispenseMotor2HardAlarm, ref Glue2Alarm);
            GlobalParame.Glue2AlarmFrm = new ShowAlarm.WinShowAlarm(Glue2Alarm);
            GlobalParame.Glue2AlarmFrm.PLC = GlobalParame.PLC;
            #endregion
            if (GlobalParame.PLC.IsConnected)
            {
                SysPara.plcConnectState = GlobalParame.PLC.IsConnected;
                ThreadHeartBeat = new Thread(new ThreadStart(HeartBeat));
                ThreadHeartBeat.IsBackground = true;
                ThreadHeartBeat.Start();

                TimerRefreshUI.Tick += new EventHandler(timerRefreshUI_Tick);
                TimerRefreshUI.Start();
            }
            TimerMonitor.Tick += new EventHandler(timerRefreshMotor_Tick);
            TimerMonitor.Start();
            
            mainwin.SwitchPermission(UserManager.UserTypes.Operator);

        }

        double pos = 0;
        SmartGlue.Infrastructure.AxisStatus status;
        private void timerRefreshMotor_Tick(object sender, EventArgs e)
        {
            #region X1移动
            G4.Motion.MotionModel.Instance.GetAxisStatus(G4.Motion.EnumAxisType.X1, ref status);
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.X1, out pos);
            Global.ModulePara.X1_IsMoving = status.Moving;
            Global.ModulePara.X1 = pos;
            #endregion
            #region Y1移动
            G4.Motion.MotionModel.Instance.GetAxisStatus(G4.Motion.EnumAxisType.Y1, ref status);
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.Y1, out pos);
            Global.ModulePara.Y1_IsMoving = status.Moving;
            Global.ModulePara.Y1 = pos;
            #endregion
            #region Z1移动
            G4.Motion.MotionModel.Instance.GetAxisStatus(G4.Motion.EnumAxisType.Z1, ref status);
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.Z1, out pos);
            Global.ModulePara.Z1_IsMoving = status.Moving;
            Global.ModulePara.Z1 = pos;
            if (GlobalParame.PLC.IsConnected)
            {
                GlobalParame.PLC.Write(GetAddressFormDgv("OB_DispenseZPos", uc_MountingFlow.Dispense1FlowWriteDgvDatas), (float)pos);
            }
            #endregion
            #region X2移动
            G4.Motion.MotionModel.Instance.GetAxisStatus(G4.Motion.EnumAxisType.X2, ref status);
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.X2, out pos);
            Global.ModulePara.X2_IsMoving = status.Moving;
            Global.ModulePara.X2 = pos;
            #endregion
            #region Y2移动
            G4.Motion.MotionModel.Instance.GetAxisStatus(G4.Motion.EnumAxisType.Y2, ref status);
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.Y2, out pos);
            Global.ModulePara.Y2_IsMoving = status.Moving;
            Global.ModulePara.Y2 = pos;
            #endregion
            #region Z2移动
            G4.Motion.MotionModel.Instance.GetAxisStatus(G4.Motion.EnumAxisType.Z2, ref status);
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.Z2, out pos);
            Global.ModulePara.Z2_IsMoving = status.Moving;
            Global.ModulePara.Z2 = pos;
            if (GlobalParame.PLC.IsConnected)
            {
                GlobalParame.PLC.Write(GetAddressFormDgv("OB_DispenseZPos", uc_MountingFlow.Dispense2FlowWriteDgvDatas), (float)pos);
            }

            #endregion
            #region Y3移动
            G4.Motion.MotionModel.Instance.GetAxisStatus(G4.Motion.EnumAxisType.Y3, ref status);
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.Y3, out pos);
            Global.ModulePara.Y3_IsMoving = status.Moving;
            Global.ModulePara.Y3 = pos;
            #endregion

            #region Z3移动
            G4.Motion.MotionModel.Instance.GetAxisStatus(G4.Motion.EnumAxisType.Z3, ref status);
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.Z3, out pos);
            Global.ModulePara.Z3_IsMoving = status.Moving;
            Global.ModulePara.Z3 = pos;
            #endregion

            #region Z4移动
            G4.Motion.MotionModel.Instance.GetAxisStatus(G4.Motion.EnumAxisType.Z4, ref status);
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.Z4, out pos);
            Global.ModulePara.Z4_IsMoving = status.Moving;
            Global.ModulePara.Z4 = pos;
            if (GlobalParame.PLC.IsConnected)
            {
                GlobalParame.PLC.Write(GetAddressFormDgv("OB_MountingZPos", uc_MountingFlow.Dispense1FlowWriteDgvDatas), (float)pos);
            }
            #endregion

            #region R1移动
            G4.Motion.MotionModel.Instance.GetAxisStatus(G4.Motion.EnumAxisType.R1, ref status);
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.R1, out pos);
            Global.ModulePara.R1_IsMoving = status.Moving;
            Global.ModulePara.R1 = pos;
            #endregion

            #region R2移动
            G4.Motion.MotionModel.Instance.GetAxisStatus(G4.Motion.EnumAxisType.R2, ref status);
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.R2, out pos);
            Global.ModulePara.R2_IsMoving = status.Moving;
            Global.ModulePara.R2 = pos;
            #endregion

            #region R3移动
            G4.Motion.MotionModel.Instance.GetAxisStatus(G4.Motion.EnumAxisType.R3, ref status);
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.R3, out pos);
            Global.ModulePara.R3_IsMoving = status.Moving;
            Global.ModulePara.R3 = pos;
            #endregion
        }

        /// <summary>
        /// 伺服上电使能
        /// </summary>
        private void ServoOn()
        {
            int ret = G4.Motion.MotionModel.Instance.Initialize("S20");
            if (G4.Motion.MotionModel.Instance.IsInitialized)
            {
                Log.log.Write("S20设备所有轴都使能成功!", Color.Black);
            }
            else
            {
                Log.log.Write("设备各轴使能失败......", Color.Red);
            }
        }

        private void timerSpy_Tick(object sender, EventArgs e)
        {
            model.RemainSpace = CheckDirveFreeSpace();
            try
            {
                //判断德创有无掉线
                if (DCCKTCPClient.IsConnected)
                {
                    bDCCKHeartBeat = false;
                    DCCKTCPClient.Send("Copy");
                    Thread.Sleep(800);
                    if (!bDCCKHeartBeat)
                    {
                        disConnectTime++;
                        if (disConnectTime >= 3)
                        {
                            disConnectTime = 0;
                            Log.log.Write("德创掉线，请检测德创软件是否正常开启", Color.Red);
                            GlobalParame.MountingAlarmFrm.ShowAlarmForm(10000, "德创掉线，请检测德创软件是否正常开启", "德创掉线，请检测德创软件是否正常开启");
                            DCCKTCPClient.Dispose();
                        }
                    }
                    else
                    {
                        disConnectTime = 0;
                        if (!DCCKTCPClient.IsConnected)
                            DCCKTCPClient.Connect();
                    }
                }
                else
                {
                    DCCKTCPClient.Dispose();
                    DCCKTCPClient.Connect();
                }
            }
            catch (Exception)
            {
            }
        }

        private void timerRefreshUI_Tick(object sender, EventArgs e)
        {
            if (model.UserName == "Adminstrator")
            {
                mountingFlow.IsLockParam = false;
                leftautoglue.IsLockParam = false;
                rightautoglue.IsLockParam = false;
            }
            else 
            {
                mountingFlow.IsLockParam = true;
                leftautoglue.IsLockParam = true;
                rightautoglue.IsLockParam = true;
            }
            Global.ModulePara.PlcState = GlobalParame.PLC.IsConnected;
            Global.ModulePara.TcpState = GlobalParame.DCCKTCPClient.IsConnected;
            Global.ModulePara.ScannerState = GlobalParame.HeightLaser.IsConnected;
            switch (model.MachineStatus)
            {
                case "离线":
                    model.MachineStatusColor = new SolidColorBrush(Colors.Red);
                    break;
                case "待机":
                    model.MachineStatusColor = new SolidColorBrush(Colors.Blue);
                    break;
                case "自动":
                    model.MachineStatusColor = new SolidColorBrush(Colors.Green);
                    break;
                case "故障":
                    model.MachineStatusColor = new SolidColorBrush(Colors.DarkGoldenrod);
                    break;
                case "手动":
                    model.MachineStatusColor = new SolidColorBrush(Colors.MediumPurple);
                    break;
                default:
                    model.MachineStatusColor = new SolidColorBrush(Colors.Red);
                    break;
            }
            if (model.MachineStatus!="自动"&& model.ProjectInfor != null && null != model.ProjectInfor.MountingProject
                && null != model.ProjectInfor.MountingProject.OpenFile&& model.ProjectInfor.MountingProject.OpenFile.StartSignal)
            {
                ChangedFile(model.ProjectInfor.MountingProject.OpenFile);
            }
        }

        /// <summary>
        /// 初始化TimeSpan
        /// </summary>
        /// <param name="myTimer">定时器</param>
        /// <param name="interval">定时器间隔</param>
        private void InitTimer(ref DispatcherTimer myTimer, TimeSpan interval)
        {
            if (myTimer == null)
                myTimer = new DispatcherTimer();
            myTimer.Interval = interval;
        }



        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        private readonly MainModel model = new MainModel();

        internal static readonly string CommConfigPath = AppDomain.CurrentDomain.BaseDirectory + "Config\\CommConfig.xml";
        internal static readonly string PasswordConfigPath = AppDomain.CurrentDomain.BaseDirectory + "Config\\PasswordConfig.xml";
        string strErr = string.Empty;

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (model.SettingPara == null) return;
            model.SettingPara.Clone();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Encoding = Encoding.UTF8;
            settings.Indent = true;
            try
            {
                string folder = System.IO.Path.GetDirectoryName(CommConfigPath);
                if (!System.IO.Directory.Exists(folder))
                {
                    System.IO.Directory.CreateDirectory(folder);
                }
                using (XmlWriter writer = XmlWriter.Create(CommConfigPath, settings))
                {
                    XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                    ns.Add("", "");
                    XmlSerializer formatter = new XmlSerializer(typeof(ClsParam));
                    formatter.Serialize(writer, model.SettingPara, ns);
                    writer.Close();
                }
                var ret = Xceed.Wpf.Toolkit.MessageBox.Show($"参数保存成功!", "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Information, null);
                SaveAlarmCode();
                Log.log.WriteInfo("参数保存成功!");
            }
            catch (Exception ex)
            {
                var ret = Xceed.Wpf.Toolkit.MessageBox.Show($"参数保存失败......" + ex.Message, "提示", System.Windows.MessageBoxButton.OK, MessageBoxImage.Warning, null);
                Log.log.WriteInfo("参数保存失败......");
            }
        }

        private void Read_Click(object sender, RoutedEventArgs e)
        {
            LoadConfig();
        }

        private void LoadConfig()
        {
            FileStream fs = new FileStream(CommConfigPath, FileMode.Open, FileAccess.Read);
            XmlSerializer xs = new XmlSerializer(typeof(ClsParam));
            try
            {
                ClsFunction.SettingDataDirectory = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"config\SettingData");
                ClsParam cameraPara = (ClsParam)xs.Deserialize(fs);
                model.SettingPara = cameraPara;
                LoadAlarmCode();
            }
            catch (Exception ex)
            {
                Log.log.WriteError("加载配置参数Error" + ex.Message);
            }
        }

        #region【加载报警代码】
        private void LoadAlarmCode()
        {
            #region【点胶模组1报警代码加载】
            if (dgv_Motor1HardAlarm.LoadXmlFileFromPath(Global.Motor1HardAlarmFilePath))
                AlarmCodeToDictionary(dgv_Motor1HardAlarm.myDgv, ref Global.DicMotor1HardAlarmCode);
            #endregion

            #region【贴装模组报警代码加载】
            if (dgv_MountingHardAlarm.LoadXmlFileFromPath(Global.MountingHardAlarmFilePath))
                AlarmCodeToDictionary(dgv_MountingHardAlarm.myDgv, ref Global.DicMountingHardAlarmCode);
            #endregion

            #region【点胶模组2爆浆代码加载】
            if (dgv_DispenseMotor2HardAlarm.LoadXmlFileFromPath(Global.Motor2HardAlarmFilePath))
                AlarmCodeToDictionary(dgv_DispenseMotor2HardAlarm.myDgv, ref Global.DicMotor2HardAlarmCode);
            #endregion

        }
        #endregion

        #region【保存报警代码】
        private void SaveAlarmCode()
        {
            ///点胶模组1报警代码保存
            dgv_Motor1HardAlarm.SaveXmlFileFromPath(Global.Motor1HardAlarmFilePath);
            ///贴装报警代码保存
            dgv_MountingHardAlarm.SaveXmlFileFromPath(Global.MountingHardAlarmFilePath);
            ///点胶模组2报警代码保存
            dgv_DispenseMotor2HardAlarm.SaveXmlFileFromPath(Global.Motor2HardAlarmFilePath);
        }
        #endregion

        private void AlarmCodeToDictionary(DataGridView codeDgv, ref Dictionary<string, string> alarmCodeDic)
        {
            alarmCodeDic = new Dictionary<string, string>();
            for (int i = 0; i < codeDgv.Rows.Count; i++)
            {
                if (!alarmCodeDic.Keys.Contains(codeDgv.Rows[i].Cells[1].Value.ToString()))
                    alarmCodeDic.Add(codeDgv.Rows[i].Cells[1].Value.ToString(), codeDgv.Rows[i].Cells[2].Value.ToString());
            }
        }


        List<string> HeaderList = new List<string>();

        #region【创建报警代码表格初始化】
        private void CreateAlarmDatagrid()
        {
            InitDataGridHeader(ref dgv_Motor1HardAlarm, SysPara.AlarmDgvHeader);

            InitDataGridHeader(ref dgv_MountingHardAlarm, SysPara.AlarmDgvHeader);

            InitDataGridHeader(ref dgv_DispenseMotor2HardAlarm, SysPara.AlarmDgvHeader);
        }

        /// <summary>
        /// 初始各表头
        /// </summary>
        private void InitDataGridHeader(ref VUDatagridView myDgv, string[] dgvHeader)
        {
            myDgv.DgvHeader = dgvHeader;
            myDgv.myDgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }
        #endregion


        #region 【获取硬盘空间】      
        //☆☆☆☆☆☆☆☆☆【检查硬盘可用空间】☆☆☆☆☆☆☆☆☆    
        private string CheckDirveFreeSpace()
        {
            //判断剩余硬盘             
            double TotalSpace;
            string remainSpace = string.Empty;
            string savepath = SysPara.WorkLog;
            string savepathDriveInfo = savepath.Substring(0, savepath.IndexOf("\\")) + "\\";
            System.IO.DriveInfo[] drives = System.IO.DriveInfo.GetDrives();
            try
            {
                foreach (System.IO.DriveInfo drive in drives)
                {
                    if (drive.Name == savepathDriveInfo)
                    {
                        double freeSpace = (drive.TotalFreeSpace / (1024 * 1024 * 1024));
                        remainSpace = (drive.TotalFreeSpace / (1024 * 1024 * 1024)).ToString("F2") + "G";
                        TotalSpace = (drive.TotalSize / (1024 * 1024 * 1024));
                        string Ratio = (freeSpace / TotalSpace).ToString();
                        if (double.Parse(Ratio) < 0.3)
                        {
                            Log.log.Write(string.Format("内存不足提示：图片存储空间仅为{0}", remainSpace), System.Drawing.Color.Red);
                            ClsFunction.DeleteFile(SysPara.LogPath, 30);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Xceed.Wpf.Toolkit.MessageBox.Show("Error:检测硬盘空间 " + ex.Message);
            }
            return remainSpace;
        }
        #endregion

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;
        }

        public string GetAddressFormDgv(string name, List<CommunicateDgvData> communicateDgvDatas)
        {
            if (communicateDgvDatas != null && communicateDgvDatas.Count > 0)
            {
                for (int i = 0; i < communicateDgvDatas.Count; i++)
                {
                    if (communicateDgvDatas[i].name == name)
                        return communicateDgvDatas[i].data;
                }
                return "999";
            }
            else { return "999"; }
        }
        public bool StopAllMotor()
        {
            return G4.Motion.MotionModel.Instance.StopAllAxes() == 0 ? true : false;
        }

        /// <summary>
        /// 防止程序二次启动
        /// </summary>
        /// <param name="ProcessName">程序名称</param>
        /// <param name="Counter">程序允许打开个数</param>
        /// <returns></returns>
        private bool ProcessIsOpen(string ProcessName, int Counter)
        {
            string[] ProcessNames;
            bool openFlag = false;
            Process[] pro = Process.GetProcesses();
            ProcessNames = new string[pro.Length];
            int count = 0;
            for (int i = 0; i < pro.Length; i++)
            {
                ProcessNames[i] = pro[i].ProcessName.ToString();
                if (pro[i].ProcessName.ToString().Contains(ProcessName) && !pro[i].ProcessName.ToString().Contains(".vshost"))                    //遍历进程名称
                {
                    count++;
                }
                if (count == Counter)
                {
                    openFlag = true;
                    break;
                }
                else
                    openFlag = false;
            }
            return openFlag;
        }

        private void CheckVPProg()
        {
            //注意这个路径可能要更改
            string exefile = @"C:\Users\CTOS\Desktop\VP.lnk";
            if (!ProcessIsOpen("DCCK.VisionPlus.GStudio", 1))
            {
                if (File.Exists(exefile))
                {
                    Process process = new Process();   // params 为 string 类型的参数，多个参数以空格分隔，如果某个参数为空，可以传入””
                    ProcessStartInfo startInfo = new ProcessStartInfo(exefile);
                    process.StartInfo = startInfo;
                    process.Start();
                }
                else
                {
                    Log.log.Write("视觉软件路径不正确，打开失败", Color.Red);
                }
            }
        }

        private void SaveParam_Click(object sender, RoutedEventArgs e)
        {
            object project = null;
            object projectTXorRX = null;
            string projectName = string.Empty;
            string path = string.Empty;
            switch (Tab.SelectedIndex)
            {
                case 1:
                    project = model.ProjectInfor.MountingProject.LeftGlue;
                    projectTXorRX = model.ProjectInfor.MountingProject.LeftGlue.GlueExcuteModel;
                    projectName = "左点胶参数";
                    break;
                case 2:
                    project = model.ProjectInfor.MountingProject.RightGlue;
                    projectName = "右点胶参数";
                    path = SysPara.SysRGlueParamPath;
                    break;
                case 0:
                    project = model.ProjectInfor.MountingProject;
                    projectName = "贴装参数";
                    break;
                case 3:
                    project = model.ProjectInfor.InitProject;
                    projectName = "初始化参数";
                    path = SysPara.SysInitParamPath;
                    break;
                default:
                    return;
            }
            if (null == project) return;
            RefreashDifferentThreadUI(confirmFrm, () =>
            {
                if (confirmFrm != null)
                {
                    confirmFrm.Dispose();
                }
                confirmFrm = new frmConfirm();
                confirmFrm.fnSetTextMessageNShow($"当前保存的型号是{SysPara.SysModel}{projectName},是否保存？选【YES】保存，选【Cancel】不保存", true, false, true);
            });
            if (confirmFrm.dResult == System.Windows.Forms.DialogResult.Yes)
            {
                if (Tab.SelectedIndex == 1)
                {
                    if (SysPara.SysModel == "RX")
                    {
                        XmlSerializerHelper.Save(project, SysPara.SysLGlueBaseParamPath);
                        XmlSerializerHelper.Save(projectTXorRX, SysPara.SysLGlueRXParamPath);
                        Log.log.Write("当前保存的模板数据是RX", Color.Red);
                    }
                    else
                    {
                        XmlSerializerHelper.Save(project, SysPara.SysLGlueBaseParamPath);
                        XmlSerializerHelper.Save(projectTXorRX, SysPara.SysLGlueTXParamPath);
                        Log.log.Write("当前保存的模板数据是TX", Color.Red);
                    }
                }
                else if (Tab.SelectedIndex == 0)
                {
                    if (SysPara.SysModel == "RX")
                    {
                        XmlSerializerHelper.Save(project, SysPara.SysMountingRXParamPath);
                        Log.log.Write("当前保存的模板数据是RX", Color.Red);
                    }
                    else
                    {
                        XmlSerializerHelper.Save(project, SysPara.SysMountingTXParamPath);
                        Log.log.Write("当前保存的模板数据是TX", Color.Red);
                    }
                }
                else
                {
                    XmlSerializerHelper.Save(project, path);
                    Log.log.Write($"当前保存的模板数据是{projectName},保存路径{path}", Color.Red);
                }
            } 
        }

        private void LoadParmeFormFile_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = "(*.xml)|*.xml";
            if (openFileDialog.ShowDialog() == true)
            {
                string strTempError = string.Empty;
                switch (Tab.SelectedIndex)
                {
                    case 1:
                        model.ProjectInfor.MountingProject.LeftGlue.GlueExcuteModel = XmlSerializerHelper.Load<GlueModel>(openFileDialog.FileName, ref strTempError,false);
                        break;
                    case 2:
                        model.ProjectInfor.MountingProject.RightGlue = XmlSerializerHelper.Load<GlueProject>(openFileDialog.FileName, ref strTempError, false); 
                        break;
                    case 0:
                        model.ProjectInfor.MountingProject = XmlSerializerHelper.Load<MountingProject>(openFileDialog.FileName, ref strTempError, false); 
                        break;
                    case 3:
                        model.ProjectInfor.InitProject = XmlSerializerHelper.Load<SysInitialParame>(openFileDialog.FileName, ref strTempError, false); 
                        break;
                    default:
                        return;
                }
                if (!string.IsNullOrEmpty(strTempError))
                    LogSv.Log.log.WriteError(strTempError);
            }
        }

        private void SaveAs_Click(object sender, RoutedEventArgs e)
        {
            object project = null;
            object projectRXorTX = null;
            string projectName = "空";
            switch (Tab.SelectedIndex)
            {
                case 1:
                    project = model.ProjectInfor.MountingProject.LeftGlue;
                    projectRXorTX = model.ProjectInfor.MountingProject.LeftGlue.GlueExcuteModel;
                    projectName = "左点胶参数";
                    break;
                case 2:
                    project = model.ProjectInfor.MountingProject.RightGlue;
                    projectName = "右点胶参数";
                    break;
                case 0:
                    project = model.ProjectInfor.MountingProject;
                    projectName = "贴装参数";
                    break;
                case 3:
                    project = model.ProjectInfor.InitProject;
                    projectName = "初始化参数";
                    break;
                default:
                    return;
            }
            if (null == project) return;
            Microsoft.Win32.SaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();
            saveFileDialog.Filter = "(*.xml)|*.xml";
            if (saveFileDialog.ShowDialog() == true)
            {
                if (projectRXorTX != null)
                {
                    XmlSerializerHelper.Save(projectRXorTX, saveFileDialog.FileName);
                }
                else
                {
                    XmlSerializerHelper.Save(project, saveFileDialog.FileName);
                }
                Log.log.Write($"当前保存的模板数据是{projectName}", Color.Red);
            }
            else
            {
                return;
            }
        }

        private void RefreashDifferentThreadUI(System.Windows.Forms.Control control, Action action)
        {
            if (control.InvokeRequired)
            {
                Action refreshUI = new Action(action);
                control.Invoke(refreshUI);
            }
            else
            {
                action.Invoke();
            }
        }

        int RighttaskNo = 0;

        private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            mainwin.SwitchPermission(UserManager.UserTypes.Adminstrator);
        }


        //PLC心跳交互
        private void HeartBeat()
        {
            JTimer heartBeatTM = new JTimer();
            JTimer refreshJt = new JTimer();
            heartBeatTM.Restart();
            refreshJt.Restart();
            while (true)
            {
                Dispatcher.Invoke(() =>
                {
                    #region 空闲信号
                    if (!leftautoglue.ExcuteProject.StationStatus.IsDispGlue && !leftautoglue.ExcuteProject.StationStatus.IsCaliNeedle &&
                    !leftautoglue.ExcuteProject.StationStatus.IsWeightRun && !leftautoglue.ExcuteProject.StationStatus.IsDropGlueRun &&
                     !leftautoglue.ExcuteProject.StationStatus.IsTapeClean
                       && !mountingFlow.ExcuteProject.StationStatus.IsMounting && !mountingFlow.ExcuteProject.StationStatus.IsDischarge)
                    {
                        leftautoglue.ExcuteProject.PLC_StandbyPosition = true;
                    }
                    else
                    {
                        leftautoglue.ExcuteProject.PLC_StandbyPosition = false;
                        //Log.log.Write("左点胶非空闲状态",Color.Red);
                    }
                    //if (rightautoglue.ExcuteProject.GlueStandbyFlag)
                    //{
                    //    rightautoglue.ExcuteProject.PLC_StandbyPosition = true;
                    //}
                    //else
                    //{
                    //    rightautoglue.ExcuteProject.PLC_StandbyPosition = false;
                    //}
                    #endregion
                    #region 安全光栅
                    if (!Googo.Manul.GloMotionParame.InputList.GetValue("IB_SafetyGrating") && model.ProjectInfor.InitProject.InitialMode.EnableGrating)
                    {
                        if (leftautoglue.ExcuteProject.GlueStandbyFlag && rightautoglue.ExcuteProject.GlueStandbyFlag
                        && !mountingFlow.ExcuteProject.StationStatus.IsMounting && !mountingFlow.ExcuteProject.StationStatus.IsDischarge)
                        {
                            if (refreshJt.On(5000))
                            {
                                Log.log.Write("光栅信号被触发,当前流程未启动，不触发停止", Color.Red);
                                refreshJt.Restart();
                            }
                        }
                        else
                        {
                            InitialFlow.Stop();
                            Glue1Flow.Stop();
                            Glue2Flow.Stop();
                            mountingFlow.Stop();
                            leftautoglue.ResetChart();
                            rightautoglue.ResetChart();
                            mountingFlow.ResetChart();
                            initial.ResetChart();
                            StopAllMotor();
                            if (refreshJt.On(5000))
                            {
                                Log.log.Write("光栅信号被触发,当前流程已启动，触发停止", Color.Red);
                                refreshJt.Restart();
                            }
                        }
                        //StopY3Motor();
                    }
                    #endregion
                    #region 触发手动信号
                    if ((bool)GlobalParame.PLC.Read(GetAddressFormDgv("IB_AutoManual", uc_MountingFlow.Dispense1MoveReadDgvDatas)))
                    {
                        GlobalParame.PLC.Write(GetAddressFormDgv("OB_AutoManual", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true);
                        model.MachineStatus = "手动";
                        if (model.ProjectInfor.InitProject.InitialMode.DebugModel)
                        {
                            if (mountingFlow.IsRun || Glue1Flow.IsRun || Glue2Flow.IsRun)
                            {
                                mountingFlow.Stop();
                                Glue1Flow.Stop();
                                Glue2Flow.Stop();
                                Log.log.Write("当前为调试模式，自动运行中切换成手动模式，已暂停当前流程", Color.Black);
                            }
                        }
                        else
                        {
                            if (mountingFlow.IsRun || Glue1Flow.IsRun || Glue2Flow.IsRun)
                            {
                                Glue1Flow.Stop();
                                Glue2Flow.Stop();
                                mountingFlow.Stop();
                                StopAllMotor();
                                leftautoglue.ResetChart();
                                rightautoglue.ResetChart();
                                mountingFlow.ResetChart();
                                ModuleBase.BInitIsOK = false;
                                Log.log.Write("自动运行中切换成手动模式，已停机，需要重新初始化", Color.Red);
                            }
                        }
                    }
                    else
                    {
                        GlobalParame.PLC.Write(GetAddressFormDgv("OB_AutoManual", uc_MountingFlow.Dispense1MoveWriteDgvDatas), false);
                        model.MachineStatus = "自动";
                        if (model.UserName != "Operator")
                            mainwin.SwitchPermission(UserManager.UserTypes.Operator);
                    }
                    if ((bool)GlobalParame.PLC.Read(GetAddressFormDgv("IB_AutoManual", uc_MountingFlow.Dispense2MoveReadDgvDatas)))
                    {
                        GlobalParame.PLC.Write(GetAddressFormDgv("OB_AutoManual", uc_MountingFlow.Dispense2MoveWriteDgvDatas), true);
                        //model.MachineStatus = "手动";
                    }
                    else
                    {
                        GlobalParame.PLC.Write(GetAddressFormDgv("OB_AutoManual", uc_MountingFlow.Dispense2MoveWriteDgvDatas), false);
                        //model.MachineStatus = "自动";
                    }
                    #endregion
                    #region 急停
                    if (!Googo.Manul.GloMotionParame.InputList.GetValue("IB_EmergencyStop"))
                    {
                        InitialFlow.Stop();
                        Glue1Flow.Stop();
                        Glue2Flow.Stop();
                        mountingFlow.Stop();
                        StopAllMotor();
                        leftautoglue.ResetChart();
                        rightautoglue.ResetChart();
                        mountingFlow.ResetChart();
                        initial.ResetChart();
                        Log.log.Write("急停信号被触发", Color.Red);
                    }
                    #endregion
                    #region 轴报警
                    if (Googo.Manul.GloMotionParame.FlowStop && model.MachineStatus == "自动")
                    {
                        InitialFlow.Stop();
                        Glue1Flow.Stop();
                        Glue2Flow.Stop();
                        mountingFlow.Stop();
                        StopAllMotor();
                        leftautoglue.ResetChart();
                        rightautoglue.ResetChart();
                        mountingFlow.ResetChart();
                        initial.ResetChart();
                        Log.log.Write("轴报警，流程停止", Color.Red);
                    }
                    #endregion
                    if (heartBeatTM.On(500))
                    {
                        GlobalParame.PLC.Write(GetAddressFormDgv("OB_HeartBeat", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true);
                        GlobalParame.PLC.Write(GetAddressFormDgv("OB_Heartbeat", uc_MountingFlow.Dispense2MoveWriteDgvDatas), true);
                        //model.ProjectInfor.InitProject.InitialMode.Signal_W_HeartBeat = true;
                        heartBeatTM.Restart();

                        if ((bool)GlobalParame.PLC.Read(GetAddressFormDgv("IB_Initial", uc_MountingFlow.Dispense1MoveReadDgvDatas)) || (bool)GlobalParame.PLC.Read(GetAddressFormDgv("IB_Initial", uc_MountingFlow.Dispense2MoveReadDgvDatas)))
                        {
                            if (!InitialFlow.IsRun)
                            {
                                Googo.Manul.GloMotionParame.FlowStop = false;
                                leftautoglue.Stop();
                                rightautoglue.Stop();
                                mountingFlow.Stop();
                                leftautoglue.ResetChart();
                                rightautoglue.ResetChart();
                                mountingFlow.ResetChart();
                                initial.ResetChart();
                                //GlobalParame.PLC.Read(GetAddressFormDgv("IB_Initial", uc_MountingFlow.Dispense1MoveReadDgvDatas))
                                //initial.InitialCompeleted = false;
                                InitialFlow.RunAsync();
                            }
                        }

                        if (ModuleBase.BInitIsOK)
                        {
                            if ((bool)GlobalParame.PLC.Read(GetAddressFormDgv("IB_Start", uc_MountingFlow.Dispense1MoveReadDgvDatas)))
                            {
                                GlobalParame.Glue1AlarmFrm.CloseAlarmFrm();
                                GlobalParame.MountingAlarmFrm.CloseAlarmFrm();
                                string tempError = string.Empty;
                                //LefttaskNo = (ushort)GlobalParame.PLC.Read(GetAddressFormDgv("IB_TaskNo", uc_MountingFlow.Dispense1FlowReadDgvDatas));                                
                                if ((bool)GlobalParame.PLC.Read(GetAddressFormDgv("IB_RXModel", uc_MountingFlow.Dispense1MoveReadDgvDatas)))       //RX模式
                                {
                                    if (SysPara.SysModel != "RX")
                                    {
                                        RefreashDifferentThreadUI(confirmFrm, () =>
                                        {
                                            if (confirmFrm != null)
                                            {
                                                confirmFrm.Dispose();
                                            }
                                            confirmFrm = new frmConfirm();
                                            confirmFrm.fnSetTextMessageNShow($"启动任务的型号是RX,与当前使用的型号{SysPara.SysModel}不一致,是否切换程序？选【YES】切换，选【Cancel】取消", true, false, true);
                                        });
                                        if (confirmFrm.dResult == System.Windows.Forms.DialogResult.Yes)
                                        {
                                            SysPara.SysModel = "RX";
                                            model.ModelName = "RX";
                                            model.ProjectInfor.MountingProject =XmlSerializerHelper.Load<MountingProject>(SysPara.SysMountingRXParamPath, ref tempError,false);
                                            model.ProjectInfor.MountingProject.LeftGlue = XmlSerializerHelper.Load<GlueProject>(SysPara.SysLGlueBaseParamPath, ref tempError, false); 
                                            model.ProjectInfor.MountingProject.LeftGlue.GlueExcuteModel = XmlSerializerHelper.Load<GlueModel>(SysPara.SysLGlueRXParamPath, ref tempError, false); 
                                            model.ProjectInfor.MountingProject.RightGlue = XmlSerializerHelper.Load<GlueProject>(SysPara.SysRGlueParamPath, ref tempError, false); 
                                            Log.log.Write($"当前使用的型号{SysPara.SysModel},配置文件路径：{SysPara.SysTXParamPath}", Color.Red);
                                        }
                                        else { return; }
                                    }
                                }
                                else if ((bool)GlobalParame.PLC.Read(GetAddressFormDgv("IB_TXModel", uc_MountingFlow.Dispense1MoveReadDgvDatas)))
                                {
                                    if (SysPara.SysModel != "TX")
                                    {
                                        RefreashDifferentThreadUI(confirmFrm, () =>
                                        {
                                            if (confirmFrm != null)
                                            {
                                                confirmFrm.Dispose();
                                            }
                                            confirmFrm = new frmConfirm();
                                            confirmFrm.fnSetTextMessageNShow($"启动任务的型号是TX,与当前使用的型号{SysPara.SysModel}不一致,是否切换程序？选【YES】切换，选【Cancel】取消", true, false, true);
                                        });
                                        if (confirmFrm.dResult == System.Windows.Forms.DialogResult.Yes)
                                        {
                                            SysPara.SysModel = "TX";
                                            model.ModelName = "TX";
                                            model.ProjectInfor.MountingProject = XmlSerializerHelper.Load<MountingProject>(SysPara.SysMountingTXParamPath, ref tempError, false);
                                            model.ProjectInfor.MountingProject.LeftGlue = XmlSerializerHelper.Load<GlueProject>(SysPara.SysLGlueBaseParamPath, ref tempError, false);
                                            model.ProjectInfor.MountingProject.LeftGlue.GlueExcuteModel = XmlSerializerHelper.Load<GlueModel>(SysPara.SysLGlueTXParamPath, ref tempError, false);
                                            model.ProjectInfor.MountingProject.RightGlue = XmlSerializerHelper.Load<GlueProject>(SysPara.SysRGlueParamPath, ref tempError, false);
                                            Log.log.Write($"当前使用的型号{SysPara.SysModel},配置文件路径：{SysPara.SysTXParamPath}", Color.Red);
                                        }
                                        else { return; }
                                    }
                                }
                                else
                                {
                                    Log.log.Write("当前RX_TX模式错误", Color.Red);
                                    return;
                                }
                                GlobalParame.PLC.Write(GetAddressFormDgv("OB_" + "Start", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true);
                                if (!mountingFlow.IsRun && !Glue1Flow.IsRun)
                                {
                                    bool a = Glue1Flow.IsRun;
                                    Glue1Flow.RunAsync();
                                    mountingFlow.RunAsync();
                                    Log.log.Write("启用左工位信号", Color.Red);
                                }
                            }
                            if ((bool)GlobalParame.PLC.Read(GetAddressFormDgv("IB_Start", uc_MountingFlow.Dispense2MoveReadDgvDatas)))
                            {
                                GlobalParame.Glue2AlarmFrm.CloseAlarmFrm();
                                RighttaskNo = (ushort)GlobalParame.PLC.Read(GetAddressFormDgv("IB_TaskNo", uc_MountingFlow.Dispense2FlowReadDgvDatas));
                                GlobalParame.PLC.Write(GetAddressFormDgv("OB_" + "AutoRun", uc_MountingFlow.Dispense2MoveWriteDgvDatas), true);

                                Glue2Flow.RunAsync();
                                Log.log.Write("启用右工位信号", Color.Red);
                            }
                        }


                        if ((bool)GlobalParame.PLC.Read(GetAddressFormDgv("IB_" + "Paused", uc_MountingFlow.Dispense1MoveReadDgvDatas)))
                        {
                            Glue1Flow.Stop();
                            InitialFlow.Stop();
                            mountingFlow.Stop();
                            if (!(bool)GlobalParame.PLC.Read(GetAddressFormDgv("OB_" + "Paused", uc_MountingFlow.Dispense1MoveWriteDgvDatas)))
                            {
                                Log.log.Write("触发暂停信号", Color.Red);

                            }
                            GlobalParame.PLC.Write(GetAddressFormDgv("OB_" + "Paused", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true);
                        }
                        if ((bool)GlobalParame.PLC.Read(GetAddressFormDgv("IB_" + "Paused", uc_MountingFlow.Dispense2MoveReadDgvDatas)))
                        {
                            Glue2Flow.Stop();
                            if (!(bool)GlobalParame.PLC.Read(GetAddressFormDgv("OB_" + "Stop", uc_MountingFlow.Dispense2MoveWriteDgvDatas)))
                            {
                                Log.log.Write("触发暂停信号", Color.Red);
                            }
                            GlobalParame.PLC.Write(GetAddressFormDgv("OB_" + "Stop", uc_MountingFlow.Dispense2MoveWriteDgvDatas), true);                
                        }
                    }
                });

                Thread.Sleep(10);
            }

        }

        /// <summary>
        /// 算法客户端接受信息事件
        /// </summary>
        /// <remarks>
        /// 可以重新该事件，在里面做自己想要的事件处理
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Alg_DataReceived(object sender, SuperSimpleTcp.DataReceivedEventArgs e)
        {
            if (e.Data == null || null == e.Data.Array)
                return;
            string strRec = Encoding.UTF8.GetString(e.Data.Array);
            if (strRec.Contains("Copy"))
                bDCCKHeartBeat = true;

        }

        private void ChangedFile(OpenFile fileSignal)
        {
            if (!fileSignal.WaitStartReset()) return;
            string temFileName = fileSignal.FileName;
            if (string.IsNullOrEmpty(temFileName))
            {
                fileSignal.WaitRuningReset(false);
                LogSv.Log.log.WriteError("PLC给的文件名称为空");
                return;
            }
            string strDir = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config", temFileName);
            if (!Directory.Exists(strDir))
            {
                fileSignal.WaitRuningReset(false);
                LogSv.Log.log.WriteError($"找不到文件路径为{strDir}的配方");
                return;
            }
            string SysLGlueParamFile= System.IO.Path.Combine(strDir, "SysLGlueParam.xml");
            string SysMountingParamfile = System.IO.Path.Combine(strDir, "SysMountingParam.xml");          
            if (!File.Exists(SysLGlueParamFile) || !File.Exists(SysMountingParamfile))
            {
                fileSignal.WaitRuningReset(false);
                LogSv.Log.log.WriteError($"在文件夹{strDir}中找不到对应配方文件");
                return;
            }
            SysPara.SysModel = temFileName;
            model.ModelName = temFileName;
            string tempError = string.Empty;
            model.ProjectInfor.MountingProject = XmlSerializerHelper.Load<MountingProject>(SysMountingParamfile, ref tempError, false);
            model.ProjectInfor.MountingProject.LeftGlue = XmlSerializerHelper.Load<GlueProject>(SysPara.SysLGlueBaseParamPath, ref tempError, false);
            model.ProjectInfor.MountingProject.LeftGlue.GlueExcuteModel = XmlSerializerHelper.Load<GlueModel>(SysLGlueParamFile, ref tempError, false);
            model.ProjectInfor.MountingProject.RightGlue = XmlSerializerHelper.Load<GlueProject>(SysPara.SysRGlueParamPath, ref tempError, false);
            Log.log.Write($"当前使用的型号{SysPara.SysModel},配置文件路径：{SysPara.SysTXParamPath}", Color.Red);
            fileSignal.WaitRuningReset(string.IsNullOrEmpty(tempError));
        }
    }
}
