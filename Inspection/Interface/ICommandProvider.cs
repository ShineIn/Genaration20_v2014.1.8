﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inspection
{       
    internal class CommandProvider 
    {
        public static IDelegateCommand Create(Action execute, Func<bool> canExecute = null)
        {
            return new DelegateCommand(execute, canExecute);
        }

        public static IDelegateCommand CreateAsync(Func<Task> execute, Func<bool> canExecute = null)
        {
            return new DelegateCommand(execute, canExecute);
        }

        public static IDelegateCommand<T> Create<T>(Action<T> execute, Func<T, bool> canExecute = null)
        {
            return new DelegateCommand<T>(execute, canExecute);
        }

        public IDelegateCommand<T> CreateAsync<T>(Func<T, Task> execute, Func<T, bool> canExecute = null)
        {
            return new DelegateCommand<T>(execute, canExecute);
        }
    }
}
