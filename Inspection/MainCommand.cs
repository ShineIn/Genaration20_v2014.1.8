﻿using Googo.Manul;
using Inspection.Helper;
using ModuleData;
using ModuleData20;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;
using UserManager;
using YHSDK;

namespace Inspection
{
    public partial class MainWindow
    {
        private void AddCommand()
        {
            CommandBindings.Add(new CommandBinding(InitialCommand, Initial_Excute, Initial_CanExcute));
            CommandBindings.Add(new CommandBinding(RunCommand, Run_Excute, Run_CanExcute));
            CommandBindings.Add(new CommandBinding(StopCommand, Stop_Excute, Stop_CanExcute));
            CommandBindings.Add(new CommandBinding(LoginCommand, Login_Excute));
            CommandBindings.Add(new CommandBinding(PauseCommand, Pause_Excute, Stop_CanExcute));
            CommandBindings.Add(new CommandBinding(ResetCommand, ResetCommand_Excute, ResetCommand_CanExcute));
        }
        public static RoutedCommand InitialCommand = new RoutedCommand();
        public static RoutedCommand RunCommand = new RoutedCommand();
        public static RoutedCommand StopCommand = new RoutedCommand();
        public static RoutedCommand LoginCommand = new RoutedCommand();
        public static RoutedCommand PauseCommand = new RoutedCommand();
        public static RoutedCommand ResetCommand = new RoutedCommand();


        private void Initial_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            InitialFlow.RunAsync();
        }

        private void Initial_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = InitialFlow != null && !InitialFlow.IsRun;
        }

        private void Run_Excute(object sender, ExecutedRoutedEventArgs e)
        {            
            Glue1Flow.RunAsync();
            Glue2Flow.RunAsync();
            MountingFlow.RunAsync();
        } 

        private void Run_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = Glue1Flow != null && !Glue1Flow.IsRun && Glue2Flow != null && !Glue2Flow.IsRun && MountingFlow != null && !MountingFlow.IsRun;
        }

        private void Stop_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            if (null != InitialFlow)
                InitialFlow.Stop();
            if (null != Glue1Flow)
                Glue1Flow.Stop();
            if (null != Glue2Flow)
                Glue2Flow.Stop();
            if (null != MountingFlow)
                MountingFlow.Stop();
            GlobalParame.PLC.Write(GetAddressFormDgv("OB_" + "Paused", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true);
            GlobalParame.PLC.Write(GetAddressFormDgv("OB_" + "Paused", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true);
        }

        private void Stop_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = (Glue1Flow != null && Glue1Flow.IsRun) || (InitialFlow != null && InitialFlow.IsRun) || (Glue2Flow != null && Glue2Flow.IsRun) || (MountingFlow != null && MountingFlow.IsRun);
        }


        private void Login_Excute(object sender, ExecutedRoutedEventArgs e)
        {
           WinLogInxaml winLog = new WinLogInxaml();
            UserTypes user = new UserTypes();
            if (winLog.ShowDialog() == false)
            {
                user = AccountHelper.LogInType;
                SwitchPermission(user);
            }
        }


        public void SwitchPermission(UserTypes permision)
        {
            switch (permision)
            {
                case UserTypes.Operator:
                    SaveButtonView.IsEnabled = false;
                    mountingparam.IsEnabled = false;
                    initparam.IsEnabled = false;
                    leftautoglue.ExcuteProject.IsEnableEdit = false;
                    rightautoglue.ExcuteProject.IsEnableEdit = false;
                    mountingFlow.ExcuteProject.IsEnableEdit = false;
                    leftautoglue.ExcuteProject.IsEnableEditTabel = false;
                    rightautoglue.ExcuteProject.IsEnableEditTabel = false;
                    RunFlowView.IsEnabled = true;
                    DeviceDebugView.IsEnabled = false;
                    ParameView.IsEnabled = false;
                    MotorManagerView.IsEnabled = false;
                    model.UserName = "Operator";
                    break;
                case UserTypes.Engineer:
                    SaveButtonView.IsEnabled = true;
                    mountingparam.IsEnabled = true;
                    initparam.IsEnabled = true;
                    leftautoglue.ExcuteProject.IsEnableEdit = false;
                    rightautoglue.ExcuteProject.IsEnableEdit = false;
                    mountingFlow.ExcuteProject.IsEnableEdit = false;
                    leftautoglue.ExcuteProject.IsEnableEditTabel = true;
                    rightautoglue.ExcuteProject.IsEnableEditTabel = true;
                    RunFlowView.IsEnabled = true;
                    DeviceDebugView.IsEnabled = true;
                    ParameView.IsEnabled = false;
                    MotorManagerView.IsEnabled = false;
                    model.UserName = "Engineer";
                    break;
                case UserTypes.Adminstrator:
                    SaveButtonView.IsEnabled = true;
                    mountingparam.IsEnabled = true;
                    initparam.IsEnabled = true;
                    leftautoglue.ExcuteProject.IsEnableEdit = true;
                    rightautoglue.ExcuteProject.IsEnableEdit = true;
                    mountingFlow.ExcuteProject.IsEnableEdit = true;
                    leftautoglue.ExcuteProject.IsEnableEditTabel = true;
                    rightautoglue.ExcuteProject.IsEnableEditTabel = true;
                    RunFlowView.IsEnabled = true;
                    DeviceDebugView.IsEnabled = true;
                    ParameView.IsEnabled = true;
                    MotorManagerView.IsEnabled = true;
                    model.UserName = "Adminstrator";
                    break;
                default:
                    break;
            }

        }

        private void Pause_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            if (null != InitialFlow)
                InitialFlow.Stop();
            if (null != Glue1Flow)
                Glue1Flow.Stop();
            if (null != Glue2Flow)
                Glue2Flow.Stop();
            if (null != MountingFlow)
                MountingFlow.Stop();
        }

        private void ResetCommand_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            if (null != InitialFlow)
            { 
                InitialFlow.Stop();
            }
            if (null != Glue1Flow)
                Glue1Flow.Stop();
            if (null != Glue2Flow)
                Glue2Flow.Stop();
            if (null != MountingFlow)
                MountingFlow.Stop();
        }

        private void ResetCommand_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        
        private IModule InitialFlow = null;
        private IModule Glue1Flow = null;
        private IModule Glue2Flow = null;
        private IModule MountingFlow = null;
    }

}
