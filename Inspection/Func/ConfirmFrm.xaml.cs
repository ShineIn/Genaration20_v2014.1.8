﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.Windows.Forms;

namespace Inspection
{

    /// <summary>
    /// ConfirmFrm.xaml 的交互逻辑
    /// </summary>
    public partial class ConfirmFrm : Window
    {
        /// <summary>
        /// DialogResult.Yes es un reintentar y Abort es no reintentar
        /// </summary>
        public DialogResult dResult = System.Windows.Forms.DialogResult.None;
        public ConfirmFrm()
        {
            InitializeComponent();
        }

        public void fnSetMessageAndButtons(string sMessage, bool bRetryEnabled, bool bSkipEnabled, bool bAbortEnabled)
        {
            if (this.Dispatcher.CheckAccess())
            {
                this.Dispatcher.BeginInvoke((MethodInvoker)(() =>
                {
                    btnConfirm.Visibility = (bRetryEnabled) ? Visibility.Visible : Visibility.Hidden;
                    btnIgnore.Visibility = (bSkipEnabled) ? Visibility.Visible : Visibility.Hidden;
                    btnCancel.Visibility = (bAbortEnabled) ? Visibility.Visible : Visibility.Hidden;
                    dResult = System.Windows.Forms.DialogResult.None;
                    lbl_Tips.Content = sMessage;
                }));
            }
            else
            {
                btnConfirm.Visibility = (bRetryEnabled) ? Visibility.Visible : Visibility.Hidden;
                btnIgnore.Visibility = (bSkipEnabled) ? Visibility.Visible : Visibility.Hidden;
                btnCancel.Visibility = (bAbortEnabled) ? Visibility.Visible : Visibility.Hidden;
                dResult = System.Windows.Forms.DialogResult.None;
                lbl_Tips.Content = sMessage;
            }
        }

        public void fnSetTextMessageNShow(string strMessage, bool bRetryEnabled, bool bSkipEnabled, bool bAbortEnabled)
        {
            if (this.Dispatcher.CheckAccess())
            {
                this.Dispatcher.BeginInvoke((MethodInvoker)(() =>
                {
                    btnConfirm.Visibility = (bRetryEnabled) ? Visibility.Visible : Visibility.Hidden;
                    btnIgnore.Visibility = (bSkipEnabled) ? Visibility.Visible : Visibility.Hidden;
                    btnCancel.Visibility = (bAbortEnabled) ? Visibility.Visible : Visibility.Hidden;
                    dResult = System.Windows.Forms.DialogResult.None;
                    lbl_Tips.Content = strMessage;
                    this.ShowDialog();
                }));
            }
            else
            {
                btnConfirm.Visibility = (bRetryEnabled) ? Visibility.Visible : Visibility.Hidden;
                btnIgnore.Visibility = (bSkipEnabled) ? Visibility.Visible : Visibility.Hidden;
                btnCancel.Visibility = (bAbortEnabled) ? Visibility.Visible : Visibility.Hidden;
                dResult = System.Windows.Forms.DialogResult.None;
                lbl_Tips.Content = strMessage;
                this.ShowDialog();
            }
        }

        public void fnChangeButtonRetryMessage(string msg)
        {
            this.Dispatcher.BeginInvoke((MethodInvoker)(() =>
            {
                btnConfirm.Content = msg;
            }));
        }
        public void doHideWindow()
        {
            this.Dispatcher.Invoke((MethodInvoker)(() =>
            {
                this.Hide();
            }));
        }

        private void btnConfirm_Click(object sender, RoutedEventArgs e)
        {
            this.Dispatcher.BeginInvoke((MethodInvoker)(() =>
            {
                dResult = System.Windows.Forms.DialogResult.Yes;
                this.Hide();
            }));
        }

        private void btnIgnore_Click(object sender, RoutedEventArgs e)
        {
            this.Dispatcher.BeginInvoke((MethodInvoker)(() =>
            {
                dResult = System.Windows.Forms.DialogResult.Ignore;
                this.Hide();
            }));
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Dispatcher.BeginInvoke((MethodInvoker)(() =>
            {
                dResult = System.Windows.Forms.DialogResult.Abort;
                this.Hide();
            }));
        }
    }
}
