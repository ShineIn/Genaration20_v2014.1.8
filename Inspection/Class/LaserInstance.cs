﻿using Helper;
using Helper.Log4Net;
using Helper.Socket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Inspection
{
    public class LaserInstance
    {
        private DMTcpClient tcpClient;
        private bool _connected;
        
        public bool Connected { get => _connected; set => _connected = value; }

        private string _recevicedMsg;
        public string RecevicedMsg { get => _recevicedMsg; set => _recevicedMsg = value; }
       

        private bool _proSwitch;
        public bool ProSwitch
        {
            get
            {
                if (_recevicedMsg=="SW,01,163")
                {
                    _proSwitch = true;
                }
                else
                {
                    _proSwitch = false;
                }
                return _proSwitch;
            }
        }
        public bool Connect(string serverIP, int port)
        {
            try
            {
                if (tcpClient == null)
                {
                    tcpClient = new DMTcpClient();
                    tcpClient.ServerIp = serverIP;
                    tcpClient.ServerPort = port;
                    tcpClient.ReConnectionTime = 2000;
                    tcpClient.OnReceviceByte += TcpClient_OnReceviceByte;
                    tcpClient.OnStateInfo += TcpClient_OnStateInfo;
                    tcpClient.OnErrorMsg += TcpClient_OnErrorMsg;
                }
                else if (tcpClient.Tcpclient.Connected == true)
                {
                    return true;
                }
                tcpClient.StartConnection();
                return true;
            }
            catch { }
            return false;
        }
        public bool Trigger()
        {
            if (tcpClient != null && tcpClient.Tcpclient.Connected)
            {
                string triggerCommand = "M0" + "\r\n";
                tcpClient.SendCommand(Encoding.ASCII.GetBytes(triggerCommand));
                return true;
            }
            else { return false; }
        }

        public void SwitchProgram(int proNumber)
        {
            if (tcpClient != null && tcpClient.Tcpclient.Connected)
            {
                string switchCommand = "SW,01,163,+00000000" + proNumber.ToString() + "\r\n";
                tcpClient.SendCommand(Encoding.ASCII.GetBytes(switchCommand));
            }
        }
        private void TcpClient_OnErrorMsg(string msg)
        {

        }
        private void TcpClient_OnStateInfo(string msg, SocketState state)
        {
            if (state == SocketState.Disconnect)
            {
                _connected = false;
            }
            else if (state == SocketState.Connected)
            {
                _connected = true;
            }
        }

        public void DisConnect()
        {
            if (tcpClient != null)
            {
                tcpClient.OnReceviceByte -= TcpClient_OnReceviceByte;
                tcpClient.OnStateInfo -= TcpClient_OnStateInfo;
                tcpClient.OnErrorMsg -= TcpClient_OnErrorMsg;
                tcpClient.StopConnection();
                tcpClient.Dispose();
                tcpClient = null;
                _connected = false;
            }
        }
        private void TcpClient_OnReceviceByte(byte[] date)
        {
            _recevicedMsg = DBCTool.ToDBC(System.Text.Encoding.Default.GetString(date));
        }
    }
}
