﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Microsoft.VisualBasic;
using MySql.Data;
using MySql.Data.MySqlClient;
using MySqlLib;
using LogSv;

namespace Inspection
{
   public class SqlOperator
    {

        public static object SqlObj = new object();
        //数据库信息       
        public  string TabName = "";
        public static List<string> ColVal = new List<string>();

        //=======  增加索引值 【Add on 20200225】
        public static int rowIndex = 0;                                                          //数据库表格中存储数据的行数


        private MySqlConnection _conn;
        public MySqlConnection conn
        {
            get { return _conn; }
            set { _conn = value; }
        }

        public MySqlOperator mySql;
        public MySqlOperator MySql
        {
            get { return mySql; }
            set { mySql = value; }
        }
        /// <summary>
        /// 数据库连接模式
        /// </summary>
        private MySqlOperator.LinkMode _linkMode;
        public SqlOperator(MySqlLib.MySqlOperator.LinkMode LinkMode)
        {
            _linkMode = LinkMode;         
        }

        /// <summary>
        /// 服务器名称
        /// </summary>
        private string serverName;
        public string ServerName
        {
            get { return serverName; }
            set { serverName = value; }
        }

        /// <summary>
        /// 端口号
        /// </summary>
        private int port;
        public int Port
        {
            get { return port; }
            set { port = value; }
        }

        /// <summary>
        /// 用户名
        /// </summary>
        private string userID;
        public string UserID
        {
            get { return userID; }
            set { userID = value; }
        }

        /// <summary>
        /// 登陆密码
        /// </summary>
        private string password;
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        /// <summary>
        /// 数据源
        /// </summary>
        private string dataBase;
        public string DataBase
        {
            get { return dataBase; }
            set { dataBase = value; }
        }


        /// <summary>
        /// 数据库参数设定
        /// </summary>
        public void InitMySql()
        {
            MySql.Server = serverName;
            MySql.Port = port;
            MySql.User_ID = userID;
            MySql.Pwd = password;
            MySql.DataBase = dataBase;
        }

        /// <summary>
        /// 数据库连接
        /// </summary>
        /// <returns></returns>
        public  MySqlConnection ConnectionSQL()
        {
            try
            {
                MySqlConnection conn = MySql.SQL_Connection();

                if (conn != null)
                {
                    Log.log.Write("数据库连接成功！", System.Drawing.Color.Green);
                    return conn;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                Log.log.Write("数据库连接Error" + ex.Message, System.Drawing.Color.Red);
                return null;
            }
        }

        /// <summary>
        /// 带逗号的字符串转换为List类型
        /// </summary>
        /// <param name="strColVal"></param>
        /// <returns></returns>
        public  bool ParaseColVal(string strColVal)
        {
            ColVal.Clear();
            try
            {
                string[] MidVal = strColVal.Split(',');
                foreach (string val in MidVal)
                {
                    ColVal.Add(val);
                }
                return true;
            }
            catch (Exception ex)
            {
                Log.log.Write("字符串转换至listError" + ex.Message, System.Drawing.Color.Red);
                return false;
            }
        }

        /// <summary>
        //表格是否存在
        /// </summary>
        /// <param name="TabName">表格名称</param>
        /// <returns>存在则返回true，否则返回false</returns>
        public  bool TabExist(string TabName)
        {
            DataTable dt = new DataTable();
            dt = MySql.SQL_IterateTables(conn);
            bool Ret = false;
            List<string> TabList = new List<string>();
            try
            {
                if (dt.Columns.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TabList.Add(dt.Rows[i].ItemArray[0].ToString());
                    }
                    if (TabList.Contains(TabName.ToLower()))
                        Ret = true;
                    else
                        Ret = false;
                }
            }
            catch (Exception ex)
            {
               Log.log.Write("Err :表格名查询错误！" + ex.Message, System.Drawing.Color.Red);
            }
            return Ret;
        }

        /// <summary>
        /// 例举database中的表格
        /// </summary>
        /// <param name="TabList">表格列表</param>
        /// <returns></returns>
        public bool IterateTables(ref List<string> TabList)
        {
            TabList = new List<string>();
            if(null== conn) return false;
            DataTable dt = new DataTable();
            dt = MySql.SQL_IterateTables(conn);
            bool Ret = false;           
            try
            {
                if (dt.Columns.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        TabList.Add(dt.Rows[i].ItemArray[0].ToString());
                    }                  
                }
                Ret= true;
            }
            catch (Exception ex)
            {
                Log.log.Write("Err :表格名查询错误！" + ex.Message, System.Drawing.Color.Red);
                Ret = false;
            }
            return Ret;
        }

        public bool DeleteSelectData(string TabName, string condition)
        {
            try
            {
                lock (SqlObj)
                {
                    bool Ret = MySql.SQL_Delete_Single_Row(conn, TabName, condition);
                    if (Ret)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                Log.log.Write("读取单行数据Error：" + ex.Message, System.Drawing.Color.Red);
                return false;
            }
        }

        /// <summary>
        /// 读取数据库中一行数据
        /// </summary>
        /// <param name="TabName">表格名</param>
        /// <param name="condition">读取条件</param>     
        /// <param name="dt">读取到返回的DataTable</param>
        /// <returns></returns>
        public  bool ReadData(string TabName, string condition, ref DataTable dt)
        {
            try
            {               
                lock (SqlObj)
                {                    
                    bool Ret = MySql.SQL_Read_Single_Data(conn, TabName, condition, out dt);
                    if (Ret)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                Log.log.Write("读取单行数据Error：" + ex.Message, System.Drawing.Color.Red);
                return false;
            }
        }

        /// <summary>
        /// 批量读取数据库中数据
        /// </summary>
        /// <param name="TabName">表格名</param>
        /// <param name="dt">读取到返回的DataTable</param>
        /// <returns></returns>
        public  bool BatchReadData(string TabName, ref DataTable dt)
        {
            try
            {               
                lock (SqlObj)
                {
                    bool Ret = MySql.SQL_Batch_Read_Data(conn, TabName, out dt);
                    if (Ret)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                Log.log.Write("批量读取数据失败Error:" + ex.Message, System.Drawing.Color.Red);
                return false;
            }
        }

        /// <summary>
        /// 更新表格单列
        /// </summary>
        /// <param name="TabName">表格名</param>
        /// <param name="Expression">更新的内容</param>
        /// <param name="Condition">更新的条件</param>
        /// <returns>更新成功返回true,更新失败返回false</returns>
        public  bool UpDateData(string TabName, string Expression, string Condition)
        {
            try
            {             
                lock (SqlObj)
                {
                    bool Ret = MySql.SQL_Update_Single_Column(conn, TabName, Expression, Condition);
                    if (Ret)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                Log.log.Write("更新表格Error" + ex.Message, System.Drawing.Color.Red);
                return false;
            }
        }

        /// <summary>
        /// 向指定列写入数据
        /// </summary>
        /// <param name="TabName">表格名</param>
        /// <param name="ColName">列名</param>
        /// <param name="ColVal">列值</param>
        /// <returns>更新成功返回true,更新失败返回false</returns>
        public  bool WriteData(string TabName, List<string> ColName, List<string> ColVal)
        {
            bool Ret = false;
            try
            {               
                lock (SqlObj)
                {
                    Ret = MySql.SQL_Write_Columns(conn, TabName, ColName, ColVal);
                    if (Ret)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                Log.log.Write("向表格中写入数据Error:" + ex.Message, System.Drawing.Color.Red);
                return false;
            }
        }

        /// <summary>
        /// 向指定列写入数据
        /// </summary>
        /// <param name="TabName">表格名</param>
        /// <param name="ColName">列名</param>
        /// <param name="ColVal">列值</param>
        /// <returns>更新成功返回true,更新失败返回false</returns>
        public bool WriteData(string TabName, string[] ColName, string[] ColVal)
        {
            bool Ret = false;
            List<string> colName = ColName.ToList();
            List<string> colVal = ColVal.ToList();
            try
            {               
                lock (SqlObj)
                {
                    Ret = MySql.SQL_Write_Columns(conn, TabName, colName, colVal);
                    if (Ret)
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                Log.log.Write("向表格中写入数据Error:" + ex.Message, System.Drawing.Color.Red);
                return false;
            }
        }

        /// <summary>
        /// 初始化数据库表头信息
        /// </summary>
        /// <param name="strHead">表头信息</param>
        /// <returns></returns>
        private List<string> InitDatabases(string strHead,ref List<string> headerList)
        {
            List<string>HeaderName = new List<string>();
            headerList.Clear();
            try
            {
                if (strHead == null)
                    return null;
                string[] Header = strHead.Split(',');
                List<string> VarType = new List<string>();
                foreach (string strName in Header)
                {
                    HeaderName.Add(strName);
                    headerList.Add(strName);
                }
                HeaderName.Add("UpLoadTime");
                return HeaderName;
            }
            catch (Exception ex)
            {
                Log.log.Write("初始化表头Error " + ex.Message, System.Drawing.Color.Red);
                return null;
            }          
        }

        private List<string> HeaderNameList = new List<string>();
        /// <summary>
        /// 创建表格
        /// </summary>
        /// <param name="TabName">表格名称</param>
        /// <returns>创建成功返回true,否则返回false</returns>
        public  bool CreateTab(string TabName,string headNames)
        {
            List<string> HeaderName = InitDatabases(headNames,ref HeaderNameList);
            bool Ret = false;
            List<string> ColType = new List<string>();
            if (HeaderName != null)
            {                      
                try
                {
                    for (int i = 0; i < HeaderName.Count - 1; i++)
                    {
                        ColType.Add("varchar(50)");
                    }
                    ColType.Add("TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");
                    if (string.IsNullOrEmpty(TabName))
                    {
                        Interaction.MsgBox("表格名为空！");
                    }
                    else
                    {
                        Ret = MySql.SQL_CreateTable(conn, TabName, HeaderName, ColType);
                        return Ret;
                    }
                }
                catch (Exception ex)
                {
                    Log.log.Write("Err;表格创建异常" + ex.Message, System.Drawing.Color.Red);
                    Ret = false;
                }
            }
            else
            {
              Ret = false;
            }
            return Ret;
        }

        /// <summary>
        /// 创建表格
        /// </summary>
        /// <param name="headerNames">表头名称</param>
        /// <param name="TabName">表格名</param>
        /// <returns></returns>
        public  bool CreateTableByDate(string headerNames, string TabName)
        {
            try
            {                            
                if (!TabExist(TabName))
                {
                    if (CreateTab(TabName, headerNames))
                    {
                        Log.log.Write(string.Format("表格{0}创建成功！", TabName), System.Drawing.Color.Black);
                        return true;
                    }
                    else
                    {
                        Log.log.Write(string.Format("表格{0}创建失败.....！", TabName), System.Drawing.Color.Red);
                        return false;
                    }
                }
                else
                {
                    Log.log.Write(string.Format("表格{0}已经存在......", TabName), System.Drawing.Color.Black);
                    return true;
                }                
            }
            catch (Exception ex)
            {
               Log.log.Write("表格创建Error:"+ex.Message,System.Drawing.Color.Red);
                return false;
            }
        }

        /// <summary>
        /// 读取数据库，按时间先后进行排列显示
        /// </summary>
        /// <param name="TabName">表格名称</param>
        /// <param name="dt">读取到返回的表格</param>
        /// <returns>读取成功返回true,读取失败返回false</returns>
        public  bool ReadOrderByDate(string TabName,List<string> HeaderName, ref DataTable dt)
        {
            try
            {                
                bool Ret = MySql.SQL_Order_Columns(conn, TabName, HeaderName, "UpLoadTime", out dt);
                if (Ret)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                Log.log.Write("读取数据库数据按时间先后显示Error:" + ex.Message, System.Drawing.Color.Red);
                return false;
            }
        }

        /// <summary>
        /// 将datagridView中的数据导出到CSV中
        /// </summary>
        /// <param name="dgvData">DataGridView中控件</param>
        /// <returns>导出成功返回true，导出失败返回false</returns>
        public bool SQL_ExportToCSV(System.Windows.Forms.DataGridView dgvData)
        {
            if (MySql.SQL_ExportToCSV(dgvData))
                return true;
            else
                return false;
        }

        object obj = new object();

        /// <summary>
        /// 读取数据库中单列表格的值
        /// </summary>
        /// <param name="TabName">表格名称</param>
        /// <param name="HeaderName">表头</param>
        /// <param name="dt">读取到的信息</param>
        /// <param name="litRate">返回读取到的</param>
        /// <returns></returns>
        public bool SQL_ReadSingleCol(string TabName,string HeaderName,ref DataTable dt, ref List<string> litRate)
        {
            lock (obj)
            {              
                bool Ret = MySql.SQL_Read_Column_Val(conn, TabName, HeaderName, out dt);
                litRate = dt.AsEnumerable().Select(d => d.Field<string>(HeaderName)).ToList();
                return Ret;
            }
        }

        /// <summary>
        /// 更新多列数据
        /// </summary>
        /// <param name="tabName"></param>
        /// <param name="KeyVals"></param>
        /// <param name="ColVal"></param>
        /// <param name="Condition"></param>
        /// <returns></returns>
        public bool SQL_Upate_Multi_Columns(string tabName, List<string> KeyVals, List<string> ColVal, string Condition)
        {
            lock (obj)
            {              
                if (mySql.SQL_Update_Muti_Columns_Date(conn, tabName, KeyVals, ColVal, Condition))
                    return true;
                else
                    return false;
            }
        }
    }
}
