﻿using Infrastructure;
using Inspection.Helper;
using ModuleData;
using ModuleData20;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inspection
{
    [Serializable]
    public  class ProjectInfor: PropertyChangedNotify
    {
        public MountingProject MountingProject
        {
            get { return mountingProject; }
            set { mountingProject = value; OnPropertyChanged(); }
        }
        private MountingProject mountingProject = new MountingProject();


        public SysInitialParame InitProject
        {
            get { return initProject; }
            set { initProject = value; OnPropertyChanged(); }
        }
        private SysInitialParame initProject = new SysInitialParame();



    }
}
