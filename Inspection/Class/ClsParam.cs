﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Inspection
{
    public class ClsParam : ICloneable
    {
        //连接相机个数
        public int CameraCount { get; set; }

        //扫码枪串口号
        public string ScannerSP { get; set; }

        //扫码枪波特率
        public int BaudRate { get; set; }

        //扫码枪数据位
        public int DataBit { get; set; }

        //扫码停止位
        public string StopBit { get; set; }

        //扫码枪校验位
        public string Parity { get; set; }

        //扫码枪是否使能
        public bool ScannerIsEnable { get; set; }
    
        //数据库连接模式
        public string LinkMode { get; set; }

        //端口号
        public int PortNo { get; set; }

        //用户名
        public string  UserName { get; set; }

        //密码
        public string Password { get; set; }

        //数据库名称
        public string SqlName { get; set; }

        //数据源名称
        public string DataSource { get; set; }

        //数据库是否开启
        public bool IsSqlOpen { get; set; }


        //刷新时间
        public int RefreshTime { get; set; }

        //数据库表头文件路径
        public string DBHeaderFilePath { get; set; }

        //服务器IP地址
        public string ServerIP { get; set; }

        //服务器PortNo
        public int ServerPort { get; set; }

        //TCP通讯是否启用
        public bool IsServerEnable { get; set; }


        object ICloneable.Clone()
        {
            throw new NotImplementedException();
        }

        public ClsParam Clone()
        {
            return new ClsParam
            {
                CameraCount = CameraCount ,
                ScannerSP = ScannerSP ,
                BaudRate = BaudRate ,
                DataBit = DataBit ,
                StopBit = StopBit,
                Parity = Parity ,       
                ScannerIsEnable = ScannerIsEnable,
                LinkMode = LinkMode ,
                PortNo = PortNo ,
                UserName = UserName ,
                Password = Password ,
                SqlName = SqlName ,
                DataSource = DataSource ,
                IsSqlOpen = IsSqlOpen ,
                RefreshTime = RefreshTime ,
                DBHeaderFilePath = DBHeaderFilePath ,
                ServerIP = ServerIP ,
                ServerPort = ServerPort ,
                IsServerEnable = IsServerEnable ,
            };
        }
    }
}
