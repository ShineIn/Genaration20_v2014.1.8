﻿using Prism.Commands;
using SmartGlue.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Inspection.Class
{
    public class WeightometerModel : ModelBindableBase
    {
        readonly XParamScope XParam;
        SerialPort comm = new SerialPort();
        public WeightometerModel()
        {
            XParam = AppSetting.Scope("External.Weightometer");
            CutPeelCommand = new DelegateCommand(OnCutPeelCommand);
            ZeroClearingCommand = new DelegateCommand(OnZeroClearingCommand);
        }

        ~WeightometerModel()
        {
            mIsRunning = false;
        }
        public string  PortName="Com1";
        public int BaudRate = 38400;
        public bool IsEnabled = true;
        public async void Initialize()
        {
            await Task.Run(()=> {
                Init();
            });
        }

        /// <summary>
        /// 采集实时稳定值，超时返回double.NaN
        /// </summary>
        /// <param name="millisecondsTimeout"></param>
        /// <returns></returns>
        public async Task<double> FetchValue(int millisecondsTimeout)
        {
            double val = double.NaN;
            DateTime now = DateTime.Now;
            if (await Task.Run(() => SpinWait.SpinUntil(() => IsStable && _UpdateTime > now, millisecondsTimeout)))
            {
                val = _RealTimeValue;
            }
            return val;
        }

        protected override void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            base.OnPropertyChanged(args);
            switch(args.PropertyName)
            {
                case "IsEnabled":
                case "BaudRate":
                case "PortName":
                    Initialize();
                    break;
            }
        }

         DelegateCommand CutPeelCommand { get;  set; }
        private void OnCutPeelCommand()
        {
            if (!comm.IsOpen)
                return;

            lock (comm)
            {
                comm.Write(new byte[] { 0x02, 0x00, 0x72, 0x55, 0xAA, 0x72, 0x72, 0x0A }, 0, 8);
            }
        }

         DelegateCommand ZeroClearingCommand { get;  set; }
        private void OnZeroClearingCommand()
        {
            if (!comm.IsOpen)
                return;

            lock (comm)
            {
                comm.Write(new byte[] { 0x02, 0x00, 0x70, 0x70, 0x70, 0x70, 0x70, 0x0A }, 0, 8);
            }
        }

        #region private
        private void Init()
        {
            lock (comm)
            {
                if (comm.IsOpen)
                {
                    mIsRunning = false;
                    comm.DataReceived -= Comm_DataReceived;
                    comm.DiscardInBuffer();
                    comm.DiscardOutBuffer();
                    comm.Close();
                }
                    
                if(IsEnabled)
                {
                    try
                     {
                        comm.ReadTimeout = 3000;
                        comm.PortName = PortName;
                        comm.BaudRate = BaudRate;
                        comm.Parity = Parity.Even;
                        comm.Open();
                        comm.DataReceived += Comm_DataReceived;
                        comm.ReceivedBytesThreshold = 8;
                        start();
                    }
                    catch (Exception)
                    {
                        mIsRunning = false;
                        Log(Tag, LogLevel.Error, TR("D_M003000"));
                    }
                }
            }
        }

        bool mIsRunning = false;
        private void start()
        {
            mIsRunning = true;
            var th = new Thread(() => {
                while (mIsRunning)
                {
                    lock(comm)
                    {
                        if (!comm.IsOpen)
                            break;

                        comm.Write(new byte[] { 0x02, 0x00, 0x75, 0x00, 0x01, 0x75, 0x75, 0x0A }, 0, 8);
                    }
                    Thread.Sleep(500);
                }
            });
            th.IsBackground = true;
            th.Start();
        }
        public void stop()
        { 
            mIsRunning = false;
            Thread.Sleep(500);
            if (comm.IsOpen)
            {
               
                comm.DataReceived -= Comm_DataReceived;
                comm.DiscardInBuffer();
                comm.DiscardOutBuffer();
                comm.Close();
            }
        }
        private void Comm_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            lock (comm)
            {
                try
                {
                    var readBuffer = new byte[8];
                    var n = comm.Read(readBuffer, 0, 8);
                    if (n == 8)
                    {
                        if (readBuffer[0] == 0xAA)
                        {
                            var data = readBuffer.Skip(2).Take(4).ToArray();
                            int value = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
                            _RealTimeValue = value / 100f;
                            _UpdateTime = DateTime.Now;
                            _QualityLeavel = (readBuffer[6] & 1) > 0 ? 1 : 0;
                            RaisePropertyChanged("IsConnected");
                            RaisePropertyChanged("RealValue");
                            RaisePropertyChanged("IsStable");
                        }
                    }
                    comm.ReadExisting();
                }
                catch (Exception)
                {

                }
            }
        }

        private double _RealTimeValue;
        private DateTime _UpdateTime;
        private int _QualityLeavel;
        #endregion

        #region 状态
        /// <summary>
        /// 连接状态
        /// </summary>
        public bool IsConnected
        {
            get { return _UpdateTime > DateTime.Now.AddSeconds(-5); }
        }
        /// <summary>
        /// 实时重量
        /// </summary>
        public double RealValue
        {
            get
            {
                return _UpdateTime > DateTime.Now.AddSeconds(-3) ? _RealTimeValue : 0;
            }
        }
        /// <summary>
        ///  实时值是否稳定
        /// </summary>
        public bool IsStable
        {
            get { return _QualityLeavel == 1; }
        }

        #endregion

        #region 参数
        //public bool IsEnabled
        //{
        //    get { return XParam.Value<bool>("IsEnabled"); }
        //    set { XParam.Value("IsEnabled", value); RaisePropertyChanged("IsEnabled"); }
        //}
        //public int BaudRate
        //{
        //    get { return XParam.Default("BaudRate", 9600).Value<int>("BaudRate"); }
        //    set { XParam.Value("BaudRate", value); RaisePropertyChanged("BaudRate"); }
        //}
        //public string PortName
        //{
        //    get { return XParam.Default("PortName", "COM1").Value("PortName"); }
        //    set { XParam.Value("PortName", value); RaisePropertyChanged("PortName"); }
        //}

        #endregion
    }
}
