﻿using LogSv;
using SuperSimpleTcp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inspection.Class
{
    internal class KMeasureHeight
    {
        public SimpleTcpClient tcpClient;

        public string  reciceMsg;
        public KMeasureHeight(string ip, int port, bool isEnable)
        {
            InitTcpClient(ip, port, isEnable);        
        }

        private bool InitTcpClient(string ip, int port, bool isEnable)
        {
            if (!isEnable) return false;
            if (tcpClient == null)
                tcpClient = new SimpleTcpClient(ip,  port);
            tcpClient.Events.DataReceived += TcpClient_DataReceived;
            if (!ClsFunction.IsIpAdress(ip)) return false;
            try
            {
                tcpClient.Connect();
            }
            catch (Exception ex)
            { }
            return tcpClient.IsConnected;
        }

        public bool ConnectTcpClient(string ip, int port)
        {
            if (!ClsFunction.IsIpAdress(ip)) return false;
            if (tcpClient == null)
            {
                tcpClient = new SimpleTcpClient(ip, port);
                tcpClient.Events.DataReceived += TcpClient_DataReceived;
            }
            try
            {
                tcpClient.Connect();

            }
            catch (Exception ex)
            { }
            return tcpClient.IsConnected;
        }


        public void DisConnect()
        {
            if (tcpClient != null)
            {
                tcpClient.Disconnect();
                tcpClient.Dispose();
            }
        }

        private void TcpClient_DataReceived(object sender, DataReceivedEventArgs e)
        {
            //Log.log.Write($"接收到来自服务器的内容为:{e.MessageString}",System.Drawing.Color.Black);
        }

        private void TcpClient_DelimiterDataReceived(object sender, Message e)
        {
            string recievedMsg = e.MessageString.Replace("\n", "");
            reciceMsg = recievedMsg;
            Log.log.Write($"接收到来自测高返回内容为:{recievedMsg}", System.Drawing.Color.Black);
        }

        public void SendCommend(string commend)
        {
            tcpClient.Send(commend);
        }
    }
}
