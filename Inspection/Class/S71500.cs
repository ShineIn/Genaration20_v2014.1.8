﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using S7.Net;
using LogSv;
using System.Drawing;
using S7.Net.Types;
using System.Text.RegularExpressions;

namespace Inspection.Class
{
    public class S71500
    {
        public Plc Plc { get;private set; }

        public string Name { get; set; }

        private string Ip = string.Empty;
        private CpuType CpuType = CpuType.S71500;
        private short Rack = 0;
        private short Slot = 0;

        public S71500(CpuType cpuType, string ip, short rack = 0, short slot = 0)
        {
            CpuType = cpuType;
            if (string.IsNullOrEmpty(ip)) throw new ArgumentNullException(nameof(ip));
            Regex regex = new Regex(@"^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$");
            var match = regex.Match(ip);
            if (!match.Success)
            {
                throw new Exception($"CommunicationManager: Address ({ip}) is not vaild ip address");
            }
            Ip = ip;
            Rack = rack;
            Slot = slot;
        }

        public S71500(Plc tempPlc)
        {
            System.Diagnostics.Debug.Assert(null != tempPlc);
            Plc = tempPlc;
        }


        //连接PLC
        public bool ConnectPLC()
        {
            if (Plc == null) { Plc = new Plc(CpuType, Ip, Rack, Slot); }
            try
            {
                Plc.Open();
            }
            catch { }
            return Plc.IsConnected;
        }


        //断开连接
        public bool DisConnect()
        {
            if (Plc == null) { return true; }
            Plc.Close();
            return !Plc.IsConnected;
        }


        /// <summary>
        /// 读操作
        /// </summary>
        /// <param name="address">地址</param>
        /// <returns></returns>
        public object ReadBool(string address)
        {
            if (Plc == null)
            {
                return null;
            }
            var val = Plc.Read(address);
            return (bool)val;
        }


        /// <summary>
        /// 读操作
        /// </summary>
        /// <param name="address">地址</param>
        /// <returns></returns>
        public object Read(string address, DataType tpye)
        {
            if (Plc == null)
            {
                return null;
            }
            var val = Plc.Read(address);
            switch (tpye)
            {
                case DataType.BOOL:
                    return (bool)val;
                case DataType.BYTE:
                    return (byte)val;
                case DataType.INT:
                    return ((ushort)val).ConvertToShort();
                case DataType.WORD:
                    return ((ushort)val).ConvertToShort();
                case DataType.DWORD:
                    return ((uint)val).ConvertToInt();
                case DataType.REAL:
                    return ((uint)val).ConvertToFloat();
                default:
                    return val;
            }
        }


        /// <summary>
        /// 写操作
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="val">变量值</param>
        public bool Write(string address, object val)
        {
            if (Plc == null) { return false; }
            Plc.Write(address, val);
            return true;
            //if (Plc.Read(address) != val) return false;             //写完后读值比较，相同则返回写入成功
            //else return true;
        }
    }
    static class S7Extension
    {
        /// <summary>
        /// 读操作
        /// </summary>
        /// <param name="address">地址</param>
        /// <returns></returns>
        internal static bool ReadValue<T>(this Plc module, string address, out T value)
        {
            Type tartype = typeof(T);
            value = default(T);
            if (module == null)
            {
                return false;
            }
            object val = module.Read(address);
            if (tartype == typeof(bool) || tartype == typeof(byte) || tartype == typeof(uint))
            {
                value = (T)val;
            }
            else if (tartype == typeof(string))
            {
                val = val.ValToBinString();
                value = (T)val;
            }
            else if (tartype == typeof(int))
            {
                val = val.ToString().BinStringToInt32();
                value = (T)val;
            }
            else if (tartype == typeof(float))
            {
                val = ((uint)val).ConvertToFloat();
                value = (T)val;
            }
            else if (tartype == typeof(short))
            {
                val = ((ushort)val).ConvertToShort();
                value = (T)val;
            }
            return true;
        }

        /// <summary>
        /// 读操作
        /// </summary>
        /// <param name="address">地址</param>
        /// <returns></returns>
        internal static bool ReadValue(this Plc module, string address, Type tartype, out dynamic value)
        {
            value = null;
            if (module == null)
            {
                return false;
            }
            object val = module.Read(address);
            if (tartype == typeof(bool) || tartype == typeof(byte) || tartype == typeof(uint))
            {
                value = val;
            }
            else if (tartype == typeof(string))
            {
                val = val.ValToBinString();
                value = val;
            }
            else if (tartype == typeof(int))
            {
                val = val.ToString().BinStringToInt32();
                value = val;
            }
            else if (tartype == typeof(float))
            {
                val = ((uint)val).ConvertToFloat();
                value = val;
            }
            else if (tartype == typeof(short))
            {
                val = ((ushort)val).ConvertToShort();
                value = val;
            }
            return true;
        }

        /// <summary>
        /// 写操作
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="val">变量值</param>
        internal static bool WriteValue(this Plc module, string address, object val)
        {
            if (!module.IsConnected) { return false; }
            module.Write(address, val);
            if (module.Read(address) != val) return false;             //写完后读值比较，相同则返回写入成功
            else return true;
        }

    }

    public enum DataType
    {
        BOOL,
        BYTE,
        INT,
        WORD,
        DWORD,
        REAL,
        STRING,
    }
}
