﻿using Inspection.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Inspection
{
    internal class ClsStateInfo : PropertyChangedNotify
    {

        /// <summary>
        /// PLC连接状态
        /// </summary>
        private bool plcState;
        public bool PlcState
        {
            get => plcState;
            set
            {
                plcState = value;
                OnPropertyChanged(nameof(plcState));
            }
        }

        /// <summary>
        /// 扫码枪连接状态
        /// </summary>
        private bool scannerState;
        public bool ScannerState
        {
            get => scannerState;
            set
            {
                scannerState = value;
                OnPropertyChanged(nameof(scannerState));
            }
        }

        /// <summary>
        /// 打印机连接状态
        /// </summary>
        private bool printerState;
        public bool PrinterState
        {
            get => printerState;
            set
            {
                printerState = value;
                OnPropertyChanged(nameof(printerState));
            }
        }

        /// <summary>
        /// 与服务器连接状态
        /// </summary>
        private bool tcpState;
        public bool TcpState
        {
            get => tcpState;
            set
            {
                tcpState = value;
                OnPropertyChanged(nameof(TcpState));
            }
        }

        /// <summary>
        /// 当前生产产品型号
        /// </summary>
        private string productMode;
        public string ProductMode
        {
            get => productMode;
            set
            {
                productMode = value;
                OnPropertyChanged(nameof(productMode));
            }
        }



        /// <summary>
        /// Tcp通讯时的IP地址
        /// </summary>
        private string tcpIP;
        public string TcpIP
        {
            get => tcpIP;
            set
            {
                tcpIP = value;
                OnPropertyChanged(nameof(TcpIP));
            }
        }

        /// <summary>
        /// Tcp通讯时的端口号
        /// </summary>
        private int portNo;
        public int PortNo
        {
            get => portNo;
            set
            {
                portNo = value;
                OnPropertyChanged(nameof(PortNo));
            }
        }

        /// <summary>
        /// 当前X1坐标
        /// </summary>
        private double x1;
        public double X1
        {
            get => x1;
            set
            {
                x1 = value;
                OnPropertyChanged(nameof(x1));
            }
        }

        /// <summary>
        /// 当前X1移动状态
        /// </summary>
        private bool x1_IsMoving;
        public bool X1_IsMoving
        {
            get => x1_IsMoving;
            set
            {
                x1_IsMoving = value;
                OnPropertyChanged(nameof(x1_IsMoving));
            }
        }

        /// <summary>
        /// 当前Y1坐标
        /// </summary>
        private double y1;
        public double Y1
        {
            get => y1;
            set
            {
                y1 = value;
                OnPropertyChanged(nameof(y1));
            }
        }

        /// <summary>
        /// 当前Y1移动状态
        /// </summary>
        private bool y1_IsMoving;
        public bool Y1_IsMoving
        {
            get => y1_IsMoving;
            set
            {
                y1_IsMoving = value;
                OnPropertyChanged(nameof(y1_IsMoving));
            }
        }

        /// <summary>
        /// 当前Z1坐标
        /// </summary>
        private double z1;
        public double Z1
        {
            get => z1;
            set
            {
                z1 = value;
                OnPropertyChanged(nameof(z1));
            }
        }

        /// <summary>
        /// 当前Z1移动状态
        /// </summary>
        private bool z1_IsMoving;
        public bool Z1_IsMoving
        {
            get => z1_IsMoving;
            set
            {
                z1_IsMoving = value;
                OnPropertyChanged(nameof(z1_IsMoving));
            }
        }


        /// <summary>
        /// 当前X2坐标
        /// </summary>
        private double x2;
        public double X2
        {
            get => x2;
            set
            {
                x2 = value;
                OnPropertyChanged(nameof(x2));
            }
        }

        /// <summary>
        /// 当前X2移动状态
        /// </summary>
        private bool x2_IsMoving;
        public bool X2_IsMoving
        {
            get => x2_IsMoving;
            set
            {
                x2_IsMoving = value;
                OnPropertyChanged(nameof(x2_IsMoving));
            }
        }

        /// <summary>
        /// 当前Y2坐标
        /// </summary>
        private double y2;
        public double Y2
        {
            get => y2;
            set
            {
                y2 = value;
                OnPropertyChanged(nameof(y2));
            }
        }

        /// <summary>
        /// 当前Y2移动状态
        /// </summary>
        private bool y2_IsMoving;
        public bool Y2_IsMoving
        {
            get => y2_IsMoving;
            set
            {
                y2_IsMoving = value;
                OnPropertyChanged(nameof(y2_IsMoving));
            }
        }

        /// <summary>
        /// 当前Z2坐标
        /// </summary>
        private double z2;
        public double Z2
        {
            get => z2;
            set
            {
                z2 = value;
                OnPropertyChanged(nameof(z2));
            }
        }

        /// <summary>
        /// 当前Z2移动状态
        /// </summary>
        private bool z2_IsMoving;
        public bool Z2_IsMoving
        {
            get => z2_IsMoving;
            set
            {
                z2_IsMoving = value;
                OnPropertyChanged(nameof(z2_IsMoving));
            }
        }


        /// <summary>
        /// 当前Y3坐标
        /// </summary>
        private double y3;
        public double Y3
        {
            get => y3;
            set
            {
                y3 = value;
                OnPropertyChanged(nameof(y3));
            }
        }

        /// <summary>
        /// 当前X2移动状态
        /// </summary>
        private bool y3_IsMoving;
        public bool Y3_IsMoving
        {
            get => y3_IsMoving;
            set
            {
                y3_IsMoving = value;
                OnPropertyChanged(nameof(y3_IsMoving));
            }
        }

        /// <summary>
        /// 当前z3坐标
        /// </summary>
        private double z3;
        public double Z3
        {
            get => z3;
            set
            {
                z3 = value;
                OnPropertyChanged(nameof(z3));
            }
        }

        /// <summary>
        /// 当前Z3移动状态
        /// </summary>
        private bool z3_IsMoving;
        public bool Z3_IsMoving
        {
            get => z3_IsMoving;
            set
            {
                z3_IsMoving = value;
                OnPropertyChanged(nameof(z3_IsMoving));
            }
        }

        /// <summary>
        /// 当前Z4坐标
        /// </summary>
        private double z4;
        public double Z4
        {
            get => z4;
            set
            {
                z4 = value;
                OnPropertyChanged(nameof(z4));
            }
        }

        /// <summary>
        /// 当前Z4移动状态
        /// </summary>
        private bool z4_IsMoving;
        public bool Z4_IsMoving
        {
            get => z4_IsMoving;
            set
            {
                z4_IsMoving = value;
                OnPropertyChanged(nameof(z4_IsMoving));
            }
        }



        /// <summary>
        /// 当前R1坐标
        /// </summary>
        private double r1;
        public double R1
        {
            get => r1;
            set
            {
                r1 = value;
                OnPropertyChanged(nameof(r1));
            }
        }

        /// <summary>
        /// 当前R1移动状态
        /// </summary>
        private bool r1_IsMoving;
        public bool R1_IsMoving
        {
            get => r1_IsMoving;
            set
            {
                r1_IsMoving = value;
                OnPropertyChanged(nameof(r1_IsMoving));
            }
        }

        /// <summary>
        /// 当前R2坐标
        /// </summary>
        private double r2;
        public double R2
        {
            get => r2;
            set
            {
                r2 = value;
                OnPropertyChanged(nameof(r2));
            }
        }

        /// <summary>
        /// 当前R2移动状态
        /// </summary>
        private bool r2_IsMoving;
        public bool R2_IsMoving
        {
            get => r2_IsMoving;
            set
            {
                r2_IsMoving = value;
                OnPropertyChanged(nameof(r2_IsMoving));
            }
        }

        /// <summary>
        /// 当前R3坐标
        /// </summary>
        private double r3;
        public double R3
        {
            get => r3;
            set
            {
                r3 = value;
                OnPropertyChanged(nameof(r3));
            }
        }

        /// <summary>
        /// 当前R3移动状态
        /// </summary>
        private bool r3_IsMoving;
        public bool R3_IsMoving
        {
            get => r3_IsMoving;
            set
            {
                r3_IsMoving = value;
                OnPropertyChanged(nameof(r3_IsMoving));
            }
        }
    }
}
