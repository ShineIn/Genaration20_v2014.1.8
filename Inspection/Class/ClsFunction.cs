﻿using Inspection.Views;
using LogSv;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Xml;

namespace Inspection
{
    internal class ClsFunction
    {

        public static string SettingDataDirectory;

        public static string ModuleName;

        public static string RecipeName;

        #region【输入字符串是否为IP地址格式】
        /// <summary>
        /// IP地址校验基于正则表达式
        /// </summary>
        /// <param name="ip">IP地址字符串</param>
        /// <returns>如果符合IP地址的校验规则则返回true,否则返回false</returns>
        public static bool IsIpAdress(string ip)
        {
            if (string.IsNullOrEmpty(ip) || ip.Length < 7 || ip.Length > 15)
            {
                return false;
            }
            string regformat = @"^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$";
            Regex regex = new Regex(regformat, RegexOptions.IgnoreCase);
            return regex.IsMatch(ip);
        }
        #endregion

        #region【读取CSV文件】
        /// <summary>
        /// 读取CSV文件
        /// </summary>
        /// <param name="myDgv"></param>
        /// <param name="fileName">csv文件存放路径</param>
        /// <param name="delimiters">分割符</param>
        /// <param name="firstRowContainsFieldNames">是否包含头文件</param>
        /// <returns></returns>
        public static void ReadCsv(ref DataGrid myDgv, string filePath)
        {
            System.Text.Encoding encoding = Encoding.UTF8;
            DataTable dt = new DataTable();
            using (TextFieldParser tfp = new TextFieldParser(filePath, System.Text.Encoding.GetEncoding(-0)))
            {
                tfp.SetDelimiters(",");

                #region【表头】
                if (!tfp.EndOfData)
                {
                    string[] fields = tfp.ReadFields();
                    for (int i = 0; i < fields.Length; i++)
                    {
                        dt.Columns.Add(fields[i]);
                    }
                }
                #endregion

                #region【行内容】
                while (!tfp.EndOfData)
                {
                    string[] fields = tfp.ReadFields();
                    DataRow dr = dt.NewRow();
                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        dr[i] = fields[i];
                    }
                    dt.Rows.Add(dr);
                }
                #endregion
            }
            myDgv.ItemsSource = dt.DefaultView;
        }
        #endregion

        #region【将字节转换为16进制】
        /// <summary>
        /// byte字节转换为16进制字符串
        /// </summary>
        /// <param name="byData">输入的字节数</param>
        /// <returns></returns>
        public static string BytetoHexStr(byte[] byData)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < byData.Length; i++)
            {
                builder.Append(string.Format("{0:x2}", byData[i]));
            }
            return builder.ToString().Trim();
        }
        #endregion

        #region【字节数组转换为16进制字符串】
        public static string ASCIItoHexStr(byte[] byData)
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < byData.Length; i++)
            {
                builder.Append(string.Format("{0:x2}", byData[i]));
            }
            return builder.ToString().Trim();
        }
        #endregion

        #region【16进制字符串转ASCII码】
        /// <summary>
        /// 16进制字符串转ASCII码
        /// </summary>
        /// <param name="hexStr">16进制字符串</param>
        /// <returns></returns>
        public static string HexToString(string hexStr)
        {
            if (hexStr.Contains(" "))
                hexStr = hexStr.Replace(" ", "");
            byte[] buff = new byte[hexStr.Length / 2];
            int index = 0;
            for (int i = 0; i < hexStr.Length; i += 2)
            {
                buff[index] = Convert.ToByte(hexStr.Substring(i, 2), 16);
                ++index;
            }
            return Encoding.Default.GetString(buff);
        }
        #endregion

        #region【将字符串转换为16进制字符串】
        /// <summary>
        /// ASCII至16进制字符串转换
        /// </summary>
        /// <param name="iData"></param>
        /// <returns></returns>
        public static byte[] ASCIItoHex(string iData)
        {
            int iLen = (Encoding.ASCII.GetBytes(iData)).Length;
            byte[] oData = new byte[iLen / 2];
            for (int i = 0; i < iLen / 2; i++)
            {
                oData[i] = Convert.ToByte(iData.Substring(i * 2, 2), 16);
            }
            return oData;
        }
        #endregion

        #region【操作DatsSet】
        /// <summary>
        /// 初始化SettingData
        /// </summary>
        /// <param name="SettingData">DataSet名</param>
        public static void InitSettingData(DataSet SettingData)
        {
            SettingData.Clear();
            string sSettingDataPath = String.Format("{0}\\{1}.xml", SettingDataDirectory, ModuleName);
            if (File.Exists(sSettingDataPath))
                SettingData.ReadXml(sSettingDataPath);
            for (int i = 0; i < SettingData.Tables.Count; i++)
                if (SettingData.Tables[i].Rows.Count == 0)
                {
                    DataRow NewRow = SettingData.Tables[i].NewRow();
                    SettingData.Tables[i].Rows.Add(NewRow);
                }
            SettingData.AcceptChanges();
        }

        /// <summary>
        /// 将值保存到SettingData中
        /// </summary>
        /// <param name="SettingData">DataSet名</param>
        /// <returns></returns>
        public static bool WriteSettingData(DataSet SettingData)
        {
            bool bSucced = false;
            try
            {
                string sSettingDataPath = String.Format("{0}\\{1}.xml", SettingDataDirectory, ModuleName);
                FileInfo fileInfo = new FileInfo(sSettingDataPath);
                if (fileInfo.Directory.Exists == false)
                    fileInfo.Directory.Create();
                SettingData.AcceptChanges();
                SettingData.WriteXml(sSettingDataPath);
                Log.log.Write("SettingData 参数保存成功!", System.Drawing.Color.Red);
                bSucced = true;
            }
            catch (Exception ex)
            {
                Log.log.Write("SettingData 参数保存Error:" + ex.Message, System.Drawing.Color.Red);
                bSucced = false;
            }
            return bSucced;
        }

        /// <summary>
        /// 将值保存到RecipeData中
        /// </summary>
        /// <param name="RecipePath"></param>
        /// <param name="RecipeData"></param>
        public static void WriteRecipeData(string RecipePath, DataSet RecipeData)
        {
            FileInfo fileInfo = new FileInfo(RecipePath);
            if (fileInfo.Directory.Exists == false)
                fileInfo.Directory.Create();
            RecipeData.AcceptChanges();
            DataSet ds = new DataSet();
            ds.Merge(RecipeData);
            ds.WriteXml(RecipePath);
        }

        public static void WritePSetData(string attribute, string value)
        {
            string sSettingDataPath = String.Format("{0}\\{1}.xml", SettingDataDirectory, ModuleName);
            UpdateEx(sSettingDataPath, "PSet", attribute, value);
        }

        /// <summary>
        /// 更新二级节点的值
        /// </summary>
        /// <param name="path">文件全称路径</param>
        /// <param name="nodeName">节点名称</param>
        /// <param name="attribute">属性名称</param>
        /// <param name="value">需要修改的值</param>
        /**************************************************
        * 使用示列: 
        * XmlHelper.Insert(path, "ProductSet", "bEnableCarA", "false")
        ************************************************/
        public static void UpdateEx(string path, string nodeName, string attribute, string value)
        {
            try
            {
                if (File.Exists(path))
                {
                    XmlDocument xmlDocument = new XmlDocument();
                    xmlDocument.Load(path);
                    XmlNode xn = xmlDocument.DocumentElement;
                    foreach (XmlNode node in xn.ChildNodes)
                    {
                        if (node.Name == nodeName)
                        {
                            node[attribute].InnerText = value;
                        }
                    }
                    xmlDocument.Save(path);
                }
                else
                {
                    System.Windows.Forms.MessageBox.Show("[" + path + "]文件不存在！");
                }
            }
            catch (Exception ex)
            {
                string str = ex.ToString();
            }
        }

        /// <summary>
        /// 修改数据
        /// </summary>
        /// <param name="path">路径</param>
        /// <param name="node">节点</param>
        /// <param name="attribute">属性名，非空时修改该节点属性值，否则修改节点值</param>
        /// <param name="value">值</param>
        /// <returns></returns>
        /**************************************************
         * 使用示列:
         * XmlHelper.Insert(path, "/Node", "", "Value")
         * XmlHelper.Insert(path, "/Node", "Attribute", "Value")
         ************************************************/
        public static void Update(string path, string node, string attribute, string value)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                XmlNode xn = doc.SelectSingleNode(node);
                XmlElement xe = (XmlElement)xn;
                if (attribute.Equals(""))
                    xe.InnerText = value;
                else
                    xe.SetAttribute(attribute, value);
                doc.Save(path);
            }
            catch (Exception ex)
            {
                string str = ex.ToString();
            }
        }


        private static dynamic GetDefaultValue(Type tp)
        {
            dynamic dmc = null;
            switch (Type.GetTypeCode(tp))
            {
                case TypeCode.Boolean:
                    dmc = false;
                    break;
                case TypeCode.String:
                    dmc = string.Empty;
                    break;
                case TypeCode.DateTime:
                    dmc = DateTime.MinValue;
                    break;
                case TypeCode.Char:
                    dmc = char.MinValue;
                    break;
                case TypeCode.Byte:
                    dmc = byte.MinValue;
                    break;
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                    dmc = uint.MinValue;
                    break;
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:
                    dmc = 0;
                    break;
            }
            return dmc;
        }

        public static dynamic GetSettingValue(DataSet SettingData, string TableName, string ColumnName, int RowIndex = 0)
        {
            dynamic Value = null;
            if (SettingData.Tables[TableName].Columns.IndexOf(ColumnName) >= 0)
            {
                Value = SettingData.Tables[TableName].Rows[RowIndex][ColumnName, DataRowVersion.Original];
                if (Value.ToString() == "")
                {
                    Log.log.WriteError("2025    " + "Setting data was not set value, ColumnName=\"" + ColumnName + "\"");
                    Value = GetDefaultValue(SettingData.Tables[TableName].Columns[ColumnName].DataType);
                }
            }
            else
                Log.log.WriteError("2023    " + "Setting data was not found the column, ColumnName=\"" + ColumnName + "\"");
            return Value;
        }

        /// <summary>
        /// 初始化加载ReciepeData中保存的数据
        /// </summary>
        /// <param name="RecipeData"></param>
        /// <param name="RecipePath"></param>
        public static void InitRecipeData(ref DataSet RecipeData, string RecipePath)
        {
            if (File.Exists(RecipePath))
            {
                DataSet ds = new DataSet();
                RecipeData.Clear();
                ds.Merge(RecipeData);

                ds.ReadXml(RecipePath);
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    try
                    {
                        DataTable dt = ds.Tables[i].Copy();
                        RecipeData.Tables[ds.Tables[i].TableName].Clear();
                        RecipeData.Merge(dt);
                    }
                    catch (Exception ex)
                    {
                        Log.log.Write("初始化ReciepeDataError" + ex.Message, System.Drawing.Color.Red);
                    }
                }
            }
            for (int i = 0; i < RecipeData.Tables.Count; i++)
                if (RecipeData.Tables[i].Rows.Count == 0)
                {
                    DataRow NewRow = RecipeData.Tables[i].NewRow();
                    RecipeData.Tables[i].Rows.Add(NewRow);
                }
            RecipeData.AcceptChanges();
        }

        /// <summary>
        /// 获取Reciepe中的数据值
        /// </summary>
        /// <param name="RecipeData">DataSet中的值</param>
        /// <param name="TableName">表格名</param>
        /// <param name="ColumnName">列名</param>
        /// <param name="RowIndex">行名(默认为第0行)</param>
        /// <returns></returns>
        public static dynamic GetRecipeValue(DataSet RecipeData, string TableName, string ColumnName, int RowIndex = 0)
        {
            dynamic Value = null;
            if (RecipeData.Tables[TableName].Columns.IndexOf(ColumnName) >= 0)
            {
                Value = RecipeData.Tables[TableName].Rows[RowIndex][ColumnName, DataRowVersion.Original];
                if (Value.ToString() == "")
                {
                    Log.log.WriteError("2026    " + "Recipe data was not set value, ColumnName=\"" + ColumnName + "\"");
                    Value = GetDefaultValue(RecipeData.Tables[TableName].Columns[ColumnName].DataType);
                }
            }
            else
                Log.log.WriteError("2024    " + "Recipe data was not found the column, ColumnName=\"" + ColumnName + "\"");
            return Value;
        }


        public static void ReadRecipeData(string RecipePath, DataSet RecipeData)
        {
            if (File.Exists(RecipePath))
            {
                DataSet ds = new DataSet();
                ds.Merge(RecipeData);
                ds.ReadXml(RecipePath);
                for (int i = 0; i < ds.Tables.Count; i++)
                    if (ds.Tables[i].Namespace == RecipeName)
                    {
                        try
                        {
                            DataTable dt = ds.Tables[i].Copy();
                            RecipeData.Tables[ds.Tables[i].TableName].Clear();
                            RecipeData.Merge(dt);
                        }
                        catch (Exception) { }
                    }
            }
            for (int i = 0; i < RecipeData.Tables.Count; i++)
                if (RecipeData.Tables[i].Rows.Count == 0)
                {
                    DataRow NewRow = RecipeData.Tables[i].NewRow();
                    RecipeData.Tables[i].Rows.Add(NewRow);
                }
            RecipeData.AcceptChanges();
        }
        #endregion

        #region【Delete files older than the specified time in the specified path=>删除指定路径下超过指定时间的文件】
        /// <summary>
        /// Delete files older than the specified time in the specified path
        /// </summary>
        /// <param name="fileDirectory">Folder where the path is stored =>存放路径的文件夹</param>
        /// <param name="overDueDays">Exceeded specified time=>超过的指定时间</param>
        public static void DeleteFile(string fileDirectory, int overDueDays)
        {
            DateTime NowTime = DateTime.Now;
            if (!Directory.Exists(fileDirectory)) return;
            string[] folderInfo = Directory.GetDirectories(fileDirectory);
            string[] files = Directory.GetFiles(fileDirectory, "*.*", System.IO.SearchOption.AllDirectories);
            foreach (var file in files)
            {
                FileInfo fileInfo = new FileInfo(file);
                TimeSpan t = NowTime - fileInfo.CreationTime;
                int day = t.Days;
                if (day > overDueDays)
                    File.Delete(file);
            }
        }
        #endregion

        /// <summary>
        /// 将浮点数转换为高低位
        /// </summary>
        /// <param name="fstring"></param>
        /// <param name="Ret"></param>
        public static void FloatToInt(float floatValue, out Int16[] ret)
        {
            ret = new Int16[2];
            byte[] bArr = BitConverter.GetBytes(floatValue);
            ret[0] = (Int16)Convert.ToUInt32((bArr[1] << 8) + bArr[0]);
            ret[1] = (Int16)Convert.ToInt32((bArr[3] << 8) + bArr[2]);
        }

        /// <summary>
        /// 将浮点数转换为高低位
        /// </summary>
        /// <param name="fstring"></param>
        /// <param name="Ret"></param>
        public static void FloatToASCII(float floatValue, out string[] ret)
        {
            ret = new string[2];
            byte[] bArr = BitConverter.GetBytes(floatValue);
            int bArr1 = (Int32)Convert.ToUInt32((bArr[1] << 8) + bArr[0]);
            int bArr2 = (Int32)Convert.ToInt32((bArr[3] << 8) + bArr[2]);
            ret[0] = (Convert.ToString(bArr1, 16)).ToUpper();
            ret[1] = (Convert.ToString(bArr2, 16)).ToUpper();
            for (int i = 0; i < 2; i++)
            {
                switch (ret[i].Length)
                {
                    case 1:
                        ret[i] += "000";
                        break;
                    case 2:
                        ret[i] += "00";
                        break;
                    case 3:
                        ret[i] += "0";
                        break;
                    default:
                        ret[i] += "";
                        break;
                }
            }
        }

        /// <summary>
        /// 读取的short类型的高低位字符串转换为浮点数
        /// </summary>
        /// <param name="lowBitValue">低位数据</param>
        /// <param name="hightBitValue">高位数据</param>
        /// <param name="floatValue"></param>
        public static void BitToFloat(Int16 lowBitValue, Int16 hightBitValue, out float floatValue)
        {
            byte[] ibArr1 = BitConverter.GetBytes(lowBitValue);
            byte[] ibArr2 = BitConverter.GetBytes(hightBitValue);
            byte[] ibArr = new byte[4] { ibArr1[0], ibArr1[1], ibArr2[0], ibArr2[1] };
            floatValue = BitConverter.ToSingle(ibArr, 0);
        }

        public static bool BytesToFloat(byte[] inputByte,out float floatValue)
        {
            if (inputByte.Length < 4)
            {
                floatValue = 0;
                return false;
            }
            byte[] dataBuf1 = new byte[2] { 0, 0 };
            byte[] dataBuf2 = new byte[2] { 0, 0 };

            dataBuf1[0] = inputByte[1];
            dataBuf1[1] = inputByte[0];
            Int16 lowValue = BitConverter.ToInt16(dataBuf1, 0);

            dataBuf2[0] = inputByte[3];
            dataBuf2[1] = inputByte[2];
            Int16 highValue = BitConverter.ToInt16(dataBuf2, 0);           
            BitToFloat(lowValue, highValue, out floatValue);
            return true;
        }
     

        #region【从指定路径下打开指定的exe】       
        public static void OpenExe(string exePath)
        {
            Process p = new Process();
            p.StartInfo.FileName = exePath;
            p.Start();
        }
        #endregion

        #region 【进程结束】
        public static void KillProcess(string ProcessName)
        {
            Process[] pro = Process.GetProcesses();
            for (int i = 0; i < pro.Length; i++)
            {
                if (pro[i].ProcessName.ToString() == ProcessName)                      //遍历进程名称
                {
                    pro[i].Kill();                                                     //结束进程
                }
            }
        }
        #endregion

        #region 【判断程序是否已经运行】
        public static bool ProcessIsOpen(string ProcessName)
        {
            string[] ProcessNames;
            bool openFlag = false;
            Process[] pro = Process.GetProcesses();
            ProcessNames = new string[pro.Length];
            for (int i = 0; i < pro.Length; i++)
            {
                ProcessNames[i] = pro[i].ProcessName.ToString();
                if (pro[i].ProcessName.ToString() == ProcessName)                         //遍历进程名称
                {
                    openFlag = true;
                    break;
                }
            }
            return openFlag;
        }
        #endregion
    }
}
