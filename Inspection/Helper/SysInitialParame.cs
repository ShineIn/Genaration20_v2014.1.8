﻿using G4.GlueCore;
using Infrastructure;
using ModuleData;
using ModuleData20.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inspection.Helper
{
    /// <summary>
    /// 系统初始化参数
    /// </summary>
    [Serializable]
    public class SysInitialParame : PropertyChangedNotify
    {
        /// <summary>
        /// 初始化参数
        /// </summary>
        public StationTwoIntial InitialMode 
        {
            get { return initialMode; }
            set
            {
                initialMode = value;
                OnPropertyChanged();
            }
        }
        private StationTwoIntial initialMode = new StationTwoIntial { AxisWorkModule1 = EnumWorkModule.S20_Left, AxisWorkModule2 = EnumWorkModule.S20_Right };

        /// <summary>
        /// 德创TCP端口
        /// </summary>
        public int DCTcpPortNum
        {
            get { return _dCTcpPortNum; }
            set { _dCTcpPortNum = value; OnPropertyChanged(); }
        }
        private int _dCTcpPortNum = 0;

        /// <summary>
        /// 右工位德创TCP端口
        /// </summary>
        public int RightSlotDCTcpPortNum
        {
            get { return _rightSlotDCTcpPortNum; }
            set { _rightSlotDCTcpPortNum = value; OnPropertyChanged(); }
        }
        private int _rightSlotDCTcpPortNum = 3001;

        /// <summary>
        /// 左激光的IP
        /// </summary>
        public string LaserIP
        {
            get { return laserIP; }
            set
            {
                if (ClsFunction.IsIpAdress(value))
                {
                    laserIP = value;
                    OnPropertyChanged();
                }
            }
        }
        private string laserIP = "10.66.81.35";

        /// <summary>
        /// 左激光的端口
        /// </summary>
        public int LaserPortNum
        {
            get { return _laserPortNum; }
            set { _laserPortNum = value; OnPropertyChanged(); }
        }
        private int _laserPortNum = 64000;

        /// <summary>
        /// 右激光的IP
        /// </summary>
        public string RightLaserIP
        {
            get { return rightlaserIP; }
            set
            {
                if (ClsFunction.IsIpAdress(value))
                {
                    rightlaserIP = value;
                    OnPropertyChanged();
                }
            }
        }
        private string rightlaserIP = "10.66.81.34";

        /// <summary>
        /// 右激光的端口
        /// </summary>
        public int RightLaserPortNum
        {
            get { return _rightlaserPortNum; }
            set { _rightlaserPortNum = value; OnPropertyChanged(); }
        }
        private int _rightlaserPortNum = 64000;


        /// <summary>
        /// PLC的IP
        /// </summary>
        public string PLCIP
        {
            get { return _pLCIP; }
            set
            {
                if (ClsFunction.IsIpAdress(value))
                {
                    _pLCIP = value;
                    OnPropertyChanged();
                }
            }
        }
        private string _pLCIP = "10.66.81.24";


        /// <summary>
        /// 德创心跳端口
        /// </summary>
        public int DCHBTcpPortNum
        {
            get { return _dCHBTcpPortNum; }
            set { _dCHBTcpPortNum = value; OnPropertyChanged(); }
        }
        private int _dCHBTcpPortNum = 10086;


        /// <summary>
        /// 德创心跳间隔
        /// </summary>
        public int DCHBTickInterval
        {
            get { return _dCHBTickInterval; }
            set { _dCHBTickInterval = value; OnPropertyChanged(); }
        }
        private int _dCHBTickInterval = 5;

        public void Save()
        {
            XmlSerializerHelper.Save(this, FilePath);
        }

        public static SysInitialParame Load()
        {
            if (!System.IO.File.Exists(FilePath)) return new SysInitialParame();
            string strErr = string.Empty;
            SysInitialParame system = XmlSerializerHelper.Load<SysInitialParame>(FilePath, ref strErr,false);
            return system;
        }

        private static string FilePath = AppDomain.CurrentDomain.BaseDirectory + "Config\\Sys.xml";
    }
}
