﻿using Infrastructure;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace Inspection.Helper
{
    class LogNetHelper : ILogService
    {
        private static ILog log = LogManager.GetLogger("Logger");

        public void Debug(object message)
        {
            if (log.IsDebugEnabled)
            {
                log.Debug(message);
            }
        }

        public void Debug(Exception ex1)
        {
            if (log.IsDebugEnabled)
            {
                log.Debug(ex1.Message.ToString() + "/r/n" + ex1.Source.ToString() + "/r/n" + ex1.TargetSite.ToString() + "/r/n" + ex1.StackTrace.ToString());
            }
        }

        public void Error(object message)
        {
            if (log.IsErrorEnabled)
            {
                log.Error(message);
            }
        }

        public void Fatal(object message)
        {
            if (log.IsFatalEnabled)
            {
                log.Fatal(message);
            }
        }

        public void Info(object message)
        {
            if (log.IsInfoEnabled)
            {
                log.Info(message);
            }
        }

        public void Warn(object message)
        {
            if (log.IsWarnEnabled)
            {
                log.Warn(message);
            }
        }
    }
}