﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using G4.GlueCore;
using Googo.Manul;
using HslCommunication.Profinet.Inovance;
using Inspection.Class;
using Inspection.Views;
using ModuleData;
using S7.Net;

namespace Inspection.Helper
{
    internal  class GlobalParame
    {
        #region INotifyPropertyChanged
        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void OnPropertyChanged([CallerMemberName] string name = null)
        {
            if (StaticPropertyChanged != null)
            {
                StaticPropertyChanged.Invoke(null, new PropertyChangedEventArgs(name));
            }
        }
        #endregion INotifyPropertyChanged

        /// <summary>
        /// PLC
        /// </summary>
        public static Plc PLC
        {
            get { return _plc; }
            set
            { 
                _plc = value;
                OnPropertyChanged();
                GloMotionParame.SiemenPLC = value;
            }
        }
        private static Plc _plc =null;

        /// <summary>
        /// 初始化参数
        /// </summary>
        public static SysInitialParame SysParame
        {
            get { return sysParame; }
            set
            {
                sysParame = value;
                OnPropertyChanged();
            }
        }
        private static SysInitialParame sysParame = new SysInitialParame ();

        /// <summary>
        /// 线扫测高激光器
        /// </summary>
        public static LaserInstance Laser
        {
            get { return _laser; }
            set { _laser = value; OnPropertyChanged(); }
        }
        private static LaserInstance _laser = null;

        public static SuperSimpleTcp.SimpleTcpClient DCCKTCPClient
        {
            get { return _dCClient; }
            set { _dCClient = value;OnPropertyChanged(); }
        }
        private static SuperSimpleTcp.SimpleTcpClient _dCClient;

        public static SuperSimpleTcp.SimpleTcpClient RightSlotDCCKTCPClient
        {
            get { return _rightSlotDCCKTCPClient; }
            set { _rightSlotDCCKTCPClient = value; OnPropertyChanged(); }
        }
        private static SuperSimpleTcp.SimpleTcpClient _rightSlotDCCKTCPClient;

        /// <summary>
        /// 贴装报警提示窗
        /// </summary>
        public static ShowAlarm.WinShowAlarm MountingAlarmFrm
        {
            get { return _mountingAlarmFrm; }
            set { _mountingAlarmFrm = value; OnPropertyChanged(); }
        }
        private static ShowAlarm.WinShowAlarm _mountingAlarmFrm;

        /// <summary>
        /// 左点胶报警提示窗
        /// </summary>
        public static ShowAlarm.WinShowAlarm Glue1AlarmFrm
        {
            get { return _glue1AlarmFrm; }
            set { _glue1AlarmFrm = value; OnPropertyChanged(); }
        }
        private static ShowAlarm.WinShowAlarm _glue1AlarmFrm;


        /// <summary>
        /// 右点胶报警提示窗
        /// </summary>
        public static ShowAlarm.WinShowAlarm Glue2AlarmFrm
        {
            get { return _glue2AlarmFrm; }
            set { _glue2AlarmFrm = value; OnPropertyChanged(); }
        }
        private static ShowAlarm.WinShowAlarm _glue2AlarmFrm;



        public static SuperSimpleTcp.SimpleTcpClient HeightLaser
        {
            get { return _heightLaser; }
            set { _heightLaser = value; OnPropertyChanged(); }
        }
        private static SuperSimpleTcp.SimpleTcpClient _heightLaser;

        public static SuperSimpleTcp.SimpleTcpClient RightHeightLaser
        {
            get { return _rightHeightLaser; }
            set { _rightHeightLaser = value; OnPropertyChanged(); }
        }
        private static SuperSimpleTcp.SimpleTcpClient _rightHeightLaser;

        /// <summary>
        /// 点测高激光器
        /// </summary>
        public static KMeasureHeight PLaser
        {
            get { return _pLaser; }
            set { _pLaser = value; OnPropertyChanged(); }
        }
        private static KMeasureHeight _pLaser = null;

        // <summary>
        /// UV灯
        /// </summary>
        public static UVLight_ModbusTcp UVLight
        {
            get { return _uVLight; }
            set { _uVLight = value; OnPropertyChanged(); }
        }
        private static UVLight_ModbusTcp _uVLight = null;
      
    }
}
