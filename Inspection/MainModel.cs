﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using Inspection.Helper;
using Inspection.Views;
using LogSv;
using ModuleData;

namespace Inspection
{
    internal class MainModel : PropertyChangedNotify
    {
           /// <summary>
        /// 程序版本
        /// </summary>
        private string version = "";
        public string SoftWareVersion
        {
            get
            {
                if (version == "")
                {
                    string tempver = string.Concat("SoftVersion : ", System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
                    version = tempver;
                    Log.log.Write($"当前程序版本号为：{tempver}", System.Drawing.Color.Black);
                }
                return version;
            }
            set
            {
                version = value;
                OnPropertyChanged("SoftWareVersion");
            }
        }




        private ClsParam settingPara = new ClsParam();
        public ClsParam SettingPara
        {
            get
            {
                if (settingPara == null)
                {
                    throw new ArgumentNullException("settingPara is Null");                   
                }
                return settingPara;
            }
            set
            {            
                settingPara = value;
                OnPropertyChanged("SettingPara");
            }
        }

        /// <summary>
        /// 程序更新时间
        /// </summary>
        private string modifyDate;
        public string ModifyDate
        {
            get
            {
                string tempDate = string.Concat("Modify Date : ", System.IO.Directory.GetLastWriteTime(Assembly.GetExecutingAssembly().Location).ToString("yyyy/MM/dd HH:mm:ss tt"));
                modifyDate = tempDate;
                Log.log.WriteInfo($"当前程序最新生成时间为:{modifyDate}");
                return modifyDate;
            }
            set
            {
                modifyDate = value;
                OnPropertyChanged("ModifyDate");
            }
        }

        /// <summary>
        /// 数据库连接状态
        /// </summary>
        private bool sqlConnState;
        public bool SqlConnState
        {
            get { return sqlConnState; }
            set
            {
                sqlConnState = value;
                OnPropertyChanged("sqlConnState");
            }
        }

        /// <summary>
        /// 设备状态字体颜色
        /// </summary>
        private SolidColorBrush machineStatusColor;
        public SolidColorBrush MachineStatusColor
        {
            get => machineStatusColor;
            set
            {
                machineStatusColor = value;
                OnPropertyChanged(nameof(machineStatusColor));
            }
        }

        /// <summary>
        /// 设备状态
        /// </summary>
        private string machineStatus;
        public string MachineStatus
        {
            get => machineStatus;
            set
            {
                machineStatus = value;
                OnPropertyChanged(nameof(machineStatus));
            }
        }

        /// <summary>
        /// 型号
        /// </summary>
        private string modelName;
        public string ModelName
        {
            get => modelName;
            set
            {
                modelName = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// 用户
        /// </summary>
        private string userName;
        public string UserName
        {
            get => userName;
            set
            {
                userName = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// 磁盘剩余空间
        /// </summary>
        public string RemainSpace
        {
            get => remainSpace;
            set
            {
                remainSpace = value;
                OnPropertyChanged("RemainSpace");
            }
        }
        private string remainSpace;


        public UserManagement.UserManagement userManagement;     
        private IDelegateCommand login;
        public IDelegateCommand LoginCommand
        {
            get
            {
                return login ?? (login = CommandProvider.Create(() =>
                {
                    userManagement = new UserManagement.UserManagement();
                    if (userManagement != null)
                        userManagement.Close();
                    Log.log.Write("按下用户登录按钮", System.Drawing.Color.Black);
                    userManagement = new UserManagement.UserManagement();
                    userManagement.SqlServerName = "localhost";
                    userManagement.UserID = "root";
                    userManagement.PortNum = 3306;
                    userManagement.DataBase = "mysql";
                    userManagement.Password = "123456";
                    userManagement.ShowDialog();
                    userManagement.Activate();
                }));
            }
        }

        private IDelegateCommand exitCommand;
        public IDelegateCommand ExitCommand
        {
            get
            {
                return exitCommand ?? (exitCommand = CommandProvider.Create(() =>
                {
                    var ret = Xceed.Wpf.Toolkit.MessageBox.Show($"是否退出程序？", "询问", System.Windows.MessageBoxButton.OKCancel, MessageBoxImage.Question, null);
                    if (ret == MessageBoxResult.OK)
                    {                                        
                        Log.log.Write("退出程序!", System.Drawing.Color.Black);
                        System.Environment.Exit(0);
                    }
                    else                   
                        return;                   
                }));
            }
        }   
        ConcurrentDictionary<Type, object> _views;

        private ProjectInfor projectInfor=new ProjectInfor();
        public ProjectInfor ProjectInfor
        {
            get { return projectInfor; }
            set
            {
                projectInfor = value;
                OnPropertyChanged();
            }
        }
        
    }
}
