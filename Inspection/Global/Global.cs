﻿using MvCodeReaderSDKNet;
using SuperSimpleTcp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Inspection
{
    internal class Global
    {

        /// <summary>
        /// 新建静态属性变更通知
        /// </summary>
        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        protected static void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != StaticPropertyChanged)
            {
                StaticPropertyChanged(null, new PropertyChangedEventArgs(PropertyName));
            }
        }

        public static ClsStateInfo ModulePara
        {
            get => bindData;
            set
            {
                bindData = value;
                StaticPropertyChanged?.Invoke(null, new PropertyChangedEventArgs(nameof(ModulePara)));
            }
        }
        private static ClsStateInfo bindData = new ClsStateInfo();


        //※★※★※★※★※★※★※★【报警代码】※★※★※★※★※★※★※★※★※
        #region【点胶模组1报警】
        public static readonly string Motor1HardAlarmFilePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\AlarmCode\\Motor1HardAlarmCode.xml");
        public static Dictionary<string, string> DicMotor1HardAlarmCode;

        #endregion

        #region【贴装模组报警代码存储路径】
        public static readonly string MountingHardAlarmFilePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\AlarmCode\\MoutingHardAlarmCode.xml");
        public static Dictionary<string, string> DicMountingHardAlarmCode;
        #endregion

        #region【点胶模组2报警】
        public static readonly string Motor2HardAlarmFilePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\AlarmCode\\Motor2HardAlarmCode.xml");
        public static Dictionary<string, string> DicMotor2HardAlarmCode;

        #endregion
        //※★※★※★※★※★※★※★※★※★※★※★※★※★※★※★※★※★※★※★※★※★

    }
}
