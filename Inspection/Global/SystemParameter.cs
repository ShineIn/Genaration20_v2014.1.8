﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using MySql.Data;
using MySqlLib;
using UserManagement;
using MvCodeReaderSDKNet;

namespace Inspection
{
    public class SysPara
    {    
        public static string WorkLog;
        public static string LogPath;

        public static bool bInitIsOK = false;
        public static bool bIniting = false;
        public static bool bPLCInitIsOK = false;
        public static bool bMotionInitIsOK = false;


        public static string SysRXParamPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\SysRXParam.xml");
        public static string SysTXParamPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\SysTXParam.xml");

        public static string SysLGlueBaseParamPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\SysLGlueBaseParam.xml");
        public static string SysLGlueRXParamPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\SysLGlueRXParam.xml");
        public static string SysLGlueTXParamPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\SysLGlueTXParam.xml");
        public static string SysRGlueParamPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\SysRGlueParam.xml");
        public static string SysMountingRXParamPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\SysMountingRXParam.xml");
        public static string SysMountingTXParamPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\SysMountingTXParam.xml");
        public static string SysInitParamPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\SysInitParam.xml");
        public static string FileOperatePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\FileOperate.fileChanged");


        public static string SysModel = "RX";
        public static string[] AlarmDgvHeader = new string[] {"代码", "DB地址", "类别", "报警信息","处理信息"};


        public static SqlOperator mySql;
        public static bool IsSqlOpen;

      
        public static bool plcConnectState;

    }
}
