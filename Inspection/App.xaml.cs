﻿using Infrastructure;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using LogSv;
using System.Drawing;
using System.Diagnostics;
using Microsoft.VisualBasic;
using Googo.Manul;
using Inspection.Helper;
using Inspection.Class;
using G4.Motion;
using S7.Net;
using System.Xml.Serialization;
using G4.GlueCore;

namespace Inspection
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            DispatcherUnhandledException += App_DispatcherUnhandledException;
            Exit += App_Exit;
            SysPara.LogPath = LogPath();
            Log.LogPath = SysPara.LogPath;                                     //注意一定是设定存放路径在前，才能开始进行日志写入
            Log.log.Write("程序启动.....", System.Drawing.Color.Black);
            Log.log.Write($"程序运行日志存放路径为{SysPara.LogPath}", System.Drawing.Color.Black);
            if (ProcessIsOpen(System.Reflection.Assembly.GetExecutingAssembly().GetName().Name.ToString(), 2))
            {
                Xceed.Wpf.Toolkit.MessageBox.Show("软件已经开启，请勿重复打开.......", "警告", MessageBoxButton.OK, MessageBoxImage.Warning, null);
                Environment.Exit(0);
                return;
            }
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            if (0 != MotionModel.Instance.Initialize("S20"))
            {
                MessageBox.Show("打開伺服驅動器失敗");
                //Environment.Exit(0);
            }
            GloMotionParame.Initial();
            LogServiceHelper.Register(new LogNetHelper());
            //SysInitialParame parame = SysInitialParame.Load();
            //if (null == parame) return;
            //GlobalParame.SysParame = parame;
            //GlobalParame.DCCKTCPClient = new SuperSimpleTcp.SimpleTcpClient("127.0.0.1", parame.DCTcpPortNum);
            //GlobalParame.HeightLaser = new SuperSimpleTcp.SimpleTcpClient(parame.LaserIP, parame.LaserPortNum);
            //GlobalParame.RightHeightLaser = new SuperSimpleTcp.SimpleTcpClient(parame.RightLaserIP, parame.RightLaserPortNum);
            //GlobalParame.PLC = new S7.Net.Plc(CpuType.S71500, parame.PLCIP, 0, 0);
            //try
            //{
            //    GlobalParame.DCCKTCPClient.Connect();
            //    GlobalParame.HeightLaser.Connect();
            //    GlobalParame.PLC.Open();
            //}
            //catch (Exception ex)
            //{
            //    Debug.WriteLine(ex);
            //}
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
        }


        private void App_Exit(object sender, ExitEventArgs e)
        {
            Environment.Exit(0);
        }

        private void App_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            e.Handled = true;
            Exception ex = e.Exception;
            string errorMsg = "App_DispatcherUnhandledException : \n\n" + ex.Message + ex.StackTrace;
            MessageBox.Show(errorMsg);
            LogServiceHelper.Error(string.Format("【{0}】-【App_DispatcherUnhandledException】-【异常】:{1}", "App", errorMsg));
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception exp = (Exception)e.ExceptionObject;
            string strErr = exp.ToString();
            LogServiceHelper.Error(strErr);
            MessageBox.Show(strErr);
        }

        private string LogPath()
        {
            List<string> driverList = GetDiskList();
            string baseDriver = string.Empty, logPath = string.Empty;
            if (driverList.Contains("D:\\"))
            {
                baseDriver = "D:\\";
            }
            else if (driverList.Contains("E:\\"))
            {
                baseDriver = "E:\\";
            }
            SysPara.WorkLog = baseDriver + "WorkLog";
            logPath = SysPara.WorkLog + @"\LogMessage";
            if (!Directory.Exists(SysPara.WorkLog)) Directory.CreateDirectory(SysPara.WorkLog);
            if (!Directory.Exists(logPath)) Directory.CreateDirectory(logPath);
            return logPath;
        }


        /// <summary>
        /// 获取硬盘信息
        /// </summary>
        /// <returns></returns>
        private List<string> GetDiskList()
        {
            string[] drivers = Environment.GetLogicalDrives();
            return new List<string>(drivers);
        }

        /// <summary>
        /// 防止程序二次启动
        /// </summary>
        /// <param name="ProcessName">程序名称</param>
        /// <param name="Counter">程序允许打开个数</param>
        /// <returns></returns>
        private bool ProcessIsOpen(string ProcessName, int Counter)
        {
            string[] ProcessNames;
            bool openFlag = false;
            Process[] pro = Process.GetProcesses();
            ProcessNames = new string[pro.Length];
            int count = 0;
            for (int i = 0; i < pro.Length; i++)
            {
                ProcessNames[i] = pro[i].ProcessName.ToString();
                if (pro[i].ProcessName.ToString().Contains(ProcessName) && !pro[i].ProcessName.ToString().Contains(".vshost"))                    //遍历进程名称
                {
                    count++;
                }
                if (count == Counter)
                {
                    openFlag = true;
                    break;
                }
                else
                    openFlag = false;
            }
            return openFlag;
        }
    }
}
