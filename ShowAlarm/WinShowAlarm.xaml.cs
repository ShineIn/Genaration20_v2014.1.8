﻿using LogSv;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Threading;
using S7.Net;
using System.Globalization;
using System.Windows.Markup;
using Googo.Manul;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;

namespace ShowAlarm
{
    /// <summary>
    /// ShowAlarm.xaml 的交互逻辑
    /// </summary>
    public partial class WinShowAlarm : Window, INotifyPropertyChanged
    {
        public WinShowAlarm(List<Tuple<int, string, string, string, string>> alarmDatas)
        {
            InitializeComponent();
            alarmDgvDatas = alarmDatas;
            RefrehAlarm();
        }

        public static DependencyProperty AlarmCodeProperty = DependencyProperty.Register("AlarmCode", typeof(short), typeof(WinShowAlarm), new FrameworkPropertyMetadata((short)0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty AlarmInfoProperty = DependencyProperty.Register("AlarmInfo", typeof(string), typeof(WinShowAlarm), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty TipsInfoProperty = DependencyProperty.Register("TipsInfo", typeof(string), typeof(WinShowAlarm), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty AlarmTimeProperty = DependencyProperty.Register("AlarmTime", typeof(string), typeof(WinShowAlarm), new FrameworkPropertyMetadata("", FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {

                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        public List<Tuple<int, string, string, string, string>> AlarmDgvDatas
        {
            get { return alarmDgvDatas; }
            set { alarmDgvDatas = value; }
        }
        private List<Tuple<int, string, string, string, string>> alarmDgvDatas = new List<Tuple<int, string, string, string, string>>();

        public Queue<int> Alarm
        {
            get { return alarm; }
            set { alarm = value; }
        }
        private Queue<int> alarm = new Queue<int>();
        public short AlarmCode
        {
            get { return (short)GetValue(AlarmCodeProperty); }
            set { SetValue(AlarmCodeProperty, value); }
        }

        /// <summary>
        /// PLC
        /// </summary>
        public Plc PLC
        {
            get { return _plc; }
            set
            {
                _plc = value;
            }
        }
        private static Plc _plc = null;

        public string AlarmInfo
        {
            get { return (string)GetValue(AlarmInfoProperty); }
            set { SetValue(AlarmInfoProperty, value); }
        }

        public string TipsInfo
        {
            get { return (string)GetValue(TipsInfoProperty); }
            set { SetValue(TipsInfoProperty, value); }
        }

        public string AlarmTime
        {
            get { return (string)GetValue(AlarmTimeProperty); }
            set { SetValue(AlarmTimeProperty, value); }
        }

        public DateTime CurTime
        {
            get { return curTime; }
            set { curTime = value; UpdateTime(); }
        }
        private DateTime curTime = new DateTime();


        private DateTime StartTime = new DateTime();
        public void ShowAlarmForm(short codeNo, string code, string tipInfo)
        {
            if (codeNo == lastcode) return;
            lastcode = codeNo;
            Dispatcher.Invoke(() =>
            {
                if (alarmform.IsActive) return;
                else
                {
                    AlarmCode = codeNo;
                    AlarmInfo = code;
                    TipsInfo = tipInfo;
                    StartTime = DateTime.Now;
                    alarmform.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                    alarmform.Width = 1200;
                    alarmform.Height = 500;
                    alarmform.Show();
                }
            });
        }

        private void UpdateTime()
        {
            AlarmTime = (CurTime - StartTime).ToString(@"hh\:mm\:ss");
        }

        Thread thr_ShowAlarm;
        Task task;
        private void RefrehAlarm()
        {
            if (thr_ShowAlarm != null)
            {
                if (thr_ShowAlarm.IsAlive)
                {
                    thr_ShowAlarm.Abort();
                }
            }
            thr_ShowAlarm = new Thread(new ThreadStart(() =>
            {
                while (true)
                {
                    if (Alarm.Count > 0)
                    {
                        task = Task.Factory.StartNew(() =>
                        {
                            ShowAlarm((short)Alarm.Dequeue());
                        });
                    }
                    Dispatcher.Invoke(() =>
                    {
                        CurTime = DateTime.Now;
                    });
                    Thread.Sleep(10);
                }
            }));
            thr_ShowAlarm.IsBackground = true;
            thr_ShowAlarm.Start();

        }

        /// <summary>
        /// 根据报警代码，从报警代码表内找报警内容
        /// </summary>
        /// <param name="codeNo"></param>
        private void ShowAlarm(short codeNo)
        {
            if (alarmDgvDatas.Count <= 0) return;
            Tuple<int, string, string, string, string> alramInfo;
            alramInfo = alarmDgvDatas.Find(p => p.Item1 == codeNo);
            if (alramInfo != null)
            {
                Dispatcher.Invoke(() =>
                {
                    ShowAlarmForm((short)alramInfo.Item1, alramInfo.Item4, alramInfo.Item5);
                    if (PLC.IsConnected)
                    {
                        switch (alramInfo.Item3)
                        {
                            case "硬件":
                                PLC.Write(alramInfo.Item2, (short)1000);
                                break;
                            case "流程":
                                PLC.Write(alramInfo.Item2, (short)2000);
                                break;
                            case "预警":
                                PLC.Write(alramInfo.Item2, (short)3000);
                                break;
                            default:
                                Log.log.Write($"{alramInfo.Item1}报警类别错误,正确类别是(硬件，流程，预警)中的一种，请检查报警代码", System.Drawing.Color.Red);
                                break;
                        }
                    }
                });
            }
            else
            {
                Log.log.Write($"未找到报警号为：{codeNo}对应的报警信息", System.Drawing.Color.Red);
            }
        }

        public void CloseAlarmFrm()
        {
            Dispatcher.Invoke(() =>
            {
                if (this.IsActive)
                    this.Close();
            });
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;  // cancels the window close
            this.Hide();      // Programmatically hides the window
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            StageErrorInfor stageError = GloMotionParame.RunErrorList.FirstOrDefault((item) => { return item.Index == 1; });
            if (null == stageError)
            {
                stageError = new StageErrorInfor { Index = 1 };
                GloMotionParame.RunErrorList.Add(stageError);
            }
            RunErrorInfor runError = stageError.AlarmCodeList.FirstOrDefault((tt) => { return tt.AlarmCode == (short)1; });
            if (null == runError)
            {
                runError = new RunErrorInfor { AlarmCode = (short)1, AlarmContent = AlarmInfo };
                stageError.AlarmCodeList.Add(runError);
            }
            runError.Solution = soltxt.Text;
            string dir = System.IO.Path.GetDirectoryName(GloMotionParame.RunErrorPath);
            if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
            XmlSerializer xs = new XmlSerializer(typeof(ObservableCollection<StageErrorInfor>));
            FileStream fs = new FileStream(GloMotionParame.RunErrorPath, FileMode.OpenOrCreate, FileAccess.Write);
            xs.Serialize(fs, GloMotionParame.RunErrorList);
            fs.Close();
        }

        private int lastcode = -1;
    }

    public class EngineSolConverter : MarkupExtension, IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (null == value || DependencyProperty.UnsetValue == value || !(value is int code)) return string.Empty;
            StageErrorInfor stageError = GloMotionParame.RunErrorList.FirstOrDefault((item) => { return item.Index == 1; });
            if (null == stageError || null == stageError.AlarmCodeList)
                return string.Empty;
            RunErrorInfor runError = stageError.AlarmCodeList.FirstOrDefault((tt) => { return tt.AlarmCode == (short)code; });
            if (null == runError) return string.Empty;
            return runError.Solution;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
