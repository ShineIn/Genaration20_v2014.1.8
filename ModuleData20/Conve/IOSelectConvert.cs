﻿using Googo.Manul;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Markup;
using System.Windows;

namespace ModuleData20
{
    public class IOSelectConvert : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (null == value || DependencyProperty.UnsetValue == value || null == parameter) return null;
            string strParame = parameter.ToString();
            int index = (int)value;
            IOCollection oStatuses = null;
            if (strParame == "Input") oStatuses = GloMotionParame.InputList;
            else if (strParame == "Output") oStatuses = GloMotionParame.OutputList;
            if (oStatuses == null) return null;
            return oStatuses.FirstOrDefault((temp) => { return temp.BitIndex == index; });
        }


        public object ConvertBack(object value, Type targetTypes, object parameter, CultureInfo culture)
        {
            if (DependencyProperty.UnsetValue == value || null == value) return null;
            IOStatus oStatus = value as IOStatus;
            if (oStatus != null)
            {
                return oStatus.BitIndex;
            }
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
