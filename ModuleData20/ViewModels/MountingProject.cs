﻿using G4.GlueCore;
using Googo.Manul;
using ModuleData.ViewModels;
using ModuleData20.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ModuleData;
using System.Xml.Serialization;
using ModuleData20.Views;
using System.ComponentModel;

namespace ModuleData20
{
    /// <summary>
    /// 贴装项目参数
    /// </summary>
    [Serializable]
    public class MountingProject : CommunicaBase ,IStationParent   //NotifyChangedBase,
    {
        /// <summary>
        /// 贴装参数
        /// </summary>
        public GlueModuleParams GluePram
        {
            get { return gluePram; }
            set { gluePram = value; OnPropertyChanged(); }
        }
        private GlueModuleParams gluePram = new GlueModuleParams();

        ///// <summary>
        ///// 安全点
        ///// </summary>
        public PositionInfor GlueSafetyPos
        {
            get { return glueSafetyPos; }
            set { glueSafetyPos = value; OnPropertyChanged(); }
        }
        private PositionInfor glueSafetyPos = new PositionInfor();

        /// <summary>
        /// 是否可编辑映射按钮
        /// </summary>
        public bool IsEnableEdit
        {
            get { return isEnableEdit; }
            set { isEnableEdit = value; OnPropertyChanged(); }
        }
        private bool isEnableEdit = false;


        /// <summary>
        /// 贴装执行参数
        /// </summary>
        [DisplayName("贴装参数")]
        public MountingMode MountingExcuteModel
        {
            get { return mountingExcuteModel; }
            set
            {
                mountingExcuteModel = value;
                OnPropertyChanged();
                if (value != null) value.ParentStation = this;
            }
        }
        private MountingMode mountingExcuteModel = new MountingMode();


        /// <summary>
        /// 贴装流程中点胶1执行参数
        /// </summary>
        [DisplayName("左点胶参数")]
        public GlueProject LeftGlue
        {
            get { return leftGlue; }
            set
            {
                leftGlue = value;
                OnPropertyChanged();
                if (value != null) value.ParentStation = this;
            }
        }
        private GlueProject leftGlue = new GlueProject();

        /// <summary>
        /// 贴装流程中点胶1执行参数
        /// </summary>
        [DisplayName("右点胶参数")]
        public GlueProject RightGlue
        {
            get { return rightGlue; }
            set
            {
                rightGlue = value;
                OnPropertyChanged();
                if (value != null) value.ParentStation = this;
            }
        }
        private GlueProject rightGlue = new GlueProject();


        /// <summary>
        /// 贴装流程中点胶1执行参数
        /// </summary>
        [DisplayName("初始化参数")]
        public StationTwoIntial InitParm
        {
            get { return initParm; }
            set
            {
                initParm = value;
                OnPropertyChanged();
                if (value != null) value.ParentStation = this;
            }
        }
        private StationTwoIntial initParm = new StationTwoIntial();

        /// <summary>
        /// 点胶模组设置
        /// </summary>
        public EnumWorkModule AxisWorkModule
        {
            get { return axisWorkModule; }
            set { axisWorkModule = value; OnPropertyChanged(); }
        }
        EnumWorkModule axisWorkModule = EnumWorkModule.S10;


        [XmlIgnore]
        public MountingModuleRunStatus StationStatus
        {
            get { return stationStatus; }
            set { stationStatus = value; OnPropertyChanged(); }
        }
        private MountingModuleRunStatus stationStatus = new MountingModuleRunStatus();

        /// <summary>
        /// 切换配方
        /// </summary>
        [DisplayName("切换配方")]
        public OpenFile OpenFile
        {
            get { return openFile; }
            set { openFile = value; OnPropertyChanged(); }
        }
        private OpenFile openFile = new OpenFile();
    }
}
