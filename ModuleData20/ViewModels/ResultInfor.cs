﻿using Googo.Manul;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ModuleData20
{

    /// <summary>
    /// 点胶生产数据
    /// </summary>
    public class ResultInfor : CommunicaBase
    {
        /// <summary>
        /// 生产时间
        /// </summary>
        public DateTime Time
        {
            get { return _time; }
            set { _time = value; OnPropertyChanged(); }
        }
        private DateTime _time = DateTime.Now;

        /// <summary>
        /// 产品SN
        /// </summary>
        [UpLoad]
        public string ProductSN
        {
            get { return productSN; }
            set { productSN = value; OnPropertyChanged(); }
        }
        private string productSN = string.Empty;

        /// <summary>
        /// 生产序号
        /// </summary>
        [UpLoad]
        public int ProcessIndex
        {
            get { return processIndex; }
            set { processIndex = value; OnPropertyChanged(); }
        }
        private int processIndex = 0;


        /// <summary>
        /// 光阑平面度
        /// </summary>
        [UpLoad]
        public double PiecePlatness
        {
            get { return piecePlatness; }
            set { piecePlatness = value; OnPropertyChanged(); }
        }
        private double piecePlatness = 0;

        /// <summary>
        /// pcb一次定位坐标
        /// </summary>
        public PositionInfor PcbFirstLocation
        {
            get { return pcbFirstLocation; }
            set { pcbFirstLocation = value; OnPropertyChanged(); }
        }
        private PositionInfor pcbFirstLocation = new PositionInfor();


        /// <summary>
        /// piece一次定位坐标
        /// </summary>
        public PositionInfor PieceFirstLocation
        {
            get { return pieceFirstLocation; }
            set { pieceFirstLocation = value; OnPropertyChanged(); }
        }
        private PositionInfor pieceFirstLocation = new PositionInfor();

        /// <summary>
        /// 二次定位偏移角度
        /// </summary>
        public double SecLocationAngle
        {
            get { return secLocationAngle; }
            set { secLocationAngle = value; OnPropertyChanged(); }
        }
        private double secLocationAngle = 0.0;


        /// <summary>
        /// 二次定位偏移XY
        /// </summary>
        public PositionInfor SecLocationXY
        {
            get { return secLocationXY; }
            set { secLocationXY = value; OnPropertyChanged();}
        }
        private PositionInfor secLocationXY = new PositionInfor();


        /// <summary>
        /// 复检结果
        /// </summary>
        public PositionInfor RecheckOffset
        {
            get { return recheckOffset; }
            set { recheckOffset = value; OnPropertyChanged();}
        }
        private PositionInfor recheckOffset = new PositionInfor();


        /// <summary>
        /// 复检右边结果
        /// </summary>
        public PositionInfor RecheckRightOffset
        {
            get { return recheckRightOffset; }
            set { recheckRightOffset = value; OnPropertyChanged(); }
        }
        private PositionInfor recheckRightOffset = new PositionInfor();
        

         /// <summary>
         /// 测量高度列表
         /// </summary>
         /// <remarks>
         /// 位移传感器测量出来的值
         /// </remarks>
         [UpLoad]
        public List<float> ReckeckHeight
        {
            get { return reckeckHeight; }
            set { reckeckHeight = value; OnPropertyChanged(); }
        }
        private List<float> reckeckHeight = new List<float>();


        /// <summary>
        /// 二次固化时间
        /// </summary>
        /// <remarks>
        /// 外部需要用测获取
        /// </remarks>
        [Browsable(false)]
        [XmlIgnore]
        public float SecUVTime
        {
            get { return secUVTime; }
            set { secUVTime = value; OnPropertyChanged(); }
        }
        float secUVTime = 0;


        /// <summary>
        /// 二次固化强度
        /// </summary>
        /// <remarks>
        /// 外部需要用测获取
        /// </remarks>
        [Browsable(false)]
        [XmlIgnore]
        public float SecUVPower
        {
            get { return secUVPower; }
            set { secUVPower = value; OnPropertyChanged(); }
        }
        float secUVPower = 0;

        /// <summary>
        /// 力控电流值
        /// </summary>
        /// <remarks>
        /// 外部需要用测获取
        /// </remarks>
        [Browsable(false)]
        [XmlIgnore]
        public float ForceControlValue
        {
            get { return forceControlValue; }
            set { forceControlValue = value; OnPropertyChanged(); }
        }
        float forceControlValue = 0;


        /// <summary>
        /// 力控所有数据
        /// </summary>
        /// <remarks>
        /// 外部需要用测获取
        /// </remarks>
        [Browsable(false)]
        [XmlIgnore]
        public string ForceControlValuePath
        {
            get { return forceControlValuePath; }
            set { forceControlValuePath = value; OnPropertyChanged(); }
        }
        string forceControlValuePath = string.Empty;

        /// <summary>
        /// 贴装总结果
        /// </summary>
        /// <remarks>
        /// 外部需要用测获取
        /// </remarks>
        [Browsable(false)]
        [XmlIgnore]
        public string MountingResult
        {
            get { return mountingResult; }
            set { mountingResult = value; OnPropertyChanged(); }
        }
        string mountingResult = string.Empty;

        /// <summary>
        /// 贴装复检视觉图像路径
        /// </summary>
        /// <remarks>
        /// 外部需要用测获取
        /// </remarks>
        [Browsable(false)]
        [XmlIgnore]
        public string MountingImagePath
        {
            get { return mountingImagePath; }
            set { mountingImagePath = value; OnPropertyChanged(); }
        }
        string mountingImagePath = string.Empty;


        [Browsable(false)]
        [XmlIgnore]
        [UpLoad]
        public short AlarmCode
        {
            get { return alarmCode; }
            set { alarmCode = value; OnPropertyChanged(); }
        }
        private short alarmCode = 0;

        [UpLoad]
        public string AlgImgPath
        {
            get { return algImgPath; }
            set { algImgPath = value; OnPropertyChanged(); }
        }
        private string algImgPath = string.Empty;

        public string ResultDetail
        {
            get { return resultDetail; }
            set { resultDetail = value; OnPropertyChanged(); }
        }
        private string resultDetail = string.Empty;

        /// <summary>
        /// 产品NG代码
        /// </summary>
        [UpLoad, XmlIgnore, DisplayName("产品NG代码")]
        public string ProNGCode
        {
            get { return proNGCode; }
            set { proNGCode = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        private string proNGCode = string.Empty;
    }
}
