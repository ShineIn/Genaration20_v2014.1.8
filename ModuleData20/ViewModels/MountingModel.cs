﻿using G4.GlueCore;
using Googo.Manul;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using ModuleData;
using System.Reflection;
using System.Windows;
using ModuleData20.Views;
using System.Drawing;
using System.Windows.Media.Imaging;

namespace ModuleData20.ViewModels
{
    public class MPositionAndCommand : NotifyChangedBase
    {


        public string StrCommand
        {
            get { return strCommand; }
            set { strCommand = value; OnPropertyChanged(); }
        }
        private string strCommand = "Mounting";
    }

    /// <summary>
    /// 贴装执行参数
    /// </summary>
    [Serializable]
    public class MountingMode : CommunicaBase, IStationModule
    {
        #region 公有属性

        /// <summary>
        /// 贴装YZZ坐标
        /// </summary>
        public ObservableCollection<PositionInfor> MountingCamPointsOrder
        {
            get { return mountingCamPointsOrder; }
            set { mountingCamPointsOrder = value; OnPropertyChanged(); }
        }
        private ObservableCollection<PositionInfor> mountingCamPointsOrder = new ObservableCollection<PositionInfor>();




        /// <summary>
        /// 贴装R1R2R3坐标
        /// </summary>
        public ObservableCollection<PositionInfor> MircPlatformCamPointsOrder
        {
            get { return mircPlatformCamPointsOrder; }
            set { mircPlatformCamPointsOrder = value; OnPropertyChanged(); }
        }
        private ObservableCollection<PositionInfor> mircPlatformCamPointsOrder = new ObservableCollection<PositionInfor>();


        /// <summary>
        /// 公共点位表（针对贴装流程中用点胶1模组轴的点位）
        /// </summary>
        public ObservableCollection<PositionInfor> PublicCamPointsOrder
        {
            get { return publicCamPointsOrder; }
            set { publicCamPointsOrder = value; OnPropertyChanged(); }
        }
        private ObservableCollection<PositionInfor> publicCamPointsOrder = new ObservableCollection<PositionInfor>();


        /// <summary>
        /// 点胶模组1位置
        /// </summary>
        public ObservableCollection<PositionInfor> MountingGlue1Points
        {
            get { return mountingGlue1Points; }
            set { mountingGlue1Points = value; OnPropertyChanged(); }
        }
        private ObservableCollection<PositionInfor> mountingGlue1Points = new ObservableCollection<PositionInfor>();

        /// <summary>
        /// 单次装结果
        /// </summary>
        [XmlIgnore]
        public ResultInfor MountingResult
        {
            get { return mountingResult; }
            set { mountingResult = value; OnPropertyChanged(); }
        }
        private ResultInfor mountingResult = new ResultInfor();
        /// <summary>
        /// 贴装结果
        /// </summary>
        [XmlIgnore]
        public ObservableCollection<ResultInfor> MountingResults
        {
            get { return mountingResults; }
            set { mountingResults = value; OnPropertyChanged(); }
        }
        ObservableCollection<ResultInfor> mountingResults = new ObservableCollection<ResultInfor>();


        /// <summary>
        /// 力控数据
        /// </summary>
        [XmlIgnore]
        public List<CurvesParm> ForceControlData
        {
            get { return forceControlData; }
            set { forceControlData = value; OnPropertyChanged(); }
        }
        List<CurvesParm> forceControlData = new List<CurvesParm>();

        /// <summary>
        /// 力控数据是否保存曲线
        /// </summary>
        [XmlIgnore]
        public string ForceSaveImagePath
        {
            get { return forceSaveImagePath; }
            set { forceSaveImagePath = value; OnPropertyChanged(); }
        }
        string forceSaveImagePath = string.Empty;


        /// <summary>
        /// 任务号
        /// </summary>
        public string Signal_R_RXProgram
        {
            get { return signal_R_RXProgram; }
            set { signal_R_RXProgram = value; OnPropertyChanged(); }
        }
        string signal_R_RXProgram = "DB3000.DBX2.0";

        /// <summary>
        /// 贴装OK结果
        /// </summary>
        public string Signal_W_RXMountingFinishedOK
        {
            get { return signal_W_RXMountingFinishedOK; }
            set { signal_W_RXMountingFinishedOK = value; OnPropertyChanged(); }
        }
        string signal_W_RXMountingFinishedOK = "DB3001.DBX2.2";

        /// <summary>
        /// 贴装NG结果
        /// </summary>
        public string Signal_W_RXMountingFinishedNG
        {
            get { return signal_W_RXMountingFinishedNG; }
            set { signal_W_RXMountingFinishedNG = value; OnPropertyChanged(); }
        }
        string signal_W_RXMountingFinishedNG = "DB3001.DBX2.3";

        /// <summary>
        /// 贴装中
        /// </summary>
        public string Signal_W_RXReceiveProgram
        {
            get { return signal_W_RXReceiveProgram; }
            set { signal_W_RXReceiveProgram = value; OnPropertyChanged(); }
        }
        string signal_W_RXReceiveProgram = "DB3001.DBX2.0";

        /// <summary>
        /// 贴装完成
        /// </summary>
        public string Signal_W_RXMountingFinished
        {
            get { return signal_W_RXMountingFinished; }
            set { signal_W_RXMountingFinished = value; OnPropertyChanged(); }
        }
        string signal_W_RXMountingFinished = "DB3001.DBX2.1";


        /// <summary>
        /// 任务号
        /// </summary>
        public string Signal_R_TXProgram
        {
            get { return signal_R_TXProgram; }
            set { signal_R_TXProgram = value; OnPropertyChanged(); }
        }
        string signal_R_TXProgram = "DB3000.DBX2.4";

        /// <summary>
        /// 贴装OK结果
        /// </summary>
        public string Signal_W_TXMountingFinishedOK
        {
            get { return signal_W_TXMountingFinishedOK; }
            set { signal_W_TXMountingFinishedOK = value; OnPropertyChanged(); }
        }
        string signal_W_TXMountingFinishedOK = "DB3001.DBX2.6";

        /// <summary>
        /// 贴装NG结果
        /// </summary>
        public string Signal_W_TXMountingFinishedNG
        {
            get { return signal_W_TXMountingFinishedNG; }
            set { signal_W_TXMountingFinishedNG = value; OnPropertyChanged(); }
        }
        string signal_W_TXMountingFinishedNG = "DB3001.DBX2.4";

        /// <summary>
        /// 贴装中
        /// </summary>
        public string Signal_W_TXReceiveProgram
        {
            get { return signal_W_TXReceiveProgram; }
            set { signal_W_TXReceiveProgram = value; OnPropertyChanged(); }
        }
        string signal_W_TXReceiveProgram = "DB3001.DBX2.4";

        /// <summary>
        /// 贴装完成
        /// </summary>
        public string Signal_W_TXMountingFinished
        {
            get { return signal_W_TXMountingFinished; }
            set { signal_W_TXMountingFinished = value; OnPropertyChanged(); }
        }
        string signal_W_TXMountingFinished = "DB3001.DBX2.1";


        /// <summary>
        /// Y3速度
        /// </summary>
        public double Y3Speed
        {
            get { return y3Speed; }
            set { y3Speed = value; OnPropertyChanged(); }
        }
        double y3Speed = 50;

        /// <summary>
        /// Y3加减速度
        /// </summary>
        public double Y3AccSpeed
        {
            get { return y3AccSpeed; }
            set { y3AccSpeed = value; OnPropertyChanged(); }
        }
        double y3AccSpeed = 100;

        /// <summary>
        /// Z3速度
        /// </summary>
        public double Z3Speed
        {
            get { return z3Speed; }
            set { z3Speed = value; OnPropertyChanged(); }
        }
        double z3Speed = 50;

        /// <summary>
        /// Z3加减速度
        /// </summary>
        public double Z3AccSpeed
        {
            get { return z3AccSpeed; }
            set { z3AccSpeed = value; OnPropertyChanged(); }
        }
        double z3AccSpeed = 100;

        /// <summary>
        /// Z4速度
        /// </summary>
        public double Z4Speed
        {
            get { return z4Speed; }
            set { z4Speed = value; OnPropertyChanged(); }
        }
        double z4Speed = 50;

        /// <summary>
        /// R1加减速度
        /// </summary>
        public double Z4AccSpeed
        {
            get { return z4AccSpeed; }
            set { z4AccSpeed = value; OnPropertyChanged(); }
        }
        double z4AccSpeed = 100;


        /// <summary>
        /// R1速度
        /// </summary>
        public double R1Speed
        {
            get { return r1Speed; }
            set { r1Speed = value; OnPropertyChanged(); }
        }
        double r1Speed = 50;

        /// <summary>
        /// R1加减速度
        /// </summary>
        public double R1AccSpeed
        {
            get { return r1AccSpeed; }
            set { r1AccSpeed = value; OnPropertyChanged(); }
        }
        double r1AccSpeed = 100;

        /// <summary>
        /// R2速度
        /// </summary>
        public double R2Speed
        {
            get { return r2Speed; }
            set { r2Speed = value; OnPropertyChanged(); }
        }
        double r2Speed = 50;

        /// <summary>
        /// R2加减速度
        /// </summary>
        public double R2AccSpeed
        {
            get { return r2AccSpeed; }
            set { r2AccSpeed = value; OnPropertyChanged(); }
        }
        double r2AccSpeed = 100;

        /// <summary>
        /// R3速度
        /// </summary>
        public double R3Speed
        {
            get { return r3Speed; }
            set { r3Speed = value; OnPropertyChanged(); }
        }
        double r3Speed = 50;

        /// <summary>
        /// R3加减速度
        /// </summary>
        public double R3AccSpeed
        {
            get { return r3AccSpeed; }
            set { r3AccSpeed = value; OnPropertyChanged(); }
        }
        double r3AccSpeed = 100;


        /// <summary>
        /// 配置文件路径
        /// </summary>
        public string FilePath
        {
            get { return filePath; }
            set { filePath = value; OnPropertyChanged(); /*UpDatePos(value)*/; }
        }
        string filePath = "";

        [XmlIgnore, UpLoad]
        /// <summary>
        /// 光阑平面度
        /// </summary>
        public float PiecePlatness
        {
            get { return piecePlatness; }
            set
            {
                piecePlatness = value;
                //UpLoadPro(value);
                OnPropertyChanged();
                IsPiecePlatnessNg = piecePlatness > PiecePlatnessSpc ? true : false;
            }
        }
        float piecePlatness = 0.0f;

        [XmlIgnore, DownLoad]
        /// <summary>
        /// 光阑平面度阈值
        /// </summary>
        public float PiecePlatnessSpc
        {
            get
            {
                //object obj = DownLoadPro();
                //if (obj == null || !(obj is float)) return 0;
                //piecePlatnessSpc = (float)obj;
                return piecePlatnessSpc;
            }
            set { piecePlatnessSpc = value; OnPropertyChanged(); }
        }
        float piecePlatnessSpc = 5;

        /// <summary>
        /// 视觉NG调整次数
        /// </summary>
        public int VisNgCount
        {
            get { return visNgCount; }
            set { visNgCount = value; OnPropertyChanged(); }
        }
        int visNgCount = 5;

        [XmlIgnore]
        /// <summary>
        /// pcb一次定位坐标
        /// </summary>
        public PositionInfor PcbFirstLocation
        {
            get { return pcbFirstLocation; }
            set { pcbFirstLocation = value; OnPropertyChanged(); }
        }
        PositionInfor pcbFirstLocation = new PositionInfor();

        [XmlIgnore]
        /// <summary>
        /// piece一次定位坐标
        /// </summary>
        public PositionInfor PieceFirstLocation
        {
            get { return pieceFirstLocation; }
            set { pieceFirstLocation = value; OnPropertyChanged(); }
        }
        PositionInfor pieceFirstLocation = new PositionInfor();








        [XmlIgnore]
        /// <summary>
        /// 复检结果
        /// </summary>
        public PositionInfor RecheckOffset
        {
            get { return recheckOffset; }
            set { recheckOffset = value; OnPropertyChanged(); IsRecheckOffsetNg = (RecheckOffset.XPos > RecheckOffsetSpec.XPos) || (RecheckOffset.YPos > RecheckOffsetSpec.YPos) ? true : false; }
        }
        PositionInfor recheckOffset = new PositionInfor();

        /// <summary>
        /// 复检结果上下限
        /// </summary>
        public PositionInfor RecheckOffsetSpec
        {
            get { return recheckOffsetSpec; }
            set { recheckOffsetSpec = value; OnPropertyChanged(); }
        }
        PositionInfor recheckOffsetSpec = new PositionInfor();

        /// <summary>
        /// 光阑与PCB段差上限
        /// </summary>
        //[DownLoad]
        public float PiecePcbHeightHSpec
        {
            get
            {
                //object obj = DownLoadPro();
                //if (obj == null || !(obj is double)) return 0;
                //piecePcbHeightHSpec = (double)obj;
                return piecePcbHeightHSpec;
            }
            set { piecePcbHeightHSpec = value; OnPropertyChanged(); }
        }
        float piecePcbHeightHSpec = 2;

        /// <summary>
        /// 光阑与PCB段差下限
        /// </summary>
        public float PiecePcbHeightLSpec
        {
            get { return piecePcbHeightLSpec; }
            set { piecePcbHeightLSpec = value; OnPropertyChanged(); }
        }
        float piecePcbHeightLSpec = 2;

        /// <summary>
        /// 光阑与外壳段差上限
        /// </summary>
        public float PieceHousingHeightHSpec
        {
            get { return pieceHousingHeightHSpec; }
            set { pieceHousingHeightHSpec = value; OnPropertyChanged(); }
        }
        float pieceHousingHeightHSpec = 2;

        /// <summary>
        /// 光阑与外壳段差下限
        /// </summary>
        public float PieceHousingHeightLSpec
        {
            get { return pieceHousingHeightLSpec; }
            set { pieceHousingHeightLSpec = value; OnPropertyChanged(); }
        }
        float pieceHousingHeightLSpec = 2;


        /// <summary>
        /// UV二次固化时间
        /// </summary>
        public long UVSecTime
        {
            get { return uVSecTime; }
            set { uVSecTime = value; OnPropertyChanged(); }
        }
        long uVSecTime = 40000;

        /// <summary>
        /// UV二次固化位置
        /// </summary>
        public PositionInfor SecUVPos
        {
            get { return secUVPos; }
            set { secUVPos = value; OnPropertyChanged(); }
        }
        PositionInfor secUVPos = new PositionInfor();

        /// <summary>
        /// 是否启用软着陆
        /// </summary>
        public bool IsUseSoftLand
        {
            get { return isUseSoftLand; }
            set { isUseSoftLand = value; OnPropertyChanged(); }
        }
        bool isUseSoftLand = false;


        /// <summary>
        /// 光阑结果是否NG
        /// </summary>
        //[UpLoad]
        public bool IsPiecePlatnessNg
        {
            get { return isPiecePlatnessNg; }
            set { isPiecePlatnessNg = value;/*UpLoadPro(value)*/; OnPropertyChanged(); }
        }
        bool isPiecePlatnessNg = false;

        /// <summary>
        /// 光阑二次定位角度结果是否NG
        /// </summary>
        public bool IsSecLocationAngleNg
        {
            get { return isSecLocationAngleNg; }
            set { isSecLocationAngleNg = value; OnPropertyChanged(); }
        }
        bool isSecLocationAngleNg = false;

        /// <summary>
        /// 光阑二次定位XY结果是否NG
        /// </summary>
        public bool IsSecLocationXYNg
        {
            get { return isSecLocationXYNg; }
            set
            {
                isSecLocationXYNg = value;
                OnPropertyChanged();

            }
        }
        bool isSecLocationXYNg = false;


        /// <summary>
        /// 复检结果是否NG
        /// </summary>
        public bool IsRecheckOffsetNg
        {
            get { return isRecheckOffsetNg; }
            set { isRecheckOffsetNg = value; OnPropertyChanged(); }
        }
        bool isRecheckOffsetNg = true;

        /// <summary>
        /// 光阑与PCB测高
        /// </summary>
        public List<string> PiecePcbHeightList
        {
            get { return piecePcbHeightList; }
            set
            {
                piecePcbHeightList = value;
                OnPropertyChanged();
                IsPiecePcbHeightNg = false;
                foreach (var item in piecePcbHeightList)
                {
                    if (Convert.ToDouble(item) > PiecePcbHeightHSpec || Convert.ToDouble(item) < PiecePcbHeightLSpec)
                    {
                        IsPiecePcbHeightNg = true;
                    }
                }
            }
        }
        List<string> piecePcbHeightList = new List<string>();

        /// <summary>
        /// 光阑与PCB测高是否NG
        /// </summary>
        public bool IsPiecePcbHeightNg
        {
            get { return isPiecePcbHeightNg; }
            set { isPiecePcbHeightNg = value; OnPropertyChanged(); }
        }
        bool isPiecePcbHeightNg = false;

        /// <summary>
        /// 光阑与Hosuing测高
        /// </summary>
        public List<string> PieceHousingHeightList
        {
            get { return piecePcbHeightList; }
            set
            {
                pieceHousingHeightList = value;
                OnPropertyChanged();
                IspieceHousingHeightNg = false;
                foreach (var item in pieceHousingHeightList)
                {
                    if (Convert.ToDouble(item) > PieceHousingHeightHSpec || Convert.ToDouble(item) < PieceHousingHeightLSpec)
                    {
                        IspieceHousingHeightNg = true;
                    }
                }
            }
        }
        List<string> pieceHousingHeightList = new List<string>();

        /// <summary>
        /// 光阑与Housing测高是否NG
        /// </summary>
        public bool IspieceHousingHeightNg
        {
            get { return ispieceHousingHeightNg; }
            set { ispieceHousingHeightNg = value; OnPropertyChanged(); }
        }
        bool ispieceHousingHeightNg = false;
        #endregion 公有属性


        [XmlIgnore]
        public IStationParent ParentStation { get; set; }



        private void UpDatePos(string filePath)
        {
            string basePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\");
            MountingCamPointsOrder = ReadPosCommand(basePath + filePath + "_YZZ" + ".csv");
            MircPlatformCamPointsOrder = ReadPosCommand(basePath + filePath + "_MircPlatform" + ".csv");
        }


        private ObservableCollection<PositionInfor> ReadPosCommand(string filePath)
        {
            string[] lines = File.ReadAllLines(filePath, Encoding.GetEncoding(936));
            ObservableCollection<PositionInfor> positionInfors = new ObservableCollection<PositionInfor>();
            if (lines == null) return new ObservableCollection<PositionInfor>();
            int j = 0;
            foreach (string line in lines)
            {
                j++;
                if (string.IsNullOrEmpty(line) || j == 1) continue;
                string[] arry = line.Split(',');
                if (arry == null || arry.Length < 6) continue;
                PositionInfor position = new PositionInfor { Name = arry[0], StrCommand = arry[arry.Length - 2], Descri = arry[arry.Length - 1] };
                double dvalue = 0;
                for (int i = 1; i < 4; i++)
                {
                    if (string.IsNullOrEmpty(arry[i]) || !double.TryParse(arry[i], out dvalue)) continue;
                    if (i == 1) position.XPos = dvalue;
                    else if (i == 2) position.YPos = dvalue;
                    else if (i == 3) position.ZPos = dvalue;
                }
                positionInfors.Add(position);
            }
            return positionInfors;
        }


        #region 界面显示参数
        [XmlIgnore]
        /// <summary>
        /// 贴装任务号
        /// </summary>
        public string MountingTask
        {
            get { return mountingTask; }
            set { mountingTask = value; OnPropertyChanged(); }
        }
        string mountingTask = "RX";

        [XmlIgnore]
        /// <summary>
        /// 贴装引导任务
        /// </summary>
        public string MountingVisLocTask
        {
            get { return mountingVisLocTask; }
            set { mountingVisLocTask = value; OnPropertyChanged(); }
        }
        string mountingVisLocTask = "";

        [XmlIgnore]
        /// <summary>
        /// 贴装复检任务
        /// </summary>
        public string MountingVisRCKTask
        {
            get { return mountingVisRCKTask; }
            set { mountingVisRCKTask = value; OnPropertyChanged(); }
        }
        string mountingVisRCKTask = "";

        [XmlIgnore]
        /// <summary>
        /// 引导角度次数
        /// </summary>
        public int LocationAngleCount
        {
            get { return locationAngleCount; }
            set
            {
                locationAngleCount = value;
                OnPropertyChanged();
                IsSecLocACountNg = LocationAngleCount > LocAngleCountLmint ? true : false;
            }
        }
        int locationAngleCount = 0;

        [XmlIgnore]
        /// <summary>
        /// 引导角度次数上限
        /// </summary>
        public int LocAngleCountLmint
        {
            get { return locAngleCountLmint; }
            set { locAngleCountLmint = value; OnPropertyChanged(); }
        }
        int locAngleCountLmint = 0;

        [XmlIgnore]
        /// <summary>
        /// 引导XY次数
        /// </summary>
        public int LocationXYCount
        {
            get { return locationXYCount; }
            set
            {
                locationXYCount = value;
                OnPropertyChanged();
                IsSecLocXYCountNg = LocationXYCount > LocXYCountLmint ? true : false;
            }
        }
        int locationXYCount = 0;

        /// <summary>
        /// 引导角度次数上限
        /// </summary>
        public int LocXYCountLmint
        {
            get { return locXYCountLmint; }
            set { locXYCountLmint = value; OnPropertyChanged(); }
        }
        int locXYCountLmint = 0;





        /// <summary>
        /// 光阑测高上下限 0下限 1上限
        /// </summary>
        /// <remarks>
        /// 位移传感器测量出来的值
        /// </remarks>

        public List<float> PieceHeightSpec
        {
            get
            {
                return pieceHeightSpec;
            }
            set
            {
                pieceHeightSpec = value; 
                OnPropertyChanged(); 
            }
        }
        List<float> pieceHeightSpec = new List<float>();

        [XmlIgnore]
        /// <summary>
        /// 测量高度列表
        /// </summary>
        /// <remarks>
        /// 位移传感器测量出来的值
        /// </remarks>

        public List<float> ReckeckHeight
        {
            get { return reckeckHeight; }
            set
            {
                reckeckHeight = value;
                OnPropertyChanged();
                if (ReckeckHeight.Count > 0) StopHeightcheck1 = ReckeckHeight[0];
                if (ReckeckHeight.Count > 1) StopHeightcheck2 = ReckeckHeight[1];
                if (ReckeckHeight.Count > 2) StopHeightcheck3 = ReckeckHeight[2];
                if (ReckeckHeight.Count > 3) StopHeightcheck4 = ReckeckHeight[3];
                if (ReckeckHeightSpec.Count > 1)
                {
                    foreach (var height in ReckeckHeight)
                    {
                        if (height < ReckeckHeightSpec[0] || height > ReckeckHeightSpec[1])
                        {
                            IsReckeckHeightNg = true;
                            return;
                        }
                        else { IsReckeckHeightNg = false; }
                    }
                }
            }
        }
        List<float> reckeckHeight = new List<float>();


        /// <summary>
        /// 光阑pcb高度检测Spec 0下限 1上限
        /// </summary>
        /// <remarks>
        /// 位移传感器测量出来的值
        /// </remarks>

        public List<float> ReckeckHeightSpec
        {
            get { return reckeckHeightSpec; }
            set { reckeckHeightSpec = value; OnPropertyChanged(); }
        }
        List<float> reckeckHeightSpec = new List<float>();

        /// <summary>
        /// 光阑定位角度次数NG
        /// </summary>
        public bool IsSecLocACountNg
        {
            get { return isSecLocACountNg; }
            set { isSecLocACountNg = value; OnPropertyChanged(); }
        }
        bool isSecLocACountNg = false;

        /// <summary>
        /// 光阑定位XY次数NG
        /// </summary>
        public bool IsSecLocXYCountNg
        {
            get { return isSecLocXYCountNg; }
            set { isSecLocXYCountNg = value; OnPropertyChanged(); }
        }
        bool isSecLocXYCountNg = false;

        /// <summary>
        /// 光阑测高NG
        /// </summary>
        public bool IsPieceHeightNg
        {
            get { return isPieceHeightNg; }
            set { isPieceHeightNg = value; OnPropertyChanged(); }
        }
        bool isPieceHeightNg = false;

        /// <summary>
        /// 光阑pcb段差测高NG
        /// </summary>
        public bool IsReckeckHeightNg
        {
            get { return isReckeckHeightNg; }
            set { isReckeckHeightNg = value; OnPropertyChanged(); }
        }
        bool isReckeckHeightNg = false;

        [DownLoad]
        /// <summary>
        /// 二次定位偏移角度Spec
        /// </summary>
        public float SecLocationAngleSpec
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return 0;
                secLocationAngleSpec = (float)obj;
                return /*secLocationAngleSpec*/0.005f;
            }
            set { secLocationAngleSpec = value; OnPropertyChanged(); }
        }
        float secLocationAngleSpec = 0.0f;

        [XmlIgnore, UpLoad]
        /// <summary>
        /// 二次定位偏移角度
        /// </summary>
        public float SecLocationAngle
        {
            get { return secLocationAngle; }
            set { secLocationAngle = value; UpLoadPro(value); OnPropertyChanged(); IsSecLocationAngleNg = SecLocationAngle > SecLocationAngleSpec ? true : false; }
        }
        float secLocationAngle = 0.0f;

        [XmlIgnore, UpLoad]
        /// <summary>
        /// 二次定位偏移XY
        /// </summary>
        public PositionInfor SecLocationXY
        {
            get { return secLocationXY; }
            set { secLocationXY = value; UpLoadPro(value); OnPropertyChanged(); IsSecLocationXYNg = (SecLocationXY.XPos > SecLocationXYSpec.XPos) || (SecLocationXY.YPos > SecLocationXYSpec.YPos) ? true : false; }
        }
        PositionInfor secLocationXY = new PositionInfor();


        /// <summary>
        /// 二次定位偏移XYSpec
        /// </summary>
        public PositionInfor SecLocationXYSpec
        {
            get
            {
                return secLocationXYSpec;
            }
            set { secLocationXYSpec = value; OnPropertyChanged(); }
        }
        PositionInfor secLocationXYSpec = new PositionInfor();

        [XmlIgnore]
        /// <summary>
        /// 光阑测高
        /// </summary>
        /// <remarks>
        /// 位移传感器测量出来的值
        /// </remarks>

        public List<float> PieceHeight
        {
            get { return pieceHeight; }
            set
            {
                pieceHeight = value;
                OnPropertyChanged();
                if (PieceHeightSpec.Count > 1)
                {
                    if (pieceHeight.Count > 0) Heightcheck1 = pieceHeight[0];
                    if (pieceHeight.Count > 1) Heightcheck2 = pieceHeight[1];
                    if (pieceHeight.Count > 2) Heightcheck3 = pieceHeight[2];
                    for (int i = 0; i < pieceHeight.Count; i++)
                    {
                        if (pieceHeight[i] < PieceHeightSpec[0] || pieceHeight[i] > PieceHeightSpec[1])
                        {
                            IsPieceHeightNg = true;
                            return;
                        }
                        else { IsPieceHeightNg = false; }
                    }
                }
            }
        }
        List<float> pieceHeight = new List<float>();
        #endregion


        #region MES 输入

        [DownLoad, DisplayName("PCBA二维码"),XmlIgnore]
        /// <summary>
        /// PCBA二维码
        /// </summary>
        public string PCBA_ID
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is string)) return pCBA_ID;
                pCBA_ID = (string)obj;
                return pCBA_ID;
            }
            set { pCBA_ID = value; OnPropertyChanged(); }
        }
        string pCBA_ID = string.Empty;

        [DownLoad, DisplayName("料号"),XmlIgnore]
        /// <summary>
        /// 料号
        /// </summary>
        public string RXorTXPartNo
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is string)) return rXorTXPartNo;
                rXorTXPartNo = (string)obj;
                return rXorTXPartNo;
            }
            set { rXorTXPartNo = value; OnPropertyChanged(); }
        }
        string rXorTXPartNo = string.Empty;


        [DownLoad, DisplayName("总任务程序号"), XmlIgnore]
        /// <summary>
        /// 总任务程序号
        /// </summary>
        public string PrgNo
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is string)) return prgNo;
                prgNo = (string)obj;
                return prgNo;
            }
            set { prgNo = value; OnPropertyChanged(); }
        }
        string prgNo = string.Empty;


    

        [DownLoad, DisplayName("高度传感器校验上限"), XmlIgnore]
        /// <summary>
        /// 高度传感器校验上限
        /// </summary>
        public float HeightSensorCalibrationUpperLimit
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return heightSensorCalibrationUpperLimit;
                heightSensorCalibrationUpperLimit = (float)obj;
                return heightSensorCalibrationUpperLimit;
            }
            set { heightSensorCalibrationUpperLimit = value; OnPropertyChanged(); }
        }
        float heightSensorCalibrationUpperLimit = 0;

        [DownLoad, DisplayName("高度传感器校验下限"), XmlIgnore]
        /// <summary>
        /// 高度传感器校验下限
        /// </summary>
        public float HeightSensorCalibrationLowerLimit
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return heightSensorCalibrationLowerLimit;
                heightSensorCalibrationLowerLimit = (float)obj;
                return heightSensorCalibrationLowerLimit;
            }
            set { heightSensorCalibrationLowerLimit = value; OnPropertyChanged(); }
        }
        float heightSensorCalibrationLowerLimit = 0;

        [DownLoad, DisplayName("相机校验位置设定坐标X"), XmlIgnore]
        /// <summary>
        /// 相机校验位置设定坐标X
        /// </summary>
        public float CCDCalibrationSettingPosX
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return cCDCalibrationSettingPosX;
                cCDCalibrationSettingPosX = (float)obj;
                return cCDCalibrationSettingPosX;
            }
            set { cCDCalibrationSettingPosX = value; OnPropertyChanged(); }
        }
        float cCDCalibrationSettingPosX = 0;

        [DownLoad, DisplayName("相机校验位置设定坐标Y"), XmlIgnore]
        /// <summary>
        /// 相机校验位置设定坐标Y
        /// </summary>
        public float CCDCalibrationSettingPosY
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return cCDCalibrationSettingPosY;
                cCDCalibrationSettingPosY = (float)obj;
                return cCDCalibrationSettingPosY;
            }
            set { cCDCalibrationSettingPosY = value; OnPropertyChanged(); }
        }
        float cCDCalibrationSettingPosY = 0;

        [DownLoad, DisplayName("CCD校验程序号"), XmlIgnore]
        /// <summary>
        /// CCD校验程序号
        /// </summary>
        public float CCDCalibrationPrgNo
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return cCDCalibrationPrgNo;
                cCDCalibrationPrgNo = (float)obj;
                return cCDCalibrationPrgNo;
            }
            set { cCDCalibrationPrgNo = value; OnPropertyChanged(); }
        }
        float cCDCalibrationPrgNo = 0;


        [DownLoad, DisplayName("光阑视觉引导的可接受的偏差量X"), XmlIgnore]
        /// <summary>
        /// 光阑视觉引导的可接受的偏差量X
        /// </summary>
        public float STOPCCDGuideActPosAcceptDeviationX
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return sTOPCCDGuideActPosAcceptDeviationX;
                sTOPCCDGuideActPosAcceptDeviationX = (float)obj;
                return sTOPCCDGuideActPosAcceptDeviationX;
            }
            set { sTOPCCDGuideActPosAcceptDeviationX = value; OnPropertyChanged(); }
        }
        float sTOPCCDGuideActPosAcceptDeviationX = 0;

        [DownLoad, DisplayName("光阑视觉引导的可接受的偏差量Y"), XmlIgnore]
        /// <summary>
        /// 光阑视觉引导的可接受的偏差量Y
        /// </summary>
        public float STOPCCDGuideActPosAcceptDeviationY
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return sTOPCCDGuideActPosAcceptDeviationY;
                sTOPCCDGuideActPosAcceptDeviationY = (float)obj;
                return sTOPCCDGuideActPosAcceptDeviationY;
            }
            set { sTOPCCDGuideActPosAcceptDeviationY = value; OnPropertyChanged(); }
        }
        float sTOPCCDGuideActPosAcceptDeviationY = 0;

        [DownLoad, DisplayName("光阑视觉引导的可接受的偏差量R"), XmlIgnore]
        /// <summary>
        /// 光阑视觉引导的可接受的偏差量R
        /// </summary>
        public float STOPCCDGuideActPosAcceptDeviationR
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return sTOPCCDGuideActPosAcceptDeviationR;
                sTOPCCDGuideActPosAcceptDeviationR = (float)obj;
                return sTOPCCDGuideActPosAcceptDeviationR;
            }
            set { sTOPCCDGuideActPosAcceptDeviationR = value; OnPropertyChanged(); }
        }
        float sTOPCCDGuideActPosAcceptDeviationR = 0;

        [DownLoad, DisplayName("RFB视觉引导的可接受的偏差量X"), XmlIgnore]
        /// <summary>
        /// RFB视觉引导的可接受的偏差量X
        /// </summary>
        public float RFBCCDGuideActPosAcceptDeviationX
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return rFBCCDGuideActPosAcceptDeviationX;
                rFBCCDGuideActPosAcceptDeviationX = (float)obj;
                return rFBCCDGuideActPosAcceptDeviationX;
            }
            set { rFBCCDGuideActPosAcceptDeviationX = value; OnPropertyChanged(); }
        }
        float rFBCCDGuideActPosAcceptDeviationX = 0;


        [DownLoad, DisplayName("RFB视觉引导的可接受的偏差量Y"), XmlIgnore]
        /// <summary>
        /// RFB视觉引导的可接受的偏差量Y
        /// </summary>
        public float RFBCCDGuideActPosAcceptDeviationY
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return rFBCCDGuideActPosAcceptDeviationY;
                rFBCCDGuideActPosAcceptDeviationY = (float)obj;
                return rFBCCDGuideActPosAcceptDeviationY;
            }
            set { rFBCCDGuideActPosAcceptDeviationY = value; OnPropertyChanged(); }
        }
        float rFBCCDGuideActPosAcceptDeviationY = 0;

        [DownLoad, DisplayName("RFB视觉引导的可接受的偏差量R"), XmlIgnore]
        /// <summary>
        /// RFB视觉引导的可接受的偏差量R
        /// </summary>
        public float RFBCCDGuideActPosAcceptDeviationR
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return rFBCCDGuideActPosAcceptDeviationR;
                rFBCCDGuideActPosAcceptDeviationR = (float)obj;
                return rFBCCDGuideActPosAcceptDeviationR;
            }
            set { rFBCCDGuideActPosAcceptDeviationR = value; OnPropertyChanged(); }
        }
        float rFBCCDGuideActPosAcceptDeviationR = 0;


        [DownLoad, DisplayName("CCD引导程序号1光阑"), XmlIgnore]
        /// <summary>
        /// CCD引导程序号1光阑
        /// </summary>
        public float CCDGuidePrgNo_1
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return cCDGuidePrgNo_1;
                cCDGuidePrgNo_1 = (float)obj;
                return cCDGuidePrgNo_1;
            }
            set { cCDGuidePrgNo_1 = value; OnPropertyChanged(); }
        }
        float cCDGuidePrgNo_1 = 0;

        [DownLoad, DisplayName("CCD引导程序号2PCB"), XmlIgnore]
        /// <summary>
        /// CCD引导程序号2PCB
        /// </summary>
        public float CCDGuidePrgNo_2
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return cCDGuidePrgNo_2;
                cCDGuidePrgNo_2 = (float)obj;
                return cCDGuidePrgNo_2;
            }
            set { cCDGuidePrgNo_2 = value; OnPropertyChanged(); }
        }
        float cCDGuidePrgNo_2 = 0;

       

        [DownLoad, DisplayName("音圈电机下压电流设定值上限"), XmlIgnore]
        /// <summary>
        /// 音圈电机下压电流设定值上限
        /// </summary>
        public float VoiceCoilMotorCurrentsettingvalueUpperLimit
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return voiceCoilMotorCurrentsettingvalueUpperLimit;
                voiceCoilMotorCurrentsettingvalueUpperLimit = (float)obj;
                return voiceCoilMotorCurrentsettingvalueUpperLimit;
            }
            set { voiceCoilMotorCurrentsettingvalueUpperLimit = value; OnPropertyChanged(); }
        }
        float voiceCoilMotorCurrentsettingvalueUpperLimit = 0;

        [DownLoad, DisplayName("音圈电机下压电流设定值下限"), XmlIgnore]
        /// <summary>
        /// 音圈电机下压电流设定值下限
        /// </summary>
        public float VoiceCoilMotorCurrentsettingvalueLowerLimit
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return voiceCoilMotorCurrentsettingvalueLowerLimit;
                voiceCoilMotorCurrentsettingvalueLowerLimit = (float)obj;
                return voiceCoilMotorCurrentsettingvalueLowerLimit;
            }
            set { voiceCoilMotorCurrentsettingvalueLowerLimit = value; OnPropertyChanged(); }
        }
        float voiceCoilMotorCurrentsettingvalueLowerLimit = 0;

        [DownLoad, DisplayName("光阑贴装高度上限值"), XmlIgnore]
        /// <summary>
        /// 光阑贴装高度上限值
        /// </summary>
        public float STOPHeightUpperLimit
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return sTOPHeightUpperLimit;
                sTOPHeightUpperLimit = (float)obj;
                return sTOPHeightUpperLimit;
            }
            set { sTOPHeightUpperLimit = value; OnPropertyChanged(); }
        }
        float sTOPHeightUpperLimit = 0;


        [DownLoad, DisplayName("光阑贴装高度下限值"), XmlIgnore]
        /// <summary>
        /// 光阑贴装高度下限值
        /// </summary>
        public float STOPHeightLowerLimit
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return sTOPHeightLowerLimit;
                sTOPHeightLowerLimit = (float)obj;
                return sTOPHeightLowerLimit;
            }
            set { sTOPHeightLowerLimit = value; OnPropertyChanged(); }
        }
        float sTOPHeightLowerLimit = 0;


        [DownLoad, DisplayName("光阑高度上限值"), XmlIgnore]
        /// <summary>
        /// 光阑高度上限值
        /// </summary>
        public float STOPAccepttheightUpperLimit
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return sTOPAccepttheightUpperLimit;
                sTOPAccepttheightUpperLimit = (float)obj;
                return sTOPAccepttheightUpperLimit;
            }
            set { sTOPAccepttheightUpperLimit = value; OnPropertyChanged(); }
        }
        float sTOPAccepttheightUpperLimit = 0;

        [DownLoad, DisplayName("光阑高度下限值"), XmlIgnore]
        /// <summary>
        /// 光阑高度下限值
        /// </summary>
        public float STOPAccepttheightLowerLimit
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return sTOPAccepttheightLowerLimit;
                sTOPAccepttheightLowerLimit = (float)obj;
                return sTOPAccepttheightLowerLimit;
            }
            set { sTOPAccepttheightLowerLimit = value; OnPropertyChanged(); }
        }
        float sTOPAccepttheightLowerLimit = 0;

        [DownLoad, DisplayName("固化台UV灯照胶时间"), XmlIgnore]
        /// <summary>
        /// 固化台UV灯照胶时间
        /// </summary>
        public float UVCuringTime
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return uVCuringTime;
                uVCuringTime = (float)obj;
                return uVCuringTime;
            }
            set { uVCuringTime = value; OnPropertyChanged(); }
        }
        float uVCuringTime = 0;




        #endregion






        #region  MES 输出



        [UpLoad, DisplayName("装配工装ID（穴位号）"), XmlIgnore]
        /// <summary>
        /// 装配工装ID（穴位号）
        /// </summary>
        public float RXAssemblyToolID
        {
            get
            {
                return rXAssemblyToolID;
            }
            set { rXAssemblyToolID = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float rXAssemblyToolID = 0;

        [UpLoad, DisplayName("光阑装配工装ID（穴位号)"), XmlIgnore]
        /// <summary>
        /// 光阑装配工装ID（穴位号）
        /// </summary>
        public float RXSTOPAssemblyToolID
        {
            get
            {
                return rXSTOPAssemblyToolID;
            }
            set { rXSTOPAssemblyToolID = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float rXSTOPAssemblyToolID = 0;

        [UpLoad, DisplayName("光阑视觉引导实际的偏差量X"), XmlIgnore]
        /// <summary>
        /// 光阑视觉引导实际的偏差量X
        /// </summary>
        public float STOPCCDGuideActPosDeviationX
        {
            get
            {
                return sTOPCCDGuideActPosDeviationX;
            }
            set { sTOPCCDGuideActPosDeviationX = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float sTOPCCDGuideActPosDeviationX = 0;

        [UpLoad, DisplayName("光阑视觉引导实际的偏差量Y"), XmlIgnore]
        /// <summary>
        /// 光阑视觉引导实际的偏差量Y
        /// </summary>
        public float STOPCCDGuideActPosDeviationY
        {
            get
            {
                return sTOPCCDGuideActPosDeviationY;
            }
            set { sTOPCCDGuideActPosDeviationY = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float sTOPCCDGuideActPosDeviationY = 0;

        [UpLoad, DisplayName("光阑视觉引导实际的偏差量R"), XmlIgnore]
        /// <summary>
        /// 光阑视觉引导实际的偏差量R
        /// </summary>
        public float STOPCCDGuideActPosDeviationR
        {
            get
            {
                return sTOPCCDGuideActPosDeviationR;
            }
            set { sTOPCCDGuideActPosDeviationR = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float sTOPCCDGuideActPosDeviationR = 0;


        [UpLoad, DisplayName("RFB视觉引导实际的偏差量X"), XmlIgnore]
        /// <summary>
        /// RFB视觉引导实际的偏差量X
        /// </summary>
        public float RFBCCDGuideActPosDeviationX
        {
            get
            {
                return rFBCCDGuideActPosDeviationX;
            }
            set { rFBCCDGuideActPosDeviationX = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float rFBCCDGuideActPosDeviationX = 0;


        [UpLoad, DisplayName("RFB视觉引导实际的偏差量Y"), XmlIgnore]
        /// <summary>
        /// RFB视觉引导实际的偏差量Y
        /// </summary>
        public float RFBCCDGuideActPosDeviationY
        {
            get
            {
                return rFBCCDGuideActPosDeviationY;
            }
            set { rFBCCDGuideActPosDeviationY = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float rFBCCDGuideActPosDeviationY = 0;

        [UpLoad, DisplayName("RFB视觉引导实际的偏差量R"), XmlIgnore]
        /// <summary>
        /// RFB视觉引导实际的偏差量X
        /// </summary>
        public float RFBCCDGuideActPosDeviationR
        {
            get
            {
                return rFBCCDGuideActPosDeviationR;
            }
            set { rFBCCDGuideActPosDeviationR = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float rFBCCDGuideActPosDeviationR = 0;


        [UpLoad, DisplayName("视觉引导实际位置X"), XmlIgnore]
        /// <summary>
        /// 视觉引导实际位置X
        /// </summary>
        public float GuideActPosX
        {
            get
            {
                return guideActPosX;
            }
            set { guideActPosX = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float guideActPosX = 0;

        [UpLoad, DisplayName("视觉引导实际位置Y"), XmlIgnore]
        /// <summary>
        /// 视觉引导实际位置Y
        /// </summary>
        public float GuideActPosY
        {
            get
            {
                return guideActPosY;
            }
            set { guideActPosY = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float guideActPosY = 0;

        [UpLoad, DisplayName("视觉引导实际位置R"), XmlIgnore]
        /// <summary>
        /// 视觉引导实际位置R
        /// </summary>
        public float GuideActPosR
        {
            get
            {
                return guideActPosR;
            }
            set { guideActPosR = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float guideActPosR = 0;

        [UpLoad, DisplayName("音圈电机移载速度"), XmlIgnore]
        /// <summary>
        /// 音圈电机移载速度
        /// </summary>
        public float SETVoiceCoilMotormovespeed
        {
            get
            {
                return sETVoiceCoilMotormovespeed;
            }
            set { sETVoiceCoilMotormovespeed = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float sETVoiceCoilMotormovespeed = 0;

        [UpLoad, DisplayName("音圈电机下压电流最大值"), XmlIgnore]
        /// <summary>
        /// 音圈电机下压电流最大值
        /// </summary>
        public float SETVoiceCoilMotorCurrentactualvalue
        {
            get
            {
                return sETVoiceCoilMotorCurrentactualvalue;
            }
            set { sETVoiceCoilMotorCurrentactualvalue = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float sETVoiceCoilMotorCurrentactualvalue = 0;

        [UpLoad, DisplayName("压入力原始数据（力/位移曲线）数据路径"), XmlIgnore]
        /// <summary>
        /// 压入力原始数据（力/位移曲线）路径
        /// </summary>
        public string  PressureCurvesDataPath
        {
            get
            {
                return pressureCurvesDataPath;
            }
            set { pressureCurvesDataPath = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        string pressureCurvesDataPath = string.Empty;


        [UpLoad, DisplayName("压入力原始数据（力/位移曲线）图路径"), XmlIgnore]
        /// <summary>
        /// 压入力原始数据（力/位移曲线）图路径
        /// </summary>
        public string PressureCurvesPath
        {
            get
            {
                return pressureCurvesPath;
            }
            set { pressureCurvesPath = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        string pressureCurvesPath = string.Empty;



        [UpLoad, DisplayName("光阑实际安装位置X1"), XmlIgnore]
        /// <summary>
        /// 光阑实际安装位置X1
        /// </summary>
        public float CCDGuideActPosDeviationX1
        {
            get
            {
                return cCDGuideActPosDeviationX1;
            }
            set { cCDGuideActPosDeviationX1 = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float cCDGuideActPosDeviationX1 = 0;


        [UpLoad, DisplayName("光阑实际安装位置Y1"), XmlIgnore]
        /// <summary>
        /// 光阑实际安装位置Y1
        /// </summary>
        public float CCDGuideActPosDeviationY1
        {
            get
            {
                return cCDGuideActPosDeviationY1;
            }
            set { cCDGuideActPosDeviationY1 = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float cCDGuideActPosDeviationY1 = 0;

        [UpLoad, DisplayName("光阑实际安装位置X2"), XmlIgnore]
        /// <summary>
        /// 光阑实际安装位置X2
        /// </summary>
        public float CCDGuideActPosDeviationX2
        {
            get
            {
                return cCDGuideActPosDeviationX2;
            }
            set { cCDGuideActPosDeviationX2 = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float cCDGuideActPosDeviationX2 = 0;


        [UpLoad, DisplayName("光阑实际安装位置Y2"), XmlIgnore]
        /// <summary>
        /// 光阑实际安装位置Y2
        /// </summary>
        public float CCDGuideActPosDeviationY2
        {
            get
            {
                return cCDGuideActPosDeviationY2;
            }
            set { cCDGuideActPosDeviationY2 = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float cCDGuideActPosDeviationY2 = 0;

        [UpLoad, DisplayName("光阑实际安装位置R"), XmlIgnore]
        /// <summary>
        /// 光阑实际安装位置R
        /// </summary>
        public float CCDGuideActPosDeviationR
        {
            get
            {
                return cCDGuideActPosDeviationR;
            }
            set { cCDGuideActPosDeviationR = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float cCDGuideActPosDeviationR = 0;


        [UpLoad, DisplayName("光阑重复安装次数"), XmlIgnore]
        /// <summary>
        /// 光阑重复安装次数
        /// </summary>
        public float Assemblytimessetup
        {
            get
            {
                return assemblytimessetup;
            }
            set { assemblytimessetup = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float assemblytimessetup = 0;

        [UpLoad, DisplayName("音圈电机移载速度"), XmlIgnore]
        /// <summary>
        /// 音圈电机移载速度
        /// </summary>
        public float VoiceCoilMotormovespeed
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return voiceCoilMotormovespeed;
                voiceCoilMotormovespeed = (float)obj;
                return voiceCoilMotormovespeed;
            }
            set { voiceCoilMotormovespeed = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float voiceCoilMotormovespeed = 0;

        [UpLoad, DisplayName("复测光阑实际安装位置X"), XmlIgnore]
        /// <summary>
        /// 复测光阑实际安装位置X
        /// </summary>
        public float RetestActPosX
        {
            get
            {
                return retestActPosX;
            }
            set { retestActPosX = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float retestActPosX = 0;

        [UpLoad, DisplayName("复测光阑实际安装位置Y"), XmlIgnore]
        /// <summary>
        /// 复测光阑实际安装位置Y
        /// </summary>
        public float RetestActPosY
        {
            get
            {
                return retestActPosY;
            }
            set { retestActPosY = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float retestActPosY = 0;

        [UpLoad, DisplayName("复测光阑实际安装位置R"), XmlIgnore]
        /// <summary>
        /// 复测光阑实际安装位置R
        /// </summary>
        public float RetestActPosR
        {
            get
            {
                return retestActPosR;
            }
            set { retestActPosR = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float retestActPosR = 0;

        [UpLoad, DisplayName("光阑高度1"), XmlIgnore]
        /// <summary>
        /// 光阑高度1
        /// </summary>
        public float Heightcheck1
        {
            get
            {
                return heightcheck1;
            }
            set { heightcheck1 = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float heightcheck1 = 2;


        [UpLoad, DisplayName("光阑高度2"), XmlIgnore]
        /// <summary>
        /// 光阑高度2
        /// </summary>
        public float Heightcheck2
        {
            get
            {
                return heightcheck2;
            }
            set { heightcheck2 = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float heightcheck2 = 2;


        [UpLoad, DisplayName("光阑高度3"), XmlIgnore]
        /// <summary>
        /// 光阑高度3
        /// </summary>
        public float Heightcheck3
        {
            get
            {
                return heightcheck3;
            }
            set { heightcheck3 = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float heightcheck3 = 2;

        [UpLoad, DisplayName("光阑安装复检贴装高度1"), XmlIgnore]
        /// <summary>
        /// 光阑安装复检贴装高度1
        /// </summary>
        public float StopHeightcheck1
        {
            get
            {
                return stopHeightcheck1;
            }
            set { stopHeightcheck1 = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float stopHeightcheck1 = 0;


        [UpLoad, DisplayName("光阑安装复检贴装高度2"), XmlIgnore]
        /// <summary>
        /// 光阑安装复检贴装高度2
        /// </summary>
        public float StopHeightcheck2
        {
            get
            {
                return stopHeightcheck2;
            }
            set { stopHeightcheck2 = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float stopHeightcheck2 = 0;


        [UpLoad, DisplayName("光阑安装复检贴装高度3"), XmlIgnore]
        /// <summary>
        /// 光阑安装复检贴装高度2
        /// </summary>
        public float StopHeightcheck3
        {
            get
            {
                return stopHeightcheck3;
            }
            set { stopHeightcheck3 = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float stopHeightcheck3 = 0;


        [UpLoad, DisplayName("光阑安装复检贴装高度3"), XmlIgnore]
        /// <summary>
        /// 光阑安装复检贴装高度2
        /// </summary>
        public float StopHeightcheck4
        {
            get
            {
                return stopHeightcheck4;
            }
            set { stopHeightcheck4 = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float stopHeightcheck4 = 0;


        [UpLoad, DisplayName("光阑安装_固化台UV灯照胶时间"), XmlIgnore]
        /// <summary>
        /// 光阑安装_固化台UV灯照胶时间
        /// </summary>
        public float UVCurUseTime
        {
            get
            {
                return uVCurUseTime;
            }
            set { uVCurUseTime = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float uVCurUseTime = 0;


        [UpLoad, DisplayName("光阑安装_固化台UV灯照胶功率"), XmlIgnore]
        /// <summary>
        /// 光阑安装_固化台UV灯照胶功率
        /// </summary>
        public float UVCuringLightPower
        {
            get
            {
                return uVCuringLightPower;
            }
            set { uVCuringLightPower = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float uVCuringLightPower = 0;
   
        [UpLoad, DisplayName("程序号"), XmlIgnore]
        /// <summary>
        /// 程序号
        /// </summary>
        public float ProgramNo
        {
            get
            {
                return programNo;
            }
            set { programNo = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float programNo = 0;



        [UpLoad, DisplayName("光阑引导图片"), XmlIgnore]
        /// <summary>
        /// 光阑引导图片
        /// </summary>
        public string CCDGuidePicture
        {
            get
            {
                return cCDGuidePicture;
            }
            set { cCDGuidePicture = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        string cCDGuidePicture = "";

        #endregion

        #region 力控参数
        [UpLoad, DisplayName("音圈电机移载速度")/*, XmlIgnore*/]
        /// <summary>
        /// 第一段PP速度
        /// </summary>
        public float FirstPPSpd
        {
            get { return firstPPSpd; }
            set { firstPPSpd = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float firstPPSpd = 0.0f;


        /// <summary>
        /// 第一段PP距离
        /// </summary>
        public float FirstPPDis
        {
            get { return firstPPDis; }
            set { firstPPDis = value; OnPropertyChanged(); }
        }
        private float firstPPDis = 0.0f;

        /// <summary>
        /// 第二段PV距离
        /// </summary>
        public float SecPVDis
        {
            get { return secPVDis; }
            set { secPVDis = value; OnPropertyChanged(); }
        }
        private float secPVDis = 0.0f;

        /// <summary>
        /// 第三段PT距离
        /// </summary>
        public float ThirdPTSpd
        {
            get { return thirdPTSpd; }
            set { thirdPTSpd = value; OnPropertyChanged(); }
        }
        private float thirdPTSpd = 0.0f;


        /// <summary>
        /// 保压压力值
        /// </summary>
        [UpLoad, DisplayName("保压压力值")]
        public float Preesure
        {
            get { return preesure; }
            set { preesure = value;UpLoadPro(value); OnPropertyChanged(); }
        }
        private float preesure = 1000f;
        #endregion        
    }
}
