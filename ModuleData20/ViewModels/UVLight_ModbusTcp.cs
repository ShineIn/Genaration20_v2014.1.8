﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace ModuleData
{
    class UVLight_ModbusTcp
    {
        public Socket ModbusClient;
        public bool Connected { get; private set; }       //链接状态
        public void Connect(string addip, int addport)
        {
            byte[] data = new byte[1024];
            string ipadd = addip.Trim();//将服务器IP地址存放在字符串 ipadd中                     
            //创建一个套接字              
            IPEndPoint ie = new IPEndPoint(IPAddress.Parse(ipadd), addport);
            ModbusClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //将套接字与远程服务器地址相连           
            try
            {
                ModbusClient.Connect(ie);
                Connected = true;
            }
            catch (SocketException e)
            {
                //MessageBox.Show("连接设备失败！  " + e.Message);
                Connected = false;
                return;
            }
            //ThreadStart myThreaddelegate = new ThreadStart(ReceiveMsg);
            //myThread = new Thread(myThreaddelegate);
            //myThread.Start();
        }
        public void DisConnect()
        {
            try
            {
                ModbusClient.Close();
                ModbusClient.Dispose();
            }
            catch
            { }
            Connected = false;
        }



        public static byte[] CRC(byte[] data, bool Reverse = false)
        {
            int len = data.Length;
            if (len > 0)
            {
                ushort crc = 0xFFFF;

                for (int i = 0; i < len; i++)
                {
                    crc = (ushort)(crc ^ (data[i]));
                    for (int j = 0; j < 8; j++)
                    {
                        crc = (crc & 1) != 0 ? (ushort)((crc >> 1) ^ 0xA001) : (ushort)(crc >> 1);
                    }
                }
                byte hi = (byte)((crc & 0xFF00) >> 8);  //高位置
                byte lo = (byte)(crc & 0x00FF);         //低位置

                if (Reverse)
                    return new byte[] { lo, hi };
                else
                    return new byte[] { hi, lo };
            }
            return new byte[] { 0, 0 };
        }
        private static byte[] strToToHexByte(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }
        public byte[] getWriteCommand(string strAddr, int nValue)
        {
            byte[] valueArry = BitConverter.GetBytes(nValue);
            byte[] byteAddr = strToToHexByte(strAddr);

            byte[] byteHeader = new byte[6];
            byteHeader[0] = 0x00;
            byteHeader[1] = 0x00;
            byteHeader[2] = 0x00;
            byteHeader[3] = 0x00;
            byteHeader[4] = 0x00;
            byteHeader[5] = 0x11;//有效数据字节数

            byte[] byteCmd = new byte[9];
            byteCmd[0] = 0x01;//设备ID
            byteCmd[1] = 0x10;//功能码
            //写入地址
            byteCmd[2] = byteAddr[0];
            byteCmd[3] = byteAddr[1];
            //写入寄存器数量
            byteCmd[4] = 0x00;
            byteCmd[5] = 0x01;
            //写入字节数
            byteCmd[6] = 0x02;
            //写入数据
            byteCmd[7] = valueArry[1];
            byteCmd[8] = valueArry[0];
            //CRC校验
            byte[] byteCRC = CRC(byteCmd, true);

            byte[] writeCmd = new byte[byteHeader.Length + byteCmd.Length + byteCRC.Length];
            byteHeader.CopyTo(writeCmd, 0);
            byteCmd.CopyTo(writeCmd, 6);
            byteCRC.CopyTo(writeCmd, 15);

            return writeCmd;

        }
        public byte[] getReadCommand(string strAddr)
        {
            byte[] byteAddr = strToToHexByte(strAddr);

            byte[] byteHeader = new byte[6];
            byteHeader[0] = 0x00;
            byteHeader[1] = 0x00;
            byteHeader[2] = 0x00;
            byteHeader[3] = 0x00;
            byteHeader[4] = 0x00;
            byteHeader[5] = 0x11;//有效数据字节数

            byte[] byteCmd = new byte[6];
            byteCmd[0] = 0x01;//设备ID
            byteCmd[1] = 0x03;//功能码
            //写入地址
            byteCmd[2] = byteAddr[0];
            byteCmd[3] = byteAddr[1];
            //查询寄存器数量
            byteCmd[4] = 0x00;
            byteCmd[5] = 0x01;

            //CRC校验
            byte[] byteCRC = CRC(byteCmd, true);

            byte[] readCmd = new byte[byteHeader.Length + byteCmd.Length + byteCRC.Length];
            byteHeader.CopyTo(readCmd, 0);
            byteCmd.CopyTo(readCmd, 6);
            byteCRC.CopyTo(readCmd, 12);

            return readCmd;
        }

        bool getWriteResponse(string strAddr, int nTimeoutMs = 3000)
        {
            var strResponse = "";
            byte[] dataReceive = new byte[20];
            ModbusClient.ReceiveTimeout = nTimeoutMs;
            try
            {
                int nLength = ModbusClient.Receive(dataReceive);
                if (nLength > 0)
                {
                    strResponse = BitConverter.ToString(dataReceive, 8, 2).Replace("-", "").ToUpper();
                    if (strResponse == strAddr.ToUpper())
                        return true;
                }
            }
            catch
            {
                return false;
            }
            return false;
        }
        bool getReadResponse(string strAddr, out string strReturn, int nTimeoutMs = 3000)
        {
            var strResponse = "";
            byte[] dataReceive = new byte[20];
            ModbusClient.ReceiveTimeout = nTimeoutMs;
            try
            {
                int nLength = ModbusClient.Receive(dataReceive);
                if (nLength > 0)
                {
                    int nSize = dataReceive[8];
                    strResponse = BitConverter.ToString(dataReceive, 9, nSize).Replace("-", "").ToUpper();
                    strReturn = strResponse;
                    return true;
                }
            }
            catch
            {
                strReturn = strResponse;
                return false;
            }
            strReturn = strResponse;
            return false;
        }
        public bool UVLight_TurnOn(int nChannel)
        {
            if (Connected)
            {
                string strAddr = nChannel.ToString("00") + "2A";
                ModbusClient.Send(getWriteCommand(strAddr, 1));
                if (getWriteResponse(strAddr))
                {
                    return true;
                }
            }
            return false;
        }
        public bool UVLight_TurnOff(int nChannel)
        {
            if (Connected)
            {
                string strAddr = nChannel.ToString("00") + "2A";
                ModbusClient.Send(getWriteCommand(strAddr, 0));
                if (getWriteResponse(strAddr))
                {
                    return true;
                }
            }
            return false;
        }
        public int UVLight_GetOpticalPower(int nChannel)
        {
            int dRead = 0;
            if (Connected)
            {
                string strAddr = nChannel.ToString("00") + "2C";
                ModbusClient.Send(getReadCommand(strAddr));
                string strRead = "";
                if (getReadResponse(strAddr, out strRead))
                {
                    if (strRead != "")
                        dRead = Int32.Parse(strRead, System.Globalization.NumberStyles.HexNumber);
                }
            }
            return dRead;

        }
        public int UVLight_GetStrength(int nChannel)
        {
            int dRead = 0;
            if (Connected)
            {
                string strAddr = nChannel.ToString("00") + "07";
                ModbusClient.Send(getReadCommand(strAddr));
                string strRead = "";
                if (getReadResponse(strAddr, out strRead))
                {
                    if (strRead != "")
                        dRead = Int32.Parse(strRead, System.Globalization.NumberStyles.HexNumber);

                }
            }
            return dRead;
        }
        public int UVLight_GetTemperature(int nChannel)
        {
            int dRead = 0;
            if (Connected)
            {
                string strAddr = nChannel.ToString("00") + "03";
                ModbusClient.Send(getReadCommand(strAddr));
                string strRead = "";
                if (getReadResponse(strAddr, out strRead))
                {
                    if (strRead != "")
                        dRead = Int32.Parse(strRead, System.Globalization.NumberStyles.HexNumber);
                }
            }
            return dRead;
        }
        //光功率0-65535 mW
        public bool UVLight_SetOpticalPowerRange(int nChannel, int nLow, int nHigh)
        {
            if (Connected)
            {
                string strAddrHigh = nChannel.ToString("00") + "2F";
                string strAddrLow = nChannel.ToString("00") + "30";
                ModbusClient.Send(getWriteCommand(strAddrHigh, nHigh));
                if (getWriteResponse(strAddrHigh))
                {
                    ModbusClient.Send(getWriteCommand(strAddrLow, nLow));
                    if (getWriteResponse(strAddrLow))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool UVLight_SetPowerLevel(int nChannel, int nPercent)
        {
            if (Connected)
            {
                string strAddrHigh = nChannel.ToString("00") + "09";

                ModbusClient.Send(getWriteCommand(strAddrHigh, nPercent));
                if (getWriteResponse(strAddrHigh))
                {

                    return true;

                }
            }
            return false;
        }


        public bool UVLight_SetTime(int nChannel, double nTime)
        {
            if (Connected)
            {
                string strAddrHigh = nChannel.ToString("00") + "12";
                double uvTimes = nTime * 10;
                ModbusClient.Send(getWriteCommand(strAddrHigh, (int)uvTimes));
                if (getWriteResponse(strAddrHigh))
                {
                    return true;
                }
            }
            return false;
        }


        public float UVLight_GetTime(int nChannel)
        {
            int dRead = 0;
            if (Connected)
            {
                string strAddr = nChannel.ToString("00") + "12";
                ModbusClient.Send(getReadCommand(strAddr));
                string strRead = "";
                if (getReadResponse(strAddr, out strRead))
                {
                    if (strRead != "")
                        dRead = Int32.Parse(strRead, System.Globalization.NumberStyles.HexNumber);
                }
            }
            return dRead / 10.0f;
        }


        /// <summary>
        /// UV灯开关状态
        /// </summary>
        /// <param name="nChannel"></param>
        /// <returns></returns>
        public int UVLight_GetUVState(int nChannel)
        {
            int dRead = 0;
            if (Connected)
            {
                string strAddr = nChannel.ToString("00") + "04";
                ModbusClient.Send(getReadCommand(strAddr));
                string strRead = "";
                if (getReadResponse(strAddr, out strRead))
                {
                    if (strRead != "")
                        dRead = Int32.Parse(strRead, System.Globalization.NumberStyles.HexNumber);
                }
            }
            return dRead;
        }
    }

}
