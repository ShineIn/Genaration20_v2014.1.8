﻿using G4.GlueCore;
using Googo.Manul;
using ModuleData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ModuleData20.ViewModels
{
    /// <summary>
    /// 初始化绑定模板
    /// </summary>
    [Serializable]
    public class StationTwoIntial : InitialMode, IStationModule
    {
        #region 初始化属性


        /// <summary>
        /// 初始化指令
        /// </summary>
        [DownLoad,UpLoad, XmlIgnore]
        public bool Signal_W_HeartBeat
        {
            get
            {
                object temp = DownLoadPro();
                if (null == temp || !(temp is bool)) return false;
                signal_W_HeartBeat = (bool)temp;
                return signal_W_HeartBeat;
            }
            set { signal_W_HeartBeat = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        bool signal_W_HeartBeat = false;



        /// <summary>
        /// 点胶模组1设置
        /// </summary>
        public EnumWorkModule AxisWorkModule1
        {
            get { return axisWorkModule1; }
            set { axisWorkModule1 = value; OnPropertyChanged(); }
        }
        EnumWorkModule axisWorkModule1 = EnumWorkModule.S20_Left;

        /// <summary>
        /// 点胶模组2设置
        /// </summary>
        public EnumWorkModule AxisWorkModule2
        {
            get { return axisWorkModule2; }
            set { axisWorkModule2 = value; OnPropertyChanged(); }
        }
        EnumWorkModule axisWorkModule2 = EnumWorkModule.S20_Right;

        /// <summary>
        /// Z3轴回原安全高度
        /// </summary>
        public double Z3SafetyPos
        {
            get { return z3SafetyPos; }
            set { z3SafetyPos = value; OnPropertyChanged(); }
        }
        double z3SafetyPos = -40.0;

        /// <summary>
        /// 启用光栅
        /// </summary>
        public bool EnableGrating
        {
            get { return enableGrating; }
            set { enableGrating = value; OnPropertyChanged(); }
        }
        bool enableGrating = true;


        /// <summary>
        /// 调试模式
        /// </summary>
        [XmlIgnore]
        public bool DebugModel
        {
            get { return debugModel; }
            set { debugModel = value; OnPropertyChanged(); }
        }
        bool debugModel = false;

        #endregion
    }
}
