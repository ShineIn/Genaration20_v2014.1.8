﻿using S7.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ModuleData
{
    static class S7Extension
    {
        /// <summary>
        /// 读操作
        /// </summary>
        /// <param name="address">地址</param>
        /// <returns></returns>
        internal static bool ReadValue<T>(this Plc module, string address, out T value)
        {
            Type tartype = typeof(T);
            value = default(T);
            if (module == null)
            {
                return false;
            }
            object val = module.Read(address);
            if (tartype == typeof(bool) || tartype == typeof(byte) || tartype == typeof(uint))
            {
                value = (T)val;
            }
            else if (tartype == typeof(string))
            {
                val = val.ValToBinString();
                value = (T)val;
            }
            else if (tartype == typeof(int))
            {
                val = val.ToString().BinStringToInt32();
                value = (T)val;
            }
            else if (tartype == typeof(float))
            {
                val = ((uint)val).ConvertToFloat();
                value = (T)val;
            }
            else if (tartype == typeof(short))
            {
                val = ((ushort)val).ConvertToShort();
                value = (T)val;
            }
            return true;
        }

        /// <summary>
        /// 读操作
        /// </summary>
        /// <param name="address">地址</param>
        /// <returns></returns>
        internal static bool ReadValue(this Plc module, string address, Type tartype, out dynamic value)
        {
            value = null;
            if (module == null)
            {
                return false;
            }
            object val = module.Read(address);
            if (tartype == typeof(bool) || tartype == typeof(byte) || tartype == typeof(uint))
            {
                value = val;
            }
            else if (tartype == typeof(string))
            {
                val = val.ValToBinString();
                value = val;
            }
            else if (tartype == typeof(int))
            {
                val = val.ToString().BinStringToInt32();
                value = val;
            }
            else if (tartype == typeof(float))
            {
                val = ((uint)val).ConvertToFloat();
                value = val;
            }
            else if (tartype == typeof(short))
            {
                val = ((ushort)val).ConvertToShort();
                value = val;
            }
            return true;
        }

        /// <summary>
        /// 写操作
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="val">变量值</param>
        internal static bool WriteValue(this Plc module, string address, object val)
        {
            if (!module.IsConnected) { return false; }
            module.Write(address, val);
            if (module.Read(address) != val) return false;             //写完后读值比较，相同则返回写入成功
            else return true;
        }

    }
}
