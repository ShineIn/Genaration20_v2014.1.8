﻿using Googo.Manul;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ModuleData20
{
    /// <summary>
    /// PLC切换配方信号
    /// </summary>
    [Serializable]
    public class OpenFile : CommunicaBase
    {
        /// <summary>
        /// 启动信号
        /// </summary>
        [DownLoad, XmlIgnore, DisplayName("启动信号")]
        public bool StartSignal
        {
            get
            {
                object temp = DownLoadPro();
                if (null == temp || !(temp is bool)) return false;
                _startSignal = (bool)temp;
                return _startSignal;
            }
            set { _startSignal = value; OnPropertyChanged(); }
        }
        bool _startSignal = false;

        /// <summary>
        /// 运行中
        /// </summary>
        [UpLoad, DownLoad, XmlIgnore, DisplayName("运行中")]
        public bool RunIng
        {
            get
            {
                object temp = DownLoadPro();
                if (null == temp || !(temp is bool)) return false;
                _runIng = (bool)temp;
                return _runIng;
            }
            set { _runIng = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        bool _runIng = false;

        /// <summary>
        /// 运行结束
        /// </summary>
        [UpLoad, XmlIgnore, DisplayName("运行结束")]
        public bool RunFinish
        {
            get
            {
                return _runFinish;
            }
            set { _runFinish = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        bool _runFinish = false;

        /// <summary>
        /// 运行结果OK
        /// </summary>
        [UpLoad, XmlIgnore, DisplayName("运行结果OK")]
        public bool ResultOK
        {
            get
            {
                return _resultOK;
            }
            set { _resultOK = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        bool _resultOK = false;

        /// <summary>
        /// 运行结果NG
        /// </summary>
        [UpLoad,  XmlIgnore, DisplayName("运行结果NG")]
        public bool ResultNG
        {
            get
            {
                return _resultNG;
            }
            set { _resultNG = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        bool _resultNG = false;

        /// <summary>
        /// 配方名称
        /// </summary>
        [UpLoad, DownLoad, XmlIgnore, DisplayName("配方名称")]
        public string FileName
        {
            get
            {
                object temp = DownLoadPro();
                if (null == temp || !(temp is string)) return string.Empty;
                _fileName = (string)temp;
                return _fileName;              
            }
            set { _fileName = value;OnPropertyChanged(); }
        }
        string _fileName = string.Empty;

        /// <summary>
        ///给运行中信号并等待启动信号复位
        /// </summary>
        /// <remarks>
        /// 5S超时，即循环写入100次
        /// </remarks>
        /// <returns></returns>
        public bool WaitStartReset()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            while (StartSignal && stopwatch.ElapsedMilliseconds < 5000)
            {
                //清除结果后再给运行中
                ResultNG = false;
                ResultOK = false;
                RunFinish = false;
                RunIng = true;
                System.Threading.Thread.Sleep(50);
            }
            return !StartSignal;
        }

        /// <summary>
        /// 给结果到PLC，并且等待运行中信号取消
        /// </summary>
        /// <param name="bResult">结果，OK或者NG</param>
        /// <returns></returns>
        public bool WaitRuningReset(bool bResult)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            while (RunIng && stopwatch.ElapsedMilliseconds < 5000)
            {
                ResultOK = bResult;
                ResultNG = !bResult;
                System.Threading.Thread.Sleep(50);
                RunFinish = true;

            }
            return !RunIng;
        }

        /// <summary>
        /// 重置给到PLC的信号
        /// </summary>
        public void Reset()
        {
            ResultOK = false;
            ResultNG = false;
            RunFinish = false;
            RunIng = false;
        }
    }
}
