﻿using ModuleData20.Views;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ModuleData20.Helper
{
    public class ForceControl
    {
        public static List<CurvesParm> dataList = new List<CurvesParm>();
        public static string SN = string.Empty;

        /// <summary>
        /// 第一段PP速度
        /// </summary>
        public static float FirstPPSpd
        {
            get { return firstPPSpd; }
            set { firstPPSpd = value; }
        }
        private static float firstPPSpd = 0.0f;

        /// <summary>
        /// 第一段PP距离
        /// </summary>
        public static float FirstPPDis
        {
            get { return firstPPDis; }
            set { firstPPDis = value; }
        }
        private static float firstPPDis = 0.0f;

        /// <summary>
        /// 第二段PV距离
        /// </summary>
        public static float SecPVDis
        {
            get { return secPVDis; }
            set { secPVDis = value; }
        }
        private static float secPVDis = 0.0f;

        /// <summary>
        /// 第三段PT距离
        /// </summary>
        public static float ThirdPTSpd
        {
            get { return thirdPTSpd; }
            set { thirdPTSpd = value; }
        }
        private static float thirdPTSpd = 0.0f;


        /// <summary>
        /// 保压压力值
        /// </summary>
        public static float Preesure
        {
            get { return preesure; }
            set { preesure = value; }
        }
        private static float preesure = 0.0f;
        public static void InitialParm()
        {
            int rtn;
            uint code;
            ushort index;
            byte[] data1;
            //清除电机报警
            data1 = BitConverter.GetBytes(143);
            index = ushort.Parse("6040", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);


            //连续运行模式（0：不连续，9连续运行）
            data1 = BitConverter.GetBytes(0);
            index = ushort.Parse("2103", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);


            //关闭力闭环（0：关闭，1打开）
            data1 = BitConverter.GetBytes(0);
            index = ushort.Parse("201B", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);

            //启动返回原位标志清零
            data1 = BitConverter.GetBytes(0);
            index = ushort.Parse("212B", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);

            //恢复正向扭矩
            data1 = BitConverter.GetBytes(30000);
            index = ushort.Parse("60E0", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);

            //恢复反向扭矩
            data1 = BitConverter.GetBytes(30000);
            index = ushort.Parse("60E1", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);

            //停止软着陆功能
            data1 = BitConverter.GetBytes(0);
            index = ushort.Parse("2128", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);

            Thread.Sleep(10);

            //关闭软着陆功能
            data1 = BitConverter.GetBytes(0);
            index = ushort.Parse("2126", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);

            //打开位置模式
            rtn = GTN.mc.GTN_SetEcatAxisMode(1, 11, 8);


            //保压时间
            data1 = BitConverter.GetBytes(1000);
            index = ushort.Parse("211F", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);




            LogSv.Log.log.Write("力控初始化完成", Color.Green);






        }


        public static int StartSoftLand()
        {
            short rtn;
            uint code;
            ushort index;
            byte[] data1;
            byte[] data5 = BitConverter.GetBytes(0);
            LogSv.Log.log.Write("力控启动软着陆", Color.Green);
            //设置位置模式
            rtn = GTN.mc.GTN_SetEcatAxisMode(1, 11, 8);


            //设置第一段PP速度MM/S
            data1 = BitConverter.GetBytes((int)FirstPPSpd * 5000);           //10
            index = ushort.Parse("6081", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(4), out code);

            //设置第一段PP段距离MM
            data1 = BitConverter.GetBytes((int)FirstPPDis * 5000);           //16
            index = ushort.Parse("212C", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(4), out code);

            //设置第一段加速度
            data1 = BitConverter.GetBytes(200 * 5000);
            index = ushort.Parse("6083", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(4), out code);

            //设置第一段减速度
            data1 = BitConverter.GetBytes(200 * 5000);
            index = ushort.Parse("6084", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(4), out code);

            //设置第二段PV速度
            data1 = BitConverter.GetBytes(3 * 5000);            //0
            index = ushort.Parse("2127", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(4), out code);

            //设置第二段PV距离MM
            data1 = BitConverter.GetBytes((int)SecPVDis * 5000);            //0
            index = ushort.Parse("212A", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(4), out code);

            //设置第三段PT速度MM/S
            data1 = BitConverter.GetBytes((int)thirdPTSpd*5000);                    //2500
            index = ushort.Parse("2706", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(1), ref data1[0], Convert.ToUInt32(4), out code);

            //完成速度阈值
            data1 = BitConverter.GetBytes(210);
            index = ushort.Parse("2001", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);


            //保压压力值（）
            data1 = BitConverter.GetBytes((int)Preesure);                     //900
            index = ushort.Parse("2236", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);

            //正向扭矩
            data1 = BitConverter.GetBytes((int)Preesure);                    //900
            index = ushort.Parse("60E0", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);

            //读取当前位置
            index = ushort.Parse("6064", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_GetEcatSlavePdo(1, 11, index, Convert.ToByte(0), ref data5[0], Convert.ToUInt32(4));

            //设置返回原位位置mm
            //data1 = BitConverter.GetBytes(0 * 5000);
            index = ushort.Parse("2706", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(2), ref data5[0], Convert.ToUInt32(4), out code);


            //打开软着陆功能
            data1 = BitConverter.GetBytes(1);
            index = ushort.Parse("2126", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);
            //等待10MS

            Thread.Sleep(20);

            //启动软着陆功能
            data1 = BitConverter.GetBytes(1);
            index = ushort.Parse("2128", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);

            //开始读数据
            //等待软着陆完成
            byte[] data2 = new byte[4];
            byte[] data3 = new byte[4];
            byte[] data4 = new byte[4];
            uint dataout2 = 0;
            JTimer OT = new JTimer();
            dataList.Clear();
            OT.Restart();
            bool stopFlag = false;
            Task task1;
            Task task2;
            task1 = Task.Factory.StartNew(() => 
            {
                do
                {
                    index = ushort.Parse("2801", System.Globalization.NumberStyles.HexNumber);
                    rtn = GTN.mc.GTN_EcatSDOUpload(1, 11, index, Convert.ToByte(0), out data3[0], Convert.ToUInt32(4), out code, out code);                   
                    Thread.Sleep(1);
                    if (data3[0] == 1 || OT.On(20000) || stopFlag)
                    {
                        stopFlag = true;
                        LogSv.Log.log.Write("软着陆完成", Color.Green);
                        break;
                    }                      
                } while (true);
            });
            task2 = Task.Factory.StartNew(() =>
            {
                int i = 0;
                CurvesParm curvesParm = new CurvesParm();
                List<double> data = new List<double>();
                List<double> time = new List<double>();
                do
                {
                    //读取电流值
                    index = ushort.Parse("6078", System.Globalization.NumberStyles.HexNumber);
                    rtn = GTN.mc.GTN_GetEcatSlavePdo(1, 11, index, Convert.ToByte(0), ref data2[0], Convert.ToUInt32(4));

                    //读取当前位置
                    index = ushort.Parse("6064", System.Globalization.NumberStyles.HexNumber);
                    rtn = GTN.mc.GTN_GetEcatSlavePdo(1, 11, index, Convert.ToByte(0), ref data4[0], Convert.ToUInt32(4));

                    int tt1 = BitConverter.ToInt32(data4, 0);
                    if ((tt1 / 5000.0) > 21)
                    {
                        StopSoftLand();
                        LogSv.Log.log.Write($"位移超限，当前位移：" + (tt1 / 5000.0).ToString("f3"), Color.Red);
                        stopFlag = true;
                    }
                    int tt = data2[1] * 255 + data2[0];
                    if (tt < 500)
                    {
                        i++;
                        time.Add(i);
                        data.Add(double.Parse(tt.ToString()) * 3.7);
                    }
                        
                    Thread.Sleep(25);
                    if (stopFlag || OT.On(20000))
                    {
                        curvesParm.dataList = data;
                        curvesParm.timeList = time;
                        curvesParm.SaveImgPath = @"D:\WorkLog\ForceCurve\";
                        curvesParm.SN = SN;
                        curvesParm.MaxVal = (float)(tt * 3.7);
                        dataList.Add(curvesParm);
                        LogSv.Log.log.Write($"电流值="+(tt*3.7).ToString("f3"), Color.Red);
                        LogSv.Log.log.Write($"位置值=" + (tt1/5000.0).ToString("f3"), Color.Red);
                        break;
                    }
                } while (true);
            });
            Task.WaitAll(task1,task2);
            return 0;
        }


        public static void StopSoftLand()
        {
            short rtn;
            uint code;
            ushort index;
            byte[] data1;
            byte[] data3;
            JTimer OT = new JTimer();
            OT.Restart();
            LogSv.Log.log.Write("启动软着陆返回", Color.Green);
            //启动返回原位标志
            data1 = BitConverter.GetBytes(1);
            index = ushort.Parse("212B", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);


            //等待返回原位标志完成
            uint dataout3;
            data3 = new byte[4];
            data1 = BitConverter.GetBytes(1);
            OT.Restart();
            while (true)
            {
                index = ushort.Parse("2801", System.Globalization.NumberStyles.HexNumber);
                rtn = GTN.mc.GTN_EcatSDOUpload(1, 11, index, Convert.ToByte(0), out data3[0], Convert.ToUInt32(4), out dataout3, out code);
                if (data3[0].ToString() == "0" || OT.On(10000))
                {
                    break;
                }
            }

            //返回位置清零mm
            data1 = BitConverter.GetBytes(0 * 5000);
            index = ushort.Parse("2706", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(2), ref data1[0], Convert.ToUInt32(4), out code);

            //关闭返回原点标志
            data1 = BitConverter.GetBytes(0);
            index = ushort.Parse("212B", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);

            //恢复正向扭矩
            data1 = BitConverter.GetBytes(30000);
            index = ushort.Parse("60E0", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);

            //恢复反向扭矩
            data1 = BitConverter.GetBytes(30000);
            index = ushort.Parse("60E1", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);

            //停止软着陆功能
            data1 = BitConverter.GetBytes(0);
            index = ushort.Parse("2128", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);

            Thread.Sleep(20);

            //关闭软着陆功能
            data1 = BitConverter.GetBytes(0);
            index = ushort.Parse("2126", System.Globalization.NumberStyles.HexNumber);
            rtn = GTN.mc.GTN_EcatSDODownload(1, 11, index, Convert.ToByte(0), ref data1[0], Convert.ToUInt32(2), out code);

            LogSv.Log.log.Write("软着陆返回完成", Color.Green);
        }
    }
}
