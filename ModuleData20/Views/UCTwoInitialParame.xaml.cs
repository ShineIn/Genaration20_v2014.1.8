﻿using Googo.Manul.Views;
using ModuleData.ViewModels;
using ModuleData20.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ModuleData20
{
    /// <summary>
    /// UCDropGlue.xaml 的交互逻辑
    /// </summary>
    public partial class UCTwoInitialParame : UserControl
    {
        public UCTwoInitialParame()
        {
            InitializeComponent();
        }


        public static DependencyProperty SubModuleProperty = DependencyProperty.Register("SubModule", typeof(StationTwoIntial), typeof(UCTwoInitialParame), new FrameworkPropertyMetadata(new StationTwoIntial(),FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public StationTwoIntial SubModule
        {
            get { return (StationTwoIntial)GetValue(SubModuleProperty); }
            set { SetValue(SubModuleProperty, value); }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (null == SubModule) return;
            WinCommuObject winCommu = new WinCommuObject(SubModule);
            winCommu.Show();
        }
    }
}
