﻿namespace ModuleData20
{
    partial class uc_GlueInitProcess
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.UVup = new YHSDK.NPFlowChart();
            this.fc_GlueAuto2 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto3 = new YHSDK.NPFlowChart();
            this.npFlowChart1 = new YHSDK.NPFlowChart();
            this.exptionFC = new YHSDK.NPFlowChart();
            this.fc_GlueAuto1 = new YHSDK.NPFlowChart();
            this.npFlowChart2 = new YHSDK.NPFlowChart();
            this.npFlowChart5 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto6 = new YHSDK.NPFlowChart();
            this.npFlowChart8 = new YHSDK.NPFlowChart();
            this.npFlowChart10 = new YHSDK.NPFlowChart();
            this.npFlowChart12 = new YHSDK.NPFlowChart();
            this.npFlowChart13 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto7 = new YHSDK.NPFlowChart();
            this.SuspendLayout();
            // 
            // UVup
            // 
            this.UVup.AlarmCode = "";
            this.UVup.ArrowsWidth = 1;
            this.UVup.BackColor = System.Drawing.Color.White;
            this.UVup.bClearTrace = false;
            this.UVup.bIsLastFlowChart = false;
            this.UVup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UVup.bSkip = false;
            this.UVup.bStepByStepMode = false;
            this.UVup.CASE1 = null;
            this.UVup.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.UVup.CASE1Color = System.Drawing.Color.LimeGreen;
            this.UVup.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.UVup.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.UVup.CASE2 = null;
            this.UVup.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.UVup.CASE2Color = System.Drawing.Color.Red;
            this.UVup.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.UVup.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.UVup.CASE3 = null;
            this.UVup.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.UVup.CASE3Color = System.Drawing.Color.Blue;
            this.UVup.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.UVup.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.UVup.DefaultColor = System.Drawing.Color.White;
            this.UVup.eSkipPath = YHSDK.FCResultType.NEXT;
            this.UVup.ExecutedColor = System.Drawing.Color.LightGray;
            this.UVup.ExecutingColor = System.Drawing.Color.Lime;
            this.UVup.Location = new System.Drawing.Point(7, 113);
            this.UVup.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.UVup.menuOpening = false;
            this.UVup.Name = "UVup";
            this.UVup.NEXT = this.fc_GlueAuto2;
            this.UVup.NextArrowType = YHSDK.ArrowTypes.Line;
            this.UVup.NEXTColor = System.Drawing.Color.Black;
            this.UVup.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.UVup.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.UVup.Size = new System.Drawing.Size(241, 29);
            this.UVup.SkipColor = System.Drawing.Color.Yellow;
            this.UVup.SubFlowChart = null;
            this.UVup.TabIndex = 82;
            this.UVup.Text = "UV气缸抬起";
            this.UVup.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.UVup.TimeOut = 10000;
            this.UVup.TimeoutColor = System.Drawing.Color.Red;
            this.UVup.tmrTimeOut = null;
            this.UVup.FlowRun += new YHSDK.FlowRunEvent(this.UVup_FlowRun);
            // 
            // fc_GlueAuto2
            // 
            this.fc_GlueAuto2.AlarmCode = "";
            this.fc_GlueAuto2.ArrowsWidth = 1;
            this.fc_GlueAuto2.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto2.bClearTrace = false;
            this.fc_GlueAuto2.bIsLastFlowChart = false;
            this.fc_GlueAuto2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto2.bSkip = false;
            this.fc_GlueAuto2.bStepByStepMode = false;
            this.fc_GlueAuto2.CASE1 = null;
            this.fc_GlueAuto2.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto2.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto2.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.CASE2 = null;
            this.fc_GlueAuto2.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto2.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto2.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.CASE3 = null;
            this.fc_GlueAuto2.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto2.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto2.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto2.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto2.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto2.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto2.Location = new System.Drawing.Point(7, 168);
            this.fc_GlueAuto2.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto2.menuOpening = false;
            this.fc_GlueAuto2.Name = "fc_GlueAuto2";
            this.fc_GlueAuto2.NEXT = this.fc_GlueAuto3;
            this.fc_GlueAuto2.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto2.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto2.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.Size = new System.Drawing.Size(241, 32);
            this.fc_GlueAuto2.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto2.SubFlowChart = null;
            this.fc_GlueAuto2.TabIndex = 0;
            this.fc_GlueAuto2.Text = "判断视觉软件是否打开";
            this.fc_GlueAuto2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto2.TimeOut = 10000;
            this.fc_GlueAuto2.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto2.tmrTimeOut = null;
            this.fc_GlueAuto2.FlowRun += new YHSDK.FlowRunEvent(this.npFlowChart2_FlowRun);
            // 
            // fc_GlueAuto3
            // 
            this.fc_GlueAuto3.AlarmCode = "";
            this.fc_GlueAuto3.ArrowsWidth = 1;
            this.fc_GlueAuto3.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto3.bClearTrace = false;
            this.fc_GlueAuto3.bIsLastFlowChart = false;
            this.fc_GlueAuto3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto3.bSkip = false;
            this.fc_GlueAuto3.bStepByStepMode = false;
            this.fc_GlueAuto3.CASE1 = null;
            this.fc_GlueAuto3.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto3.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto3.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.CASE2 = null;
            this.fc_GlueAuto3.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto3.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto3.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.CASE3 = null;
            this.fc_GlueAuto3.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto3.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto3.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto3.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto3.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto3.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto3.Location = new System.Drawing.Point(7, 218);
            this.fc_GlueAuto3.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto3.menuOpening = false;
            this.fc_GlueAuto3.Name = "fc_GlueAuto3";
            this.fc_GlueAuto3.NEXT = this.npFlowChart1;
            this.fc_GlueAuto3.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto3.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto3.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.Size = new System.Drawing.Size(241, 32);
            this.fc_GlueAuto3.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto3.SubFlowChart = null;
            this.fc_GlueAuto3.TabIndex = 0;
            this.fc_GlueAuto3.Text = "连接视觉软件通讯";
            this.fc_GlueAuto3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto3.TimeOut = 10000;
            this.fc_GlueAuto3.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto3.tmrTimeOut = null;
            this.fc_GlueAuto3.FlowRun += new YHSDK.FlowRunEvent(this.npFlowChart3_FlowRun);
            // 
            // npFlowChart1
            // 
            this.npFlowChart1.AlarmCode = "";
            this.npFlowChart1.ArrowsWidth = 1;
            this.npFlowChart1.BackColor = System.Drawing.Color.White;
            this.npFlowChart1.bClearTrace = false;
            this.npFlowChart1.bIsLastFlowChart = false;
            this.npFlowChart1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.npFlowChart1.bSkip = false;
            this.npFlowChart1.bStepByStepMode = false;
            this.npFlowChart1.CASE1 = this.exptionFC;
            this.npFlowChart1.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart1.CASE1Color = System.Drawing.Color.LimeGreen;
            this.npFlowChart1.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.CASE2 = null;
            this.npFlowChart1.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart1.CASE2Color = System.Drawing.Color.Red;
            this.npFlowChart1.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.CASE3 = null;
            this.npFlowChart1.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart1.CASE3Color = System.Drawing.Color.Blue;
            this.npFlowChart1.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.DefaultColor = System.Drawing.Color.White;
            this.npFlowChart1.eSkipPath = YHSDK.FCResultType.NEXT;
            this.npFlowChart1.ExecutedColor = System.Drawing.Color.LightGray;
            this.npFlowChart1.ExecutingColor = System.Drawing.Color.Lime;
            this.npFlowChart1.Location = new System.Drawing.Point(7, 272);
            this.npFlowChart1.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.npFlowChart1.menuOpening = false;
            this.npFlowChart1.Name = "npFlowChart1";
            this.npFlowChart1.NEXT = this.npFlowChart2;
            this.npFlowChart1.NextArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart1.NEXTColor = System.Drawing.Color.Black;
            this.npFlowChart1.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.Size = new System.Drawing.Size(241, 32);
            this.npFlowChart1.SkipColor = System.Drawing.Color.Yellow;
            this.npFlowChart1.SubFlowChart = null;
            this.npFlowChart1.TabIndex = 83;
            this.npFlowChart1.Text = "RX盖板上升复位";
            this.npFlowChart1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.npFlowChart1.TimeOut = 10000;
            this.npFlowChart1.TimeoutColor = System.Drawing.Color.Red;
            this.npFlowChart1.tmrTimeOut = null;
            this.npFlowChart1.FlowRun += new YHSDK.FlowRunEvent(this.npFlowChart1_FlowRun_1);
            // 
            // exptionFC
            // 
            this.exptionFC.AlarmCode = "";
            this.exptionFC.ArrowsWidth = 1;
            this.exptionFC.BackColor = System.Drawing.Color.White;
            this.exptionFC.bClearTrace = false;
            this.exptionFC.bIsLastFlowChart = false;
            this.exptionFC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.exptionFC.bSkip = false;
            this.exptionFC.bStepByStepMode = false;
            this.exptionFC.CASE1 = null;
            this.exptionFC.Case1ArrowType = YHSDK.ArrowTypes.Square;
            this.exptionFC.CASE1Color = System.Drawing.Color.LimeGreen;
            this.exptionFC.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.exptionFC.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.exptionFC.CASE2 = null;
            this.exptionFC.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.exptionFC.CASE2Color = System.Drawing.Color.Red;
            this.exptionFC.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.exptionFC.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.exptionFC.CASE3 = null;
            this.exptionFC.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.exptionFC.CASE3Color = System.Drawing.Color.Blue;
            this.exptionFC.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.exptionFC.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.exptionFC.DefaultColor = System.Drawing.Color.White;
            this.exptionFC.eSkipPath = YHSDK.FCResultType.NEXT;
            this.exptionFC.ExecutedColor = System.Drawing.Color.LightGray;
            this.exptionFC.ExecutingColor = System.Drawing.Color.Lime;
            this.exptionFC.Location = new System.Drawing.Point(322, 196);
            this.exptionFC.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.exptionFC.menuOpening = false;
            this.exptionFC.Name = "exptionFC";
            this.exptionFC.NEXT = this.fc_GlueAuto1;
            this.exptionFC.NextArrowType = YHSDK.ArrowTypes.Line;
            this.exptionFC.NEXTColor = System.Drawing.Color.Black;
            this.exptionFC.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.exptionFC.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.exptionFC.Size = new System.Drawing.Size(141, 29);
            this.exptionFC.SkipColor = System.Drawing.Color.Yellow;
            this.exptionFC.SubFlowChart = null;
            this.exptionFC.TabIndex = 74;
            this.exptionFC.Text = "异常处理";
            this.exptionFC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.exptionFC.TimeOut = 10000;
            this.exptionFC.TimeoutColor = System.Drawing.Color.Red;
            this.exptionFC.tmrTimeOut = null;
            this.exptionFC.FlowRun += new YHSDK.FlowRunEvent(this.exptionFC_FlowRun);
            // 
            // fc_GlueAuto1
            // 
            this.fc_GlueAuto1.AlarmCode = "";
            this.fc_GlueAuto1.ArrowsWidth = 1;
            this.fc_GlueAuto1.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto1.bClearTrace = false;
            this.fc_GlueAuto1.bIsLastFlowChart = false;
            this.fc_GlueAuto1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto1.bSkip = false;
            this.fc_GlueAuto1.bStepByStepMode = false;
            this.fc_GlueAuto1.CASE1 = null;
            this.fc_GlueAuto1.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto1.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.CASE2 = null;
            this.fc_GlueAuto1.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto1.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.CASE3 = null;
            this.fc_GlueAuto1.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto1.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto1.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto1.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto1.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto1.Location = new System.Drawing.Point(7, 59);
            this.fc_GlueAuto1.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto1.menuOpening = false;
            this.fc_GlueAuto1.Name = "fc_GlueAuto1";
            this.fc_GlueAuto1.NEXT = this.UVup;
            this.fc_GlueAuto1.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto1.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Size = new System.Drawing.Size(241, 32);
            this.fc_GlueAuto1.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto1.SubFlowChart = null;
            this.fc_GlueAuto1.TabIndex = 0;
            this.fc_GlueAuto1.Text = "复位启动";
            this.fc_GlueAuto1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto1.TimeOut = 10000;
            this.fc_GlueAuto1.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto1.tmrTimeOut = null;
            this.fc_GlueAuto1.FlowRun += new YHSDK.FlowRunEvent(this.npFlowChart1_FlowRun);
            // 
            // npFlowChart2
            // 
            this.npFlowChart2.AlarmCode = "";
            this.npFlowChart2.ArrowsWidth = 1;
            this.npFlowChart2.BackColor = System.Drawing.Color.White;
            this.npFlowChart2.bClearTrace = false;
            this.npFlowChart2.bIsLastFlowChart = false;
            this.npFlowChart2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.npFlowChart2.bSkip = false;
            this.npFlowChart2.bStepByStepMode = false;
            this.npFlowChart2.CASE1 = this.exptionFC;
            this.npFlowChart2.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart2.CASE1Color = System.Drawing.Color.LimeGreen;
            this.npFlowChart2.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart2.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart2.CASE2 = null;
            this.npFlowChart2.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart2.CASE2Color = System.Drawing.Color.Red;
            this.npFlowChart2.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart2.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart2.CASE3 = null;
            this.npFlowChart2.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart2.CASE3Color = System.Drawing.Color.Blue;
            this.npFlowChart2.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart2.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart2.DefaultColor = System.Drawing.Color.White;
            this.npFlowChart2.eSkipPath = YHSDK.FCResultType.NEXT;
            this.npFlowChart2.ExecutedColor = System.Drawing.Color.LightGray;
            this.npFlowChart2.ExecutingColor = System.Drawing.Color.Lime;
            this.npFlowChart2.Location = new System.Drawing.Point(7, 327);
            this.npFlowChart2.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.npFlowChart2.menuOpening = false;
            this.npFlowChart2.Name = "npFlowChart2";
            this.npFlowChart2.NEXT = this.npFlowChart5;
            this.npFlowChart2.NextArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart2.NEXTColor = System.Drawing.Color.Black;
            this.npFlowChart2.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart2.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart2.Size = new System.Drawing.Size(241, 32);
            this.npFlowChart2.SkipColor = System.Drawing.Color.Yellow;
            this.npFlowChart2.SubFlowChart = null;
            this.npFlowChart2.TabIndex = 84;
            this.npFlowChart2.Text = "复位双相机Z3轴";
            this.npFlowChart2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.npFlowChart2.TimeOut = 10000;
            this.npFlowChart2.TimeoutColor = System.Drawing.Color.Red;
            this.npFlowChart2.tmrTimeOut = null;
            this.npFlowChart2.FlowRun += new YHSDK.FlowRunEvent(this.npFlowChart2_FlowRun_1);
            // 
            // npFlowChart5
            // 
            this.npFlowChart5.AlarmCode = "";
            this.npFlowChart5.ArrowsWidth = 1;
            this.npFlowChart5.BackColor = System.Drawing.Color.White;
            this.npFlowChart5.bClearTrace = false;
            this.npFlowChart5.bIsLastFlowChart = false;
            this.npFlowChart5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.npFlowChart5.bSkip = false;
            this.npFlowChart5.bStepByStepMode = false;
            this.npFlowChart5.CASE1 = this.exptionFC;
            this.npFlowChart5.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart5.CASE1Color = System.Drawing.Color.LimeGreen;
            this.npFlowChart5.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart5.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart5.CASE2 = null;
            this.npFlowChart5.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart5.CASE2Color = System.Drawing.Color.Red;
            this.npFlowChart5.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart5.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart5.CASE3 = null;
            this.npFlowChart5.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart5.CASE3Color = System.Drawing.Color.Blue;
            this.npFlowChart5.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart5.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart5.DefaultColor = System.Drawing.Color.White;
            this.npFlowChart5.eSkipPath = YHSDK.FCResultType.NEXT;
            this.npFlowChart5.ExecutedColor = System.Drawing.Color.LightGray;
            this.npFlowChart5.ExecutingColor = System.Drawing.Color.Lime;
            this.npFlowChart5.Location = new System.Drawing.Point(322, 327);
            this.npFlowChart5.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.npFlowChart5.menuOpening = false;
            this.npFlowChart5.Name = "npFlowChart5";
            this.npFlowChart5.NEXT = this.fc_GlueAuto6;
            this.npFlowChart5.NextArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart5.NEXTColor = System.Drawing.Color.Black;
            this.npFlowChart5.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart5.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart5.Size = new System.Drawing.Size(141, 32);
            this.npFlowChart5.SkipColor = System.Drawing.Color.Yellow;
            this.npFlowChart5.SubFlowChart = null;
            this.npFlowChart5.TabIndex = 86;
            this.npFlowChart5.Text = "Z3轴移到安全位";
            this.npFlowChart5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.npFlowChart5.TimeOut = 10000;
            this.npFlowChart5.TimeoutColor = System.Drawing.Color.Red;
            this.npFlowChart5.tmrTimeOut = null;
            this.npFlowChart5.FlowRun += new YHSDK.FlowRunEvent(this.npFlowChart5_FlowRun);
            // 
            // fc_GlueAuto6
            // 
            this.fc_GlueAuto6.AlarmCode = "";
            this.fc_GlueAuto6.ArrowsWidth = 1;
            this.fc_GlueAuto6.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto6.bClearTrace = false;
            this.fc_GlueAuto6.bIsLastFlowChart = false;
            this.fc_GlueAuto6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto6.bSkip = false;
            this.fc_GlueAuto6.bStepByStepMode = false;
            this.fc_GlueAuto6.CASE1 = this.exptionFC;
            this.fc_GlueAuto6.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto6.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto6.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.CASE2 = null;
            this.fc_GlueAuto6.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto6.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto6.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.CASE3 = null;
            this.fc_GlueAuto6.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto6.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto6.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto6.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto6.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto6.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto6.Location = new System.Drawing.Point(520, 327);
            this.fc_GlueAuto6.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto6.menuOpening = false;
            this.fc_GlueAuto6.Name = "fc_GlueAuto6";
            this.fc_GlueAuto6.NEXT = this.npFlowChart8;
            this.fc_GlueAuto6.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto6.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto6.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.Size = new System.Drawing.Size(241, 29);
            this.fc_GlueAuto6.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto6.SubFlowChart = null;
            this.fc_GlueAuto6.TabIndex = 37;
            this.fc_GlueAuto6.Text = "复位双点胶模组";
            this.fc_GlueAuto6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto6.TimeOut = 10000;
            this.fc_GlueAuto6.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto6.tmrTimeOut = null;
            this.fc_GlueAuto6.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto6_FlowRun);
            // 
            // npFlowChart8
            // 
            this.npFlowChart8.AlarmCode = "";
            this.npFlowChart8.ArrowsWidth = 1;
            this.npFlowChart8.BackColor = System.Drawing.Color.White;
            this.npFlowChart8.bClearTrace = false;
            this.npFlowChart8.bIsLastFlowChart = false;
            this.npFlowChart8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.npFlowChart8.bSkip = false;
            this.npFlowChart8.bStepByStepMode = false;
            this.npFlowChart8.CASE1 = this.exptionFC;
            this.npFlowChart8.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart8.CASE1Color = System.Drawing.Color.LimeGreen;
            this.npFlowChart8.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart8.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart8.CASE2 = null;
            this.npFlowChart8.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart8.CASE2Color = System.Drawing.Color.Red;
            this.npFlowChart8.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart8.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart8.CASE3 = null;
            this.npFlowChart8.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart8.CASE3Color = System.Drawing.Color.Blue;
            this.npFlowChart8.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart8.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart8.DefaultColor = System.Drawing.Color.White;
            this.npFlowChart8.eSkipPath = YHSDK.FCResultType.NEXT;
            this.npFlowChart8.ExecutedColor = System.Drawing.Color.LightGray;
            this.npFlowChart8.ExecutingColor = System.Drawing.Color.Lime;
            this.npFlowChart8.Location = new System.Drawing.Point(520, 274);
            this.npFlowChart8.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.npFlowChart8.menuOpening = false;
            this.npFlowChart8.Name = "npFlowChart8";
            this.npFlowChart8.NEXT = this.npFlowChart10;
            this.npFlowChart8.NextArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart8.NEXTColor = System.Drawing.Color.Black;
            this.npFlowChart8.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart8.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart8.Size = new System.Drawing.Size(241, 29);
            this.npFlowChart8.SkipColor = System.Drawing.Color.Yellow;
            this.npFlowChart8.SubFlowChart = null;
            this.npFlowChart8.TabIndex = 89;
            this.npFlowChart8.Text = "复位Z4轴";
            this.npFlowChart8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.npFlowChart8.TimeOut = 10000;
            this.npFlowChart8.TimeoutColor = System.Drawing.Color.Red;
            this.npFlowChart8.tmrTimeOut = null;
            this.npFlowChart8.FlowRun += new YHSDK.FlowRunEvent(this.npFlowChart8_FlowRun_1);
            // 
            // npFlowChart10
            // 
            this.npFlowChart10.AlarmCode = "";
            this.npFlowChart10.ArrowsWidth = 1;
            this.npFlowChart10.BackColor = System.Drawing.Color.White;
            this.npFlowChart10.bClearTrace = false;
            this.npFlowChart10.bIsLastFlowChart = false;
            this.npFlowChart10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.npFlowChart10.bSkip = false;
            this.npFlowChart10.bStepByStepMode = false;
            this.npFlowChart10.CASE1 = this.exptionFC;
            this.npFlowChart10.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart10.CASE1Color = System.Drawing.Color.LimeGreen;
            this.npFlowChart10.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart10.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart10.CASE2 = null;
            this.npFlowChart10.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart10.CASE2Color = System.Drawing.Color.Red;
            this.npFlowChart10.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart10.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart10.CASE3 = null;
            this.npFlowChart10.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart10.CASE3Color = System.Drawing.Color.Blue;
            this.npFlowChart10.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart10.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart10.DefaultColor = System.Drawing.Color.White;
            this.npFlowChart10.eSkipPath = YHSDK.FCResultType.NEXT;
            this.npFlowChart10.ExecutedColor = System.Drawing.Color.LightGray;
            this.npFlowChart10.ExecutingColor = System.Drawing.Color.Lime;
            this.npFlowChart10.Location = new System.Drawing.Point(520, 221);
            this.npFlowChart10.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.npFlowChart10.menuOpening = false;
            this.npFlowChart10.Name = "npFlowChart10";
            this.npFlowChart10.NEXT = this.npFlowChart12;
            this.npFlowChart10.NextArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart10.NEXTColor = System.Drawing.Color.Black;
            this.npFlowChart10.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart10.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart10.Size = new System.Drawing.Size(241, 29);
            this.npFlowChart10.SkipColor = System.Drawing.Color.Yellow;
            this.npFlowChart10.SubFlowChart = null;
            this.npFlowChart10.TabIndex = 91;
            this.npFlowChart10.Text = "复位Y3轴";
            this.npFlowChart10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.npFlowChart10.TimeOut = 10000;
            this.npFlowChart10.TimeoutColor = System.Drawing.Color.Red;
            this.npFlowChart10.tmrTimeOut = null;
            this.npFlowChart10.FlowRun += new YHSDK.FlowRunEvent(this.npFlowChart10_FlowRun);
            // 
            // npFlowChart12
            // 
            this.npFlowChart12.AlarmCode = "";
            this.npFlowChart12.ArrowsWidth = 1;
            this.npFlowChart12.BackColor = System.Drawing.Color.White;
            this.npFlowChart12.bClearTrace = false;
            this.npFlowChart12.bIsLastFlowChart = false;
            this.npFlowChart12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.npFlowChart12.bSkip = false;
            this.npFlowChart12.bStepByStepMode = false;
            this.npFlowChart12.CASE1 = this.exptionFC;
            this.npFlowChart12.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart12.CASE1Color = System.Drawing.Color.LimeGreen;
            this.npFlowChart12.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart12.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart12.CASE2 = null;
            this.npFlowChart12.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart12.CASE2Color = System.Drawing.Color.Red;
            this.npFlowChart12.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart12.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart12.CASE3 = null;
            this.npFlowChart12.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart12.CASE3Color = System.Drawing.Color.Blue;
            this.npFlowChart12.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart12.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart12.DefaultColor = System.Drawing.Color.White;
            this.npFlowChart12.eSkipPath = YHSDK.FCResultType.NEXT;
            this.npFlowChart12.ExecutedColor = System.Drawing.Color.LightGray;
            this.npFlowChart12.ExecutingColor = System.Drawing.Color.Lime;
            this.npFlowChart12.Location = new System.Drawing.Point(520, 171);
            this.npFlowChart12.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.npFlowChart12.menuOpening = false;
            this.npFlowChart12.Name = "npFlowChart12";
            this.npFlowChart12.NEXT = this.npFlowChart13;
            this.npFlowChart12.NextArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart12.NEXTColor = System.Drawing.Color.Black;
            this.npFlowChart12.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart12.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart12.Size = new System.Drawing.Size(241, 29);
            this.npFlowChart12.SkipColor = System.Drawing.Color.Yellow;
            this.npFlowChart12.SubFlowChart = null;
            this.npFlowChart12.TabIndex = 93;
            this.npFlowChart12.Text = "复位微动平台";
            this.npFlowChart12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.npFlowChart12.TimeOut = 10000;
            this.npFlowChart12.TimeoutColor = System.Drawing.Color.Red;
            this.npFlowChart12.tmrTimeOut = null;
            this.npFlowChart12.FlowRun += new YHSDK.FlowRunEvent(this.npFlowChart12_FlowRun);
            // 
            // npFlowChart13
            // 
            this.npFlowChart13.AlarmCode = "";
            this.npFlowChart13.ArrowsWidth = 1;
            this.npFlowChart13.BackColor = System.Drawing.Color.White;
            this.npFlowChart13.bClearTrace = false;
            this.npFlowChart13.bIsLastFlowChart = false;
            this.npFlowChart13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.npFlowChart13.bSkip = false;
            this.npFlowChart13.bStepByStepMode = false;
            this.npFlowChart13.CASE1 = this.exptionFC;
            this.npFlowChart13.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart13.CASE1Color = System.Drawing.Color.LimeGreen;
            this.npFlowChart13.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart13.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart13.CASE2 = null;
            this.npFlowChart13.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart13.CASE2Color = System.Drawing.Color.Red;
            this.npFlowChart13.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart13.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart13.CASE3 = null;
            this.npFlowChart13.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart13.CASE3Color = System.Drawing.Color.Blue;
            this.npFlowChart13.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart13.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart13.DefaultColor = System.Drawing.Color.White;
            this.npFlowChart13.eSkipPath = YHSDK.FCResultType.NEXT;
            this.npFlowChart13.ExecutedColor = System.Drawing.Color.LightGray;
            this.npFlowChart13.ExecutingColor = System.Drawing.Color.Lime;
            this.npFlowChart13.Location = new System.Drawing.Point(520, 109);
            this.npFlowChart13.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.npFlowChart13.menuOpening = false;
            this.npFlowChart13.Name = "npFlowChart13";
            this.npFlowChart13.NEXT = this.fc_GlueAuto7;
            this.npFlowChart13.NextArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart13.NEXTColor = System.Drawing.Color.Black;
            this.npFlowChart13.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart13.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart13.Size = new System.Drawing.Size(241, 32);
            this.npFlowChart13.SkipColor = System.Drawing.Color.Yellow;
            this.npFlowChart13.SubFlowChart = null;
            this.npFlowChart13.TabIndex = 95;
            this.npFlowChart13.Text = "复位移栽平台";
            this.npFlowChart13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.npFlowChart13.TimeOut = 10000;
            this.npFlowChart13.TimeoutColor = System.Drawing.Color.Red;
            this.npFlowChart13.tmrTimeOut = null;
            this.npFlowChart13.FlowRun += new YHSDK.FlowRunEvent(this.npFlowChart13_FlowRun);
            // 
            // fc_GlueAuto7
            // 
            this.fc_GlueAuto7.AlarmCode = "";
            this.fc_GlueAuto7.ArrowsWidth = 1;
            this.fc_GlueAuto7.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto7.bClearTrace = false;
            this.fc_GlueAuto7.bIsLastFlowChart = false;
            this.fc_GlueAuto7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto7.bSkip = false;
            this.fc_GlueAuto7.bStepByStepMode = false;
            this.fc_GlueAuto7.CASE1 = null;
            this.fc_GlueAuto7.Case1ArrowType = YHSDK.ArrowTypes.Square;
            this.fc_GlueAuto7.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto7.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.fc_GlueAuto7.CASE2 = null;
            this.fc_GlueAuto7.Case2ArrowType = YHSDK.ArrowTypes.Square;
            this.fc_GlueAuto7.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto7.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.CASE3 = null;
            this.fc_GlueAuto7.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto7.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto7.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto7.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto7.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto7.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto7.Location = new System.Drawing.Point(520, 59);
            this.fc_GlueAuto7.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto7.menuOpening = false;
            this.fc_GlueAuto7.Name = "fc_GlueAuto7";
            this.fc_GlueAuto7.NEXT = this.fc_GlueAuto1;
            this.fc_GlueAuto7.NextArrowType = YHSDK.ArrowTypes.Square;
            this.fc_GlueAuto7.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto7.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.NEXTStartPoint = YHSDK.FlowchartSides.Left;
            this.fc_GlueAuto7.Size = new System.Drawing.Size(241, 32);
            this.fc_GlueAuto7.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto7.SubFlowChart = null;
            this.fc_GlueAuto7.TabIndex = 0;
            this.fc_GlueAuto7.Text = "初始化完成";
            this.fc_GlueAuto7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto7.TimeOut = 10000;
            this.fc_GlueAuto7.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto7.tmrTimeOut = null;
            this.fc_GlueAuto7.FlowRun += new YHSDK.FlowRunEvent(this.npFlowChart8_FlowRun);
            // 
            // uc_GlueInitProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.npFlowChart13);
            this.Controls.Add(this.npFlowChart12);
            this.Controls.Add(this.npFlowChart10);
            this.Controls.Add(this.npFlowChart8);
            this.Controls.Add(this.npFlowChart5);
            this.Controls.Add(this.npFlowChart2);
            this.Controls.Add(this.npFlowChart1);
            this.Controls.Add(this.UVup);
            this.Controls.Add(this.exptionFC);
            this.Controls.Add(this.fc_GlueAuto6);
            this.Controls.Add(this.fc_GlueAuto7);
            this.Controls.Add(this.fc_GlueAuto3);
            this.Controls.Add(this.fc_GlueAuto2);
            this.Controls.Add(this.fc_GlueAuto1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "uc_GlueInitProcess";
            this.Size = new System.Drawing.Size(777, 431);
            this.ResumeLayout(false);

        }

        #endregion

        private YHSDK.NPFlowChart fc_GlueAuto1;
        private YHSDK.NPFlowChart fc_GlueAuto2;
        private YHSDK.NPFlowChart fc_GlueAuto3;
        private YHSDK.NPFlowChart fc_GlueAuto7;
        private YHSDK.NPFlowChart fc_GlueAuto6;
        private YHSDK.NPFlowChart exptionFC;
        private YHSDK.NPFlowChart UVup;
        private YHSDK.NPFlowChart npFlowChart1;
        private YHSDK.NPFlowChart npFlowChart2;
        private YHSDK.NPFlowChart npFlowChart5;
        private YHSDK.NPFlowChart npFlowChart8;
        private YHSDK.NPFlowChart npFlowChart10;
        private YHSDK.NPFlowChart npFlowChart12;
        private YHSDK.NPFlowChart npFlowChart13;
    }
}
