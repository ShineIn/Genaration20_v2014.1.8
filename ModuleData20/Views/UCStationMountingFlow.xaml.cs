﻿using S7.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using SuperSimpleTcp;
using Infrastructure;
using YHSDK;
using ModuleData20.ViewModels;
using ModuleData;
using ShowAlarm;
using System.Threading;

namespace ModuleData20.Views
{
    /// <summary>
    /// UCStationMountingFlow.xaml 的交互逻辑
    /// </summary>
    public partial class UCStationMountingFlow : UserControl, IModule
    {
        public UCStationMountingFlow()
        {
            InitializeComponent();
        }

        public static DependencyProperty DCClientProperty =
            DependencyProperty.Register("DCClient", typeof(SimpleTcpClient), typeof(UCStationMountingFlow), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));

        public static DependencyProperty HeightSenClientProperty =
    DependencyProperty.Register("HeightSenClient", typeof(SimpleTcpClient), typeof(UCStationMountingFlow), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));

        public static DependencyProperty ExcuteProjectProperty =
DependencyProperty.Register("ExcuteProject", typeof(MountingProject), typeof(UCStationMountingFlow), new FrameworkPropertyMetadata(new MountingProject(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));
        
        public static DependencyProperty StageIndexProperty =
DependencyProperty.Register("StageIndex", typeof(int), typeof(UCStationMountingFlow), new FrameworkPropertyMetadata(1, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));

        public static DependencyProperty AlarmFrmProperty =
DependencyProperty.Register("AlarmFrm", typeof(ShowAlarm.WinShowAlarm), typeof(UCStationMountingFlow), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));

        public static DependencyProperty IsLockParamProperty =
DependencyProperty.Register("IsLockParam", typeof(bool), typeof(UCStationMountingFlow), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));
        /// <summary>
        /// 德创TCP对象
        /// </summary>
        /// <remarks>
        /// TCP对象，到最外层直接进行绑定
        /// </remarks>
        public SimpleTcpClient DCClient
        {
            get { return (SimpleTcpClient)GetValue(DCClientProperty); }
            set { SetValue(DCClientProperty, value); }
        }

        /// <summary>
        /// 测高传感器TCP对象
        /// </summary>
        /// <remarks>
        /// TCP对象，到最外层直接进行绑定
        /// </remarks>
        public SimpleTcpClient HeightSenClient
        {
            get { return (SimpleTcpClient)GetValue(HeightSenClientProperty); }
            set { SetValue(HeightSenClientProperty, value); }
        }

        /// <summary>
        /// 报警提示窗
        /// </summary>
        public ShowAlarm.WinShowAlarm AlarmFrm
        {
            get { return (ShowAlarm.WinShowAlarm)GetValue(AlarmFrmProperty); }
            set { SetValue(AlarmFrmProperty, value); }
        }


        public MountingProject ExcuteProject
        {
            get { return (MountingProject)GetValue(ExcuteProjectProperty); }
            set { SetValue(ExcuteProjectProperty, value); }
        }

        public int StageIndex
        {
            get { return (int)GetValue(StageIndexProperty); }
            set { SetValue(StageIndexProperty, value); }
        }

        public bool IsLockParam
        {
            get { return (bool)GetValue(IsLockParamProperty); }
            set { SetValue(IsLockParamProperty, value); }
        }

        private static void DepenProChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            UCStationMountingFlow flow = d as UCStationMountingFlow;
            if (null == flow) return;
            if (e.Property == DCClientProperty)
            {
                SimpleTcpClient client = e.NewValue as SimpleTcpClient;
                if (null == client) return;
                flow.mounting.DCTcpClient = client;
                flow.discharge.DCTcpClient = client;
                flow.pcmounting.DCTcpClient = client;
            }
            else if (e.Property == HeightSenClientProperty)
            {
                SimpleTcpClient client = e.NewValue as SimpleTcpClient;
                if (null == client) return;
                flow.mounting.SensorClient = client;
                flow.discharge.SensorClient = client;
                flow.pcmounting.SensorClient= client;
            }
            else if (e.Property == ExcuteProjectProperty)
            {
                MountingProject tempPro = e.NewValue as MountingProject;
                if (null == tempPro) return;
                flow.mounting.DataModel = tempPro.MountingExcuteModel;
                flow.discharge.DataModel = tempPro.MountingExcuteModel;
                flow.pcmounting.DataModel = tempPro.MountingExcuteModel;
            }
            else if (e.Property == StageIndexProperty)
            {
                int tempIndex = (int)e.NewValue;
                flow.mounting.StageIndex = tempIndex;
                flow.discharge.StageIndex = tempIndex;
                flow.pcmounting.StageIndex= tempIndex;
            }
            else if (e.Property == AlarmFrmProperty)
            {
                WinShowAlarm tempFrm = (WinShowAlarm)e.NewValue;
                flow.mounting.AlarmFrm = tempFrm;
                flow.discharge.AlarmFrm = tempFrm;
            }

            else if (e.Property == IsLockParamProperty)
            {
                bool tempIsLock = (bool)e.NewValue;
                flow.mounting.IsLockParam = tempIsLock;
                flow.discharge.IsLockParam = tempIsLock;
            }
        }

        public bool IsRun
        {
            get { return mounting.IsRun || discharge.IsRun || pcmounting.IsRun; } 
        }

        public void RunAsync()
        {
            mounting.RunAsync();
            discharge.RunAsync();
            pcmounting.RunAsync();
            RefreshUI();
        }

        public void ResetChart() 
        {
            mounting.ResetChart();
            discharge.ResetChart();
            pcmounting.ResetChart();
        }

        public void Stop()
        {
            mounting.Stop();
            discharge.Stop();
            pcmounting.Stop();
        }
        private bool isScanStop = false;
        private Thread thread_RefreshUI;
        void RefreshUI()
        {
            MountingProject tem = ExcuteProject;
            if (tem == null) return;
            if (thread_RefreshUI != null)
            {
                if (thread_RefreshUI.IsAlive)
                    thread_RefreshUI.Abort();
            }
            thread_RefreshUI = new Thread(new ThreadStart(() =>
            {
                isScanStop = false;
                while (!isScanStop)
                {
                    tem.StationStatus.IsMounting = mounting.IsRun && !mounting.IsWaitForSignal && pcmounting.IsRun /*&& !pcmounting.IsWaitForSignal*/;
                    tem.StationStatus.IsDischarge = discharge.IsRun && !discharge.IsWaitForSignal;
                    System.Threading.Thread.Sleep(200);
                }
            }));
            thread_RefreshUI.IsBackground = true;
            thread_RefreshUI.Start();
        }
    }

    public class MountingModuleRunStatus : NotifyChangedBase
    {
        /// <summary>
        /// 贴装模块是否正在执行
        /// </summary>
        public bool IsMounting
        {
            get { return _isMounting; }
            set { _isMounting = value; OnPropertyChanged(); }
        }
        private bool _isMounting = false;

        /// <summary>
        /// 出料模块是否正在执行
        /// </summary>
        public bool IsDischarge
        {
            get { return _isDischarge; }
            set { _isDischarge = value; OnPropertyChanged(); }
        }
        private bool _isDischarge = false;


    }
}
