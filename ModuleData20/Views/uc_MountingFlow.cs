﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModuleData20.Views;
using ModuleData20.ViewModels;
using YHSDK;
using G4.GlueCore;
using System.Threading;
using Googo.Manul;
using ModuleData;
using System.Diagnostics;
using System.Xml.Serialization;
using SuperSimpleTcp;
using VUControl;
using LogSv;
using ModuleData.ViewModels;
using ShowAlarm;
using LiveCharts.Wpf;
using S7.Net.Types;
using DateTime = System.DateTime;
using static GTN.mc_la;

namespace ModuleData20
{
    public partial class uc_MountingFlow : ModuleBase
    {
        public uc_MountingFlow()
        {
            InitializeComponent();
        }

        #region 公有属性
        protected override void Run()
        {
            if (DCTcpClient == null || !DCTcpClient.IsConnected) return;
            if (npFlowChart10.WorkFlow == null)
                npFlowChart10.TaskReset();
            //CloseAlarmFrm();
            npFlowChart10.TaskRun();
            if (null == initParm)
            {
                MountingProject project = model.ParentStation as MountingProject;
                if (null != project)
                {
                    initParm = project.InitParm;
                }
            }


        }
        public override void ResetChart()
        {
            StartChart = npFlowChart10;
            npFlowChart10.TaskReset();
        }

        public MountingMode DataModel
        {
            get { return model; }
            set { model = value; OnPropertyChanged(); }
        }
        private MountingMode model = new MountingMode();

        /// <summary>
        /// 权限管理是否锁界面参数
        /// </summary>
        public bool IsLockParam
        {
            get { return isLockParam; }
            set { isLockParam = value; SetNudControlLock(value); OnPropertyChanged(); }
        }
        private bool isLockParam = false;


        public GlueProject GlueProject
        {
            get
            {
                MountingProject project = model.ParentStation as MountingProject;
                if (null != project)
                {
                    return project.LeftGlue;
                }
                return null;
            }
        }
        public InitialMode InitParm = null;

        //报警窗体
        public WinShowAlarm AlarmFrm
        {
            get { return alarmFrm; }
            set { alarmFrm = value; OnPropertyChanged(); }
        }
        private WinShowAlarm alarmFrm = null;

        public static int alramCode = 0;

        /// <summary>
        /// 测高传感器TCP对象
        /// </summary>
        /// <remarks>
        /// TCP对象，到最外层直接进行绑定
        /// </remarks>
        [XmlIgnore]
        public SimpleTcpClient SensorClient
        {
            get { return _sensorClient; }
            set
            {
                if (_sensorClient != null && _sensorClient.Events != null)
                {
                    _sensorClient.Events.DataReceived -= sensorDataReceived;
                    _sensorClient.Events.Disconnected -= Events_Disconnected;

                }

                _sensorClient = value;
                if (value != null && value.Events != null)
                {
                    value.Events.DataReceived += sensorDataReceived;
                    value.Events.Disconnected += Events_Disconnected;
                }
                OnPropertyChanged();
            }
        }
        private SimpleTcpClient _sensorClient;

        /// <summary>
        /// 光阑测量高度列表
        /// </summary>
        /// <remarks>
        /// 位移传感器测量出来的值
        /// </remarks>
        [Browsable(false)]
        public List<float> HeightData
        {
            get { return heightData; }
            private set { heightData = value; OnPropertyChanged(); }
        }
        List<float> heightData = new List<float>();

        /// <summary>
        /// 光阑和PCB测量相对高度列表
        /// </summary>
        /// <remarks>
        /// 贴装完后复检光阑和PCB的段差
        /// </remarks>
        [Browsable(false)]
        public List<float> LeftCheckHeightData
        {
            get { return leftCheckHeightData; }
            private set { leftCheckHeightData = value; OnPropertyChanged(); }
        }
        List<float> leftCheckHeightData = new List<float>();

        /// <summary>
        /// 视觉NG调整循环次数
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        [Browsable(false)]
        public int VisNgCount
        {
            get { return visNgCount; }
            private set { visNgCount = value; OnPropertyChanged(); }
        }
        int visNgCount = 0;

        /// <summary>
        /// 光阑平面测高次数
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        [Browsable(false)]
        public int GetHeightTime
        {
            get { return getHeightTime; }
            private set { getHeightTime = value; OnPropertyChanged(); }
        }
        int getHeightTime = 3;


        /// <summary>
        /// 光阑与PCB相对高度测高次数
        /// </summary>
        /// <remarks>
        /// 贴装完后复检光阑和PCB的段差次数
        /// </remarks>
        [Browsable(false)]
        public int LeftCheckHeightTime
        {
            get { return leftCheckHeightTime; }
            private set { leftCheckHeightTime = value; OnPropertyChanged(); }
        }
        int leftCheckHeightTime = 0;


        /// <summary>
        /// 微调组装旋转角度
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        [Browsable(false)]
        public float PcbAngleOffset
        {
            get { return pcbAngleOffset; }
            private set { pcbAngleOffset = value; OnPropertyChanged(); }
        }
        float pcbAngleOffset = 0;

        // <summary>
        /// 微调组装旋转X
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        [Browsable(false)]
        public float PcbOffsetX
        {
            get { return pcbOffsetX; }
            private set { pcbOffsetX = value; OnPropertyChanged(); }
        }
        float pcbOffsetX = 0;

        // <summary>
        /// 微调组装旋转Y
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        [Browsable(false)]
        public float PcbOffsetY
        {
            get { return pcbOffsetY; }
            private set { pcbOffsetY = value; OnPropertyChanged(); }
        }
        float pcbOffsetY = 0;

        // <summary>
        /// 光阑高度检测平面度公差
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        [Browsable(false)]
        public float HeightSpc
        {
            get { return heightSpc; }
            private set { heightSpc = value; OnPropertyChanged(); }
        }
        float heightSpc = 5;

        private StationTwoIntial initParm = null;
        public static List<MesDgvData> MesRecipeDgvDatas = new List<MesDgvData>();
        public static List<MesDgvData> MesLeftReportDgvDatas = new List<MesDgvData>();
        public static List<MesDgvData> MesRightReportDgvDatas = new List<MesDgvData>();

        public static List<CommunicateDgvData> Dispense1MoveReadDgvDatas = new List<CommunicateDgvData>();
        public static List<CommunicateDgvData> Dispense1MoveWriteDgvDatas = new List<CommunicateDgvData>();
        public static List<CommunicateDgvData> Dispense1FlowReadDgvDatas = new List<CommunicateDgvData>();
        public static List<CommunicateDgvData> Dispense1FlowWriteDgvDatas = new List<CommunicateDgvData>();

        public static List<CommunicateDgvData> Dispense2MoveReadDgvDatas = new List<CommunicateDgvData>();
        public static List<CommunicateDgvData> Dispense2MoveWriteDgvDatas = new List<CommunicateDgvData>();
        public static List<CommunicateDgvData> Dispense2FlowReadDgvDatas = new List<CommunicateDgvData>();
        public static List<CommunicateDgvData> Dispense2FlowWriteDgvDatas = new List<CommunicateDgvData>();

        public static Dictionary<string, object> DicRecipeData;

        //public static ResultInfor curesult;
        #endregion

        #region 私有属性
        JTimer AutoRunJTime = new JTimer();
        frmConfirm confirmFrm = new frmConfirm();
        List<double> pcbaDataList = new List<double>();
        List<double> pieceDataList = new List<double>();
        List<double> pcbaSecDataList = new List<double>();
        List<double> pcbaRechkDataList = new List<double>();
        private MountingMode mountingModule;
        private GlueModule glueModule;
        private PositionInfor PosCom;
        private GlueProject LeftGlue = null;
        private string assembModel = string.Empty; //当前型号
        Task AssembModuleTask;
        Task Y3MotorTask;
        int AssembModuleTaskRtn;
        int Y3MotorTaskRtn;
        string productSN = string.Empty;
        Thread thr_SoftLand = null;
        Thread thr_ReadData = null;
        string ImagePath = string.Empty;


        private string VisionNGCode = 1001.ToString();//MES上的ngcode
        private string SensorNGCode = 2001.ToString();
        private string MotionNGCode = 3001.ToString();

        /// <summary>
        /// 传感器TCP客户端接受到的最新信息
        /// </summary>
        /// <remarks>
        /// 获取之后马上清空信息
        /// </remarks>
        [XmlIgnore]
        private string SensorLastContent
        {
            get
            {
                string ret = sensorLastContent;
                sensorLastContent = string.Empty;
                return ret;
            }
            set { sensorLastContent = value; OnPropertyChanged(); }
        }
        private string sensorLastContent = string.Empty;

        #endregion

        #region 私有函数
        /// <summary>
        /// 算法客户端接受信息事件
        /// </summary>
        /// <remarks>
        /// 可以重新该事件，在里面做自己想要的事件处理
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sensorDataReceived(object sender, SuperSimpleTcp.DataReceivedEventArgs e)
        {
            if (e.Data == null) return;
            string strRec = Encoding.UTF8.GetString(e.Data.Array);
            Debug.WriteLine($"Port={e.IpPort}+Data={strRec}");
            SensorLastContent = strRec;
        }

        private void Events_Disconnected(object sender, SuperSimpleTcp.ConnectionEventArgs e)
        {
            Debug.WriteLine($"Port={e.IpPort} Disconnected Reason={e.Reason}");
        }
        SmartGlue.Infrastructure.AxisStatus status = new SmartGlue.Infrastructure.AxisStatus();
        /// <summary>
        /// 移动单Y3轴，光阑触发需要单独停止此轴
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="pos"></param>
        /// <param name="vel"></param>
        /// <param name="acc"></param>
        /// <param name="isStop"></param>
        /// <returns></returns>
        private bool MoveSingleAxis(G4.Motion.EnumAxisType axis, double pos, double vel, double acc)
        {
            double curPos = 0;
            G4.Motion.MotionModel.Instance.GetPosition(axis, out curPos);
            G4.Motion.MotionModel.Instance.GetAxisStatus(axis, ref status);
            if (!status.Moving && curPos != pos)
            {
                Y3MotorTask = Task.Run(() =>
                {
                    var rtn = G4.Motion.MotionModel.Instance.MoveTo(axis, pos, vel, acc);
                    Y3MotorTaskRtn = rtn;
                });
                return false;
            }
            else
            {
                if (null != Y3MotorTask && Y3MotorTask.IsCompleted && curPos == pos)
                {
                    if (Y3MotorTaskRtn == 0)
                    {
                        AutoRunJTime.Restart();
                        return true;
                    }
                    else
                    {
                        Log.log.Write("组装模组执行Move失败", Color.Red);
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        private int MoveTo(double X, double Y, double Z, double vel, double acc, bool IsSafety = true)
        {
            double safetyPos = 0;
            if (IsSafety)
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z1, safetyPos, vel, acc);
                if (rtn == 0)
                {
                    var rtnXY = MoveToDoubleAxis(G4.Motion.EnumAxisType.X1, X, G4.Motion.EnumAxisType.Y1, Y, vel, acc, vel, acc);
                    if (0 == rtnXY)
                    {
                        return G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z1, Z, vel, acc);
                    }
                    else
                        return -1;
                }
                else
                    return -1;
            }
            else
            {
                var rtn = MoveToDoubleAxis(G4.Motion.EnumAxisType.X1, X, G4.Motion.EnumAxisType.Y1, Y, vel, acc, vel, acc);
                if (0 == rtn)
                {
                    return G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z1, Z, vel, acc);
                }
                else
                    return -1;

            }
        }

        private void RefreashDifferentThreadUI(Control control, Action action)
        {
            Debug.WriteLine($"mesBox hashcode={control.GetHashCode()}");
            if (control.InvokeRequired)
            {
                Action refreshUI = new Action(action);
                control.Invoke(refreshUI);
            }
            else
            {
                action.Invoke();
            }
        }

        public static bool DelayMs(int delayMilliseconds)
        {
            System.DateTime now = DateTime.Now;
            double s;
            do
            {
                TimeSpan spand = DateTime.Now - now;
                s = spand.TotalMilliseconds + spand.Seconds * 1000;
                Application.DoEvents();
            }
            while (s < delayMilliseconds);
            return true;
        }

        public bool StopY3Motor()
        {
            //y3IsStop = true;
            return G4.Motion.MotionModel.Instance.StopMoving(G4.Motion.EnumAxisType.Y3) == 0 ? true : false;

        }

        /// <summary>
        /// 显示报警窗口
        /// </summary>
        /// <param name="code"></param>
        private void ShowAlarmInfo(int code)
        {
            AlarmFrm.Alarm.Enqueue(code);
            this.Stop();
        }

        /// <summary>
        /// 关闭报警窗口
        /// </summary>
        private void CloseAlarmFrm()
        {
            AlarmFrm.CloseAlarmFrm();
        }
        private void SetNudControlLock(bool isLock)
        {
            foreach (Control ctl in this.Controls)
            {
                if (ctl is NumericUpDown || ctl is NPFlowChart)
                {
                    ctl.Enabled = !isLock;
                }
            }
        }
        #region 获取dgv数据
        public void InitDgv_CommunicateSetting(VUDatagridView dataGridView, ref List<CommunicateDgvData> communicateDgvDatas)
        {
            communicateDgvDatas.Clear();
            CommunicateDgvData dgvData = new CommunicateDgvData();
            for (int i = 0; i < dataGridView.myDgv.Rows.Count; i++)
            {
                dgvData.name = dataGridView.GetDataArrayByIndex(i)[2];
                dgvData.data = dataGridView.GetDataArrayByIndex(i)[1];
                communicateDgvDatas.Add(dgvData);
            }
        }

        public string GetAddressFormDgv(string name, List<CommunicateDgvData> communicateDgvDatas)
        {
            if (communicateDgvDatas != null && communicateDgvDatas.Count > 0)
            {
                for (int i = 0; i < communicateDgvDatas.Count; i++)
                {
                    if (communicateDgvDatas[i].name == name)
                        return communicateDgvDatas[i].data;
                }
                return "999";
            }
            else { return "999"; }
        }

        private void GetRecipeFormMes(List<MesDgvData> mesDgvDatas, ref Dictionary<string, object> recipeDic)
        {
            if (recipeDic == null) recipeDic = new Dictionary<string, object>();
            if (recipeDic.Count > 0) recipeDic.Clear();
            string dbAddr = "DB3400";
            for (int i = 0; i < mesDgvDatas.Count; i++)
            {
                if (GloMotionParame.SiemenPLC.IsConnected)
                {
                    if (mesDgvDatas[i].type == "string")
                    {
                        bool bret = GloMotionParame.SiemenPLC.ReadValue(dbAddr + "DBS" + mesDgvDatas[i].address, out string value);
                        recipeDic.Add(mesDgvDatas[i].name, (string)value);
                    }
                    else if (mesDgvDatas[i].type == "real")
                    {
                        bool bret = GloMotionParame.SiemenPLC.ReadValue(dbAddr + "DBD" + mesDgvDatas[i].address, out float value);
                        recipeDic.Add(mesDgvDatas[i].name, (float)value);
                    }
                }
                else
                {
                    return;
                }
            }
        }

        private void SetReportData2Mes(List<MesDgvData> mesDgvDatas, string name, object data)
        {
            string dbAddr = string.Empty;
            if (nameof(mesDgvDatas).Contains("Right"))
                dbAddr = "DB3412";
            else
                dbAddr = "DB3411";
            for (int i = 0; i < mesDgvDatas.Count; i++)
            {
                if (GloMotionParame.SiemenPLC.IsConnected)
                {
                    if (mesDgvDatas[i].name == name)
                    {
                        if (mesDgvDatas[i].type == "string")
                            GloMotionParame.SiemenPLC.Write(dbAddr + "DBS" + mesDgvDatas[i].address, (string)data);
                        else if (mesDgvDatas[i].type == "real")
                            GloMotionParame.SiemenPLC.Write(dbAddr + "DBD" + mesDgvDatas[i].address, (float)data);
                    }
                }
            }
        }

        #endregion
        #region 轴移动
        public int MoveToHomeXYR()
        {
            var XYRRet = Task.WhenAll(Task.Run(() => { return G4.Motion.MotionModel.Instance.AxisMoveToHome(G4.Motion.EnumAxisType.R1); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.AxisMoveToHome(G4.Motion.EnumAxisType.R2); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.AxisMoveToHome(G4.Motion.EnumAxisType.R3); }));
            XYRRet.Wait();
            var result = XYRRet.Result;
            if (result.All(p => p == 0))
            {
                return 0;
            }
            else { return 1; }
        }

        public int MoveToDoubleAxis(G4.Motion.EnumAxisType axis1, double pos1, G4.Motion.EnumAxisType axis2, double pos2, double vel1, double acc1, double vel2, double acc2)
        {
            var XYRRet = Task.WhenAll(Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(axis1, pos1, vel1, acc1); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(axis2, pos2, vel2, acc2); }));
            XYRRet.Wait();
            var result = XYRRet.Result;
            if (result.All(p => p == 0))
            {
                return 0;
            }
            else { return 1; }
        }

        public int MoveToThrAxis(G4.Motion.EnumAxisType axis1, double pos1, G4.Motion.EnumAxisType axis2, double pos2, G4.Motion.EnumAxisType axis3, double pos3,
            double vel1, double acc1, double vel2, double acc2, double vel3, double acc3)
        {
            var XYRRet = Task.WhenAll(Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(axis1, pos1, vel1, acc1); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(axis2, pos2, vel2, acc2); })
                  , Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(axis3, pos3, vel3, acc3); }));
            XYRRet.Wait();
            var result = XYRRet.Result;
            if (result.All(p => p == 0))
            {
                return 0;
            }
            else { return 1; }
        }

        public int MoveXYRPlatform(G4.Motion.EnumAxisType R1, G4.Motion.EnumAxisType R2, G4.Motion.EnumAxisType R3, double XYR_XPos, double XYR_YPos, double XYR_RPos, double vel, double acc)
        {
            var XYRet = Task.WhenAll(Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(R1, XYR_XPos, vel, acc); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(R2, XYR_XPos, vel, acc); })
                  , Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(R3, XYR_YPos, vel, acc); }));
            var resultXY = XYRet.Result;

            var RRet = Task.WhenAll(Task.Run(() => { return XXYRotateTo(XYR_RPos, vel, acc); }));
            var resultR = RRet.Result;
            if (resultXY.All(p => p == 0) && resultR.All(p => p == 0))
            {
                return 0;
            }
            else { return 1; }
        }

        public int XXYRotateTo(double deg, double vel, double acc)
        {
            const double R = 91.924;
            const double Y = 225;
            const double X1 = 315;
            const double X2 = 135;

            double dx1 = R * Math.Cos((X1 + deg) * Math.PI / 180) - R * Math.Cos(X1 * Math.PI / 180);
            double dx2 = R * Math.Cos((X2 + deg) * Math.PI / 180) - R * Math.Cos(X2 * Math.PI / 180);
            double dy = R * Math.Sin((Y + deg) * Math.PI / 180) - R * Math.Sin(Y * Math.PI / 180);

            var XYRet = Task.WhenAll(Task.Run(() => { return G4.Motion.MotionModel.Instance.Move(G4.Motion.EnumAxisType.R1, dx1, vel, acc); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.Move(G4.Motion.EnumAxisType.R2, dx2, vel, acc); })
                  , Task.Run(() => { return G4.Motion.MotionModel.Instance.Move(G4.Motion.EnumAxisType.R3, dy, vel, acc); }));
            var result = XYRet.Result;
            if (result.All(p => p == 0))
            {
                return 0;
            }
            else { return 1; }
        }
        #endregion

        private bool GetSensorData(ref double height)
        {
            byte[] dataRev = new byte[1024];
            if (null == SensorClient || !SensorClient.IsConnected)
            {
                return false;
            }
            SensorClient.Send("M0\r\n");
            string mLocationBuffer = string.Empty;
            int iretryCount = 0;
            while (string.IsNullOrEmpty(mLocationBuffer) && iretryCount < 10)
            {
                iretryCount++;
                Thread.Sleep(100);
                mLocationBuffer = SensorLastContent;
            }
            if (string.IsNullOrEmpty(mLocationBuffer))
            {
                return false;
            }
            var arr = mLocationBuffer.Split(',');
            if (arr == null || arr.Length < 2)
            {
                return false;
            }
            if (double.TryParse(arr[1], out double val))
            {
                height = val * 0.001;
                return true;
            }
            return false;
        }

        private List<double> get_xyr_from_string(string input_string)
        {
            List<double> arrResult = new List<double>();
            try
            {
                string[] strArr;
                string[] strArrPath;
                strArrPath = input_string.Split(';');
                if (strArrPath.Length > 1)
                    ImagePath = strArrPath[1];
                strArr = strArrPath[0].Split(',');
                foreach (string str in strArr)
                {
                    arrResult.Add(double.Parse(str));
                }
                return arrResult;
            }
            catch (Exception ex)
            {
                arrResult.Add(1.1);
                arrResult.Add(1.1);
                return arrResult;
            }

        }

        private string[] GetDataByAnnotation(VUControl.VUDatagridView dgv, string annotation)
        {
            string[] GetDataInfo = dgv.GetDataArrayByFlagName(1, annotation);
            return GetDataInfo;
        }


        private void CheckMotionPosition()
        {
            if (!Googo.Manul.GloMotionParame.InputList.GetValue("IB_RXCoverUp"))
            {
                ShowAlarmInfo(11);
            }
            if (!(Googo.Manul.GloMotionParame.InputList.GetValue("IB_" + assembModel.Split('_')[0] + "Vacuum")))
            {
                ShowAlarmInfo(11);
            }
            if (!Googo.Manul.GloMotionParame.InputList.GetValue("IB_UVLightUp"))
            {
                ShowAlarmInfo(11);
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("Y3OrignalPos");
            double pos = 0;
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.Y3, out pos);
            if (pos > 0 + 0.05)
            {
                ShowAlarmInfo(11);
            }
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.Z3, out pos);
            if (pos > PosCom.YPos + 0.05)
            {
                ShowAlarmInfo(11);
            }
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.Z4, out pos);
            if (pos > PosCom.ZPos + 0.05)
            {
                ShowAlarmInfo(11);
            }
            PosCom = model.MircPlatformCamPointsOrder.GetItemByName("MicoPlatformOrignalPos");
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.R1, out pos);
            if (pos > PosCom.XPos + 0.05)
            {
                ShowAlarmInfo(11);
            }
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.R2, out pos);
            if (pos > PosCom.YPos + 0.05)
            {
                ShowAlarmInfo(11);
            }
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.R3, out pos);
            if (pos > PosCom.ZPos + 0.05)
            {
                ShowAlarmInfo(11);
            }
            PosCom = model.PublicCamPointsOrder.GetItemByName("GetHeightCompeletedPos");
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.X1, out pos);
            if (pos > PosCom.XPos + 0.05)
            {
                ShowAlarmInfo(11);
            }
            G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.Z1, out pos);
            if (pos > PosCom.ZPos + 0.05)
            {
                ShowAlarmInfo(11);
            }
        }
        #endregion

        #region 贴装流程



        private YHSDK.FCResultType npFlowChart9_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private YHSDK.FCResultType npFlowChart10_FlowRun(object sender, EventArgs e)
        {
            WritePlcIsSuccess(GetAddressFormDgv("OB_StatusNo", Dispense1FlowWriteDgvDatas), 0);
            if (null == model.MountingCamPointsOrder || model.MountingCamPointsOrder.Count == 0)
            {
                Log.log.Write($"Mark位置和拍照指令数量位空", Color.Red);
                return FCResultType.IDLE;
            }
            if (string.IsNullOrEmpty(model.Signal_R_RXProgram))
            {
                Log.log.Write($"M启动指令位置为空", Color.Red);
                return FCResultType.IDLE;
            }

            pcbaDataList.Clear();
            pieceDataList.Clear();
            pcbAngleOffset = 0;
            pcbaSecDataList.Clear();
            pcbaRechkDataList.Clear();
            pcbOffsetX = 0;
            pcbOffsetY = 0;
            visNgCount = 0;
            bool ret1 = false;
            bool ret2 = false;
            bool ret3 = false;
            bool ret4 = false;
            bool ret5 = false;
            bool bvalue = false;

            bool bret = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("IB_RXAssemblyTask", Dispense1MoveReadDgvDatas), out bvalue);
            //Log.log.Write($"RX可贴装任务信号为{bvalue}",Color.Red);
            if (bret && bvalue)
            {
                if (model.RXorTXPartNo == "")
                {
                    RefreashDifferentThreadUI(confirmFrm, () =>
                    {
                        if (confirmFrm != null)
                        {
                            confirmFrm.Dispose();
                        }
                        confirmFrm = new frmConfirm();
                        confirmFrm.fnSetTextMessageNShow("当前物料码为空，请扫码，选【YES】重新扫码，选【Cancel】不用码继续生产", true, false, true);
                    });
                    if (confirmFrm.dResult == DialogResult.Yes)
                    {
                        Log.log.Write("等待扫码", Color.Black);
                        return FCResultType.IDLE;
                    }
                    else if (confirmFrm.dResult == DialogResult.Abort)
                    {
                        Log.log.Write("当前码为空，并选择了继续执行生产流程", Color.Red);
                    }
                    else { return FCResultType.IDLE; }
                }
                ret2 = WritePlcIsSuccess(GetAddressFormDgv("OB_RXAssemblyOK", Dispense1MoveWriteDgvDatas), false);
                ret3 = WritePlcIsSuccess(GetAddressFormDgv("OB_RXAssemblyNG", Dispense1MoveWriteDgvDatas), false);
                ret4 = WritePlcIsSuccess(GetAddressFormDgv("OB_RXAssemblyTaskSendData", Dispense1MoveWriteDgvDatas), false);
                //ret5 = WritePlcIsSuccess(GetAddressFormDgv("OB_StandBy", Dispense1MoveWriteDgvDatas), false);
                if (/*ret1 && */ret2 && ret3 && ret4)
                {
                    if (WritePlcIsSuccess(GetAddressFormDgv("OB_" + "RXAssemblyTask", Dispense1MoveWriteDgvDatas), true))
                    {
                        Log.log.Write(string.Format($"接收到RX贴装任务指令，贴装模组开始工作"), Color.Black);
                        model.RecheckOffset = new PositionInfor();
                        model.SecLocationAngle = 0.0f;
                        model.SecLocationXY = new PositionInfor();
                        model.PiecePlatness = 0.0f;
                        model.PieceFirstLocation = new PositionInfor();
                        model.PcbFirstLocation = new PositionInfor();
                        model.PiecePcbHeightList = new List<string>();
                        model.PieceHousingHeightList = new List<string>();
                        model.MountingResult = new ResultInfor();
                        assembModel = "RX_";
                        model.MountingTask = "RX";
                        model.ProgramNo = 1;
                        model.MountingVisLocTask = "1";
                        model.MountingVisRCKTask = "2";
                        model.LocationXYCount = 2;
                        model.LocationAngleCount = 3;
                        model.Assemblytimessetup = (float)1;

                        model.RXAssemblyToolID = 1;
                        model.RXSTOPAssemblyToolID = 1;

                        productSN = model.RXorTXPartNo != "" ? model.RXorTXPartNo : "NoSN";
                        model.MountingResult.ProductSN = productSN;
                        model.PieceHeightSpec = new List<float> { model.STOPAccepttheightLowerLimit, model.STOPAccepttheightUpperLimit };
                        model.ReckeckHeightSpec = new List<float> { model.STOPHeightLowerLimit, model.STOPHeightUpperLimit };
                        Log.log.Write($"当前点胶产品SN为{productSN}", Color.Black);
                      
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        return FCResultType.IDLE;
                    }
                }
            }
            bool bvalue1 = false;
            bool bret1 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("IB_TXAssemblyTask", Dispense1MoveReadDgvDatas), out bvalue1);
            //Log.log.Write($"TX可贴装任务信号为{bvalue1}", Color.Red);
            if (bret1 && bvalue1)
            {
                if (model.RXorTXPartNo == "")
                {
                    RefreashDifferentThreadUI(confirmFrm, () =>
                    {
                        if (confirmFrm != null)
                        {
                            confirmFrm.Dispose();
                        }
                        confirmFrm = new frmConfirm();
                        confirmFrm.fnSetTextMessageNShow("当前物料码为空，请扫码，选【YES】重新扫码，选【Cancel】不用码继续生产", true, false, true);
                    });
                    if (confirmFrm.dResult == DialogResult.Yes)
                    {
                        Log.log.Write("等待扫码", Color.Black);
                        return FCResultType.IDLE;
                    }
                    else if (confirmFrm.dResult == DialogResult.Abort)
                    {
                        Log.log.Write("当前码为空，并选择了继续执行生产流程", Color.Red);
                    }
                    else { return FCResultType.IDLE; }
                }
                //ret1 = WritePlcIsSuccess(GetAddressFormDgv("OB_" + "TXAssemblyTask", Dispense1MoveWriteDgvDatas), true);
                ret2 = WritePlcIsSuccess(GetAddressFormDgv("OB_TXAssemblyOK", Dispense1MoveWriteDgvDatas), false);
                ret3 = WritePlcIsSuccess(GetAddressFormDgv("OB_TXAssemblyNG", Dispense1MoveWriteDgvDatas), false);
                ret4 = WritePlcIsSuccess(GetAddressFormDgv("OB_TXAssemblyTaskSendData", Dispense1MoveWriteDgvDatas), false);
                //ret5 = WritePlcIsSuccess(GetAddressFormDgv("OB_StandBy", Dispense1MoveWriteDgvDatas), false);
                if (/*ret1 &&*/ ret2 && ret3 && ret4)
                {
                    if (WritePlcIsSuccess(GetAddressFormDgv("OB_" + "TXAssemblyTask", Dispense1MoveWriteDgvDatas), true))
                    {
                        Log.log.Write(string.Format($"接收到TX贴装任务指令，贴装模组开始工作"), Color.Black);
                        model.RecheckOffset = new PositionInfor();
                        model.SecLocationAngle = 0.0f;
                        model.SecLocationXY = new PositionInfor();
                        model.PiecePlatness = 0.0f;
                        model.PieceFirstLocation = new PositionInfor();
                        model.PcbFirstLocation = new PositionInfor();
                        model.MountingResult = new ResultInfor();
                        assembModel = "TX_";
                        model.MountingTask = "TX";
                        model.MountingVisLocTask = "1";
                        model.MountingVisRCKTask = "2";
                        model.ProgramNo = 2;
                        model.LocationXYCount = 0;
                        model.LocationAngleCount = 0;
                        model.Assemblytimessetup = (float)1;
                        productSN = model.RXorTXPartNo != "" ? model.RXorTXPartNo : "NoSN";
                        model.MountingResult.ProductSN = productSN;
                        model.PieceHeightSpec = new List<float> { model.STOPAccepttheightLowerLimit, model.STOPAccepttheightUpperLimit };
                        model.ReckeckHeightSpec = new List<float> { model.STOPHeightLowerLimit, model.STOPHeightUpperLimit };
                        Log.log.Write($"当前点胶产品SN为{productSN}", Color.Black);
                       
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        return FCResultType.IDLE;
                    }
                }
            }
            return FCResultType.IDLE;
        }

        private FCResultType npFlowChart12_FlowRun(object sender, EventArgs e)
        {
            if (WritePlcIsSuccess(GetAddressFormDgv("OB_StatusNo", Dispense1FlowWriteDgvDatas), Convert.ToUInt16(txb_MountingReport1.Value)))        //写入状态标志位决定报工状态
                return FCResultType.NEXT;
            else
            {
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart11_FlowRun(object sender, EventArgs e)
        {
            if (assembModel.Contains("RX"))
            {
                if (Googo.Manul.GloMotionParame.InputList.GetValue("IB_RXGrapVacuum"))
                {
                    return FCResultType.NEXT;
                }
                else
                {

                    Log.log.Write("当前光阑真空盖板型号不正确，请检查", Color.Red);
                    ShowAlarmInfo(1051);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (Googo.Manul.GloMotionParame.InputList.GetValue("IB_TXGrapVacuum"))
                {
                    return FCResultType.NEXT;
                }
                else
                {
                    Log.log.Write("当前光阑真空盖板型号不正确，请检查", Color.Red);
                    ShowAlarmInfo(1052);
                    return FCResultType.IDLE;
                }
            }
        }

        private FCResultType npFlowChart14_FlowRun(object sender, EventArgs e)
        {
            if (Googo.Manul.GloMotionParame.InputList.GetValue("IB_RXCoverUp") && !Googo.Manul.GloMotionParame.InputList.GetValue("IB_RXCoverDown"))
            {
                AutoRunJTime.Restart();
                if (assembModel.Contains("RX"))
                    return FCResultType.NEXT;
                else { return FCResultType.CASE2; }
            }
            else
            {
                Googo.Manul.GloMotionParame.OutputList.SetValue("OB_RXCoverDown", false);
                Googo.Manul.GloMotionParame.OutputList.SetValue("OB_RXCoverUp", true);
                if (!Googo.Manul.GloMotionParame.InputList.GetValue("IB_RXCoverDown") && Googo.Manul.GloMotionParame.InputList.GetValue("IB_RXCoveUp"))
                {
                    AutoRunJTime.Restart();
                    heightData.Clear();
                    leftCheckHeightData.Clear();
                    if (assembModel.Contains("RX"))
                        return FCResultType.NEXT;
                    else { return FCResultType.CASE2; }
                }
                else
                {
                    if (AutoRunJTime.On(30000))
                    {
                        model.MountingResult.ProNGCode = MotionNGCode;
                        //Log.log.Write("盖板气缸上升超时", Color.Red);
                        Log.log.Write("盖板气缸上升超时", Color.Red);
                        AutoRunJTime.Restart();
                        ShowAlarmInfo(1022);
                        return FCResultType.IDLE;
                    }
                    return FCResultType.IDLE;
                }
            }
        }

        private FCResultType npFlowChart13_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1 || null == model)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("PutCoverPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
            int iret = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Y3, PosCom.XPos, model.Y3Speed, model.Y3AccSpeed);
            if (iret==0)
            {
                return FCResultType.NEXT;
            }
            else
            {
                Log.log.Write($"{0} 组装模组执行Move失败,错误码={iret}", Color.Red);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart15_FlowRun(object sender, EventArgs e)
        {
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_RXCoverDown", true);
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_RXCoverUp", false);
            if (Googo.Manul.GloMotionParame.InputList.GetValue("IB_RXCoverDown") && !Googo.Manul.GloMotionParame.InputList.GetValue("IB_RXCoverUp"))
            {
                AutoRunJTime.Restart();
                return FCResultType.NEXT;
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    model.MountingResult.ProNGCode = MotionNGCode;

                    //Log.log.Write("盖板气缸下降超时", Color.Red);
                    Log.log.Write($"{0} 盖板气缸下降超时", Color.Red);
                    AutoRunJTime.Restart();
                    ShowAlarmInfo(1023);
                    return FCResultType.IDLE;
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart16_FlowRun(object sender, EventArgs e)
        {
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_RXCoverOff", true);
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_RXCoverOn", false);
            if (Googo.Manul.GloMotionParame.InputList.GetValue("IB_RXCoverInplace"))
            {
                AutoRunJTime.Restart();
                return FCResultType.NEXT;
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    model.MountingResult.ProNGCode = MotionNGCode;

                    //Log.log.Write("盖板松开超时", Color.Red);
                    Log.log.Write($"{0} 盖板松开超时", Color.Red);
                    AutoRunJTime.Restart();
                    ShowAlarmInfo(1024);
                    return FCResultType.IDLE;
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart17_FlowRun(object sender, EventArgs e)
        {
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_RXCoverDown", false);
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_RXCoverUp", true);
            if (!Googo.Manul.GloMotionParame.InputList.GetValue("IB_RXCoverDown") && Googo.Manul.GloMotionParame.InputList.GetValue("IB_RXCoverUp"))
            {
                AutoRunJTime.Restart();
                heightData.Clear();
                leftCheckHeightData.Clear();
                return FCResultType.NEXT;
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    model.MountingResult.ProNGCode = MotionNGCode;

                    Log.log.Write($"{0} 盖板气缸上升超时", Color.Red);
                    AutoRunJTime.Restart();
                    ShowAlarmInfo(1022);
                    return FCResultType.IDLE;
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart18_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                model.MountingResult.ProNGCode = MotionNGCode;

                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("GetHeightPos");
            if (PosCom == null)
            {
                model.MountingResult.ProNGCode = MotionNGCode;

                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            if (MoveSingleAxis(G4.Motion.EnumAxisType.Y3, PosCom.XPos, model.Y3Speed, model.Y3AccSpeed))
            {
                return FCResultType.NEXT;
            }
            else
            {
                DelayMs(100);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart20_FlowRun(object sender, EventArgs e)
        {
            AutoRunJTime.Restart();
            AssembModuleTaskRtn = 0;
            return FCResultType.NEXT;
            //if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            //{
            //    if (AssembModuleTaskRtn == 0)
            //    {
            //        AutoRunJTime.Restart();
            //        return FCResultType.NEXT;
            //    }
            //    else
            //    {
            //        //Log.log.Write("组装模组执行Move失败", Color.Red);
            //        Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
            //        //ShowAlarmInfo(2201);
            //        return FCResultType.IDLE;
            //    }
            //}
            //else
            //{
            //    if (AutoRunJTime.On(30000))
            //    {
            //        AutoRunJTime.Restart();
            //        //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
            //        Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
            //        //ShowAlarmInfo(2201);
            //    }
            //    return FCResultType.IDLE;
            //}
        }

        private FCResultType npFlowChart21_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                model.MountingResult.ProNGCode = MotionNGCode;

                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            int Index = heightData.Count + 1;
            PosCom = model.PublicCamPointsOrder.GetItemByName("GetHeightPos" + Index);
            if (PosCom == null)
            {
                model.MountingResult.ProNGCode = MotionNGCode;

                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = MoveTo(PosCom.XPos, PosCom.YPos, PosCom.ZPos, GlueProject.GlueExcuteModel.GlueXYAxisSpd, GlueProject.GlueExcuteModel.GlueXYAxisAcc);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart22_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    model.MountingResult.ProNGCode = MotionNGCode;

                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);

                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart23_FlowRun(object sender, EventArgs e)
        {
            double height = 0;
            if (GetSensorData(ref height))
            {
                heightData.Add((float)height);
                AutoRunJTime.Restart();
                return FCResultType.NEXT;
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 测高数据获取超时", Color.Red);
                    model.MountingResult.ProNGCode = SensorNGCode;

                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart24_FlowRun(object sender, EventArgs e)
        {
            if (heightData.Count >= getHeightTime)
            {
                return FCResultType.NEXT;
            }
            else
            {
                return FCResultType.CASE1;
            }
        }

        private FCResultType npFlowChart25_FlowRun(object sender, EventArgs e)
        {
            if (model.PublicCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.PublicCamPointsOrder.GetItemByName("GetHeightCompeletedPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = MoveTo(PosCom.XPos, PosCom.YPos, PosCom.ZPos, GlueProject.GlueExcuteModel.GlueXYAxisSpd, GlueProject.GlueExcuteModel.GlueXYAxisAcc);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart26_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart27_FlowRun(object sender, EventArgs e)
        {
            model.PiecePlatness = (float)Math.Round(heightData.Max() - heightData.Min(), 3);
            model.MountingResult.PiecePlatness = Math.Round(heightData.Max() - heightData.Min(), 3);
            model.PieceHeight = heightData;
            if (model.IsPieceHeightNg)
            {
                model.MountingResult.ProNGCode = SensorNGCode;

                ShowAlarmInfo(1055);
                Log.log.Write($"{0} 单点测高数据超限", Color.Red);
                return FCResultType.CASE1;
            }
            if (heightData.Max() - heightData.Min() > model.PiecePlatnessSpc)
            {
                model.MountingResult.ProNGCode = SensorNGCode;

                ShowAlarmInfo(1050);
                Log.log.Write($"{0} 光阑平面度异常", Color.Red);
                return FCResultType.CASE1;
            }
            else
            {
                //Log.log.Write($"光阑平面度：{SysPara.HeightData.Max() - SysPara.HeightData.Min()}", Color.Red);
                Log.log.Write($"光阑平面度：{Math.Round(heightData.Max() - heightData.Min(), 3)}", Color.Green);
                PosCom = model.MountingCamPointsOrder.GetItemByName("TakePicture_pcba");
                if (PosCom == null)
                {
                    Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                    return FCResultType.IDLE;
                }
                AssembModuleTask = Task.Run(() =>
                {
                    var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z4, PosCom.ZPos, model.Z4Speed, model.Z4AccSpeed);
                    AssembModuleTaskRtn = rtn;
                });
                AutoRunJTime.Restart();
                return FCResultType.NEXT;
            }
        }

        private FCResultType npFlowChart64_FlowRun(object sender, EventArgs e)
        {
            double pos = 999;
            double posX1 = 999;
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.Z4, out pos);
                    G4.Motion.MotionModel.Instance.GetPosition(G4.Motion.EnumAxisType.X1, out posX1);
                    if (pos > 12.5 && posX1 < 20)
                    {
                        if (WritePlcIsSuccess(GetAddressFormDgv("OB_StatusNo", Dispense1FlowWriteDgvDatas), Convert.ToUInt16(txb_MountingReport2.Value)))        //写入状态标志位决定报工状态
                        {
                            AutoRunJTime.Restart();
                            return FCResultType.NEXT;
                        }
                        else
                        {
                            return FCResultType.IDLE;
                        }
                    }
                    else
                    {
                        if (pos < 12.5)
                        {
                            Log.log.Write($"Z4轴未在安全高度当前Z4轴所在位置坐标：{pos},设定安全位置为13.5，请检查", Color.Red);
                            ShowAlarmInfo(1053);
                        }
                        if (posX1 > 20)
                        {
                            Log.log.Write($"Z4轴未在安全高度当前X1轴所在位置坐标：{posX1},设定安全位置为20，请检查", Color.Red);
                            ShowAlarmInfo(1054);
                        }
                        return FCResultType.IDLE;
                    }
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }

        }

        private FCResultType npFlowChart29_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("TakePicture_pcba");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = MoveToDoubleAxis(G4.Motion.EnumAxisType.Z3, PosCom.YPos, G4.Motion.EnumAxisType.Z4, PosCom.ZPos,
                model.Z3Speed, model.Z3AccSpeed, model.Z4Speed, model.Z4AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;

        }

        private FCResultType npFlowChart30_FlowRun(object sender, EventArgs e)
        {
            bool ret = MoveSingleAxis(G4.Motion.EnumAxisType.Y3, PosCom.XPos, model.Y3Speed, model.Y3AccSpeed);
            if ((null == AssembModuleTask || AssembModuleTask.IsCompleted) && ret)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);

                    return FCResultType.IDLE;
                }
            }
            else
            {
                DelayMs(100);
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);

                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart33_FlowRun(object sender, EventArgs e)
        {
            //通知德创视觉Location任务
            AlgLastContent = string.Empty;
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("TakePicture_pcba");
            if (PosCom == null || string.IsNullOrEmpty(PosCom.StrCommand))
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            if (null != DCTcpClient && DCTcpClient.IsConnected)
            {
                AutoRunJTime.Restart();
                DCTcpClient.Send(PosCom.StrCommand + "," + productSN);
                return FCResultType.NEXT;
            }
            else
            {
                Log.log.Write($"DCCK TCP连接断开", System.Drawing.Color.Red);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart34_FlowRun(object sender, EventArgs e)
        {
            //通知德创视觉Location任务
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("TakePicture_piece");
            if (PosCom == null || string.IsNullOrEmpty(PosCom.StrCommand))
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            if (null != DCTcpClient && DCTcpClient.IsConnected)
            {
                AutoRunJTime.Restart();
                DCTcpClient.Send(PosCom.StrCommand + "," + productSN);
                return FCResultType.NEXT;
            }
            else
            {
                Log.log.Write($"DCCK TCP连接断开", System.Drawing.Color.Red);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart31_FlowRun(object sender, EventArgs e)
        {
            string mLocationBuffer = AlgLastContent;
            if (string.IsNullOrEmpty(mLocationBuffer))
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    ShowAlarmInfo(1063);
                    Log.log.Write($"接收DCCK数据超时", System.Drawing.Color.Red);
                }
                return FCResultType.IDLE;
            }
            else
            {
                if (mLocationBuffer.Contains("NG"))
                {
                    Log.log.Write("PCB引导X数据超限，请检查视觉", System.Drawing.Color.Red);
                    ShowAlarmInfo(1056);
                    return FCResultType.CASE1;
                }
                pcbaDataList = get_xyr_from_string(mLocationBuffer);
                if (pcbaDataList.Count >= 3)
                {
                    if (Math.Abs(pcbaDataList[0]) > model.RFBCCDGuideActPosAcceptDeviationX)
                    {
                        Log.log.Write("PCB引导X数据超限，请检查视觉", System.Drawing.Color.Red);
                        ShowAlarmInfo(1056);
                        return FCResultType.CASE1;
                    }
                    if (Math.Abs(pcbaDataList[1]) > model.RFBCCDGuideActPosAcceptDeviationY)
                    {
                        Log.log.Write("PCB引导Y数据超限，请检查视觉", System.Drawing.Color.Red);
                        ShowAlarmInfo(1057);
                        return FCResultType.CASE1;
                    }
                    if (Math.Abs(pcbaDataList[2]) > model.RFBCCDGuideActPosAcceptDeviationR)
                    {
                        Log.log.Write("PCB引导R数据超限，请检查视觉", System.Drawing.Color.Red);
                        ShowAlarmInfo(1058);
                        return FCResultType.CASE1;
                    }
                }
                else
                {
                    Log.log.Write("PCB引导数据格式超限，请检查视觉", System.Drawing.Color.Red);
                    ShowAlarmInfo(1059);
                    return FCResultType.CASE1;
                }
                if (pcbaDataList.Count > 1)
                {
                    //Log.log.Write($"PCB引导X:{ pcbaDataList[0]},引导Y:{pcbaDataList[1]},引导R:{pcbaDataList[2]}", Color.Black);
                    PositionInfor p = new PositionInfor();
                    p.XPos = pcbaDataList[0];
                    p.YPos = pcbaDataList[1];
                    p.ZPos = pcbaDataList[2];
                    model.RFBCCDGuideActPosDeviationX = (float)pcbaDataList[0];
                    model.RFBCCDGuideActPosDeviationY = (float)pcbaDataList[1];
                    model.RFBCCDGuideActPosDeviationR = (float)pcbaDataList[2];
                    model.PcbFirstLocation = p;
                    Log.log.Write($"PCB引导X:{pcbaDataList[0]},引导Y:{pcbaDataList[1]},引导R:{pcbaDataList[2]}", System.Drawing.Color.Green);
                    model.MountingResult.PcbFirstLocation = p;
                    return FCResultType.NEXT;
                }
                else
                {
                    if (AutoRunJTime.On(30000))
                    {
                        //Log.log.Write("获取pcba数据失败", Color.Red);
                        Log.log.Write("获取pcba数据失败", System.Drawing.Color.Red);
                        AutoRunJTime.Restart();
                        ShowAlarmInfo(1040);
                        return FCResultType.CASE1;
                    }
                    return FCResultType.IDLE;
                }
            }
        }

        private FCResultType npFlowChart36_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("TakePicture_piece");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z3, PosCom.YPos, model.Z3Speed, model.Z3AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart35_FlowRun(object sender, EventArgs e)
        {
            if ((null == AssembModuleTask || AssembModuleTask.IsCompleted) && MoveSingleAxis(G4.Motion.EnumAxisType.Y3, PosCom.XPos, model.Y3Speed, model.Y3AccSpeed))
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                DelayMs(100);
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart32_FlowRun(object sender, EventArgs e)
        {
            string mLocationBuffer = AlgLastContent;
            if (string.IsNullOrEmpty(mLocationBuffer))
            {
                if (AutoRunJTime.On(10000))
                {
                    AutoRunJTime.Restart();
                    ShowAlarmInfo(1064);
                    Log.log.Write($"接收DCCK数据超时", System.Drawing.Color.Red);
                }
                return FCResultType.IDLE;
            }
            else
            {
                if (mLocationBuffer.Contains("NG"))
                {
                    // Log.log.Write($"光阑引导X数据{pieceDataList[0]}超限，请检查视觉", System.Drawing.Color.Red);
                    Log.log.Write($"光阑引导NG，请检查视觉", System.Drawing.Color.Red);
                    ShowAlarmInfo(1060);
                    return FCResultType.CASE1;
                }
                pieceDataList = get_xyr_from_string(mLocationBuffer);
                if (pieceDataList.Count >= 3)
                {
                    if (Math.Abs(pieceDataList[0]) > model.STOPCCDGuideActPosAcceptDeviationX)
                    {
                        Log.log.Write($"光阑引导X数据{pieceDataList[0]}超限，请检查视觉", System.Drawing.Color.Red);
                        ShowAlarmInfo(1060);
                        return FCResultType.CASE1;
                    }
                    if (Math.Abs(pieceDataList[1]) > model.STOPCCDGuideActPosAcceptDeviationY)
                    {
                        Log.log.Write($"光阑引导Y数据{pieceDataList[1]}超限，请检查视觉", System.Drawing.Color.Red);
                        ShowAlarmInfo(1061);
                        return FCResultType.CASE1;
                    }
                    if (Math.Abs(pieceDataList[2]) > model.STOPCCDGuideActPosAcceptDeviationR)
                    {
                        Log.log.Write($"光阑引导R数据{pieceDataList[2]}超限，请检查视觉", System.Drawing.Color.Red);
                        ShowAlarmInfo(1062);
                        return FCResultType.CASE1;
                    }
                }
                if (pieceDataList.Count > 0)
                {
                    //Log.log.Write($"光阑引导X:{ pieceDataList[0]},引导Y:{pieceDataList[1]},引导R:{pieceDataList[2]}", Color.Black);
                    PositionInfor p = new PositionInfor();
                    p.XPos = pieceDataList[0];
                    p.YPos = pieceDataList[1];
                    p.ZPos = pieceDataList[2];
                    model.STOPCCDGuideActPosDeviationX = (float)pieceDataList[0];
                    model.STOPCCDGuideActPosDeviationY = (float)pieceDataList[1];
                    model.STOPCCDGuideActPosDeviationR = (float)pieceDataList[2];
                    model.PieceFirstLocation = p;
                    Log.log.Write($"光阑引导X:{pieceDataList[0]},引导Y:{pieceDataList[1]},引导R:{pieceDataList[2]}", Color.Green);
                    model.MountingResult.PieceFirstLocation = p;
                    return FCResultType.NEXT;
                }
                else
                {
                    if (AutoRunJTime.On(30000))
                    {
                        //Log.log.Write("获取piece数据失败", Color.Red);
                        Log.log.Write($"{0} 获取piece数据失败", Color.Red);
                        AutoRunJTime.Restart();
                        ShowAlarmInfo(1039);
                        return FCResultType.CASE1;
                    }
                    return FCResultType.IDLE;
                }
            }
        }

        private FCResultType npFlowChart38_FlowRun(object sender, EventArgs e)
        {
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = MoveXYRPlatform(G4.Motion.EnumAxisType.R1, G4.Motion.EnumAxisType.R2, G4.Motion.EnumAxisType.R3, pieceDataList[0], pieceDataList[1], pieceDataList[2],
                    model.R1Speed, model.R1AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart37_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart40_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("Pickup_piece");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            model.SETVoiceCoilMotormovespeed =(float) model.Z4Speed;
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z4, PosCom.ZPos, model.Z4Speed, model.Z4AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart39_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart196_FlowRun(object sender, EventArgs e)
        {
            bool ret1 = false;
            bool ret = false;
            ret1 = WritePlcIsSuccess(GetAddressFormDgv("OB_" + assembModel.Split('_')[0] + "PieceVacuum", Dispense1MoveWriteDgvDatas), true);
            //GloMotionParame.SiemenPLC.Write(GetAddressFormDgv("OB_" + assembModel.Split('_')[0] + "PieceVacuum", Dispense1MoveWriteDgvDatas), true);
            GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("IB_" + assembModel.Split('_')[0] + "PieceVacuum", Dispense1MoveReadDgvDatas), out ret);
            if (ret && ret1)
            {
                return FCResultType.NEXT;
            }
            else
            {
                if (AutoRunJTime.On(3000))
                {
                    ShowAlarmInfo(1065);
                    AutoRunJTime.Restart();
                }
                return FCResultType.IDLE;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //AlarmFrm.Alarm.Enqueue(2200);
            //this.Stop();
            string SN = "SN";
            model.ForceSaveImagePath = "d:\\" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss-ms") + "test.png";
        }




        private FCResultType npFlowChart41_FlowRun(object sender, EventArgs e)
        {
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_" + assembModel.Split('_')[0] + "Vacuum", true);
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_" + assembModel.Split('_')[0] + "BreakVacuum", false);
            if (Googo.Manul.GloMotionParame.InputList.GetValue("IB_" + assembModel.Split('_')[0] + "Vacuum"))
            {
                AutoRunJTime.Restart();
                return FCResultType.NEXT;
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    if (assembModel.Contains("RX"))
                    {
                        ShowAlarmInfo(1038);
                        Log.log.Write("RX吸取光阑真空超时", Color.Red);
                    }
                    else
                    {
                        ShowAlarmInfo(1037);
                        Log.log.Write("TX吸取光阑真空超时", Color.Red);
                    }
                    AutoRunJTime.Restart();
                    return FCResultType.IDLE;
                }
                return FCResultType.IDLE;
            }
        }




        private FCResultType npFlowChart45_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("WaitPos_piece");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z4, PosCom.ZPos, model.Z4Speed, model.Z4AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart44_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    if (Googo.Manul.GloMotionParame.InputList.GetValue("IB_" + assembModel.Split('_')[0] + "Vacuum"))
                    {
                        AutoRunJTime.Restart();
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        Log.log.Write("光阑吸取失败，返回吸取位置重新吸取", Color.Red);
                        return FCResultType.CASE1;
                    }
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart47_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("TakePicture_assmebly");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z3, PosCom.YPos, model.Z3Speed, model.Z3AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart46_FlowRun(object sender, EventArgs e)
        {
            if ((null == AssembModuleTask || AssembModuleTask.IsCompleted) && MoveSingleAxis(G4.Motion.EnumAxisType.Y3, PosCom.XPos, model.Y3Speed, model.Y3AccSpeed))
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                DelayMs(100);
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart43_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MircPlatformCamPointsOrder.GetItemByName("AssembReadyPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = MoveXYRPlatform(G4.Motion.EnumAxisType.R1, G4.Motion.EnumAxisType.R2, G4.Motion.EnumAxisType.R3, PosCom.XPos + pcbaDataList[0], PosCom.YPos + pcbaDataList[1], PosCom.ZPos + pcbaDataList[2],
                    model.R1Speed, model.R1AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart42_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart49_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("SecPicture_piece");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = MoveToDoubleAxis(G4.Motion.EnumAxisType.Z3, PosCom.YPos, G4.Motion.EnumAxisType.Z4, PosCom.ZPos,
                model.Z3Speed, model.Z3AccSpeed, model.Z4Speed, model.Z4AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart48_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart51_FlowRun(object sender, EventArgs e)
        {
            //通知德创视觉Location任务
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("SecPicture_piece");
            if (PosCom == null || string.IsNullOrEmpty(PosCom.StrCommand))
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            if (null != DCTcpClient && DCTcpClient.IsConnected)
            {
                AutoRunJTime.Restart();
                DCTcpClient.Send(PosCom.StrCommand);
                return FCResultType.NEXT;
            }
            else
            {
                Log.log.Write($"DCCK TCP连接断开", System.Drawing.Color.Red);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart50_FlowRun(object sender, EventArgs e)
        {
            string mLocationBuffer = AlgLastContent;
            if (string.IsNullOrEmpty(mLocationBuffer))
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    Log.log.Write($"接收DCCK数据超时", System.Drawing.Color.Red);
                }
                return FCResultType.IDLE;
            }
            else
            {
                if (mLocationBuffer.Contains("PieceOK"))
                {
                    return FCResultType.NEXT;
                }
                else
                {
                    if (AutoRunJTime.On(30000))
                    {
                        Log.log.Write("获取光阑拍照返回值失败", Color.Red);
                        AutoRunJTime.Restart();
                        return FCResultType.CASE1;
                    }
                    return FCResultType.IDLE;
                }
            }
        }

        private FCResultType npFlowChart199_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("TakePicture_pcba");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z3, PosCom.YPos, model.Z3Speed, model.Z3AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart198_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart202_FlowRun(object sender, EventArgs e)
        {
            //通知德创视觉Location任务
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("AssembReadyPos");
            if (PosCom == null || string.IsNullOrEmpty(PosCom.StrCommand))
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            if (null != DCTcpClient && DCTcpClient.IsConnected)
            {
                AutoRunJTime.Restart();
                DCTcpClient.Send(PosCom.StrCommand + "," + productSN);
                return FCResultType.NEXT;
            }
            else
            {
                Log.log.Write($"DCCK TCP连接断开", System.Drawing.Color.Red);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart201_FlowRun(object sender, EventArgs e)
        {
            string mLocationBuffer = AlgLastContent;
            if (string.IsNullOrEmpty(mLocationBuffer))
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    Log.log.Write($"接收DCCK数据超时", System.Drawing.Color.Red);
                }
                return FCResultType.IDLE;
            }
            else
            {
                if (mLocationBuffer.Contains("NG"))
                {
                    return FCResultType.CASE1;
                }
                double angle = double.Parse(mLocationBuffer);
                if (Math.Abs(angle) <= model.SecLocationAngleSpec)
                {
                    model.SecLocationAngle = (float)angle;
                    model.MountingResult.SecLocationAngle = angle;
                    Log.log.Write($"当前复判PCB角度为：{angle},不需要调整角度", Color.Green);
                    visNgCount = 0;
                    return FCResultType.CASE2;
                }
                if (Math.Abs(angle) <= 3)
                {
                    pcbAngleOffset += (float)angle;
                    Log.log.Write($"当前复判PCB角度为：{pcbAngleOffset}", Color.Black);
                    return FCResultType.NEXT;
                }
                else
                {
                    Log.log.Write($"当前复判PCB角度为：{pcbAngleOffset}超限，请人工确认", Color.Red);
                    return FCResultType.IDLE;
                }
            }
        }

        private FCResultType npFlowChart66_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MircPlatformCamPointsOrder.GetItemByName("AssembReadyPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = MoveXYRPlatform(G4.Motion.EnumAxisType.R1, G4.Motion.EnumAxisType.R2, G4.Motion.EnumAxisType.R3, PosCom.XPos + pcbaDataList[0], PosCom.YPos + pcbaDataList[1], PosCom.ZPos + pcbaDataList[2] + pcbAngleOffset,
                    model.R1Speed, model.R1AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart174_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart208_FlowRun(object sender, EventArgs e)
        {
            //通知德创视觉Location任务
            if (model.MircPlatformCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MircPlatformCamPointsOrder.GetItemByName("XYAlign");
            if (PosCom == null || string.IsNullOrEmpty(PosCom.StrCommand))
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            if (null != DCTcpClient && DCTcpClient.IsConnected)
            {
                AutoRunJTime.Restart();
                DCTcpClient.Send(PosCom.StrCommand + "," + productSN);
                return FCResultType.NEXT;
            }
            else
            {
                Log.log.Write($"DCCK TCP连接断开", System.Drawing.Color.Red);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart204_FlowRun(object sender, EventArgs e)
        {
            string mLocationBuffer = AlgLastContent;
            if (string.IsNullOrEmpty(mLocationBuffer))
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    Log.log.Write($"接收DCCK数据超时", System.Drawing.Color.Red);
                }
                return FCResultType.IDLE;
            }
            else
            {
                if (mLocationBuffer.Contains("NG"))
                {
                    pcbaSecDataList.Add(999);
                    pcbaSecDataList.Add(999);
                    return FCResultType.CASE1;
                }
                pcbaSecDataList = get_xyr_from_string(mLocationBuffer);

                for (int i = 0; i < pcbaSecDataList.Count; i++)
                {
                    if (Math.Abs(pcbaSecDataList[i]) > 1)
                    {
                        Log.log.Write("光阑二次引导数据超限，请检查视觉", Color.Red);
                        return FCResultType.CASE1;
                    }
                }
                if (pcbaSecDataList.Count > 1)
                {
                    pcbOffsetX += (float)pcbaSecDataList[0];
                    pcbOffsetY += (float)pcbaSecDataList[1];
                    if (Math.Abs(pcbaSecDataList[0]) <= model.SecLocationXYSpec.XPos && Math.Abs(pcbaSecDataList[1]) <= model.SecLocationXYSpec.YPos)
                    {
                        PositionInfor p = new PositionInfor();
                        p.XPos = pcbaSecDataList[0];
                        p.YPos = pcbaSecDataList[1];
                        p.ZPos = 0;
                        model.SecLocationXY = p;
                        model.MountingResult.SecLocationXY = p;
                        model.GuideActPosX = pcbOffsetX;
                        model.GuideActPosY = pcbOffsetY;
                        model.GuideActPosR = PcbAngleOffset;
                        Log.log.Write($"光阑二次引导X:{pcbaSecDataList[0]},引导Y:{pcbaSecDataList[1]},,不需要调整XY", Color.Black);
                        if (model.IsUseSoftLand)
                            return FCResultType.CASE3;
                        else
                            return FCResultType.CASE2;
                    }
                    else
                    {
                        Log.log.Write($"光阑二次引导X:{pcbaSecDataList[0]},引导Y:{pcbaSecDataList[1]}", Color.Black);
                        return FCResultType.NEXT;
                    }
                }
                else
                {
                    if (AutoRunJTime.On(30000))
                    {
                        Log.log.Write("获取pcb数据失败", Color.Red);
                        AutoRunJTime.Restart();
                        return FCResultType.CASE1;
                    }
                    return FCResultType.IDLE;
                }
            }
        }

        private FCResultType npFlowChart205_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MircPlatformCamPointsOrder.GetItemByName("XYAlign");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = MoveXYRPlatform(G4.Motion.EnumAxisType.R1, G4.Motion.EnumAxisType.R2, G4.Motion.EnumAxisType.R3, PosCom.XPos + pcbaDataList[0] + pcbOffsetX, PosCom.YPos + pcbaDataList[1] + pcbOffsetY, PosCom.ZPos + pcbaDataList[2] + pcbAngleOffset,
                    model.R1Speed, model.R1AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart206_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart54_FlowRun(object sender, EventArgs e)
        {
            if (model.IsUseSoftLand)
            {
                Log.log.Write("执行软着陆", Color.Green);
                AssembModuleTask = Task.Run(() =>
                {
                    Helper.ForceControl.dataList = new List<CurvesParm>();
                    Helper.ForceControl.SN = model.MountingResult.ProductSN;
                    Helper.ForceControl.FirstPPSpd = model.FirstPPSpd;
                    Helper.ForceControl.FirstPPDis = model.FirstPPDis;
                    Helper.ForceControl.SecPVDis = model.SecPVDis;
                    Helper.ForceControl.ThirdPTSpd = model.ThirdPTSpd;
                    Helper.ForceControl.Preesure = model.Preesure;
                    var rtn = Helper.ForceControl.StartSoftLand();
                    AssembModuleTaskRtn = rtn;
                });
                AutoRunJTime.Restart();
                return FCResultType.NEXT;

            }
            else
            {
                if (model.MountingCamPointsOrder.Count < 1)
                {
                    Log.log.Write("贴装定位点位异常", Color.Red);
                    return FCResultType.IDLE;
                }
                PosCom = model.MountingCamPointsOrder.GetItemByName("AssembPos");
                if (PosCom == null)
                {
                    Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                    return FCResultType.IDLE;
                }
                AssembModuleTask = Task.Run(() =>
                {
                    var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z4, PosCom.ZPos, model.Z4Speed, model.Z4AccSpeed);
                    AssembModuleTaskRtn = rtn;
                });
                AutoRunJTime.Restart();
                return FCResultType.NEXT;
            }


        }

        private FCResultType npFlowChart53_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    if (model.IsUseSoftLand)
                    {
                        model.MountingResult.ForceControlValue = Helper.ForceControl.dataList[0].MaxVal;
                        model.MountingResult.ForceControlValuePath = Helper.ForceControl.dataList[0].SaveImgPath + model.MountingResult.ProductSN + @"\" + DateTime.Now.ToString("yyyy-MM-dd") + ".csv";
                        model.SETVoiceCoilMotorCurrentactualvalue = Helper.ForceControl.dataList[0].MaxVal;
                        //model.PressureCurvesPath = Helper.ForceControl.dataList[0].SaveImgPath + model.MountingResult.ProductSN + @"\";
                        model.PressureCurvesDataPath = Helper.ForceControl.dataList[0].SaveImgPath + model.MountingResult.ProductSN + @"\" + DateTime.Now.ToString("yyyy-MM-dd") + ".csv";
                        model.ForceControlData = Helper.ForceControl.dataList;
                        model.VoiceCoilMotormovespeed = model.FirstPPSpd;
                    }
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart221_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("RecheckPicture_piece");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z3, PosCom.YPos, model.Z3Speed, model.Z3AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart219_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart220_FlowRun(object sender, EventArgs e)
        {
            //通知德创视觉Location任务
            if (model.MircPlatformCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("RecheckPicture_piece");
            if (PosCom == null || string.IsNullOrEmpty(PosCom.StrCommand))
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            if (null != DCTcpClient && DCTcpClient.IsConnected)
            {
                AutoRunJTime.Restart();
                DCTcpClient.Send(PosCom.StrCommand);
                return FCResultType.NEXT;
            }
            else
            {
                Log.log.Write($"DCCK TCP连接断开", System.Drawing.Color.Red);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart55_FlowRun(object sender, EventArgs e)
        {
            string mLocationBuffer = AlgLastContent;
            if (string.IsNullOrEmpty(mLocationBuffer))
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    Log.log.Write($"接收DCCK数据超时", System.Drawing.Color.Red);
                }
                return FCResultType.IDLE;
            }
            else
            {
                pcbaRechkDataList = get_xyr_from_string(mLocationBuffer);
                for (int i = 0; i < pcbaRechkDataList.Count; i++)
                {
                    if (Math.Abs(pcbaRechkDataList[i]) > 1)
                    {
                        Log.log.Write("视觉复检结果超限，需要重新定位", Color.Red);
                        return FCResultType.CASE1;
                    }
                }
                if (pcbaRechkDataList.Count > 3)
                {
                    if (Math.Abs(pcbaRechkDataList[0]) <= DataModel.RecheckOffsetSpec.XPos && Math.Abs(pcbaRechkDataList[1]) <= DataModel.RecheckOffsetSpec.XPos &&
                        Math.Abs(pcbaRechkDataList[2]) <= DataModel.RecheckOffsetSpec.YPos && Math.Abs(pcbaRechkDataList[3]) <= DataModel.RecheckOffsetSpec.YPos)
                    {
                        PositionInfor p = new PositionInfor();
                        p.XPos = pcbaRechkDataList[0];
                        p.YPos = pcbaRechkDataList[2];
                        p.ZPos = 0;
                        model.RecheckOffset = p;
                        model.MountingResult.RecheckOffset = p;
                        PositionInfor pRight = new PositionInfor();
                        pRight.XPos = pcbaRechkDataList[1];
                        pRight.YPos = pcbaRechkDataList[3];
                        pRight.ZPos = 0;
                        model.MountingResult.RecheckRightOffset = pRight;
                        model.CCDGuideActPosDeviationX1 = (float)pcbaRechkDataList[0];
                        model.CCDGuideActPosDeviationY1 = (float)pcbaRechkDataList[2];
                        model.CCDGuideActPosDeviationX2 = (float)pcbaRechkDataList[1];
                        model.CCDGuideActPosDeviationY2 = (float)pcbaRechkDataList[3];
                        model.CCDGuideActPosDeviationR = model.SecLocationAngle;
                        Log.log.Write($"左光阑复检定位偏移X:{pcbaRechkDataList[0]},左光阑复检定位偏移Y:{pcbaRechkDataList[2]}", Color.Black);
                        Log.log.Write($"右光阑复检定位偏移X:{pcbaRechkDataList[1]},右光阑复检定位偏移Y:{pcbaRechkDataList[3]}", Color.Black);
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        model.CCDGuideActPosDeviationX1 = (float)pcbaRechkDataList[0];
                        model.CCDGuideActPosDeviationY1 = (float)pcbaRechkDataList[2];
                        model.CCDGuideActPosDeviationX2 = (float)pcbaRechkDataList[1];
                        model.CCDGuideActPosDeviationY2 = (float)pcbaRechkDataList[3];
                        model.CCDGuideActPosDeviationR = model.SecLocationAngle;
                        Log.log.Write("视觉复检结果超限，需要重新定位", Color.Red);
                        return FCResultType.CASE1;
                    }
                }
                else
                {
                    if (AutoRunJTime.On(30000))
                    {
                        Log.log.Write("获取pcb数据失败", Color.Red);
                        AutoRunJTime.Restart();
                        return FCResultType.CASE1;
                    }
                    return FCResultType.IDLE;
                }
            }
        }

        private FCResultType npFlowChart217_FlowRun(object sender, EventArgs e)
        {
            string mLocationBuffer = AlgLastContent;
            if (string.IsNullOrEmpty(mLocationBuffer))
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    Log.log.Write($"接收DCCK数据超时", System.Drawing.Color.Red);
                }
                return FCResultType.IDLE;
            }
            else
            {
                string rtn = mLocationBuffer;
                if (mLocationBuffer.Contains("OK"))
                {
                    return FCResultType.NEXT;
                }
                else
                {
                    if (AutoRunJTime.On(30000))
                    {
                        Log.log.Write("获取光阑拍照返回值失败", Color.Red);
                        AutoRunJTime.Restart();
                        return FCResultType.CASE1;
                    }
                    return FCResultType.IDLE;
                }
            }
        }

        private FCResultType npFlowChart215_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("RecheckPicture_pcb");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z3, PosCom.YPos, model.Z3Speed, model.Z3AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart214_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart56_FlowRun(object sender, EventArgs e)
        {
            //通知德创视觉Location任务
            if (model.MircPlatformCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("RecheckPicture_pcb");
            if (PosCom == null || string.IsNullOrEmpty(PosCom.StrCommand))
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            if (null != DCTcpClient && DCTcpClient.IsConnected)
            {
                AutoRunJTime.Restart();
                DCTcpClient.Send(PosCom.StrCommand + "," + productSN);
                return FCResultType.NEXT;
            }
            else
            {
                Log.log.Write($"DCCK TCP连接断开", System.Drawing.Color.Red);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart65_FlowRun(object sender, EventArgs e)
        {
            if (assembModel.Contains("TX"))
            {
                Googo.Manul.GloMotionParame.OutputList.SetValue("OB_TXVacuum", false);
                Googo.Manul.GloMotionParame.OutputList.SetValue("OB_TXBreakVacuum", true);
            }
            else
            {
                Googo.Manul.GloMotionParame.OutputList.SetValue("OB_RXVacuum", false);
                Googo.Manul.GloMotionParame.OutputList.SetValue("OB_RXBreakVacuum", true);
                Thread.Sleep(5000);
            }
          
            if ((!GloMotionParame.InputList.GetValue("IB_RXVacuum") && assembModel.Contains("RX")) || (!GloMotionParame.InputList.GetValue("IB_TXVacuum") && assembModel.Contains("TX")))
            {
                AutoRunJTime.Restart();
                return FCResultType.NEXT;
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    Log.log.Write("盖板破真空信号异常", Color.Red);
                    ShowAlarmInfo(1054);
                    AutoRunJTime.Restart();
                    return FCResultType.IDLE;
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart177_FlowRun(object sender, EventArgs e)
        {
            RefreashDifferentThreadUI(confirmFrm, () =>
            {
                if (confirmFrm != null)
                {
                    confirmFrm.Dispose();
                }
                confirmFrm = new frmConfirm();
                confirmFrm.fnSetTextMessageNShow("贴装复检视觉异常，是否需要重新抬起光阑引导调整？选【YES】重新调整，选【Cancel】当NG产品排出", true, false, true);
            });
            if (confirmFrm.dResult == DialogResult.Yes)
            {
                pcbAngleOffset = 0;
                pcbOffsetX = 0;
                pcbOffsetY = 0;
                model.SecLocationAngle = 0;
                model.SecLocationXY = new PositionInfor();
                return FCResultType.CASE1;
            }
            else if (confirmFrm.dResult == DialogResult.Abort)
            {
                Helper.ForceControl.StopSoftLand();
                return FCResultType.CASE2;
            }
            else { return FCResultType.IDLE; }
        }

        private FCResultType npFlowChart59_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("AssembCompeletedPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z3, PosCom.YPos, model.Z3Speed, model.Z3AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart58_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart60_FlowRun(object sender, EventArgs e)
        {
            //bool bret = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("IB_RXAssemblyTask", Dispense1MoveReadDgvDatas), out bool bvalue);
            pcbaDataList.Clear();
            pieceDataList.Clear();
            pcbAngleOffset = 0;
            pcbaSecDataList.Clear();
            pcbaRechkDataList.Clear();
            pcbOffsetX = 0;
            pcbOffsetY = 0;
            visNgCount = 0;
            AutoRunJTime.Restart();
            bool ret1 = false;
            bool ret2 = false;
            bool ret3 = false;
            bool ret4 = false;
            if (assembModel.Contains("RX"))
            {
                ret1 = WritePlcIsSuccess(GetAddressFormDgv("OB_RXAssemblyOK", Dispense1MoveWriteDgvDatas), true);
                ret2 = WritePlcIsSuccess(GetAddressFormDgv("OB_RXAssemblyNG", Dispense1MoveWriteDgvDatas), false);

                if (ret1 && ret2 /*&& ret3 && ret4*/)
                {
                    if (WritePlcIsSuccess(GetAddressFormDgv("OB_RXAssemblyTaskSendData", Dispense1MoveWriteDgvDatas), true))
                    {
                        //WritePlcIsSuccess(GetAddressFormDgv("OB_StandBy", Dispense1MoveWriteDgvDatas), true);
                        Log.log.Write(string.Format($"回复RX贴装任务完成，结果OK"), Color.Black);
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        return FCResultType.IDLE;
                    }
                }
                return FCResultType.IDLE;
            }
            else
            {
                ret1 = WritePlcIsSuccess(GetAddressFormDgv("OB_TXAssemblyOK", Dispense1MoveWriteDgvDatas), true);
                ret2 = WritePlcIsSuccess(GetAddressFormDgv("OB_TXAssemblyNG", Dispense1MoveWriteDgvDatas), false);

                if (ret1 && ret2 /*&& ret3 && ret4*/)
                {
                    if (WritePlcIsSuccess(GetAddressFormDgv("OB_TXAssemblyTaskSendData", Dispense1MoveWriteDgvDatas), true))
                    {
                        //WritePlcIsSuccess(GetAddressFormDgv("OB_StandBy", Dispense1MoveWriteDgvDatas), true);
                        Log.log.Write(string.Format($"回复TX贴装任务完成，结果OK"), Color.Black);
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        return FCResultType.IDLE;
                    }
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart222_FlowRun(object sender, EventArgs e)
        {
            bool ret1 = false;
            bool ret2 = false;
            bool ret3 = false;
            bool ret4 = false;
            if (assembModel.Contains("RX"))
            {
                
                ret1 = WritePlcIsSuccess(GetAddressFormDgv("OB_RXAssemblyOK", Dispense1MoveWriteDgvDatas), false);
                ret2 = WritePlcIsSuccess(GetAddressFormDgv("OB_RXAssemblyNG", Dispense1MoveWriteDgvDatas), true);

                if (ret1 && ret2 /*&& ret3 && ret4*/)
                {
                    if (WritePlcIsSuccess(GetAddressFormDgv("OB_RXAssemblyTaskSendData", Dispense1MoveWriteDgvDatas), true))
                    {
                        //WritePlcIsSuccess(GetAddressFormDgv("OB_StandBy", Dispense1MoveWriteDgvDatas), true);
                        Log.log.Write(string.Format($"回复RX贴装任务完成，结果NG"), Color.Black);
                        pcbaDataList.Clear();
                        pieceDataList.Clear();
                        pcbAngleOffset = 0;
                        pcbaSecDataList.Clear();
                        pcbaRechkDataList.Clear();
                        pcbOffsetX = 0;
                        pcbOffsetY = 0;
                        model.LocationXYCount = 0;
                        model.LocationAngleCount = 0;
                        return FCResultType.NEXT;
                    }
                    else
                        return FCResultType.IDLE;
                }
                return FCResultType.IDLE;
            }
            else
            {
                ret1 = WritePlcIsSuccess(GetAddressFormDgv("OB_TXAssemblyOK", Dispense1MoveWriteDgvDatas), false);
                ret2 = WritePlcIsSuccess(GetAddressFormDgv("OB_TXAssemblyNG", Dispense1MoveWriteDgvDatas), true);

                if (ret1 && ret2/* && ret3 && ret4*/)
                {
                    if (WritePlcIsSuccess(GetAddressFormDgv("OB_TXAssemblyTaskSendData", Dispense1MoveWriteDgvDatas), true))
                    {
                        //WritePlcIsSuccess(GetAddressFormDgv("OB_StandBy", Dispense1MoveWriteDgvDatas), true);
                        Log.log.Write(string.Format($"回复TX贴装任务完成，结果NG"), Color.Black);
                        pcbaDataList.Clear();
                        pieceDataList.Clear();
                        pcbAngleOffset = 0;
                        pcbaSecDataList.Clear();
                        pcbaRechkDataList.Clear();
                        pcbOffsetX = 0;
                        pcbOffsetY = 0;
                        model.LocationXYCount = 0;
                        model.LocationAngleCount = 0;
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        return FCResultType.IDLE;
                    }
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart159_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart160_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart161_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart164_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart163_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart165_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart28_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart166_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart167_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart168_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart169_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart170_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart171_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart172_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart173_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart197_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart200_FlowRun(object sender, EventArgs e)
        {
            RefreashDifferentThreadUI(confirmFrm, () =>
            {
                if (confirmFrm != null)
                {
                    confirmFrm.Dispose();
                }
                confirmFrm = new frmConfirm();
                confirmFrm.fnSetTextMessageNShow("光阑视觉算法异常，是否重新拍照判断？选【YES】重新拍照，选【Cancel】当NG产品排出", true, false, true);
            });
            if (confirmFrm.dResult == DialogResult.Yes)
            {
                return FCResultType.CASE1;
            }
            else if (confirmFrm.dResult == DialogResult.Abort)
            {
                return FCResultType.CASE2;
            }
            else { return FCResultType.IDLE; }
        }

        private FCResultType npFlowChart52_FlowRun(object sender, EventArgs e)
        {
            model.LocAngleCountLmint = model.VisNgCount;
            if (model.LocationAngleCount >= model.VisNgCount)
            {
                RefreashDifferentThreadUI(confirmFrm, () =>
                {
                    if (confirmFrm != null)
                    {
                        confirmFrm.Dispose();
                    }
                    confirmFrm = new frmConfirm();
                    confirmFrm.fnSetTextMessageNShow("光阑视觉贴装次数超限，选【YES】重新拍照，选【Cancel】当NG产品排出", true, false, true);
                });
                if (confirmFrm.dResult == DialogResult.Yes)
                {
                    pcbAngleOffset = 0;
                    model.LocationAngleCount = 0;
                    model.SecLocationAngle = 0;
                    model.Assemblytimessetup += (float)1;
                    return FCResultType.CASE2;
                }
                else if (confirmFrm.dResult == DialogResult.Abort)
                {
                    return FCResultType.CASE1;
                }
                else { return FCResultType.IDLE; }
            }
            else
            {
                model.LocationAngleCount++;
                DelayMs(500);
                return FCResultType.NEXT;
            }
        }

        private FCResultType npFlowChart175_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart203_FlowRun(object sender, EventArgs e)
        {
            if (Math.Abs(pcbaSecDataList[0]) == 999.999 || Math.Abs(pcbaSecDataList[1]) == 999.999)
            {
                Log.log.Write("光阑二次引导Y方向偏差过大，需要重新引导角度，请检查视觉", Color.Red);
                RefreashDifferentThreadUI(confirmFrm, () =>
                {
                    if (confirmFrm != null)
                    {
                        confirmFrm.Dispose();
                    }
                    confirmFrm = new frmConfirm();
                    confirmFrm.fnSetTextMessageNShow("光阑二次引导Y方向偏差过大，需要重新引导角度，是否重新引导角度？选【YES】重新引导角度，选【Cancel】当NG产品排出", true, false, true);
                });
                if (confirmFrm.dResult == DialogResult.Yes)
                {
                    return FCResultType.CASE3;
                }
                else if (confirmFrm.dResult == DialogResult.Abort)
                {
                    return FCResultType.CASE2;
                }
                else { return FCResultType.IDLE; }
            }
            else
            {
                RefreashDifferentThreadUI(confirmFrm, () =>
                {
                    if (confirmFrm != null)
                    {
                        confirmFrm.Dispose();
                    }
                    confirmFrm = new frmConfirm();
                    confirmFrm.fnSetTextMessageNShow("Pcb视觉异常，是否重新拍照判断？选【YES】重新拍照，选【Cancel】当NG产品排出", true, false, true);
                });
                if (confirmFrm.dResult == DialogResult.Yes)
                {
                    return FCResultType.CASE1;
                }
                else if (confirmFrm.dResult == DialogResult.Abort)
                {
                    return FCResultType.CASE2;
                }
                else { return FCResultType.IDLE; }
            }

        }

        private FCResultType npFlowChart207_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart209_FlowRun(object sender, EventArgs e)
        {
            model.LocXYCountLmint = model.VisNgCount;
            if (model.LocationXYCount >= model.VisNgCount)
            {
                RefreashDifferentThreadUI(confirmFrm, () =>
                {
                    if (confirmFrm != null)
                    {
                        confirmFrm.Dispose();
                    }
                    confirmFrm = new frmConfirm();
                    confirmFrm.fnSetTextMessageNShow("pcb视觉贴装次数超限，选【YES】重新拍照，选【Cancel】当NG产品排出", true, false, true);
                });
                if (confirmFrm.dResult == DialogResult.Yes)
                {
                    model.LocationXYCount = 0;
                    model.Assemblytimessetup += (float)1;
                    pcbOffsetX = 0;
                    pcbOffsetY = 0;
                    model.SecLocationXY = new PositionInfor();
                    return FCResultType.CASE2;
                }
                else if (confirmFrm.dResult == DialogResult.Abort)
                {
                    return FCResultType.CASE1;
                }
                else { return FCResultType.IDLE; }
            }
            else
            {
                model.LocationXYCount++;
                DelayMs(500);
                return FCResultType.NEXT;
            }
        }

        private FCResultType npFlowChart176_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart218_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart216_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        #endregion

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void ReadData()
        {
            int temp = 0;
            List<double> time = new List<double>();
            List<double> pressure = new List<double>();
            List<CurvesParm> data = new List<CurvesParm>();
            CurvesParm curvesParm = new CurvesParm();
            do
            {
                temp++;
                pressure.Add(temp);
                //System.Windows.Application.Current.Dispatcher.Invoke(() => { model.ForceControlData = data; });

                Thread.Sleep(10);
            } while (temp < 241);
            //DateTime timeBase = new DateTime(2020, 1, 1, 0, 0, 0);
            //double timeCoefficientOA = timeBase.AddSeconds(0.1).ToOADate() - timeBase.ToOADate();
            double aa = 0;
            for (int i = 0; i < pressure.Count; i++)
            {
                time.Add(i * 10);
            }
            curvesParm.timeList = time;
            curvesParm.dataList = pressure;
            curvesParm.SN = "test";
            curvesParm.SaveImgPath = @"D:\WorkLog\ForceCurve\";

            data.Add(curvesParm);
            model.ForceControlData = data;

        }

        private FCResultType npFlowChart162_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart2_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("AssembPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z4, PosCom.ZPos - 5, model.Z4Speed, model.Z4AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart1_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        /// <summary>
        /// 写入PLC并检测返回成功
        /// </summary>
        /// <param name="addr"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private bool WritePlcIsSuccess(string addr, object value)
        {
            Type type = value.GetType();
            if (type == typeof(bool))
            {
                GloMotionParame.SiemenPLC.Write(addr, (bool)value);
                DelayMs(5);
                if ((bool)value == (bool)GloMotionParame.SiemenPLC.Read(addr)) return true;
                else return false;
            }
            else if (type == typeof(string))
            {
                GloMotionParame.SiemenPLC.Write(addr, (string)value);
                DelayMs(5);
                if ((string)value == (string)GloMotionParame.SiemenPLC.Read(addr)) return true;
                else return false;
            }
            else if (type == typeof(float))
            {
                GloMotionParame.SiemenPLC.Write(addr, (float)value);
                DelayMs(5);
                if ((float)value == (float)GloMotionParame.SiemenPLC.Read(addr)) return true;
                else return false;
            }
            else if (type == typeof(ushort))
            {
                GloMotionParame.SiemenPLC.Write(addr, (ushort)value);
                DelayMs(5);
                if ((ushort)value == (ushort)GloMotionParame.SiemenPLC.Read(addr)) return true;
                else return false;
            }
            return false;
        }

        private FCResultType npFlowChart3_FlowRun(object sender, EventArgs e)
        {
            //需要提前吸起来，才能软着陆，软着陆结束会直接抬起来
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_" + assembModel.Split('_')[0] + "Vacuum", true);
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_" + assembModel.Split('_')[0] + "BreakVacuum", false);
            DelayMs(100);
            if (Googo.Manul.GloMotionParame.InputList.GetValue("IB_" + assembModel.Split('_')[0] + "Vacuum"))
            {
                AutoRunJTime.Restart();
                Helper.ForceControl.StopSoftLand();
                return FCResultType.NEXT;
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    if (assembModel.Contains("RX"))
                    {
                        ShowAlarmInfo(1037);
                        Log.log.Write("RX吸取光阑真空超时", Color.Red);
                    }
                    else
                    {
                        ShowAlarmInfo(1038);
                        Log.log.Write("TX吸取光阑真空超时", Color.Red);
                    }
                    AutoRunJTime.Restart();
                    return FCResultType.IDLE;
                }
                return FCResultType.IDLE;
            }
        }
    }
    public struct CommunicateDgvData
    {
        public string name;
        public string data;
    }


    public struct MesDgvData
    {
        public string name;
        public string type;
        public string address;
        public string infor;
    }
}
