﻿using ModuleData20.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ModuleData20
{
    /// <summary>
    /// UC_MountingView.xaml 的交互逻辑
    /// </summary>
    public partial class UC_MountingView : UserControl
    {
        public UC_MountingView()
        {
            InitializeComponent();
        }

        public static DependencyProperty SubModuleProperty = DependencyProperty.Register("SubModule", typeof(MountingMode), typeof(UC_MountingView), new FrameworkPropertyMetadata(new MountingMode(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty SubProjectProperty = DependencyProperty.Register("SubProject", typeof(MountingProject), typeof(UC_MountingView), new FrameworkPropertyMetadata(new MountingProject(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));


        public MountingMode SubModule
        {
            get { return (MountingMode)GetValue(SubModuleProperty); }
            set { SetValue(SubModuleProperty, value); }
        }

        public MountingProject SubProject
        {
            get { return (MountingProject)GetValue(SubProjectProperty); }
            set { SetValue(SubProjectProperty, value); }
        }      
    }

    public class BoolStatusConv : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue || null == value || !(value is bool brun)) return "空闲";
            return brun ? "运行中" : "空闲";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
