﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using SuperSimpleTcp;
using Infrastructure;
using YHSDK;
using LogSv;
using ModuleData;
using ModuleData20.ViewModels;
using ShowAlarm;

namespace ModuleData20
{
    /// <summary>
    /// UCStationGlueFlow.xaml 的交互逻辑
    /// </summary>
    public partial class UCStationTwoInitial : UserControl, IModule
    {
        public UCStationTwoInitial()
        {
            InitializeComponent();
        }

        public static DependencyProperty DCClientProperty =
            DependencyProperty.Register("DCClient", typeof(SimpleTcpClient), typeof(UCStationTwoInitial), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));


        public static DependencyProperty DataModelProperty =
DependencyProperty.Register("DataModel", typeof(StationTwoIntial), typeof(UCStationTwoInitial), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));


        /// <summary>
        /// 德创TCP对象
        /// </summary>
        /// <remarks>
        /// TCP对象，到最外层直接进行绑定
        /// </remarks>
        public SimpleTcpClient DCClient
        {
            get { return (SimpleTcpClient)GetValue(DCClientProperty); }
            set { SetValue(DCClientProperty, value); }
        }

        public StationTwoIntial DataModel
        {
            get { return (StationTwoIntial)GetValue(DataModelProperty); }
            set { SetValue(DataModelProperty, value); }
        }
        private StationTwoIntial model = new StationTwoIntial();

        

        private static void DepenProChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            UCStationTwoInitial flow = d as UCStationTwoInitial;
            if (null == flow) return;
            if (e.Property == DCClientProperty)
            {
                SimpleTcpClient client = e.NewValue as SimpleTcpClient;
                flow.initial.DCTcpClient= client;
            }
            else if (e.Property == DataModelProperty)
            {
                flow.initial.DataModel = e.NewValue as StationTwoIntial;
            }

        }

        public bool IsRun
        {
            get
            {
     
                return initial.IsRun;
            }
        }

        public void RunAsync()
        {
            initial.RunAsync();
        }


        public void ResetChart()
        {
            initial.ResetChart();
        }

        public void Stop()
        {
            initial.Stop();
        }

    }
}
