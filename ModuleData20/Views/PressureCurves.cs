﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace ModuleData20
{
    public partial class PressureCurves : UserControl
    {
        /// <summary>
        /// 曲线名字
        /// </summary>
        public string CurveName;

        /// <summary>
        /// 产品的SN
        /// </summary>
        public string SerialNumber;

        public PressureCurves()
        {
            InitializeComponent();

            chart1.BackColor = Color.WhiteSmoke;
            chart1.BackGradientStyle = GradientStyle.TopBottom;
            chart1.BackSecondaryColor = Color.White;
            chart1.BorderlineColor = Color.FromArgb(26, 59, 105);
            chart1.BorderlineDashStyle = ChartDashStyle.Solid;
            chart1.BorderlineWidth = 2;
            chart1.BorderSkin.SkinStyle = BorderSkinStyle.None;

            ChartArea chartArea = new ChartArea("PressureCurves");
            chartArea.BackColor = Color.Gainsboro;
            chartArea.BackGradientStyle = GradientStyle.TopBottom;
            chartArea.BackSecondaryColor = Color.White;
            chartArea.BorderColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.BorderDashStyle = ChartDashStyle.Solid;
            chart1.ChartAreas.Add(chartArea);

            Title title = new Title("PressureCurves");
            title.Font = new Font("Trebuchet MS", 12F, FontStyle.Bold);
            title.ForeColor = Color.FromArgb(26, 59, 105);
            title.ShadowColor = Color.FromArgb(32, 0, 0, 0);
            title.ShadowOffset = 2;
            chart1.Titles.Add(title);

            Legend legend = new Legend("Series");
            legend.Alignment = StringAlignment.Far;
            legend.LegendStyle = LegendStyle.Row;
            legend.Docking = Docking.Bottom;
            legend.BackColor = Color.Transparent;
            legend.Font = new Font("Trebuchet MS", 8.25F, FontStyle.Bold);
            legend.IsTextAutoFit = false;
            chart1.Legends.Add(legend);

            Series seriesTorque = new Series("Pressure");
            seriesTorque.ChartType = SeriesChartType.FastLine;
            seriesTorque.ChartArea = "PressureCurves";
            seriesTorque.BorderColor = Color.FromArgb(224, 64, 10);
            seriesTorque.ShadowColor = Color.Black;
            seriesTorque.BorderWidth = 2;
            seriesTorque.XValueType = ChartValueType.Double;
            seriesTorque.YValueType = ChartValueType.Double;
            seriesTorque.YAxisType = AxisType.Primary;
            seriesTorque.XAxisType = AxisType.Primary;
            chart1.Series.Add(seriesTorque);

            chartArea.AxisX.Title = "Time";
            chartArea.AxisX.TitleFont = new Font("Trebuchet MS", 8.25F, FontStyle.Bold);
           // chartArea.AxisX.LabelStyle.Format = "s.f";
            chartArea.AxisX.LabelStyle.Font = new Font("Trebuchet MS", 8.25F, FontStyle.Bold);
            chartArea.AxisX.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisX.LineWidth = 2;
            chartArea.AxisX.MajorGrid.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisX.ScrollBar.LineColor = Color.Black;
            chartArea.AxisX.ScrollBar.Size = 10;
            chartArea.AxisX.Minimum = 0;
            chartArea.AxisX.Interval = 10;
            //chartArea.AxisX.LabelStyle.Format = "s.f";

            chartArea.AxisY.Title = "Pressure";
            chartArea.AxisY.TitleFont = new Font("Trebuchet MS", 8.25F, FontStyle.Bold);
            chartArea.AxisY.IsLabelAutoFit = false;
            chartArea.AxisY.LabelStyle.Font = new Font("Trebuchet MS", 8.25F, FontStyle.Bold);
            chartArea.AxisY.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisY.LineWidth = 2;
            chartArea.AxisY.MajorGrid.LineColor = Color.FromArgb(64, 64, 64, 64);
            chartArea.AxisY.ScrollBar.LineColor = Color.Black;
            chartArea.AxisY.ScrollBar.Size = 10;

        }

        /// <summary>
        /// 绘制压力曲线
        /// </summary>
        /// <param name="Pressure">压力数组</param>     
        /// <param name="Times">时间数组</param>
        public void ShowImage(List<double> Pressures, List<double> Times)
        {
            chart1.Titles[0].Text = $"Pressure Curves\n{CurveName} OF {SerialNumber}";
            chart1.Series["Pressure"].Points.Clear();       

            for (int i = 0; i < Pressures.Count; i++)
            {
                chart1.Series["Pressure"].Points.AddXY(Times[i], Pressures[i]);           
            }
            if (chart1.InvokeRequired)
            {
                chart1.Invoke(new Action(() => { chart1.Invalidate(); }));
            }
            else
            {
                chart1.Invalidate();
            }
        }

        /// <summary>
        /// 将图像保存为png文件
        /// </summary>
        /// <param name="pngFileName">png文件的完整路径</param>
        public void SaveImage(string pngFileName)
        {
            if (chart1.InvokeRequired)
            {
                chart1.Invoke(new Action(() => { chart1.SaveImage(pngFileName, ChartImageFormat.Png); }));
            }
            else
            {
                chart1.SaveImage(pngFileName, ChartImageFormat.Png);
            }

        }

        private void Initial_tsm_Click(object sender, EventArgs e)
        {
            Helper.ForceControl.InitialParm();
        }

        private void StartSoftLand_tsm_Click(object sender, EventArgs e)
        {

        }
    }
}
