﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Googo.Manul;
using LiveCharts;
using LiveCharts.Wpf;
using ModuleData20.ViewModels;

namespace ModuleData20.Views
{
    /// <summary>
    /// UCForceContro.xaml 的交互逻辑
    /// </summary>
    public partial class UCForceControl : System.Windows.Controls.UserControl, INotifyPropertyChanged
    {
        public static DependencyProperty DefectListProperty = DependencyProperty.Register("DefectList",
   typeof(List<CurvesParm>), typeof(UCForceControl), new FrameworkPropertyMetadata(new List<CurvesParm>(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnSourceChanged));

        //      public static DependencyProperty SaveImagePathProperty = DependencyProperty.Register("SaveImagePath",
        //typeof(string), typeof(UCForceControl), new FrameworkPropertyMetadata(string.Empty, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, OnSaveImage));

        public static DependencyProperty SubModuleProperty = DependencyProperty.Register("SubModule", typeof(MountingMode), typeof(UCForceControl), new FrameworkPropertyMetadata(new MountingMode(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));


        public List<CurvesParm> DefectList
        {
            get { return (List<CurvesParm>)GetValue(DefectListProperty); }
            set { SetValue(DefectListProperty, value); }
        }

        public MountingMode SubModule
        {
            get { return (MountingMode)GetValue(SubModuleProperty); }
            set { SetValue(SubModuleProperty, value); }
        }

        public UCForceControl()
        {
            InitializeComponent();
        }
        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {

                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged       


        private static void OnSourceChanged(DependencyObject sendor, DependencyPropertyChangedEventArgs e)
        {
            UCForceControl resultControl = sendor as UCForceControl;
            if (null == resultControl) return;
            if (e.Property == DefectListProperty)
            {
                List<CurvesParm> temp = e.NewValue as List<CurvesParm>;
                if (temp.Count < 1) return;

                resultControl.pressureCurves.CurveName = "VoiceCoilMotorCurrentCurve";
                resultControl.pressureCurves.SerialNumber = temp[0].SN;
                resultControl.pressureCurves.ShowImage(temp[0].dataList, temp[0].timeList);
                string imgPath = temp[0].SaveImgPath + temp[0].SN + "\\";
                if (!Directory.Exists(imgPath))
                {
                    Directory.CreateDirectory(imgPath);
                }
                string strTemp = imgPath + temp[0].SN + "_" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + ".png";
                if (null != resultControl.SubModule)
                    resultControl.SubModule.PressureCurvesPath = strTemp;
                resultControl.pressureCurves.SaveImage(strTemp);
                string header = string.Empty;
                string data = string.Empty;
                for (int i = 0; i < temp[0].dataList.Count; i++)
                {
                    header += temp[0].timeList[i].ToString() + ",";
                    data += temp[0].dataList[i].ToString() + ",";
                }
                SaveData(temp[0].SaveImgPath + temp[0].SN + "\\", header, data);
            }
        }

        private void Panel_MouseClick(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Right) == MouseButtons.Right)
            {
                ContextMenu.IsOpen = true;
            }
        }



        private void Init_Click(object sender, RoutedEventArgs e)
        {
            Helper.ForceControl.InitialParm();
        }

        private void StartSoftLand_Click(object sender, RoutedEventArgs e)
        {
            DefectList = new List<CurvesParm>();
            Helper.ForceControl.FirstPPSpd = SubModule.FirstPPSpd;
            Helper.ForceControl.FirstPPDis = SubModule.FirstPPDis;
            Helper.ForceControl.SecPVDis = SubModule.SecPVDis;
            Helper.ForceControl.ThirdPTSpd = SubModule.ThirdPTSpd;
            Helper.ForceControl.Preesure = SubModule.Preesure;
            Helper.ForceControl.StartSoftLand();
            Helper.ForceControl.StopSoftLand();
            DefectList = Helper.ForceControl.dataList;
        }

        private void Panel_MouseClick(object sender, MouseButtonEventArgs e)
        {
            if (e.RightButton == MouseButtonState.Pressed)
            {
                ContextMenu.IsOpen = true;
            }
        }

        public static void SaveData(string FileName, string header, string sSaveData)
        {
            //数据记录
            string sfileName = FileName;
            string strFileName = sfileName + "\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".csv";

            if (!Directory.Exists(sfileName))
            {
                Directory.CreateDirectory(sfileName);
            }
            if (!File.Exists(strFileName))
            {
                SaveCSV(header, strFileName);//列首名称
            }
            //string strMid = "";
            //for (int i = 0; i < sSaveData.Length; i++)
            //{
            //    strMid += sSaveData[i] + ",";
            //}
            SaveCSV(sSaveData, strFileName);
        }


        public static void SaveCSV(string str, string fullPath)
        {
            FileInfo fi = new FileInfo(fullPath);
            if (!fi.Directory.Exists)
            {
                fi.Directory.Create();
            }
            FileStream fs = new FileStream(fullPath, System.IO.FileMode.Append, System.IO.FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.UTF8);
            sw.WriteLine(str);
            sw.Close();
            fs.Close();
        }

    }

    public struct CurvesParm
    {
        public List<double> dataList;
        public List<double> timeList;
        public float MaxVal;
        public string SN;
        public string SaveImgPath;
    }
}
