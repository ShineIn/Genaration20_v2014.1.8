﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ModuleData20
{
    /// <summary>
    /// UCGlueResults.xaml 的交互逻辑
    /// </summary>
    public partial class UCMountingResults : UserControl
    {
        public UCMountingResults()
        {
            InitializeComponent();
        }

        public static DependencyProperty ResultListProperty = DependencyProperty.Register("ResultList", typeof(IList<ResultInfor>), typeof(UCMountingResults), new FrameworkPropertyMetadata(new ObservableCollection<ResultInfor>(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public IList<ResultInfor> ResultList
        {
            get { return (IList<ResultInfor>)GetValue(ResultListProperty); }
            set { SetValue(ResultListProperty, value); }
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (null != ResultList)
                ResultList.Clear();
        }
    }

    public class ListConvert : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (DependencyProperty.UnsetValue == value || null == value) return string.Empty;
            if (value is IEnumerable<float> list)
            {
                string strRet = string.Empty;
                foreach (var item in list)
                {
                    strRet += item.ToString("F3") + ",";
                }
                return strRet;
            }
            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
