﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using G4.Motion;
using Infrastructure;
using Microsoft.Win32;
using ModuleData20.ViewModels;
using VUControl;
using ModuleData.Views;

namespace ModuleData20
{
    /// <summary>
    /// Interaction logic for UCMounting.xaml
    /// </summary>
    public partial class UCMounting : System.Windows.Controls.UserControl
    {
        public UCMounting()
        {
            InitializeComponent();
            CreateAlarmDatagrid();
            LoadCommCMD();
        }

        public static DependencyProperty SubModuleProperty = DependencyProperty.Register("SubModule", typeof(MountingMode), typeof(UCMounting), new FrameworkPropertyMetadata(new MountingMode(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static DependencyProperty XAxisNumProperty = DependencyProperty.Register("XAxisNum", typeof(EnumAxisType), typeof(UCMounting), new FrameworkPropertyMetadata(EnumAxisType.Y3, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty YAxisNumProperty = DependencyProperty.Register("YAxisNum", typeof(EnumAxisType), typeof(UCMounting), new FrameworkPropertyMetadata(EnumAxisType.Z3, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty ZAxisNumProperty = DependencyProperty.Register("ZAxisNum", typeof(EnumAxisType), typeof(UCMounting), new FrameworkPropertyMetadata(EnumAxisType.Z4, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));


        public static DependencyProperty R1AxisNumProperty = DependencyProperty.Register("R1AxisNum", typeof(EnumAxisType), typeof(UCMounting), new FrameworkPropertyMetadata(EnumAxisType.R1, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty R2AxisNumProperty = DependencyProperty.Register("R2AxisNum", typeof(EnumAxisType), typeof(UCMounting), new FrameworkPropertyMetadata(EnumAxisType.R2, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty R3AxisNumProperty = DependencyProperty.Register("R3AxisNum", typeof(EnumAxisType), typeof(UCMounting), new FrameworkPropertyMetadata(EnumAxisType.R3, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));


        public static DependencyProperty Glue1XAxisNumProperty = DependencyProperty.Register("Glue1XAxisNum", typeof(EnumAxisType), typeof(UCMounting), new FrameworkPropertyMetadata(EnumAxisType.X1, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty Glue1YAxisNumProperty = DependencyProperty.Register("Glue1YAxisNum", typeof(EnumAxisType), typeof(UCMounting), new FrameworkPropertyMetadata(EnumAxisType.Y1, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty Glue1ZAxisNumProperty = DependencyProperty.Register("Glue1ZAxisNum", typeof(EnumAxisType), typeof(UCMounting), new FrameworkPropertyMetadata(EnumAxisType.Z1, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        
        public static DependencyProperty VisableProperty = DependencyProperty.Register("Visable", typeof(Visibility), typeof(UCMounting), new FrameworkPropertyMetadata(Visibility.Collapsed, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));


        //※★※★※★※★※★※★※★【通讯交互命令】※★※★※★※★※★※★※★※★※
        #region【设备配方参数】
        public static readonly string DeviceRecipeParaPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\CommCMD\\DeviceRecipeCMD.xml");
        public static Dictionary<string, string> DicDeviceRecipeCMD;

        #endregion
        #region【设备报工参数】
        public static readonly string DeviceReportParaPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\CommCMD\\DeviceReportCMD.xml");
        public static Dictionary<string, string> DicDeviceReportCMD;

        #endregion
        #region【设备右工位报工参数】
        public static readonly string DeviceRightReportParaPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\CommCMD\\DeviceRightReportCMD.xml");
        public static Dictionary<string, string> DicDeviceRightReportCMD;
        #endregion

        #region【1#点胶模组参数】
        //点胶模组1运控读取流程
        public static readonly string Dispense1ReadMovePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\CommCMD\\Dispense1ReadMoveCMD.xml");
        public static Dictionary<string, string> DicDispense1ReadMoveCMD;
        //点胶模组1运控写入流程
        public static readonly string Dispense1WriteMovePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\CommCMD\\Dispense1WriteMoveCMD.xml");
        public static Dictionary<string, string> DicDispense1WriteMoveCMD;
        //点胶模组读取流程数据
        public static readonly string Dispense1ReadFlowPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\CommCMD\\Dispense1ReadFlowCMD.xml");
        public static Dictionary<string, string> DicDispense1ReadFlowCMD;
        //点胶模组1写入流程数据
        public static readonly string Dispense1WriteFlowPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\CommCMD\\Dispense1WriteFlowCMD.xml");
        public static Dictionary<string, string> DicDispense1WriteFlowCMD;
        #endregion

        #region【2#点胶模组参数】
        //点胶模组2运控读取流程
        public static readonly string Dispense2ReadMovePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\CommCMD\\Dispense2ReadMoveCMD.xml");
        public static Dictionary<string, string> DicDispense2ReadMoveCMD;
        //点胶模组2运控写入流程
        public static readonly string Dispense2WriteMovePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\CommCMD\\Dispense2WriteMoveCMD.xml");
        public static Dictionary<string, string> DicDispense2WriteMoveCMD;
        //点胶模组读取流程数据
        public static readonly string Dispense2ReadFlowPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\CommCMD\\Dispense2ReadFlowCMD.xml");
        public static Dictionary<string, string> DicDispense2ReadFlowCMD;
        //点胶模组2写入流程数据
        public static readonly string Dispense2WriteFlowPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\CommCMD\\Dispense2WriteFlowCMD.xml");
        public static Dictionary<string, string> DicDispense2WriteFlowCMD;
        #endregion

        #region【1#点胶模组参数】
        //贴装模组1运控读取流程
        public static readonly string Mounting1ReadMovePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\CommCMD\\Mounting1ReadMoveCMD.xml");
        public static Dictionary<string, string> DicMounting1ReadMoveCMD;
        //贴装模组1运控写入流程
        public static readonly string Mounting1WriteMovePath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\CommCMD\\Mounting1WriteMoveCMD.xml");
        public static Dictionary<string, string> DicMounting1WriteMoveCMD;
        //贴装模组1读取流程数据
        public static readonly string Mounting1ReadFlowPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\CommCMD\\Mounting1ReadFlowCMD.xml");
        public static Dictionary<string, string> DicMounting1ReadFlowCMD;
        //贴装模组1写入流程数据
        public static readonly string Mounting1WriteFlowPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\CommCMD\\Mounting1WriteFlowCMD.xml");
        public static Dictionary<string, string> DicMounting1WriteFlowCMD;
        //贴装模组2写入流程数据
        public static readonly string Mounting2WriteFlowPath = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Config\\CommCMD\\Mounting2WriteFlowCMD.xml");
        public static Dictionary<string, string> DicMounting2WriteFlowCMD;


        public static string[] CommCMDDgvHeader = new string[] { "序号", "DB地址", "名称", "数据类型", "备注" };
        #endregion
        //※★※★※★※★※★※★※★※★※★※★※★※★※★※★※★※★※★※★※★※★※★
        public MountingMode SubModule
        {
            get { return (MountingMode)GetValue(SubModuleProperty); }
            set { SetValue(SubModuleProperty, value); }
        }


        public EnumAxisType XAxisNum
        {
            get { return (EnumAxisType)GetValue(XAxisNumProperty); }
            set { SetValue(XAxisNumProperty, value); }
        }

        public EnumAxisType YAxisNum
        {
            get { return (EnumAxisType)GetValue(YAxisNumProperty); }
            set { SetValue(YAxisNumProperty, value); }
        }

        public EnumAxisType ZAxisNum
        {
            get { return (EnumAxisType)GetValue(ZAxisNumProperty); }
            set { SetValue(ZAxisNumProperty, value); }
        }

        public EnumAxisType R1AxisNum
        {
            get { return (EnumAxisType)GetValue(R1AxisNumProperty); }
            set { SetValue(R1AxisNumProperty, value); }
        }

        public EnumAxisType R2AxisNum
        {
            get { return (EnumAxisType)GetValue(R2AxisNumProperty); }
            set { SetValue(R2AxisNumProperty, value); }
        }

        public EnumAxisType R3AxisNum
        {
            get { return (EnumAxisType)GetValue(R3AxisNumProperty); }
            set { SetValue(R3AxisNumProperty, value); }
        }

        public EnumAxisType Glue1XAxisNum
        {
            get { return (EnumAxisType)GetValue(Glue1XAxisNumProperty); }
            set { SetValue(Glue1XAxisNumProperty, value); }
        }

        public EnumAxisType Glue1YAxisNum
        {
            get { return (EnumAxisType)GetValue(Glue1YAxisNumProperty); }
            set { SetValue(Glue1YAxisNumProperty, value); }
        }

        public EnumAxisType Glue1ZAxisNum
        {
            get { return (EnumAxisType)GetValue(Glue1ZAxisNumProperty); }
            set { SetValue(Glue1ZAxisNumProperty, value); }
        }

        public static VUControl.VUDatagridView Dgv_MReadFlowPara
        {
            get { return dgv_MReadFlowPara; }
            set { dgv_MReadFlowPara = value; }
        }
        public static VUControl.VUDatagridView dgv_MReadFlowPara = new VUControl.VUDatagridView();

        public static VUControl.VUDatagridView Dgv_MWriteFlowPara
        {
            get { return dgv_MWriteFlowPara; }
            set { dgv_MWriteFlowPara = value; }
        }
        public static VUControl.VUDatagridView dgv_MWriteFlowPara = new VUControl.VUDatagridView();

        public static VUControl.VUDatagridView Dgv_MWriteMovePara
        {
            get { return dgv_MWriteMovePara; }
            set { dgv_MWriteMovePara = value; }
        }
        public static VUControl.VUDatagridView dgv_MWriteMovePara = new VUControl.VUDatagridView();

        public static VUControl.VUDatagridView Dgv_MReadMovePara
        {
            get { return dgv_MReadMovePara; }
            set { dgv_MReadMovePara = value; }
        }
        public static VUControl.VUDatagridView dgv_MReadMovePara = new VUControl.VUDatagridView();




        public static VUControl.VUDatagridView Dgv_G2ReadFlowPara
        {
            get { return dgv_G2ReadFlowPara; }
            set { dgv_G2ReadFlowPara = value; }
        }
        public static VUControl.VUDatagridView dgv_G2ReadFlowPara = new VUControl.VUDatagridView();

        public static VUControl.VUDatagridView Dgv_G2WriteFlowPara
        {
            get { return dgv_G2WriteFlowPara; }
            set { dgv_G2WriteFlowPara = value; }
        }
        public static VUControl.VUDatagridView dgv_G2WriteFlowPara = new VUControl.VUDatagridView();

        public static VUControl.VUDatagridView Dgv_G2WriteMovePara
        {
            get { return dgv_G2WriteMovePara; }
            set { dgv_G2WriteMovePara = value; }
        }
        public static VUControl.VUDatagridView dgv_G2WriteMovePara = new VUControl.VUDatagridView();

        public static VUControl.VUDatagridView Dgv_G2ReadMovePara
        {
            get { return dgv_G2ReadMovePara; }
            set { dgv_G2ReadMovePara = value; }
        }
        public static VUControl.VUDatagridView dgv_G2ReadMovePara = new VUControl.VUDatagridView();

        


        public Visibility Visable
        {
            get { return (Visibility)GetValue(VisableProperty); }
            set { SetValue(VisableProperty, value); }
        }

        #region【加载通讯交互指令】
        private void LoadCommCMD()
        {
            #region【MES通讯交互指令加载】
            if (dgv_MesRecipePara.LoadXmlFileFromPath(DeviceRecipeParaPath))
                DataGridToDictionary(dgv_MesRecipePara.myDgv, ref DicDeviceRecipeCMD);
            #endregion
            #region【设备报工通讯交互指令加载】
            if (dgv_MesReportPara.LoadXmlFileFromPath(DeviceReportParaPath))
                DataGridToDictionary(dgv_MesReportPara.myDgv, ref DicDeviceReportCMD);
            #endregion
            #region【设备右工位报工通讯交互指令加载】
            if (dgv_RightMesReportPara.LoadXmlFileFromPath(DeviceRightReportParaPath))
                DataGridToDictionary(dgv_RightMesReportPara.myDgv, ref DicDeviceRightReportCMD);
            #endregion    

            #region【贴装模组1通讯交互指令加载】
            //读取贴装1控制流程
            if (dgv_MountingReadMovePara.LoadXmlFileFromPath(Dispense1ReadMovePath))
                DataGridToDictionary(dgv_MountingReadMovePara.myDgv, ref DicMounting1ReadMoveCMD);
            //写入贴装1运动流程
            if (dgv_MountingWriteMovePara.LoadXmlFileFromPath(Dispense1WriteMovePath))
                DataGridToDictionary(dgv_MountingWriteMovePara.myDgv, ref DicMounting1WriteMoveCMD);
            //读取贴装1流程
            if (dgv_MountingReadFlowPara.LoadXmlFileFromPath(Dispense1ReadFlowPath))
                DataGridToDictionary(dgv_MountingReadFlowPara.myDgv, ref DicMounting1ReadFlowCMD);
            //写入贴装1流程
            if (dgv_MountingWriteFlowPara.LoadXmlFileFromPath(Dispense1WriteFlowPath))
                DataGridToDictionary(dgv_MountingWriteFlowPara.myDgv, ref DicMounting1WriteFlowCMD);

            dgv_MReadFlowPara = dgv_MountingReadFlowPara;
            dgv_MWriteFlowPara = dgv_MountingWriteFlowPara;
            dgv_MWriteMovePara = dgv_MountingWriteMovePara;
            dgv_MReadMovePara = dgv_MountingReadMovePara;

            #endregion
            #region【点胶模组2通讯交互指令加载】
            //读取贴装1控制流程
            if (dgv_Glue2ReadMovePara.LoadXmlFileFromPath(Dispense2ReadMovePath))
                DataGridToDictionary(dgv_Glue2ReadMovePara.myDgv, ref DicDispense2ReadMoveCMD);
            //写入贴装1运动流程
            if (dgv_Glue2WriteMovePara.LoadXmlFileFromPath(Dispense2WriteMovePath))
                DataGridToDictionary(dgv_Glue2WriteMovePara.myDgv, ref DicDispense2WriteMoveCMD);
            //读取贴装1流程
            if (dgv_Glue2ReadFlowPara.LoadXmlFileFromPath(Dispense2ReadFlowPath))
                DataGridToDictionary(dgv_Glue2ReadFlowPara.myDgv, ref DicDispense2ReadFlowCMD);
            //写入贴装1流程
            if (dgv_Glue2WriteFlowPara.LoadXmlFileFromPath(Dispense2WriteFlowPath))
                DataGridToDictionary(dgv_Glue2WriteFlowPara.myDgv, ref DicDispense2WriteFlowCMD);

            dgv_G2ReadFlowPara = dgv_Glue2ReadFlowPara;
            dgv_G2WriteFlowPara = dgv_Glue2WriteFlowPara;
            dgv_G2WriteMovePara = dgv_Glue2WriteMovePara;
            dgv_G2ReadMovePara = dgv_Glue2ReadMovePara;

            #endregion

            InitDgv_MesSetting(dgv_MesRecipePara, ref uc_MountingFlow.MesRecipeDgvDatas);
            InitDgv_MesSetting(dgv_MesReportPara, ref uc_MountingFlow.MesLeftReportDgvDatas);
            InitDgv_MesSetting(dgv_RightMesReportPara, ref uc_MountingFlow.MesRightReportDgvDatas);

            InitDgv_CommunicateSetting(Dgv_MReadMovePara, ref uc_MountingFlow.Dispense1MoveReadDgvDatas);
            InitDgv_CommunicateSetting(Dgv_MWriteMovePara, ref uc_MountingFlow.Dispense1MoveWriteDgvDatas);
            InitDgv_CommunicateSetting(Dgv_MReadFlowPara, ref uc_MountingFlow.Dispense1FlowReadDgvDatas);
            InitDgv_CommunicateSetting(Dgv_MWriteFlowPara, ref uc_MountingFlow.Dispense1FlowWriteDgvDatas);

            InitDgv_CommunicateSetting(Dgv_G2ReadMovePara, ref uc_MountingFlow.Dispense2MoveReadDgvDatas);
            InitDgv_CommunicateSetting(Dgv_G2WriteMovePara, ref uc_MountingFlow.Dispense2MoveWriteDgvDatas);
            InitDgv_CommunicateSetting(Dgv_G2ReadFlowPara, ref uc_MountingFlow.Dispense2FlowReadDgvDatas);
            InitDgv_CommunicateSetting(Dgv_G2WriteFlowPara, ref uc_MountingFlow.Dispense2FlowWriteDgvDatas);

        }


        #region【保存报警代码】
        private void SaveCommCMD()
        {
            ///MES通讯交互指令保存
            dgv_MesRecipePara.SaveXmlFileFromPath(DeviceRecipeParaPath);
            dgv_MesReportPara.SaveXmlFileFromPath(DeviceReportParaPath);
            dgv_RightMesReportPara.SaveXmlFileFromPath(DeviceRightReportParaPath);

            ///贴装模组1通讯交互指令保存
            dgv_MountingReadMovePara.SaveXmlFileFromPath(Dispense1ReadMovePath);
            dgv_MountingWriteMovePara.SaveXmlFileFromPath(Dispense1WriteMovePath);
            dgv_MountingReadFlowPara.SaveXmlFileFromPath(Dispense1ReadFlowPath);
            dgv_MountingWriteFlowPara.SaveXmlFileFromPath(Dispense1WriteFlowPath);
            ///点胶模组2通讯交互指令保存
            dgv_Glue2ReadMovePara.SaveXmlFileFromPath(Dispense2ReadMovePath);
            dgv_Glue2WriteMovePara.SaveXmlFileFromPath(Dispense2WriteMovePath);
            dgv_Glue2ReadFlowPara.SaveXmlFileFromPath(Dispense2ReadFlowPath);
            dgv_Glue2WriteFlowPara.SaveXmlFileFromPath(Dispense2WriteFlowPath);
        }
        #endregion

        private void DataGridToDictionary(DataGridView codeDgv, ref Dictionary<string, string> cmdCodeDic)
        {
            if (cmdCodeDic == null) cmdCodeDic = new Dictionary<string, string>();
            if (cmdCodeDic.Count > 0) cmdCodeDic.Clear();
            for (int i = 0; i < codeDgv.Rows.Count; i++)
            {
                if (!cmdCodeDic.ContainsKey(codeDgv.Rows[i].Cells[1].Value.ToString()))
                    cmdCodeDic.Add(codeDgv.Rows[i].Cells[1].Value.ToString(), codeDgv.Rows[i].Cells[2].Value.ToString());
            }
        }
        #endregion

        #region 获取dgv数据
        public void InitDgv_CommunicateSetting(VUDatagridView dataGridView, ref List<CommunicateDgvData> communicateDgvDatas)
        {
            communicateDgvDatas.Clear();
            CommunicateDgvData dgvData = new CommunicateDgvData();
            for (int i = 0; i < dataGridView.myDgv.Rows.Count; i++)
            {
                dgvData.name = dataGridView.GetDataArrayByIndex(i)[2];
                dgvData.data = dataGridView.GetDataArrayByIndex(i)[1];
                communicateDgvDatas.Add(dgvData);
            }
        }

        public void InitDgv_MesSetting(VUDatagridView dataGridView, ref List<MesDgvData> mesDgvDatas)
        {
            mesDgvDatas.Clear();
            MesDgvData dgvData = new MesDgvData();
            for (int i = 0; i < dataGridView.myDgv.Rows.Count; i++)
            {
                dgvData.address = dataGridView.GetDataArrayByIndex(i)[1];
                dgvData.name = dataGridView.GetDataArrayByIndex(i)[2];
                dgvData.type = dataGridView.GetDataArrayByIndex(i)[3];
                dgvData.infor = dataGridView.GetDataArrayByIndex(i)[4];
                mesDgvDatas.Add(dgvData);
            }
        }

        public string GetAddressFormDgv(string name, List<CommunicateDgvData> communicateDgvDatas)
        {
            if (communicateDgvDatas != null && communicateDgvDatas.Count > 0)
            {
                for (int i = 0; i < communicateDgvDatas.Count; i++)
                {
                    if (communicateDgvDatas[i].name == name)
                        return communicateDgvDatas[i].data;
                }
                return "999";
            }
            else { return "999"; }
        }
        #endregion
        #region【初始化表头】
        internal void CreateAlarmDatagrid()
        {
            #region【MES交互命令】
            InitDataGridHeader(ref dgv_MesRecipePara, CommCMDDgvHeader);
            InitDataGridHeader(ref dgv_MesReportPara, CommCMDDgvHeader);
            InitDataGridHeader(ref dgv_RightMesReportPara, CommCMDDgvHeader);
            #endregion


            #region【贴装模组交互指令】
            InitDataGridHeader(ref dgv_MountingReadMovePara, CommCMDDgvHeader);
            InitDataGridHeader(ref dgv_MountingWriteMovePara, CommCMDDgvHeader);
            InitDataGridHeader(ref dgv_MountingReadFlowPara, CommCMDDgvHeader);
            InitDataGridHeader(ref dgv_MountingWriteFlowPara, CommCMDDgvHeader);
            #endregion

            #region【贴装模组交互指令】
            InitDataGridHeader(ref dgv_Glue2ReadMovePara, CommCMDDgvHeader);
            InitDataGridHeader(ref dgv_Glue2WriteMovePara, CommCMDDgvHeader);
            InitDataGridHeader(ref dgv_Glue2ReadFlowPara, CommCMDDgvHeader);
            InitDataGridHeader(ref dgv_Glue2WriteFlowPara, CommCMDDgvHeader);
            #endregion

        }


        /// <summary>
        /// 初始化表头
        /// </summary>
        private void InitDataGridHeader(ref VUDatagridView myDgv, string[] dgvHeader)
        {
            myDgv.DgvHeader = dgvHeader;
            myDgv.myDgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        #endregion

        private void TabControl_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if(Visable == Visibility.Collapsed)
                Visable = Visibility.Visible;
            else
                Visable = Visibility.Collapsed;
        }
    }
}
