﻿using G4.GlueCore;
using G4.Motion;
using Googo.Manul;
using LogSv;
using ModuleData.ViewModels;
using SuperSimpleTcp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Xml.Serialization;
using YHSDK;
using System.Windows.Threading;
using ModuleData20.ViewModels;
using ModuleData;
using ShowAlarm;

namespace ModuleData20
{
    [Serializable]
    public partial class uc_GlueInitProcess : ModuleBase
    {
        public uc_GlueInitProcess()
        {
            InitializeComponent();

        }

        private int GlueModuleError = 0;
        /// <summary>
        /// 点胶对象1
        /// </summary>
        private GlueModule glueModule;

        /// <summary>
        /// 点胶对象2
        /// </summary>
        private GlueModule glueModule2;

        private JTimer InitialJTime = new JTimer();

        public bool InitialCompeleted = false;

        #region 公有属性

        public StationTwoIntial DataModel
        {
            get { return model; }
            set { model = value; OnPropertyChanged(); }
        }
        private StationTwoIntial model = new StationTwoIntial();


        /// <summary>
        /// 是否初始化模块
        /// </summary>
        public override bool IsInitialModule { get { return true; } }

        #endregion 公有属性

        #region 公有函数
        protected override void Run()
        {
            if (null == model) return;
            if (fc_GlueAuto1.WorkFlow == null)
                fc_GlueAuto1.TaskReset();
            fc_GlueAuto1.TaskRun();
        }

        public override void ResetChart()
        {
            StartChart = fc_GlueAuto1;
            fc_GlueAuto1.TaskReset();
        }




        #endregion 公有函数

        private int MoveToHomeXYR()
        {
            var XYRRet = Task.WhenAll(Task.Run(() => { return G4.Motion.MotionModel.Instance.AxisMoveToHome(G4.Motion.EnumAxisType.R1); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.AxisMoveToHome(G4.Motion.EnumAxisType.R2); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.AxisMoveToHome(G4.Motion.EnumAxisType.R3); }));
            XYRRet.Wait();
            var result = XYRRet.Result;
            if (result.All(p => p == 0))
            {
                return 0;
            }
            else { return 1; }
        }

        private YHSDK.FCResultType npFlowChart1_FlowRun(object sender, EventArgs e)
        {
            //bool bret = model.Signal_R_Initialize;
            //model.Signal_R_Initialize = bret;
            if (true)
            {
                BInitIsOK = false;
                glueModule = new GlueModule(DataModel.AxisWorkModule1, new GlueModuleParams());
                glueModule2 = new GlueModule(DataModel.AxisWorkModule2, new GlueModuleParams());
                model.Signal_W_InitializeOK = false;
                model.Signal_W_Initializing = true;
                GloMotionParame.SiemenPLC.Write("DB3001.DBX1.3", true);         //初始化中
                GloMotionParame.SiemenPLC.Write("DB3101.DBX1.3", true);
                G4.Motion.MotionModel.Instance.StopAllAxes();
                Log.log.Write(string.Format($"接收到初始化指令，开始初始化任务"), Color.Black);
                return FCResultType.NEXT;
            }
            return FCResultType.IDLE;

        }
        private FCResultType npFlowChart2_FlowRun(object sender, EventArgs e)
        {
            //注意这个路径可能要更改
            string exefile = @"C:\Program Files\DCCKVisionPlus\Libs\DCCK.VisionPlus.GStudio.exe";
            if (ProcessIsOpen("DCCK.VisionPlus.GStudio", 1))
            {
                return FCResultType.NEXT;
                //Xceed.Wpf.Toolkit.MessageBox.Show("V+软件已经开启，请勿重复打开.......", "警告", MessageBoxButton.OK, MessageBoxImage.Warning, null);
            }
            if (File.Exists(exefile))
            {
                Process process = new Process();   // params 为 string 类型的参数，多个参数以空格分隔，如果某个参数为空，可以传入””
                ProcessStartInfo startInfo = new ProcessStartInfo(exefile);
                process.StartInfo = startInfo;
                process.Start();
                return FCResultType.NEXT;
            }
            else
            {
                Log.log.Write("视觉软件路径不正确，打开失败", Color.Red);
                return FCResultType.IDLE;
            }
        }

        /// <summary>
        /// 防止程序二次启动
        /// </summary>
        /// <param name="ProcessName">程序名称</param>
        /// <param name="Counter">程序允许打开个数</param>
        /// <returns></returns>
        private bool ProcessIsOpen(string ProcessName, int Counter)
        {
            string[] ProcessNames;
            bool openFlag = false;
            Process[] pro = Process.GetProcesses();
            ProcessNames = new string[pro.Length];
            int count = 0;
            for (int i = 0; i < pro.Length; i++)
            {
                ProcessNames[i] = pro[i].ProcessName.ToString();
                if (pro[i].ProcessName.ToString().Contains(ProcessName) && !pro[i].ProcessName.ToString().Contains(".vshost"))                    //遍历进程名称
                {
                    count++;
                }
                if (count == Counter)
                {
                    openFlag = true;
                    break;
                }
                else
                    openFlag = false;
            }
            return openFlag;
        }

        private FCResultType npFlowChart3_FlowRun(object sender, EventArgs e)
        {
            if (null != DCTcpClient && DCTcpClient.IsConnected)
            {
                return FCResultType.NEXT;
            }
            else
            {
                try
                {
                    DCTcpClient.Connect();
                    return FCResultType.NEXT;
                }
                catch (Exception)
                {

                    PrintLog($"DCCK TCP连接断开", System.Drawing.Color.Red);
                    return FCResultType.IDLE;
                }
            }
        }

        /// <summary>
        /// 复位点胶模组
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        private FCResultType npFlowChart8_FlowRun(object sender, EventArgs e)
        {
            //model.Signal_W_InitializeOK = true;
            //model.Signal_W_Initializing = false;//点胶任务接受
            GloMotionParame.SiemenPLC.Write("DB3001.DBX0.1", true);
            GloMotionParame.SiemenPLC.Write("DB3101.DBX0.1", true);
            GloMotionParame.SiemenPLC.Write("DB3001.DBX1.0", true);         //左空闲
            GloMotionParame.SiemenPLC.Write("DB3101.DBX1.0", true);         //右空闲
            Thread.Sleep(500);
            if ((bool)GloMotionParame.SiemenPLC.Read("DB3001.DBX0.1") && (bool)GloMotionParame.SiemenPLC.Read("DB3101.DBX0.1"))
            {
                BInitIsOK = true;
                InitialCompeleted = true;
                Log.log.Write("返回plc初始化完成状态", Color.Red);
                Stop();
            }
            return FCResultType.IDLE;
        }

        private FCResultType fc_GlueAuto6_FlowRun(object sender, EventArgs e)
        {

            var rtn = Task.WhenAll(Task.Run(() => { return glueModule.ExecuteMoveToHome(); }),
            Task.Run(() => { return glueModule2.ExecuteMoveToHome(); }));
            rtn.Wait();
            if (0 != rtn.Result[0])
            {
                Log.log.Write($"左点胶模组回原失败，错误码={rtn.Result[0]}", System.Drawing.Color.Red);
                return FCResultType.CASE1;
            }
            if (0 != rtn.Result[1])
            {
                Log.log.Write($"右点胶模组回原失败，错误码={rtn.Result[1]}", System.Drawing.Color.Red);
                return FCResultType.CASE1;
            }
            return FCResultType.NEXT;
        }

        private FCResultType exptionFC_FlowRun(object sender, EventArgs e)
        {
            model.Signal_W_InitializeOK = false;
            model.Signal_W_Initializing = false;
            //从配置表里拿数据进行异常处理
            GloMotionParame.WriteErrCode(StageIndex, (short)GlueModuleError);
            return FCResultType.NEXT;
        }

        private FCResultType UVup_FlowRun(object sender, EventArgs e)
        {
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_UVLightUp", true);
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_UVLightDown", false);
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_UVLightOn", false);
            if (Googo.Manul.GloMotionParame.InputList.GetValue("IB_UVLightUp") && !Googo.Manul.GloMotionParame.InputList.GetValue("IB_UVLightDown")
                && !Googo.Manul.GloMotionParame.InputList.GetValue("IB_UVLightOn"))
            {
                InitialJTime.Restart();
                return FCResultType.NEXT;
            }
            else
            {
                if (InitialJTime.On(30000))
                {
                    Log.log.Write("UV灯复位超时", Color.Red);
                    return FCResultType.IDLE;
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart1_FlowRun_1(object sender, EventArgs e)
        {
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_RXCoverUp", true);
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_RXCoverDown", false);
            //Googo.Manul.GloMotionParame.OutputList.SetValue("OB_MoveDispenseCIn", true);
            //Googo.Manul.GloMotionParame.OutputList.SetValue("OB_MoveDispenseCyOut", false);
            if (Googo.Manul.GloMotionParame.InputList.GetValue("IB_RXCoverUp") && !Googo.Manul.GloMotionParame.InputList.GetValue("IB_RXCoverDown")
                /*&& Googo.Manul.GloMotionParame.InputList.GetValue("IB_MoveDispenseVacuumIn") && !Googo.Manul.GloMotionParame.InputList.GetValue("IB_MoveDispenseVacuumOut")*/)
            {
                InitialJTime.Restart();
                return FCResultType.NEXT;
            }
            else
            {
                if (InitialJTime.On(30000))
                {
                    Log.log.Write("气缸回原超时", Color.Red);
                    return FCResultType.IDLE;
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart2_FlowRun_1(object sender, EventArgs e)
        {
            int iret = G4.Motion.MotionModel.Instance.AxisMoveToHome(G4.Motion.EnumAxisType.Z3);
            if (0 == iret)
            {
                return FCResultType.NEXT;
            }
            Log.log.Write($"Z3轴回原超时,错误码={iret}", System.Drawing.Color.Red);
            return FCResultType.CASE1;
        }

        private FCResultType npFlowChart5_FlowRun(object sender, EventArgs e)
        {
            int iret = MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z3, model.Z3SafetyPos, 20, 20);
            if (0 == iret)
            {
                return FCResultType.NEXT;
            }
            Log.log.Write($"Z3回安全高度超时,错误码={iret}", System.Drawing.Color.Red);
            return FCResultType.CASE1;
        }

        private FCResultType npFlowChart8_FlowRun_1(object sender, EventArgs e)
        {
            Helper.ForceControl.InitialParm();          //先初始化软着陆，改为位移模式
            int iret = G4.Motion.MotionModel.Instance.AxisMoveToHome(G4.Motion.EnumAxisType.Z4);
            if (0 == iret)
            {
                return FCResultType.NEXT;
            }
            Log.log.Write($"Z4轴回原,错误码={iret}", System.Drawing.Color.Red);
            return FCResultType.CASE1;
        }

        private FCResultType npFlowChart10_FlowRun(object sender, EventArgs e)
        {
            int iret = G4.Motion.MotionModel.Instance.AxisMoveToHome(G4.Motion.EnumAxisType.Y3);
            if (0 == iret)
            {
                return FCResultType.NEXT;
            }
            Log.log.Write($"Y3轴回原失败,错误码={iret}", System.Drawing.Color.Red);
            return FCResultType.CASE1;
        }

        private FCResultType npFlowChart12_FlowRun(object sender, EventArgs e)
        {
            int iret = MoveToHomeXYR();
            if (0 == iret)
            {
                return FCResultType.NEXT;
            }
            Log.log.Write($"微动平台回原失败,错误码={iret}", System.Drawing.Color.Red);
            return FCResultType.CASE1;
        }

        private FCResultType npFlowChart13_FlowRun(object sender, EventArgs e)
        {
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_MoveDispenseCIn", true);
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_MoveDispenseCyOut", false);
            Stopwatch stopwatch = Stopwatch.StartNew();
            while ((!GloMotionParame.InputList.GetValue("IB_MoveDispenseVacuumIn") || GloMotionParame.InputList.GetValue("IB_MoveDispenseVacuumOut"))
                && stopwatch.ElapsedMilliseconds < 5000)
            {
                Thread.Sleep(50);
            }
            if (!GloMotionParame.InputList.GetValue("IB_MoveDispenseVacuumIn") || GloMotionParame.InputList.GetValue("IB_MoveDispenseVacuumOut"))
            {
                Log.log.Write("移栽气缸回原超时", Color.Red);
                return FCResultType.CASE1;
            }
            return FCResultType.NEXT;
        }
    }
}
