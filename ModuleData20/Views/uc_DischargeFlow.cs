﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ModuleData20.Views;
using ModuleData20.ViewModels;
using YHSDK;
using G4.GlueCore;
using System.Threading;
using Googo.Manul;
using ModuleData;
using System.Diagnostics;
using System.Xml.Serialization;
using SuperSimpleTcp;
using VUControl;
using LogSv;
using ModuleData.ViewModels;
using ShowAlarm;
using System.Reflection;
using System.Windows.Media.Media3D;

namespace ModuleData20
{
    public partial class uc_DischargeFlow : ModuleBase
    {
        public uc_DischargeFlow()
        {
            InitializeComponent();
            InitDgv_CommunicateSetting(UCMounting.Dgv_MReadMovePara, ref Dispense1MoveReadDgvDatas);
            InitDgv_CommunicateSetting(UCMounting.Dgv_MWriteMovePara, ref Dispense1MoveWriteDgvDatas);
            InitDgv_CommunicateSetting(UCMounting.Dgv_MReadFlowPara, ref Dispense1FlowReadDgvDatas);
            InitDgv_CommunicateSetting(UCMounting.Dgv_MWriteFlowPara, ref Dispense1FlowWriteDgvDatas);
        }

        #region 公有属性
        protected override void Run()
        {
            if (DCTcpClient == null || !DCTcpClient.IsConnected) return;
            if (npFlowChart10.WorkFlow == null)
                npFlowChart10.TaskReset();
            //CloseAlarmFrm();
            npFlowChart10.TaskRun();
            if (null == LeftGlue)
            {
                MountingProject project = model.ParentStation as MountingProject;
                if (null != project)
                {
                    LeftGlue = project.LeftGlue;
                }
            }
        }
        public override void ResetChart()
        {
            StartChart = npFlowChart10;
            npFlowChart10.TaskReset();
        }

        public MountingMode DataModel
        {
            get { return model; }
            set { model = value; OnPropertyChanged(); }
        }
        private MountingMode model = new MountingMode();

        /// <summary>
        /// 权限管理是否锁界面参数
        /// </summary>
        public bool IsLockParam
        {
            get { return isLockParam; }
            set { isLockParam = value; SetNudControlLock(value); OnPropertyChanged(); }
        }
        private bool isLockParam = false;
        /// <summary>
        /// 测高传感器TCP对象
        /// </summary>
        /// <remarks>
        /// TCP对象，到最外层直接进行绑定
        /// </remarks>
        [XmlIgnore]
        public SimpleTcpClient SensorClient
        {
            get { return _sensorClient; }
            set
            {
                if (_sensorClient != null && _sensorClient.Events != null)
                {
                    _sensorClient.Events.DataReceived -= sensorDataReceived;
                    _sensorClient.Events.Disconnected -= Events_Disconnected;

                }

                _sensorClient = value;
                if (value != null && value.Events != null)
                {
                    value.Events.DataReceived += sensorDataReceived;
                    value.Events.Disconnected += Events_Disconnected;
                }
                OnPropertyChanged();
            }
        }
        private SimpleTcpClient _sensorClient;

        /// <summary>
        /// 光阑测量高度列表
        /// </summary>
        /// <remarks>
        /// 位移传感器测量出来的值
        /// </remarks>
        [Browsable(false)]
        public List<double> HeightData
        {
            get { return heightData; }
            private set { heightData = value; OnPropertyChanged(); }
        }
        List<double> heightData = new List<double>();

        /// <summary>
        /// 光阑和PCB测量相对高度列表
        /// </summary>
        /// <remarks>
        /// 贴装完后复检光阑和PCB的段差
        /// </remarks>
        [Browsable(false)]
        public List<float> LeftCheckHeightData
        {
            get { return leftCheckHeightData; }
            private set { leftCheckHeightData = value; OnPropertyChanged(); }
        }
        List<float> leftCheckHeightData = new List<float>();

        /// <summary>
        /// 视觉NG调整循环次数
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        [Browsable(false)]
        public int VisNgCount
        {
            get { return visNgCount; }
            private set { visNgCount = value; OnPropertyChanged(); }
        }
        int visNgCount = 0;

        /// <summary>
        /// 光阑平面测高次数
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        [Browsable(false)]
        public int GetHeightTime
        {
            get { return getHeightTime; }
            private set { getHeightTime = value; OnPropertyChanged(); }
        }
        int getHeightTime = 3;


        /// <summary>
        /// 光阑与PCB相对高度测高次数
        /// </summary>
        /// <remarks>
        /// 贴装完后复检光阑和PCB的段差次数
        /// </remarks>
        [Browsable(false)]
        public int LeftCheckHeightTime
        {
            get { return leftCheckHeightTime; }
            private set { leftCheckHeightTime = value; OnPropertyChanged(); }
        }
        int leftCheckHeightTime = 8;


        /// <summary>
        /// 微调组装旋转角度
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        [Browsable(false)]
        public double PcbAngleOffset
        {
            get { return pcbAngleOffset; }
            private set { pcbAngleOffset = value; OnPropertyChanged(); }
        }
        double pcbAngleOffset = 0;

        // <summary>
        /// 微调组装旋转X
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        [Browsable(false)]
        public double PcbOffsetX
        {
            get { return pcbOffsetX; }
            private set { pcbOffsetX = value; OnPropertyChanged(); }
        }
        double pcbOffsetX = 0;

        // <summary>
        /// 微调组装旋转Y
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        [Browsable(false)]
        public double PcbOffsetY
        {
            get { return pcbOffsetY; }
            private set { pcbOffsetY = value; OnPropertyChanged(); }
        }
        double pcbOffsetY = 0;

        // <summary>
        /// 光阑高度检测平面度公差
        /// </summary>
        /// <remarks>
        /// 
        /// </remarks>
        [Browsable(false)]
        public double HeightSpc
        {
            get { return heightSpc; }
            private set { heightSpc = value; OnPropertyChanged(); }
        }
        double heightSpc = 0;
        #endregion

        #region 私有属性
        JTimer AutoRunJTime = new JTimer();
        frmConfirm confirmFrm = new frmConfirm();
        List<double> pcbaDataList = new List<double>();
        List<double> pieceDataList = new List<double>();
        List<double> pcbaSecDataList = new List<double>();
        List<double> pcbaRechkDataList = new List<double>();
        private MountingMode mountingModule;
        private GlueModule glueModule;
        private PositionInfor PosCom;
        private string assembModel = string.Empty; //当前型号
        Task AssembModuleTask;
        int AssembModuleTaskRtn;
        Task Y3MotorTask;
        int Y3MotorTaskRtn;
        bool uvLightStatus = false;
        bool dischargeRes = false;
        private GlueProject LeftGlue = null;
        private List<CommunicateDgvData> Dispense1MoveReadDgvDatas = new List<CommunicateDgvData>();
        private List<CommunicateDgvData> Dispense1MoveWriteDgvDatas = new List<CommunicateDgvData>();
        private List<CommunicateDgvData> Dispense1FlowReadDgvDatas = new List<CommunicateDgvData>();
        private List<CommunicateDgvData> Dispense1FlowWriteDgvDatas = new List<CommunicateDgvData>();
        string basePathForceImage = @"D:\WorkLog\ForceImage\";
        string ImagePath = string.Empty;
        /// <summary>
        /// 传感器TCP客户端接受到的最新信息
        /// </summary>
        /// <remarks>
        /// 获取之后马上清空信息
        /// </remarks>
        [XmlIgnore]
        private string SensorLastContent
        {
            get
            {
                string ret = sensorLastContent;
                sensorLastContent = string.Empty;
                return ret;
            }
            set { sensorLastContent = value; OnPropertyChanged(); }
        }
        private string sensorLastContent = string.Empty;

        //报警窗体
        public WinShowAlarm AlarmFrm
        {
            get { return alarmFrm; }
            set { alarmFrm = value; OnPropertyChanged(); }
        }
        private WinShowAlarm alarmFrm = null;

        /// <summary>
        /// uv灯
        /// </summary>
        private readonly UVLight_ModbusTcp UVTcp = new UVLight_ModbusTcp();

        #endregion

        #region 私有函数
        /// <summary>
        /// 算法客户端接受信息事件
        /// </summary>
        /// <remarks>
        /// 可以重新该事件，在里面做自己想要的事件处理
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sensorDataReceived(object sender, SuperSimpleTcp.DataReceivedEventArgs e)
        {
            if (e.Data == null) return;
            string strRec = Encoding.UTF8.GetString(e.Data.Array);
            Debug.WriteLine($"Port={e.IpPort}+Data={strRec}");
            SensorLastContent = strRec;
        }

        private void Events_Disconnected(object sender, SuperSimpleTcp.ConnectionEventArgs e)
        {
            Debug.WriteLine($"Port={e.IpPort} Disconnected Reason={e.Reason}");
        }
        /// <summary>
        /// 移动单Y3轴，光阑触发需要单独停止此轴
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="pos"></param>
        /// <param name="vel"></param>
        /// <param name="acc"></param>
        /// <param name="isStop"></param>
        /// <returns></returns>
        private bool MoveSingleAxis(G4.Motion.EnumAxisType axis, double pos, double vel, double acc)
        {
            double curPos = 0;
            SmartGlue.Infrastructure.AxisStatus status = new SmartGlue.Infrastructure.AxisStatus();
            G4.Motion.MotionModel.Instance.GetPosition(axis, out curPos);
            G4.Motion.MotionModel.Instance.GetAxisStatus(axis, ref status);
            if (!status.Moving && curPos != pos)
            {
                Y3MotorTask = Task.Run(() =>
                {
                    var rtn = G4.Motion.MotionModel.Instance.MoveTo(axis, pos, vel, acc);
                    Y3MotorTaskRtn = rtn;
                });
                return false;
            }
            else
            {
                if (null != Y3MotorTask && Y3MotorTask.IsCompleted && curPos == pos)
                {
                    if (Y3MotorTaskRtn == 0)
                    {
                        AutoRunJTime.Restart();
                        return true;
                    }
                    else
                    {
                        Log.log.Write("组装模组执行Move失败", Color.Red);
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        private int MoveTo(double X, double Y, double Z, double vel, double acc, bool IsSafety = true)
        {
            double safetyPos = 0;
            if (IsSafety)
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z1, safetyPos, vel, acc);
                if (rtn == 0)
                {
                    var rtnXY = MoveToDoubleAxis(G4.Motion.EnumAxisType.X1, X, G4.Motion.EnumAxisType.Y1, Y, vel, acc, vel, acc);
                    if (0 == rtnXY)
                    {
                        return G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z1, Z, vel, acc);
                    }
                    else
                        return -1;
                }
                else
                    return -1;
            }
            else
            {
                var rtn = MoveToDoubleAxis(G4.Motion.EnumAxisType.X1, X, G4.Motion.EnumAxisType.Y1, Y, vel, acc, vel, acc);
                if (0 == rtn)
                {
                    return G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z1, Z, vel, acc);
                }
                else
                    return -1;

            }
        }

        private void RefreashDifferentThreadUI(Control control, Action action)
        {
            if (control.InvokeRequired)
            {
                Action refreshUI = new Action(action);
                control.Invoke(refreshUI);
            }
            else
            {
                action.Invoke();
            }
        }
        public static bool DelayMs(int delayMilliseconds)
        {
            DateTime now = DateTime.Now;
            Double s;
            do
            {
                TimeSpan spand = DateTime.Now - now;
                s = spand.TotalMilliseconds + spand.Seconds * 1000;
                Application.DoEvents();
            }
            while (s < delayMilliseconds);
            return true;
        }
        /// <summary>
        /// 显示报警窗口
        /// </summary>
        /// <param name="code"></param>
        private void ShowAlarmInfo(int code)
        {
            AlarmFrm.Alarm.Enqueue(code);
            //GloMotionParame.SiemenPLC.Write(GetAddressFormDgv("OB_" + "RXAssemblyTask", Dispense1MoveWriteDgvDatas), true);
            this.Stop();
        }

        /// <summary>
        /// 关闭报警窗口
        /// </summary>
        private void CloseAlarmFrm()
        {
            AlarmFrm.CloseAlarmFrm();
        }

        private void SetNudControlLock(bool isLock)
        {
            foreach (Control ctl in this.Controls)
            {
                if (ctl is NumericUpDown || ctl is NPFlowChart)
                {
                    ctl.Enabled = !isLock;
                }
            }
        }
        #region 获取dgv数据
        public void InitDgv_CommunicateSetting(VUDatagridView dataGridView, ref List<CommunicateDgvData> communicateDgvDatas)
        {
            communicateDgvDatas.Clear();
            CommunicateDgvData dgvData = new CommunicateDgvData();
            for (int i = 0; i < dataGridView.myDgv.Rows.Count; i++)
            {
                dgvData.name = dataGridView.GetDataArrayByIndex(i)[2];
                dgvData.data = dataGridView.GetDataArrayByIndex(i)[1];
                communicateDgvDatas.Add(dgvData);
            }
        }

        public string GetAddressFormDgv(string name, List<CommunicateDgvData> communicateDgvDatas)
        {
            if (communicateDgvDatas != null && communicateDgvDatas.Count > 0)
            {
                for (int i = 0; i < communicateDgvDatas.Count; i++)
                {
                    if (communicateDgvDatas[i].name == name)
                        return communicateDgvDatas[i].data;
                }
                return "999";
            }
            else { return "999"; }
        }
        #endregion
        #region 轴移动
        public int MoveToHomeXYR()
        {
            var XYRRet = Task.WhenAll(Task.Run(() => { return G4.Motion.MotionModel.Instance.AxisMoveToHome(G4.Motion.EnumAxisType.R1); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.AxisMoveToHome(G4.Motion.EnumAxisType.R2); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.AxisMoveToHome(G4.Motion.EnumAxisType.R3); }));
            XYRRet.Wait();
            var result = XYRRet.Result;
            if (result.All(p => p == 0))
            {
                return 0;
            }
            else { return 1; }
        }

        public int MoveToDoubleAxis(G4.Motion.EnumAxisType axis1, double pos1, G4.Motion.EnumAxisType axis2, double pos2, double vel1, double acc1, double vel2, double acc2)
        {
            var XYRRet = Task.WhenAll(Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(axis1, pos1, vel1, acc1); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(axis2, pos2, vel2, acc2); }));
            XYRRet.Wait();
            var result = XYRRet.Result;
            if (result.All(p => p == 0))
            {
                return 0;
            }
            else { return 1; }
        }

        public int MoveToThrAxis(G4.Motion.EnumAxisType axis1, double pos1, G4.Motion.EnumAxisType axis2, double pos2, G4.Motion.EnumAxisType axis3, double pos3,
            double vel1, double acc1, double vel2, double acc2, double vel3, double acc3)
        {
            var XYRRet = Task.WhenAll(Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(axis1, pos1, vel1, acc1); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(axis2, pos2, vel2, acc2); })
                  , Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(axis3, pos3, vel3, acc3); }));
            XYRRet.Wait();
            var result = XYRRet.Result;
            if (result.All(p => p == 0))
            {
                return 0;
            }
            else { return 1; }
        }

        public int MoveXYRPlatform(G4.Motion.EnumAxisType R1, G4.Motion.EnumAxisType R2, G4.Motion.EnumAxisType R3, double XYR_XPos, double XYR_YPos, double XYR_RPos, double vel, double acc)
        {
            var XYRet = Task.WhenAll(Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(R1, XYR_XPos, vel, acc); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(R2, XYR_XPos, vel, acc); })
                  , Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(R3, XYR_YPos, vel, acc); }));
            var resultXY = XYRet.Result;

            var RRet = Task.WhenAll(Task.Run(() => { return XXYRotateTo(XYR_RPos, vel, acc); }));
            var resultR = RRet.Result;
            if (resultXY.All(p => p == 0) && resultR.All(p => p == 0))
            {
                return 0;
            }
            else { return 1; }
        }

        public int XXYRotateTo(double deg, double vel, double acc)
        {
            const double R = 91.924;
            const double Y = 225;
            const double X1 = 315;
            const double X2 = 135;

            double dx1 = R * Math.Cos((X1 + deg) * Math.PI / 180) - R * Math.Cos(X1 * Math.PI / 180);
            double dx2 = R * Math.Cos((X2 + deg) * Math.PI / 180) - R * Math.Cos(X2 * Math.PI / 180);
            double dy = R * Math.Sin((Y + deg) * Math.PI / 180) - R * Math.Sin(Y * Math.PI / 180);

            var XYRet = Task.WhenAll(Task.Run(() => { return G4.Motion.MotionModel.Instance.Move(G4.Motion.EnumAxisType.R1, dx1, vel, acc); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.Move(G4.Motion.EnumAxisType.R2, dx2, vel, acc); })
                  , Task.Run(() => { return G4.Motion.MotionModel.Instance.Move(G4.Motion.EnumAxisType.R3, dy, vel, acc); }));
            var result = XYRet.Result;
            if (result.All(p => p == 0))
            {
                return 0;
            }
            else { return 1; }
        }
        #endregion

        private bool GetSensorData(ref double height)
        {
            byte[] dataRev = new byte[1024];
            if (null == SensorClient || !SensorClient.IsConnected)
            {
                return false;
            }
            SensorClient.Send("M0\r\n");
            string mLocationBuffer = string.Empty;
            int iretryCount = 0;
            while (string.IsNullOrEmpty(mLocationBuffer) && iretryCount < 10)
            {
                iretryCount++;
                Thread.Sleep(100);
                mLocationBuffer = SensorLastContent;
            }
            if (string.IsNullOrEmpty(mLocationBuffer))
            {
                return false;
            }
            var arr = mLocationBuffer.Split(',');
            if (arr == null || arr.Length < 2)
            {
                return false;
            }
            if (double.TryParse(arr[1], out double val))
            {
                height = val * 0.001;
                return true;
            }
            return false;
        }

        private List<double> get_xyr_from_string(string input_string)
        {
            List<double> arrResult = new List<double>();
            try
            {
                string[] strArr;
                string[] strArrPath;
                strArrPath = input_string.Split(';');
                if (strArrPath.Length > 1)
                    ImagePath = strArrPath[1];
                strArr = strArrPath[0].Split(',');
                foreach (string str in strArr)
                {
                    arrResult.Add(double.Parse(str));
                }
                return arrResult;
            }
            catch (Exception ex)
            {
                arrResult.Add(1.1);
                arrResult.Add(1.1);
                return arrResult;
            }

        }

        private string[] GetDataByAnnotation(VUControl.VUDatagridView dgv, string annotation)
        {
            string[] GetDataInfo = dgv.GetDataArrayByFlagName(1, annotation);
            return GetDataInfo;
        }

        /// <summary>
        /// 写入PLC并检测返回成功
        /// </summary>
        /// <param name="addr"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private bool WritePlcIsSuccess(string addr, object value)
        {
            Type type = value.GetType();
            if (type == typeof(bool))
            {
                GloMotionParame.SiemenPLC.Write(addr, (bool)value);
                DelayMs(10);
                if ((bool)value == (bool)GloMotionParame.SiemenPLC.Read(addr)) return true;
                else return false;
            }
            else if (type == typeof(string))
            {
                GloMotionParame.SiemenPLC.Write(addr, (string)value);
                DelayMs(50);
                if ((string)value == (string)GloMotionParame.SiemenPLC.Read(addr)) return true;
                else return false;
            }
            else if (type == typeof(float))
            {
                GloMotionParame.SiemenPLC.Write(addr, (float)value);
                DelayMs(50);
                if ((float)value == (float)GloMotionParame.SiemenPLC.Read(addr)) return true;
                else return false;
            }
            else if (type == typeof(ushort))
            {
                GloMotionParame.SiemenPLC.Write(addr, (ushort)value);
                DelayMs(50);
                if ((ushort)value == (ushort)GloMotionParame.SiemenPLC.Read(addr)) return true;
                else return false;
            }
            return true;
        }
        #endregion



        #region 贴装流程

        #endregion

        private YHSDK.FCResultType npFlowChart9_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }
        bool ret1 = false;
        bool ret2 = false;
        bool ret3 = false;
        bool ret4 = false;
        bool ret5 = false;
        private YHSDK.FCResultType npFlowChart10_FlowRun(object sender, EventArgs e)
        {
            bool bvalue = false;
            bool bvalue1 = false;
            if (null == model.MountingCamPointsOrder || model.MountingCamPointsOrder.Count == 0)
            {
                PrintLog($"Mark位置和拍照指令数量位空", Color.Red);
                return FCResultType.IDLE;
            }
            if (string.IsNullOrEmpty(model.Signal_R_RXProgram))
            {
                PrintLog($"M启动指令位置为空", Color.Red);
                return FCResultType.IDLE;
            }
            bool bret = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("IB_RXDischarge", uc_MountingFlow.Dispense1MoveReadDgvDatas), out bvalue);
            //Log.log.Write($"RX可出料任务信号为{bvalue}", Color.Red);
            if (bret && bvalue)
            {
                //ret1 = WritePlcIsSuccess(GetAddressFormDgv("OB_" + "RXDischargeTask", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true);
                ret2 = WritePlcIsSuccess(GetAddressFormDgv("OB_RXDischargeTaskOK", uc_MountingFlow.Dispense1MoveWriteDgvDatas), false);
                ret3 = WritePlcIsSuccess(GetAddressFormDgv("OB_RXDischargeTaskNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), false);
                ret4 = WritePlcIsSuccess(GetAddressFormDgv("OB_RXDischargeTaskSendData", uc_MountingFlow.Dispense1MoveWriteDgvDatas), false);
                ret5 = WritePlcIsSuccess(GetAddressFormDgv("OB_StandBy", uc_MountingFlow.Dispense1MoveWriteDgvDatas), false);

                if (/*ret1 && */ret2 && ret3 && ret4 && ret5)
                {
                    if (WritePlcIsSuccess(GetAddressFormDgv("OB_" + "RXDischargeTask", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true))
                    {
                        PrintLog(string.Format($"接收到RX出料任务指令，贴装模组开始工作"), Color.Black);
                        LeftCheckHeightData.Clear();
                        assembModel = "RX_";
                        dischargeRes = false;
                        
                        bool bret11 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("OB_RXAssemblyNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), out bool bvalueRX);
                        bool bret2 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("OB_TXAssemblyNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), out bool bvalueTX);
                        bool bret3 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("OB_RXDispenseTaskNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), out bool bvalueRXGlueRet);
                        bool bret4 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("OB_TXDispenseTaskNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), out bool bvalueTXGlueRet);
                        if ((bret11 && bvalueRX) || (bret3 && bvalueRXGlueRet) || (bret2 && bvalueTX) || (bret4 && bvalueTXGlueRet))
                        {
                            Log.log.Write("贴装或点胶流程NG，执行直接出料流程", Color.Red);
                            return FCResultType.NEXT;
                        }
                        else
                        {
                            Log.log.Write("贴装和点胶流程OK，执行直接复检流程", Color.Green);
                            return FCResultType.CASE1;
                        }
                    }
                    else
                    {
                        return FCResultType.IDLE;
                    }
                }
                return FCResultType.IDLE;
            }
            bool bret1 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("IB_TXDischarge", uc_MountingFlow.Dispense1MoveReadDgvDatas), out bvalue1);
            //Log.log.Write($"TX可出料任务信号为{bvalue1}", Color.Red);
            if (bret1 && bvalue1)
            {
                //ret1 = WritePlcIsSuccess(GetAddressFormDgv("OB_" + "TXDischargeTask", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true);
                ret2 = WritePlcIsSuccess(GetAddressFormDgv("OB_TXDischargeTaskOK", uc_MountingFlow.Dispense1MoveWriteDgvDatas), false);
                ret3 = WritePlcIsSuccess(GetAddressFormDgv("OB_TXDischargeTaskNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), false);
                ret4 = WritePlcIsSuccess(GetAddressFormDgv("OB_TXDischargeTaskSendData", uc_MountingFlow.Dispense1MoveWriteDgvDatas), false);
                ret5 = WritePlcIsSuccess(GetAddressFormDgv("OB_StandBy", uc_MountingFlow.Dispense1MoveWriteDgvDatas), false);

                if (/*ret1 && */ret2 && ret3 && ret4 && ret5)
                {
                    if (WritePlcIsSuccess(GetAddressFormDgv("OB_" + "TXDischargeTask", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true))
                    {
                        PrintLog(string.Format($"接收到TX出料任务指令，贴装模组开始工作"), Color.Black);
                        LeftCheckHeightData.Clear();
                        assembModel = "TX_";
                        dischargeRes = false;
                        
                        bool bret11 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("OB_RXAssemblyNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), out bool bvalueRX);
                        bool bret2 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("OB_TXAssemblyNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), out bool bvalueTX);
                        bool bret3 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("OB_RXDispenseTaskNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), out bool bvalueRXGlueRet);
                        bool bret4 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("OB_TXDispenseTaskNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), out bool bvalueTXGlueRet);
                        if ((bret11 && bvalueRX) || (bret3 && bvalueRXGlueRet) || (bret2 && bvalueTX) || (bret4 && bvalueTXGlueRet))
                        {
                            Log.log.Write("贴装或点胶流程NG，执行直接出料流程", Color.Red);
                            return FCResultType.NEXT;
                        }
                        else
                        {
                            Log.log.Write("贴装和点胶流程OK，执行直接复检流程", Color.Green);
                            return FCResultType.CASE1;
                        }
                    }
                    else
                    {
                        return FCResultType.IDLE;
                    }
                }
                else { return FCResultType.IDLE; }
            }
            return FCResultType.IDLE;
        }

        private FCResultType npFlowChart68_FlowRun(object sender, EventArgs e)
        {
            if (model.IsUseSoftLand)
            {
                Helper.ForceControl.StopSoftLand();
                model.ForceSaveImagePath = basePathForceImage + model.MountingResult + "_" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss-ms") + ".png";
            }
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("Z4OrignalPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z4, PosCom.ZPos, model.Z4Speed, model.Z4AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart67_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                    //bool bret1 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("OB_RXAssemblyNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), out bool bvalueRX);
                    //bool bret2 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("OB_TXAssemblyNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), out bool bvalueTX);
                    //bool bret3 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("OB_RXDispenseTaskNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), out bool bvalueRXGlueRet);
                    //bool bret4 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("OB_TXDispenseTaskNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), out bool bvalueTXGlueRet);
                    //if ((bret1 && bvalueRX) || (bret3 && bvalueRXGlueRet) || (bret2 && bvalueTX) || (bret4 && bvalueTXGlueRet))
                    //{
                    //    Log.log.Write("贴装或点胶流程NG，执行直接出料流程", Color.Red);
                    //    return FCResultType.NEXT;
                    //}
                    //else
                    //{
                    //    Log.log.Write("贴装和点胶流程OK，执行直接复检流程", Color.Red);
                    //    return FCResultType.CASE1;
                    //}
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart70_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MircPlatformCamPointsOrder.GetItemByName("MicoPlatformOrignalPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = MoveXYRPlatform(G4.Motion.EnumAxisType.R1, G4.Motion.EnumAxisType.R2, G4.Motion.EnumAxisType.R3, PosCom.XPos/* + pcbOffsetX*/, PosCom.YPos/* + pcbOffsetY*/, PosCom.ZPos/* + pcbAngleOffset*/,
                    model.R1Speed, model.R1AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart69_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    bool bret1 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("OB_RXAssemblyNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), out bool bvalueRX);
                    bool bret2 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("OB_TXAssemblyNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), out bool bvalueTX);
                    bool bret3 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("OB_RXDispenseTaskNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), out bool bvalueRXGlueRet);
                    bool bret4 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("OB_TXDispenseTaskNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), out bool bvalueTXGlueRet);
                    if ((bret1 && bvalueRX) || (bret3 && bvalueRXGlueRet))
                    {
                        Log.log.Write("RX贴装流程NG，直接排出", Color.Red);
                        model.MountingResult.ReckeckHeight = new List<float> { 0, 0, 0, 0 };
                        return FCResultType.CASE2;
                    }
                    else if ((bret2 && bvalueTX) || (bret4 && bvalueTXGlueRet))
                    {
                        Log.log.Write("TX贴装流程NG，直接排出", Color.Red);
                        model.MountingResult.ReckeckHeight = new List<float> { 0, 0, 0, 0 };
                        return FCResultType.CASE3;
                    }
                    else
                        return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart238_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("SecUVLightPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            //AssembModuleTask = Task.Run(() =>
            //{
            //    var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Y3, PosCom.XPos, model.Y3Speed, model.Y3AccSpeed);
            //    AssembModuleTaskRtn = rtn;
            //});
            //AutoRunJTime.Restart();
            //return FCResultType.NEXT;
            if (MoveSingleAxis(G4.Motion.EnumAxisType.Y3, PosCom.XPos, model.Y3Speed, model.Y3AccSpeed))
            {
                return FCResultType.NEXT;
            }
            else
            {
                DelayMs(100);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart237_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart235_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            int Index = heightData.Count;
            PosCom = model.PublicCamPointsOrder.GetItemByName("GlueSecUVLightPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = MoveTo(PosCom.XPos, PosCom.YPos, PosCom.ZPos, LeftGlue.GlueExcuteModel.GlueXYAxisSpd, LeftGlue.GlueExcuteModel.GlueXYAxisAcc);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart236_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart233_FlowRun(object sender, EventArgs e)
        {
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_UVLightDown", true);
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_UVLightUp", false);
            if (!Googo.Manul.GloMotionParame.InputList.GetValue("IB_UVLightUp") && Googo.Manul.GloMotionParame.InputList.GetValue("IB_UVLightDown"))
            {
                AutoRunJTime.Restart();
                return FCResultType.NEXT;
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    Log.log.Write("UV灯气缸下降超时", Color.Red);
                    AutoRunJTime.Restart();
                    return FCResultType.IDLE;
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart234_FlowRun(object sender, EventArgs e)
        {
            if (AutoRunJTime.On((int)(DataModel.UVCuringTime * 1000) - 1000))
            {
                if (UVTcp.Connected && UVTcp != null && uvLightStatus)
                {
                    //读通道1 UV灯功率
                    int nReadPower = UVTcp.UVLight_GetOpticalPower(1);
                    Log.log.Write($"UV灯功率:{ nReadPower}", Color.Black);
                    //读通道1 UV灯强度
                    int nReadStrength = UVTcp.UVLight_GetStrength(1);
                    Log.log.Write($"UV灯强度:{ nReadPower}", Color.Black);
                    //读通道1 UV灯温度（光头温度）
                    int nReadTemperature = UVTcp.UVLight_GetTemperature(1);
                    Log.log.Write($"光头温度:{ nReadTemperature}", Color.Black);
                    //读取通道1光照时间
                    model.MountingResult.SecUVTime = UVTcp.UVLight_GetTime(1);
                    Log.log.Write($"UV灯光照时间:{model.MountingResult.SecUVTime}", Color.Black);
                    model.MountingResult.SecUVPower = (float)nReadStrength;
                    //关闭通道1 UV灯
                    //UVTcp.UVLight_TurnOff(1);
                    DataModel.UVCurUseTime = DataModel.UVCuringTime;
                    DataModel.UVCuringLightPower = (float)nReadPower;
                    uvLightStatus = false;
                    return FCResultType.IDLE;
                }
                if (AutoRunJTime.On((int)(DataModel.UVCuringTime * 1000) + 2000))
                {
                    if (UVTcp.UVLight_GetUVState(1) == 1)
                    {
                        Log.log.Write("UV灯打开时间超限，请检查", Color.Red);
                        ShowAlarmInfo(1066);
                        return FCResultType.IDLE;
                    }
                    else
                    {
                        AutoRunJTime.Restart();
                        return FCResultType.NEXT;
                    }
                }
                else
                {
                    Log.log.Write("UV灯未连接成功", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (UVTcp.Connected && !uvLightStatus)
                {
                    //设置光照时间
                    UVTcp.UVLight_SetTime(1, (double)DataModel.UVCuringTime);
                    //设置功率10%-100%
                    UVTcp.UVLight_SetPowerLevel(1, 40);
                    //设置功率范围
                    UVTcp.UVLight_SetOpticalPowerRange(1, 0, 20000);
                    //打开通道1 UV灯
                    uvLightStatus = UVTcp.UVLight_TurnOn(1);
                    return FCResultType.IDLE;
                }
                else
                {
                    return FCResultType.IDLE;
                }
            }
        }

        private FCResultType npFlowChart1_FlowRun(object sender, EventArgs e)
        {
            if (!UVTcp.Connected)
                UVTcp.Connect(LeftGlue.StrUVIP, LeftGlue.UVPortNum);
            Thread.Sleep(400);
            if (UVTcp.Connected)
            {
                PrintLog("UV灯TCP连接成功", Color.Black);

                AutoRunJTime.Restart();
                uvLightStatus = false;
                return FCResultType.NEXT;
            }
            else
            {
                PrintLog("UV灯TCP连接失败", Color.Red);
                return FCResultType.CASE1;
            }
        }

        private FCResultType npFlowChart57_FlowRun(object sender, EventArgs e)
        {
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_UVLightDown", false);
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_UVLightUp", true);
            if (Googo.Manul.GloMotionParame.InputList.GetValue("IB_UVLightUp") && !Googo.Manul.GloMotionParame.InputList.GetValue("IB_UVLightDown"))
            {
                AutoRunJTime.Restart();
                return FCResultType.NEXT;
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    Log.log.Write("UV灯气缸上升超时", Color.Red);
                    AutoRunJTime.Restart();
                    return FCResultType.IDLE;
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart226_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("RecheckHeightPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            //AssembModuleTask = Task.Run(() =>
            //{
            //    var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Y3, PosCom.XPos, model.Y3Speed, model.Y3AccSpeed);
            //    AssembModuleTaskRtn = rtn;
            //});
            //AutoRunJTime.Restart();
            //return FCResultType.NEXT;
            if (MoveSingleAxis(G4.Motion.EnumAxisType.Y3, PosCom.XPos, model.Y3Speed, model.Y3AccSpeed))
            {
                return FCResultType.NEXT;
            }
            else
            {
                DelayMs(100);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart225_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                    //if (WritePlcIsSuccess(GetAddressFormDgv("OB_StatusNo", uc_MountingFlow.Dispense1FlowWriteDgvDatas), Convert.ToUInt16(txb_DischargeReport2.Value)))        //写入状态标志位决定报工状态
                    //{
                    //    AutoRunJTime.Restart();
                    //    return FCResultType.NEXT;
                    //}
                    //else
                    //{
                    //    return FCResultType.IDLE;
                    //}
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart228_FlowRun(object sender, EventArgs e)
        {
            string Index = (LeftCheckHeightData.Count + 1).ToString();
            if (model.PublicCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.PublicCamPointsOrder.GetItemByName("CheckHeightPos" + Index);
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = MoveTo(PosCom.XPos, PosCom.YPos, PosCom.ZPos, LeftGlue.GlueExcuteModel.GlueXYAxisSpd, LeftGlue.GlueExcuteModel.GlueXYAxisAcc);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart229_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart230_FlowRun(object sender, EventArgs e)
        {
            double height = 0;
            if (GetSensorData(ref height))
            {
                LeftCheckHeightData.Add((float)height);
                AutoRunJTime.Restart();
                return FCResultType.NEXT;
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 测高数据获取超时", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart227_FlowRun(object sender, EventArgs e)
        {
            if (LeftCheckHeightData.Count >= LeftCheckHeightTime)
            {
                int index = 0;
                List<float> temp = new List<float>();
                float hSpec = 0;
                float lSpec = 0;
                hSpec = DataModel.ReckeckHeightSpec[1];
                lSpec = DataModel.ReckeckHeightSpec[0];
                for (int i = 0; i < LeftCheckHeightData.Count; i++)
                {
                    index++;
                    if (index == 2)
                    {
                        temp.Add((float)Math.Round(LeftCheckHeightData[i - 1] - LeftCheckHeightData[i], 3));
                        if ((LeftCheckHeightData[i - 1] - LeftCheckHeightData[i]) > hSpec || (LeftCheckHeightData[i - 1] - LeftCheckHeightData[i]) < lSpec)
                        {
                            Log.log.Write($"当前光阑平面与PCB平面高度差：{(LeftCheckHeightData[i - 1] - LeftCheckHeightData[i])}，结果：NG", System.Drawing.Color.Red);
                        }
                        else { Log.log.Write($"当前光阑平面与PCB平面高度差：{(LeftCheckHeightData[i - 1] - LeftCheckHeightData[i])}，结果：OK", System.Drawing.Color.Green); }
                        index = 0;
                    }
                }
                //if (assembModel.Contains("RX"))
                //    model.PiecePcbHeightList = temp;
                //else
                //    model.PieceHousingHeightList = temp;
                model.MountingResult.ReckeckHeight = temp;
                model.ReckeckHeight = temp;
                return FCResultType.NEXT;
            }
            else
            {
                return FCResultType.CASE1;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            model.MountingResult.ProductSN = "1";
            PositionInfor p = new PositionInfor();
            p.XPos = -1.3;
            p.YPos = -1;
            p.ZPos = -4;
            model.MountingResult.SecLocationXY = p;
            model.MountingResult.ReckeckHeight = new List<float> { -1.1f, 2.2f, 3.3f };
            System.Windows.Application.Current.Dispatcher.Invoke(() => { model.MountingResults.Add(model.MountingResult); });
            string path = @"D:\WorkLog\Mounting";
            Helper.FileOperate.GetResult(model.MountingResult, path);
        }

        private FCResultType npFlowChart232_FlowRun(object sender, EventArgs e)
        {
            if (model.PublicCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            int Index = heightData.Count;
            PosCom = model.PublicCamPointsOrder.GetItemByName("GetHeightCompeletedPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = MoveTo(PosCom.XPos, PosCom.YPos, PosCom.ZPos, LeftGlue.GlueExcuteModel.GlueXYAxisSpd, LeftGlue.GlueExcuteModel.GlueXYAxisAcc);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart231_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    if (assembModel.Contains("TX"))
                    {
                        return FCResultType.CASE1;
                    }
                    else
                        return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart72_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("PutCoverPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            //AssembModuleTask = Task.Run(() =>
            //{
            //    var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Y3, PosCom.XPos, model.Y3Speed, model.Y3AccSpeed);
            //    AssembModuleTaskRtn = rtn;
            //});
            //AutoRunJTime.Restart();
            //return FCResultType.NEXT;
            if (MoveSingleAxis(G4.Motion.EnumAxisType.Y3, PosCom.XPos, model.Y3Speed, model.Y3AccSpeed))
            {
                return FCResultType.NEXT;
            }
            else
            {
                DelayMs(100);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart71_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart75_FlowRun(object sender, EventArgs e)
        {
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_RXCoverDown", true);
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_RXCoverUp", false);
            if (Googo.Manul.GloMotionParame.InputList.GetValue("IB_RXCoverDown") && !Googo.Manul.GloMotionParame.InputList.GetValue("IB_RXCoverUp"))
            {
                AutoRunJTime.Restart();
                return FCResultType.NEXT;
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    Log.log.Write("盖板气缸下降超时", Color.Red);
                    AutoRunJTime.Restart();
                    ShowAlarmInfo(1023);
                    return FCResultType.IDLE;
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart74_FlowRun(object sender, EventArgs e)
        {
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_RXCoverOff", false);
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_RXCoverOn", true);
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
            if (Googo.Manul.GloMotionParame.InputList.GetValue("IB_RXCoverInplace"))
            {
                AutoRunJTime.Restart();
                return FCResultType.NEXT;
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    Log.log.Write("盖板松开超时", Color.Red);
                    AutoRunJTime.Restart();
                    return FCResultType.IDLE;
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart73_FlowRun(object sender, EventArgs e)
        {
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_RXCoverDown", false);
            Googo.Manul.GloMotionParame.OutputList.SetValue("OB_RXCoverUp", true);
            if (!Googo.Manul.GloMotionParame.InputList.GetValue("IB_RXCoverDown") && Googo.Manul.GloMotionParame.InputList.GetValue("IB_RXCoverUp"))
            {
                AutoRunJTime.Restart();
                return FCResultType.NEXT;
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    Log.log.Write("盖板气缸上升超时", Color.Red);
                    AutoRunJTime.Restart();
                    ShowAlarmInfo(1021);
                    return FCResultType.IDLE;
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart77_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("Y3OrignalPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            //AssembModuleTask = Task.Run(() =>
            //{
            //    var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Y3, PosCom.XPos, model.Y3Speed, model.Y3AccSpeed);
            //    AssembModuleTaskRtn = rtn;
            //});
            //AutoRunJTime.Restart();
            //return FCResultType.NEXT;
            if (MoveSingleAxis(G4.Motion.EnumAxisType.Y3, PosCom.XPos, model.Y3Speed, model.Y3AccSpeed))
            {
                return FCResultType.NEXT;
            }
            else
            {
                DelayMs(100);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart76_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    model.MountingResult.MountingResult = dischargeRes ? "OK" : "NG";
                    System.Windows.Application.Current.Dispatcher.Invoke(() => { model.MountingResults.Add(model.MountingResult); });
                    string path = @"D:\WorkLog\Mounting";
                    Helper.FileOperate.GetResult(model.MountingResult, path);
                    if (assembModel.Contains("RX"))
                    {
                        ret1 = WritePlcIsSuccess(GetAddressFormDgv("OB_RXDischargeTaskOK", uc_MountingFlow.Dispense1MoveWriteDgvDatas), dischargeRes);
                        ret2 = WritePlcIsSuccess(GetAddressFormDgv("OB_RXDischargeTaskNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), !dischargeRes);

                        if (ret1 && ret2/* && ret3 && ret4*/)
                        {
                            if (WritePlcIsSuccess(GetAddressFormDgv("OB_RXDischargeTaskSendData", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true))
                            {
                                WritePlcIsSuccess(GetAddressFormDgv("OB_StandBy", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true);
                                PrintLog(string.Format($"RX出料任务结束"), Color.Black);
                                return FCResultType.NEXT;
                            }
                            else
                            {
                                return FCResultType.IDLE;
                            }
                        }
                        return FCResultType.IDLE;
                    }
                    else
                    {
                        ret1 = WritePlcIsSuccess(GetAddressFormDgv("OB_TXDischargeTaskOK", uc_MountingFlow.Dispense1MoveWriteDgvDatas), dischargeRes);
                        ret2 = WritePlcIsSuccess(GetAddressFormDgv("OB_TXDischargeTaskNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), !dischargeRes);
                        if (ret1 && ret2/* && ret3 && ret4*/)
                        {
                            if (WritePlcIsSuccess(GetAddressFormDgv("OB_TXDischargeTaskSendData", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true))
                            {
                                WritePlcIsSuccess(GetAddressFormDgv("OB_StandBy", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true);
                                PrintLog(string.Format($"TX出料任务结束"), Color.Black);
                                return FCResultType.NEXT;
                            }
                        }
                        return FCResultType.IDLE;
                    }
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart45_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("WaitPos_piece");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z4, PosCom.ZPos, model.Z4Speed, model.Z4AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart44_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart47_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("TakePicture_assmebly");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z3, PosCom.YPos, model.Z3Speed, model.Z3AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart46_FlowRun(object sender, EventArgs e)
        {
            //bool ret = MoveSingleAxis(G4.Motion.EnumAxisType.Y3, PosCom.XPos, model.Y3Speed, model.Y3AccSpeed);
            if ((null == AssembModuleTask || AssembModuleTask.IsCompleted)/* && ret*/)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                DelayMs(100);
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart221_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("RecheckPicture_piece");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z3, PosCom.YPos, model.Z3Speed, model.Z3AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart219_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart220_FlowRun(object sender, EventArgs e)
        {
            //通知德创视觉Location任务
            if (model.MircPlatformCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("RecheckPicture_piece");
            if (PosCom == null || string.IsNullOrEmpty(PosCom.StrCommand))
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            if (null != DCTcpClient && DCTcpClient.IsConnected)
            {
                AutoRunJTime.Restart();
                DCTcpClient.Send(PosCom.StrCommand);
                return FCResultType.NEXT;
                //if (WritePlcIsSuccess(GetAddressFormDgv("OB_StatusNo", uc_MountingFlow.Dispense1FlowWriteDgvDatas), Convert.ToUInt16(txb_DischargeReport1.Value)))        //写入状态标志位决定报工状态
                //    return FCResultType.NEXT;
                //else
                //{
                //    return FCResultType.IDLE;
                //}
            }
            else
            {
                Log.log.Write($"DCCK TCP连接断开", System.Drawing.Color.Red);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart217_FlowRun(object sender, EventArgs e)
        {
            string mLocationBuffer = AlgLastContent;
            if (string.IsNullOrEmpty(mLocationBuffer))
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    Log.log.Write($"接收DCCK数据超时", System.Drawing.Color.Red);
                }
                return FCResultType.IDLE;
            }
            else
            {
                string rtn = mLocationBuffer;
                if (mLocationBuffer.Contains("PieceOK"))
                {
                    return FCResultType.NEXT;
                }
                else
                {
                    if (AutoRunJTime.On(30000))
                    {
                        Log.log.Write("获取光阑拍照返回值失败", Color.Red);
                        AutoRunJTime.Restart();
                        return FCResultType.CASE1;
                    }
                    return FCResultType.IDLE;
                }
            }
        }

        private FCResultType npFlowChart215_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("RecheckPicture_pcb");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z3, PosCom.YPos, model.Z3Speed, model.Z3AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart214_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart56_FlowRun(object sender, EventArgs e)
        {
            //通知德创视觉Location任务
            AlgLastContent = string.Empty;
            if (model.MircPlatformCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("RecheckPicture_pcb");
            if (PosCom == null || string.IsNullOrEmpty(PosCom.StrCommand))
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            if (null != DCTcpClient && DCTcpClient.IsConnected)
            {
                AutoRunJTime.Restart();
                DCTcpClient.Send(PosCom.StrCommand + "," + model.MountingResult.ProductSN);
                return FCResultType.NEXT;
            }
            else
            {
                Log.log.Write($"DCCK TCP连接断开", System.Drawing.Color.Red);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart55_FlowRun(object sender, EventArgs e)
        {
            string mLocationBuffer;
            mLocationBuffer = string.Empty;
            int iretry = 0;
            model.CCDGuidePicture = "D:\\01_ImageSaves\\RecordedImage\\"+DateTime.Now.ToString("yyyy-MM-dd")+ "\\TaskNo：RX最终复检";
            while (string.IsNullOrEmpty(mLocationBuffer) && iretry < 10)
            {
                System.Threading.Thread.Sleep(100);
                mLocationBuffer = AlgLastContent;
                iretry++;
            }
            if (string.IsNullOrEmpty(mLocationBuffer) || mLocationBuffer.Contains("PieceOK"))
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    Log.log.Write($"接收DCCK数据超时", System.Drawing.Color.Red);
                }
                return FCResultType.IDLE;
            }
            else
            {
                pcbaRechkDataList = get_xyr_from_string(mLocationBuffer);
                if (pcbaRechkDataList.Contains(1.1))
                {
                    dischargeRes = false;
                    Log.log.Write("视觉复检结果超限，当NG产品排出", Color.Red);
                    model.MountingResult.MountingImagePath = ImagePath;
                    return FCResultType.NEXT;
                }
                if (pcbaRechkDataList.Count > 3)
                {
                    if (Math.Abs(pcbaRechkDataList[0]) <= DataModel.RecheckOffsetSpec.XPos && Math.Abs(pcbaRechkDataList[1]) <= DataModel.RecheckOffsetSpec.XPos &&
                        Math.Abs(pcbaRechkDataList[2]) <= DataModel.RecheckOffsetSpec.YPos && Math.Abs(pcbaRechkDataList[3]) <= DataModel.RecheckOffsetSpec.YPos)
                    {
                        dischargeRes = true;
                        PositionInfor p = new PositionInfor();
                        p.XPos = pcbaRechkDataList[0];
                        p.YPos = pcbaRechkDataList[2];
                        p.ZPos = 0;
                        PositionInfor pRight = new PositionInfor();
                        pRight.XPos = pcbaRechkDataList[1];
                        pRight.YPos = pcbaRechkDataList[3];
                        pRight.ZPos = 0;
                        model.RecheckOffset = p;
                        model.MountingResult.RecheckOffset = p;
                        model.MountingResult.RecheckRightOffset = pRight;
                        model.CCDGuideActPosDeviationX1 = (float)pcbaRechkDataList[0];
                        model.CCDGuideActPosDeviationY1 = (float)pcbaRechkDataList[2];
                        model.CCDGuideActPosDeviationX2 = (float)pcbaRechkDataList[1];
                        model.CCDGuideActPosDeviationY2 = (float)pcbaRechkDataList[3];
                        model.CCDGuideActPosDeviationR = model.SecLocationAngle;
                        Log.log.Write($"左光阑复检定位偏移X:{pcbaRechkDataList[0]},左光阑复检定位偏移Y:{pcbaRechkDataList[2]}", Color.Black);
                        Log.log.Write($"右光阑复检定位偏移X:{pcbaRechkDataList[1]},右光阑复检定位偏移Y:{pcbaRechkDataList[3]}", Color.Black);
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        dischargeRes = false;
                        model.CCDGuideActPosDeviationX1 = (float)pcbaRechkDataList[0];
                        model.CCDGuideActPosDeviationY1 = (float)pcbaRechkDataList[2];
                        model.CCDGuideActPosDeviationX2 = (float)pcbaRechkDataList[1];
                        model.CCDGuideActPosDeviationY2 = (float)pcbaRechkDataList[3];
                        model.CCDGuideActPosDeviationR = model.SecLocationAngle;
                        Log.log.Write($"左光阑复检定位偏移X:{pcbaRechkDataList[0]},左光阑复检定位偏移Y:{pcbaRechkDataList[2]}", Color.Black);
                        Log.log.Write($"右光阑复检定位偏移X:{pcbaRechkDataList[1]},右光阑复检定位偏移Y:{pcbaRechkDataList[3]}", Color.Black);
                        Log.log.Write("视觉复检结果超限，当NG产品排出", Color.Red);
                        model.MountingResult.MountingImagePath = ImagePath;
                        return FCResultType.NEXT;
                    }
                }
                else
                {
                    if (AutoRunJTime.On(30000))
                    {
                        Log.log.Write("获取pcb数据失败", Color.Red);
                        AutoRunJTime.Restart();
                        return FCResultType.IDLE;
                    }
                    return FCResultType.IDLE;
                }
            }
        }



        private FCResultType npFlowChart59_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("AssembCompeletedPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z3, PosCom.YPos, model.Z3Speed, model.Z3AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart3_FlowRun_1(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MircPlatformCamPointsOrder.GetItemByName("MicoPlatformOrignalPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = MoveXYRPlatform(G4.Motion.EnumAxisType.R1, G4.Motion.EnumAxisType.R2, G4.Motion.EnumAxisType.R3, PosCom.XPos/* + pcbOffsetX*/, PosCom.YPos/* + pcbOffsetY*/, PosCom.ZPos /*+ pcbAngleOffset*/,
                    model.R1Speed, model.R1AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart58_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart5_FlowRun(object sender, EventArgs e)
        {
            if (model.IsUseSoftLand)
            {
                Helper.ForceControl.StopSoftLand();
                model.ForceSaveImagePath = basePathForceImage + model.MountingResult + "_" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss-ms") + ".png";
            }
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("Z4OrignalPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z4, PosCom.ZPos, model.Z4Speed, model.Z4AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart4_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart2_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    if (!dischargeRes && assembModel.Contains("RX"))
                    {
                        AutoRunJTime.Restart();
                        Log.log.Write("出料复检结果NG，执行RX出料流程", Color.Black);
                        model.MountingResult.ReckeckHeight = new List<float> { 0, 0, 0, 0 };
                        return FCResultType.CASE2;
                    }
                    else if (!dischargeRes && assembModel.Contains("TX"))
                    {
                        AutoRunJTime.Restart();
                        Log.log.Write("出料复检结果NG，执行TX出料流程", Color.Black);
                        model.MountingResult.ReckeckHeight = new List<float> { 0, 0, 0, 0 };
                        return FCResultType.CASE3;
                    }
                    else
                    {
                        AutoRunJTime.Restart();
                        Log.log.Write("出料复检结果OK，执行复检测高流程", Color.Black);
                        return FCResultType.NEXT;
                    }
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart7_FlowRun(object sender, EventArgs e)
        {
            if (model.PublicCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            int Index = heightData.Count;
            PosCom = model.PublicCamPointsOrder.GetItemByName("GetHeightCompeletedPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = MoveTo(PosCom.XPos, PosCom.YPos, PosCom.ZPos, LeftGlue.GlueExcuteModel.GlueXYAxisSpd, LeftGlue.GlueExcuteModel.GlueXYAxisAcc);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart6_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart9_FlowRun_1(object sender, EventArgs e)
        {
            if (model.IsUseSoftLand)
            {
                Helper.ForceControl.StopSoftLand();
                model.ForceSaveImagePath = basePathForceImage + model.MountingResult + "_" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss-ms") + ".png";
            }
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("Z4OrignalPos");
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            AssembModuleTask = Task.Run(() =>
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z4, PosCom.ZPos, model.Z4Speed, model.Z4AccSpeed);
                AssembModuleTaskRtn = rtn;
            });
            AutoRunJTime.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart8_FlowRun(object sender, EventArgs e)
        {
            if (null == AssembModuleTask || AssembModuleTask.IsCompleted)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    return FCResultType.NEXT;
                }
                else
                {
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }
    }

}
