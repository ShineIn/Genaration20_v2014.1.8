﻿using Infrastructure;
using ModuleData20.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Googo.Manul.Views;

namespace ModuleData20
{
    /// <summary>
    /// UCDropGlue.xaml 的交互逻辑
    /// </summary>
    public partial class UCMountingProject : UserControl
    {
        public UCMountingProject()
        {
            InitializeComponent();
        }

        public static DependencyProperty SubProjectProperty = DependencyProperty.Register("SubProject", typeof(MountingProject), typeof(UCMountingProject), new FrameworkPropertyMetadata(new MountingProject(),FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public MountingProject SubProject
        {
            get { return (MountingProject)GetValue(SubProjectProperty); }
            set { SetValue(SubProjectProperty, value); }
        }

        private void SavePro_Click(object sender, RoutedEventArgs e)
        {
            if (null == SubProject) return;
            Microsoft.Win32.SaveFileDialog fd = new Microsoft.Win32.SaveFileDialog();
            fd.Title = "保存项目";
            fd.Filter = "(*.xml)|*.xml";
            if (true == fd.ShowDialog())
            {
                XmlSerializerHelper.Save(SubProject, fd.FileName);
            }
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog fd = new Microsoft.Win32.OpenFileDialog();
            fd.Multiselect = false;
            fd.Title = "选择导入项目";
            fd.Filter = "(*.xml)|*.xml";
            if (fd.ShowDialog() == true)
            {
                string strErr = string.Empty;
                MountingProject project = XmlSerializerHelper.Load<MountingProject>(fd.FileName, ref strErr, false);
                if (null != project)
                    SubProject = project;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (null == SubProject) return;
            WinCommuObject winCommu = new WinCommuObject(SubProject);
            winCommu.Show();
        }
    }
}
