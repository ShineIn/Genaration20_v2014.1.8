﻿using System;
using System.Windows.Forms;
using Sunny.UI;

namespace ModuleData20.Views
{
    public partial class frmConfirm : UIForm
    {
        /// <summary>
        /// DialogResult.Yes es un reintentar y Abort es no reintentar
        /// </summary>
        public DialogResult dResult = DialogResult.None;

        public frmConfirm()
        {
            InitializeComponent();
            this.ControlBox = false;
            this.Show();
            this.Hide();
        }

        public void fnSetMessageAndButtons(string sMessage, bool bRetryEnabled, bool bSkipEnabled, bool bAbortEnabled)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((MethodInvoker)(() =>
                {
                    btnConfirm.Visible = (bRetryEnabled) ? true : false;
                    btnIgnore.Visible = (bSkipEnabled) ? true : false;
                    btnCancel.Visible = (bAbortEnabled) ? true : false;
                    dResult = DialogResult.None;
                    lbl_Tips.Text = sMessage;
                }));
            }
            else
            {
                btnConfirm.Visible = (bRetryEnabled) ? true : false;
                btnIgnore.Visible = (bSkipEnabled) ? true : false;
                btnCancel.Visible = (bAbortEnabled) ? true : false;
                dResult = DialogResult.None;
                lbl_Tips.Text = sMessage;
            }
        }

        public void fnSetTextMessageNShow(string strMessage, bool bRetryEnabled, bool bSkipEnabled, bool bAbortEnabled)
        {
            if (this.InvokeRequired)
            {
                this.BeginInvoke((MethodInvoker)(() =>
                {
                    btnConfirm.Visible = (bRetryEnabled) ? true : false;
                    btnIgnore.Visible = (bSkipEnabled) ? true : false;
                    btnCancel.Visible = (bAbortEnabled) ? true : false;
                    dResult = DialogResult.None;
                    lbl_Tips.Text = strMessage;
                    this.ShowDialog();
                }));
            }
            else
            {
                btnConfirm.Visible = (bRetryEnabled) ? true : false;
                btnIgnore.Visible = (bSkipEnabled) ? true : false;
                btnCancel.Visible = (bAbortEnabled) ? true : false;
                dResult = DialogResult.None;
                lbl_Tips.Text = strMessage;
                this.ShowDialog();
            }
        }

        public void fnChangeButtonRetryMessage(string msg)
        {
            this.BeginInvoke((MethodInvoker)(() =>
            {
                btnConfirm.Text = msg;
            }));
        }
        public void doHideWindow()
        {
            this.BeginInvoke((MethodInvoker)(() =>
            {
                this.Hide();
            }));
        }
        private void btnReintentar_Click(object sender, EventArgs e)
        {
            this.BeginInvoke((MethodInvoker)(() =>
            {
                dResult = DialogResult.Yes;
                this.Hide();
            }));
        }
        private void btnSaltar_Click(object sender, EventArgs e)
        {
            this.BeginInvoke((MethodInvoker)(() =>
            {
                dResult = DialogResult.Ignore;
                this.Hide();
            }));
        }
        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.BeginInvoke((MethodInvoker)(() =>
            {
                dResult = DialogResult.Abort;
                this.Hide();
            }));
        }
    }
}
