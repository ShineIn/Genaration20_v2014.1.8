﻿using Googo.Manul;
using LogSv;
using ModuleData;
using ModuleData20.ViewModels;
using Newtonsoft.Json;
using SuperSimpleTcp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using VUControl;
using YHSDK;

namespace ModuleData20
{
    public partial class uc_PCMounting : ModuleBase
    {
        public uc_PCMounting()
        {
            InitializeComponent();
            InitDgv_CommunicateSetting(UCMounting.Dgv_MReadMovePara, ref Dispense1MoveReadDgvDatas);
            InitDgv_CommunicateSetting(UCMounting.Dgv_MWriteMovePara, ref uc_MountingFlow.Dispense1MoveWriteDgvDatas);
            InitDgv_CommunicateSetting(UCMounting.Dgv_MReadFlowPara, ref Dispense1FlowReadDgvDatas);
            InitDgv_CommunicateSetting(UCMounting.Dgv_MWriteFlowPara, ref Dispense1FlowWriteDgvDatas);
        }
        protected override void Run()
        {
            if (DCTcpClient == null || !DCTcpClient.IsConnected) return;
            if (npFlowChart10.WorkFlow == null)
                npFlowChart10.TaskReset();
            //CloseAlarmFrm();
            npFlowChart10.TaskRun();
            if (null == LeftGlue)
            {
                MountingProject project = model.ParentStation as MountingProject;
                if (null != project)
                {
                    LeftGlue = project.LeftGlue;
                }
            }
        }
        public override void ResetChart()
        {
            StartChart = npFlowChart10;
            npFlowChart10.TaskReset();
        }



        /// <summary>
        /// 测高传感器TCP对象
        /// </summary>
        /// <remarks>
        /// TCP对象，到最外层直接进行绑定
        /// </remarks>
        [XmlIgnore]
        public SimpleTcpClient SensorClient
        {
            get { return _sensorClient; }
            set
            {
                if (_sensorClient != null && _sensorClient.Events != null)
                {
                    _sensorClient.Events.DataReceived -= sensorDataReceived;
                    _sensorClient.Events.Disconnected -= Events_Disconnected;

                }

                _sensorClient = value;
                if (value != null && value.Events != null)
                {
                    value.Events.DataReceived += sensorDataReceived;
                    value.Events.Disconnected += Events_Disconnected;
                }
                OnPropertyChanged();
            }
        }
        private SimpleTcpClient _sensorClient;
        #region 私有属性
        private List<CommunicateDgvData> Dispense1MoveReadDgvDatas = new List<CommunicateDgvData>();
        private List<CommunicateDgvData> Dispense1MoveWriteDgvDatas = new List<CommunicateDgvData>();
        private List<CommunicateDgvData> Dispense1FlowReadDgvDatas = new List<CommunicateDgvData>();
        private List<CommunicateDgvData> Dispense1FlowWriteDgvDatas = new List<CommunicateDgvData>();
        private PositionInfor PosCom;
        JTimer AutoRunJTime = new JTimer();
        Task AssembModuleTask;
        int AssembModuleTaskRtn;
        Task Y3MotorTask;
        int Y3MotorTaskRtn;
        string ImagePath = string.Empty;
        bool spotCheckResult = false;
        List<double> pcbaRechkDataList = new List<double>();
        private GlueProject LeftGlue = null;
        public MountingMode DataModel
        {
            get { return model; }
            set { model = value; OnPropertyChanged(); }
        }
        private MountingMode model = new MountingMode();
        int SpotCheckType = 0;      //1:贴装精度  2：测高  3 点胶


        /// <summary>
        /// 光阑和PCB测量相对高度列表
        /// </summary>
        /// <remarks>
        /// 贴装完后复检光阑和PCB的段差
        /// </remarks>
        [Browsable(false)]
        public List<float> LeftCheckHeightData
        {
            get { return leftCheckHeightData; }
            private set { leftCheckHeightData = value; OnPropertyChanged(); }
        }
        List<float> leftCheckHeightData = new List<float>();

        /// <summary>
        /// 传感器TCP客户端接受到的最新信息
        /// </summary>
        /// <remarks>
        /// 获取之后马上清空信息
        /// </remarks>
        [XmlIgnore]
        private string SensorLastContent
        {
            get
            {
                string ret = sensorLastContent;
                sensorLastContent = string.Empty;
                return ret;
            }
            set { sensorLastContent = value; OnPropertyChanged(); }
        }
        private string sensorLastContent = string.Empty;
        #endregion


        #region 私有函数
        #region 获取dgv数据
        public void InitDgv_CommunicateSetting(VUDatagridView dataGridView, ref List<CommunicateDgvData> communicateDgvDatas)
        {
            communicateDgvDatas.Clear();
            CommunicateDgvData dgvData = new CommunicateDgvData();
            for (int i = 0; i < dataGridView.myDgv.Rows.Count; i++)
            {
                dgvData.name = dataGridView.GetDataArrayByIndex(i)[2];
                dgvData.data = dataGridView.GetDataArrayByIndex(i)[1];
                communicateDgvDatas.Add(dgvData);
            }
        }

        public string GetAddressFormDgv(string name, List<CommunicateDgvData> communicateDgvDatas)
        {
            if (communicateDgvDatas != null && communicateDgvDatas.Count > 0)
            {
                for (int i = 0; i < communicateDgvDatas.Count; i++)
                {
                    if (communicateDgvDatas[i].name == name)
                        return communicateDgvDatas[i].data;
                }
                return "999";
            }
            else { return "999"; }
        }
        #endregion
        /// <summary>
        /// 写入PLC并检测返回成功
        /// </summary>
        /// <param name="addr"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private bool WritePlcIsSuccess(string addr, object value)
        {
            Type type = value.GetType();
            if (type == typeof(bool))
            {
                GloMotionParame.SiemenPLC.Write(addr, (bool)value);
                DelayMs(5);
                if ((bool)value == (bool)GloMotionParame.SiemenPLC.Read(addr)) return true;
                else return false;
            }
            else if (type == typeof(string))
            {
                GloMotionParame.SiemenPLC.Write(addr, (string)value);
                DelayMs(5);
                if ((string)value == (string)GloMotionParame.SiemenPLC.Read(addr)) return true;
                else return false;
            }
            else if (type == typeof(float))
            {
                GloMotionParame.SiemenPLC.Write(addr, (float)value);
                DelayMs(5);
                if ((float)value == (float)GloMotionParame.SiemenPLC.Read(addr)) return true;
                else return false;
            }
            else if (type == typeof(ushort))
            {
                GloMotionParame.SiemenPLC.Write(addr, (ushort)value);
                DelayMs(5);
                if ((ushort)value == (ushort)GloMotionParame.SiemenPLC.Read(addr)) return true;
                else return false;
            }
            return false;
        }

        public static bool DelayMs(int delayMilliseconds)
        {
            System.DateTime now = DateTime.Now;
            double s;
            do
            {
                TimeSpan spand = DateTime.Now - now;
                s = spand.TotalMilliseconds + spand.Seconds * 1000;
                Application.DoEvents();
            }
            while (s < delayMilliseconds);
            return true;
        }

        /// <summary>
        /// 移动单Y3轴，光阑触发需要单独停止此轴
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="pos"></param>
        /// <param name="vel"></param>
        /// <param name="acc"></param>
        /// <param name="isStop"></param>
        /// <returns></returns>
        private bool MoveSingleAxis(G4.Motion.EnumAxisType axis, double pos, double vel, double acc)
        {
            double curPos = 0;
            SmartGlue.Infrastructure.AxisStatus status = new SmartGlue.Infrastructure.AxisStatus();
            G4.Motion.MotionModel.Instance.GetPosition(axis, out curPos);
            G4.Motion.MotionModel.Instance.GetAxisStatus(axis, ref status);
            if (!status.Moving && curPos != pos)
            {
                Y3MotorTask = Task.Run(() =>
                {
                    var rtn = G4.Motion.MotionModel.Instance.MoveTo(axis, pos, vel, acc);
                    Y3MotorTaskRtn = rtn;
                });
                return false;
            }
            else
            {
                if (null != Y3MotorTask && Y3MotorTask.IsCompleted && curPos == pos)
                {
                    if (Y3MotorTaskRtn == 0)
                    {
                        AutoRunJTime.Restart();
                        return true;
                    }
                    else
                    {
                        Log.log.Write("组装模组执行Move失败", Color.Red);
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        private int MoveTo(double X, double Y, double Z, double vel, double acc, bool IsSafety = true)
        {
            if (IsSafety)
            {
                var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z1, 0, vel, acc);
                if (rtn != 0) return rtn;
            }
            var rtnXY = MoveToDoubleAxis(G4.Motion.EnumAxisType.X1, X, G4.Motion.EnumAxisType.Y1, Y, vel, acc, vel, acc);
            if (0 != rtnXY) return rtnXY;
            return G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z1, Z, vel, acc);
        }

        /// <summary>
        /// 算法客户端接受信息事件
        /// </summary>
        /// <remarks>
        /// 可以重新该事件，在里面做自己想要的事件处理
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sensorDataReceived(object sender, SuperSimpleTcp.DataReceivedEventArgs e)
        {
            if (e.Data == null) return;
            string strRec = Encoding.UTF8.GetString(e.Data.Array);
            Debug.WriteLine($"Port={e.IpPort}+Data={strRec}");
            SensorLastContent = strRec;
        }

        private void Events_Disconnected(object sender, SuperSimpleTcp.ConnectionEventArgs e)
        {
            Debug.WriteLine($"Port={e.IpPort} Disconnected Reason={e.Reason}");
        }
        private bool GetSensorData(ref double height)
        {
            byte[] dataRev = new byte[1024];
            if (null == SensorClient || !SensorClient.IsConnected)
            {
                return false;
            }
            SensorClient.Send("M0\r\n");
            string mLocationBuffer = string.Empty;
            int iretryCount = 0;
            while (string.IsNullOrEmpty(mLocationBuffer) && iretryCount < 10)
            {
                iretryCount++;
                Thread.Sleep(100);
                mLocationBuffer = SensorLastContent;
            }
            if (string.IsNullOrEmpty(mLocationBuffer))
            {
                return false;
            }
            var arr = mLocationBuffer.Split(',');
            if (arr == null || arr.Length < 2)
            {
                return false;
            }
            if (double.TryParse(arr[1], out double val))
            {
                height = val * 0.001;
                return true;
            }
            return false;
        }

        private List<double> get_xyr_from_string(string input_string)
        {
            List<double> arrResult = new List<double>();
            try
            {
                string[] strArr;
                string[] strArrPath;
                strArrPath = input_string.Split(';');
                if (strArrPath.Length > 1)
                    ImagePath = strArrPath[1];
                strArr = strArrPath[0].Split(',');
                foreach (string str in strArr)
                {
                    arrResult.Add(double.Parse(str));
                }
                return arrResult;
            }
            catch (Exception ex)
            {
                arrResult.Add(1.1);
                arrResult.Add(1.1);
                return arrResult;
            }

        }
        #endregion
        #region 轴移动
        public int MoveToHomeXYR()
        {
            var XYRRet = Task.WhenAll(Task.Run(() => { return G4.Motion.MotionModel.Instance.AxisMoveToHome(G4.Motion.EnumAxisType.R1); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.AxisMoveToHome(G4.Motion.EnumAxisType.R2); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.AxisMoveToHome(G4.Motion.EnumAxisType.R3); }));
            XYRRet.Wait();
            var result = XYRRet.Result;
            if (result.All(p => p == 0))
            {
                return 0;
            }
            else { return 1; }
        }

        public int MoveToDoubleAxis(G4.Motion.EnumAxisType axis1, double pos1, G4.Motion.EnumAxisType axis2, double pos2, double vel1, double acc1, double vel2, double acc2)
        {
            var XYRRet = Task.WhenAll(Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(axis1, pos1, vel1, acc1); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(axis2, pos2, vel2, acc2); }));
            XYRRet.Wait();
            var result = XYRRet.Result;
            if (result.All(p => p == 0))
            {
                return 0;
            }
            else { return 1; }
        }

        public int MoveToThrAxis(G4.Motion.EnumAxisType axis1, double pos1, G4.Motion.EnumAxisType axis2, double pos2, G4.Motion.EnumAxisType axis3, double pos3,
            double vel1, double acc1, double vel2, double acc2, double vel3, double acc3)
        {
            var XYRRet = Task.WhenAll(Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(axis1, pos1, vel1, acc1); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(axis2, pos2, vel2, acc2); })
                  , Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(axis3, pos3, vel3, acc3); }));
            XYRRet.Wait();
            var result = XYRRet.Result;
            if (result.All(p => p == 0))
            {
                return 0;
            }
            else { return 1; }
        }

        public int MoveXYRPlatform(G4.Motion.EnumAxisType R1, G4.Motion.EnumAxisType R2, G4.Motion.EnumAxisType R3, double XYR_XPos, double XYR_YPos, double XYR_RPos, double vel, double acc)
        {
            var XYRet = Task.WhenAll(Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(R1, XYR_XPos, vel, acc); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(R2, XYR_XPos, vel, acc); })
                  , Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(R3, XYR_YPos, vel, acc); }));
            var resultXY = XYRet.Result;

            var RRet = Task.WhenAll(Task.Run(() => { return XXYRotateTo(XYR_RPos, vel, acc); }));
            var resultR = RRet.Result;
            if (resultXY.All(p => p == 0) && resultR.All(p => p == 0))
            {
                return 0;
            }
            else { return 1; }
        }

        public int XXYRotateTo(double deg, double vel, double acc)
        {
            const double R = 91.924;
            const double Y = 225;
            const double X1 = 315;
            const double X2 = 135;

            double dx1 = R * Math.Cos((X1 + deg) * Math.PI / 180) - R * Math.Cos(X1 * Math.PI / 180);
            double dx2 = R * Math.Cos((X2 + deg) * Math.PI / 180) - R * Math.Cos(X2 * Math.PI / 180);
            double dy = R * Math.Sin((Y + deg) * Math.PI / 180) - R * Math.Sin(Y * Math.PI / 180);

            var XYRet = Task.WhenAll(Task.Run(() => { return G4.Motion.MotionModel.Instance.Move(G4.Motion.EnumAxisType.R1, dx1, vel, acc); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.Move(G4.Motion.EnumAxisType.R2, dx2, vel, acc); })
                  , Task.Run(() => { return G4.Motion.MotionModel.Instance.Move(G4.Motion.EnumAxisType.R3, dy, vel, acc); }));
            var result = XYRet.Result;
            if (result.All(p => p == 0))
            {
                return 0;
            }
            else { return 1; }
        }
        #endregion

        private YHSDK.FCResultType npFlowChart10_FlowRun(object sender, EventArgs e)
        {
            //return FCResultType.IDLE;
            bool bvalue1, bvalue2, bvalue3;
            bool bret1 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("IB_AutoSpotCheckMounting", uc_MountingFlow.Dispense1MoveReadDgvDatas), out bvalue1);     //贴装精度点检
            //bool bret2 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("IB_AutoSpotCheckHeight", uc_MountingFlow.Dispense1MoveReadDgvDatas), out bvalue2);     //测高点检
            //bool bret3 = GloMotionParame.SiemenPLC.ReadValue(GetAddressFormDgv("IB_AutoSpotCheckGlue", uc_MountingFlow.Dispense1MoveReadDgvDatas), out bvalue3);     //点胶点检

            if (bret1 && bvalue1)
            {
                SpotCheckType = 1;
                bool ret2 = WritePlcIsSuccess(GetAddressFormDgv("OB_SpotCheckMountingOK", uc_MountingFlow.Dispense1MoveWriteDgvDatas), false);
                bool ret3 = WritePlcIsSuccess(GetAddressFormDgv("OB_SpotCheckMountingNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), false);
                bool ret4 = WritePlcIsSuccess(GetAddressFormDgv("OB_SpotCheckMountingTaskSendData", uc_MountingFlow.Dispense1MoveWriteDgvDatas), false);
                if (ret2 && ret3 && ret4)
                {
                    if (WritePlcIsSuccess(GetAddressFormDgv("OB_" + "SpotCheckMountingTask", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true))
                    {
                        Log.log.Write(string.Format($"接收到贴装精度点检任务指令，贴装模组开始工作"), Color.Black);

                        return FCResultType.NEXT;
                    }
                    else
                    {
                        return FCResultType.IDLE;
                    }
                }
                else
                {
                    return FCResultType.IDLE;
                }
            }
            //else if (bret2 && bvalue2)
            //{
            //    SpotCheckType = 2;
            //    bool ret2 = WritePlcIsSuccess(GetAddressFormDgv("OB_SpotCheckHeightOK", uc_MountingFlow.Dispense1MoveWriteDgvDatas), false);
            //    bool ret3 = WritePlcIsSuccess(GetAddressFormDgv("OB_SpotCheckHeightNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), false);
            //    bool ret4 = WritePlcIsSuccess(GetAddressFormDgv("OB_SpotCheckHeightTaskSendData", uc_MountingFlow.Dispense1MoveWriteDgvDatas), false);
            //    if (ret2 && ret3 && ret4)
            //    {
            //        if (WritePlcIsSuccess(GetAddressFormDgv("OB_" + "SpotCheckHeightTask", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true))
            //        {
            //            Log.log.Write(string.Format($"接收到贴装精度点检任务指令，贴装模组开始工作"), Color.Black);

            //            return FCResultType.NEXT;
            //        }
            //        else
            //        {
            //            return FCResultType.IDLE;
            //        }
            //    }
            //    else
            //    {
            //        return FCResultType.IDLE;
            //    }
            //}
            //else if (bret3 && bvalue3)
            //{
            //    SpotCheckType = 3;
            //    bool ret2 = WritePlcIsSuccess(GetAddressFormDgv("OB_SpotCheckGlueOK", uc_MountingFlow.Dispense1MoveWriteDgvDatas), false);
            //    bool ret3 = WritePlcIsSuccess(GetAddressFormDgv("OB_SpotCheckGlueNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), false);
            //    bool ret4 = WritePlcIsSuccess(GetAddressFormDgv("OB_SpotCheckGlueTaskSendData", uc_MountingFlow.Dispense1MoveWriteDgvDatas), false);
            //    if (ret2 && ret3 && ret4)
            //    {
            //        if (WritePlcIsSuccess(GetAddressFormDgv("OB_" + "SpotCheckGlueTask", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true))
            //        {
            //            Log.log.Write(string.Format($"接收到點膠精度点检任务指令，贴装模组开始工作"), Color.Black);

            //            return FCResultType.NEXT;
            //        }
            //        else
            //        {
            //            return FCResultType.IDLE;
            //        }
            //    }
            //    else
            //    {
            //        return FCResultType.IDLE;
            //    }
            //}
            else
            {
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart1_FlowRun(object sender, EventArgs e)
        {
            model.RecheckOffset = new PositionInfor();
            model.SecLocationAngle = 0.0f;
            model.SecLocationXY = new PositionInfor();
            model.PiecePlatness = 0.0f;
            model.PieceFirstLocation = new PositionInfor();
            model.PcbFirstLocation = new PositionInfor();
            model.PiecePcbHeightList = new List<string>();
            model.PieceHousingHeightList = new List<string>();
            model.MountingResult = new ResultInfor();
            LeftCheckHeightData.Clear();
            model.LocationXYCount = 0;
            model.LocationAngleCount = 0;
            model.Assemblytimessetup = 0;
            model.PieceHeightSpec = new List<float> { model.STOPAccepttheightLowerLimit, model.STOPAccepttheightUpperLimit };
            model.ReckeckHeightSpec = new List<float> { model.STOPHeightLowerLimit, model.STOPHeightUpperLimit };
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart47_FlowRun(object sender, EventArgs e)
        {
            string cmd = string.Empty;
            if (SpotCheckType == 1 || SpotCheckType == 3)
            {
                cmd = "TakePicture_assmebly";
            }
            else if (SpotCheckType == 2)
            {
                cmd = "RecheckHeightPos";
            }
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName(cmd);
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            if (MoveSingleAxis(G4.Motion.EnumAxisType.Y3, PosCom.XPos, model.Y3Speed, model.Y3AccSpeed))
            {
                return FCResultType.NEXT;
            }
            else
            {
                DelayMs(100);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart46_FlowRun(object sender, EventArgs e)
        {
            if ((null == AssembModuleTask || AssembModuleTask.IsCompleted)/* && ret*/)
            {
                if (AssembModuleTaskRtn == 0)
                {
                    AutoRunJTime.Restart();
                    if (SpotCheckType == 2)
                    {
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        return FCResultType.CASE1;
                    }
                }
                else
                {
                    //Log.log.Write("组装模组执行Move失败", Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                    return FCResultType.IDLE;
                }
            }
            else
            {
                DelayMs(100);
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 组装模组执行Move失败", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart228_FlowRun(object sender, EventArgs e)
        {
            string Index = (LeftCheckHeightData.Count + 1).ToString();
            if (model.PublicCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.PublicCamPointsOrder.GetItemByName("CheckHeightPos" + Index);
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }

            var rtn = MoveTo(PosCom.XPos, PosCom.YPos, PosCom.ZPos, LeftGlue.GlueExcuteModel.GlueXYAxisSpd, LeftGlue.GlueExcuteModel.GlueXYAxisAcc);
            if (0 != rtn)
            {
                //Log.log.Write("组装模组执行Move失败", Color.Red);
                Log.log.Write($"{0} 组装模组执行Move失败,错误码{rtn}", Color.Red);
                return FCResultType.IDLE;
            }
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart230_FlowRun(object sender, EventArgs e)
        {
            double height = 0;
            if (GetSensorData(ref height))
            {
                LeftCheckHeightData.Add((float)height);
                AutoRunJTime.Restart();
                return FCResultType.NEXT;
            }
            else
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    //Log.log.Write($"组装模组Move超时", System.Drawing.Color.Red);
                    Log.log.Write($"{0} 测高数据获取超时", Color.Red);
                }
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart227_FlowRun(object sender, EventArgs e)
        {
            if (LeftCheckHeightData.Count >= 8)
            {
                int index = 0;
                List<float> temp = new List<float>();
                float hSpec = 0;
                float lSpec = 0;
                hSpec = DataModel.ReckeckHeightSpec[1];
                lSpec = DataModel.ReckeckHeightSpec[0];
                spotCheckResult = true;
                for (int i = 0; i < LeftCheckHeightData.Count; i++)
                {
                    index++;
                    if (index == 2)
                    {
                        temp.Add((float)Math.Round(LeftCheckHeightData[i - 1] - LeftCheckHeightData[i], 3));
                        if ((LeftCheckHeightData[i - 1] - LeftCheckHeightData[i]) > hSpec || (LeftCheckHeightData[i - 1] - LeftCheckHeightData[i]) < lSpec)
                        {
                            Log.log.Write($"当前光阑平面与PCB平面高度差：{(LeftCheckHeightData[i - 1] - LeftCheckHeightData[i])}，结果：NG", System.Drawing.Color.Red);
                            spotCheckResult = false;
                        }
                        else { Log.log.Write($"当前光阑平面与PCB平面高度差：{(LeftCheckHeightData[i - 1] - LeftCheckHeightData[i])}，结果：OK", System.Drawing.Color.Green); }
                        index = 0;
                    }
                }
                //if (assembModel.Contains("RX"))
                //    model.PiecePcbHeightList = temp;
                //else
                //    model.PieceHousingHeightList = temp;
                model.MountingResult.ReckeckHeight = temp;
                model.ReckeckHeight = temp;
                return FCResultType.NEXT;
            }
            else
            {
                return FCResultType.CASE1;
            }
        }

        private FCResultType npFlowChart43_FlowRun(object sender, EventArgs e)
        {
            string cmd = string.Empty;
            if (SpotCheckType == 1)
            {
                cmd = "SpotCheckMountingPos";
            }
            else
            {
                cmd = "SpotCheckGlue";
            }
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MircPlatformCamPointsOrder.GetItemByName(cmd);
            if (PosCom == null)
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            var rtn = MoveXYRPlatform(G4.Motion.EnumAxisType.R1, G4.Motion.EnumAxisType.R2, G4.Motion.EnumAxisType.R3, PosCom.XPos, PosCom.YPos, PosCom.ZPos,
                model.R1Speed, model.R1AccSpeed);
            if (0 != rtn)
            {
                //Log.log.Write("组装模组执行Move失败", Color.Red);
                Log.log.Write($"贴装模组执行Move失败,错误码{rtn}", Color.Red);
                return FCResultType.IDLE;
            }
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart54_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("AssembPos");
            if (PosCom == null)
            {
                Log.log.Write($"AssembPos序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z4, PosCom.ZPos, model.Z4Speed, model.Z4AccSpeed);
            if (0 != rtn)
            {
                Log.log.Write($"Z4模组执行Move失败,错误码{rtn}", Color.Red);
                return FCResultType.IDLE;
            }
            if (SpotCheckType == 1)
                return FCResultType.NEXT;
            else
                return FCResultType.CASE1;
        }

        private FCResultType npFlowChart221_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("RecheckPicture_piece");
            if (PosCom == null)
            {
                Log.log.Write($"RecheckPicture_piece 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z3, PosCom.YPos, model.Z3Speed, model.Z3AccSpeed);
            if (0 != rtn)
            {
                Log.log.Write($" Z3 模组执行Move失败,错误码={rtn}", Color.Red);
                return FCResultType.IDLE;
            }
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart220_FlowRun(object sender, EventArgs e)
        {
            //通知德创视觉Location任务
            if (model.MircPlatformCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("RecheckPicture_piece");
            if (PosCom == null || string.IsNullOrEmpty(PosCom.StrCommand))
            {
                Log.log.Write($"RecheckPicture_piece 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            if (null != DCTcpClient && DCTcpClient.IsConnected)
            {
                AutoRunJTime.Restart();
                DCTcpClient.Send(PosCom.StrCommand);
                return FCResultType.NEXT;
            }
            else
            {
                Log.log.Write($"DCCK TCP连接断开", System.Drawing.Color.Red);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart217_FlowRun(object sender, EventArgs e)
        {
            string mLocationBuffer = AlgLastContent;
            if (string.IsNullOrEmpty(mLocationBuffer))
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    Log.log.Write($"接收DCCK数据超时", System.Drawing.Color.Red);
                }
                return FCResultType.IDLE;
            }
            else
            {
                Log.log.Write($"光阑拍照返回数据:{mLocationBuffer}", Color.Blue);
                if (mLocationBuffer.Contains("OK"))
                {
                    return FCResultType.NEXT;
                }
                else
                {
                    if (AutoRunJTime.On(30000))
                    {
                        Log.log.Write("获取光阑拍照返回值失败", Color.Red);
                        AutoRunJTime.Restart();
                        return FCResultType.NEXT;
                    }
                    return FCResultType.IDLE;
                }
            }
        }

        private FCResultType npFlowChart215_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("RecheckPicture_pcb");
            if (PosCom == null)
            {
                Log.log.Write($"RecheckPicture_pcb序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z3, PosCom.YPos, model.Z3Speed, model.Z3AccSpeed);
            if (0 != rtn)
            {
                Log.log.Write($"Z3模组执行Move失败,错误码{rtn}", Color.Red);
                return FCResultType.IDLE;
            }
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart56_FlowRun(object sender, EventArgs e)
        {
            //通知德创视觉Location任务
            if (model.MircPlatformCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("RecheckPicture_pcb");
            if (PosCom == null || string.IsNullOrEmpty(PosCom.StrCommand))
            {
                Log.log.Write($"{0} 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            if (null != DCTcpClient && DCTcpClient.IsConnected)
            {
                AutoRunJTime.Restart();
                DCTcpClient.Send(PosCom.StrCommand + "," + "SpotCheck");
                return FCResultType.NEXT;
            }
            else
            {
                Log.log.Write($"DCCK TCP连接断开", System.Drawing.Color.Red);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart55_FlowRun(object sender, EventArgs e)
        {
            string mLocationBuffer;
            mLocationBuffer = string.Empty;
            int iretry = 0;
            while (string.IsNullOrEmpty(mLocationBuffer) && iretry < 10)
            {
                System.Threading.Thread.Sleep(100);
                mLocationBuffer = AlgLastContent;
                iretry++;
            }
            if (string.IsNullOrEmpty(mLocationBuffer) || mLocationBuffer.Contains("PieceOK"))
            {
                if (AutoRunJTime.On(30000))
                {
                    AutoRunJTime.Restart();
                    Log.log.Write($"接收DCCK数据超时", System.Drawing.Color.Red);
                }
                return FCResultType.IDLE;
            }
            else
            {
                pcbaRechkDataList = get_xyr_from_string(mLocationBuffer);
                if (pcbaRechkDataList.Contains(1.1))
                {
                    spotCheckResult = false;
                    Log.log.Write("视觉流程执行失败，请检查重新执行，当NG产品排出", Color.Red);
                    return FCResultType.NEXT;
                }
                if (pcbaRechkDataList.Count > 3)
                {
                    if (Math.Abs(pcbaRechkDataList[0]) <= DataModel.RecheckOffsetSpec.XPos && Math.Abs(pcbaRechkDataList[1]) <= DataModel.RecheckOffsetSpec.XPos &&
                        Math.Abs(pcbaRechkDataList[2]) <= DataModel.RecheckOffsetSpec.YPos && Math.Abs(pcbaRechkDataList[3]) <= DataModel.RecheckOffsetSpec.YPos)
                    {
                        spotCheckResult = true;
                        PositionInfor p = new PositionInfor();
                        p.XPos = pcbaRechkDataList[0];
                        p.YPos = pcbaRechkDataList[2];
                        p.ZPos = 0;
                        model.RecheckOffset = p;
                        model.MountingResult.RecheckOffset = p;
                        PositionInfor pRight = new PositionInfor();
                        pRight.XPos = pcbaRechkDataList[1];
                        pRight.YPos = pcbaRechkDataList[3];
                        pRight.ZPos = 0;
                        model.MountingResult.RecheckRightOffset = pRight;
                        model.CCDGuideActPosDeviationX1 = (float)pcbaRechkDataList[0];
                        model.CCDGuideActPosDeviationY1 = (float)pcbaRechkDataList[2];
                        model.CCDGuideActPosDeviationX2 = (float)pcbaRechkDataList[1];
                        model.CCDGuideActPosDeviationY2 = (float)pcbaRechkDataList[3];
                        model.CCDGuideActPosDeviationR = model.SecLocationAngle;
                        Log.log.Write($"左光阑复检定位偏移X:{pcbaRechkDataList[0]},左光阑复检定位偏移Y:{pcbaRechkDataList[2]}", Color.Black);
                        Log.log.Write($"右光阑复检定位偏移X:{pcbaRechkDataList[1]},右光阑复检定位偏移Y:{pcbaRechkDataList[3]}", Color.Black);
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        spotCheckResult = false;
                        model.CCDGuideActPosDeviationX1 = (float)pcbaRechkDataList[0];
                        model.CCDGuideActPosDeviationY1 = (float)pcbaRechkDataList[2];
                        model.CCDGuideActPosDeviationX2 = (float)pcbaRechkDataList[1];
                        model.CCDGuideActPosDeviationY2 = (float)pcbaRechkDataList[3];
                        model.CCDGuideActPosDeviationR = model.SecLocationAngle;
                        Log.log.Write($"左光阑复检定位偏移X:{pcbaRechkDataList[0]},左光阑复检定位偏移Y:{pcbaRechkDataList[2]}", Color.Black);
                        Log.log.Write($"右光阑复检定位偏移X:{pcbaRechkDataList[1]},右光阑复检定位偏移Y:{pcbaRechkDataList[3]}", Color.Black);
                        Log.log.Write("视觉复检结果超限，当NG产品排出", Color.Red);
                        return FCResultType.NEXT;
                    }
                }
                else
                {
                    if (AutoRunJTime.On(30000))
                    {
                        Log.log.Write("获取pcb数据失败", Color.Red);
                        AutoRunJTime.Restart();
                        return FCResultType.IDLE;
                    }
                    return FCResultType.IDLE;
                }
            }
        }

        private FCResultType npFlowChart59_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("AssembCompeletedPos");
            if (PosCom == null)
            {
                Log.log.Write($"AssembCompeletedPos 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z3, PosCom.YPos, model.Z3Speed, model.Z3AccSpeed);
            if (0 != rtn)
            {
                Log.log.Write($"Z3模组执行Move失败,错误码{rtn}", Color.Red);
                return FCResultType.IDLE;
            }
            return FCResultType.NEXT;
        }


        private FCResultType npFlowChart68_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("Z4OrignalPos");
            if (PosCom == null)
            {
                Log.log.Write($"Z4OrignalPos 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            var rtn = G4.Motion.MotionModel.Instance.MoveTo(G4.Motion.EnumAxisType.Z4, PosCom.ZPos, model.Z4Speed, model.Z4AccSpeed);
            if (0 != rtn)
            {
                Log.log.Write($"Z4模组执行Move失败,错误码{rtn}", Color.Red);
                return FCResultType.IDLE;
            }
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart70_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MircPlatformCamPointsOrder.GetItemByName("MicoPlatformOrignalPos");
            if (PosCom == null)
            {
                Log.log.Write($"MicoPlatformOrignalPos序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            var rtn = MoveXYRPlatform(G4.Motion.EnumAxisType.R1, G4.Motion.EnumAxisType.R2, G4.Motion.EnumAxisType.R3, PosCom.XPos/* + pcbOffsetX*/, PosCom.YPos/* + pcbOffsetY*/, PosCom.ZPos /*+ pcbAngleOffset*/,
                model.R1Speed, model.R1AccSpeed);
            if (0 != rtn)
            {
                //Log.log.Write("组装模组执行Move失败", Color.Red);
                Log.log.Write($"微动组装模组执行Move失败,错误码{rtn}", Color.Red);
                return FCResultType.IDLE;
            }
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart232_FlowRun(object sender, EventArgs e)
        {
            if (model.PublicCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.PublicCamPointsOrder.GetItemByName("GetHeightCompeletedPos");
            if (PosCom == null)
            {
                Log.log.Write($"GetHeightCompeletedPos 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            var rtn = MoveTo(PosCom.XPos, PosCom.YPos, PosCom.ZPos, LeftGlue.GlueExcuteModel.GlueXYAxisSpd, LeftGlue.GlueExcuteModel.GlueXYAxisAcc);
            if (0 != rtn)
            {
                //Log.log.Write("组装模组执行Move失败", Color.Red);
                Log.log.Write($"点胶模组执行Move失败,错误码{rtn}", Color.Red);
                return FCResultType.IDLE;
            }
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart77_FlowRun(object sender, EventArgs e)
        {
            if (model.MountingCamPointsOrder.Count < 1)
            {
                Log.log.Write("贴装定位点位异常", Color.Red);
                return FCResultType.IDLE;
            }
            PosCom = model.MountingCamPointsOrder.GetItemByName("Y3OrignalPos");
            if (PosCom == null)
            {
                Log.log.Write($"Y3OrignalPos 序号的位置指令为空", Color.Red);
                return FCResultType.IDLE;
            }
            if (MoveSingleAxis(G4.Motion.EnumAxisType.Y3, PosCom.XPos, model.Y3Speed, model.Y3AccSpeed))
            {
                return FCResultType.NEXT;
            }
            else
            {
                DelayMs(100);
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart3_FlowRun(object sender, EventArgs e)
        {
            PositionInfor point = LeftGlue.GlueExcuteModel.GlueCheckPointOrders[0];
            if (null == point || point.ExtenCommand == null) return FCResultType.IDLE;
            var rtn = MoveTo(point.XPos, point.YPos, point.ZPos, LeftGlue.GlueExcuteModel.GlueXYAxisSpd, LeftGlue.GlueExcuteModel.GlueXYAxisAcc);
            if (0 != rtn)
            {
                //Log.log.Write("组装模组执行Move失败", Color.Red);
                Log.log.Write($"{0} 组装模组执行Move失败,错误码{rtn}", Color.Red);
                return FCResultType.IDLE;
            }
            return FCResultType.NEXT;

        }

        private FCResultType npFlowChart5_FlowRun(object sender, EventArgs e)
        {
            PositionInfor point = LeftGlue.GlueExcuteModel.GlueCheckPointOrders[0];
            if (null == point || point.ExtenCommand == null) return FCResultType.IDLE;
            byte[] dataRev = new byte[1024];
            //通知德创视觉Location任务
            string mLocationBuffer = AlgLastContent;
            point.ExtenCommand.DownLoad();
            point.ExtenCommand.SN = "SpotCheck";
            String strCommand = !string.IsNullOrEmpty(point.StrCommand) ? point.StrCommand : JsonConvert.SerializeObject(point.ExtenCommand);

            if (string.IsNullOrEmpty(strCommand)) return FCResultType.IDLE;
            DCTcpClient.Send(strCommand);
            mLocationBuffer = string.Empty;
            int iretry = 0;
            while (string.IsNullOrEmpty(mLocationBuffer) && iretry < 20)
            {
                System.Threading.Thread.Sleep(100);
                mLocationBuffer = AlgLastContent;
                iretry++;
            }
            if (string.IsNullOrEmpty(mLocationBuffer))
            {
                PrintLog($"DCCK TCP连接断开", System.Drawing.Color.Red);
                return FCResultType.IDLE;
            }
            ReceiveData glueDisCheck = JsonConvert.DeserializeObject<ReceiveData>(mLocationBuffer);
            if (null == glueDisCheck || glueDisCheck.ResultList == null || glueDisCheck.ResultList.Count < 4)
            {
                PrintLog($"视觉未发送点胶检测结果", System.Drawing.Color.Red);
                return FCResultType.CASE1;
            }
            if (point.Result != null)
            {
                point.Result.Assign(glueDisCheck);
                point.Result.UpLoads();
            }
            if (glueDisCheck.Result == 1 && (bool)glueDisCheck.ResultList[3].Value)
            {
                spotCheckResult = true;
            }
            spotCheckResult = false;
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart4_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart60_FlowRun(object sender, EventArgs e)
        {
            if (SpotCheckType == 1)
            {
                bool ret1 = WritePlcIsSuccess(GetAddressFormDgv("OB_SpotCheckMountingOK", uc_MountingFlow.Dispense1MoveWriteDgvDatas), spotCheckResult);
                bool ret2 = WritePlcIsSuccess(GetAddressFormDgv("OB_SpotCheckMountingNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), !spotCheckResult);
                if (ret1 && ret2)
                {
                    if (WritePlcIsSuccess(GetAddressFormDgv("OB_SpotCheckMountingTaskSendData", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true))
                    {
                        WritePlcIsSuccess(GetAddressFormDgv("OB_StandBy", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true);
                        PrintLog(string.Format($"贴装精度点检任务结束"), Color.Black);
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        return FCResultType.IDLE;
                    }
                }
                return FCResultType.IDLE;
            }
            else if (SpotCheckType == 2)
            {
                bool ret1 = WritePlcIsSuccess(GetAddressFormDgv("OB_SpotCheckHeightOK", uc_MountingFlow.Dispense1MoveWriteDgvDatas), spotCheckResult);
                bool ret2 = WritePlcIsSuccess(GetAddressFormDgv("OB_SpotCheckHeightNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), !spotCheckResult);
                if (ret1 && ret2)
                {
                    if (WritePlcIsSuccess(GetAddressFormDgv("OB_SpotCheckHeightTaskSendData", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true))
                    {
                        WritePlcIsSuccess(GetAddressFormDgv("OB_StandBy", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true);
                        PrintLog(string.Format($"测高点检任务结束"), Color.Black);
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        return FCResultType.IDLE;
                    }
                }
                return FCResultType.IDLE;
            }
            else if (SpotCheckType == 3)
            {
                bool ret1 = WritePlcIsSuccess(GetAddressFormDgv("OB_SpotCheckGlueOK", uc_MountingFlow.Dispense1MoveWriteDgvDatas), spotCheckResult);
                bool ret2 = WritePlcIsSuccess(GetAddressFormDgv("OB_SpotCheckGlueNG", uc_MountingFlow.Dispense1MoveWriteDgvDatas), !spotCheckResult);
                if (ret1 && ret2)
                {
                    if (WritePlcIsSuccess(GetAddressFormDgv("OB_SpotCheckGlueTaskSendData", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true))
                    {
                        WritePlcIsSuccess(GetAddressFormDgv("OB_StandBy", uc_MountingFlow.Dispense1MoveWriteDgvDatas), true);
                        PrintLog(string.Format($"胶水点检任务结束"), Color.Black);
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        return FCResultType.IDLE;
                    }
                }
                return FCResultType.IDLE;
            }
            else return FCResultType.IDLE;
        }
    }
}
