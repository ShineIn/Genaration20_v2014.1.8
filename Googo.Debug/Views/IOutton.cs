﻿using G4.Motion;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows;
using System.Windows.Controls;

namespace Googo.Manul
{
    public class IOButton : Button, INotifyPropertyChanged
    {
        public IOButton() 
        {
        
        }
        static IOButton()
        {
            // set the key to reference the style for this control
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(IOButton), new FrameworkPropertyMetadata(typeof(IOButton)));
        }

        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        public static DependencyProperty IOInforProperty = DependencyProperty.Register("IOInfor", typeof(IOStatus), typeof(IOButton), new PropertyMetadata(null));

        public IOStatus IOInfor
        {
            get { return (IOStatus)GetValue(IOInforProperty); }
            set { SetValue(IOInforProperty, value); }
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            ISelectedParent parent = GetParentSource(this);
            parent.SelectBit = this.IOInfor;
            if (null != IOInfor && IOInfor.IsOutModule && MotionModel.Instance.IsInitialized)
            {
                MotionModel.Instance.SetDo(IOInfor.BitIndex, !IOInfor.IsHasVlue);
            }
        }

        private ISelectedParent GetParentSource(DependencyObject element)
        {
            DependencyObject _element = element;
            while (_element != null)
            {
                _element = VisualTreeHelper.GetParent(_element);
                if (_element is ISelectedParent)
                {
                    return (ISelectedParent)_element;
                }
            }
            return null;
        }
    }
}
