﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using G4.Motion;

namespace Googo.Manul
{
    /// <summary>
    /// IOButton.xaml 的交互逻辑
    /// </summary>
    public partial class IOButton : Button, INotifyPropertyChanged
    {
        public IOButton()
        {
            InitializeComponent();
        }

        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        public static DependencyProperty IOInforProperty = DependencyProperty.Register("IOInfor", typeof(IOStatus), typeof(IOButton), new PropertyMetadata(null));

        public IOStatus IOInfor
        {
            get { return (IOStatus)GetValue(IOInforProperty); }
            set { SetValue(IOInforProperty, value); }
        }



        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);
            base.OnMouseDown(e);
            ISelectedParent parent = GetParentSource(this);
            parent.SelectBit = this.IOInfor;
            if (null != IOInfor && IOInfor.IsOutModule && MotionModel.Instance.IsInitialized)
            {
                MotionModel.Instance.SetDo(IOInfor.BitIndex, !IOInfor.IsHasVlue);
            }
        }

        protected override void OnMouseRightButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseRightButtonDown(e);
            ISelectedParent parent = GetParentSource(this);
            parent.SelectBit = IOInfor;
        }

        private ISelectedParent GetParentSource(DependencyObject element)
        {
            DependencyObject _element = element;
            while (_element != null)
            {
                _element = VisualTreeHelper.GetParent(_element);
                if (_element is ISelectedParent)
                {
                    return (ISelectedParent)_element;
                }
            }
            return null;
        }

    }

    public class ImageIOConvert : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Uri uriSource = null;
            if ((bool)value)
            {
                uriSource = new Uri("../Images/Green.png", UriKind.Relative);
            }
            else
            {
                uriSource = new Uri("../Images/Red.png", UriKind.Relative);
            }
            return new BitmapImage(uriSource);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class ToolTipConvert : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue || value == null || !(value is IOStatus status)) return string.Empty;
            return $"Name:{status.Name},Index={status.BitIndex}";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
