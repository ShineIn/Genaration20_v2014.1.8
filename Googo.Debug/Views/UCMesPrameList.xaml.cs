﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Googo.Manul.Views
{
    /// <summary>
    /// Interaction logic for UCMesPrameItem.xaml
    /// </summary>
    public partial class UCMesPrameList : UserControl, INotifyPropertyChanged
    {
        public UCMesPrameList()
        {
            InitializeComponent();
        }

        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        public static DependencyProperty ParameListProperty = DependencyProperty.Register("ParameList", typeof(IList<ParameterItem>),
            typeof(UCMesPrameList), new FrameworkPropertyMetadata(new ObservableCollection<ParameterItem>() , FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));


        public IList<ParameterItem> ParameList
        {
            get { return (IList<ParameterItem>)GetValue(ParameListProperty); }
            set { SetValue(ParameListProperty, value); }
        }

        public IEnumerable<Type> TypeSource
        {
            get
            {
                return new Type[] { typeof(int), typeof(bool), typeof(float), typeof(uint), typeof(short)
            ,typeof(double),typeof(ushort),typeof(string)};
            }
        }


        public ParameterItem SelectParame
        {
            get { return selectParame; }
            set { selectParame = value; OnPropertyChanged(); }
        }
        private ParameterItem selectParame = null;

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if (null != selectParame)
            {
                ParameList.Remove(selectParame);
            }
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            if (null != ParameList)
            {
                ParameList.Add(new ParameterItem { Name=$"parame_{ParameList.Count}",Datatype=typeof(double)});
            }
        }
    }
}
