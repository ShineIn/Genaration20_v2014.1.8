﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Googo.Manul
{
    /// <summary>
    /// WinFilter.xaml 的交互逻辑
    /// </summary>
    public partial class WinCondition : Window, INotifyPropertyChanged
    {
        public WinCondition(ICondition filter)
        {
            InitializeComponent();
            Debug.Assert(filter != null);
            ConditionType = filter.GetType();
            ConditionInfor = filter;
            panel.Controls.Add(property);
            property.Dock = System.Windows.Forms.DockStyle.Fill;
        }

        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        public Type ConditionType
        {
            get { return conditionType; }
            set
            {
                conditionType = value; OnPropertyChanged();
                if (null != value)
                {
                    ConditionInfor = (ICondition)Activator.CreateInstance(value);
                }
            }
        }
        private Type conditionType;

        public IEnumerable<Type> TypeSource
        {
            get { return new Type[] { typeof(IOCondition), typeof(AxisCondition), typeof(ConditionList),typeof(PLCBaseCondition),typeof(PLCAxisCon) }; }
        }

        public ICondition ConditionInfor
        {
            get { return conditionInfor; }
            set { conditionInfor = value; OnPropertyChanged(); property.SelectedObject = value; }
        }
        private ICondition conditionInfor;


        private void confirm_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            this.Close();
        }
        private System.Windows.Forms.PropertyGrid property = new System.Windows.Forms.PropertyGrid();

    }
}
