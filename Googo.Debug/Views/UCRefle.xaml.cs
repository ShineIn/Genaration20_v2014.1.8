﻿using Infrastructure;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Googo.Manul.Views
{
    /// <summary>
    /// Interaction logic for UCRefle.xaml
    /// </summary>
    public partial class UCRefle : UserControl, INotifyPropertyChanged
    {
        public UCRefle()
        {
            InitializeComponent();
        }

        public static DependencyProperty SubCommuniObjectProperty = DependencyProperty.Register("SubCommuniObject", typeof(CommunicaBase),
typeof(UCRefle), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));


        public CommunicaBase SubCommuniObject
        {
            get { return (CommunicaBase)GetValue(SubCommuniObjectProperty); }
            set { SetValue(SubCommuniObjectProperty, value); }
        }

        public ObservableCollection<OBjectProperty> ParentSource
        {
            get { return parentSource; }
            set { parentSource = value; OnPropertyChanged(); }
        }
        private ObservableCollection<OBjectProperty> parentSource = new ObservableCollection<OBjectProperty>();

        public OBjectProperty SelectedProperty
        {
            get { return selectedProperty; }
            set { selectedProperty = value; OnPropertyChanged(); }
        }
        private OBjectProperty selectedProperty = null;


        public IEnumerable<Type> TypeSource
        {
            get
            {
                return new Type[] { typeof(int), typeof(bool), typeof(float), typeof(uint), typeof(short)
            ,typeof(double),typeof(ushort),typeof(string)};
            }
        }


        public ArqToPLC SelectedArq
        {
            get { return selectedArq; }
            set { selectedArq = value; OnPropertyChanged(); }
        }
        private ArqToPLC selectedArq;


        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {

                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged


        private void Add_Click(object sender, RoutedEventArgs e)
        {
            if (null != selectedProperty && selectedProperty.ArqumentList != null)
                selectedProperty.ArqumentList.Add(new ArqToPLC { Datatype = typeof(bool) });
        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            if (null != selectedProperty && selectedArq != null)
                selectedProperty.ArqumentList.Remove(selectedArq);
        }

        private static void DepenProChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            UCRefle uC = d as UCRefle;
            if (null == uC) return;
            CommunicaBase communica = e.NewValue as CommunicaBase;
            uC.ParentSource.Add(new OBjectProperty(communica) { Name = communica.GetType().Name });
        }


        private void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            SelectedProperty = (OBjectProperty)e.NewValue;
        }
        private void LoadExcel_Click(object sender, RoutedEventArgs e)
        {
            if (parentSource == null) return;
            OpenFileDialog fd = new OpenFileDialog();
            fd.Multiselect = false;
            fd.Title = "选择导入映射文件";
            fd.Filter = $"(*.refle)|*.refle";
            if (fd.ShowDialog() == true)
            {
                string strError = string.Empty;
                OBjectProperty property = XmlSerializerHelper.Load<OBjectProperty>(fd.FileName, ref strError, false);
                if (null == property) return;
                property.Assign(SubCommuniObject);
                parentSource.Clear();
                parentSource.Add(property);
            }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (parentSource == null || 0 == parentSource.Count) return;
            SaveFileDialog fd = new SaveFileDialog();
            fd.Title = "保存映射文件";
            fd.Filter = $"(*.refle)|*.refle";
            if (fd.ShowDialog() == true)
            {
                XmlSerializerHelper.Save(parentSource[0], fd.FileName);
            }
        }
    }
}
