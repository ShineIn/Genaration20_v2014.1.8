﻿using G4.Motion;
using SmartGlue.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Googo.Manul
{
    /// <summary>
    /// UCSingleAxisJog.xaml 的交互逻辑
    /// </summary>
    public partial class UCSingleAxisJog : UserControl
    {
        public UCSingleAxisJog()
        {
            InitializeComponent();
        }

        public static DependencyProperty AxisNumProperty = DependencyProperty.Register("AxisNum", typeof(EnumAxisType), typeof(UCSingleAxisJog), new PropertyMetadata(EnumAxisType.X));
        public static DependencyProperty AxisVelProperty = DependencyProperty.Register("AxisVel", typeof(double), typeof(UCSingleAxisJog), new PropertyMetadata(100.0));
        public static DependencyProperty AxisAccProperty = DependencyProperty.Register("AxisAcc", typeof(double), typeof(UCSingleAxisJog), new PropertyMetadata(50.0));
        public static DependencyProperty IsStepMoveProperty = DependencyProperty.Register("IsStepMove", typeof(bool), typeof(UCSingleAxisJog), new PropertyMetadata(false));
        public static DependencyProperty StepDistanceProperty = DependencyProperty.Register("StepDistance", typeof(double), typeof(UCSingleAxisJog), new PropertyMetadata(1.5));
        public static DependencyProperty LeftBtnSourceProperty = DependencyProperty.Register("LeftBtnImageSource", typeof(ImageSource), typeof(UCSingleAxisJog), new PropertyMetadata(new BitmapImage(new Uri("../Images/Left.png", UriKind.Relative))));
        public static DependencyProperty RightBtnSourceProperty = DependencyProperty.Register("RightBtnImageSource", typeof(ImageSource), typeof(UCSingleAxisJog), new PropertyMetadata(new BitmapImage(new Uri("../Images/Right.png", UriKind.Relative))));

        public EnumAxisType AxisNum
        {
            get { return (EnumAxisType)GetValue(AxisNumProperty); }
            set { SetValue(AxisNumProperty, value); }
        }

        public double AxisVel
        {
            get { return (double)GetValue(AxisVelProperty); }
            set { SetValue(AxisVelProperty, value); }
        }

        public double AxisAcc
        {
            get { return (double)GetValue(AxisAccProperty); }
            set { SetValue(AxisAccProperty, value); }
        }

        public bool IsStepMove
        {
            get { return (bool)GetValue(IsStepMoveProperty); }
            set { SetValue(IsStepMoveProperty, value); }
        }

        public double StepDistance
        {
            get { return (double)GetValue(StepDistanceProperty); }
            set { SetValue(StepDistanceProperty, value); }
        }

        public ImageSource LeftBtnImageSource
        {
            get { return (ImageSource)GetValue(LeftBtnSourceProperty); }
            set { SetValue(LeftBtnSourceProperty, value); }
        }

        public ImageSource RightBtnImageSource
        {
            get { return (ImageSource)GetValue(RightBtnSourceProperty); }
            set { SetValue(RightBtnSourceProperty, value); }
        }

        private void Neg_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!MotionModel.Instance.IsInitialized) return;
            if (IsStepMove)
            {
                MotionModel.Instance.Move(AxisNum, -StepDistance);
            }
            else
            {
                MotionModel.Instance.StartMoving(AxisNum, -AxisVel, AxisAcc);
            }
            IsMouseDown = true;
        }

        private void Button_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if ((!IsMouseDown || IsStepMove) && !MotionModel.Instance.IsInitialized) return;
            MotionModel.Instance.StopMoving(AxisNum);
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            if ((!IsMouseDown || IsStepMove) && !MotionModel.Instance.IsInitialized) return;
            MotionModel.Instance.StopMoving(AxisNum);
        }


        private void Pos_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!MotionModel.Instance.IsInitialized) return;
            if (IsStepMove)
            {
                MotionModel.Instance.Move(AxisNum, -StepDistance);
            }
            else
            {
                MotionModel.Instance.StartMoving(AxisNum, AxisVel, AxisAcc);
            }
            IsMouseDown = true;
        }

        private bool IsMouseDown = false;
    }
}
