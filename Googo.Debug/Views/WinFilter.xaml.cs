﻿using G4.Motion;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Googo.Manul
{
    /// <summary>
    /// WinFilter.xaml 的交互逻辑
    /// </summary>
    public partial class WinFilter : Window, INotifyPropertyChanged
    {
        public WinFilter(FilterInfor filter,bool IsModify=false)
        {
            InitializeComponent();
            Debug.Assert(filter != null);
            FilterType = filter.GetType();
            FilterInfor = filter;
            typecom.IsEnabled= !IsModify;
        }

        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        public Type FilterType
        {
            get { return filterType; }
            set
            {
                filterType = value; OnPropertyChanged();
                if (null != value)
                {
                    FilterInfor = (FilterInfor)Activator.CreateInstance(value);
                }
                if (typeof(AxisFilter) == value)
                {
                    AxisVisible = Visibility.Visible;
                    OutputVisible = Visibility.Collapsed;
                }
                else if (typeof(OutputFilter) == value)
                {
                    AxisVisible = Visibility.Collapsed;
                    OutputVisible = Visibility.Visible;
                }
            }
        }
        private Type filterType;

        public IEnumerable<Type> FilterTypeSource
        {
            get { return new Type[] { typeof(AxisFilter), typeof(OutputFilter) }; }
        }

        public  IEnumerable<EnumAxisType> AxisSource
        {
            get { return Enum.GetValues(typeof(EnumAxisType)).OfType<EnumAxisType>(); }
        }

        public FilterInfor FilterInfor
        {
            get { return filterInfor; }
            set { filterInfor = value; OnPropertyChanged(); }
        }
        private FilterInfor filterInfor;

        public Visibility AxisVisible
        {
            get { return axisVisible; }
            private set { axisVisible = value; OnPropertyChanged(); }
        }
        private Visibility axisVisible = Visibility.Visible;

        public Visibility OutputVisible
        {
            get { return outputVisible; }
            private set { outputVisible = value; OnPropertyChanged(); }
        }
        private Visibility outputVisible = Visibility.Collapsed;

        private void confirm_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            this.Close();
        }
    }

    public class OutSelected : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(null==value||!(value is int index)||null== GloMotionParame.OutputList) return null;
            return GloMotionParame.OutputList.FirstOrDefault((item) => { return item.BitIndex == index; });
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (null == value || !(value is IOStatus status)) return -1;
            return status.BitIndex;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
           return this;
        }
    }
}
