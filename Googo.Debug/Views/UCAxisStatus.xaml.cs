﻿using SmartGlue.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Googo.Manul
{
    /// <summary>
    /// UCAxisStatus.xaml 的交互逻辑
    /// </summary>
    public partial class UCAxisStatus : UserControl
    {
        public UCAxisStatus()
        {
            InitializeComponent();
        }

        public static DependencyProperty StatusProperty = DependencyProperty.Register("Status",typeof(AxisStatus),typeof(UCAxisStatus),new PropertyMetadata(new AxisStatus()));

        public AxisStatus Status
        {
            get { return (AxisStatus)GetValue(StatusProperty); }
            set { SetValue(StatusProperty,value); }
        }
    }
}
