﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using G4.Motion;
using SmartGlue.Infrastructure;
using System.Diagnostics;
using System.IO;
using System.Xml.Serialization;
using Googo.Manul;

namespace Googo.Manul
{
    /// <summary>
    /// UCMotorJog.xaml 的交互逻辑
    /// </summary>
    public partial class UCMotors : UserControl, INotifyPropertyChanged
    {
        public UCMotors()
        {
            InitializeComponent();
            CommandBindings.Add(new CommandBinding(AxisHome, Home_Excute, Home_CanExcute));
            CommandBindings.Add(new CommandBinding(StopAxis, StopAxis_Excute, StopAxis_CanExcute));
            CommandBindings.Add(new CommandBinding(AxisEnable, AxisEnable_Excute, AxisEnable_CanExcute));
            CommandBindings.Add(new CommandBinding(DeleteAxis, DeleteAxis_Excute, DeleteAxis_CanExcute));
            CommandBindings.Add(new CommandBinding(ModifyAxis, ModifyAxis_Excute, DeleteAxis_CanExcute));
            CommandBindings.Add(new CommandBinding(AddAxis, AddAxis_Excute, AddAxisEnble));
            CommandBindings.Add(new CommandBinding(GoToAxis, GoToAxis_Excute, GoToAxis_CanExcute));
            Loaded += UCMotors_Loaded;
        }

        private void UCMotors_Loaded(object sender, RoutedEventArgs e)
        {
            if (null == scanTask || scanTask.IsCompleted)
            {
                scanTask = Task.Run(() =>
                {
                    AxisStatus status = new AxisStatus();
                    while (true)
                    {
                        if (selectedAxis != null)
                        {
                            var item = MotionModel.Instance.GetAxisStatus(selectedAxis.AxisNum, ref status);
                            SelectedAxisStatus = status;
                            MotionModel.Instance.GetPosition(selectedAxis.AxisNum, out double pos);
                            CurrentAxisPos = pos;
                        }
                        System.Threading.Thread.Sleep(100);
                    }
                });
            }
        }

        public static RoutedCommand AxisHome = new RoutedCommand();
        public static RoutedCommand StopAxis = new RoutedCommand();
        public static RoutedCommand AxisEnable = new RoutedCommand();
        public static RoutedCommand AddAxis = new RoutedCommand();
        public static RoutedCommand DeleteAxis = new RoutedCommand();
        public static RoutedCommand ModifyAxis = new RoutedCommand();
        public static RoutedCommand GoToAxis = new RoutedCommand();

        public static DependencyProperty AxisListProperty = DependencyProperty.Register("AxisList", typeof(IList<AxisInfor>), typeof(UCMotors), new FrameworkPropertyMetadata(new ObservableCollection<AxisInfor>(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public IList<AxisInfor> AxisList
        {
            get { return (IList<AxisInfor>)GetValue(AxisListProperty); }
            set { SetValue(AxisListProperty, value); OnPropertyChanged(); }
        }

        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        #region 公有属性
        public AxisInfor SelectedAxis
        {
            get { return selectedAxis; }
            set { selectedAxis = value; OnPropertyChanged(); }
        }
        private AxisInfor selectedAxis = null;

        public AxisStatus SelectedAxisStatus
        {
            get { return selectedAxisStatus; }
            set { selectedAxisStatus = value; OnPropertyChanged(); }
        }
        private AxisStatus selectedAxisStatus;

        /// <summary>
        /// 当前位置
        /// </summary>
        public double CurrentAxisPos
        {
            get { return currentAxisPos; }
            set { currentAxisPos = value; OnPropertyChanged(); }
        }
        private double currentAxisPos;

        /// <summary>
        /// 当前位置
        /// </summary>
        public double TargetPos
        {
            get { return targetPos; }
            set { targetPos = value; OnPropertyChanged(); }
        }
        private double targetPos;
        #endregion 公有属性

        private void Home_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            homeTask = Task.Run(() =>
            {
                int iret = MotionModel.Instance.AxisMoveToHome(selectedAxis.AxisNum);
                Debug.WriteLine($"return {iret}");
            });
        }

        private void Home_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (!MotionModel.Instance.IsInitialized || selectedAxis == null) return;
            e.CanExecute = homeTask == null || homeTask.IsCompleted;
        }

        private void StopAxis_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            int iret = MotionModel.Instance.StopMoving(selectedAxis.AxisNum);
            Debug.WriteLine($"return {iret}");
        }

        private void StopAxis_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = MotionModel.Instance.IsInitialized;
        }

        private void AxisEnable_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            MotionModel.Instance.SetAxisEnable(selectedAxis.AxisNum, !selectedAxisStatus.Enabled);
        }

        private void AxisEnable_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = MotionModel.Instance.IsInitialized && selectedAxis != null;
        }

        private void DeleteAxis_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            AxisList.Remove(selectedAxis);
        }

        private void ModifyAxis_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            WinRegAxis win = new WinRegAxis();
            AxisInfor clone = selectedAxis.Clone();
            win.AxisInfor = clone;
            if (true == win.ShowDialog())
            {
                //修改之后IO不能和其他已经存在的冲突
                //所以能够修改的条件是，找不到相同IO，且不是选择项的
                if (null == AxisList.FirstOrDefault((item) => item.Equals(clone) && item != selectedAxis))
                {
                    selectedAxis.AxisNum = clone.AxisNum;
                    selectedAxis.Name = clone.Name;
                    selectedAxis.CustomName = clone.CustomName;
                }
            }
        }


        private void DeleteAxis_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = selectedAxis != null;
        }


        private void Save_Click(object sender, RoutedEventArgs e)
        {
            if (AxisList != null)
            {
                string dir = System.IO.Path.GetExtension(GloMotionParame.MotorsPath);
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);
                XmlSerializer xs = new XmlSerializer(typeof(ObservableCollection<AxisInfor>));
                FileStream fs = new FileStream(GloMotionParame.MotorsPath, FileMode.Create, FileAccess.Write);
                xs.Serialize(fs, AxisList);
                fs.Close();
            }
        }

        private void AddAxis_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            WinRegAxis win = new WinRegAxis();
            if (true == win.ShowDialog())
            {
                if (null != AxisList.FirstOrDefault((item) => item.Equals(win.AxisInfor)))
                {
                    MessageBox.Show("已存在相同名称或号码信息一样的轴", "警告", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                AxisList.Add(win.AxisInfor);
            }
        }

        private void AddAxisEnble(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = AxisList != null;
        }

        private void GoToAxis_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            int iret = MotionModel.Instance.MoveTo(selectedAxis.AxisNum, targetPos);
            Debug.WriteLine($"return {iret}");
        }

        private void GoToAxis_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (!MotionModel.Instance.IsInitialized || selectedAxis == null) return;
            e.CanExecute = selectedAxisStatus.Enabled;
        }

        private Task homeTask = null;
        private Task scanTask = null;

    }


    public class JogSpeedConvert : MarkupExtension, IMultiValueConverter
    {

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null || values.Length != 2) return 0.0;
            if (values[0] == DependencyProperty.UnsetValue || values[1] == DependencyProperty.UnsetValue) return 0.0;
            if (!(values[0] is double) || !(values[1] is int)) return 0.0;
            return (double)values[0] * (int)values[1] / 100;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
