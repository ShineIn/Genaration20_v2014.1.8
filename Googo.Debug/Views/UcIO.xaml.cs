﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Googo.Manul
{
    /// <summary>
    /// UcIO.xaml 的交互逻辑
    /// </summary>
    public partial class UcIO : UserControl,ISelectedParent, INotifyPropertyChanged
    {
        public static RoutedCommand AddBit = new RoutedCommand();
        public static RoutedCommand InsertBit = new RoutedCommand();
        public static RoutedCommand DeleteBit = new RoutedCommand();
        public static RoutedCommand ModifyBit = new RoutedCommand();

        public UcIO()
        {
            InitializeComponent();
            CommandBindings.Add(new CommandBinding(AddBit, AddBit_Excute, AddBitEnble));
            CommandBindings.Add(new CommandBinding(InsertBit, InsertBit_Excute, DeleteBitEnble));
            CommandBindings.Add(new CommandBinding(DeleteBit, DeleteBit_Excute, DeleteBitEnble));
            CommandBindings.Add(new CommandBinding(ModifyBit, ModifyBit_Excute, DeleteBitEnble));
        }

        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        public static DependencyProperty IOStatuListProperty = DependencyProperty.Register("IOStatuList", typeof(IList<IOStatus>),
            typeof(UcIO),new FrameworkPropertyMetadata(new IOCollection(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));


        public IList<IOStatus> IOStatuList
        {
            get { return (IList<IOStatus>)GetValue(IOStatuListProperty); }
            set { SetValue(IOStatuListProperty, value); }
        }

        public bool IsOutputModule { get; set; }

        public IOStatus SelectBit
        {
            get { return selectBit; }
            set { selectBit = value;OnPropertyChanged(); }
        }
        private IOStatus selectBit = null;


        #region IO信号修改
        private void AddBit_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            IOStatus status = new IOStatus() { IsOutModule=IsOutputModule};
            WinIOInforSet winIO = new WinIOInforSet();
            winIO.Model = status;
            if (true == winIO.ShowDialog())
            {
                if (null != IOStatuList.FirstOrDefault((item) => item.Equals(status)))
                {
                    MessageBox.Show("已存在相同名称或点位信息一样的IO点", "警告", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                IOStatuList.Add(status);
            }
        }

        private void AddBitEnble(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = IOStatuList != null;
        }

        private void InsertBit_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            IOStatus status = new IOStatus() { IsOutModule = IsOutputModule };
            WinIOInforSet winIO = new WinIOInforSet();
            winIO.Model = status;
            if (true == winIO.ShowDialog())
            {
                if (null != IOStatuList.FirstOrDefault((item) => item.Equals(status)))
                {
                    MessageBox.Show("已存在相同名称或点位信息一样的IO点", "警告", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
                int index = IOStatuList.IndexOf(selectBit);
                IOStatuList.Insert(index, status);
            }
        }

        private void DeleteBit_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            IOStatuList.Remove(selectBit);
        }

        /// <summary>
        /// 只有开发者模式下能够删除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteBitEnble(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = selectBit != null;
        }

        private void ModifyBit_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            WinIOInforSet winIO = new WinIOInforSet();
            IOStatus cloneIO = selectBit.Clone();
            winIO.Model = cloneIO;
            winIO.Title = "修改IO信息";
            if (true == winIO.ShowDialog())
            {
                //修改之后IO不能和其他已经存在的冲突
                //所以能够修改的条件是，找不到相同IO，且不是选择项的
                if (null == IOStatuList.FirstOrDefault((item) => item.Equals(cloneIO) && item != selectBit))
                {
                    selectBit.BitDescri = cloneIO.BitDescri;
                    selectBit.BitIndex = cloneIO.BitIndex;
                    selectBit.IsOutModule = IsOutputModule;
                }
            }
        }
        #endregion IO信号修改
    }

    public interface ISelectedParent
    {
        IOStatus SelectBit { get; set; }
    }
}
