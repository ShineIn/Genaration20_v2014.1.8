﻿using G4.Motion;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Googo.Manul
{
    /// <summary>
    /// UCXYZStage.xaml 的交互逻辑
    /// </summary>
    public partial class UCYZZStage : UserControl, INotifyPropertyChanged
    {
        public UCYZZStage()
        {
            InitializeComponent();
            Loaded += UCYZZStage_Loaded;
            Unloaded += UCYZZStage_Unloaded;
        }

        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        public static DependencyProperty Y3AxisNumProperty = DependencyProperty.Register("Y3AxisNum", typeof(EnumAxisType), typeof(UCYZZStage), new PropertyMetadata(EnumAxisType.Y3));
        public static DependencyProperty Z3AxisNumProperty = DependencyProperty.Register("Z3AxisNum", typeof(EnumAxisType), typeof(UCYZZStage), new PropertyMetadata(EnumAxisType.Z3));
        public static DependencyProperty Z4AxisNumProperty = DependencyProperty.Register("Z4AxisNum", typeof(EnumAxisType), typeof(UCYZZStage), new PropertyMetadata(EnumAxisType.Z4));
        public static DependencyProperty AxisAccProperty = DependencyProperty.Register("AxisAcc", typeof(double), typeof(UCYZZStage), new PropertyMetadata(100.0));
        public static DependencyProperty LeftBtnImageSourceAccProperty = DependencyProperty.Register("LeftBtnImageSource", typeof(ImageSource), typeof(UCYZZStage), new PropertyMetadata(new BitmapImage(new Uri("../Images/Left.png", UriKind.Relative))));
        public static DependencyProperty RightBtnImageSourceAccProperty = DependencyProperty.Register("RightBtnImageSource", typeof(ImageSource), typeof(UCYZZStage), new PropertyMetadata(new BitmapImage(new Uri("../Images/Right.png", UriKind.Relative))));

        public EnumAxisType Y3AxisNum
        {
            get { return (EnumAxisType)GetValue(Y3AxisNumProperty); }
            set { SetValue(Y3AxisNumProperty, value); }
        }

        public EnumAxisType Z3AxisNum
        {
            get { return (EnumAxisType)GetValue(Z3AxisNumProperty); }
            set { SetValue(Z3AxisNumProperty, value); }
        }

        public EnumAxisType Z4AxisNum
        {
            get { return (EnumAxisType)GetValue(Z4AxisNumProperty); }
            set { SetValue(Z4AxisNumProperty, value); }
        }
        public ImageSource LeftBtnImageSource
        {
            get { return (ImageSource)GetValue(LeftBtnImageSourceAccProperty); }
            set { SetValue(LeftBtnImageSourceAccProperty, value); }
        }

        public ImageSource RightBtnImageSource
        {
            get { return (ImageSource)GetValue(RightBtnImageSourceAccProperty); }
            set { SetValue(RightBtnImageSourceAccProperty, value); }
        }
        public double MaxVel
        {
            get { return maxVel; }
            set 
            {
                if (value > 0)
                {
                    maxVel = value;
                    OnPropertyChanged();
                    OnPropertyChanged("AxisVel");
                }   
            }
        }
        private double maxVel = 200;

        public double AxisAcc
        {
            get { return (double)GetValue(AxisAccProperty); }
            set { SetValue(AxisAccProperty, value); }
        }

        public double PercentValue
        {
            get { return percentValue; }
            set { percentValue = value; OnPropertyChanged(); OnPropertyChanged("AxisVel"); }
        }
        private double percentValue = 50;

        public double AxisVel
        {
            get { return percentValue * MaxVel / 100; }
        }

        public double Y3Pos
        {
            get { return y3Pos; }
            set { y3Pos = value; OnPropertyChanged(); }
        }
        private double y3Pos = 0;

        public double Z3Pos
        {
            get { return z3Pos; }
            set { z3Pos = value; OnPropertyChanged(); }
        }
        private double z3Pos = 0;

        public double Z4Pos
        {
            get { return z4Pos; }
            set { z4Pos = value; OnPropertyChanged();  }
        }
        private double z4Pos = 0;

        private void UCYZZStage_Unloaded(object sender, RoutedEventArgs e)
        {
            IsLoad = false;
        }

        private void UCYZZStage_Loaded(object sender, RoutedEventArgs e)
        {
            EnumAxisType y3AxisNum = Y3AxisNum;
            EnumAxisType z3AxisNum = Z3AxisNum;
            EnumAxisType z4AxisNum = Z4AxisNum;
            IsLoad = true;
            Task.Run(() =>
            {
                while (IsLoad && MotionModel.Instance.IsInitialized)
                {
                    double pos = 0;
                    MotionModel.Instance.GetPosition(y3AxisNum, out pos);
                    Y3Pos = pos;
                    MotionModel.Instance.GetPosition(z3AxisNum, out pos);
                    Z3Pos = pos;
                    MotionModel.Instance.GetPosition(z4AxisNum, out pos);
                    Z4Pos = pos;
                    System.Threading.Thread.Sleep(500);
                }
            });
        }

        private bool IsLoad = false;
    }
}
