﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using Infrastructure;

namespace Googo.Manul.Views
{
    /// <summary>
    /// UCCommuObject.xaml 的交互逻辑
    /// </summary>
    public partial class WinCommuObject : Window
    {
        public WinCommuObject(CommunicaBase sourceobj)
        {
            Debug.Assert(null != sourceobj);       
            InitializeComponent();
            ucref.SubCommuniObject = sourceobj;
        }
    }

    [Serializable]
    public class OBjectProperty : PropertyChangedBase, INameBase
    {
        public OBjectProperty() { }

        public OBjectProperty(CommunicaBase communica)
        {
            Debug.Assert(null != communica);
            Name = communica.Name;
            communica.CheckArquments();
            ArqumentList = communica.Arquments;
            GetChildren(communica);
        }


        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged(); }
        }
        private string _name = string.Empty;

        public ObservableCollection<OBjectProperty> Children
        {
            get { return children; }
            set { children = value; OnPropertyChanged(); }
        }
        private ObservableCollection<OBjectProperty> children = new ObservableCollection<OBjectProperty>();

        public ObservableCollection<ArqToPLC> ArqumentList
        {
            get { return arqumentList; }
            set { arqumentList = value; OnPropertyChanged(); }
        }
        private ObservableCollection<ArqToPLC> arqumentList = null;

        /// <summary>
        /// 将映射关系赋值给子项
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        public void Assign(CommunicaBase parent)
        {
            Debug.Assert(null != parent);
            parent.Arquments = arqumentList;
            PropertyInfo[] propertyInfos = parent.GetType().GetProperties();
            if (null == propertyInfos || 0 == propertyInfos.Count() || children == null) return;
            foreach (PropertyInfo item in propertyInfos)
            {
                if (null == item || !baseType.IsAssignableFrom(item.PropertyType)) continue;
                object pro = item.GetValue(parent);
                if (null == pro) continue;
                OBjectProperty match = children.FirstOrDefault((tt) => { return tt.Name == item.Name; });
                match.Assign((CommunicaBase)pro);
            }
            return;
        }

        private void GetChildren(CommunicaBase communica)
        {
            PropertyInfo[] propertyInfos = communica.GetType().GetProperties();
            if (null == propertyInfos || 0 == propertyInfos.Count()) return;
            foreach (PropertyInfo item in propertyInfos)
            {
                if (null == item ) continue;
                BrowsableAttribute browsable = item.GetCustomAttribute<BrowsableAttribute>();
                if (null != browsable) continue;
                object pro = item.GetValue(communica);
                if (null == pro) continue;
                if (baseType.IsAssignableFrom(item.PropertyType))
                {             
                    OBjectProperty property = new OBjectProperty((CommunicaBase)pro);
                    property.Name = item.GetLabDispContent();
                    this.children.Add(property);
                }
                else if (IsEnumable(item.PropertyType))
                {
                    OBjectProperty parent = null;
                    int i = 1;
                    foreach (var child in (System.Collections.IEnumerable)pro)
                    {
                        if (!baseType.IsAssignableFrom(child.GetType())) break;                        
                        string labName = item.GetLabDispContent();
                        if (parent == null) parent = new OBjectProperty { Name = labName };
                        OBjectProperty childrenObj = new OBjectProperty((CommunicaBase)child);
                        if (string.IsNullOrEmpty(childrenObj.Name)) childrenObj.Name = $"子项_{i}";
                        parent.children.Add(childrenObj);
                        i++;
                    }
                    if (null != parent)
                    {
                        children.Add(parent);
                    }
                }
            }
        }

        private bool IsEnumable(Type type)
        {
            Type[] arry=type.GetInterfaces();
            if (null == arry) return false;
           return arry.Any((tt)=> { return tt == typeof(System.Collections.IEnumerable); });
        }

        private static Type baseType = typeof(CommunicaBase);
    }
}
