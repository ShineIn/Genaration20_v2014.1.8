﻿using G4.Motion;
using Googo.Manul;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Googo.Manul
{
    /// <summary>
    /// WinRegister.xaml 的交互逻辑
    /// </summary>
    public partial class WinRegAxis : Window, INotifyPropertyChanged
    {
        public WinRegAxis()
        {
            InitializeComponent();
        }


        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        #endregion INotifyPropertyChanged

        public IEnumerable<EnumAxisType> AixsAllType
        {
            get { return Enum.GetValues(typeof(EnumAxisType)).OfType<EnumAxisType>(); }
        }

        #region 公有属性
        public AxisInfor AxisInfor
        {
            get { return axisInfor; }
            set { axisInfor = value; OnPropertyChanged("AxisInfor"); }
        }
        private AxisInfor axisInfor = new AxisInfor();
        #endregion 公有属性

        private void confirm_Click(object sender, RoutedEventArgs e)
        {
            if (null != axisInfor && (string.IsNullOrEmpty(axisInfor.Name) || string.IsNullOrEmpty(axisInfor.CustomName)))
            {
                MessageBox.Show("名称未输入"); return;
            }
            DialogResult = true;
            this.Close();
        }
    }
}
