﻿using G4.Motion;
using NPOI.SS.Formula.Functions;
using NPOI.SS.UserModel;
using NPOI.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using Googo.Manul.Views;

namespace Googo.Manul
{
    /// <summary>
    /// UCPosList.xaml 的交互逻辑
    /// </summary>
    public partial class UCPosList : UserControl, INotifyPropertyChanged
    {
        public UCPosList()
        {
            InitializeComponent();
        }

        public static DependencyProperty PositionListProperty = DependencyProperty.Register("PositionList", typeof(IList<PositionInfor>), typeof(UCPosList), new FrameworkPropertyMetadata(new ObservableCollection<PositionInfor>(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty XAxisNumProperty = DependencyProperty.Register("XAxisNum", typeof(EnumAxisType), typeof(UCPosList), new FrameworkPropertyMetadata(EnumAxisType.X0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty YAxisNumProperty = DependencyProperty.Register("YAxisNum", typeof(EnumAxisType), typeof(UCPosList), new FrameworkPropertyMetadata(EnumAxisType.Y0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty ZAxisNumProperty = DependencyProperty.Register("ZAxisNum", typeof(EnumAxisType), typeof(UCPosList), new FrameworkPropertyMetadata(EnumAxisType.Z0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        #region 公有属性
        public IList<PositionInfor> PositionList
        {
            get { return (IList<PositionInfor>)GetValue(PositionListProperty); }
            set { SetValue(PositionListProperty, value); }
        }

        public PositionInfor SelectedPos
        {
            get { return selectedPos; }
            set { selectedPos = value; OnPropertyChanged(); }
        }
        private PositionInfor selectedPos;

        public EnumAxisType XAxisNum
        {
            get { return (EnumAxisType)GetValue(XAxisNumProperty); }
            set { SetValue(XAxisNumProperty, value); }
        }

        public EnumAxisType YAxisNum
        {
            get { return (EnumAxisType)GetValue(YAxisNumProperty); }
            set { SetValue(YAxisNumProperty, value); }
        }

        public EnumAxisType ZAxisNum
        {
            get { return (EnumAxisType)GetValue(ZAxisNumProperty); }
            set { SetValue(ZAxisNumProperty, value); }
        }

        public string XYPreMoveZ
        {
            get { return $"{XAxisNum},{YAxisNum}先移动{ZAxisNum}后动"; }
        }

        public string XSingleMove
        {
            get { return $"{XAxisNum}移动"; }
        }

        public string YSingleMove
        {
            get { return $"{YAxisNum}移动"; }
        }

        public string ZSingleMove
        {
            get { return $"{ZAxisNum}移动"; }
        }

        public string ZPreMoveXY
        {
            get { return $"{ZAxisNum}先移动{XAxisNum},{YAxisNum}后动"; }
        }

        public Visibility CommandVisible
        {
            get { return commandVisible; }
            set { commandVisible=value; OnPropertyChanged(); }
        }
        private Visibility commandVisible = Visibility.Hidden;

        public bool IsNameEditable
        {
            get { return _isNameEditable; }
            set { _isNameEditable = value; OnPropertyChanged(); }
        }
        private bool _isNameEditable = false;
        #endregion 公有属性

        #region 私有函数
        private void Add_Excute(object sender, RoutedEventArgs e)
        {
            if (PositionList == null)
                PositionList = new ObservableCollection<PositionInfor>();
            PositionInfor pos = new PositionInfor { Name = $"位置_{PositionList.Count}" };
            if (MotionModel.Instance.IsInitialized)
            {
                double tempos = 0;
                MotionModel.Instance.GetPosition(XAxisNum, out tempos);
                pos.XPos = tempos;
                MotionModel.Instance.GetPosition(YAxisNum, out tempos);
                pos.YPos = tempos;
                MotionModel.Instance.GetPosition(ZAxisNum, out tempos);
                pos.ZPos = tempos;
            }
            PositionList.Add(pos);
            SelectedPos = pos;
        }


        private void Delete_Excute(object sender, RoutedEventArgs e)
        {
            if (selectedPos != null)
                PositionList.Remove(selectedPos);
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            MotionModel.Instance.StopMoving(XAxisNum);
            MotionModel.Instance.StopMoving(YAxisNum);
            MotionModel.Instance.StopMoving(ZAxisNum);
        }

        #endregion 私有函数

        private void UpDate_Click(object sender, RoutedEventArgs e)
        {
            if (selectedPos == null || !MotionModel.Instance.IsInitialized) return;
            int iret = MotionModel.Instance.GetPosition(XAxisNum, out double pos);
            if (0 != iret) return;
            selectedPos.XPos = pos;
            iret = MotionModel.Instance.GetPosition(YAxisNum, out pos);
            if (0 != iret) return;
            selectedPos.YPos = pos;
            iret = MotionModel.Instance.GetPosition(ZAxisNum, out pos);
            if (0 != iret) return;
            selectedPos.ZPos = pos;
        }

        private void XYPreMove_Click(object sender, RoutedEventArgs e)
        {
            if (selectedPos == null || !MotionModel.Instance.IsInitialized
                || (null != moveTask && !moveTask.IsCompleted)) return;
            EnumAxisType tempX = XAxisNum;
            EnumAxisType tempY = YAxisNum;
            EnumAxisType tempZ = ZAxisNum;
            if (XAxisNum != EnumAxisType.R1)
            {
                moveTask = Task.Run(() =>
                {
                    int iret = MotionModel.Instance.LineTo(tempX, tempY, selectedPos.XPos, selectedPos.YPos);
                    if (iret != 0) return;
                    MotionModel.Instance.MoveTo(tempZ, selectedPos.ZPos);
                });
            }
            else
            {
                moveTask = Task.Run(() =>
                {
                    int iret = MoveXYRPlatform(tempX, tempY,tempZ,selectedPos.XPos, selectedPos.YPos, selectedPos.ZPos,20,100);
                    if (iret != 0) return;
                });
            }
        }

        private int MoveXYRPlatform(G4.Motion.EnumAxisType R1, G4.Motion.EnumAxisType R2, G4.Motion.EnumAxisType R3, double XYR_XPos, double XYR_YPos, double XYR_RPos, double vel, double acc)
        {
            var XYRet = Task.WhenAll(Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(R1, XYR_XPos, vel, acc); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(R2, XYR_XPos, vel, acc); })
                  , Task.Run(() => { return G4.Motion.MotionModel.Instance.MoveTo(R3, XYR_YPos, vel, acc); }));
            var resultXY = XYRet.Result;

            var RRet = Task.WhenAll(Task.Run(() => { return XXYRotateTo(XYR_RPos, vel, acc); }));
            var resultR = RRet.Result;
            if (resultXY.All(p => p == 0) && resultR.All(p => p == 0))
            {
                return 0;
            }
            else { return 1; }
        }

        public int XXYRotateTo(double deg, double vel, double acc)
        {
            const double R = 91.924;
            const double Y = 225;
            const double X1 = 315;
            const double X2 = 135;

            double dx1 = R * Math.Cos((X1 + deg) * Math.PI / 180) - R * Math.Cos(X1 * Math.PI / 180);
            double dx2 = R * Math.Cos((X2 + deg) * Math.PI / 180) - R * Math.Cos(X2 * Math.PI / 180);
            double dy = R * Math.Sin((Y + deg) * Math.PI / 180) - R * Math.Sin(Y * Math.PI / 180);

            var XYRet = Task.WhenAll(Task.Run(() => { return G4.Motion.MotionModel.Instance.Move(G4.Motion.EnumAxisType.R1, dx1, vel, acc); })
                 , Task.Run(() => { return G4.Motion.MotionModel.Instance.Move(G4.Motion.EnumAxisType.R2, dx2, vel, acc); })
                  , Task.Run(() => { return G4.Motion.MotionModel.Instance.Move(G4.Motion.EnumAxisType.R3, dy, vel, acc); }));
            var result = XYRet.Result;
            if (result.All(p => p == 0))
            {
                return 0;
            }
            else { return 1; }
        }

        private void Jog_Click(object sender, RoutedEventArgs e)
        {
            if (XAxisNum == EnumAxisType.Y3)
            {
                UCYZZStage stage = new UCYZZStage()
                {
                    Y3AxisNum = XAxisNum,
                    Z3AxisNum = YAxisNum,
                    Z4AxisNum = ZAxisNum,
                    LeftBtnImageSource = new BitmapImage(new Uri("../Images/Left.png", UriKind.Relative)),
                    RightBtnImageSource = new BitmapImage(new Uri("../Images/Right.png", UriKind.Relative))
                };
                Window window = new Window() { Content = stage, Width = 350.0, Height = 350.0 };
                window.WindowStyle = WindowStyle.ToolWindow;
                window.Topmost = true;
                window.Title = $"{XAxisNum},{YAxisNum},{ZAxisNum} 轴手动调试";
                window.Show();
            }
            else if (XAxisNum == EnumAxisType.R1)
            {
                UCYZZStage stage = new UCYZZStage()
                {
                    Y3AxisNum = XAxisNum,
                    Z3AxisNum = YAxisNum,
                    Z4AxisNum = ZAxisNum,
                    LeftBtnImageSource = new BitmapImage(new Uri("../Images/rotatecw.png", UriKind.Relative)),
                    RightBtnImageSource = new BitmapImage(new Uri("../Images/rotateccw.png", UriKind.Relative))
                };
                stage.lab_X.Content = "R1";
                stage.lab_Y.Content = "R2";
                stage.lab_Z.Content = "R3";
                Window window = new Window() { Content = stage, Width = 350.0, Height = 350.0 };
                window.WindowStyle = WindowStyle.ToolWindow;
                window.Topmost = true;
                window.Title = $"{XAxisNum},{YAxisNum},{ZAxisNum} 轴手动调试";
                window.Show();
            }
            else
            {
                UCXYZStage stage = new UCXYZStage() { XAxisNum = XAxisNum, YAxisNum = YAxisNum, ZAxisNum = ZAxisNum,
                    XYZLeftBtnImageSource = new BitmapImage(new Uri("../Images/Left.png", UriKind.Relative)),
                    XYZRightBtnImageSource = new BitmapImage(new Uri("../Images/Right.png", UriKind.Relative))
                };
                Window window = new Window() { Content = stage, Width = 350.0, Height = 350.0 };
                window.WindowStyle = WindowStyle.ToolWindow;
                window.Topmost = true;
                window.Title = $"{XAxisNum},{YAxisNum},{ZAxisNum} 轴手动调试";
                window.Show();
            }
        }
        private Task moveTask = null;

        private void ZPreMove_Click(object sender, RoutedEventArgs e)
        {
            if (selectedPos == null || !MotionModel.Instance.IsInitialized
         || (null != moveTask && !moveTask.IsCompleted)) return;
            EnumAxisType tempX = XAxisNum;
            EnumAxisType tempY = YAxisNum;
            EnumAxisType tempZ = ZAxisNum;
            moveTask = Task.Run(() =>
            {
                int iret = MotionModel.Instance.MoveTo(tempZ, selectedPos.ZPos);
                if (iret != 0) return;
                MotionModel.Instance.LineTo(tempX, tempY, selectedPos.XPos, selectedPos.YPos);
            });
        }

        private void ZMove_Click(object sender, RoutedEventArgs e)
        {
            if (selectedPos == null || !MotionModel.Instance.IsInitialized
         || (null != moveTask && !moveTask.IsCompleted)) return;        
            EnumAxisType tempZ = ZAxisNum;
            moveTask = Task.Run(() =>
            {
                int iret = MotionModel.Instance.MoveTo(tempZ, selectedPos.ZPos);
            });
        }

        private void XMove_Click(object sender, RoutedEventArgs e)
        {
            if (selectedPos == null || !MotionModel.Instance.IsInitialized
         || (null != moveTask && !moveTask.IsCompleted)) return;
            EnumAxisType tempX = XAxisNum;
            moveTask = Task.Run(() =>
            {
                int iret = MotionModel.Instance.MoveTo(tempX, selectedPos.XPos);
            });
        }

        private void YMove_Click(object sender, RoutedEventArgs e)
        {
            if (selectedPos == null || !MotionModel.Instance.IsInitialized
         || (null != moveTask && !moveTask.IsCompleted)) return;
            EnumAxisType tempY = YAxisNum;
            moveTask = Task.Run(() =>
            {
                int iret = MotionModel.Instance.MoveTo(tempY, selectedPos.YPos);
            });
        }

        private void MesEdit_Click(object sender, RoutedEventArgs e)
        {       
            if (null == selectedPos || selectedPos.ExtenCommand == null) return;
            Window window = new Window();
            UCMesData mesData = new UCMesData { SubCommuniObject= selectedPos.ExtenCommand };
            window.Content = mesData;
            window.Show();
        }

        private void UpData_Click(object sender, RoutedEventArgs e)
        {
            if (null == selectedPos || selectedPos.Result == null) return;
            Window window = new Window();
            UCMesData mesData = new UCMesData { SubCommuniObject = selectedPos.Result };
            window.Content = mesData;
            window.Show();
        }
    }

    public class IndexConverter : MarkupExtension, IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            DependencyObject item = (DependencyObject)values[0];
            if (null == item) return null;
            ItemsControl listView = ItemsControl.ItemsControlFromItemContainer(item);
            if (null == listView) return null;
            int index = listView.ItemContainerGenerator.IndexFromContainer(item) + 1;
            return index.ToString();
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
