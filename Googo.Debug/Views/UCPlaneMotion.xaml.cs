﻿using G4.Motion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Googo.Manul
{
    /// <summary>
    /// UCPlaneMotion.xaml 的交互逻辑
    /// </summary>
    public partial class UCPlaneMotion : UserControl
    {
        public UCPlaneMotion()
        {
            InitializeComponent();
        }

        public static DependencyProperty XAxisNumProperty = DependencyProperty.Register("XAxisNum", typeof(EnumAxisType), typeof(UCPlaneMotion), new PropertyMetadata(EnumAxisType.X));
        public static DependencyProperty YAxisNumProperty = DependencyProperty.Register("YAxisNum", typeof(EnumAxisType), typeof(UCPlaneMotion), new PropertyMetadata(EnumAxisType.Y));
        public static DependencyProperty AxisVelProperty = DependencyProperty.Register("AxisVel", typeof(double), typeof(UCPlaneMotion), new PropertyMetadata(100.0));
        public static DependencyProperty AxisAccProperty = DependencyProperty.Register("AxisAcc", typeof(double), typeof(UCPlaneMotion), new PropertyMetadata(50.0));
        public static DependencyProperty IsStepMoveProperty = DependencyProperty.Register("IsStepMove", typeof(bool), typeof(UCPlaneMotion), new PropertyMetadata(false));
        public static DependencyProperty StepDistanceProperty = DependencyProperty.Register("StepDistance", typeof(double), typeof(UCPlaneMotion), new PropertyMetadata(1.5));

        public EnumAxisType XAxisNum
        {
            get { return (EnumAxisType)GetValue(XAxisNumProperty); }
            set { SetValue(XAxisNumProperty, value); }
        }

        public EnumAxisType YAxisNum
        {
            get { return (EnumAxisType)GetValue(YAxisNumProperty); }
            set { SetValue(YAxisNumProperty, value); }
        }

        public double AxisVel
        {
            get { return (double)GetValue(AxisVelProperty); }
            set { SetValue(AxisVelProperty, value); }
        }

        public double AxisAcc
        {
            get { return (double)GetValue(AxisAccProperty); }
            set { SetValue(AxisAccProperty, value); }
        }

        public bool IsStepMove
        {
            get { return (bool)GetValue(IsStepMoveProperty); }
            set { SetValue(IsStepMoveProperty, value); }
        }

        public double StepDistance
        {
            get { return (double)GetValue(StepDistanceProperty); }
            set { SetValue(StepDistanceProperty, value); }
        }

        private void X_Neg_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!MotionModel.Instance.IsInitialized) return;
            if (IsStepMove)
                MotionModel.Instance.Move(XAxisNum, -StepDistance);
            else
                MotionModel.Instance.StartMoving(XAxisNum, -AxisVel, AxisAcc);   
            X_IsMouseDown = true;
        }

        private void X_Button_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if ((!X_IsMouseDown || IsStepMove) && !MotionModel.Instance.IsInitialized) return;
            MotionModel.Instance.StopMoving(XAxisNum);
        }

        private void X_Button_MouseLeave(object sender, MouseEventArgs e)
        {
            if ((!X_IsMouseDown || IsStepMove) && !MotionModel.Instance.IsInitialized) return;
            MotionModel.Instance.StopMoving(XAxisNum);
        }


        private void X_Pos_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!MotionModel.Instance.IsInitialized) return;
            if (IsStepMove)
                MotionModel.Instance.Move(XAxisNum, StepDistance);
            else
                MotionModel.Instance.StartMoving(XAxisNum, AxisVel, AxisAcc);
            X_IsMouseDown = true;
        }

        private void Y_Neg_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!MotionModel.Instance.IsInitialized) return;
            if (IsStepMove)
                MotionModel.Instance.Move(YAxisNum, -StepDistance);
            else
                MotionModel.Instance.StartMoving(YAxisNum, -AxisVel, AxisAcc);
            Y_IsMouseDown = true;
        }

        private void Y_Button_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            if ((!Y_IsMouseDown|| IsStepMove) && !MotionModel.Instance.IsInitialized) return;
            MotionModel.Instance.StopMoving(YAxisNum);
        }

        private void Y_Button_MouseLeave(object sender, MouseEventArgs e)
        {
            if ((!Y_IsMouseDown|| IsStepMove) && !MotionModel.Instance.IsInitialized) return;
            MotionModel.Instance.StopMoving(YAxisNum);
        }


        private void Y_Pos_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (!MotionModel.Instance.IsInitialized) return;
            if (IsStepMove)
                MotionModel.Instance.Move(YAxisNum, StepDistance);
            else
                MotionModel.Instance.StartMoving(YAxisNum, AxisVel, AxisAcc);
            Y_IsMouseDown = true;
        }

        private bool Y_IsMouseDown = false;
        private bool X_IsMouseDown = false;
    }
}
