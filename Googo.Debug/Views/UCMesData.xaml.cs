﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Googo.Manul.Views
{
    /// <summary>
    /// Interaction logic for UCMesData.xaml
    /// </summary>
    public partial class UCMesData : UserControl, INotifyPropertyChanged
    {
        public UCMesData()
        {
            InitializeComponent();
        }

        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        public static DependencyProperty SubCommuniObjectProperty = DependencyProperty.Register("SubCommuniObject", typeof(CommunicaBase),
typeof(UCMesData), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));


        public CommunicaBase SubCommuniObject
        {
            get { return (CommunicaBase)GetValue(SubCommuniObjectProperty); }
            set { SetValue(SubCommuniObjectProperty, value); }
        }

        public IList<ParameterItem> ParameterItems
        {
            get { return parameterItems; }
            set { parameterItems = value;OnPropertyChanged(); }
        }
        private IList<ParameterItem> parameterItems = null;

        private static void DepenProChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            UCMesData uC = d as UCMesData;
            if (null == uC) return;
            CommunicaBase communica = e.NewValue as CommunicaBase;
            uC.pro.SelectedObject = communica;
            if (communica == null) return;
            else if (communica is SendData send)
            {
                uC.ParameterItems = send.ParameterList;
            }
            else if (communica is ReceiveData rec)
            {
                uC.ParameterItems = rec.ResultList;
            }
        }
    }
}
