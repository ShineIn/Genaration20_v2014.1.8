﻿using G4.Motion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Googo.Manul
{
    /// <summary>
    /// UCAxisPosList.xaml 的交互逻辑
    /// </summary>
    public partial class UCAxisPosList : UserControl
    {
        public UCAxisPosList()
        {
            InitializeComponent();
        }

        public static DependencyProperty AxisListProperty = DependencyProperty.Register("AxisList", typeof(IList<AxisPosStatus>), typeof(UCAxisPosList), new PropertyMetadata(new List<AxisPosStatus>()));

        public IList<AxisPosStatus> AxisList
        {
            get { return (IList<AxisPosStatus>)GetValue(AxisListProperty); }
            set { SetValue(AxisListProperty, value); }
        }
    }

    public class AxisPosStatus : PropertyChangedBase
    {
        public EnumAxisType AxisName
        {
            get { return axisName; }
            set { axisName = value; OnPropertyChanged(); }
        }
        private EnumAxisType axisName = EnumAxisType.X;

        public double Position
        {
            get { return position; }
            set { position = value; OnPropertyChanged(); }
        }
        private double position = 0;

        public bool IsMoving
        {
            get { return _isMoving; }
            set { _isMoving = value; OnPropertyChanged(); }
        }
        private bool _isMoving = false;
    }
}
