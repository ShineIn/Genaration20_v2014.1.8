﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using outputStatus = System.Collections.Generic.KeyValuePair<int, bool>;
using System.Globalization;
using G4.Motion;

namespace Googo.Manul
{
    /// <summary>
    /// UCFilterMana.xaml 的交互逻辑
    /// </summary>
    public partial class UCFilterMana : UserControl, INotifyPropertyChanged
    {
        public UCFilterMana()
        {
            InitializeComponent();
            CommandBindings.Add(new CommandBinding(AddFilter, AddFilter_Excute, AddFilter_CanExcute));
            CommandBindings.Add(new CommandBinding(DeleteFilter, DeleteFilter_Excute, DeleteFilter_CanExcute));
            CommandBindings.Add(new CommandBinding(ModifyFilter, ModifyFilter_Excute, DeleteFilter_CanExcute));
            CommandBindings.Add(new CommandBinding(AddCondition, AddCondition_Excute, AddCondition_CanExcute));
            CommandBindings.Add(new CommandBinding(DeleteCondition, DeleteCondition_Excute, DeleteCondition_CanExcute));
            CommandBindings.Add(new CommandBinding(AddConditionToChild, AddConditionToChild_Excute, AddConditionToChild_CanExcute));
        }

        public static RoutedCommand AddFilter = new RoutedCommand();
        public static RoutedCommand DeleteFilter = new RoutedCommand();
        public static RoutedCommand ModifyFilter = new RoutedCommand();
        public static RoutedCommand AddCondition = new RoutedCommand();
        public static RoutedCommand DeleteCondition = new RoutedCommand();
        public static RoutedCommand AddConditionToChild = new RoutedCommand();

        public static DependencyProperty FilterListProperty = DependencyProperty.Register("FilterList", typeof(ObservableCollection<FilterInfor>), typeof(UCFilterMana), new FrameworkPropertyMetadata(new ObservableCollection<FilterInfor>(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public ObservableCollection<FilterInfor> FilterList
        {
            get { return (ObservableCollection<FilterInfor>)GetValue(FilterListProperty); }
            set { SetValue(FilterListProperty, value); }
        }

        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        #region 公有属性
        public FilterInfor SelectedFilter
        {
            get { return selectedFilter; }
            set { selectedFilter = value; OnPropertyChanged(); }
        }
        private FilterInfor selectedFilter = null;

        public ICondition SelectedCondition
        {
            get { return selectedCondition; }
            set { selectedCondition = value; OnPropertyChanged(); }
        }
        private ICondition selectedCondition = null;
        #endregion 公有属性

        public static IEnumerable<bool> BoolSource
        {
            get { return new bool[] { true, false }; }
        }

        public static IEnumerable<DeviceType> DeviceSource
        {
            get { return new DeviceType[] { DeviceType.Input, DeviceType.Output }; }
        }

        public static IEnumerable<EnumAxisType> AxisSource
        {
            get { return Enum.GetValues(typeof(EnumAxisType)).OfType<EnumAxisType>(); }
        }

        public static IEnumerable<Type> DataTypeSource
        {
            get { return new Type[] { typeof(int), typeof(string), typeof(float), typeof(short), typeof(uint) }; }
        }


        private void AddFilter_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            WinFilter winFilter = new WinFilter(new AxisFilter());
            if (true == winFilter.ShowDialog())
            {
                FilterList.Add(winFilter.FilterInfor);
                selectedFilter = winFilter.FilterInfor;
            }
        }

        private void AddFilter_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = FilterList != null;
        }

        private void DeleteFilter_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            FilterList.Remove(selectedFilter);
        }

        private void ModifyFilter_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            WinFilter winFilter = new WinFilter(selectedFilter, true);
            winFilter.ShowDialog();
        }

        private void DeleteFilter_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = selectedFilter != null;
        }

        private void DeleteCondition_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            RemoveSuccess(selectedFilter.Conditions, selectedCondition);
        }

        private bool RemoveSuccess(IList<ICondition> conditions, ICondition selectItem)
        {
            if (null == conditions || selectItem == null) return false;
            if (conditions.Contains(selectItem))
            {
                conditions.Remove(selectItem);
                return true;
            }
            foreach (ConditionList item in conditions.OfType<ConditionList>())
            {
                if (item != null)
                {
                    if (RemoveSuccess(item.Conditions, selectItem)) return true;
                }
            }
            return false;
        }

        private void AddConditionToChild_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            WinCondition winCondition = new WinCondition(new IOCondition());
            if (true == winCondition.ShowDialog())
            {
                winCondition.ConditionInfor.ParentFilterType = selectedFilter.GetType();
                ((ConditionList)selectedCondition).Conditions.Add(winCondition.ConditionInfor);
                SelectedCondition = winCondition.ConditionInfor;

            }
        }

        private void AddConditionToChild_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = selectedFilter != null && selectedCondition != null && selectedCondition.GetType() == typeof(ConditionList);
        }

        private void DeleteCondition_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = selectedFilter != null && selectedCondition != null;
        }


        private void AddCondition_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            WinCondition winCondition = new WinCondition(new IOCondition());
            if (true == winCondition.ShowDialog())
            {
                winCondition.ConditionInfor.ParentFilterType = selectedFilter.GetType();
                selectedFilter.Conditions.Add(winCondition.ConditionInfor);
                SelectedCondition = winCondition.ConditionInfor;
            }
        }

        private void AddCondition_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = selectedFilter != null;
        }

        private void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            SelectedCondition = (ICondition)condi.SelectedItem;
        }


        private void SaveToFlash_Click(object sender, RoutedEventArgs e)
        {
            if (FilterList == null || FilterList.Count == 0) return;
            IEnumerable<OutputFilter> oufilter = FilterList.OfType<OutputFilter>();
            IEnumerable<AxisFilter> axisfilter = FilterList.OfType<AxisFilter>();
            Dictionary<outputStatus, IEnumerable<Func<bool>>> oufiDic = new Dictionary<outputStatus, IEnumerable<Func<bool>>>();
            Dictionary<EnumAxisType, IEnumerable<Func<double, bool>>> axisfiDic = new Dictionary<EnumAxisType, IEnumerable<Func<double, bool>>>();
            if (null != oufilter)
            {
                foreach (var item in oufilter)
                {
                    if (null == item) continue;
                    var pair = item.GetOutPutFilter();
                    oufiDic[pair.Key] = pair.Value;
                }
            }
            if (null != axisfilter)
            {
                foreach (var item in axisfilter)
                {
                    if (null == item) continue;
                    var pair = item.GetAxisFilter();
                    axisfiDic[pair.Key] = pair.Value;
                }
            }
            G4.Motion.MotionModel.Instance.AddCondition(oufiDic, axisfiDic);
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            string dir = System.IO.Path.GetDirectoryName(GloMotionParame.ConditionPath);
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            FileStream fs = new FileStream(GloMotionParame.ConditionPath, FileMode.OpenOrCreate);
            BinaryFormatter binary = new BinaryFormatter();
            binary.Context = new StreamingContext(StreamingContextStates.Persistence | StreamingContextStates.File);
            binary.Serialize(fs, FilterList);
            fs.Flush();
            fs.Close();
            fs.Dispose();
        }

    }

    public class TypeVisible : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (null == value || !(value is Type) || null == parameter) return Visibility.Collapsed;
            Type type = (Type)value;
            return type.Name == parameter.ToString() ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class IOSourceConvert : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (null == value || !(value is DeviceType deviceType)) return null;
            if (deviceType == DeviceType.Input) return GloMotionParame.InputList;
            else if (deviceType == DeviceType.Output) return GloMotionParame.OutputList;
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class IOSelectConvert : MarkupExtension, IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (null == values || values.Length != 2 || DependencyProperty.UnsetValue == values[0]
                || DependencyProperty.UnsetValue == values[1] || null == values[0] || null == values[1]) return null;
            DeviceType deviceType = (DeviceType)values[0];
            device = deviceType;
            int index = (int)values[1];
            IOCollection oStatuses = null;
            if (deviceType == DeviceType.Input) oStatuses = GloMotionParame.InputList;
            else if (deviceType == DeviceType.Output) oStatuses = GloMotionParame.OutputList;
            if (oStatuses == null) return null;
            return oStatuses.FirstOrDefault((temp) => { return temp.BitIndex == index; });
        }


        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            if (DependencyProperty.UnsetValue == value || null == value) return null;
            IOStatus oStatus = value as IOStatus;
            if (oStatus != null)
            {
                return new object[] { device, oStatus.BitIndex };
            }
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        private DeviceType device;
    }
}
