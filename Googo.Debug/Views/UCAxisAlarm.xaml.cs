﻿using G4.Motion;
using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using Microsoft.Win32;

namespace Googo.Manul
{
    /// <summary>
    /// UCAxisAlarm.xaml 的交互逻辑
    /// </summary>
    public partial class UCAxisAlarm : UserControl, INotifyPropertyChanged
    {
        public UCAxisAlarm()
        {
            InitializeComponent();
            CommandBindings.Add(new CommandBinding(AddAxis, AddAxis_Excute, AddAxis_CanExcute));
            CommandBindings.Add(new CommandBinding(DeleteAxis, DeleteAxis_Excute, DeleteAxis_CanExcute));
            CommandBindings.Add(new CommandBinding(AddAlarmItem, AddAlarm_Excute, AddAlarm_CanExcute));
            CommandBindings.Add(new CommandBinding(DeleteAlarm, DeleteAlarm_Excute, DeleteAlarm_CanExcute));    
        }

        public static RoutedCommand AddAxis = new RoutedCommand();
        public static RoutedCommand DeleteAxis = new RoutedCommand();
        public static RoutedCommand AddAlarmItem = new RoutedCommand();
        public static RoutedCommand DeleteAlarm = new RoutedCommand();

        public static DependencyProperty AxisAlarmListProperty = DependencyProperty.Register("AxisAlarmList", typeof(IList<AxisAlarm>), typeof(UCAxisAlarm), new FrameworkPropertyMetadata(new ObservableCollection<AxisAlarm>(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public IList<AxisAlarm> AxisAlarmList
        {
            get { return (IList<AxisAlarm>)GetValue(AxisAlarmListProperty); }
            set { SetValue(AxisAlarmListProperty, value); }
        }

        public AxisAlarm SelectedAxis
        {
            get { return selectedAxis; }
            set { selectedAxis=value; OnPropertyChanged(); }
        }
        private AxisAlarm selectedAxis;

        public UpPLCAlarm SelectedAlarm
        {
            get { return selectedAlarm; }
            set { selectedAlarm = value; OnPropertyChanged(); }
        }
        private UpPLCAlarm selectedAlarm;

        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        private void AddAxis_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            AxisAlarmList.Add(new AxisAlarm { AxisNum=EnumAxisType.X});
        }

        private void AddAxis_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = AxisAlarmList != null;
        }

        private void DeleteAxis_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            AxisAlarmList.Remove(selectedAxis);
        }

        private void DeleteAxis_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = selectedAxis != null;
        }

        private void AddAlarm_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            selectedAxis.PLCAlarms.Add(new UpPLCAlarm { AlarmType=AlarmTYpe.Alarm});
        }

        private void AddAlarm_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = selectedAxis != null&& selectedAxis.PLCAlarms!=null;
        }

        private void DeleteAlarm_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            selectedAxis.PLCAlarms.Remove(selectedAlarm);
        }

        private void DeleteAlarm_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = selectedAlarm != null ;
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            SaveAlarm(GloMotionParame.AlarmPath,GloMotionParame.AxisStatusList);
        }

        internal void SaveAlarm(string filePath, ObservableCollection<AxisAlarm> axisAlarms)
        {
            string dir = System.IO.Path.GetDirectoryName(filePath);
            if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
            XmlSerializer xs = new XmlSerializer(typeof(ObservableCollection<AxisAlarm>));
            FileStream fs = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.Write);
            xs.Serialize(fs, axisAlarms);
            fs.Close();
        }

        private void Open_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = false;
            ofd.Title = "导入报警文件";
            ofd.Filter = "(*.xml)|*.xml";
            if (true == ofd.ShowDialog())
            {
                GloMotionParame.LoadAlarm(ofd.FileName);
            }
        }
    }
}
