﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;

namespace Googo.Manul
{
    /// <summary>
    /// Interaction logic for UCRunError.xaml
    /// </summary>
    public partial class UCRunError : UserControl, INotifyPropertyChanged
    {
        public UCRunError()
        {
            InitializeComponent();
            CommandBindings.Add(new CommandBinding(AddAddress, AddDBError_Excute, AddDBError_CanExcute));
            CommandBindings.Add(new CommandBinding(DeleteAddress, DeleteFilter_Excute, DeleteFilter_CanExcute));
            CommandBindings.Add(new CommandBinding(AddCode, AddCondition_Excute, AddCondition_CanExcute));
            CommandBindings.Add(new CommandBinding(DeleteCode, DeleteCondition_Excute, DeleteCondition_CanExcute));
        }

        public static RoutedCommand AddAddress = new RoutedCommand();
        public static RoutedCommand DeleteAddress = new RoutedCommand();
        public static RoutedCommand AddCode = new RoutedCommand();
        public static RoutedCommand DeleteCode = new RoutedCommand();

        public static DependencyProperty ErrorListProperty = DependencyProperty.Register("ErrorList", typeof(IList<StageErrorInfor>), typeof(UCRunError), new FrameworkPropertyMetadata(new ObservableCollection<StageErrorInfor>(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public IList<StageErrorInfor> ErrorList
        {
            get { return (IList<StageErrorInfor>)GetValue(ErrorListProperty); }
            set { SetValue(ErrorListProperty, value); }
        }

        public StageErrorInfor SelectedAddre
        {
            get { return selectedAddre; }
            set { selectedAddre = value; OnPropertyChanged(); }
        }
        private StageErrorInfor selectedAddre = null;

        public RunErrorInfor SelectedCode
        {
            get { return selectedCode; }
            set { selectedCode = value; OnPropertyChanged(); }
        }
        private RunErrorInfor selectedCode = null;

        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged


        private void AddDBError_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            ErrorList.Add(new StageErrorInfor());
        }

        private void AddDBError_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = ErrorList != null;
        }

        private void DeleteFilter_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            ErrorList.Remove(selectedAddre);
        }


        private void DeleteFilter_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = selectedAddre != null;
        }

        private void DeleteCondition_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            selectedAddre.AlarmCodeList.Remove(selectedCode);
        }

        private void DeleteCondition_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = selectedCode != null;
        }


        private void AddCondition_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            selectedAddre.AlarmCodeList.Add(new RunErrorInfor());
        }

        private void AddCondition_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = selectedAddre != null;
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            string dir = System.IO.Path.GetDirectoryName(GloMotionParame.RunErrorPath);
            if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
            XmlSerializer xs = new XmlSerializer(typeof(ObservableCollection<StageErrorInfor>));
            FileStream fs = new FileStream(GloMotionParame.RunErrorPath, FileMode.OpenOrCreate, FileAccess.Write);
            xs.Serialize(fs, ErrorList);
            fs.Close();
        }
    }
}
