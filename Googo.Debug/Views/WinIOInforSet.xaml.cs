﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Googo.Manul
{
    /// <summary>
    /// WinIOInforSet.xaml 的交互逻辑
    /// </summary>
    public partial class WinIOInforSet : Window
    {
        public WinIOInforSet()
        {
            InitializeComponent();
        }

        public IOStatus Model
        {
            get { return model; }
            set 
            {
                if (value != null)
                {
                    model = value;
                    DataContext = value;
                }
            }
        }
        private IOStatus model = null;

        private void Confirm_Click(object sender, RoutedEventArgs e)
        {
            if (model != null && (string.IsNullOrEmpty(model.Name) || string.IsNullOrEmpty(model.BitDescri)))
            {
                MessageBox.Show("名称未输入"); return;
            }
            this.DialogResult = true;
            this.Close();
        }

        private void Cancel_Click_1(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
