﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using G4.Motion;
using System.Reflection;
using NPOI;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using Microsoft.Win32;
using NPOI.Util;
using Googo.Manul;

namespace Googo.Manul
{
    /// <summary>
    /// UCIOManager.xaml 的交互逻辑
    /// </summary>
    public partial class UCIOManager : UserControl
    {
        public UCIOManager()
        {
            InitializeComponent();
            Loaded += UCIOManager_Loaded;
        }

        public static DependencyProperty InputListProperty = DependencyProperty.Register("InputList", typeof(IList<IOStatus>),
    typeof(UCIOManager), new FrameworkPropertyMetadata(new IOCollection(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static DependencyProperty OutputListProperty = DependencyProperty.Register("OutputList", typeof(IList<IOStatus>),
typeof(UCIOManager), new FrameworkPropertyMetadata(new IOCollection(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        private void UCIOManager_Loaded(object sender, RoutedEventArgs e)
        {
            StartScan();
        }

        private void StartScan()
        {
            Interupt = false;
            if (scanTask == null || scanTask.IsCompleted)
            {
                IList<IOStatus> inputList = InputList;
                IList<IOStatus> outputList = OutputList;
                scanTask = Task.Run(() =>
                {
                    while (MotionModel.Instance.IsInitialized&&!Interupt)
                    {
                        if (null != inputList)
                        {
                            foreach (var item in inputList)
                            {
                                item?.GetValue();
                            }
                        }
                        if (null != outputList)
                        {
                            foreach (var item in outputList)
                            {
                                item?.GetValue();
                            }
                        }
                        System.Threading.Thread.Sleep(100);
                    }
                });
            }
        }

        public IList<IOStatus> InputList
        {
            get { return (IList<IOStatus>)GetValue(InputListProperty); }
            set { SetValue(InputListProperty, value); }
        }

        public IList<IOStatus> OutputList
        {
            get { return (IList<IOStatus>)GetValue(OutputListProperty); }
            set { SetValue(OutputListProperty, value); }
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {      
            XSSFWorkbook workbook2007 = new XSSFWorkbook();  //新建xlsx工作簿
            FileStream file2007 = new FileStream(GloMotionParame.FilePath, FileMode.OpenOrCreate);
            ISheet sheet = workbook2007.CreateSheet("Input");
            WriteSheet(InputList, sheet);
            sheet = workbook2007.CreateSheet("Output");
            WriteSheet(OutputList, sheet);
            workbook2007.Write(file2007);
            file2007.Close();
            workbook2007.Close();
        }

        private int WriteSheet(IList<IOStatus> statuses, ISheet sheet)
        {
            PropertyInfo[] infos = typeof(IOStatus).GetProperties();
            if (null == infos || infos.Length == 0) return 0;
            if (null == statuses || statuses.Count == 0 || sheet == null) return -1;
            IRow row = sheet.CreateRow(0);
            for (int i = 0; i < infos.Count(); i++)
            {
                row.CreateCell(i).SetCellValue(infos[i].Name);
            }
            for (int i = 0; i < statuses.Count; i++)
            {
                row = sheet.CreateRow(i + 1);
                for (int k = 0; k < infos.Count(); k++)
                {
                    ICell cell = row.CreateCell(k);
                    object setvalue = infos[k].GetValue(statuses[i]);
                    if (setvalue is double)
                        cell.SetCellValue((double)setvalue);
                    else if (setvalue is int)
                        cell.SetCellValue((int)setvalue);
                    else if (setvalue is string)
                        cell.SetCellValue((string)setvalue);
                    else if (setvalue is bool)
                        cell.SetCellValue((bool)setvalue);
                }
            }
            return 0;
        }


        private void Open_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.Multiselect = false;
            fd.Title = "选择导入表格";
            fd.Filter = $"(*.xls)|*.xls|(*.xlsx)|*.xlsx";
            Interupt = true;
            if (fd.ShowDialog() == true)
            {
                GloMotionParame.LoadExcel(fd.FileName);
            }
            StartScan();
        }
        private Task scanTask;
        private bool Interupt = false;
    }

    [Serializable]
    public class IOCollection : ObservableCollection<IOStatus>
    {
        public bool GetValue(string name)
        {
            IOStatus status = Items.FirstOrDefault((item) => { return item.Name.Trim() == name.Trim(); });
            if (null == status)
                throw new Exception($"Unfound {name} IO module");
            return status.GetValue();
        }

        public void SetValue(string name, bool bvalue)
        {
            IOStatus status = Items.FirstOrDefault((item) => { return item.Name.Trim() == name.Trim(); });
            if (null == status)
                throw new Exception($"Unfound {name} IO module");
            if (status.IsOutModule) status.SetValue(bvalue);
        }
    }
}

