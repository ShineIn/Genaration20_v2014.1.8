﻿using G4.Motion;
using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Googo.Manul
{
    /// <summary>
    /// UCXYZStage.xaml 的交互逻辑
    /// </summary>
    public partial class UCXYZStage : UserControl, INotifyPropertyChanged
    {
        public UCXYZStage()
        {
            InitializeComponent();
            Loaded += UCXYZStage_Loaded;
            Unloaded += UCXYZStage_Unloaded;
        }

        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        public static DependencyProperty XAxisNumProperty = DependencyProperty.Register("XAxisNum", typeof(EnumAxisType), typeof(UCXYZStage), new PropertyMetadata(EnumAxisType.X));
        public static DependencyProperty YAxisNumProperty = DependencyProperty.Register("YAxisNum", typeof(EnumAxisType), typeof(UCXYZStage), new PropertyMetadata(EnumAxisType.Y));
        public static DependencyProperty ZAxisNumProperty = DependencyProperty.Register("ZAxisNum", typeof(EnumAxisType), typeof(UCXYZStage), new PropertyMetadata(EnumAxisType.Z));
        public static DependencyProperty AxisAccProperty = DependencyProperty.Register("AxisAcc", typeof(double), typeof(UCXYZStage), new PropertyMetadata(100.0));
        public static DependencyProperty XYZLeftBtnImageSourceAccProperty = DependencyProperty.Register("XYZLeftBtnImageSource", typeof(ImageSource), typeof(UCXYZStage), new PropertyMetadata(new BitmapImage(new Uri("../Images/Left.png", UriKind.Relative))));
        public static DependencyProperty XYZRightBtnImageSourceAccProperty = DependencyProperty.Register("XYZRightBtnImageSource", typeof(ImageSource), typeof(UCXYZStage), new PropertyMetadata(new BitmapImage(new Uri("../Images/Right.png", UriKind.Relative))));
        public EnumAxisType XAxisNum
        {
            get { return (EnumAxisType)GetValue(XAxisNumProperty); }
            set { SetValue(XAxisNumProperty, value); }
        }

        public EnumAxisType YAxisNum
        {
            get { return (EnumAxisType)GetValue(YAxisNumProperty); }
            set { SetValue(YAxisNumProperty, value); }
        }

        public EnumAxisType ZAxisNum
        {
            get { return (EnumAxisType)GetValue(ZAxisNumProperty); }
            set { SetValue(ZAxisNumProperty, value); }
        }
        public ImageSource XYZLeftBtnImageSource
        {
            get { return (ImageSource)GetValue(XYZLeftBtnImageSourceAccProperty); }
            set { SetValue(XYZLeftBtnImageSourceAccProperty, value); }
        }

        public ImageSource XYZRightBtnImageSource
        {
            get { return (ImageSource)GetValue(XYZRightBtnImageSourceAccProperty); }
            set { SetValue(XYZRightBtnImageSourceAccProperty, value); }
        }
        public double MaxVel
        {
            get { return maxVel; }
            set 
            {
                if (value > 0)
                {
                    maxVel = value;
                    OnPropertyChanged();
                    OnPropertyChanged("AxisVel");
                }   
            }
        }
        private double maxVel = 200;

        public double AxisAcc
        {
            get { return (double)GetValue(AxisAccProperty); }
            set { SetValue(AxisAccProperty, value); }
        }

        public double PercentValue
        {
            get { return percentValue; }
            set { percentValue = value; OnPropertyChanged(); OnPropertyChanged("AxisVel"); }
        }
        private double percentValue = 50;

        public double AxisVel
        {
            get { return percentValue * MaxVel / 100; }
        }

        public double XPos
        {
            get { return xPos; }
            set { xPos = value; OnPropertyChanged(); }
        }
        private double xPos = 0;

        public double YPos
        {
            get { return yPos; }
            set { yPos = value; OnPropertyChanged(); }
        }
        private double yPos = 0;

        public double ZPos
        {
            get { return zPos; }
            set { zPos = value; OnPropertyChanged();  }
        }
        private double zPos = 0;

        public bool IsStepMove
        {
            get { return isStepMove; }
            set { isStepMove=value;OnPropertyChanged(); }
        }
        private bool isStepMove = false;

        public double StepDistance
        {
            get { return _stepDistance; }
            set { _stepDistance = value;OnPropertyChanged() ; }
        }
        private double _stepDistance = 2;

        private void UCXYZStage_Unloaded(object sender, RoutedEventArgs e)
        {
            IsLoad = false;
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            MotionModel.Instance.StopMoving(XAxisNum);        
            MotionModel.Instance.StopMoving(YAxisNum);      
            MotionModel.Instance.StopMoving(ZAxisNum);
        }

        private void UCXYZStage_Loaded(object sender, RoutedEventArgs e)
        {
            EnumAxisType xAxisNum = XAxisNum;
            EnumAxisType yAxisNum = YAxisNum;
            EnumAxisType zAxisNum = ZAxisNum;
            IsLoad = true;
            Task.Run(() =>
            {
                while (IsLoad && MotionModel.Instance.IsInitialized)
                {
                    double pos = 0;
                    MotionModel.Instance.GetPosition(xAxisNum, out pos);
                    XPos = pos;
                    MotionModel.Instance.GetPosition(yAxisNum, out pos);
                    YPos = pos;
                    MotionModel.Instance.GetPosition(zAxisNum, out pos);
                    ZPos = pos;
                    System.Threading.Thread.Sleep(500);
                }
            });
        }

        private bool IsLoad = false;


    }
}
