﻿using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;
using outputStatus = System.Collections.Generic.KeyValuePair<int, bool>;
using Googo.Manul;
using G4.Motion;
using S7.Net;
using SmartGlue.Infrastructure;
using System.Diagnostics;
using Infrastructure;
using NPOI.OpenXmlFormats.Vml.Office;
using NPOI.SS.Formula.Functions;
using LogSv;
using Log = LogSv.Log;
using System.Drawing;
using static NPOI.HSSF.Util.HSSFColor;
using ShowAlarm;

namespace Googo.Manul
{
    public static class GloMotionParame
    {
        #region 公有属性
        public static ObservableCollection<FilterInfor> SaveFilterList
        {
            get { return saveFilterList; }
            set { saveFilterList = value; OnPropertyChanged(); }
        }
        private static ObservableCollection<FilterInfor> saveFilterList = new ObservableCollection<FilterInfor>();

        public static IOCollection InputList
        {
            get { return inputList; }
            set { inputList = value; OnPropertyChanged(); }
        }
        private static IOCollection inputList = new IOCollection();

        public static IOCollection OutputList
        {
            get { return outputList; }
            set { outputList = value; OnPropertyChanged(); }
        }
        private static IOCollection outputList = new IOCollection();


        public static ObservableCollection<AxisInfor> AxisList
        {
            get { return axisList; }
            set { axisList = value; OnPropertyChanged(); }
        }
        private static ObservableCollection<AxisInfor> axisList = new ObservableCollection<AxisInfor>();

        public static bool FlowIsInitialized
        {
            get { return flowIsInitialized; }
            set { flowIsInitialized = value; OnPropertyChanged(); }
        }
        private static bool flowIsInitialized = false;


        /// <summary>
        /// 轴报警外部停流程
        /// </summary>
        public static bool FlowStop
        {
            get { return flowStop; }
            set { flowStop = value; OnPropertyChanged(); }
        }
        private static bool flowStop = false;

        public static ObservableCollection<AxisAlarm> AxisStatusList
        {
            get { return axisStatusList; }
            set
            {
                axisStatusList = value;
                OnPropertyChanged();
                Stop = true;
                ScanAxisStatus(value);
            }
        }
        private static ObservableCollection<AxisAlarm> axisStatusList = new ObservableCollection<AxisAlarm>();

        public static ObservableCollection<StageErrorInfor> RunErrorList
        {
            get { return runErrorList; }
            set
            {
                runErrorList = value;
                OnPropertyChanged();
            }
        }
        private static ObservableCollection<StageErrorInfor> runErrorList = new ObservableCollection<StageErrorInfor>();


        public static Plc SiemenPLC
        {
            get { return _siemenPLC; }
            set
            {
                _siemenPLC = value;
                OnPropertyChanged();
                //Stop = true;
                //ScanAxisStatus(axisStatusList);
            }
        }
        private static Plc _siemenPLC;

     

        public static WinShowAlarm AlarmFrm
        {
            get { return alarmFrm; }
            set { alarmFrm = value; OnPropertyChanged(); }
        }
        private static WinShowAlarm alarmFrm = new WinShowAlarm(new List<Tuple<int, string, string, string, string>>());

        #endregion 公有属性

        #region INotifyPropertyChanged
        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        private static void OnPropertyChanged([CallerMemberName] string name = null)
        {
            if (StaticPropertyChanged != null)
            {
                StaticPropertyChanged.Invoke(null, new PropertyChangedEventArgs(name));
            }
        }
        #endregion INotifyPropertyChanged

        #region 公有函数
        public static void Initial()
        {
            LoadExcel(FilePath);
            LoadFilter();
            LoadMotors();
            LoadRunAlarm(RunErrorPath);
            LoadAlarm(GloMotionParame.AlarmPath);
        }

        /// <summary>
        /// 写报警信息到PLC对应的地址
        /// </summary>
        /// <param name="stageIndex">模块号 默认是1</param>
        /// <param name="errorCode">报警信息</param>
        public static void WriteErrCode(int stageIndex, short errorCode)
        {
            if (null == _siemenPLC || !_siemenPLC.IsConnected) return;
            IEnumerable<StageErrorInfor> errors = runErrorList.Where((temp) => { return temp.Index == stageIndex; });
            if (errors == null || errors.Count() == 0) return;
            foreach (var item in errors)
            {
                if (item == null || item.AlarmCodeList == null) continue;
                RunErrorInfor infor = item.AlarmCodeList.FirstOrDefault((tt) => { return tt.AlarmCode == errorCode; });
                if (null != infor)
                {
                    if (!string.IsNullOrEmpty(item.Address))
                        _siemenPLC.Write(item.Address, errorCode);
                    return;
                }
            }
        }

        internal static void LoadExcel(string path)
        {
            if (!System.IO.File.Exists(path)) return;
            XSSFWorkbook workbook2007 = new XSSFWorkbook(path);
            ISheet sheet = workbook2007.GetSheet("Input");
            InputList.Clear();
            GetSheet(InputList, sheet, false);
            OutputList.Clear();
            sheet = workbook2007.GetSheet("Output");
            GetSheet(OutputList, sheet);
            workbook2007.Close();
        }

        internal static void LoadFilter()
        {
            if (!File.Exists(ConditionPath)) return;
            FileStream fs = new FileStream(ConditionPath, FileMode.Open, FileAccess.Read);
            BinaryFormatter binary = new BinaryFormatter();
            binary.Context = new StreamingContext(StreamingContextStates.Persistence | StreamingContextStates.File);
            SaveFilterList = (ObservableCollection<FilterInfor>)binary.Deserialize(fs);
            fs.Flush();
            fs.Close();
            fs.Dispose();
            SaveToFlash();
        }

        internal static void LoadMotors()
        {
            if (System.IO.File.Exists(MotorsPath))
            {
                XmlSerializer xs = new XmlSerializer(typeof(ObservableCollection<AxisInfor>));
                FileStream fs = new FileStream(MotorsPath, FileMode.Open, FileAccess.Read);
                object config = xs.Deserialize(fs);
                fs.Close();
                AxisList = (ObservableCollection<AxisInfor>)config;
            }
        }


        internal static void LoadAlarm(string filePath)
        {
            if (System.IO.File.Exists(filePath))
            {
                XmlSerializer xs = new XmlSerializer(typeof(ObservableCollection<AxisAlarm>));
                FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                object config = xs.Deserialize(fs);
                fs.Close();
                AxisStatusList = (ObservableCollection<AxisAlarm>)config;
            }
        }

        internal static void LoadRunAlarm(string filePath)
        {
            if (System.IO.File.Exists(filePath))
            {
                XmlSerializer xs = new XmlSerializer(typeof(ObservableCollection<StageErrorInfor>));
                FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                object config = xs.Deserialize(fs);
                fs.Close();
                RunErrorList = (ObservableCollection<StageErrorInfor>)config;
            }
        }
        #endregion 公有函数

        private static int GetSheet(IList<IOStatus> statusList, ISheet sheet, bool IsOut = true)
        {
            PropertyInfo[] infos = typeof(IOStatus).GetProperties();
            if (null == infos || infos.Length == 0) return 0;
            if (null == statusList || sheet == null) return -1;
            IRow row = sheet.GetRow(0);
            Dictionary<int, PropertyInfo> pairs = new Dictionary<int, PropertyInfo>();
            for (int k = 0; k < row.LastCellNum; k++)
            {
                string proName = row.GetCell(k).ToString();
                PropertyInfo match = infos.FirstOrDefault((temp) => { return temp.Name == proName; });
                pairs[k] = match;
            }
            for (int i = 1; i <= sheet.LastRowNum; i++)
            {
                IOStatus status = new IOStatus();
                row = sheet.GetRow(i);
                ICell cell = row.GetCell(0);
                if (cell == null || cell.ToString() == string.Empty) break;
                for (int k = 0; k < row.LastCellNum; k++)
                {
                    PropertyInfo property = pairs[k];
                    if (property == null) continue;
                    cell = row.GetCell(k);
                    if (cell.CellType == CellType.Boolean)
                        property.SetValue(status, cell.BooleanCellValue);
                    else if (cell.CellType == CellType.String)
                        property.SetValue(status, cell.StringCellValue);
                    else if (cell.CellType == CellType.Numeric)
                    {
                        if (property.PropertyType == typeof(double))
                            property.SetValue(status, cell.NumericCellValue);
                        else if (property.PropertyType == typeof(int))
                            property.SetValue(status, (int)cell.NumericCellValue);
                    }
                }
                status.IsOutModule = IsOut;
                statusList.Add(status);
            }
            return 0;
        }

        /// <summary>
        /// 开启线程扫描状态
        /// </summary>
        /// <param name="axiss"></param>
        private static void ScanAxisStatus(IEnumerable<AxisAlarm> axiss)
        {
            if (null == axiss || axiss.Count() == 0) return;
            if (null == task || task.IsCompleted)
            {
                task = Task.Run(() =>
                {
                    Stop = false;
                    while (!Stop)
                    {
                        if (MotionModel.Instance.IsInitialized && FlowIsInitialized)
                        {
                            foreach (var item in axiss)
                            {
                                if (null == item) continue;
                                AxisStatus axisStatus = new AxisStatus();
                                if (0 == MotionModel.Instance.GetAxisStatus(item.AxisNum, ref axisStatus))
                                {
                                    UpPLCAlarm alarm = null;
                                    if (axisStatus.Alarm)
                                    {
                                        alarm = item.PLCAlarms.FirstOrDefault((temp) => { return temp.AlarmType == AlarmTYpe.Alarm; });
                                        Log.log.Write($"{item.AxisNum}轴报警，PLC报警地址{item.Address}", Color.Red);
                                        AlarmFrm.ShowAlarmForm(1, item.AxisNum.ToString() + "轴报警", "请检查" + item.AxisNum.ToString() + "轴位置");
                                        FlowStop = true;
                                    }
                                    else if (axisStatus.PositiveLimit)
                                    {
                                        alarm = item.PLCAlarms.FirstOrDefault((temp) => { return temp.AlarmType == AlarmTYpe.PLimit; });
                                        Log.log.Write($"{item.AxisNum}轴正限位报警，PLC报警地址{item.Address}", Color.Red);
                                        AlarmFrm.ShowAlarmForm(1, item.AxisNum.ToString() + "轴正限位报警", "请检查" + item.AxisNum.ToString() + "轴位置");
                                        FlowStop = true;
                                    }
                                    else if (axisStatus.NegativeLimit)
                                    {
                                        alarm = item.PLCAlarms.FirstOrDefault((temp) => { return temp.AlarmType == AlarmTYpe.NLimit; });
                                        Log.log.Write($"{item.AxisNum}轴负限位报警，PLC报警地址{item.Address}", Color.Red);
                                        AlarmFrm.ShowAlarmForm(1, item.AxisNum.ToString() + "轴负限位报警", "请检查" + item.AxisNum.ToString() + "轴位置");
                                        FlowStop = true;
                                    }
                                    if (alarm == null) continue;
                                    try
                                    {
                                        if (SiemenPLC != null && SiemenPLC.IsConnected)
                                        {
                                            SiemenPLC.WriteValue(item.Address, (ushort)alarm.AlarmCode);
                                            //alarm.WriteToPLC();
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        Debug.WriteLine(ex);
                                    }
                                }
                            }
                        }
                        System.Threading.Thread.Sleep(200);
                    }
                });
            }
        }

        private static void SaveToFlash()
        {
            if (SaveFilterList == null || SaveFilterList.Count == 0) return;
            IEnumerable<OutputFilter> oufilter = SaveFilterList.OfType<OutputFilter>();
            IEnumerable<AxisFilter> axisfilter = SaveFilterList.OfType<AxisFilter>();
            Dictionary<outputStatus, IEnumerable<Func<bool>>> oufiDic = new Dictionary<outputStatus, IEnumerable<Func<bool>>>();
            Dictionary<EnumAxisType, IEnumerable<Func<double, bool>>> axisfiDic = new Dictionary<EnumAxisType, IEnumerable<Func<double, bool>>>();
            if (null != oufilter)
            {
                foreach (var item in oufilter)
                {
                    if (null == item) continue;
                    var pair = item.GetOutPutFilter();
                    oufiDic[pair.Key] = pair.Value;
                }
            }
            if (null != axisfilter)
            {
                foreach (var item in axisfilter)
                {
                    if (null == item) continue;
                    var pair = item.GetAxisFilter();
                    axisfiDic[pair.Key] = pair.Value;
                }
            }
            G4.Motion.MotionModel.Instance.AddCondition(oufiDic, axisfiDic);
        }

        internal static readonly string FilePath = AppDomain.CurrentDomain.BaseDirectory + "Config\\IOList.xls";
        internal static readonly string ConditionPath = AppDomain.CurrentDomain.BaseDirectory + "Config\\Condition.bin";
        internal static readonly string MotorsPath = AppDomain.CurrentDomain.BaseDirectory + "Config\\AllMotorParame.xml";
        internal static readonly string AlarmPath = AppDomain.CurrentDomain.BaseDirectory + "Config\\AxisAlarm.xml";
        public static readonly string RunErrorPath = AppDomain.CurrentDomain.BaseDirectory + "Config\\RunAlarm.xml";


        private static Task task;
        private static bool Stop = true;
    }

    public static class S7Extension
    {
        /// <summary>
        /// 读操作
        /// </summary>
        /// <param name="address">地址</param>
        /// <returns></returns>
        internal static bool ReadValue<T>(this Plc module, string address, out T value)
        {
            Type tartype = typeof(T);
            value = default(T);
            if (!module.IsConnected || string.IsNullOrEmpty(address)) { return false; }
            object val = null;
            try
            {
                if (tartype.IsValueType)
                    val = module.Read(address);
                else
                {
                    string[] array = address.Split('.');
                    if (array.Length < 2)
                    {
                        throw new InvalidAddressException("To few periods for DB address");
                    }
                    int dbNumber = int.Parse(array[0].Substring(2));
                    int startByte = int.Parse(array[1].Substring(3));
                    val = module.Read(DataType.DataBlock, dbNumber, startByte, VarType.String, 32, 0);
                }
            }
            catch (Exception ex)
            {
                LogServiceHelper.Error(ex);
                return false;
            }
            if (tartype == typeof(bool) || tartype == typeof(byte) || tartype == typeof(uint))
            {
                value = (T)val;
            }
            else if (tartype == typeof(string))
            {
                val = val.ValToBinString();
                value = (T)val;
            }
            else if (tartype == typeof(int))
            {
                val = val.ToString().BinStringToInt32();
                value = (T)val;
            }
            else if (tartype == typeof(float))
            {
                val = ((uint)val).ConvertToFloat();
                value = (T)val;
            }
            else if (tartype == typeof(short))
            {
                val = ((ushort)val).ConvertToShort();
                value = (T)val;
            }
            return true;
        }

        /// <summary>
        /// 读操作
        /// </summary>
        /// <param name="address">地址</param>
        /// <returns></returns>
        internal static bool ReadValue(this Plc module, string address, Type tartype, out dynamic value)
        {
            value = null;
            if (!module.IsConnected || string.IsNullOrEmpty(address)) { return false; }
            object val = null;
            try
            {
                Stopwatch sw = Stopwatch.StartNew();
                if (tartype.IsValueType)
                    val = module.Read(address);
                else
                {
                    string[] array = address.Split('.');
                    if (array.Length < 2)
                    {
                        throw new InvalidAddressException("To few periods for DB address");
                    }
                    int dbNumber = int.Parse(array[0].Substring(2));
                    int startByte = int.Parse(array[1].Substring(3));

                    if (address.Contains("DBS"))
                    {
                        var reservedLength = (byte)module.Read(DataType.DataBlock, dbNumber, startByte, VarType.Byte, 1);
                        val = (string)module.Read(DataType.DataBlock, dbNumber, startByte, VarType.S7String, reservedLength);
                    }
                    else
                        val = module.Read(DataType.DataBlock, dbNumber, startByte, VarType.String, 32, 0);
                }
                sw.Stop();
                if (sw.ElapsedMilliseconds > 100)
                    Debug.WriteLine($"read plc using {sw.ElapsedMilliseconds} ms");
            }
            catch (Exception ex)
            {
                LogServiceHelper.Error(ex);
                return false;
            }
            if (tartype == typeof(bool) || tartype == typeof(byte) || tartype == typeof(uint))
            {
                value = val;
            }
            else if (tartype == typeof(string))
            {
                val = Convert.ToString(val).Replace("\0", "");
                //val = val.ValToBinString();
                value = val;
            }
            else if (tartype == typeof(int))
            {
                val = val.ToString().BinStringToInt32();
                value = val;
            }
            else if (tartype == typeof(float))
            {
                val = ((uint)val).ConvertToFloat();
                value = val;
            }
            else if (tartype == typeof(short))
            {
                val = ((ushort)val).ConvertToShort();
                value = val;
            }
            return true;
        }

        /// <summary>
        /// 写操作
        /// </summary>
        /// <param name="address">地址</param>
        /// <param name="val">变量值</param>
        internal static bool WriteValue(this Plc module, string address, dynamic val)
        {
            if (!module.IsConnected || string.IsNullOrEmpty(address)) { return false; }
            try
            {
                Stopwatch sw = Stopwatch.StartNew();
                Type tartype = val.GetType();
                if (tartype.IsValueType)
                    module.Write(address, val);
                else
                {
                    string[] array = address.Split('.');
                    if (array.Length < 2)
                    {
                        throw new InvalidAddressException("To few periods for DB address");
                    }
                    int dbNumber = int.Parse(array[0].Substring(2));
                    int startByte = int.Parse(array[1].Substring(3));
                    byte[] arry = System.Text.Encoding.ASCII.GetBytes(val);

                    if (address.Contains("DBS"))
                    {
                        //byte[] temArr = new byte[2] { (byte)Convert.ToInt32("FE".Substring(0, 2), 16), (byte)Convert.ToInt32("FE".Substring(0, 2), 16) };
                        //arry = temArr.Concat(arry).ToArray();
                        var bytes = S7.Net.Types.S7String.ToByteArray(val, arry.Length);
                        module.WriteBytes(DataType.DataBlock, dbNumber, startByte, bytes);
                    }
                    else
                        module.Write(DataType.DataBlock, dbNumber, startByte, arry);
                }
                sw.Stop();
                if (sw.ElapsedMilliseconds > 100)
                    Debug.WriteLine($"write plc using {sw.ElapsedMilliseconds} ms");
            }
            catch (Exception ex)
            {
                LogServiceHelper.Error(ex);
                return false;
            }
            //module.Write(address, val);
            return true;
        }

    }
}
