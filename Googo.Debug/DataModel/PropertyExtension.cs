﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows;
using System.ComponentModel.DataAnnotations;
using System.Windows.Controls;

namespace Googo.Manul
{
    public static class PropertyExtension
    {
        public static string GetLabDispContent(this PropertyInfo propertyInfo)
        {
            DisplayAttribute attribute = propertyInfo.GetCustomAttribute<DisplayAttribute>(true);
            if (attribute != null)
            {
                return attribute.Name;
            }
            else
            {
                DisplayNameAttribute displayName = propertyInfo.GetCustomAttribute<DisplayNameAttribute>(true);
                if (displayName != null) return displayName.DisplayName;
            }
            return propertyInfo.Name;
        }

    }
}