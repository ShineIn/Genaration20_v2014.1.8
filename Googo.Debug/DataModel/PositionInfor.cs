﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Googo.Manul
{
    /// <summary>
    /// 位置信息
    /// </summary>
    [Serializable]
    public class PositionInfor:PropertyChangedBase, INameBase
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged(); }
        }
        private string name = string.Empty;

        /// <summary>
        /// X坐标
        /// </summary>
        public double XPos
        {
            get { return xPos; }
            set { xPos = value; OnPropertyChanged(); }
        }
        private double xPos = 0;

        /// <summary>
        /// Y坐标
        /// </summary>
        public double YPos
        {
            get { return yPos; }
            set { yPos = value; OnPropertyChanged(); }
        }
        private double yPos = 0;

        /// <summary>
        /// Z坐标
        /// </summary>
        public double ZPos
        {
            get { return zPos; }
            set { zPos = value; OnPropertyChanged(); }
        }
        private double zPos = 0;

        public string StrCommand
        {
            get { return strCommand; }
            set { strCommand = value; OnPropertyChanged(); }
        }
        private string strCommand = "Glue_1";

        /// <summary>
        /// 附加信息
        /// </summary>
        public string Descri
        {
            get { return descri; }
            set { descri = value; OnPropertyChanged(); }
        }
        private string descri = string.Empty;

        /// <summary>
        /// 新增算法Mes交互命令
        /// </summary>
        public SendData ExtenCommand
        {
            get { return extenCommand; }
            set { extenCommand = value;OnPropertyChanged(); }
        }
        private SendData extenCommand=new SendData();

        /// <summary>
        /// 新增算法Mes交互命令
        /// </summary>
        public ReceiveData Result
        {
            get { return result; }
            set { result = value; OnPropertyChanged(); }
        }
        private ReceiveData result = new ReceiveData();

        public override string ToString()
        {
            return $"{name},{xPos},{yPos},{zPos},{strCommand},{descri}";
        }
    }
}
