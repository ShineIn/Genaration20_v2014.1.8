﻿using G4.Motion;
using NPOI.HPSF;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Data;
using System.ComponentModel;
using SmartGlue.Infrastructure;
using S7.Net;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using System.Linq.Expressions;
using System.Collections.ObjectModel;
//using Excel = Microsoft.Office.Interop.Excel;

namespace Googo.Manul
{
    /// <summary>
    /// 报警类型
    /// </summary>
    public enum AlarmTYpe
    {
        None = -1,
        Alarm,
        PLimit,
        NLimit,
        TimeOut
    }

    public class UpPLCAlarm : PropertyChangedBase
    {
        /// <summary>
        /// 报警条码
        /// </summary>
        public short AlarmCode
        {
            get { return alarmCode; }
            set { alarmCode = value; OnPropertyChanged(); }
        }
        private short alarmCode = 1200;
        /// <summary>
        /// 报警地址
        /// </summary>
        public string Address
        {
            get { return address; }
            set { address = value; OnPropertyChanged(); }
        }
        private string address = string.Empty;

        /// <summary>
        /// 报警信息
        /// </summary>
        public string AlarmContent
        {
            get { return alarmContent; }
            set { alarmContent = value; OnPropertyChanged(); }
        }
        private string alarmContent = string.Empty;

        /// <summary>
        /// 报警的类型
        /// </summary>
        public AlarmTYpe AlarmType
        {
            get { return alarmType; }
            set { alarmType = value; OnPropertyChanged(); }
        }
        private AlarmTYpe alarmType = AlarmTYpe.Alarm;

        #region shangchuan 


        public void WriteToPLC()
        {
            if (GloMotionParame.SiemenPLC == null || !GloMotionParame.SiemenPLC.IsConnected
                ||string.IsNullOrEmpty(address)|| alarmCode<0) return;
            GloMotionParame.SiemenPLC.Write(Address, AlarmCode);
        }
        #endregion
    }

    /// <summary>
    /// 轴报警信息
    /// </summary>
    public class AxisAlarm : PropertyChangedBase
    {
        /// <summary>
        /// 轴号
        /// </summary>
        public EnumAxisType AxisNum
        {
            get { return _axisNum; }
            set { _axisNum = value; OnPropertyChanged(); }
        }
        private EnumAxisType _axisNum = EnumAxisType.X0;

        /// <summary>
        /// 报警地址
        /// </summary>
        public string Address
        {
            get { return address; }
            set { address = value; OnPropertyChanged(); }
        }
        private string address = string.Empty;

        /// <summary>
        /// 检查报警的状态
        /// </summary>
        public ObservableCollection<UpPLCAlarm> PLCAlarms
        {
            get { return pLCAlarms; }
            set { pLCAlarms = value; OnPropertyChanged(); }
        }
        private ObservableCollection<UpPLCAlarm> pLCAlarms = new ObservableCollection<UpPLCAlarm>();
    }
}
