﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Googo.Manul
{
    public static class NameListExten
    {
        public static T GetItemByName<T>(this IList<T> source, string name) where T : INameBase
        {
            T status = source.FirstOrDefault((item) => { return item.Name == name; });
            if (null == status)
                throw new Exception($"Unfound {name} item");
            return status;
        }  
    }
}
