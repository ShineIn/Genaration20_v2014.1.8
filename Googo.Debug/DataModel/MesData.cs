﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace Googo.Manul
{
    /// <summary>
    /// 参数类型
    /// </summary>
    [Serializable]
    public class ParameterItem : CommunicaBase
    {
        [XmlIgnore]
        public Type Datatype
        {
            get
            {
                if (datatype != null) return datatype;
                return string.IsNullOrEmpty(StrTypeName) ? null : Type.GetType(StrTypeName);
            }
            set
            {
                StrTypeName = null != value ? value.AssemblyQualifiedName : string.Empty;
                datatype = value;
                OnPropertyChanged();
            }
        }
        private Type datatype = null;
        [JsonIgnore]
        public string StrTypeName { get; set; }

        [UpLoad, DownLoad]
        public object Value
        {
            get { return _value; }
            set
            {
                //if (CanConvert(value, out object tar))
                //{
                _value = value;
                OnPropertyChanged();
                //}
            }
        }
        private object _value;

        /// <summary>
        /// 匹配的映射关系
        /// </summary>
        [JsonIgnore]
        public string MatchAddress
        {
            get { return matchAddress; }
            set { matchAddress = value; OnPropertyChanged(); }
        }
        private string matchAddress = string.Empty;

        /// <summary>
        /// 上传所有的属性参数
        /// </summary>
        public override void UpLoads()
        {
            string strMatch = matchAddress;
            if (string.IsNullOrEmpty(strMatch) && null != Arquments && Arquments.Count > 0)
            {
                strMatch = Arquments[0].Address;
            }
            if (Datatype == null || null == _value || string.IsNullOrEmpty(strMatch)) return;
            if (null == GloMotionParame.SiemenPLC || !GloMotionParame.SiemenPLC.IsConnected) return;
            Type tarType = ConverType(Datatype);
            TypeConverter converter = TypeDescriptor.GetConverter(tarType);
            if (null == converter) return;
            object result = converter.ConvertFromString(_value.ToString());
            GloMotionParame.SiemenPLC.WriteValue(strMatch, result);
        }


        /// <summary>
        /// 下载所有的属性参数
        /// </summary>
        public override void DownLoad()
        {
            string strMatch = matchAddress;
            if (string.IsNullOrEmpty(strMatch) && null != Arquments && Arquments.Count > 0)
            {
                strMatch = Arquments[0].Address;
            }
            if (Datatype == null || null == _value) return;
            if (Datatype == null || null == _value || string.IsNullOrEmpty(strMatch)) return;
            if (null == GloMotionParame.SiemenPLC || !GloMotionParame.SiemenPLC.IsConnected) return;
            Type tarType = ConverType(Datatype);
            GloMotionParame.SiemenPLC.ReadValue(strMatch, tarType, out dynamic reValue);
            if (null == reValue) return;
            Type type = reValue.GetType();
            _value = reValue;
        }

        /// <summary>
        /// 做一层类型转换，主要是针对double和float之间，因为plc只有float，Vp默认使用double
        /// </summary>
        /// <param name="sourceType"></param>
        /// <returns></returns>
        private Type ConverType(Type sourceType)
        {
            if (sourceType == typeof(double)) return typeof(float);
            return sourceType;
        }

        /// <summary>
        /// 判断输入类型是否可以转换
        /// </summary>
        /// <param name="tempValue"></param>
        /// <param name="tarValue"></param>
        /// <returns></returns>
        private bool CanConvert(object tempValue, out object tarValue)
        {
            tarValue = null;
            if (Datatype == null || null == tempValue) return false;
            TypeConverter converter = TypeDescriptor.GetConverter(Datatype);
            Type souType = tempValue.GetType();
            if (null == converter) return false;
            object result = converter.ConvertFromString(tempValue.ToString());
            tarValue = result;
            return null != result;
        }
    }

    /// <summary>
    /// 算法配方下载
    /// </summary>
    [Serializable]
    public class SendData : CommunicaBase
    {
        [JsonIgnore]
        public override string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged(); }
        }
        private string _name = string.Empty;


        public ObservableCollection<ParameterItem> ParameterList
        {
            get { return parameterList; }
            set { parameterList = value; }
        }
        ObservableCollection<ParameterItem> parameterList = new ObservableCollection<ParameterItem>();

        [DownLoad]
        public string SN { get; set; }

        [DownLoad]
        public Int16 RecipeNo { get; set; }

        [DownLoad]
        public Int16 TaskNo { get; set; }
        public bool CalibrationFlag { get; set; }

        public override void DownLoad()
        {
            DownLoadPro(nameof(SN));
            DownLoadPro(nameof(RecipeNo));
            DownLoadPro(nameof(TaskNo));
            foreach (var item in parameterList)
            {
                item?.DownLoad();
            }
        }
    }


    /// <summary>
    /// 算法结果上传
    /// </summary>
    [Serializable]
    public class ReceiveData : CommunicaBase
    {
        [JsonIgnore]
        public override string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged(); }
        }
        private string _name = string.Empty;

        [UpLoad]
        public Int16 Result { get; set; }//0:Mistake 1:OK 2:NG 3:Error


        public ObservableCollection<ParameterItem> ResultList
        {
            get { return resultList; }
            set { resultList = value; }
        }
        ObservableCollection<ParameterItem> resultList = new ObservableCollection<ParameterItem>();

        public override void UpLoads()
        {
            UpLoadPro(nameof(Result));
            foreach (var item in resultList)
            {
                item?.UpLoads();
            }
        }

        public void Assign(ReceiveData source)
        {
            Debug.Assert(null != source);
            Result = source.Result;
            foreach (var item in source.resultList)
            {
                if (item == null || string.IsNullOrEmpty(item.Name) || null == item.Datatype) continue;
                var match = resultList.FirstOrDefault((tt) => { return tt.Name == item.Name && tt.Datatype == item.Datatype; });
                if (match == null) continue;
                match.Value = item.Value;
            }
        }
    }
}
