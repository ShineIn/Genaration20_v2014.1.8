﻿using G4.Motion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Googo.Manul
{
    /// <summary>
    /// IO信息
    /// </summary>
    [Serializable]
    public class IOStatus : PropertyChangedBase, ICloneable, IEquatable<IOStatus>, INameBase
    {
        #region 公有属性
        /// <summary>
        /// IO的bit位
        /// </summary>
        public int BitIndex
        {
            get { return _bitIndex; }
            set
            {
                if (value >= 0)
                {
                    _bitIndex = value;
                    OnPropertyChanged();
                }
            }
        }
        private int _bitIndex;

        /// <summary>
        /// IO位是否有值，既非零
        /// </summary>
        [XmlIgnore]
        public bool IsHasVlue
        {
            get { return _IsHasVlue; }
            set
            {
                _IsHasVlue = value;
                OnPropertyChanged();
            }
        }
        private bool _IsHasVlue = false;

        /// <summary>
        /// 点位描述，显示在按钮上的
        /// </summary>
        public string BitDescri
        {
            get { return _bitDescri; }
            set { _bitDescri = value; OnPropertyChanged(); }
        }
        private string _bitDescri = string.Empty;

        /// <summary>
        /// 软件调用名称
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged(); }
        }
        private string _name = string.Empty;

        /// <summary>
        /// 是否是输出模块
        /// </summary>
        public bool IsOutModule
        {
            get { return _isOutModule; }
            set
            {
                _isOutModule = value;
                OnPropertyChanged();
            }
        }
        private bool _isOutModule = false;
        #endregion 公有属性

        #region 公有函数
        public IOStatus Clone()
        {
            return new IOStatus
            {
                _bitIndex = _bitIndex,
                _name=_name,
                _bitDescri = _bitDescri,
                _isOutModule = _isOutModule,
            };
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        public bool Equals(IOStatus other)
        {
            if (other == null)
                return false;
            if ((_bitIndex == other._bitIndex || _name == other._name)
                && _isOutModule == other._isOutModule)
            {
                return true;
            }
            return false;
        }

        public override bool Equals(object other)
        {
            if (ReferenceEquals(this, other))
                return true;
            return Equals(other as IOStatus);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public bool GetValue()
        {
            bool bret = false;
            if (IsOutModule)
            {
                if (0 == MotionModel.Instance.GetDo(_bitIndex, out bret))
                    IsHasVlue = bret;
            }
            else
            {
                if (0 == MotionModel.Instance.GetDi(_bitIndex, out bret))
                    IsHasVlue = bret;
            }
            return bret;
        }

        public int SetValue(bool bvalue)
        {
            bool bret = false;
            if (IsOutModule)
            {
                int iret = MotionModel.Instance.SetDo(_bitIndex, bvalue);
                if (0 == iret) return iret;
                iret = MotionModel.Instance.GetDo(_bitIndex, out bret);
                if (0 == iret) return iret;
                IsHasVlue = bret;
                return 0;
            }
            return -1;
        }
        #endregion 公有函数
    }
}
