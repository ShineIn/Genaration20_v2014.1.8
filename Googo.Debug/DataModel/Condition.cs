﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using G4.Motion;
using NPOI.SS.Formula.Functions;

namespace Googo.Manul
{
    /// <summary>
    /// 硬件类型
    /// </summary>
    public enum DeviceType : int
    {
        None = 0,
        Output = 0x001,
        Input = 0x002,
        Axis = 0x004,
        PLC = 0x008,
    }

    /// <summary>
    /// 条件接口
    /// </summary>
    public interface ICondition : INotifyPropertyChanged, INameBase
    {
        /// <summary>
        /// 过滤的硬件
        /// </summary>
        DeviceType FilterDevice { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        bool IsEnabled { get; set; }

        /// <summary>
        /// 设置输出的过滤条件，当委托返回true时触发条件，不执行
        /// </summary>
        [Browsable(false)]
        Func<bool> OutProtectedFilter { get; }

        /// <summary>
        /// 轴移动的过滤条件,当委托返回true时候则不执行轴移动
        /// </summary>
        [Browsable(false)]
        Func<double, bool> AxisProtectedFilter { get; }

        [Browsable(false)]
        Type ParentFilterType { get; set; }
    }

    /// <summary>
    /// 硬件过滤条件
    /// </summary>
    [Serializable]
    public abstract class DeviceFilter : ICondition
    {
        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged(); }
        }
        private string name = string.Empty;

        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value;
                OnPropertyChanged();
            }
        }
        private bool isEnabled = true;

        /// <summary>
        /// 点位序号
        /// </summary>
        public int PortIndex
        {
            get { return portIndex; }
            set
            {
                if (value >= 0)
                {
                    portIndex = value;
                    OnPropertyChanged();
                }
            }
        }
        private int portIndex = 1;

        /// <summary>
        /// 是否有值或者高电平
        /// </summary>
        public bool IsHighValue
        {
            get { return isHighValue; }
            set
            {
                isHighValue = value;
                OnPropertyChanged();
            }
        }
        private bool isHighValue = false;

        /// <summary>
        /// 移动轴目标位置下限
        /// </summary>
        public double TarPosLowLimit
        {
            get { return tarPosLowLimit; }
            set
            {
                tarPosLowLimit = value;
                OnPropertyChanged();
            }
        }
        private double tarPosLowLimit = -999;

        /// <summary>
        /// 移动轴目标位置上限
        /// </summary>
        public double TarPosHighLimit
        {
            get { return tarPosHighLimit; }
            set
            {
                tarPosHighLimit = value;
                OnPropertyChanged();
            }
        }
        private double tarPosHighLimit = 999;

        /// <summary>
        /// 父筛选器类型
        /// </summary>
        [Browsable(false)]
        public Type ParentFilterType
        {
            get { return parentFilterType; }
            set { parentFilterType = value; OnPropertyChanged(); }
        }
        private Type parentFilterType = null;

        public abstract DeviceType FilterDevice { get; set; }

        public abstract Func<bool> OutProtectedFilter { get; }

        public abstract Func<double, bool> AxisProtectedFilter { get; }

        /// <summary>
        /// 获取输入条件,如果读取的值和输入的值相等则触发保护
        /// </summary>
        /// <returns></returns>
        protected bool filterInput()
        {
            if(!isEnabled) return false;
            bool breadValue = false;
            if (0 != MotionModel.Instance.GetDi(PortIndex, out breadValue)) return true;
            if (breadValue == IsHighValue)
            {
                Debug.WriteLine($"Condition {Name},PortIndex={PortIndex} is {breadValue}");
                return true;
            }
            return false;
        }

        /// <summary>
        /// 输出过滤,,如果读取的值和输入的值相等则触发保护
        /// </summary>
        /// <returns></returns>
        protected bool filterOutput()
        {
            if(!isEnabled) return false;    
            bool breadValue = false;
            if (0 != MotionModel.Instance.GetDo(PortIndex, out breadValue)) return true;
            if (breadValue == IsHighValue)
            {
                Debug.WriteLine($"Condition {Name},PortIndex={PortIndex} is {breadValue}");
                return true;
            }
            return false;
        }
    }

    /// <summary>
    /// 输入和输出过滤条件
    /// </summary>
    [Serializable]
    public class IOCondition : DeviceFilter
    {
        public override DeviceType FilterDevice
        {
            get { return actionDevice; }
            set
            {
                if (((int)value & 3) > 0)
                {
                    actionDevice = value;
                    OnPropertyChanged();
                }
            }
        }
        private DeviceType actionDevice = DeviceType.Output;

        [Browsable(false)]
        public override Func<bool> OutProtectedFilter
        {
            get
            {
                if (ParentFilterType == null || ParentFilterType != typeof(OutputFilter)) return null;
                if (actionDevice == DeviceType.Input)
                {
                    return filterInput;
                }
                if (actionDevice == DeviceType.Output)
                {
                    return filterOutput;
                }
                return null;
            }
        }

        [Browsable(false)]
        public override Func<double, bool> AxisProtectedFilter
        {
            get
            {
                if (ParentFilterType == null || ParentFilterType != typeof(AxisFilter)) return null;
                return new Func<double, bool>((pos) =>
                {
                    if (pos < TarPosLowLimit || pos > TarPosHighLimit) return false;//不在保护区，不检测
                    if (actionDevice == DeviceType.Input)
                    {
                        return filterInput();
                    }
                    else if (actionDevice == DeviceType.Output)
                    {
                        return filterOutput();
                    }
                    return false;
                });
            }
        }
    }

    /// <summary>
    /// 轴的移动条件，当轴的运动目标位置进入设定的范围，
    /// 则去读取映射轴的坐标，如果当前位置在保护范围之内则返回false而使轴不移动，否则返回true
    /// </summary>
    [Serializable]
    public class AxisCondition : DeviceFilter
    {
        [field: NonSerialized]
        [Browsable(false)]
        public override DeviceType FilterDevice
        {
            get { return DeviceType.Axis; }
            set { }
        }

        /// <summary>
        /// 检测的轴号
        /// </summary>
        public EnumAxisType MapAxisNum
        {
            get { return mapAxisNum; }
            set
            {
                if (value > 0)
                {
                    mapAxisNum = value;
                    OnPropertyChanged();
                }
            }
        }
        private EnumAxisType mapAxisNum = EnumAxisType.X;

        /// <summary>
        /// 保护轴的位置下限
        /// </summary>
        public double MapLowLimit
        {
            get { return mapLowLimit; }
            set
            {
                mapLowLimit = value;
                OnPropertyChanged();
            }
        }
        private double mapLowLimit = -999;

        /// <summary>
        /// 保护轴的位置上限
        /// </summary>
        public double MapHighLimit
        {
            get { return mapHighLimit; }
            set
            {
                mapHighLimit = value;
                OnPropertyChanged();
            }
        }
        private double mapHighLimit = 999;

        /// <summary>
        /// 过滤条件是轴是没有Out委托
        /// </summary>
        [Browsable(false)]
        public override Func<bool> OutProtectedFilter
        {
            get
            {
                if (ParentFilterType == null || ParentFilterType != typeof(OutputFilter)) return null;
                return new Func<bool>(() =>
                {
                    if (0 != MotionModel.Instance.GetPosition(MapAxisNum, out double mappos))
                    {
                        Debug.WriteLine($"mapAxis= {MapAxisNum} get position fail");
                        return true;
                    }
                    if (mappos < MapLowLimit || mappos > MapHighLimit)//当前位置不在保护范围之内则可以移动
                    {
                        return false;
                    }
                    Debug.WriteLine($"mapAxis= {MapAxisNum} current position={mappos} is in protected range {MapLowLimit} :{MapHighLimit}");
                    return true;
                });
            }
        }

        [Browsable(false)]
        public override Func<double, bool> AxisProtectedFilter
        {
            get
            {
                if (ParentFilterType == null || ParentFilterType != typeof(AxisFilter)) return null;
                return AxisGetPos;
            }
        }

        /// <summary>
        /// 轴判断保护条件 
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        private bool AxisGetPos(double pos)
        {
            if (pos < TarPosLowLimit || pos > TarPosHighLimit) return false;
            if (0 != MotionModel.Instance.GetPosition(MapAxisNum, out double mappos))
            {
                Debug.WriteLine($"mapAxis= {MapAxisNum} get position fail");
                return true;
            }
            if (mappos < MapLowLimit || mappos > MapHighLimit)//当前位置不在保护范围之内则可以移动
            {
                return false;
            }
            Debug.WriteLine($"mapAxis= {MapAxisNum} current position={mappos} is in protected range {MapLowLimit} :{MapHighLimit}");
            return true;
        }
    }


    /// <summary>
    /// 过滤器列表，判断子项过滤器，并将结果汇总返回
    /// </summary>
    [Serializable]
    public class ConditionList : ICondition
    {
        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged(); }
        }
        private string name = string.Empty;

        [field: NonSerialized]
        [Browsable(false)]
        public DeviceType FilterDevice
        {
            get { return DeviceType.None; }
            set { }
        }

        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value;
                OnPropertyChanged();
            }
        }
        private bool isEnabled = true;

        /// <summary>
        /// 是否进行与运算
        /// </summary>
        public bool IsAndOparate
        {
            get { return isAndOparate; }
            set
            {
                isAndOparate = value;
                OnPropertyChanged();
            }
        }
        private bool isAndOparate = true;

        /// <summary>
        /// 父筛选器类型
        /// </summary>
        [Browsable(false)]
        public Type ParentFilterType
        {
            get { return parentFilterType; }
            set { parentFilterType = value; OnPropertyChanged(); }
        }
        private Type parentFilterType = null;

        [Browsable(false)]
        public ObservableCollection<ICondition> Conditions
        {
            get { return conditions; }
            set
            {
                conditions = value;
                OnPropertyChanged();
            }
        }
        private ObservableCollection<ICondition> conditions = new ObservableCollection<ICondition>();

        [Browsable(false)]
        public Func<bool> OutProtectedFilter
        {
            get
            {
                if (null == conditions || conditions.Count == 0) return null;
                return IOActionFilter;
            }
        }

        private bool IOActionFilter()
        {
            if (!isEnabled) return false;
            if (isAndOparate)
            {
                ICondition mat = conditions.FirstOrDefault((item) => { return item.IsEnabled && item.OutProtectedFilter != null && item.OutProtectedFilter.Invoke() == false; });
                return mat == null;
            }
            else
            {
                ICondition mat = conditions.FirstOrDefault((item) => { return item.IsEnabled && item.OutProtectedFilter != null && item.OutProtectedFilter.Invoke() == true; });
                return mat != null;
            }
        }

        [Browsable(false)]
        public Func<double, bool> AxisProtectedFilter
        {
            get
            {
                if (null == conditions || conditions.Count == 0) return null;
                return AxisActionFilter;
            }
        }

        private bool AxisActionFilter(double tarpos)
        {
            if(!isEnabled) return false;
            if (isAndOparate)//如果两个条件都满足，则执行过滤
            {
                ICondition mat = conditions.FirstOrDefault((item) => { return item != null && item.IsEnabled && item.AxisProtectedFilter != null && item.AxisProtectedFilter.Invoke(tarpos) == false; });
                return mat == null;
            }
            else //只要右一个条件满足则过滤
            {
                ICondition mat = conditions.FirstOrDefault((item) => { return item != null && item.IsEnabled && item.AxisProtectedFilter != null && item.AxisProtectedFilter.Invoke(tarpos) == true; });
                return mat != null;
            }
        }
    }

    /// <summary>
    /// PLC设置输出条件
    /// </summary>
    public class PLCBaseCondition : ICondition
    {
        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged(); }
        }
        private string name = string.Empty;

        public bool IsEnabled
        {
            get { return isEnabled; }
            set
            {
                isEnabled = value;
                OnPropertyChanged();
            }
        }
        private bool isEnabled = true;

        /// <summary>
        /// PLC地址
        /// </summary>
        public string Address
        {
            get { return address; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    address = value;
                    OnPropertyChanged();
                }
            }
        }
        private string address;

        /// <summary>
        /// 是否有值或者高电平
        /// </summary>
        public Type DataType
        {
            get { return _dataType; }
            set
            {
                if (value != null && (value.IsValueType || value == typeof(string)))
                {
                    _dataType = value;
                    OnPropertyChanged();
                }
            }
        }
        private Type _dataType = typeof(float);

        /// <summary>
        /// 移动轴目标位置下限
        /// </summary>
        public bool IsRange
        {
            get { return _isRange; }
            set
            {
                _isRange = value;
                OnPropertyChanged();
            }
        }
        private bool _isRange = false;

        /// <summary>
        /// 目标值
        /// </summary>
        public object TargetValue
        {
            get { return targetValue; }
            set
            {
                targetValue = value;
                OnPropertyChanged();
            }
        }
        private object targetValue = null;

        /// <summary>
        /// 最小值
        /// </summary>
        public double MinValue
        {
            get { return _minValue; }
            set
            {
                _minValue = value;
                OnPropertyChanged();
            }
        }
        private double _minValue = -999;

        /// <summary>
        /// 最小值
        /// </summary>
        public double MaxValue
        {
            get { return _maxValue; }
            set
            {
                _maxValue = value;
                OnPropertyChanged();
            }
        }
        private double _maxValue = 999;

        /// <summary>
        /// 父筛选器类型
        /// </summary>
        [Browsable(false)]
        public virtual Type ParentFilterType
        {
            get { return parentFilterType; }
            set 
            {
                if (value == typeof(OutputFilter))
                {
                    parentFilterType = value;
                    OnPropertyChanged();
                }
            }
        }
        private Type parentFilterType = null;

        /// <summary>
        /// 硬件类型是PLC
        /// </summary>
        [Browsable(false)]
        public DeviceType FilterDevice
        {
            get { return DeviceType.PLC; }
            set { }
        }

        public virtual Func<bool> OutProtectedFilter
        {
            get { return GetPLCValue; }
        }

        public virtual Func<double, bool> AxisProtectedFilter { get; }

        /// <summary>
        /// 获取PLC的值并判断条件
        /// </summary>
        /// <returns></returns>
        protected bool GetPLCValue()
        {
            if (!isEnabled || string.IsNullOrEmpty(address) || null == _dataType) return false;
            if (GloMotionParame.SiemenPLC == null || !GloMotionParame.SiemenPLC.IsConnected) return true;
            if (!GloMotionParame.SiemenPLC.ReadValue(address, _dataType, out dynamic value)) return true;
            if (!IsRange && targetValue.ToString() == ((object)value).ToString())
            {
                return true;
            }
            if (IsRange)
            {
                if (value >= _minValue && value < _maxValue)
                {
                    return true;
                }
            }
            return false;
        }
    }

    /// <summary>
    /// PLC轴移动条件
    /// </summary>
    public class PLCAxisCon : PLCBaseCondition
    {
        /// <summary>
        /// 父筛选器类型
        /// </summary>
        [Browsable(false)]
        public override Type ParentFilterType
        {
            get { return parentFilterType; }
            set
            {
                if (value == typeof(AxisFilter))
                {
                    parentFilterType = value;
                    OnPropertyChanged();
                }        
            }
        }
        private Type parentFilterType = null;

        /// <summary>
        /// 移动轴目标位置下限
        /// </summary>
        public double TarPosLowLimit
        {
            get { return tarPosLowLimit; }
            set
            {
                tarPosLowLimit = value;
                OnPropertyChanged();
            }
        }
        private double tarPosLowLimit = -999;

        /// <summary>
        /// 移动轴目标位置上限
        /// </summary>
        public double TarPosHighLimit
        {
            get { return tarPosHighLimit; }
            set
            {
                tarPosHighLimit = value;
                OnPropertyChanged();
            }
        }
        private double tarPosHighLimit = 999;

        public override Func<double, bool> AxisProtectedFilter
        {
            get { return AxisProtect; }
        }

        private bool AxisProtect(double pos)
        {
            if (pos < TarPosLowLimit || pos > TarPosHighLimit) return false;
            return GetPLCValue();
        }
    }
}
