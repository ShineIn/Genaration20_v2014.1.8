﻿using G4.Motion;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using outputStatus = System.Collections.Generic.KeyValuePair<int, bool>;


namespace Googo.Manul
{
    /// <summary>
    /// 过滤信息
    /// </summary>
    [Serializable]
    public abstract class FilterInfor : PropertyChangedBase, INameBase
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged(); }
        }
        private string name = string.Empty;

        public ObservableCollection<ICondition> Conditions
        {
            get { return conditions; }
            set { conditions = value; OnPropertyChanged(); }
        }
        private ObservableCollection<ICondition> conditions = new ObservableCollection<ICondition>();
    }

    /// <summary>
    /// 输出过滤器
    /// </summary>
    [Serializable]
    public class OutputFilter : FilterInfor
    {
        /// <summary>
        /// 点位序号
        /// </summary>
        public int PortIndex
        {
            get { return portIndex; }
            set
            {
                if (value >= 0)
                {
                    portIndex = value;
                    OnPropertyChanged();
                }
            }
        }
        private int portIndex = 1;

        /// <summary>
        /// 是否有值或者高电平
        /// </summary>
        public bool IsHighValue
        {
            get { return isHighValue; }
            set
            {
                isHighValue = value;
                OnPropertyChanged();
            }
        }
        private bool isHighValue = false;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public KeyValuePair<outputStatus, IEnumerable<Func<bool>>> GetOutPutFilter()
        {
            outputStatus key = new outputStatus(portIndex, isHighValue);
            List<Func<bool>> funcs = new List<Func<bool>>();
            if (null != Conditions && Conditions.Count > 0)
            {
                foreach (var func in Conditions)
                {
                    if (null != func&& func.IsEnabled && null != func.OutProtectedFilter)
                        funcs.Add(func.OutProtectedFilter);
                }
            }
            return new KeyValuePair<outputStatus, IEnumerable<Func<bool>>>(key, funcs);
        }
    }

    /// <summary>
    /// 轴过滤器过滤器
    /// </summary>
    [Serializable]
    public class AxisFilter : FilterInfor
    {
        /// <summary>
        /// 轴号
        /// </summary>
        public EnumAxisType AxisNum
        {
            get { return axisNum; }
            set
            {
                axisNum = value;
                OnPropertyChanged();
            }
        }
        private EnumAxisType axisNum = EnumAxisType.X;

        public KeyValuePair<EnumAxisType, IEnumerable<Func<double, bool>>> GetAxisFilter()
        {
            List<Func<double, bool>> funcs = new List<Func<double, bool>>();
            if (null != Conditions && Conditions.Count > 0)
            {
                foreach (var func in Conditions)
                {
                    if (null != func&& func.IsEnabled && null != func.AxisProtectedFilter)
                        funcs.Add(func.AxisProtectedFilter);
                }
            }
            return new KeyValuePair<EnumAxisType, IEnumerable<Func<double, bool>>>(axisNum, funcs);
        }
    }
}
