﻿using G4.Motion;
using SmartGlue.Infrastructure;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Googo.Manul
{
    /// <summary>
    /// 轴的信息
    /// </summary>
    [Serializable]
    public class AxisInfor : PropertyChangedBase, ICloneable, IEquatable<AxisInfor>, INameBase
    {
        #region 公有属性
        /// <summary>
        /// 轴名称
        /// </summary>
        public string Name
        {
            get { return _name; }
            set
            {
                if (string.IsNullOrEmpty(value)) return;
                _name = value;
                OnPropertyChanged();
            }
        }
        private string _name = "X";

        /// <summary>
        /// 用户名称
        /// </summary>
        public string CustomName
        {
            get { return _customName; }
            set
            {
                if (string.IsNullOrEmpty(value)) return;
                _customName = value;
                OnPropertyChanged();
            }
        }
        private string _customName = "X";

        /// <summary>
        /// 轴号
        /// </summary>
        public EnumAxisType AxisNum
        {
            get { return _axisNum; }
            set { _axisNum = value; OnPropertyChanged(); }
        }
        private EnumAxisType _axisNum = EnumAxisType.X0;

        /// <summary>
        /// 最大速度
        /// </summary>
        public double Speed
        {
            get { return speed; }
            set
            {
                if (value > 0)
                {
                    speed = value;
                    OnPropertyChanged();
                }
            }
        }
        private double speed = 50;

        /// <summary>
        /// 手动速度的比例
        /// </summary>
        public int JogSpeedRate
        {
            get { return jogSpeedRate; }
            set
            {
                if (value > 0 && value <= 100)
                {
                    jogSpeedRate = value;
                    OnPropertyChanged();
                }
            }
        }
        private int jogSpeedRate = 50;

        /// <summary>
        /// 加速度
        /// </summary>
        public double Acc
        {
            get { return acc; }
            set
            {
                if (value > 0)
                {
                    acc = value;
                    OnPropertyChanged();
                }
            }
        }
        private double acc = 1000;

        /// <summary>
        /// 减速度
        /// </summary>
        public double Dec
        {
            get { return dec; }
            set
            {
                if (value > 0)
                {
                    dec = value;
                    OnPropertyChanged();
                }
            }
        }
        private double dec = 1000;

        [XmlIgnore]
        [field: NonSerialized]
        public AxisStatus CurrentStatus
        {
            get
            {
                AxisStatus status = new AxisStatus();
                int iret = MotionModel.Instance.GetAxisStatus(_axisNum, ref status);
                Debug.WriteLine($"get status fail {iret}");
                return status;
            }
        }

        [XmlIgnore]
        [field: NonSerialized]
        public MotionModel BaseModel
        {
            get { return MotionModel.Instance; }
        }
        #endregion 公有属性

        #region 公有函数
        public AxisInfor Clone()
        {
            return new AxisInfor
            {
                _name = _name,
                _customName = _customName,
                _axisNum = _axisNum,
                speed = speed,
                jogSpeedRate = jogSpeedRate,
                acc = acc,
                dec = dec,
            };
        }

        object ICloneable.Clone()
        {
            return Clone();
        }

        public bool Equals(AxisInfor other)
        {
            if (other == null)
                return false;
            if (_name == other._name || _axisNum == other._axisNum)
            {
                return true;
            }
            return false;
        }

        public override bool Equals(object other)
        {
            if (ReferenceEquals(this, other))
                return true;
            return Equals(other as AxisInfor);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public int AxisMoveToHome()
        {
            return MotionModel.Instance.AxisMoveToHome(_axisNum);
        }

        public int GetPosition(EnumAxisType AxisType, out double pos)
        {
            return MotionModel.Instance.GetPosition(_axisNum, out pos);
        }

        public int MoveToSafePosition()
        {
            return MotionModel.Instance.MoveToSafePosition(_axisNum);
        }

        public int StopMoving()
        {
            return MotionModel.Instance.StopMoving(_axisNum);
        }

        public int MoveTo(double pos)
        {
            return MotionModel.Instance.MoveTo(_axisNum, pos, speed, acc);
        }
        #endregion 公有函数
    }
}
