﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Googo.Manul
{
    /// <summary>
    /// 运行报警信息
    /// </summary>
    public class RunErrorInfor:PropertyChangedBase
    {
        /// <summary>
        /// 报警条码
        /// </summary>
        public short AlarmCode
        {
            get { return alarmCode; }
            set { alarmCode = value; OnPropertyChanged(); }
        }
        private short alarmCode = 1200;

        /// <summary>
        /// 报警信息
        /// </summary>
        public string AlarmContent
        {
            get { return alarmContent; }
            set { alarmContent = value; OnPropertyChanged(); }
        }
        private string alarmContent = string.Empty;

        /// <summary>
        /// 解决办法
        /// </summary>
        public string Solution
        {
            get { return solution; }
            set { solution = value; OnPropertyChanged(); }
        }
        private string solution = string.Empty;
    }

    /// <summary>
    /// 模板报警信息
    /// </summary>
    public class StageErrorInfor : PropertyChangedBase
    {
        /// <summary>
        /// 报警地址
        /// </summary>
        public string Address
        {
            get { return address; }
            set { address = value; OnPropertyChanged(); }
        }
        private string address = string.Empty;

        public int Index
        {
            get { return index; }
            set 
            {
                if (value > 0)
                {
                    index = value;
                    OnPropertyChanged();
                }       
            }
        }
        private int index = 1;

        /// <summary>
        /// 报警代码地址
        /// </summary>
        public ObservableCollection<RunErrorInfor> AlarmCodeList
        {
            get { return alarmCodeList; }
            set { alarmCodeList = value; OnPropertyChanged(); }
        }
        private ObservableCollection<RunErrorInfor> alarmCodeList = new ObservableCollection<RunErrorInfor>();
    }
}
