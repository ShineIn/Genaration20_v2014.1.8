﻿using NPOI.SS.UserModel;
using NPOI.Util;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;
using System.Xml.Serialization;
using System.Xml;
using System.ComponentModel;
using System.Globalization;
using Newtonsoft.Json;
using Googo.Manul.Views;

namespace Googo.Manul
{
    /// <summary>
    /// 表格导入的参数
    /// </summary>
    [Serializable]
    public class ArqToPLC : PropertyChangedBase, INameBase
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged(); }
        }
        private string _name = string.Empty;

        /// <summary>
        /// 名称
        /// </summary>
        [XmlIgnore]
        public string DisPlayName
        {
            get { return _disPlayName; }
            set { _disPlayName = value; OnPropertyChanged(); }
        }
        private string _disPlayName = string.Empty;

        /// <summary>
        /// 类型
        /// </summary>
        [XmlIgnore]
        public Type Datatype
        {
            get { return string.IsNullOrEmpty(StrTypeName) ? null : Type.GetType(StrTypeName); }
            set
            {
                StrTypeName = null != value ? value.AssemblyQualifiedName : string.Empty;
                OnPropertyChanged();
            }
        }


        public string StrTypeName { get; set; }


        /// <summary>
        /// 地址
        /// </summary>
        public string Address
        {
            get { return _address; }
            set { _address = value; OnPropertyChanged(); }
        }
        private string _address = string.Empty;

        /// <summary>
        /// 描述
        /// </summary>
        public string Descri
        {
            get { return _descri; }
            set { _descri = value; OnPropertyChanged(); }
        }
        private string _descri = string.Empty;
    }

    /// <summary>
    /// 和PLC的交互特性
    /// </summary>
    public abstract class CommunicaPropertyAttribute : Attribute
    { }

    /// <summary>
    /// 下载特性
    /// </summary>
    public class DownLoadAttribute : CommunicaPropertyAttribute
    { }

    /// <summary>
    /// 上传特性
    /// </summary>
    public class UpLoadAttribute : CommunicaPropertyAttribute
    { }

    /// <summary>
    /// 基础交互块
    /// </summary>
    [Serializable]
    public abstract class CommunicaBase : PropertyChangedBase, INameBase
    {
        #region 公有属性
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name
        {
            get { return _name; }
            set { _name = value; OnPropertyChanged(); }
        }
        private string _name = string.Empty;
        #endregion 公有属性

        /// <summary>
        /// 表格导入参数
        /// </summary>
        [JsonIgnore]
        public ObservableCollection<ArqToPLC> Arquments
        {
            get { return arquments; }
            set { arquments = value; OnPropertyChanged(); }
        }
        private ObservableCollection<ArqToPLC> arquments = new ObservableCollection<ArqToPLC>();



        #region 公有函数
        /// <summary>
        /// 读取PLC的地址
        /// </summary>
        /// <param name="filePath"></param>
        public void ReadExcel(string filePath)
        {
            if (!File.Exists(filePath)) return;
            XSSFWorkbook workbook2007 = new XSSFWorkbook(filePath);

            ISheet sheet = workbook2007.GetSheetAt(0);
            if (null == sheet) return;
            List<ArqToPLC> arqToPLCs = GetSheet(sheet);
            if (null == arqToPLCs) return;
            Arquments = new ObservableCollection<ArqToPLC>(arqToPLCs);
        }

        public void WriteToExcel(string filePath)
        {
            if (System.IO.Path.IsPathRooted(filePath))
            {
                string dir = System.IO.Path.GetDirectoryName(filePath);
                if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
            }
            XSSFWorkbook workbook2007 = new XSSFWorkbook();  //新建xlsx工作簿
            FileStream file2007 = new FileStream(filePath, FileMode.OpenOrCreate);
            ISheet sheet = workbook2007.CreateSheet("Refle");
            WriteSheet(arquments, sheet);
            workbook2007.Write(file2007);
            file2007.Close();
            workbook2007.Close();
        }

        private static int WriteSheet(IList<ArqToPLC> arquments, ISheet sheet)
        {
            if (sheet == null || null == arquments || arquments.Count() == 0) return 0;
            IRow row = sheet.CreateRow(0);
            row.CreateCell(0).SetCellValue("名称");
            row.CreateCell(1).SetCellValue("地址");
            row.CreateCell(2).SetCellValue("类型");
            row.CreateCell(3).SetCellValue("备注");
            for (int i = 0; i < arquments.Count(); i++)
            {
                ArqToPLC arq = arquments[i];
                if (null == arq) continue;
                row = sheet.CreateRow(i + 1);
                ICell cell = row.CreateCell(0);
                cell.SetCellValue(arq.Name);
                cell = row.CreateCell(1);
                cell.SetCellValue(arq.Address);
                cell = row.CreateCell(2);
                cell.SetCellValue(arq.Datatype.Name);
                cell = row.CreateCell(3);
                cell.SetCellValue(arq.Descri);
            }
            return 0;
        }


        private static List<ArqToPLC> GetSheet(ISheet sheet)
        {
            IRow row = sheet.GetRow(0);
            int proIndex = -1;
            int addreIndex = -1;
            int typeIndex = -1;
            int descriIndex = -1;
            List<ArqToPLC> arquments = new List<ArqToPLC>();
            for (int k = 0; k < row.LastCellNum; k++)
            {
                string proName = row.GetCell(k).ToString();
                if (string.IsNullOrEmpty(proName)) continue;
                if (proName.Contains("地址"))
                {
                    addreIndex = k;
                }
                else if (proName.Contains("名称"))
                {
                    proIndex = k;
                }
                else if (proName.Contains("类型"))
                {
                    typeIndex = k;
                }
                else if (proName.Contains("备注"))
                {
                    descriIndex = k;
                }
                if (addreIndex != -1 && proIndex != -1 && typeIndex != -1 && descriIndex != -1)
                {
                    break;
                }
            }
            for (int i = 1; i <= sheet.LastRowNum; i++)
            {
                ArqToPLC arqu = new ArqToPLC();
                row = sheet.GetRow(i);
                ICell cell = row.GetCell(proIndex);
                if (cell == null || cell.ToString() == string.Empty) continue;
                arqu.Name = cell.ToString();
                cell = row.GetCell(addreIndex);
                if (cell == null || cell.ToString() == string.Empty) continue;
                arqu.Address = cell.ToString();
                cell = row.GetCell(descriIndex);
                if (cell == null || cell.ToString() == string.Empty) continue;
                arqu.Descri = cell.ToString();
                cell = row.GetCell(typeIndex);
                if (cell == null || cell.ToString() == string.Empty) continue;
                string strType = cell.ToString().Trim().ToLower();
                Type type = ConveType(strType);
                if (null == type) continue;
                arqu.Datatype = type;
                arquments.Add(arqu);
            }
            return arquments;
        }

        /// <summary>
        /// 上传所有的属性参数
        /// </summary>
        public virtual void UpLoads()
        {
            PropertyInfo[] propertyInfos = GetType().GetProperties();
            if (null == propertyInfos || 0 == propertyInfos.Count()) return;
            foreach (PropertyInfo pro in propertyInfos)
            {
                if (pro == null || !pro.CanRead) continue;
                object setvalue = pro.GetValue(this);
                if (null == setvalue) continue;
                if (pro.PropertyType.IsValueType || pro.PropertyType == typeof(string))
                {
                    UpLoadPro(setvalue, pro);
                }
                else if (pro.PropertyType.BaseType == typeof(CommunicaBase))
                {
                    ((CommunicaBase)setvalue).UpLoads();
                }
                else if (setvalue is System.Collections.IEnumerable ttList)
                {
                    foreach (var item in ttList)
                    {
                        if (null == item) continue;
                        if (item.GetType().BaseType != typeof(CommunicaBase))
                        {
                            break;
                        }
                        ((CommunicaBase)item).UpLoads();
                    }
                }
            }
        }

        /// <summary>
        /// 上传属性的值
        /// </summary>
        /// <param name="setvalue"></param>
        /// <param name="PropertyName"></param>
        /// <returns></returns>
        protected virtual bool UpLoadPro(object setvalue, [CallerMemberName] string PropertyName = null)
        {
            PropertyInfo[] propertyInfos = GetType().GetProperties();
            if (null == propertyInfos || 0 == propertyInfos.Count()) return false;
            var match = propertyInfos.FirstOrDefault((tt) => { return tt.Name == PropertyName; });
            if (null != match)
                return UpLoadPro(setvalue, match);
            return false;
        }

        /// <summary>
        /// 下载所有的属性参数
        /// </summary>
        public virtual void DownLoad()
        {
            PropertyInfo[] propertyInfos = GetType().GetProperties();
            if (null == propertyInfos || 0 == propertyInfos.Count()) return;
            foreach (PropertyInfo pro in propertyInfos)
            {
                if (pro == null) continue;
                object hh = DownLoadPro(pro);
                if (null != hh && pro.CanWrite)
                    pro.SetValue(this, hh);
                if (pro == null || pro.CanRead) continue;
                object setvalue = pro.GetValue(this);
                if (null == setvalue) continue;
                if (pro.PropertyType.IsValueType || pro.PropertyType == typeof(string))
                {
                    UpLoadPro(setvalue, pro);
                }
                else if (pro.PropertyType.BaseType == typeof(CommunicaBase))
                {
                    ((CommunicaBase)setvalue).UpLoads();
                }
                else if (setvalue is System.Collections.IEnumerable ttList)
                {
                    foreach (var item in ttList)
                    {
                        if (null == item) continue;
                        if (item.GetType().BaseType != typeof(CommunicaBase))
                        {
                            break;
                        }
                        ((CommunicaBase)item).UpLoads();
                    }
                }
            }
        }

        /// <summary>
        /// 获取属性对应地址的值
        /// </summary>
        /// <param name="PropertyName"></param>
        /// <returns></returns>
        protected virtual object DownLoadPro([CallerMemberName] string PropertyName = null)
        {
            PropertyInfo[] propertyInfos = GetType().GetProperties();
            if (null == propertyInfos || 0 == propertyInfos.Count()) return null;
            var match = propertyInfos.FirstOrDefault((tt) => { return tt.Name == PropertyName; });
            if (null == match) return null;
            return DownLoadPro(match);
        }

        public void CheckArquments()
        {
            if (null == arquments) arquments = new ObservableCollection<ArqToPLC>();
            PropertyInfo[] propertyInfos = GetType().GetProperties();
            if (null == propertyInfos || 0 == propertyInfos.Count()) return;
            foreach (PropertyInfo item in propertyInfos)
            {
                if (null == item) continue;
                IEnumerable<CommunicaPropertyAttribute> attributes = item.GetCustomAttributes<CommunicaPropertyAttribute>();
                if (null == attributes || attributes.Count() == 0) continue;
                string strDisp = item.GetLabDispContent();
                if (item.PropertyType.IsValueType || item.PropertyType == typeof(string))
                {
                    var match = arquments.FirstOrDefault((tt) => { return tt.Name == item.Name; });
                    if (null == match)
                    {
                        match = new ArqToPLC { Name = item.Name, Datatype = item.PropertyType };
                        arquments.Add(match);
                    }
                    match.DisPlayName = strDisp;
                }
            }
        }
        #endregion 公有函数

        #region 私有函数
        private bool UpLoadPro(object setvalue, PropertyInfo pro)
        {
            Debug.Assert(null != pro && null != setvalue);
            if (null == arquments || arquments.Count == 0) return false;
          var list=  pro.GetCustomAttributes<UpLoadAttribute>();
            if (null == list || list.Count() == 0) return false;
            UpLoadAttribute update = list.ElementAtOrDefault(0);
            if (null == update) return false;
            ArqToPLC arqument = arquments.FirstOrDefault((tt) => { return tt.Datatype == pro.PropertyType && tt.Name == pro.Name; });
            if (null == arqument || string.IsNullOrEmpty(arqument.Address)) return false;
            if (null == GloMotionParame.SiemenPLC || !GloMotionParame.SiemenPLC.IsConnected) return false;
            return GloMotionParame.SiemenPLC.WriteValue(arqument.Address, setvalue);
        }

        public virtual bool IsGetFromPLC
        {
            get { return _isGetFromPLC; }
            set
            {
                _isGetFromPLC = value;
                SetChildren(value);
                OnPropertyChanged();
            }
        }
        private bool _isGetFromPLC = true;
        /// <summary>
        /// 获取某个属性的值
        /// </summary>
        /// <param name="pro"></param>
        /// <returns></returns>
        private object DownLoadPro(PropertyInfo pro)
        {
            Debug.Assert(null != pro);
            if (null == arquments || arquments.Count == 0 || !IsGetFromPLC) return null;
            var list = pro.GetCustomAttributes<DownLoadAttribute>();
            if (null == list || list.Count() == 0) return false;
            DownLoadAttribute update = list.ElementAtOrDefault(0);
            if (null == update) return null;
            ArqToPLC arqument = arquments.FirstOrDefault((tt) => { return tt.Datatype == pro.PropertyType && tt.Name == pro.Name; });
            if (null == arqument || string.IsNullOrEmpty(arqument.Address)) return null;
            if (null == GloMotionParame.SiemenPLC || !GloMotionParame.SiemenPLC.IsConnected) return null;
            GloMotionParame.SiemenPLC.ReadValue(arqument.Address, pro.PropertyType, out dynamic reValue);
            if (null == reValue) return null;
            Type type = reValue.GetType();
            if (pro.PropertyType.IsAssignableFrom(type))
            {
                if (!dic.ContainsKey(pro.Name) || dic[pro.Name] != reValue)
                {
                    dic[pro.Name] = reValue;
                    OnPropertyChanged(pro.Name);
                }
                return reValue;
            }
            return null;
        }

        private static Type ConveType(string strType)
        {
            if (string.IsNullOrEmpty(strType)) return null;
            else if (strType.Contains("int")) return typeof(int);
            else if (strType.Contains("bool")) return typeof(bool);
            else if (strType.Contains("float") || strType.Contains("real")) return typeof(float);
            else if (strType.Contains("uint")) return typeof(uint);
            else if (strType.Contains("double")) return typeof(double);
            else if (strType.Contains("ushort")) return typeof(ushort);
            return null;
        }

        private void SetChildren(bool setValue)
        {
            Type baseType = typeof(CommunicaBase);
            PropertyInfo[] propertyInfos = baseType.GetProperties();
            if (null == propertyInfos || 0 == propertyInfos.Count()) return;
            foreach (PropertyInfo item in propertyInfos)
            {
                if (null == item) continue;
                object pro = item.GetValue(this);
                if (null == pro) continue;
                if (baseType.IsAssignableFrom(item.PropertyType))
                {
                    ((CommunicaBase)pro).IsGetFromPLC = setValue;
                }

            }
        }
        #endregion 私有函数


        private readonly Dictionary<string, dynamic> dic = new Dictionary<string, dynamic>();
    }

}
