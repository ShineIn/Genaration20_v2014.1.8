﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleData
{
    [Serializable]
    public sealed class PropertyUnEditAttribute : Attribute
    {
        public PropertyUnEditAttribute() { }
    }
}
