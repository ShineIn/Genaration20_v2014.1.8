﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleData
{
    /// <summary>
    /// 参数特性
    /// </summary>
    public abstract class ToolTerminalBaseAttribute : Attribute
    {
        public string Name { get; }

        public string Path { get; }

        public ToolTerminalBaseAttribute(string name, string path)
        {
            Name = name;
            Path = path;
        }
    }

    /// <summary>
    /// 输入端
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public sealed class ToolInputTerminalAttribute : ToolTerminalBaseAttribute
    {
        public ToolInputTerminalAttribute(string name, string path)
            : base(name, path)
        {
        }
    }

    /// <summary>
    /// 输出端
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public sealed class ToolOutputTerminalAttribute : ToolTerminalBaseAttribute
    {
        public ToolOutputTerminalAttribute(string name, string path)
            : base(name, path)
        {
        }
    }
}
