﻿using Googo.Manul;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace ModuleData
{
    public class PositionConv : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue || null == value) return null;
            if (value is PositionInfor positionInfor)
                return $"{positionInfor.XPos},{positionInfor.YPos},{positionInfor.ZPos}";
           else if (value is Point point)
                return $"{point.X},{point.Y}";
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            string strCo = value.ToString();
            string[] arry = strCo.Split(new char[] { ',', ';', '，', '；',';' });
            if (arry == null || arry.Length == 0) return null;
            if (targetType == typeof(PositionInfor))
            {
                if (arry.Length < 3) return null;
                if (double.TryParse(arry[0].Trim(), out double xpos) && double.TryParse(arry[1].Trim(), out double ypos)
                    && double.TryParse(arry[2].Trim(), out double zpos))
                {
                    return new PositionInfor { XPos = xpos, YPos = ypos, ZPos = zpos };
                }
            }
            if (targetType == typeof(Point))
            {
                if (arry.Length <2) return null;
                if (double.TryParse(arry[0].Trim(), out double xpos) && double.TryParse(arry[1].Trim(), out double ypos)           )
                {
                    return new Point { X = xpos, Y = ypos};
                }
            }
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public class MultiPositionConv : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == DependencyProperty.UnsetValue || null == value) return null;
            sourceType=value.GetType();
            if (value is PositionInfor positionInfor)
                return $"{positionInfor.XPos},{positionInfor.YPos},{positionInfor.ZPos}";
            else if (value is Point point)
                return $"{point.X},{point.Y}";
            else if(value is double xpos)
                return xpos.ToString();
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return null;
            string strCo = value.ToString();
            string[] arry = strCo.Split(new char[] { ',', ';', '，', '；', ';' });
            if (arry == null) return null;
            if (sourceType == typeof(PositionInfor))
            {
                if (arry.Length < 3) return null;
                if (double.TryParse(arry[0].Trim(), out double xpos) && double.TryParse(arry[1].Trim(), out double ypos)
                    && double.TryParse(arry[2].Trim(), out double zpos))
                {
                    return new PositionInfor { XPos = xpos, YPos = ypos, ZPos = zpos };
                }
            }
            if (sourceType == typeof(Point))
            {
                if (arry.Length < 2) return null;
                if (double.TryParse(arry[0].Trim(), out double xpos) && double.TryParse(arry[1].Trim(), out double ypos))
                {
                    return new Point { X = xpos, Y = ypos };
                }
            }
           else if (sourceType == typeof(double))
            {
                if (double.TryParse(arry[0].Trim(), out double xpos))
                {
                    return xpos;
                }
            }
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        private Type sourceType;
    }
}
