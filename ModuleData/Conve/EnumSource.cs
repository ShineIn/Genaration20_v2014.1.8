﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Markup;

namespace ModuleData
{
   public class EnumSource :MarkupExtension
    {
        public EnumSource(Type type)
        {
            if (null == type || !type.IsEnum)
                throw new Exception("type is not enumType");
            _type=type;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return Enum.GetValues(_type);
        }

        private Type _type;
    }
}
