﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Googo.Manul;


namespace ModuleData
{
    /// <summary>
    /// 称重绑定模板
    /// </summary>
    [Serializable]
    public class WeightModel : CommunicaBase, IGlueChildModel
    {
        #region 称重属性
        /// <summary>
        /// 触发称重指令
        /// </summary>
        [DownLoad]
        [XmlIgnore]
        public bool PLC_Sig_AutoWei
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                _pLC_Sig_AutoWei = (bool)obj;             
                return _pLC_Sig_AutoWei;
            }
            set
            {
                _pLC_Sig_AutoWei = value;
                OnPropertyChanged();
            }
        }
        private bool _pLC_Sig_AutoWei = false;

        /// <summary>
        /// 称重运行中
        /// </summary>
        [XmlIgnore,  UpLoad]
        public bool SoftSigAutoWei
        {
            get { return _softSigAutoWei; }
            set
            {
                _softSigAutoWei = value;
                softSigAutoWeiFin = !value;
                UpLoadPro(value);
                UpLoadPro(softSigAutoWeiFin, nameof(SoftSigAutoWeiFin));
                OnPropertyChanged();
                OnPropertyChanged(nameof(SoftSigAutoWeiFin));
            }
        }
        bool _softSigAutoWei = false;

        /// <summary>
        /// 称重完成
        /// </summary>
        [XmlIgnore,  UpLoad,DownLoad]       
        public bool SoftSigAutoWeiFin
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                softSigAutoWeiFin = (bool)obj;
                return softSigAutoWeiFin;
            }
            set
            {
                softSigAutoWeiFin = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        bool softSigAutoWeiFin = true;
        /// <summary>
        /// 称重结果  OK
        /// </summary>
        [XmlIgnore,  UpLoad,DownLoad]
        public bool SigAutoWeiOK
        {
            get 
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                _sigAutoWeiOK = (bool)obj;
                return _sigAutoWeiOK;
            }
            set
            {
                _sigAutoWeiOK = value;
                UpLoadPro(value);
                OnPropertyChanged();             
            }
        }
        bool _sigAutoWeiOK = true;

        /// <summary>
        /// 称重结果  NG
        /// </summary>
        [XmlIgnore,  UpLoad,DownLoad]
        public bool SigAutoWeiNG
        {
            get 
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                _sigAutoWeiNG = (bool)obj;
                return _sigAutoWeiNG;
            }
            set 
            {
                _sigAutoWeiNG = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        bool _sigAutoWeiNG = false;

        /// <summary>
        /// 称重的最终重量
        /// </summary>
        [XmlIgnore,  UpLoad]
        [field: NonSerialized]
        public float WeightometerValue
        {
            get { return weightometerValue; }
            set { weightometerValue = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        [NonSerialized]
        float weightometerValue = 0;

        /// <summary>
        /// 称重的模板序号
        /// </summary>
        public short GlueModuleNum
        {
            get { return glueModuleNum; }
            set { glueModuleNum = value; OnPropertyChanged(); }
        }
        private short glueModuleNum = 0;
        /// <summary>
        /// 称重上限
        /// </summary>
        [XmlIgnore, DownLoad]
        public float WeightometerValueMax
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return 100;
                weightometerValueMax = (float)obj;
                return weightometerValueMax;
            }
            set
            {
                weightometerValueMax = value;
                OnPropertyChanged();
            }
        }
        private float weightometerValueMax = 100;
        /// <summary>
        /// 称重下限
        /// </summary>
        [XmlIgnore, DownLoad]
        public float WeightometerValueMin
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return 0;
                weightometerValueMin = (float)obj;
                return weightometerValueMin;
            }
            set
            {
                weightometerValueMin = value;
                OnPropertyChanged();
            }
        }
        private float weightometerValueMin = 0;

        /// <summary>
        /// 称重串口号
        /// </summary>
        public string SeriPortName
        {
            get { return _seriPortName; }
            set { _seriPortName = value; OnPropertyChanged(); }
        }
        private string _seriPortName = "Com";
        /// <summary>
        /// 称重波特率
        /// </summary>
        public int BaudRate
        {
            get { return baudRate; }
            set { baudRate = value; OnPropertyChanged(); }
        }
        private int baudRate = 38400;
        #endregion

        [XmlIgnore]
        public IGlueParent ParentProject
        {
            get { return parentProject; }
            set { parentProject = value; OnPropertyChanged(); }
        }
        private IGlueParent parentProject = null;
    }
}
