﻿using G4.GlueCore;
using Googo.Manul;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleData
{
    /// <summary>
    /// 点胶模组父项目接口
    /// </summary>
    public interface IGlueParent
    {
        GlueModuleParams GluePram { get; set; }

        PositionInfor GlueSafetyPos { get; set; }

        /// <summary>
        /// 点胶模组设置
        /// </summary>
        EnumWorkModule AxisWorkModule { get; set; }

        /// <summary>
        /// 自动信号
        /// </summary>
        bool Auto_Program { get; set; }

        /// <summary>
        /// 软件给PLC的自动信号
        /// </summary>
        bool SoftWareAuto { get; set; }

        /// <summary>
        /// PLC下发任务号地址
        /// </summary>
        short ProcessAddre { get; set; }

        /// <summary>
        /// 上传任务号地址
        /// </summary>
        short UpProcessAddre { get; set; }

        /// <summary>
        /// 待机状态地址
        /// </summary>
        bool PLC_StandbyPosition { get; set; }

        /// <summary>
        /// 是否包含UV
        /// </summary>
        bool IsContainUV { get; set; }


        /// <summary>
        /// UV下降输出
        /// </summary>
         int UVDownOut { get; set; }

        /// <summary>
        /// UV上升输出
        /// </summary>
        int UVUpOut { get; set; }

        /// <summary>
        /// UV下降输入
        /// </summary>
        int UVDownIn { get; set; }

        /// <summary>
        /// UV上升输出
        /// </summary>
        int UVUpIn { get; set; }

        /// <summary>
        /// UVIP
        /// </summary>
        string StrUVIP { get; set; }

        /// <summary>
        /// UV固化位置
        /// </summary>
        PositionInfor UVPos { get; set; }

        /// <summary>
        /// UV端口
        /// </summary>
        int UVPortNum { get; set; }
    }

    /// <summary>
    /// 点胶模组子项接口
    /// </summary>
    public interface IGlueChildModel
    {
        IGlueParent ParentProject { get; set; }
    }

}
