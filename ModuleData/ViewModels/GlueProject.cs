﻿using G4.GlueCore;
using G4.Motion;
using Googo.Manul;
using ModuleData.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ModuleData
{
    /// <summary>
    /// 点胶项目参数
    /// </summary>
    [Serializable]
    public class GlueProject : CommunicaBase, IGlueParent, IStationModule
    {
        /// <summary>
        /// 点胶参数
        /// </summary>
        public GlueModuleParams GluePram
        {
            get { return gluePram; }
            set { gluePram = value; OnPropertyChanged(); }
        }
        private GlueModuleParams gluePram = new GlueModuleParams();

        /// <summary>
        /// 安全点
        /// </summary>
        public PositionInfor GlueSafetyPos
        {
            get { return glueSafetyPos; }
            set { glueSafetyPos = value; OnPropertyChanged(); }
        }
        private PositionInfor glueSafetyPos = new PositionInfor();

        /// <summary>
        /// 自动对针参数
        /// </summary>
        [DisplayName("自动对针参数")]
        public AutoCalibraNeedle AutoCalibra
        {
            get { return autoCalibra; }
            set 
            {
                autoCalibra = value;
                OnPropertyChanged();
                if(value!=null) value.ParentProject = this;
            }
        }
        private AutoCalibraNeedle autoCalibra = new AutoCalibraNeedle();

        /// <summary>
        /// 抛胶参数
        /// </summary>
        [DisplayName("抛胶参数")]
        public DropGlueModel DropData
        {
            get { return dropData; }
            set
            {
                dropData = value;
                OnPropertyChanged();
                if (value != null) value.ParentProject = this;
            }
        }
        private DropGlueModel dropData = new DropGlueModel();

        /// <summary>
        /// 点胶执行参数
        /// </summary>
        [DisplayName("点胶执行参数")]
        public GlueModel GlueExcuteModel
        {
            get { return glueExcuteModel; }
            set
            {
                glueExcuteModel = value;
                OnPropertyChanged();
                if (value != null) value.ParentProject = this;
            }
        }
        private GlueModel glueExcuteModel = new GlueModel();

        /// <summary>
        /// 擦针参数
        /// </summary>
        [DisplayName("擦针参数")]
        public ClearNeedleModel ClearMode
        {
            get { return clearMode; }
            set
            {
                clearMode = value;
                OnPropertyChanged();
                if (value != null) value.ParentProject = this;
            }
        }
        private ClearNeedleModel clearMode = new ClearNeedleModel();

        /// <summary>
        /// 称重参数
        /// </summary>
        [DisplayName("称重参数")]
        public WeightModel WeightModel
        {
            get { return weightModel; }
            set
            {
                weightModel = value;
                OnPropertyChanged();
                if (value != null) value.ParentProject = this;
            }
        }
        private WeightModel weightModel = new WeightModel();

        /// <summary>
        /// 点检参数
        /// </summary>
        [DisplayName("点检参数")]
        public SpotCheck SpotCheckModel
        {
            get { return spotCheckModel; }
            set
            {
                spotCheckModel = value;
                OnPropertyChanged();
                if (value != null) value.ParentProject = this;
            }
        }
        private SpotCheck spotCheckModel = new SpotCheck();

        /// <summary>
        /// 点胶模组设置
        /// </summary>
        public EnumWorkModule AxisWorkModule
        {
            get { return axisWorkModule; }
            set 
            {
                axisWorkModule = value;
                OnPropertyChanged();
                if (null != gluePram)
                    ExcuteGlueModule =new GlueModule(value, gluePram);
            }
        }
        EnumWorkModule axisWorkModule = EnumWorkModule.S10;

        /// <summary>
        /// 自动信号
        /// </summary>
        [DownLoad, XmlIgnore]
        public bool Auto_Program
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                auto_Program = (bool)obj;
                return auto_Program;
            }
            set
            {
                auto_Program = value;
                OnPropertyChanged();
            }
        }
        bool auto_Program = false;

        /// <summary>
        /// 软件给PLC的自动信号
        /// </summary>
        [UpLoad, XmlIgnore]
        public bool SoftWareAuto
        {
            get
            {
                return softWareAuto;
            }
            set
            {
                softWareAuto = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        bool softWareAuto = false;

        /// <summary>
        /// PLC->运控任务地址
        /// </summary>
        [DownLoad, XmlIgnore]
        public short ProcessAddre
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is short)) return -1;
                processAddre = (short)obj;
                return processAddre;
            }
            set
            {
                processAddre = value;
                OnPropertyChanged();
            }
        }
        short processAddre = 1;

        /// <summary>
        /// 运控->PLC任务地址
        /// </summary>
        [UpLoad, XmlIgnore]
        public short UpProcessAddre
        {
            get
            {
                return upProcessAddre;
            }
            set
            {
                upProcessAddre = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        short upProcessAddre = 1;

        /// <summary>
        /// 是否包含UV
        /// </summary>
        public bool IsContainUV
        {
            get { return isContainUV; }
            set { isContainUV = value;OnPropertyChanged(); }
        }
        private bool isContainUV=false;

        /// <summary>
        /// UV下降输出
        /// </summary>
        public int UVDownOut
        {
            get { return _uVDownOut; }
            set { _uVDownOut = value; OnPropertyChanged(); }
        }
        int _uVDownOut = 5;

        /// <summary>
        /// UV上升输出
        /// </summary>
        public int UVUpOut
        {
            get { return _uVUpOut; }
            set { _uVUpOut = value; OnPropertyChanged(); }
        }
        int _uVUpOut = 5;

        /// <summary>
        /// UV下降输入
        /// </summary>
        public int UVDownIn
        {
            get { return _uVDownIn; }
            set { _uVDownIn = value; OnPropertyChanged(); }
        }
        int _uVDownIn = 5;

        /// <summary>
        /// UV上升输出
        /// </summary>
        public int UVUpIn
        {
            get { return _uVUpIn; }
            set { _uVUpIn = value; OnPropertyChanged(); }
        }
        int _uVUpIn = 5;

        /// <summary>
        /// UVIP
        /// </summary>
        public string StrUVIP
        {
            get { return strUVIPStrUVIP; }
            set { strUVIPStrUVIP = value; OnPropertyChanged(); }
        }
        string strUVIPStrUVIP = "10.66.81.98";

  

      
        /// <summary>
        /// UV固化位置
        /// </summary>
        public PositionInfor UVPos
        {
            get { return _uVPos; }
            set { _uVPos = value; OnPropertyChanged(); }
        }
        PositionInfor _uVPos = new PositionInfor();

        /// <summary>
        /// UV端口
        /// </summary>
        public int UVPortNum
        {
            get { return _uVPortNum; }
            set { _uVPortNum = value; OnPropertyChanged(); }
        }
        int _uVPortNum = 502;

        /// <summary>
        /// 一次固化时间
        /// </summary>
        [DownLoad, DisplayName("一次UV时间")]
        public float FirstUVTime
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return firstUVTime;
                firstUVTime = (float)obj;
                return firstUVTime;
            }
            set
            {
                firstUVTime = value;
                //UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        float firstUVTime = 10000f;

        /// <summary>
        /// 一次固化强度
        /// </summary>
        //[UpLoad, XmlIgnore]
        public float FirstUVPower
        {
            get
            {
                return firstUVPower;
            }
            set
            {
                firstUVPower = value;
                //UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        float firstUVPower = 60f;

        /// <summary>
        /// PLC待机位置信号
        /// </summary>
        [UpLoad, XmlIgnore]
        public bool PLC_StandbyPosition
        {
            get
            {
                return pLC_StandbyPosition;
            }
            set
            {
                pLC_StandbyPosition = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        bool pLC_StandbyPosition = true;


        /// <summary>
        /// 点胶模组待机状态标志位
        /// </summary>
        [XmlIgnore]
        public bool GlueStandbyFlag
        {
            get
            {
                return glueStandbyFlag;
            }
            set
            {
                glueStandbyFlag = value;
                OnPropertyChanged();
            }
        }
        bool glueStandbyFlag = true;

        /// <summary>
        /// 是否可编辑映射按钮
        /// </summary>
        public bool IsEnableEdit
        {
            get { return isEnableEdit; }
            set { isEnableEdit = value; OnPropertyChanged(); }
        }
        private bool isEnableEdit = false;

        /// <summary>
        /// 是否可编辑参数表
        /// </summary>
        public bool IsEnableEditTabel
        {
            get { return isEnableEditTabel; }
            set { isEnableEditTabel = value; OnPropertyChanged(); }
        }
        private bool isEnableEditTabel = false;


        [XmlIgnore]
        public GlueModule ExcuteGlueModule
        {
            get
            {
                if (excuteGlueModule == null&&(null!= gluePram&&File.Exists(gluePram.GluePathFileName)))
                    excuteGlueModule = new GlueModule(axisWorkModule,gluePram);   
                return excuteGlueModule; 
            }
           set { excuteGlueModule = value; }
        }
        private GlueModule excuteGlueModule = null;

        [XmlIgnore]
        public IStationParent ParentStation { get ; set; }

        [XmlIgnore]
        public ModuleRunStatus StationStatus
        {
            get { return stationStatus; }
            set { stationStatus = value;OnPropertyChanged(); }
        }
        private  ModuleRunStatus stationStatus = new ModuleRunStatus();

        public bool UVLightUp()
        {
            if (!isContainUV) return true;
            MotionModel.Instance.SetDo(_uVUpOut, true);
            MotionModel.Instance.SetDo(_uVDownOut, false);
            bool isuvDown = true;
            bool isuvUpIn = false;
            int RetryNum = 0;
            while ((isuvDown||!isuvUpIn)&&RetryNum<20) 
            {
                Thread.Sleep(200);
                MotionModel.Instance.GetDi(_uVDownIn, out isuvDown);
                MotionModel.Instance.GetDi(_uVUpIn, out isuvUpIn);
                RetryNum++;
            }       
            return !isuvDown && isuvUpIn;      
        }

        public bool UVLightDown()
        {
            if (!isContainUV) return true;
            MotionModel.Instance.SetDo(_uVUpOut, false);
            MotionModel.Instance.SetDo(_uVDownOut, true);
            bool isuvDown = false;
            bool isuvUpIn = true;
            int RetryNum = 0;
            while ((!isuvDown || isuvUpIn) && RetryNum < 20)
            {
                Thread.Sleep(200);
                MotionModel.Instance.GetDi(_uVDownIn, out isuvDown);
                MotionModel.Instance.GetDi(_uVUpIn, out isuvUpIn);
                RetryNum++;
            }
            return isuvDown && !isuvUpIn;
        }
    }
}
