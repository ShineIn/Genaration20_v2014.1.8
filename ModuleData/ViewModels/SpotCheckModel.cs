﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Windows;
using Googo.Manul;
using System.ComponentModel;
using System.Diagnostics;

namespace ModuleData
{
    /// <summary>
    /// 通用交互信号
    /// </summary>
    public class CommonSignal : CommunicaBase
    {
        /// <summary>
        /// 启动信号
        /// </summary>
        [DownLoad, XmlIgnore, DisplayName("启动信号")]
        public bool StartSignal
        {
            get
            {
                object temp = DownLoadPro();
                if (null == temp || !(temp is bool)) return false;
                _startSignal = (bool)temp;
                return _startSignal;
            }
            set { _startSignal = value; OnPropertyChanged(); }
        }
        bool _startSignal = false;

        /// <summary>
        /// 运行中
        /// </summary>
        [UpLoad, DownLoad, XmlIgnore, DisplayName("运行中")]
        public bool RunIng
        {
            get
            {
                object temp = DownLoadPro();
                if (null == temp || !(temp is bool)) return false;
                _runIng = (bool)temp;
                return _runIng;
            }
            set { _runIng = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        bool _runIng = false;

        /// <summary>
        /// 运行结束
        /// </summary>
        [UpLoad, XmlIgnore, DisplayName("运行结束")]
        public bool RunFinish
        {
            get
            {
                return _runFinish;
            }
            set
            {
                _runFinish = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        bool _runFinish = false;

        /// <summary>
        /// 运行结果OK
        /// </summary>
        [UpLoad, XmlIgnore, DisplayName("运行结果OK")]
        public bool ResultOK
        {
            get
            {
                return _resultOK;
            }
            set { _resultOK = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        bool _resultOK = false;

        /// <summary>
        /// 运行结果NG
        /// </summary>
        [UpLoad, XmlIgnore, DisplayName("运行结果NG")]
        public bool ResultNG
        {
            get
            {
                return _resultNG;
            }
            set { _resultNG = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        bool _resultNG = false;

        /// <summary>
        /// 给运行中信号并等待启动信号复位
        /// </summary>
        /// <remarks>
        /// 5S超时，即循环写入100次
        /// </remarks>
        /// <returns></returns>
        public bool WaitStartReset()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            bool brun = true;
            while (brun && stopwatch.ElapsedMilliseconds < 5000)
            {
                //清除结果后再给运行中
                ResultNG = false;
                ResultOK = false;
                RunFinish = false;
                RunIng = true;
                System.Threading.Thread.Sleep(50);
                brun = StartSignal;
            }
            return !StartSignal;
        }

        /// <summary>
        /// 给结果到PLC，并且等待运行中信号取消
        /// </summary>
        /// <param name="bResult">结果，OK或者NG</param>
        /// <returns></returns>
        public bool WaitRuningReset(bool bResult)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            bool brun = true;
            while (brun && stopwatch.ElapsedMilliseconds < 5000)
            {
                ResultOK = bResult;
                ResultNG = !bResult;
                RunFinish = true;
                System.Threading.Thread.Sleep(50);
                brun = RunIng;
            }
            return !RunIng;
        }

        /// <summary>
        /// 重置给到PLC的信号
        /// </summary>
        public void Reset()
        {
            ResultOK = false;
            ResultNG = false;
            RunFinish = false;
            RunIng = false;
        }
    }

    /// <summary>
    /// 自动对针绑定模板
    /// </summary>
    [Serializable]
    public class SpotCheck : CommunicaBase, IGlueChildModel
    {
        /// <summary>
        /// 产品点胶复检视觉点检信号
        /// </summary>
        [DisplayName("视觉点检")]
        public CommonSignal VisionRecheck
        {
            get { return visionRecheck; }
            set { visionRecheck = value; OnPropertyChanged(); }
        }
        private CommonSignal visionRecheck = new CommonSignal();

        /// <summary>
        /// 产品测高点检信号
        /// </summary>
        [DisplayName("基恩士测高")]
        public CommonSignal KeyenceHeightLaserTest
        {
            get { return keyenceHeightLaserTest; }
            set { keyenceHeightLaserTest = value; OnPropertyChanged(); }
        }
        private CommonSignal keyenceHeightLaserTest = new CommonSignal();

        /// <summary>
        ///点检标志视觉点检
        /// </summary>
        [DisplayName("点检标志块视觉点检")]
        public CommonSignal VisionMark
        {
            get { return _visionMark; }
            set { _visionMark = value; OnPropertyChanged(); }
        }
        private CommonSignal _visionMark = new CommonSignal();


        /// <summary>
        /// 点检视觉拍照位置
        /// </summary>
        public Point AutoCheckVisPoint
        {
            get { return autoCheckVisPoint; }
            set
            {
                autoCheckVisPoint = value; OnPropertyChanged();
            }
        }
        Point autoCheckVisPoint = new Point(0, 0);
        /// <summary>
        /// 点检视觉高度
        /// </summary>
        public double AutoCheckVisZ
        {
            get { return autoCheckVisZ; }
            set { autoCheckVisZ = value; OnPropertyChanged(); }
        }
        double autoCheckVisZ = 0;


        /// <summary>
        /// 点检激光测高位置
        /// </summary>
        public Point AutoCheckLaserPoint
        {
            get { return autoCheckLaserPoint; }
            set
            {
                autoCheckLaserPoint = value; OnPropertyChanged();
            }
        }
        Point autoCheckLaserPoint = new Point(0, 0);
        /// <summary>
        /// 点检视觉高度
        /// </summary>
        public double AutoCheckLaserZ
        {
            get { return autoCheckLaserZ; }
            set { autoCheckLaserZ = value; OnPropertyChanged(); }
        }
        double autoCheckLaserZ = 0;

        /// <summary>
        /// 启用移栽气缸
        /// </summary>
        public bool IsUseMoveCylinder
        {
            get { return isUseMoveCylinder; }
            set { isUseMoveCylinder = value; OnPropertyChanged(); }
        }
        bool isUseMoveCylinder = false;


        /// <summary>
        /// 点检结果
        /// </summary>
        [DisplayName("点检结果")]
        public List<ParameterItem> AllResult
        {
            get { return allResult; }
            set { allResult = value; OnPropertyChanged(); }
        }
        private List<ParameterItem> allResult = new List<ParameterItem>();


        [XmlIgnore]
        public IGlueParent ParentProject
        {
            get { return parentProject; }
            set { parentProject = value; OnPropertyChanged(); }
        }
        private IGlueParent parentProject = null;

        public bool IsHaveStartSignal(out CommonSignal workSlot)
        {
            workSlot = null;
            if (null != visionRecheck && visionRecheck.StartSignal)
            {
                workSlot = visionRecheck;
                return true;
            }
            if (null != keyenceHeightLaserTest && keyenceHeightLaserTest.StartSignal)
            {
                workSlot = keyenceHeightLaserTest;
                return true;
            }
            if (null != _visionMark && _visionMark.StartSignal)
            {
                workSlot = _visionMark;
                return true;
            }
            return false;
        }
    }
}
