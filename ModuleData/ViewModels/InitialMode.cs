﻿using G4.GlueCore;
using Googo.Manul;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ModuleData
{
    /// <summary>
    /// 初始化绑定模板
    /// </summary>
    [Serializable]
    public class InitialMode : CommunicaBase, IStationModule
    {
        #region 初始化属性
        /// <summary>
        /// 初始化指令
        /// </summary>
        [DownLoad, XmlIgnore]
        public bool Signal_R_Initialize
        {
            get 
            {
                object temp = DownLoadPro();
                if (null == temp || !(temp is bool)) return false;
                signal_R_Initialize = (bool)temp;
                return signal_R_Initialize;
            }
            set { signal_R_Initialize = value; OnPropertyChanged(); }
        }
        bool signal_R_Initialize = false;
        /// <summary>
        /// 初始化运行中
        /// </summary>
        [UpLoad, XmlIgnore]
        public bool Signal_W_Initializing
        {
            get { return signal_W_Initializing; }
            set { signal_W_Initializing = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        bool  signal_W_Initializing = false;
        /// <summary>
        /// 初始化完成
        /// </summary>
        [UpLoad,XmlIgnore]
        public bool Signal_W_InitializeOK
        {
            get { return signal_W_InitializeOK; }
            set { signal_W_InitializeOK = value;UpLoadPro(value); OnPropertyChanged(); }
        }
        bool signal_W_InitializeOK = false;

        /// <summary>
        /// 点胶模组设置
        /// </summary>
        public EnumWorkModule AxisWorkModule
        {
            get { return axisWorkModule; }
            set { axisWorkModule = value; OnPropertyChanged(); }
        }
        EnumWorkModule axisWorkModule = EnumWorkModule.S60;

        /// <summary>
        /// UV是否启用
        /// </summary>
        public bool IsContainUV
        {
            get { return _isContainUV; }
            set { _isContainUV = value; OnPropertyChanged(); }
        }
        bool _isContainUV = false;

        /// <summary>
        /// UV下降输出
        /// </summary>
        public int UVDownOut
        {
            get { return _uVDownOut; }
            set { _uVDownOut = value; OnPropertyChanged(); }
        }
        int _uVDownOut = 3;

        /// <summary>
        /// UV上升输出
        /// </summary>
        public int UVUpOut
        {
            get { return _uVUpOut; }
            set { _uVUpOut = value; OnPropertyChanged(); }
        }
        int _uVUpOut = 4;

        /// <summary>
        /// UV下降输入
        /// </summary>
        public int UVDownIn
        {
            get { return _uVDownIn; }
            set { _uVDownIn = value; OnPropertyChanged(); }
        }
        int _uVDownIn = 6;

        /// <summary>
        /// UV上升输ru
        /// </summary>
        public int UVUpIn
        {
            get { return _uVUpIn; }
            set { _uVUpIn = value; OnPropertyChanged(); }
        }

        [XmlIgnore]
        public IStationParent ParentStation { get; set; }

        int _uVUpIn =7;

        #endregion
    }
}
