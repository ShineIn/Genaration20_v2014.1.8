﻿using Googo.Manul;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;

namespace ModuleData
{
    /// <summary>
    /// 一个工位的点胶信号
    /// </summary>
    public class SlotSignalInfor:CommunicaBase
    {
        /// <summary>
        /// 工位启动信号
        /// </summary>
        [DownLoad, XmlIgnore,UpLoad]
        public bool StartButSignal
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                sartButSignal = (bool)obj;
                return sartButSignal;
            }
            set
            {
                sartButSignal = value;
                OnPropertyChanged();
            }
        }
        bool sartButSignal = false;

        /// <summary>
        /// 点胶OK结果
        /// </summary>
        [UpLoad, DownLoad, XmlIgnore]
        public bool Signal_W_GlueFinishedOK
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                signal_W_GlueFinishedOK = (bool)obj;
                return signal_W_GlueFinishedOK;
            }
            set
            {
                signal_W_GlueFinishedOK = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        bool signal_W_GlueFinishedOK = false;

        /// <summary>
        /// 点胶NG结果
        /// </summary>
        [UpLoad, DownLoad, XmlIgnore]
        public bool Signal_W_GlueFinishedNG
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                signal_W_GlueFinishedNG = (bool)obj;
                return signal_W_GlueFinishedNG;
            }
            set
            {
                signal_W_GlueFinishedNG = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        bool signal_W_GlueFinishedNG = false;

        /// <summary>
        /// 点胶中
        /// </summary>
        [UpLoad,DownLoad, XmlIgnore]
        public bool Signal_W_ReceiveProgram
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                signal_W_ReceiveProgram = (bool)obj;
                return signal_W_ReceiveProgram;
            }
            set
            {
                signal_W_ReceiveProgram = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        bool signal_W_ReceiveProgram = false;

        /// <summary>
        /// 点胶完成
        /// </summary>
        [UpLoad, DownLoad, XmlIgnore]
        public bool Signal_W_GlueFinished
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                signal_W_GlueFinished = (bool)obj;
                return signal_W_GlueFinished;
            }
            set
            {
                signal_W_GlueFinished = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        bool signal_W_GlueFinished = false;


        /// <summary>
        /// 报工状态
        /// </summary>
        [UpLoad, DisplayName("报工状态")]
        public short Signal_W_GlueReportStatus
        {
            get
            {
                return signal_W_GlueReportStatus;
            }
            set
            {
                signal_W_GlueReportStatus = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        short signal_W_GlueReportStatus = 1;

        public Point Offset
        {
            get { return _offset; }
            set { _offset = value; }
        }
        private Point _offset=new Point();

      
        /// <summary>
        /// 等待启动信号复位
        /// </summary>
        /// <remarks>
        /// 5S超时，即循环写入100次
        /// </remarks>
        /// <returns></returns>
        public bool WaitStartReset()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            bool brun = true;
            while (brun && stopwatch.ElapsedMilliseconds < 5000)
            {
                //清除结果后再给运行中
                Signal_W_GlueFinishedOK = false;
                Signal_W_GlueFinishedNG = false;
                Signal_W_GlueFinished = false;
                Signal_W_ReceiveProgram = true;
                System.Threading.Thread.Sleep(50);
                brun = StartButSignal;
            }
            return !StartButSignal;
        }

        /// <summary>
        /// 等待PLC清除运行中信号
        /// </summary>
        /// <param name="bResult"></param>
        /// <returns></returns>
        public bool WaitRuningReset(bool bResult)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            bool brun = true;
            while (brun && stopwatch.ElapsedMilliseconds < 5000)
            {
                Signal_W_GlueFinishedOK = bResult;
                Signal_W_GlueFinishedNG = !bResult;
                Signal_W_GlueFinished = true;
                System.Threading.Thread.Sleep(50);
                brun = Signal_W_ReceiveProgram;
            }
            //if (!Signal_W_ReceiveProgram)//只有在PLC已经清除掉运行中的情况下才把结果清掉，情掉是因为时序，在while循环中有可能PLC刚刚清掉完成，但是还没有清掉运行中，然后运控又写入完成
            //{
            //    System.Threading.Thread.Sleep(200);
            //    Signal_W_GlueFinishedOK = false;
            //    Signal_W_GlueFinishedNG = false;
            //    Signal_W_GlueFinished = false;
            //}
            return !Signal_W_ReceiveProgram;
        }
    }
}
