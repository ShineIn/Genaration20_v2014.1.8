﻿using Googo.Manul;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ModuleData
{

    /// <summary>
    /// 点胶生产数据
    /// </summary>
    public class ResultInfor : CommunicaBase
    {
        /// <summary>
        /// 生产时间
        /// </summary>
        public DateTime Time
        {
            get { return _time; }
            set { _time = value; OnPropertyChanged(); }
        }
        DateTime _time = DateTime.Now;


        /// <summary>
        /// 产品SN
        /// </summary>
        [UpLoad]
        public string ProductSN
        {
            get { return productSN; }
            set { productSN = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        string productSN = string.Empty;

        /// <summary>
        /// 生产序号
        /// </summary>
        [UpLoad]
        public int ProcessIndex
        {
            get { return processIndex; }
            set { processIndex = value; OnPropertyChanged(); }
        }
        int processIndex = 0;

        ///// <summary>
        ///// 穴位号
        ///// </summary>
        //[UpLoad]
        //public int SlotIndex
        //{
        //    get { return slotIndex; }
        //    set { slotIndex = value; OnPropertyChanged(); }
        //}
        //int slotIndex = 0;

        /// <summary>
        /// 测量高度列表
        /// </summary>
        /// <remarks>
        /// 位移传感器测量出来的值
        /// </remarks>
        [UpLoad]
        public List<float> GlueHeight
        {
            get { return glueHeights; }
            set { glueHeights = value; OnPropertyChanged(); }
        }
        List<float> glueHeights = new List<float>();

        /// <summary>
        /// 视觉校准得到的OffsetX
        /// </summary>
        /// <remarks>
        /// 外部需要用测获取
        /// </remarks>
        [XmlIgnore]
        [UpLoad]
        public float GlueOffsetX
        {
            get { return glueOffsetX; }
            set { glueOffsetX = value; OnPropertyChanged(); }
        }
        float glueOffsetX = 0;

        /// <summary>
        /// 视觉校准得到的OffsetY
        /// </summary>
        /// <remarks>
        /// 外部需要用测获取
        /// </remarks>
        [Browsable(false)]
        [XmlIgnore]
        [UpLoad]
        public float GlueOffsetY
        {
            get { return glueOffsetY; }
            set { glueOffsetY = value; OnPropertyChanged(); }
        }
        float glueOffsetY = 0;

        /// <summary>
        /// 视觉校准得到的OffsetAngle
        /// </summary>
        /// <remarks>
        /// 外部需要用测获取
        /// </remarks>
        [Browsable(false)]
        [XmlIgnore]
        [UpLoad]
        public float GlueOffsetA
        {
            get { return glueOffsetA; }
            set { glueOffsetA = value; OnPropertyChanged(); }
        }
        float glueOffsetA = 0;


        /// <summary>
        /// 一次固化时间
        /// </summary>
        /// <remarks>
        /// 外部需要用测获取
        /// </remarks>
        [Browsable(false)]
        [XmlIgnore]
        public float FirstUVTime
        {
            get { return firstUVTime; }
            set { firstUVTime = value; OnPropertyChanged(); }
        }
        float firstUVTime = 0;

        /// <summary>
        /// 点胶时间
        /// </summary>
        /// <remarks>
        /// 外部需要用测获取
        /// </remarks>
        [Browsable(false)]
        [XmlIgnore]
        public float GlueTotalTime
        {
            get { return glueTotalTime; }
            set { glueTotalTime = value; OnPropertyChanged(); }
        }
        float glueTotalTime = 0;


        

        /// <summary>
        /// 一次固化强度
        /// </summary>
        /// <remarks>
        /// 外部需要用测获取
        /// </remarks>
        [Browsable(false)]
        [XmlIgnore]
        public float FirstUVPower
        {
            get { return firstUVPower; }
            set { firstUVPower = value; OnPropertyChanged(); }
        }
        float firstUVPower = 0;

        [Browsable(false)]
        [XmlIgnore]
        [UpLoad]
        public short AlarmCode
        {
            get { return alarmCode; }
            set { alarmCode = value; OnPropertyChanged(); }
        }
        short alarmCode = 0;

        [UpLoad, DisplayName("复检图像路径")]
        public string AlgImgPath
        {
            get { return algImgPath; }
            set { algImgPath = value;UpLoadPro(value); OnPropertyChanged(); }
        }
        string algImgPath = string.Empty;

        public string ResultDetail
        {
            get { return resultDetail; }
            set { resultDetail = value; OnPropertyChanged(); }
        }
        string resultDetail = string.Empty;

        /// <summary>
        /// 产品NG代码
        /// </summary>
        [UpLoad, XmlIgnore, DisplayName("产品NG代码")]
        public string ProNGCode
        {
            get { return proNGCode; }
            set { proNGCode = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        private string proNGCode = string.Empty;
    }
}
