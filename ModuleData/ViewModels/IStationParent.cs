﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleData
{
    public interface IStationParent
    {
    }

    public interface IStationModule
    {
        IStationParent ParentStation { get; set; }
    }



}
