﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Windows;
using Googo.Manul;

namespace ModuleData
{
    /// <summary>
    /// 自动对针绑定模板
    /// </summary>
    [Serializable]
    public class AutoCalibraNeedle : CommunicaBase, IGlueChildModel
    {
        #region 自动对针模块

        #region 自动对针PLC变量
        //对针模块参数
        /// <summary>
        /// 自动对针信号
        /// </summary>
        [DownLoad]
        [XmlIgnore]
        public bool PLC_Sig_AutoNeedel
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                _pLC_Sig_AutoNeedel = (bool)obj;
                return _pLC_Sig_AutoNeedel;
            }
            set
            {
                _pLC_Sig_AutoNeedel = value;
                OnPropertyChanged();
            }
        }
        private bool _pLC_Sig_AutoNeedel = false;
        /// <summary>
        /// 对针自动运行中
        /// </summary>
        [XmlIgnore, UpLoad]      
        public bool SoftSigAutoNeedel
        {
            get { return _softSigAutoNeedel; }
            set
            {
                _softSigAutoNeedel = value;
                UpLoadPro(value);          
                OnPropertyChanged();          
            }
        }
        bool _softSigAutoNeedel = false;

        /// <summary>
        /// 对针完成
        /// </summary>
        [XmlIgnore, UpLoad, DownLoad]
        public bool SoftSigAutoNeedelFin
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                softSigAutoNeedelFin = (bool)obj;
                return softSigAutoNeedelFin;
            }
            set
            {
                softSigAutoNeedelFin = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        bool softSigAutoNeedelFin = true;


        /// <summary>
        /// 对针结果OK
        /// </summary>
        [XmlIgnore, UpLoad, DownLoad]
        public bool SigAutoNeedelOK
        {
            get 
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                _sigAutoNeedelOK = (bool)obj;
                return _sigAutoNeedelOK;
            }
            set
            {
                _sigAutoNeedelOK = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        bool _sigAutoNeedelOK = true;
        /// <summary>
        /// 对针结果NG
        /// </summary>
        /// 
        [XmlIgnore, UpLoad,DownLoad ]
        public bool SigAutoNeedelNG
        {
            get 
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                _sigAutoNeedelNG = (bool)obj;
                return _sigAutoNeedelNG; 
            }
            set
            {
                _sigAutoNeedelNG = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        bool _sigAutoNeedelNG = false;
        /// <summary>
        /// PLC--运控对针X上下限
        /// </summary>
        [XmlIgnore, DownLoad]
        public float PLC_IPC_AutoNeedelOffsetX
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return 0;
                _pLC_IPC_AutoNeedelOffsetX = (float)obj;
                return _pLC_IPC_AutoNeedelOffsetX;
            }
            set
            {
                _pLC_IPC_AutoNeedelOffsetX = value;
                OnPropertyChanged();
            }
        }
        float _pLC_IPC_AutoNeedelOffsetX = 0;

        /// <summary>
        /// PLC--运控对针Y上下限
        /// </summary>
        [XmlIgnore, DownLoad]
        public float PLC_IPC_AutoNeedelOffsetY
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return 0;
                _pLC_IPC_AutoNeedelOffsetY = (float)obj;
                return _pLC_IPC_AutoNeedelOffsetY;
            }
            set
            {
                _pLC_IPC_AutoNeedelOffsetY = value;
                OnPropertyChanged();
            }
        }
        float _pLC_IPC_AutoNeedelOffsetY = 0;
        /// <summary>
        /// PLC--运控对针Z上下限
        /// </summary>
        [XmlIgnore, DownLoad]
        public float PLC_IPC_AutoNeedelOffsetZ
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return 0;
                _pLC_IPC_AutoNeedelOffsetZ = (float)obj;
                return _pLC_IPC_AutoNeedelOffsetZ;
            }
            set
            {
                _pLC_IPC_AutoNeedelOffsetZ = value;
                OnPropertyChanged();
            }
        }
        float _pLC_IPC_AutoNeedelOffsetZ = 0;
        /// <summary>
        /// 运控--PLC X方向纠偏偏差值
        /// </summary>
        [ UpLoad]
        public float IPC_PLC_AutoNeedelOffsetX
        {
            get { return _iPC_PLC_AutoNeedelOffsetX; }
            set { _iPC_PLC_AutoNeedelOffsetX = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float _iPC_PLC_AutoNeedelOffsetX = 0;
        /// <summary>
        /// 运控--PLC Y方向纠偏偏差值
        /// </summary>
        [ UpLoad]
        public float IPC_PLC_AutoNeedelOffsetY
        {
            get { return _iPC_PLC_AutoNeedelOffsetY; }
            set { _iPC_PLC_AutoNeedelOffsetY = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float _iPC_PLC_AutoNeedelOffsetY = 0;
        /// <summary>
        /// 运控--PLC Z方向纠偏偏差值
        /// </summary>
        [ UpLoad]
        public float IPC_PLC_AutoNeedelOffsetZ
        {
            get { return _iPC_PLC_AutoNeedelOffsetZ; }
            set { _iPC_PLC_AutoNeedelOffsetZ = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float _iPC_PLC_AutoNeedelOffsetZ = 0;

        #endregion


        #region 非PLC交换指令
        #endregion
        /// <summary>
        /// 对针基准位置
        /// </summary>
        public Point AutoNeedelPoint
        {
            get { return autoNeedelPoint; }
            set
            {
                autoNeedelPoint = value; OnPropertyChanged();
            }
        }
        Point autoNeedelPoint = new Point(0, 0);
        /// <summary>
        /// 对针基准高度
        /// </summary>
        public double AutoNeedelHeight
        {
            get { return autoNeedelHeight; }
            set { autoNeedelHeight = value; OnPropertyChanged(); }
        }
        double autoNeedelHeight = 0;

        /// <summary>
        /// 启用移栽气缸
        /// </summary>
        public bool IsUseMoveCylinder
        {
            get { return isUseMoveCylinder; }
            set { isUseMoveCylinder = value; OnPropertyChanged(); }
        }
        bool isUseMoveCylinder = false;
        


        #endregion

        [XmlIgnore]
        public IGlueParent ParentProject
        {
            get { return parentProject; }
            set { parentProject = value; OnPropertyChanged(); }
        }
        private IGlueParent parentProject = null;
    }
}
