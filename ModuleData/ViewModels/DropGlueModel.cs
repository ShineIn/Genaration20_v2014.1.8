﻿using Googo.Manul;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ModuleData
{
    /// <summary>
    /// 抛胶参数
    /// </summary>
    [Serializable]
    public class DropGlueModel: CommunicaBase, IGlueChildModel
    {
        #region 排胶属性
        /// <summary>
        /// 触发排胶指令
        /// </summary>
        [DownLoad]
        [XmlIgnore]
        public bool PLC_Sig_GuleOut
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                _pLC_Sig_GuleOut = (bool)obj;
                return _pLC_Sig_GuleOut;
            }
            set
            {
                _pLC_Sig_GuleOut = value;
                OnPropertyChanged();
            }
        }
        private bool _pLC_Sig_GuleOut = false;
        /// <summary>
        /// 排胶运行中
        /// </summary>
        [XmlIgnore, UpLoad]
        public bool SoftSigGuleOut
        {
            get { return _softSigGuleOut; }
            set
            {
                _softSigGuleOut = value;
                //_softSigGuleOut = !value;
                UpLoadPro(value);
                //UpLoadPro(_softSigGuleOut, nameof(SoftSigGuleOutFin));
                OnPropertyChanged();
                //OnPropertyChanged(nameof(SoftSigGuleOutFin));
            }
        }
        bool _softSigGuleOut = false;
        /// <summary>
        /// 排胶完成
        /// </summary>
        [XmlIgnore, UpLoad,DownLoad]
        public bool SoftSigGuleOutFin
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                softSigGuleOutFin = (bool)obj;
                return softSigGuleOutFin;
            }
            set
            {
                softSigGuleOutFin = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }

        }
        bool softSigGuleOutFin = true;
        /// <summary>
        /// 排胶结果OK
        /// </summary>
        [XmlIgnore, UpLoad,DownLoad]
        public bool SigGuleOutOK
        {
            get 
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                _sigGuleOutOK = (bool)obj;
                return _sigGuleOutOK; 
            }
            set
            {
                _sigGuleOutOK = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        bool _sigGuleOutOK = true;
        /// <summary>
        /// 排胶结果NG
        /// </summary>
        [XmlIgnore, UpLoad,DownLoad]
        public bool SigGuleOutNG
        {
            get 
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                _sigGuleOutNG = (bool)obj;
                return _sigGuleOutNG; 
            }
            set
            {
                _sigGuleOutNG = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        bool _sigGuleOutNG = false;

        /// <summary>
        /// PLC--运控排胶时间参数1
        /// </summary>
        [XmlIgnore, DownLoad]
        public float PLC_IPC_GuleOutTime1
        {
            get 
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return 0;
                _pLC_IPC_GuleOutTime1 = (float)obj;
                return _pLC_IPC_GuleOutTime1;
            }
            set 
            {
                _pLC_IPC_GuleOutTime1 = value;
                OnPropertyChanged();
            }
        }
        float _pLC_IPC_GuleOutTime1 = 0;
        /// <summary>
        /// PLC--运控排胶时间参数2
        /// </summary>
        [XmlIgnore, DownLoad]
        public float PLC_IPC_GuleOutTime2
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return 0;
                _pLC_IPC_GuleOutTime2 = (float)obj;
                return _pLC_IPC_GuleOutTime2;
            }
            set
            {
                _pLC_IPC_GuleOutTime2 = value;
                OnPropertyChanged();
            }
        }
        float _pLC_IPC_GuleOutTime2 = 0;

        /// <summary>
        /// 运控--PLC实际排胶时间1
        /// </summary>
        [XmlIgnore, UpLoad]
        public float IPC_PLC_GuleOutTime1
        {
            get { return _iPC_PLC_GuleOutTime1; }
            set { _iPC_PLC_GuleOutTime1 = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float _iPC_PLC_GuleOutTime1 = 0;
        /// <summary>
        /// 运控--PLC实际排胶时间2
        /// </summary>
        [XmlIgnore, UpLoad]
        public float IPC_PLC_GuleOutTime2
        {
            get { return _iPC_PLC_GuleOutTime2; }
            set { _iPC_PLC_GuleOutTime2 = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float _iPC_PLC_GuleOutTime2 = 0;

        /// <summary>
        /// 自动排胶时间（S）
        /// </summary>
        [XmlIgnore, DownLoad]
        public float GuleOutputTime
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is float)) return 0;
                guleOutputTime = (float)obj;
                return guleOutputTime;
            }
            set
            {
                guleOutputTime = value;
                OnPropertyChanged();
            }
        }
        float guleOutputTime = 0;       

        #endregion
        [XmlIgnore]
        public IGlueParent ParentProject
        {
            get { return parentProject; }
            set { parentProject = value;OnPropertyChanged(); }
        }
        private IGlueParent parentProject = null;
    }
}
