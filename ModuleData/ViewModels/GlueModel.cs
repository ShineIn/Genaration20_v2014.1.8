﻿using G4.GlueCore;
using Googo.Manul;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;

namespace ModuleData
{
    /// <summary>
    /// 穴位数量
    /// </summary>
    public enum SlotType
    {
        Single,
        Double,
        Three,
    }


    /// <summary>
    /// 点胶执行参数
    /// </summary>
    [Serializable]
    public class GlueModel : CommunicaBase, IGlueChildModel
    {
        #region 公有属性
        /// <summary>
        /// Mark位置指令列表
        /// </summary>
        public ObservableCollection<PositionInfor> GlueCamPointsOrder
        {
            get { return glueCamPointsOrder; }
            set { glueCamPointsOrder = value; OnPropertyChanged(); }
        }
        private ObservableCollection<PositionInfor> glueCamPointsOrder = new ObservableCollection<PositionInfor>();

        /// <summary>
        /// 测高位置列表
        /// </summary>
        public ObservableCollection<PositionInfor> GlueHeightPoints
        {
            get { return glueHeightPoints; }
            set { glueHeightPoints = value; OnPropertyChanged(); }
        }
        private ObservableCollection<PositionInfor> glueHeightPoints = new ObservableCollection<PositionInfor>();

        /// <summary>
        /// 复检点位列表
        /// </summary>
        public ObservableCollection<PositionInfor> GlueCheckPointOrders
        {
            get { return glueCheckPoints; }
            set { glueCheckPoints = value; OnPropertyChanged(); }
        }
        ObservableCollection<PositionInfor> glueCheckPoints = new ObservableCollection<PositionInfor>();

        /// <summary>
        /// 点胶起始点
        /// </summary>
        public Point GluePoint
        {
            get { return gluePoint; }
            set { gluePoint = value; OnPropertyChanged(); }
        }
        Point gluePoint = new Point();



        public SlotType SlotNum
        {
            get { return slotNum; }
            set { slotNum = value; OnPropertyChanged(); }
        }
        private SlotType slotNum = SlotType.Single;

        public SlotSignalInfor FirstSlot
        {
            get { return _firstSlot; }
            set { _firstSlot = value; OnPropertyChanged(); }
        }
        private SlotSignalInfor _firstSlot = new SlotSignalInfor();

        public SlotSignalInfor SecSlot
        {
            get { return _secSlot; }
            set { _secSlot = value; OnPropertyChanged(); }
        }
        private SlotSignalInfor _secSlot = new SlotSignalInfor();

        public SlotSignalInfor ThirldSlot
        {
            get { return _thirldSlot; }
            set { _thirldSlot = value; OnPropertyChanged(); }
        }
        private SlotSignalInfor _thirldSlot = new SlotSignalInfor();

        /// <summary>
        /// 做模板的时候生成的位置高度
        /// </summary>
        public double GlueHeightBase
        {
            get { return glueHeightBase; }
            set { glueHeightBase = value; OnPropertyChanged(); }
        }
        double glueHeightBase = 0;

        /// <summary>
        /// 点胶检测结果
        /// </summary>
        public List<bool> GlueCheckResult
        {
            get { return glueCheckResult; }
            internal set { glueCheckResult = value; OnPropertyChanged(); }
        }
        List<bool> glueCheckResult = new List<bool>();

        /// <summary>
        /// 点胶最低高度
        /// </summary>
        public double GlueHeightMin
        {
            get { return glueHeightMin; }
            set { glueHeightMin = value; OnPropertyChanged(); }
        }
        double glueHeightMin = -5;

        /// <summary>
        /// 点胶最高高度
        /// </summary>
        public double GlueHeightMax
        {
            get { return glueHeightMax; }
            set { glueHeightMax = value; OnPropertyChanged(); }
        }
        double glueHeightMax = 5;

        /// <summary>
        /// PLC配方号
        /// </summary>
        [DownLoad, XmlIgnore]
        public short RecipeIndexAddre
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is short)) return 0;
                recipeIndexAddre = (short)obj;
                return recipeIndexAddre;
            }
            set { recipeIndexAddre = value; OnPropertyChanged(); }
        }
        short recipeIndexAddre = 0;

        /// <summary>
        /// PLC配方号
        /// </summary>
        [UpLoad, XmlIgnore]
        public short WriteIndexAddre
        {
            get { return writeIndexAddre; }
            set { writeIndexAddre = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        short writeIndexAddre = 0;

        /// <summary>
        /// 测量高度列表
        /// </summary>
        /// <remarks>
        /// 位移传感器测量出来的值
        /// </remarks>
        [Browsable(false)]
        [XmlIgnore]
        public List<float> GlueHeight
        {
            get { return glueHeights; }
            private set { glueHeights = value; OnPropertyChanged(); }
        }
        List<float> glueHeights = new List<float>();

        /// <summary>
        /// 视觉校准得到的OffsetX
        /// </summary>
        /// <remarks>
        /// 外部需要用测获取
        /// </remarks>
        [XmlIgnore]
        public float GlueOffsetX
        {
            get { return glueOffsetX; }
            internal set { glueOffsetX = value; OnPropertyChanged(); }
        }
        float glueOffsetX = 0;

        /// <summary>
        /// 视觉校准得到的OffsetY
        /// </summary>
        /// <remarks>
        /// 外部需要用测获取
        /// </remarks>
        [Browsable(false)]
        [XmlIgnore]
        public float GlueOffsetY
        {
            get { return glueOffsetY; }
            internal set { glueOffsetY = value; OnPropertyChanged(); }
        }
        float glueOffsetY = 0;

        /// <summary>
        /// 视觉校准得到的OffsetAngle
        /// </summary>
        /// <remarks>
        /// 外部需要用测获取
        /// </remarks>
        [Browsable(false)]
        [XmlIgnore]
        public float GlueOffsetA
        {
            get { return glueOffsetA; }
            internal set { glueOffsetA = value; OnPropertyChanged(); }
        }
        float glueOffsetA = 0;

        /// <summary>
        /// 点胶结果
        /// </summary>
        [XmlIgnore]
        public ObservableCollection<ResultInfor> DisGlueResults
        {
            get { return disGlueResults; }
            set { disGlueResults = value; OnPropertyChanged(); }
        }
        ObservableCollection<ResultInfor> disGlueResults = new ObservableCollection<ResultInfor>();

        [UpLoad]
        public ResultInfor RunResult
        {
            get { return runResult; }
            set { runResult = value; OnPropertyChanged(); }
        }
        private ResultInfor runResult = new ResultInfor();

        [DownLoad, DisplayName("产品SN"),XmlIgnore]
        /// <summary>
        /// SN
        /// </summary>
        public string ProductSN
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is string)) return productSN;
                productSN = (string)obj;
                return productSN;
            }
                set { productSN = value; OnPropertyChanged(); }
        }
        string productSN = "";

        /// <summary>
        /// 点胶补偿值
        /// </summary>
        public string GlueOffset
        {
            get { return _GuleOffset; }
            set { _GuleOffset = value; OnPropertyChanged(); }
        }
        string _GuleOffset = "1,1";

        /// <summary>
        /// 点胶引导的上下限
        /// </summary>
        public string GlueOffsetMinOrMax
        {
            get { return _GlueOffsetMinOrMax; }
            set { _GlueOffsetMinOrMax = value; OnPropertyChanged(); }
        }
        string _GlueOffsetMinOrMax = "5,5";


        /// <summary>
        /// 对针补偿值
        /// </summary>
        public string GuleNeedleOffset
        {
            get { return _GuleNeedleOffset; }
            set { _GuleNeedleOffset = value; OnPropertyChanged(); }
        }
        string _GuleNeedleOffset = "3,3";


        /// <summary>
        /// 测高平面补偿值
        /// </summary>
        public string GluePanelOffset
        {
            get { return _GluePanelOffset; }
            set { _GluePanelOffset = value; OnPropertyChanged(); }
        }
        string _GluePanelOffset = "9,8,7";
        /// <summary>
        /// 测高补偿上下限
        /// </summary>
        public string HeightOffsetMinOrMax
        {
            get { return _HeightOffsetMinOrMax; }
            set { _HeightOffsetMinOrMax = value; OnPropertyChanged(); }
        }
        string _HeightOffsetMinOrMax = "5,5,5";

        /// <summary>
        /// 最终针头的坐标
        /// </summary>
        public string NeedlePos
        {
            get { return _NeedlePos; }
            set { _NeedlePos = value; OnPropertyChanged(); }
        }
        string _NeedlePos = "";
        /// <summary>
        /// 视觉任务号
        /// </summary>
        public string VisionTaskNO
        {
            get { return _VisionTaskNO; }
            set { _VisionTaskNO = value; OnPropertyChanged(); }
        }
        string _VisionTaskNO = "999";


        /// <summary>
        /// XY轴移动速度
        /// </summary>
        public double GlueXYAxisSpd
        {
            get { return glueXYAxisSpd; }
            set { glueXYAxisSpd = value; OnPropertyChanged(); }
        }
        double glueXYAxisSpd = 20;       

        
        /// <summary>
        /// Z轴移动速度
        /// </summary>
        public double GlueZAxisSpd
        {
            get { return glueZAxisSpd; }
            set { glueZAxisSpd = value; OnPropertyChanged(); }
        }
        double glueZAxisSpd = 20;

        /// <summary>
        /// XY轴移动加速度
        /// </summary>
        public double GlueXYAxisAcc
        {
            get { return glueXYAxisAcc; }
            set { glueXYAxisAcc = value; OnPropertyChanged(); }
        }
        double glueXYAxisAcc = 100;      


        /// <summary>
        /// Z轴移动加速度
        /// </summary>
        public double GlueZAxisAcc
        {
            get { return glueZAxisAcc; }
            set { glueZAxisAcc = value; OnPropertyChanged(); }
        }
        double glueZAxisAcc = 100;

        /// <summary>
        /// 点胶路径XY补偿
        /// </summary>
        public Point DispGlueXYOffset
        {
            get { return _dispGlueXYOffset; }
            set { _dispGlueXYOffset = value; }
        }
        private Point _dispGlueXYOffset = new Point();


        /// <summary>
        /// 点胶路径Z补偿
        /// </summary>
        public double DispGlueZOffset
        {
            get { return _dispGlueZOffset; }
            set { _dispGlueZOffset = value; }
        }
        private double _dispGlueZOffset = 0;

        /// <summary>
        /// 自动流程移动模组避让位
        /// </summary>
        public PositionInfor AutoRunSafetyPos
        {
            get { return _autoRunSafetyPos; }
            set { _autoRunSafetyPos = value; }
        }
        private PositionInfor _autoRunSafetyPos = new PositionInfor();


        [UpLoad, DisplayName("光阑安装_涂胶后作业时间")]
        /// <summary>
        /// 光阑安装_涂胶后作业时间
        /// </summary>
        public float GlueOpenTimeSetting
        {
            get { return glueOpenTimeSetting; }
            set { glueOpenTimeSetting = value; OnPropertyChanged(); }
        }
        float glueOpenTimeSetting = 100;


        [UpLoad, DisplayName("点胶时间")]
        /// <summary>
        /// 光阑安装_涂胶后作业时间
        /// </summary>
        public float DispenseTotalTime
        {
            get { return dispenseTotalTime; }
            set { dispenseTotalTime = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float dispenseTotalTime = 0.0f;

        [XmlIgnore]
        /// <summary>
        /// 光阑安装_涂胶后作业时间
        /// </summary>
        public List<double> DispenseTimeList
        {
            get { return dispenseTimeList; }
            set { dispenseTimeList = value; OnPropertyChanged(); }
        }
        List<double> dispenseTimeList = new List<double>();


        [UpLoad, DisplayName("点胶面积"), XmlIgnore]
        /// <summary>
        /// 点胶面积
        /// </summary>
        public float CCDGlueArea
        {
            get
            {
                return cCDGlueArea;
            }
            set { cCDGlueArea = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float cCDGlueArea = 0;
        [UpLoad, DisplayName("点胶复检图片路径"), XmlIgnore]
        /// <summary>
        /// 点胶复检图片路径
        /// </summary>
        public string CCDGlueImagePath
        {
            get
            {
                return _CCDGlueImagePath;
            }
            set { _CCDGlueImagePath = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        string _CCDGlueImagePath = "";

        [UpLoad, DisplayName("溢胶面积"), XmlIgnore]
        /// <summary>
        /// 溢胶面积
        /// </summary>
        public float OverflowGlueArea
        {
            get
            {
                return overflowGlueArea;
            }
            set { overflowGlueArea = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float overflowGlueArea = 0;


        [UpLoad, DisplayName("出胶移动速度"), XmlIgnore]
        /// <summary>
        /// 出胶移动速度
        /// </summary>
        public float DispensingMoveSpeed
        {
            get
            {
                return dispensingMoveSpeed;
            }
            set { dispensingMoveSpeed = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float dispensingMoveSpeed = 0;


        [UpLoad, DisplayName("点胶位置"), XmlIgnore]
        /// <summary>
        /// 点胶位置
        /// </summary>
        public float CCDGluePosition
        {
            get
            {
                return cCDGluePosition;
            }
            set { cCDGluePosition = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float cCDGluePosition = 0;

        [UpLoad, DisplayName("螺杆阀转速"), XmlIgnore]
        /// <summary>
        /// 螺杆阀转速
        /// </summary>
        public float ScrewRotationSpeed
        {
            get
            {
                return screwRotationSpeed;
            }
            set { screwRotationSpeed = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float screwRotationSpeed = 0;

        [UpLoad, DisplayName("胶量实际值"), XmlIgnore]
        /// <summary>
        /// 胶量实际值
        /// </summary>
        public float WeightActualValue
        {
            get
            {
                return weightActualValue;
            }
            set { weightActualValue = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float weightActualValue = 0;

        [UpLoad, DisplayName("点胶高度"), XmlIgnore]
        /// <summary>
        /// 点胶高度
        /// </summary>
        public float DispensingHeight
        {
            get
            {
                return dispensingHeight;
            }
            set { dispensingHeight = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float dispensingHeight = 0;

        [UpLoad, DisplayName("光阑安装_固化台UV灯照胶时间"), XmlIgnore]
        /// <summary>
        /// 光阑安装_固化台UV灯照胶时间
        /// </summary>
        public float UVCurUseTime
        {
            get
            {
                return uVCurUseTime;
            }
            set { uVCurUseTime = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float uVCurUseTime = 0;


        [UpLoad, DisplayName("光阑安装_固化台UV灯照胶功率"), XmlIgnore]
        /// <summary>
        /// 光阑安装_固化台UV灯照胶功率
        /// </summary>
        public float UVCuringLightPower
        {
            get
            {
                return uVCuringLightPower;
            }
            set { uVCuringLightPower = value; UpLoadPro(value); OnPropertyChanged(); }
        }
        float uVCuringLightPower = 0;

        [DisplayName("视觉结果")]
        public List<ParameterItem> VisionResult
        {
            get
            {
                return visionResult;
            }
            set { visionResult = value; OnPropertyChanged(); }
        }
        List<ParameterItem> visionResult = new List<ParameterItem>();
        #endregion 公有属性

        [XmlIgnore]
        public IGlueParent ParentProject
        {
            get { return parentProject; }
            set { parentProject = value; OnPropertyChanged(); }
        }
        private IGlueParent parentProject = null;

        public bool IsHaveStartSignal(out SlotSignalInfor workSlot)
        {
            workSlot = null;
            if (slotNum == SlotType.Single && null != _firstSlot && _firstSlot.StartButSignal)
            {
                workSlot = _firstSlot;
                bool leftRXStartSig = (bool)GloMotionParame.SiemenPLC.Read("DB3000.DBX4.0");
                bool leftTXStartSig = (bool)GloMotionParame.SiemenPLC.Read("DB3000.DBX5.0");
                bool RightStartSig = (bool)GloMotionParame.SiemenPLC.Read("DB3100.DBX2.0");
                LogSv.Log.log.Write("左工位RX启动信号：" + leftRXStartSig.ToString(), System.Drawing.Color.Black);
                LogSv.Log.log.Write("左工位TX启动信号：" + leftTXStartSig.ToString(), System.Drawing.Color.Black);
                LogSv.Log.log.Write("右工位TX启动信号：" + RightStartSig.ToString(), System.Drawing.Color.Black);
                return true;
            }
            else if (slotNum == SlotType.Double)
            {
                if (null != _firstSlot && _firstSlot.StartButSignal)
                {
                    workSlot = _firstSlot;
                    return true;
                }
                if (null != _secSlot && _secSlot.StartButSignal)
                {
                    workSlot = _secSlot;
                    return true;
                }
            }
            else if (slotNum == SlotType.Three)
            {
                if (null != _firstSlot && _firstSlot.StartButSignal)
                {
                    workSlot = _firstSlot;
                    return true;
                }
                if (null != _secSlot && _secSlot.StartButSignal)
                {
                    workSlot = _secSlot;
                    return true;
                }
                if (null != _thirldSlot && _thirldSlot.StartButSignal)
                {
                    workSlot = _thirldSlot;
                    return true;
                }
            }
            return false;
        }


        public bool ResetStartSignal()
        {
            if (slotNum == SlotType.Single && null != _firstSlot)
            {
                _firstSlot.StartButSignal = false;
                if (_firstSlot.StartButSignal == false)
                    return true;
                else
                    return false;
            }
            else
                return true;
        }

    }
}
