﻿using Googo.Manul;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ModuleData
{
    /// <summary>
    /// 擦针绑定模板
    /// </summary>
    [Serializable]
    public class ClearNeedleModel: CommunicaBase, IGlueChildModel
    {
        #region 擦针属性
        /// <summary>
        /// 擦针开始指令
        /// </summary>
        [DownLoad]
        [XmlIgnore]
        public bool Signal_R_GuleClear
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                signal_R_GuleClear = (bool)obj;
                return signal_R_GuleClear;
            }
            set
            {
                signal_R_GuleClear = value;
                OnPropertyChanged();
            }
        }
        bool signal_R_GuleClear = false;
        /// <summary>
        /// 擦胶任务运行中
        /// </summary>
        [XmlIgnore, UpLoad]
        public bool Signal_W_GuleClearRun
        {
            get { return signal_W_GuleClearRun; }
            set
            {
                signal_W_GuleClearRun = value;
                signal_W_GuleClearFinish = !value;
                UpLoadPro(value);
                UpLoadPro(signal_W_GuleClearFinish, nameof(Signal_W_GuleClearFinish));
                OnPropertyChanged();
                OnPropertyChanged(nameof(Signal_W_GuleClearFinish));
            }
        }
        bool signal_W_GuleClearRun = false;
        /// <summary>
        /// 擦胶完成
        /// </summary>
        [XmlIgnore, UpLoad,DownLoad]
        public bool Signal_W_GuleClearFinish
        {
            get 
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                signal_W_GuleClearFinish = (bool)obj;
                return signal_W_GuleClearFinish;
            }
            set
            {
                signal_W_GuleClearFinish = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        bool signal_W_GuleClearFinish = false;
        /// <summary>
        /// 擦胶结果OK
        /// </summary>

        [XmlIgnore, UpLoad,DownLoad]
        public bool Signal_W_GuleClearOK
        {
            get
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                signal_W_GuleClearOK = (bool)obj;
                return signal_W_GuleClearOK;
            }
            set { signal_W_GuleClearOK = value;
                UpLoadPro(value);
                OnPropertyChanged(); }
        }
        bool signal_W_GuleClearOK = true;
        /// <summary>
        /// 擦胶结果NG
        /// </summary>

        [XmlIgnore, UpLoad,DownLoad]
        public bool Signal_W_GuleClearNG
        {
            get 
            {
                object obj = DownLoadPro();
                if (obj == null || !(obj is bool)) return false;
                signal_W_GuleClearNG = (bool)obj;
                return signal_W_GuleClearNG;
            }
            set { 
                signal_W_GuleClearNG = value;
                UpLoadPro(value);
                OnPropertyChanged(); 
            }
        }
        bool signal_W_GuleClearNG = false;
        /// <summary>
        /// 自动擦胶次数设定（擦胶）
        /// </summary>
        [XmlIgnore, UpLoad]
        public short Set_R_GuleClearNum
        {
            get { return set_R_GuleClearNum; }
            set { set_R_GuleClearNum = value;
                UpLoadPro(value);
                OnPropertyChanged(); }
        }
        short set_R_GuleClearNum = 0;//0

        /// <summary>
        /// 擦胶数记录
        /// </summary>
        [XmlIgnore, UpLoad]
        public int ResetCleanGlueAlarm
        {
            get { return resetCleanGlueAlarm; }
            set {
                resetCleanGlueAlarm = value;
                OnPropertyChanged(); }
        }
        int resetCleanGlueAlarm = 24;//0


        /// <summary>
        /// 擦胶数记录
        /// </summary>
        [XmlIgnore]
        public short Ref_W_GuleClearNum
        {
            get { return ref_W_GuleClearNum; }
            set
            {
                ref_W_GuleClearNum = value;
                UpLoadPro(value);
                OnPropertyChanged();
            }
        }
        short ref_W_GuleClearNum = 0;//0

        #endregion

        [XmlIgnore]
        public IGlueParent ParentProject
        {
            get { return parentProject; }
            set { parentProject = value; OnPropertyChanged(); }
        }
        private IGlueParent parentProject = null;
    }
}
