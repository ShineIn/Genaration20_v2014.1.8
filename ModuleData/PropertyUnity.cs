﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ModuleData
{
    public static class PropertyUnity
    {
        public static void GetLastObjAndpro(object findobj, string propath, out object propertyValue, out object curObj, out PropertyInfo lastpro)
        {
            Debug.Assert(findobj != null);
            curObj = (propertyValue = findobj);
            lastpro = null;
            if (string.IsNullOrEmpty(propath))
            {
                return;
            }
            string[] array = propath.Split(new char[1] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            Type type = propertyValue.GetType();
            int index = -1;
            for (int i = 0; i < array.Length; i++)
            {
                curObj = propertyValue;
                string proNameAndIndex = GetProNameAndIndex(array[i], out index);
                lastpro = type.GetProperty(proNameAndIndex);
                if (lastpro == null)
                {
                    break;
                }
                if (index >= 0)
                {
                    propertyValue = lastpro.GetValue(curObj);
                    if (propertyValue is IEnumerable)
                    {
                        propertyValue = (curObj = ((IList)propertyValue)[index]);
                        type = curObj.GetType();
                    }
                }
                else
                {
                    propertyValue = lastpro.GetValue(curObj);
                    type = lastpro.PropertyType;
                }
            }
        }

        private static string GetProNameAndIndex(string eleName, out int index)
        {
            Regex regex = new Regex("(\\[).*?(\\])");
            index = -1;
            string result = eleName;
            Match match = regex.Match(eleName);
            if (match.Success)
            {
                result = eleName.Substring(0, match.Index);
                string s = eleName.Substring(match.Index + 1, match.Length - 2);
                if (!int.TryParse(s, out index))
                {
                    index = -1;
                }
            }
            return result;
        }

        /// <summary>
        /// 参数列表是否可以设置判断
        /// </summary>
        /// <param name="tarType"></param>
        /// <param name="souType"></param>
        /// <returns></returns>
        internal static bool IsTypeCanSet(Type tarType, Type souType)
        {
            if (null == tarType || souType == null)
            {
                return false;
            }
            if (tarType == typeof(object) || souType == typeof(object) || souType == tarType)
            {
                return true;
            }
            return tarType.IsAssignableFrom(souType);
        }
    }
}
