﻿using Prism.Commands;
using SmartGlue.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ModuleData
{
    public class WeightometerModel : ModelBindableBase, IDisposable
    {
        #region 公有属性
        public string PortName { get; set; } = "Com2";
        public int BaudRate { get; set; } = 38400;
        /// <summary>
        /// 连接状态
        /// </summary>
        public bool IsConnected
        {
            get { return comm.IsOpen; }// _UpdateTime > DateTime.Now.AddSeconds(-5); }
        }
        /// <summary>
        /// 实时重量
        /// </summary>
        public double RealValue
        {
            get
            {
                return _UpdateTime > DateTime.Now.AddSeconds(-3) ? _RealTimeValue : 0;
            }
        }
        /// <summary>
        ///  实时值是否稳定
        /// </summary>
        public bool IsStable
        {
            get { return _QualityLeavel == 1; }
        }

        #endregion 公有属性
        public void Initialize()
        {
            lock (comm)
            {
                if (comm.IsOpen)
                {
                    mIsRunning = false;
                    comm.DataReceived += Comm_DataReceived;
                }
                else
                {
                    try
                    {
                        comm.ReadTimeout = 3000;
                        comm.PortName = PortName;
                        comm.BaudRate = BaudRate;
                        comm.Parity = Parity.Even;
                        comm.Open();
                        comm.DataReceived += Comm_DataReceived;
                        comm.ReceivedBytesThreshold = 8;
                        start();
                    }
                    catch (Exception ex)
                    {
                        mIsRunning = false;
                        Log(Tag, LogLevel.Error, TR("D_M003000"));
                    }
                }
            }
        }

        /// <summary>
        /// 采集实时稳定值，超时返回double.NaN
        /// </summary>
        /// <param name="millisecondsTimeout"></param>
        /// <returns></returns>
        public async Task<double> FetchValue(int millisecondsTimeout)
        {
            double val = double.NaN;
            DateTime now = DateTime.Now;
            if (await Task.Run(() => SpinWait.SpinUntil(() => IsStable && _UpdateTime > now, millisecondsTimeout)))
            {
                val = _RealTimeValue;
            }
            return val;
        }

        public void stop()
        {
            mIsRunning = false;
            Thread.Sleep(500);
            Dispose();
        }


        public void Dispose()
        {
            if (comm.IsOpen)
            {
                mIsRunning = false;
                comm.DataReceived -= Comm_DataReceived;
                comm.DiscardInBuffer();
                comm.DiscardOutBuffer();
                comm.Close();
            }
        }

        #region private
        private void start()
        {
            mIsRunning = true;
            if (null != th && th.IsAlive) return;
            th = new Thread(() =>
            {
                while (mIsRunning)
                {
                    lock (comm)
                    {
                        if (!comm.IsOpen)
                            break;
                        comm.Write(new byte[] { 0x02, 0x00, 0x75, 0x00, 0x01, 0x75, 0x75, 0x0A }, 0, 8);
                    }
                    Thread.Sleep(500);
                }
            });
            th.IsBackground = true;
            th.Start();
        }

        private void Comm_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            lock (comm)
            {
                try
                {           
                    byte[] readBuffer = new byte[comm.BytesToRead];
                    var n = comm.Read(readBuffer, 0, comm.BytesToRead);
                    if (n == 8)
                    {              
                        if (readBuffer[0] == 0xAA)
                        {
                            var data = readBuffer.Skip(2).Take(4).ToArray();
                            int value = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + data[3];
                            _RealTimeValue = value / 10000f;
                            _UpdateTime = DateTime.Now;
                            _QualityLeavel = (readBuffer[6] & 1) > 0 ? 1 : 0;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        private double _RealTimeValue;
        private DateTime _UpdateTime;
        private int _QualityLeavel;
        bool mIsRunning = false;
        private readonly SerialPort comm = new SerialPort();
        private Thread th;
        #endregion
    }
}
