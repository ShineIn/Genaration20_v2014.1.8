﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModuleData
{
    [Serializable]
    public class EditArqument : ICloneable
    {
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        private string _name = string.Empty;

        public string ProPath
        {
            get { return _proPath; }
            set { _proPath = value; }
        }
        private string _proPath = string.Empty;

        public Type DataType
        {
            get { return _dataType; }
            set { _dataType = value; }
        }
        private Type _dataType = typeof(object);

        public EditArqument Clone()
        {
            return new EditArqument
            {
                _name = _name,
                _proPath = _proPath,
                _dataType = _dataType,
            };
        }

        object ICloneable.Clone()
        {
            return Clone();
        }
    }
}
