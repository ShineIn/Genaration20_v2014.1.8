﻿using Googo.Manul;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ModuleData.Helper
{
    public class FileOperate
    {
        public static void SaveData(string FileName, string header, string sSaveData)
        {
            //数据记录
            string sfileName = FileName;
            string strFileName = sfileName + "\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".csv";

            if (!Directory.Exists(sfileName))
            {
                Directory.CreateDirectory(sfileName);
            }
            if (!File.Exists(strFileName))
            {
                SaveCSV(header, strFileName);//列首名称
            }
            //string strMid = "";
            //for (int i = 0; i < sSaveData.Length; i++)
            //{
            //    strMid += sSaveData[i] + ",";
            //}
            SaveCSV(sSaveData, strFileName);
        }


        public static void SaveCSV(string str, string fullPath)
        {
            try
            {
                FileInfo fi = new FileInfo(fullPath);
                if (!fi.Directory.Exists)
                {
                    fi.Directory.Create();
                }
                FileStream fs = new FileStream(fullPath, System.IO.FileMode.Append, System.IO.FileAccess.Write);
                StreamWriter sw = new StreamWriter(fs, System.Text.Encoding.UTF8);
                sw.WriteLine(str);
                sw.Close();
                fs.Close();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("操作文件已打开，写入失败");
            }

        }


        public static void SaveResult(ResultInfor result,string path)
        {
            PropertyInfo[] propertyInfos = result.GetType().GetProperties();
            string hearder = string.Empty;
            string data = string.Empty;
            for (int i = 0; i < propertyInfos.Count() - 3; i++)
            {
                if ((propertyInfos[i].PropertyType == typeof(Googo.Manul.PositionInfor)))
                {
                    PositionInfor p = (PositionInfor)propertyInfos[i].GetValue(result, null);
                    hearder += propertyInfos[i].Name + "_X" + ",";
                    data += $"{p.XPos}" + ",";
                    hearder += propertyInfos[i].Name + "_Y" + ",";
                    data += $"{p.YPos}" + ",";
                    hearder += propertyInfos[i].Name + "_Z" + ",";
                    data += $"{p.ZPos}" + ",";
                }
                else if ((propertyInfos[i].PropertyType == typeof(List<float>)))
                {
                    int tem = 0;
                    foreach (var item in propertyInfos[i].GetValue(result, null) as List<float>)
                    {
                        tem++;
                        hearder += propertyInfos[i].Name + "_H" + tem.ToString() + ",";
                        data += item.ToString("F3") + ",";
                    }
                }
                else
                {
                    hearder += propertyInfos[i].Name + ",";
                    data += propertyInfos[i].GetValue(result, null).ToString() + ",";
                }
            }
            SaveData(path, hearder, data);
        }
    }
}
