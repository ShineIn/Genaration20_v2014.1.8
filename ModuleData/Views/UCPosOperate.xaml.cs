﻿using G4.Motion;
using Googo.Manul;
using ModuleData.Views;
using SuperSimpleTcp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ModuleData
{
    /// <summary>
    /// UCPosOperate.xaml 的交互逻辑
    /// </summary>
    public partial class UCPosOperate : UserControl, INotifyPropertyChanged
    {
        public UCPosOperate()
        {
            InitializeComponent();
            CommandBindings.Add(new CommandBinding(MoveCommand, Move_Excute));
        }

        public static DependencyProperty PositionNameProperty = DependencyProperty.Register("PositionName", typeof(string), typeof(UCPosOperate), new FrameworkPropertyMetadata(string.Empty, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty PositionProperty = DependencyProperty.Register("Position", typeof(object), typeof(UCPosOperate), new FrameworkPropertyMetadata(0.0, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));
        public static DependencyProperty XAxisNumProperty = DependencyProperty.Register("XAxisNum", typeof(EnumAxisType), typeof(UCPosOperate), new FrameworkPropertyMetadata(EnumAxisType.X, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty YAxisNumProperty = DependencyProperty.Register("YAxisNum", typeof(EnumAxisType), typeof(UCPosOperate), new FrameworkPropertyMetadata(EnumAxisType.Y, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty ZAxisNumProperty = DependencyProperty.Register("ZAxisNum", typeof(EnumAxisType), typeof(UCPosOperate), new FrameworkPropertyMetadata(EnumAxisType.Z, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static RoutedCommand MoveCommand = new RoutedCommand();

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        public string PositionName
        {
            get { return (string)GetValue(PositionNameProperty); }
            set { SetValue(PositionNameProperty, value); }
        }

        public object Position
        {
            get { return GetValue(PositionProperty); }
            set { SetValue(PositionProperty, value); }
        }

        public EnumAxisType XAxisNum
        {
            get { return (EnumAxisType)GetValue(XAxisNumProperty); }
            set { SetValue(XAxisNumProperty, value); OnPropertyChanged("XSingleMove"); }
        }

        public EnumAxisType YAxisNum
        {
            get { return (EnumAxisType)GetValue(YAxisNumProperty); }
            set { SetValue(YAxisNumProperty, value);OnPropertyChanged("YSingleMove"); }
        }

        public EnumAxisType ZAxisNum
        {
            get { return (EnumAxisType)GetValue(ZAxisNumProperty); }
            set { SetValue(ZAxisNumProperty, value); OnPropertyChanged("ZSingleMove"); }
        }

        public string XSingleMove
        {
            get { return $"{XAxisNum}移动"; }
        }

        public string YSingleMove
        {
            get { return $"{YAxisNum}移动"; }
        }

        public string ZSingleMove
        {
            get { return $"{ZAxisNum}移动"; }
        }

        public Visibility YMoveVisible
        {
            get { return _yMoveVisible; }
            set { _yMoveVisible = value; OnPropertyChanged(); }
        }
        private Visibility _yMoveVisible = Visibility.Visible;

        public Visibility ZMoveVisible
        {
            get { return _zMoveVisible; }
            set { _zMoveVisible = value; OnPropertyChanged(); }
        }
        private Visibility _zMoveVisible = Visibility.Visible;

        private void Move_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            if (null == Position || null == e.Parameter)
            {
                return;
            }
            if (null != moveTask && !moveTask.IsCompleted)
            {              
                return;
            }
            string strCommand = e.Parameter.ToString();
            Type moveType = Position.GetType();
            EnumAxisType xtemp = XAxisNum;
            EnumAxisType ytemp = YAxisNum;
            EnumAxisType ztemp = ZAxisNum;

            if (moveType==typeof(double))
            {
                double tarPos = (double)Position;
                moveTask = Task.Run(() => { MotionModel.Instance.MoveTo(xtemp,tarPos); });
            }
            if (moveType == typeof(Point))
            {
                Point point = (Point)Position;
                if (strCommand == X)
                {
                    moveTask = Task.Run(() => { MotionModel.Instance.MoveTo(xtemp, point.X); });
                }
                else if (strCommand == Y)
                { 
                    moveTask = Task.Run(() => { MotionModel.Instance.MoveTo(ytemp, point.Y); });
                }
            }
            if (moveType == typeof(PositionInfor))
            {
                PositionInfor pos = (PositionInfor)Position;
                if (strCommand == X)
                {
                    moveTask = Task.Run(() => { MotionModel.Instance.MoveTo(xtemp, pos.XPos); });
                }
                else if (strCommand == Y)
                {
                    moveTask = Task.Run(() => { MotionModel.Instance.MoveTo(ytemp, pos.YPos); });
                }
                else if (strCommand == Z)
                { 
                    moveTask = Task.Run(() => { MotionModel.Instance.MoveTo(ztemp, pos.ZPos); });
                }
            }
        }


        private static void DepenProChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            UCPosOperate flow = d as UCPosOperate;
            if (null == flow || null == e.NewValue) return;
            if (e.Property == PositionProperty)
            {
                Type type = e.NewValue.GetType();
                if (null == type) return;
                if (type==typeof(double))
                {
                    flow.YMoveVisible = Visibility.Collapsed;
                    flow.ZMoveVisible = Visibility.Collapsed;
                }
                else if (type == typeof(Point))
                {
                    flow.YMoveVisible = Visibility.Visible;
                    flow.ZMoveVisible = Visibility.Collapsed;
                }
                else if (type == typeof(PositionInfor))
                {
                    flow.YMoveVisible = Visibility.Visible;
                    flow.ZMoveVisible = Visibility.Visible;
                }
            }
        }

        private void Stop_Click(object sender, RoutedEventArgs e)
        {
            MotionModel.Instance.StopMoving(XAxisNum);
            MotionModel.Instance.StopMoving(YAxisNum);
            MotionModel.Instance.StopMoving(ZAxisNum);
        }

        private void Update_Click(object sender, RoutedEventArgs e)
        {
            if (null == Position)
            {
                return;
            }
            if (null != moveTask && !moveTask.IsCompleted)
            {
                return;
            }  
            Type moveType = Position.GetType();
            EnumAxisType xtemp = XAxisNum;
            EnumAxisType ytemp = YAxisNum;
            EnumAxisType ztemp = ZAxisNum;

            if (moveType == typeof(double))
            {
                MotionModel.Instance.GetPosition(XAxisNum,out double tarPos);
                Position = tarPos;
            }
            if (moveType == typeof(Point))
            {
                MotionModel.Instance.GetPosition(XAxisNum, out double xtarPos);          
                MotionModel.Instance.GetPosition(YAxisNum, out double  ytarPos);
                Position = new Point(xtarPos,ytarPos);
            }
            if (moveType == typeof(PositionInfor))
            {
                PositionInfor pos = (PositionInfor)Position;
                MotionModel.Instance.GetPosition(XAxisNum, out double tarPos);
                pos.XPos = tarPos;
                MotionModel.Instance.GetPosition(YAxisNum, out  tarPos);
                pos.YPos = tarPos;
                MotionModel.Instance.GetPosition(ZAxisNum, out  tarPos);
                pos.ZPos = tarPos;
                Position = null;
                Position = pos;
            }
        }

        private const string X = "X";
        private const string Y = "Y";
        private const string Z = "Z";
        /// <summary>
        /// 移动任务，定义成静态避免多个地方操作会装机
        /// </summary>
        private static Task moveTask = null;


    }
}
