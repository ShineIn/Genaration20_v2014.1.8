﻿using G4.GlueCore;
using G4.Motion;
using LogSv;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using YHSDK;
using System.Windows;
using SuperSimpleTcp;
using System.Diagnostics;
using Googo.Manul;
using Newtonsoft.Json;

namespace ModuleData.Views
{
    [Serializable]
    public partial class uc_SpotCheck : ModuleBase
    {
        public uc_SpotCheck()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 点胶对象
        /// </summary>

        string mLocationBuffer = string.Empty;
        string SensorLastContent = string.Empty;
        int retryNum = 0;
        private CommonSignal commonSignal = null;
        #region 公有属性
        public SpotCheck DataModel
        {
            get { return model; }
            set { model = value; OnPropertyChanged(); }
        }
        private SpotCheck model = new SpotCheck();

        /// <summary>
        /// 自动点检的结果
        /// </summary>
        [Browsable(false)]
        public bool AutoSpotCheckResult
        {
            get { return autoSpotCheckResult; }
            private set { autoSpotCheckResult = value; OnPropertyChanged(); }
        }
        bool autoSpotCheckResult = false;

        /// <summary>
        /// 测高传感器TCP对象
        /// </summary>
        /// <remarks>
        /// TCP对象，到最外层直接进行绑定
        /// </remarks>
        [XmlIgnore]
        public SimpleTcpClient SensorClient
        {
            get { return _sensorClient; }
            set
            {
                if (_sensorClient != null && _sensorClient.Events != null)
                {
                    _sensorClient.Events.DataReceived -= sensorDataReceived;
                    _sensorClient.Events.Disconnected -= Events_Disconnected;
                }
                _sensorClient = value;
                if (value != null && value.Events != null)
                {
                    value.Events.DataReceived += sensorDataReceived;
                    value.Events.Disconnected += Events_Disconnected;
                }
                OnPropertyChanged();
            }
        }
        private SimpleTcpClient _sensorClient;
        #endregion 公有属性

        #region 公有函数
        protected override void Run()
        {
            if (null == model || null == model.ParentProject) return;
            glueProject = model.ParentProject as GlueProject;
            if (null == glueProject) throw new NullReferenceException("模块的父点胶模组参数为空");
            if (glueProject.ExcuteGlueModule == null) throw new Exception("点胶参数为空，或者点胶文件路径不存在");
            if (fc_GlueAuto1.WorkFlow == null)
                fc_GlueAuto1.TaskReset();
            fc_GlueAuto1.TaskRun();
        }

        public override void ResetChart()
        {
            StartChart = fc_GlueAuto1;
            fc_GlueAuto1.TaskReset();
        }

        private int MoveTo(double X, double Y, double Z, bool IsSafety = true)
        {
            double safetyPos = 0;
            double posX = 999, posY = 999, posZ = 999;
            glueProject.ExcuteGlueModule.GetPosition(out posX, out posY, out posZ);
            var rtn = -1;
            if (IsSafety)
            {
                if (posZ > safetyPos)
                {
                    rtn = glueProject.ExcuteGlueModule.ExecuteZMoveTo(safetyPos, 20, 20);
                    if (rtn != 0) return rtn;
                }
            }

            rtn = glueProject.ExcuteGlueModule.ExecuteXYMoveTo(X, Y, 20, 20);
            if (rtn != 0) return rtn;
            return glueProject.ExcuteGlueModule.ExecuteZMoveTo(Z, 20, 20);
        }

        #endregion 公有函数

        private YHSDK.FCResultType fc_Glueflow1_FlowRun(object sender, EventArgs e)
        {
            commonSignal=null;
            AutoSpotCheckResult = false;
            if (model.IsHaveStartSignal(out CommonSignal signal))
            {
                signal.WaitStartReset();
                commonSignal = signal;
                model.ParentProject.UpProcessAddre = model.ParentProject.ProcessAddre;
                PrintLog(string.Format($"接收到点检任务指令"), Color.Black);
                return FCResultType.NEXT;
            }
            return FCResultType.IDLE;
        }

        private FCResultType fc_Glueflow2_FlowRun(object sender, EventArgs e)
        {
            var rtn = MoveTo(model.AutoCheckVisPoint.X, model.AutoCheckVisPoint.Y, model.AutoCheckVisZ);
            if (rtn == 0)
            {
                DCTcpClient.Send("SpotCheck");
                while (string.IsNullOrEmpty(mLocationBuffer) && retryNum < 40)
                {
                    Thread.Sleep(200);
                    mLocationBuffer = AlgLastContent;
                    retryNum++;
                }
                if (string.IsNullOrEmpty(mLocationBuffer))
                {
                    PrintLog($"DCCK TCP接收信息超时", System.Drawing.Color.Red);
                    AutoSpotCheckResult = false;
                    return FCResultType.NEXT;
                }
                string[] res = mLocationBuffer.Split(',');
                var StrNames = new string[] { "视觉点检X", "视觉点检Y", "视觉点检R" };
                if (null == res || res.Length == 0)
                {
                    PrintLog("自动点检视觉失败，德创返回数据格式有误", Color.Red);
                    AutoSpotCheckResult = false;
                }
                for (int i = 0; i < Math.Min(3, res.Length); i++)
                {
                    string strName1 = StrNames[i];
                    ParameterItem item1 = model.AllResult.FirstOrDefault(tt => { return tt.Name == strName1; });
                    if (null == item1)
                    {
                        item1 = new ParameterItem { Name = strName1, Datatype = typeof(float) };
                        model.AllResult.Add(item1);
                    }
                    item1.Value = Convert.ToDouble(res[i]);
                    item1.UpLoads();
                    AutoSpotCheckResult = true;
                }
            }
            else
            {
                PrintLog("自动点检视觉失败", Color.Red);
                AutoSpotCheckResult = false;
            }
            return FCResultType.NEXT;
        }

        private FCResultType fc_Glueflow3_FlowRun(object sender, EventArgs e)
        {
            var rtn = MoveTo(model.ParentProject.GlueSafetyPos.XPos, model.ParentProject.GlueSafetyPos.YPos, model.ParentProject.GlueSafetyPos.ZPos, true);
            if (rtn != 0)
            {
                PrintLog("点胶模组运动到安全位失败", Color.Red);
                return FCResultType.IDLE;
            }
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart4_FlowRun(object sender, EventArgs e)
        {
            commonSignal.WaitRuningReset(AutoSpotCheckResult);
            PrintLog("自动点检结束", Color.Black);
            Thread.Sleep(1000);
            model.ParentProject.UpProcessAddre = 0;
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart1_FlowRun(object sender, EventArgs e)
        {
            if (glueProject == null) return FCResultType.IDLE;
            if (glueProject.UVLightUp())
            {
                return FCResultType.NEXT;
            }
            else
            {
                return FCResultType.IDLE;
            }
        }


        private GlueProject glueProject = null;
        private GlueModule module = null;

        private FCResultType npFlowChart2_FlowRun(object sender, EventArgs e)
        {
            if (glueProject == null) return FCResultType.IDLE;
            if (!glueProject.SpotCheckModel.IsUseMoveCylinder)
            {
                if (commonSignal==model.VisionMark)
                    return FCResultType.NEXT;
                else if (commonSignal == model.KeyenceHeightLaserTest)
                    return FCResultType.CASE1;
                else
                    return FCResultType.CASE2;
            }
            else
            {
                Googo.Manul.GloMotionParame.OutputList.SetValue("OB_MoveDispenseCyOut", true);
                Googo.Manul.GloMotionParame.OutputList.SetValue("OB_MoveDispenseCIn", false);
                if (Googo.Manul.GloMotionParame.InputList.GetValue("IB_MoveDispenseVacuumOut") && !Googo.Manul.GloMotionParame.InputList.GetValue("IB_MoveDispenseVacuumIn"))
                {
                    if (commonSignal == model.VisionMark)
                        return FCResultType.NEXT;
                    else if (commonSignal == model.KeyenceHeightLaserTest)
                        return FCResultType.CASE1;
                    else
                        return FCResultType.CASE2;
                }
                else
                {
                    return FCResultType.IDLE;
                }
            }
        }

        private FCResultType npFlowChart3_FlowRun(object sender, EventArgs e)
        {
            if (glueProject == null) return FCResultType.IDLE;
            if (!glueProject.AutoCalibra.IsUseMoveCylinder)
            {
                return FCResultType.NEXT;
            }
            else
            {
                Googo.Manul.GloMotionParame.OutputList.SetValue("OB_MoveDispenseCyOut", false);
                Googo.Manul.GloMotionParame.OutputList.SetValue("OB_MoveDispenseCIn", true);
                if (!Googo.Manul.GloMotionParame.InputList.GetValue("IB_MoveDispenseVacuumOut") && Googo.Manul.GloMotionParame.InputList.GetValue("IB_MoveDispenseVacuumIn"))
                {
                    return FCResultType.NEXT;
                }
                else
                {
                    return FCResultType.IDLE;
                }
            }
        }

        private FCResultType npFlowChart5_FlowRun(object sender, EventArgs e)
        {
            var rtn = MoveTo(model.AutoCheckLaserPoint.X, model.AutoCheckLaserPoint.Y, model.AutoCheckLaserZ);
            if (rtn == 0)
            {
                if (GetSensorData())
                {
                    PrintLog("自动点检测高成功", Color.Green);
                    AutoSpotCheckResult = true;
                }
                else
                {
                    PrintLog("自动点检测高失败", Color.Green);
                    AutoSpotCheckResult = false;
                }
            }
            else
            {
                PrintLog("自动点检测高失败", Color.Red);
                AutoSpotCheckResult = false;
            }
            return FCResultType.NEXT;
        }

        /// <summary>
        /// 获取高度信息
        /// </summary>
        /// <returns></returns>
        private bool GetSensorData()
        {
            if (null == SensorClient || !SensorClient.IsConnected)
            {
                return false;
            }
            //通知德创视觉Location任务
            SensorClient.Send("M0\r\n");
            string mLocationBuffer = string.Empty;
            int iretryCount = 0;
            while (string.IsNullOrEmpty(mLocationBuffer) && iretryCount < 50)
            {
                iretryCount++;
                Thread.Sleep(100);
                mLocationBuffer = SensorLastContent;
            }
            if (string.IsNullOrEmpty(mLocationBuffer))
            {
                return false;
            }
            var arr = mLocationBuffer.Split(',');
            if (arr == null || arr.Length < 2)
            {
                return false;
            }
            if (double.TryParse(arr[1], out double val))
            {
                if (-099999998 == val)
                    return false;
                double heightOffset = val * 0.001;
                string strName = $"测量高度_0";
                ParameterItem item = model.AllResult.FirstOrDefault(tt => { return tt.Name == strName; });
                if (null == item)
                {
                    item = new ParameterItem { Name = strName, Datatype = typeof(float) };
                    model.AllResult.Add(item);
                }
                item.Value = heightOffset;
                item.UpLoads();
                PrintLog(string.Format("位移传感器的测量值：{0}", (val * 0.001).ToString("f3")), Color.Black);
                return true;
            }
            return false;
        }

        /// <summary>
        /// 算法客户端接受信息事件
        /// </summary>
        /// <remarks>
        /// 可以重新该事件，在里面做自己想要的事件处理
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sensorDataReceived(object sender, SuperSimpleTcp.DataReceivedEventArgs e)
        {
            if (e.Data == null) return;
            string strRec = Encoding.UTF8.GetString(e.Data.Array);
            SensorLastContent = strRec;
        }

        private void Events_Disconnected(object sender, SuperSimpleTcp.ConnectionEventArgs e)
        {
            //Debug.WriteLine($"Port={e.IpPort} Disconnected Reason={e.Reason}");
        }

        private FCResultType npFlowChart6_FlowRun(object sender, EventArgs e)
        {
            if (null == glueProject.GlueExcuteModel.GlueCamPointsOrder || glueProject.GlueExcuteModel.GlueCheckPointOrders.Count == 0)
            {
                PrintLog("测高点位为空", Color.Red);
                return FCResultType.IDLE;
            }
            for (int i = 0; i < glueProject.GlueExcuteModel.GlueCheckPointOrders.Count; i++)
            {
                PositionInfor point = glueProject.GlueExcuteModel.GlueCheckPointOrders[i];
                if (null == point || point.ExtenCommand == null) continue;
                //int irtn = MoveTo(point.XPos + _runSlot.Offset.X, point.YPos + _runSlot.Offset.Y, point.ZPos);
                int irtn = MoveTo(point.XPos, point.YPos, point.ZPos);
                byte[] dataRev = new byte[1024];
                //通知德创视觉Location任务
                string mLocationBuffer = AlgLastContent;
                point.ExtenCommand.DownLoad();
                point.ExtenCommand.SN = "SpotCheck";
                String strCommand = !string.IsNullOrEmpty(point.StrCommand) ? point.StrCommand : JsonConvert.SerializeObject(point.ExtenCommand);

                if (string.IsNullOrEmpty(strCommand)) continue;
                DCTcpClient.Send(strCommand);
                mLocationBuffer = string.Empty;
                int iretry = 0;
                while (string.IsNullOrEmpty(mLocationBuffer) && iretry < 20)
                {
                    System.Threading.Thread.Sleep(100);
                    mLocationBuffer = AlgLastContent;
                    iretry++;
                }
                if (string.IsNullOrEmpty(mLocationBuffer))
                {
                    PrintLog($"DCCK TCP连接断开", System.Drawing.Color.Red);
                    return FCResultType.IDLE;
                }
                ReceiveData glueDisCheck = JsonConvert.DeserializeObject<ReceiveData>(mLocationBuffer);
                if (null == glueDisCheck || glueDisCheck.ResultList == null)
                {
                    PrintLog($"视觉未发送点胶检测结果", System.Drawing.Color.Red);
                    return FCResultType.CASE1;
                }
                foreach (var tempRe in glueDisCheck.ResultList)
                {
                    var mathres = model.AllResult.FirstOrDefault((tt) => { return tt.Name == tempRe.Name && tt.Datatype == tempRe.Datatype; });
                    if (null == mathres)
                    {
                        model.AllResult.Add(tempRe);
                        mathres = tempRe;
                    }
                    mathres.Value = tempRe.Value;
                    mathres.UpLoads();
                }
                var mathch = glueDisCheck.ResultList.FirstOrDefault((tt) => { return tt.Datatype == typeof(bool); });
                AutoSpotCheckResult = null != mathch && (bool)mathch.Value;
            }
            return FCResultType.NEXT;
        }
    }
}
