﻿using G4.GlueCore;
using G4.Motion;
using LogSv;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using YHSDK;
using System.Windows;

namespace ModuleData.Views
{
    [Serializable]
    public partial class uc_GlueExecuteAlignNeedle : ModuleBase
    {
        public uc_GlueExecuteAlignNeedle()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 点胶对象
        /// </summary>
  
        private JTimer JM_GlueModule = new JTimer();
        private int GlueModuleTaskRtn = 0;
        private Task GlueModuleTask = null;
        double minOrMax_X = 5;//X方向偏差补偿极限
        double minOrMax_Y = 5;
        double minOrMax_Z = 5;
        //基准位置
        double benchX = 0;
        double benchY = 0;

        #region 公有属性
        public AutoCalibraNeedle DataModel
        {
            get { return model; }
            set { model = value; OnPropertyChanged(); }
        }
        private AutoCalibraNeedle model = new AutoCalibraNeedle();

        /// <summary>
        /// 自动对针的结果
        /// </summary>
        [Browsable(false)]
        public bool AutoNeedelResult
        {
            get { return gutoNeedelResult; }
            private set { gutoNeedelResult = value; OnPropertyChanged(); }
        }
        bool gutoNeedelResult = false;

        /// <summary>
        /// 对针中心的X
        /// </summary>
        [Browsable(false)]
        public double ExecuteAlignNeedle_X
        {
            get { return executeAlignNeedle_X; }
            private set { executeAlignNeedle_X = value; OnPropertyChanged(); }
        }
        double executeAlignNeedle_X = 0;
        /// <summary>
        /// 对针中心的Y
        /// </summary>
        [Browsable(false)]
        public double ExecuteAlignNeedle_Y
        {
            get { return executeAlignNeedle_Y; }
            private set { executeAlignNeedle_Y = value; OnPropertyChanged(); }
        }
        double executeAlignNeedle_Y = 0;
        /// <summary>
        /// 对针中心的Z
        /// </summary>
        [Browsable(false)]
        public double ExecuteAlignNeedle_Z
        {
            get { return executeAlignNeedle_Z; }
            private set { executeAlignNeedle_Z = value; OnPropertyChanged(); }
        }
        double executeAlignNeedle_Z = 0;
        
        #endregion 公有属性

        #region 公有函数
        protected override void Run()
        {
            if (null == model || null == model.ParentProject) return;
            glueProject = model.ParentProject as GlueProject;
            if (null == glueProject) throw new NullReferenceException("模块的父点胶模组参数为空");
            if (glueProject.ExcuteGlueModule == null) throw new Exception("点胶参数为空，或者点胶文件路径不存在");
            if (fc_GlueAuto1.WorkFlow == null)
                fc_GlueAuto1.TaskReset();
            fc_GlueAuto1.TaskRun();
            //if (!IsWaitForSignal)
            //{
            //    model.ParentProject.PLC_StandbyPosition = false;
            //}
        }

        public override void ResetChart()
        {
            StartChart = fc_GlueAuto1;
            fc_GlueAuto1.TaskReset();
        }

        private int MoveTo(double X, double Y, double Z, bool IsSafety = true)
        {
            double safetyPos = 0;
            double posX = 999, posY = 999, posZ = 999;
            glueProject.ExcuteGlueModule.GetPosition(out posX, out posY, out posZ);
            var rtn = -1;
            if (IsSafety)
            {
                if (posZ > safetyPos)
                    rtn = glueProject.ExcuteGlueModule.ExecuteZMoveTo(safetyPos, 20, 20);
                else
                    rtn = 0;
                if (rtn == 0)
                {
                    var rtnXY = glueProject.ExcuteGlueModule.ExecuteXYMoveTo(X, Y, 20, 20);
                    if (0 == rtnXY)
                    {
                        return glueProject.ExcuteGlueModule.ExecuteZMoveTo(Z, 20, 20);
                    }
                    else
                        return -1;
                }
                else
                    return -1;
            }
            else
            {
                 rtn = glueProject.ExcuteGlueModule.ExecuteXYMoveTo(X, Y, 20, 20);
                if (0 == rtn)
                {
                    return glueProject.ExcuteGlueModule.ExecuteZMoveTo(Z, 20, 20);
                }
                else
                    return -1;

            }
        }

        #endregion 公有函数

        private YHSDK.FCResultType fc_Glueflow1_FlowRun(object sender, EventArgs e)
        {
            //model.ParentProject.PLC_StandbyPosition = true;
            bool bret = model.PLC_Sig_AutoNeedel;
            if (bret)
            {
                minOrMax_X = model.PLC_IPC_AutoNeedelOffsetX;
                minOrMax_Y = model.PLC_IPC_AutoNeedelOffsetY;
                minOrMax_Z = model.PLC_IPC_AutoNeedelOffsetZ;
                model.PLC_Sig_AutoNeedel = bret;
                module = new GlueModule(model.ParentProject.AxisWorkModule, model.ParentProject.GluePram);
                model.SoftSigAutoNeedel = true;
                model.SoftSigAutoNeedelFin = false;      

                model.SigAutoNeedelNG = false;
                model.SigAutoNeedelOK = false;
                model.ParentProject.UpProcessAddre = model.ParentProject.ProcessAddre;
                PrintLog(string.Format($"接收到对针任务指令，对针开始工作"), Color.Black);
                return FCResultType.NEXT;
            }
            return FCResultType.IDLE;
        }

        private FCResultType fc_Glueflow2_FlowRun(object sender, EventArgs e)
        {
            var rtn = module.ExecuteAlignNeedle(ref executeAlignNeedle_X, ref executeAlignNeedle_Y, ref executeAlignNeedle_Z);
            if (rtn == 0)
            {
                PrintLog("自动对针成功", Color.Green);
                AutoNeedelResult = true;
            }
            else
            {
                PrintLog("自动对针失败", Color.Red);
                AutoNeedelResult = false;

            }
            return FCResultType.NEXT;
        }

        private FCResultType fc_Glueflow3_FlowRun(object sender, EventArgs e)
        {
            var rtn = MoveTo(model.ParentProject.GlueSafetyPos.XPos, model.ParentProject.GlueSafetyPos.YPos, model.ParentProject.GlueSafetyPos.ZPos, true);
            if (rtn != 0)
            {
                PrintLog("点胶模组运动到安全位失败", Color.Red);
                return FCResultType.IDLE;
            }
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart4_FlowRun(object sender, EventArgs e)
        {
            if (AutoNeedelResult)
            {
                double offsetX = executeAlignNeedle_X - model.AutoNeedelPoint.X;
                double offsetY = executeAlignNeedle_Y - model.AutoNeedelPoint.Y;
                double offsetZ = executeAlignNeedle_Z - model.AutoNeedelHeight;
                bool value_X = Math.Abs(offsetX) > minOrMax_X;
                bool value_Y = Math.Abs(offsetY) > minOrMax_Y;
                bool value_Z = Math.Abs(offsetZ) > minOrMax_Z;
                if (value_X || value_Y || value_Z)
                {
                    AutoNeedelResult = false;
                    PrintLog(string.Format("自动对针偏差超过极限，OffsetXYZ:{0},{1},{2}", offsetX.ToString("F3"), offsetY.ToString("F3"), offsetZ.ToString("F3")), Color.Red);
                    return FCResultType.IDLE;
                }

                model.IPC_PLC_AutoNeedelOffsetX = (float)offsetX;
                model.IPC_PLC_AutoNeedelOffsetY = (float)offsetY;
                model.IPC_PLC_AutoNeedelOffsetZ = (float)offsetZ;

                model.SigAutoNeedelNG = false;
                model.SigAutoNeedelOK = true;
                Thread.Sleep(20);
                if (model.SigAutoNeedelNG == false && model.SigAutoNeedelOK == true)
                {
                    model.SoftSigAutoNeedelFin = true;
                    Thread.Sleep(200);
                    if (model.SoftSigAutoNeedelFin == true)
                    {
                        PrintLog(string.Format("自动对针位置XYZ:{0},{1},{2}", executeAlignNeedle_X.ToString("F3"), executeAlignNeedle_Y.ToString("F3"), executeAlignNeedle_Z.ToString("F3")), Color.Black);
                        PrintLog(string.Format("自动对针位置偏差XYZ:{0},{1},{2}", offsetX.ToString("F3"), offsetY.ToString("F3"), offsetZ.ToString("F3")), Color.Black);
                        PrintLog("自动对针成功", Color.Black);
                    }
                    else
                    {
                        return FCResultType.IDLE;
                    }
                }
                else
                {
                    return FCResultType.IDLE;
                }
            }
            else
            {
                model.SigAutoNeedelNG = true;
                model.SigAutoNeedelOK = false;
                Thread.Sleep(20);
                if (model.SigAutoNeedelNG == true && model.SigAutoNeedelOK == false)
                {
                    model.SoftSigAutoNeedelFin = true;
                    Thread.Sleep(200);
                    if (model.SoftSigAutoNeedelFin == true)
                    {
                        PrintLog("自动对针失败", Color.Red);
                    }
                    else
                    {
                        return FCResultType.IDLE;
                    }
                }
                else
                {
                    return FCResultType.IDLE;
                }
            }
            PrintLog("自动对针结束", Color.Black);
            Thread.Sleep(1000);
            model.ParentProject.UpProcessAddre = 0;
            return FCResultType.NEXT;
        }

        private FCResultType npFlowChart1_FlowRun(object sender, EventArgs e)
        {
            if (glueProject == null) return FCResultType.IDLE;
            if (glueProject.UVLightUp() || !glueProject.IsContainUV)
            {
                return FCResultType.NEXT;
            }
            else
            {
                return FCResultType.IDLE;
            }
        }


        private GlueProject glueProject = null;
        private GlueModule module = null;

        private FCResultType npFlowChart2_FlowRun(object sender, EventArgs e)
        {
            if (glueProject == null) return FCResultType.IDLE;
            if (!glueProject.AutoCalibra.IsUseMoveCylinder)
            {
                return FCResultType.NEXT;
            }
            else
            {
                Googo.Manul.GloMotionParame.OutputList.SetValue("OB_MoveDispenseCyOut", true);
                Googo.Manul.GloMotionParame.OutputList.SetValue("OB_MoveDispenseCIn", false);
                if (Googo.Manul.GloMotionParame.InputList.GetValue("IB_MoveDispenseVacuumOut") && !Googo.Manul.GloMotionParame.InputList.GetValue("IB_MoveDispenseVacuumIn"))
                {
                    return FCResultType.NEXT;
                }
                else
                {
                    return FCResultType.IDLE;
                }
            }
        }

        private FCResultType npFlowChart3_FlowRun(object sender, EventArgs e)
        {
            if (glueProject == null) return FCResultType.IDLE;
            if (!glueProject.AutoCalibra.IsUseMoveCylinder)
            {
                return FCResultType.NEXT;
            }
            else
            {
                Googo.Manul.GloMotionParame.OutputList.SetValue("OB_MoveDispenseCyOut", false);
                Googo.Manul.GloMotionParame.OutputList.SetValue("OB_MoveDispenseCIn", true);
                if (!Googo.Manul.GloMotionParame.InputList.GetValue("IB_MoveDispenseVacuumOut") && Googo.Manul.GloMotionParame.InputList.GetValue("IB_MoveDispenseVacuumIn"))
                {
                    return FCResultType.NEXT;
                }
                else
                {
                    return FCResultType.IDLE;
                }
            }
        }
    }
}
