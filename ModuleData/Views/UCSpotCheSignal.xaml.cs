﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ModuleData
{
    /// <summary>
    /// UCSpotCheSignal.xaml 的交互逻辑
    /// </summary>
    public partial class UCSpotCheSignal : UserControl
    {
        public UCSpotCheSignal()
        {
            InitializeComponent();
        }

        public static DependencyProperty SubObejctProperty = DependencyProperty.Register("SubObejct", typeof(CommonSignal), typeof(UCSpotCheSignal), new FrameworkPropertyMetadata(new CommonSignal(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public CommonSignal SubObejct
        {
            get { return (CommonSignal)GetValue(SubObejctProperty); }
            set { SetValue(SubObejctProperty, value); }
        }
    }
}
