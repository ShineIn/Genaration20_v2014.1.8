﻿namespace ModuleData.Views
{
    partial class uc_GlueExecuteDrain
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.npFlowChart1 = new YHSDK.NPFlowChart();
            this.fc_Glueflow2 = new YHSDK.NPFlowChart();
            this.fc_Glueflow3 = new YHSDK.NPFlowChart();
            this.npFlowChart4 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto1 = new YHSDK.NPFlowChart();
            this.SuspendLayout();
            // 
            // npFlowChart1
            // 
            this.npFlowChart1.AlarmCode = "";
            this.npFlowChart1.ArrowsWidth = 1;
            this.npFlowChart1.BackColor = System.Drawing.Color.White;
            this.npFlowChart1.bClearTrace = false;
            this.npFlowChart1.bIsLastFlowChart = false;
            this.npFlowChart1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.npFlowChart1.bSkip = false;
            this.npFlowChart1.bStepByStepMode = false;
            this.npFlowChart1.CASE1 = null;
            this.npFlowChart1.Case1ArrowType = YHSDK.ArrowTypes.Square;
            this.npFlowChart1.CASE1Color = System.Drawing.Color.LimeGreen;
            this.npFlowChart1.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.npFlowChart1.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.npFlowChart1.CASE2 = null;
            this.npFlowChart1.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart1.CASE2Color = System.Drawing.Color.Red;
            this.npFlowChart1.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.CASE3 = null;
            this.npFlowChart1.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart1.CASE3Color = System.Drawing.Color.Blue;
            this.npFlowChart1.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.DefaultColor = System.Drawing.Color.White;
            this.npFlowChart1.eSkipPath = YHSDK.FCResultType.NEXT;
            this.npFlowChart1.ExecutedColor = System.Drawing.Color.LightGray;
            this.npFlowChart1.ExecutingColor = System.Drawing.Color.Lime;
            this.npFlowChart1.Location = new System.Drawing.Point(27, 90);
            this.npFlowChart1.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.npFlowChart1.menuOpening = false;
            this.npFlowChart1.Name = "npFlowChart1";
            this.npFlowChart1.NEXT = this.fc_Glueflow2;
            this.npFlowChart1.NextArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart1.NEXTColor = System.Drawing.Color.Black;
            this.npFlowChart1.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.Size = new System.Drawing.Size(289, 33);
            this.npFlowChart1.SkipColor = System.Drawing.Color.Yellow;
            this.npFlowChart1.SubFlowChart = null;
            this.npFlowChart1.TabIndex = 56;
            this.npFlowChart1.Text = "UV气缸抬起";
            this.npFlowChart1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.npFlowChart1.TimeOut = 10000;
            this.npFlowChart1.TimeoutColor = System.Drawing.Color.Red;
            this.npFlowChart1.tmrTimeOut = null;
            this.npFlowChart1.FlowRun += new YHSDK.FlowRunEvent(this.npFlowChart1_FlowRun);
            // 
            // fc_Glueflow2
            // 
            this.fc_Glueflow2.AlarmCode = "";
            this.fc_Glueflow2.ArrowsWidth = 1;
            this.fc_Glueflow2.BackColor = System.Drawing.Color.White;
            this.fc_Glueflow2.bClearTrace = false;
            this.fc_Glueflow2.bIsLastFlowChart = false;
            this.fc_Glueflow2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_Glueflow2.bSkip = false;
            this.fc_Glueflow2.bStepByStepMode = false;
            this.fc_Glueflow2.CASE1 = null;
            this.fc_Glueflow2.Case1ArrowType = YHSDK.ArrowTypes.Square;
            this.fc_Glueflow2.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_Glueflow2.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.fc_Glueflow2.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.fc_Glueflow2.CASE2 = null;
            this.fc_Glueflow2.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_Glueflow2.CASE2Color = System.Drawing.Color.Red;
            this.fc_Glueflow2.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_Glueflow2.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_Glueflow2.CASE3 = null;
            this.fc_Glueflow2.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_Glueflow2.CASE3Color = System.Drawing.Color.Blue;
            this.fc_Glueflow2.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_Glueflow2.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_Glueflow2.DefaultColor = System.Drawing.Color.White;
            this.fc_Glueflow2.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_Glueflow2.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_Glueflow2.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_Glueflow2.Location = new System.Drawing.Point(27, 163);
            this.fc_Glueflow2.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.fc_Glueflow2.menuOpening = false;
            this.fc_Glueflow2.Name = "fc_Glueflow2";
            this.fc_Glueflow2.NEXT = this.fc_Glueflow3;
            this.fc_Glueflow2.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_Glueflow2.NEXTColor = System.Drawing.Color.Black;
            this.fc_Glueflow2.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_Glueflow2.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_Glueflow2.Size = new System.Drawing.Size(289, 33);
            this.fc_Glueflow2.SkipColor = System.Drawing.Color.Yellow;
            this.fc_Glueflow2.SubFlowChart = null;
            this.fc_Glueflow2.TabIndex = 38;
            this.fc_Glueflow2.Text = "开始排胶";
            this.fc_Glueflow2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_Glueflow2.TimeOut = 10000;
            this.fc_Glueflow2.TimeoutColor = System.Drawing.Color.Red;
            this.fc_Glueflow2.tmrTimeOut = null;
            this.fc_Glueflow2.FlowRun += new YHSDK.FlowRunEvent(this.fc_Glueflow2_FlowRun);
            // 
            // fc_Glueflow3
            // 
            this.fc_Glueflow3.AlarmCode = "";
            this.fc_Glueflow3.ArrowsWidth = 1;
            this.fc_Glueflow3.BackColor = System.Drawing.Color.White;
            this.fc_Glueflow3.bClearTrace = false;
            this.fc_Glueflow3.bIsLastFlowChart = false;
            this.fc_Glueflow3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_Glueflow3.bSkip = false;
            this.fc_Glueflow3.bStepByStepMode = false;
            this.fc_Glueflow3.CASE1 = null;
            this.fc_Glueflow3.Case1ArrowType = YHSDK.ArrowTypes.Square;
            this.fc_Glueflow3.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_Glueflow3.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.fc_Glueflow3.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.fc_Glueflow3.CASE2 = null;
            this.fc_Glueflow3.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_Glueflow3.CASE2Color = System.Drawing.Color.Red;
            this.fc_Glueflow3.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_Glueflow3.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_Glueflow3.CASE3 = null;
            this.fc_Glueflow3.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_Glueflow3.CASE3Color = System.Drawing.Color.Blue;
            this.fc_Glueflow3.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_Glueflow3.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_Glueflow3.DefaultColor = System.Drawing.Color.White;
            this.fc_Glueflow3.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_Glueflow3.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_Glueflow3.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_Glueflow3.Location = new System.Drawing.Point(27, 235);
            this.fc_Glueflow3.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.fc_Glueflow3.menuOpening = false;
            this.fc_Glueflow3.Name = "fc_Glueflow3";
            this.fc_Glueflow3.NEXT = this.npFlowChart4;
            this.fc_Glueflow3.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_Glueflow3.NEXTColor = System.Drawing.Color.Black;
            this.fc_Glueflow3.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_Glueflow3.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_Glueflow3.Size = new System.Drawing.Size(289, 33);
            this.fc_Glueflow3.SkipColor = System.Drawing.Color.Yellow;
            this.fc_Glueflow3.SubFlowChart = null;
            this.fc_Glueflow3.TabIndex = 39;
            this.fc_Glueflow3.Text = "移动到安全位";
            this.fc_Glueflow3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_Glueflow3.TimeOut = 10000;
            this.fc_Glueflow3.TimeoutColor = System.Drawing.Color.Red;
            this.fc_Glueflow3.tmrTimeOut = null;
            this.fc_Glueflow3.FlowRun += new YHSDK.FlowRunEvent(this.fc_Glueflow3_FlowRun);
            // 
            // npFlowChart4
            // 
            this.npFlowChart4.AlarmCode = "";
            this.npFlowChart4.ArrowsWidth = 1;
            this.npFlowChart4.BackColor = System.Drawing.Color.White;
            this.npFlowChart4.bClearTrace = false;
            this.npFlowChart4.bIsLastFlowChart = false;
            this.npFlowChart4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.npFlowChart4.bSkip = false;
            this.npFlowChart4.bStepByStepMode = false;
            this.npFlowChart4.CASE1 = null;
            this.npFlowChart4.Case1ArrowType = YHSDK.ArrowTypes.Square;
            this.npFlowChart4.CASE1Color = System.Drawing.Color.LimeGreen;
            this.npFlowChart4.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.npFlowChart4.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.npFlowChart4.CASE2 = null;
            this.npFlowChart4.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart4.CASE2Color = System.Drawing.Color.Red;
            this.npFlowChart4.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart4.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart4.CASE3 = null;
            this.npFlowChart4.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart4.CASE3Color = System.Drawing.Color.Blue;
            this.npFlowChart4.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart4.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart4.DefaultColor = System.Drawing.Color.White;
            this.npFlowChart4.eSkipPath = YHSDK.FCResultType.NEXT;
            this.npFlowChart4.ExecutedColor = System.Drawing.Color.LightGray;
            this.npFlowChart4.ExecutingColor = System.Drawing.Color.Lime;
            this.npFlowChart4.Location = new System.Drawing.Point(27, 306);
            this.npFlowChart4.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.npFlowChart4.menuOpening = false;
            this.npFlowChart4.Name = "npFlowChart4";
            this.npFlowChart4.NEXT = this.fc_GlueAuto1;
            this.npFlowChart4.NextArrowType = YHSDK.ArrowTypes.Square;
            this.npFlowChart4.NEXTColor = System.Drawing.Color.Black;
            this.npFlowChart4.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart4.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart4.Size = new System.Drawing.Size(289, 33);
            this.npFlowChart4.SkipColor = System.Drawing.Color.Yellow;
            this.npFlowChart4.SubFlowChart = null;
            this.npFlowChart4.TabIndex = 40;
            this.npFlowChart4.Text = "复位PLC信号";
            this.npFlowChart4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.npFlowChart4.TimeOut = 10000;
            this.npFlowChart4.TimeoutColor = System.Drawing.Color.Red;
            this.npFlowChart4.tmrTimeOut = null;
            this.npFlowChart4.FlowRun += new YHSDK.FlowRunEvent(this.npFlowChart4_FlowRun);
            // 
            // fc_GlueAuto1
            // 
            this.fc_GlueAuto1.AlarmCode = "";
            this.fc_GlueAuto1.ArrowsWidth = 1;
            this.fc_GlueAuto1.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto1.bClearTrace = false;
            this.fc_GlueAuto1.bIsLastFlowChart = false;
            this.fc_GlueAuto1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto1.bSkip = false;
            this.fc_GlueAuto1.bStepByStepMode = false;
            this.fc_GlueAuto1.CASE1 = null;
            this.fc_GlueAuto1.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto1.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.CASE2 = null;
            this.fc_GlueAuto1.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto1.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.CASE3 = null;
            this.fc_GlueAuto1.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto1.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto1.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto1.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto1.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto1.Location = new System.Drawing.Point(27, 17);
            this.fc_GlueAuto1.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.fc_GlueAuto1.menuOpening = false;
            this.fc_GlueAuto1.Name = "fc_GlueAuto1";
            this.fc_GlueAuto1.NEXT = this.npFlowChart1;
            this.fc_GlueAuto1.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto1.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Size = new System.Drawing.Size(289, 33);
            this.fc_GlueAuto1.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto1.SubFlowChart = null;
            this.fc_GlueAuto1.TabIndex = 37;
            this.fc_GlueAuto1.Text = "等待排胶";
            this.fc_GlueAuto1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto1.TimeOut = 10000;
            this.fc_GlueAuto1.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto1.tmrTimeOut = null;
            this.fc_GlueAuto1.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto1_FlowRun);
            // 
            // uc_GlueExecuteDrain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.npFlowChart1);
            this.Controls.Add(this.npFlowChart4);
            this.Controls.Add(this.fc_Glueflow2);
            this.Controls.Add(this.fc_Glueflow3);
            this.Controls.Add(this.fc_GlueAuto1);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "uc_GlueExecuteDrain";
            this.Size = new System.Drawing.Size(596, 444);
            this.ResumeLayout(false);

        }

        #endregion

        private YHSDK.NPFlowChart npFlowChart4;
        private YHSDK.NPFlowChart fc_GlueAuto1;
        private YHSDK.NPFlowChart fc_Glueflow2;
        private YHSDK.NPFlowChart fc_Glueflow3;
        private YHSDK.NPFlowChart npFlowChart1;
    }
}
