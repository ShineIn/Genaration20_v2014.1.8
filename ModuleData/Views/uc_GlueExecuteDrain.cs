﻿using G4.GlueCore;
using G4.Motion;
using LogSv;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using YHSDK;

namespace ModuleData.Views
{
    [Serializable]
    public partial class uc_GlueExecuteDrain : ModuleBase
    {
        public uc_GlueExecuteDrain()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 点胶对象
        /// </summary>

        private JTimer JM_GlueModule = new JTimer();
        private int GlueModuleTaskRtn = 0;
        private Task GlueModuleTask = null;
        private bool bResult = false;

        #region 公有属性
        public DropGlueModel DataModel
        {
            get { return model; }
            set { model = value; OnPropertyChanged(); }
        }
        private DropGlueModel model = new DropGlueModel();


        /// <summary>
        /// 自动对针的结果
        /// </summary>
        [Browsable(false)]
        public bool AutoNeedelResult
        {
            get { return gutoNeedelResult; }
            private set { gutoNeedelResult = value; OnPropertyChanged(); }
        }
        bool gutoNeedelResult = false;

        /// <summary>
        /// 对针中心的X
        /// </summary>
        [Browsable(false)]
        public double ExecuteAlignNeedle_X
        {
            get { return executeAlignNeedle_X; }
            private set { executeAlignNeedle_X = value; OnPropertyChanged(); }
        }
        double executeAlignNeedle_X = 0;
        /// <summary>
        /// 对针中心的Y
        /// </summary>
        [Browsable(false)]
        public double ExecuteAlignNeedle_Y
        {
            get { return executeAlignNeedle_Y; }
            private set { executeAlignNeedle_Y = value; OnPropertyChanged(); }
        }
        double executeAlignNeedle_Y = 0;


        #endregion 公有属性
        #region 公有函数
        protected override void Run()
        {
            if (null == model) return;
            if (fc_GlueAuto1.WorkFlow == null)
                fc_GlueAuto1.TaskReset();
            fc_GlueAuto1.TaskRun();
            //if (!IsWaitForSignal)
            //{
            //    model.ParentProject.PLC_StandbyPosition = false;
            //}
        }

        public override void ResetChart()
        {
            StartChart = fc_GlueAuto1;
            fc_GlueAuto1.TaskReset();
        }
        private int MoveTo(double X, double Y, double Z, bool IsSafety = true)
        {
            double safetyPos = 0;
            if (IsSafety)
            {
                var rtn = project.ExcuteGlueModule.ExecuteZMoveTo(safetyPos, 20, 20);
                if (rtn == 0)
                {
                    var rtnXY = project.ExcuteGlueModule.ExecuteXYMoveTo(X, Y, 20, 20);
                    if (0 == rtnXY)
                    {
                        return project.ExcuteGlueModule.ExecuteZMoveTo(Z, 20, 20);
                    }
                    else
                        return -1;
                }
                else
                    return -1;
            }
            else
            {
                var rtn = project.ExcuteGlueModule.ExecuteXYMoveTo(X, Y, 20, 20);
                if (0 == rtn)
                {
                    return project.ExcuteGlueModule.ExecuteZMoveTo(Z, 20, 20);
                }
                else
                    return -1;

            }
        }

        #endregion 公有函数

        private YHSDK.FCResultType fc_GlueAuto1_FlowRun(object sender, EventArgs e)
        {
            project = model.ParentProject as GlueProject; ;
            if (project == null) return FCResultType.IDLE;
            //project.PLC_StandbyPosition = true;
            bool bret = model.PLC_Sig_GuleOut;
            Thread.Sleep(100);
            if (bret)
            {
                model.PLC_Sig_GuleOut = bret;
                model.SigGuleOutOK = false;
                model.SigGuleOutNG = false;
                model.SoftSigGuleOutFin = false;
                model.SoftSigGuleOut = true;

                PrintLog(string.Format($"接收到排胶指令，开始排胶任务"), Color.Red);

                model.ParentProject.UpProcessAddre = model.ParentProject.ProcessAddre;

                return FCResultType.NEXT;
            }
            return FCResultType.IDLE;
        }

        private FCResultType fc_Glueflow2_FlowRun(object sender, EventArgs e)
        {
            var rtn = project.ExcuteGlueModule.ExecuteDrain((int)model.PLC_IPC_GuleOutTime1);
            if (rtn == 0)
            {
                PrintLog(string.Format($"排胶完成"), Color.Black);
                bResult = true;
            }
            else
            {
                bResult = false;
                PrintLog(string.Format($"排胶失败"), Color.Red);
            }
            return FCResultType.NEXT;
        }

        private FCResultType fc_Glueflow3_FlowRun(object sender, EventArgs e)
        {
            var rtn = MoveTo(model.ParentProject.GlueSafetyPos.XPos, model.ParentProject.GlueSafetyPos.YPos, model.ParentProject.GlueSafetyPos.ZPos, true);
            if (rtn == 0)
            {
                return FCResultType.NEXT;
            }
            else
            {
                PrintLog("点胶模组运动到安全位失败", Color.Red);
                return FCResultType.IDLE;
            }



        }

        private FCResultType npFlowChart4_FlowRun(object sender, EventArgs e)
        {
            if (bResult)
            {
                model.SigGuleOutOK = true;
                model.SigGuleOutNG = false;
                Thread.Sleep(200);
                if (model.SigGuleOutOK == true && model.SigGuleOutNG == false)
                {
                    model.SoftSigGuleOutFin = true;
                    Thread.Sleep(200);
                    if (model.SoftSigGuleOutFin == true)
                    {
                        Thread.Sleep(1000);
                        model.ParentProject.UpProcessAddre = 0;
                        Log.log.Write("排胶OK完成", Color.Black);
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        return FCResultType.IDLE;
                    }
                }
                else
                {
                    return FCResultType.IDLE;
                }
            }
            else
            {
                model.SigGuleOutOK = false;
                model.SigGuleOutNG = true;
                Thread.Sleep(200);
                if (model.SigGuleOutOK == false && model.SigGuleOutNG == true)
                {
                    model.SoftSigGuleOutFin = true;
                    Thread.Sleep(200);
                    if (model.SoftSigGuleOutFin == true)
                    {
                        Thread.Sleep(1000);
                        model.ParentProject.UpProcessAddre = 0;
                        Log.log.Write("排胶NG完成", Color.Red);
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        return FCResultType.IDLE;
                    }
                }
                else
                {
                    return FCResultType.IDLE;
                }
            }
        }

        private FCResultType npFlowChart1_FlowRun(object sender, EventArgs e)
        {

            if (project == null) return FCResultType.IDLE;
            if (!project.IsContainUV || project.UVLightUp())
            {
                return FCResultType.NEXT;
            }
            else
            {
                return FCResultType.IDLE;
            }
        }
        private GlueProject project = null;
    }
}
