﻿using G4.GlueCore;
using G4.Motion;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using YHSDK;

namespace ModuleData.Views
{
    public partial class uc_AutoCabil : ModuleBase
    {
        public struct GulePoint
        {
            public double X;
            public double Y;
            public double Z;
        }
        public uc_AutoCabil()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 点胶对象
        /// </summary>
        private GlueModule glueModule;
        private JTimer JM_weightometer = new JTimer();
        private int GlueModuleTaskRtn = 0;
        private Task GlueModuleTask = null;
        private double oldWeightometerValue1 = 0;
        private double newWeightometerValue1 = 0;
        private bool DispWeightometerValue1 = false;
        bool bResult = false;
        //标定参数
        Queue<GulePoint> CalibPoints = new Queue<GulePoint>();
        Queue<string> CalibOrder = new Queue<string>();
        bool CalibResult = true;
        bool CalibOver = true;
        GulePoint point;//当前移动的坐标值
        private JTimer JM_GlueModule = new JTimer();
        #region 公有属性
        public GlueModel DataModel
        {
            get { return model; }
            set { model = value; OnPropertyChanged(); }
        }
        private GlueModel model = new GlueModel();

        /// <summary>
        /// 自动对针的结果
        /// </summary>
        [Browsable(false)]
        public bool AutoNeedelResult
        {
            get { return gutoNeedelResult; }
            private set { gutoNeedelResult = value; OnPropertyChanged(); }
        }
        bool gutoNeedelResult = false;

        /// <summary>
        /// 对针中心的X
        /// </summary>
        [Browsable(false)]
        public double ExecuteAlignNeedle_X
        {
            get { return executeAlignNeedle_X; }
            private set { executeAlignNeedle_X = value; OnPropertyChanged(); }
        }
        double executeAlignNeedle_X = 0;
        /// <summary>
        /// 对针中心的Y
        /// </summary>
        [Browsable(false)]
        public double ExecuteAlignNeedle_Y
        {
            get { return executeAlignNeedle_Y; }
            private set { executeAlignNeedle_Y = value; OnPropertyChanged(); }
        }
        double executeAlignNeedle_Y = 0;
        /// <summary>
        /// 对针中心的Z
        /// </summary>
        [Browsable(false)]
        public double ExecuteAlignNeedle_Z
        {
            get { return executeAlignNeedle_Z; }
            private set { executeAlignNeedle_Z = value; OnPropertyChanged(); }
        }
        double executeAlignNeedle_Z = 0;
        #endregion 公有属性
        #region 公有函数
        protected override void Run()
        {
            return;
            if (null == model) return;
            if (fc_AutoCalib1.WorkFlow == null)
                fc_AutoCalib1.TaskReset();
            fc_AutoCalib1.TaskRun();
        }
        public override void ResetChart()
        {
            fc_AutoCalib1.TaskReset();
        }
        private int MoveTo(double X, double Y, double Z, bool IsSafety = true)
        {
            double safetyPos = 0;
            if (IsSafety)
            {
                var rtn = glueModule.ExecuteZMoveTo(safetyPos, 20, 20);
                if (rtn == 0)
                {
                    var rtnXY = glueModule.ExecuteXYMoveTo(X, Y, 20, 20);
                    if (0 == rtnXY)
                    {
                        return glueModule.ExecuteZMoveTo(Z, 20, 20);
                    }
                    else
                        return -1;
                }
                else
                    return -1;
            }
            else
            {
                var rtn = glueModule.ExecuteXYMoveTo(X, Y, 20, 20);
                if (0 == rtn)
                {
                    return glueModule.ExecuteZMoveTo(Z, 20, 20);
                }
                else
                    return -1;

            }
        }

        #endregion 公有函数

        private YHSDK.FCResultType fc_AutoCalib1_FlowRun(object sender, EventArgs e)
        {
            //model.ParentProject.PLC_StandbyPosition = true;
            //model.ParentProject.PLC_StandbyPosition = false;
            return YHSDK.FCResultType.NEXT;
        }

        private YHSDK.FCResultType npFlowChart1_FlowRun(object sender, EventArgs e)
        {
            project = model.ParentProject as GlueProject;
            if (project == null) return FCResultType.IDLE;     
            if (!project.IsContainUV||project.UVLightUp())
            {
                return FCResultType.NEXT;
            }
            else
            {
                return FCResultType.IDLE;
            }
        }

        private YHSDK.FCResultType fc_AutoCalib2_FlowRun(object sender, EventArgs e)
        {
            point = CalibPoints.Dequeue();
            var rtn = MoveTo(point.X, point.Y, point.Z, true);
            if (0 == rtn)
            {
                return FCResultType.NEXT;
            }
            else
            {
                PrintLog("移动到拍照点失败", Color.Red);
            }
            return FCResultType.NEXT;
        }

        private FCResultType fc_AutoCalib3_FlowRun(object sender, EventArgs e)
        {

            return YHSDK.FCResultType.NEXT;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (CalibOver)
            {
                //自动标定的点位中心
                double centreX = Convert.ToDouble(txt_X.Text);
                double centreY = Convert.ToDouble(txt_Y.Text);
                double centreZ = Convert.ToDouble(txt_Z.Text);
                double OffsetXYZ = Convert.ToDouble(txt_offsetXYZ.Text);
                CalibPoints.Clear();
                GulePoint G_Point = new GulePoint();
                G_Point.X = centreX;
                G_Point.Y = centreY;
                G_Point.Z = centreZ;
                CalibPoints.Enqueue(G_Point);
                CalibPoints.Enqueue(G_Point);
                for (int i = 0; i < 9; i++)
                {
                    double pointX = 0;
                    double pointY = 0;
                    switch (i)
                    {
                        case 0:
                            pointX= centreX;
                            pointY = centreY;
                            break;
                        case 1:
                            pointX = centreX- OffsetXYZ;
                            pointY = centreY;
                            break;
                        case 2:
                        case 3:
                        case 4:
                            pointX = centreX - OffsetXYZ*(i-3);
                            pointY = centreY- OffsetXYZ;
                            break;
                        case 5:
                        case 6:
                            pointX = centreX - OffsetXYZ;
                            pointY = centreY + OffsetXYZ*(i-5);
                            break;
                        case 7:
                        case 8:
                            pointX = centreX + OffsetXYZ*(i-7);
                            pointY = centreY + OffsetXYZ ;
                            break;
                            
                    }

                    G_Point.X = pointX;
                    G_Point.Y = pointY;
                    G_Point.Z = centreX;
                    CalibPoints.Enqueue(G_Point);
                }
                G_Point.X = 0;
                G_Point.Y = 0;
                G_Point.Z = -14;
                CalibPoints.Enqueue(G_Point);
                CalibPoints.Enqueue(G_Point);


                CalibOver = false;
                fc_AutoCalib1.TaskReset();
                Task.Run(() =>
                {
                    while (!CalibOver)
                    {
                        fc_AutoCalib1.TaskRun();
                        Thread.Sleep(5);
                    }
                });
            }
        }

        private FCResultType fc_AutoCalib4_FlowRun(object sender, EventArgs e)
        {
            if (null != DCTcpClient && DCTcpClient.IsConnected)
            {
                string dcckMsg = "";
                switch (CalibPoints.Count)
                {
                    case 12:
                        dcckMsg = "Calib,5,SC,0,0,0,0,0";
                        break;
                    case 11:
                        dcckMsg = string.Format("Calib,5,HBF,1,0,{0}，{1}，{2}", point.X.ToString(), point.Y.ToString(), point.Z.ToString());
                        break;
                    case 1:
                        dcckMsg = string.Format("Calib,5,HNME,1,0,0，0，0");
                        break;
                    case 0:
                        dcckMsg = string.Format("Calib,5,EC,0,0,0，0，0");
                        break;
                    default:
                        dcckMsg = string.Format("Calib,5,HNM,1,{4},{0}，{1}，{2}", point.X.ToString(), point.Y.ToString(), point.Z.ToString(), (9 - CalibPoints.Count).ToString());
                        break;
                }
                DCTcpClient.Send(dcckMsg);
                return FCResultType.NEXT;
            }
            else
            {
                PrintLog($"DCCK TCP连接断开", System.Drawing.Color.Red);
                return FCResultType.NEXT;
            }
        }
        private FCResultType fc_AutoCalib5_FlowRun(object sender, EventArgs e)
        {
            string mLocationBuffer = AlgLastContent;
            if (string.IsNullOrEmpty(mLocationBuffer))
            {
                if (JM_GlueModule.On(3000))
                {
                    PrintLog($"接收DCCK数据超时", System.Drawing.Color.Red);
                }
                return FCResultType.IDLE;
            }
            JM_GlueModule.Restart();
            if (mLocationBuffer == "OK\r\n")
            {
                return FCResultType.NEXT;
            }
            else
            {
                CalibResult = false;
                return FCResultType.NEXT;
            }
        }
        private FCResultType fc_AutoCalib6_FlowRun(object sender, EventArgs e)
        {
            if (CalibPoints.Count < 1 || CalibResult == false)
            {
                return FCResultType.NEXT;
            }
            return FCResultType.CASE1;
        }

        private FCResultType fc_AutoCalib7_FlowRun(object sender, EventArgs e)
        {
            if (CalibResult == false)
            {
                PrintLog("自动标定失败", System.Drawing.Color.Red);
            }
            else
            {
                PrintLog("自动标定成功", System.Drawing.Color.Black);
            }
            CalibOver = true;
            return FCResultType.IDLE;
        }

        private GlueProject project=null;
    }
}

