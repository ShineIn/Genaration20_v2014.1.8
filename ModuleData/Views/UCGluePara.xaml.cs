﻿using G4.GlueCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;

namespace ModuleData
{
    /// <summary>
    /// Interaction logic for UCGluePara.xaml
    /// </summary>
    public partial class UCGluePara : UserControl
    {
        public UCGluePara()
        {
            InitializeComponent();
        }

        public static DependencyProperty GlueModuleProperty = DependencyProperty.Register("GlueModule", typeof(GlueModuleParams), typeof(UCGluePara), new FrameworkPropertyMetadata(new GlueModuleParams(),FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public GlueModuleParams GlueModule
        {
            get { return (GlueModuleParams)GetValue(GlueModuleProperty); }
            set { SetValue(GlueModuleProperty, value); }
        }

        private void SelectFile_Click(object sender, RoutedEventArgs e)
        {
            if (null == GlueModule) return;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = false;
            openFileDialog.Filter = "(*.xml)|*.xml";  
            if (true == openFileDialog.ShowDialog())
            {
                GlueModule.GluePathFileName = openFileDialog.FileName;
                InvalidateVisual();
            }
        }

        private void OpenFile_Click(object sender, RoutedEventArgs e)
        {
            if (File.Exists(GlueModule.GluePathFileName))
            {
                System.Diagnostics.ProcessStartInfo Info = new System.Diagnostics.ProcessStartInfo(GlueModule.GluePathFileName);
                System.Diagnostics.Process Pro = System.Diagnostics.Process.Start(Info);
            }
            else
            {
                System.Windows.Forms.MessageBox.Show($"打开此路径{GlueModule.GluePathFileName}不存在");
            }
        }
    }
}
