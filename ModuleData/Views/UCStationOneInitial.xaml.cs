﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ModuleData.Views
{
    /// <summary>
    /// Interaction logic for UCStationOneInitial.xaml
    /// </summary>
    public partial class UCStationOneInitial : UserControl, IModule
    {
        public UCStationOneInitial()
        {
            InitializeComponent();
        }

        public static DependencyProperty SubModuleProperty = DependencyProperty.Register("SubModule", typeof(InitialMode), typeof(UCStationOneInitial), new FrameworkPropertyMetadata(new InitialMode(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));

        public InitialMode SubModule
        {
            get { return (InitialMode)GetValue(SubModuleProperty); }
            set { SetValue(SubModuleProperty, value); }
        }

        public bool IsRun => initial.IsRun;

        public void ResetChart()
        {
            initial.ResetChart();
        }

        public void RunAsync()
        {
            initial.RunAsync();
        }

        public void Stop()
        {
            initial.Stop();
        }

        private static void DepenProChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            UCStationOneInitial flow = d as UCStationOneInitial;
            if (null == flow) return;
            if (e.Property == SubModuleProperty)
            {
                InitialMode model = e.NewValue as InitialMode;
                flow.initial.DataModel = model;
            }      
        }

    }
}
