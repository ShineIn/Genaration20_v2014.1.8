﻿namespace ModuleData.Views
{
    partial class uc_AutoCabil
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.fc_AutoCalib7 = new YHSDK.NPFlowChart();
            this.fc_AutoCalib6 = new YHSDK.NPFlowChart();
            this.fc_AutoCalib2 = new YHSDK.NPFlowChart();
            this.fc_AutoCalib3 = new YHSDK.NPFlowChart();
            this.fc_AutoCalib4 = new YHSDK.NPFlowChart();
            this.fc_AutoCalib5 = new YHSDK.NPFlowChart();
            this.fc_AutoCalib1 = new YHSDK.NPFlowChart();
            this.npFlowChart1 = new YHSDK.NPFlowChart();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_X = new System.Windows.Forms.TextBox();
            this.txt_Y = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_Z = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_offsetXYZ = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // fc_AutoCalib7
            // 
            this.fc_AutoCalib7.AlarmCode = "";
            this.fc_AutoCalib7.ArrowsWidth = 1;
            this.fc_AutoCalib7.BackColor = System.Drawing.Color.White;
            this.fc_AutoCalib7.bClearTrace = false;
            this.fc_AutoCalib7.bIsLastFlowChart = false;
            this.fc_AutoCalib7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_AutoCalib7.bSkip = false;
            this.fc_AutoCalib7.bStepByStepMode = false;
            this.fc_AutoCalib7.CASE1 = null;
            this.fc_AutoCalib7.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib7.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_AutoCalib7.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib7.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib7.CASE2 = null;
            this.fc_AutoCalib7.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib7.CASE2Color = System.Drawing.Color.Red;
            this.fc_AutoCalib7.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib7.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib7.CASE3 = null;
            this.fc_AutoCalib7.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib7.CASE3Color = System.Drawing.Color.Blue;
            this.fc_AutoCalib7.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib7.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib7.DefaultColor = System.Drawing.Color.White;
            this.fc_AutoCalib7.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_AutoCalib7.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_AutoCalib7.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_AutoCalib7.Location = new System.Drawing.Point(8, 318);
            this.fc_AutoCalib7.Margin = new System.Windows.Forms.Padding(8);
            this.fc_AutoCalib7.menuOpening = false;
            this.fc_AutoCalib7.Name = "fc_AutoCalib7";
            this.fc_AutoCalib7.NEXT = null;
            this.fc_AutoCalib7.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib7.NEXTColor = System.Drawing.Color.Black;
            this.fc_AutoCalib7.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib7.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib7.Size = new System.Drawing.Size(185, 27);
            this.fc_AutoCalib7.SkipColor = System.Drawing.Color.Yellow;
            this.fc_AutoCalib7.SubFlowChart = null;
            this.fc_AutoCalib7.TabIndex = 53;
            this.fc_AutoCalib7.Text = "结束";
            this.fc_AutoCalib7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_AutoCalib7.TimeOut = 10000;
            this.fc_AutoCalib7.TimeoutColor = System.Drawing.Color.Red;
            this.fc_AutoCalib7.tmrTimeOut = null;
            this.fc_AutoCalib7.FlowRun += new YHSDK.FlowRunEvent(this.fc_AutoCalib7_FlowRun);
            // 
            // fc_AutoCalib6
            // 
            this.fc_AutoCalib6.AlarmCode = "";
            this.fc_AutoCalib6.ArrowsWidth = 1;
            this.fc_AutoCalib6.BackColor = System.Drawing.Color.White;
            this.fc_AutoCalib6.bClearTrace = false;
            this.fc_AutoCalib6.bIsLastFlowChart = false;
            this.fc_AutoCalib6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_AutoCalib6.bSkip = false;
            this.fc_AutoCalib6.bStepByStepMode = false;
            this.fc_AutoCalib6.CASE1 = this.fc_AutoCalib2;
            this.fc_AutoCalib6.Case1ArrowType = YHSDK.ArrowTypes.Square;
            this.fc_AutoCalib6.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_AutoCalib6.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib6.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib6.CASE2 = null;
            this.fc_AutoCalib6.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib6.CASE2Color = System.Drawing.Color.Red;
            this.fc_AutoCalib6.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib6.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib6.CASE3 = null;
            this.fc_AutoCalib6.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib6.CASE3Color = System.Drawing.Color.Blue;
            this.fc_AutoCalib6.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib6.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib6.DefaultColor = System.Drawing.Color.White;
            this.fc_AutoCalib6.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_AutoCalib6.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_AutoCalib6.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_AutoCalib6.Location = new System.Drawing.Point(8, 274);
            this.fc_AutoCalib6.Margin = new System.Windows.Forms.Padding(8);
            this.fc_AutoCalib6.menuOpening = false;
            this.fc_AutoCalib6.Name = "fc_AutoCalib6";
            this.fc_AutoCalib6.NEXT = this.fc_AutoCalib7;
            this.fc_AutoCalib6.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib6.NEXTColor = System.Drawing.Color.Black;
            this.fc_AutoCalib6.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib6.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib6.Size = new System.Drawing.Size(185, 27);
            this.fc_AutoCalib6.SkipColor = System.Drawing.Color.Yellow;
            this.fc_AutoCalib6.SubFlowChart = null;
            this.fc_AutoCalib6.TabIndex = 52;
            this.fc_AutoCalib6.Text = "判断标定次数";
            this.fc_AutoCalib6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_AutoCalib6.TimeOut = 10000;
            this.fc_AutoCalib6.TimeoutColor = System.Drawing.Color.Red;
            this.fc_AutoCalib6.tmrTimeOut = null;
            this.fc_AutoCalib6.FlowRun += new YHSDK.FlowRunEvent(this.fc_AutoCalib6_FlowRun);
            // 
            // fc_AutoCalib2
            // 
            this.fc_AutoCalib2.AlarmCode = "";
            this.fc_AutoCalib2.ArrowsWidth = 1;
            this.fc_AutoCalib2.BackColor = System.Drawing.Color.White;
            this.fc_AutoCalib2.bClearTrace = false;
            this.fc_AutoCalib2.bIsLastFlowChart = false;
            this.fc_AutoCalib2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_AutoCalib2.bSkip = false;
            this.fc_AutoCalib2.bStepByStepMode = false;
            this.fc_AutoCalib2.CASE1 = null;
            this.fc_AutoCalib2.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib2.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_AutoCalib2.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib2.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib2.CASE2 = null;
            this.fc_AutoCalib2.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib2.CASE2Color = System.Drawing.Color.Red;
            this.fc_AutoCalib2.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib2.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib2.CASE3 = null;
            this.fc_AutoCalib2.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib2.CASE3Color = System.Drawing.Color.Blue;
            this.fc_AutoCalib2.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib2.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib2.DefaultColor = System.Drawing.Color.White;
            this.fc_AutoCalib2.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_AutoCalib2.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_AutoCalib2.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_AutoCalib2.Location = new System.Drawing.Point(8, 97);
            this.fc_AutoCalib2.Margin = new System.Windows.Forms.Padding(8);
            this.fc_AutoCalib2.menuOpening = false;
            this.fc_AutoCalib2.Name = "fc_AutoCalib2";
            this.fc_AutoCalib2.NEXT = this.fc_AutoCalib3;
            this.fc_AutoCalib2.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib2.NEXTColor = System.Drawing.Color.Black;
            this.fc_AutoCalib2.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib2.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib2.Size = new System.Drawing.Size(185, 27);
            this.fc_AutoCalib2.SkipColor = System.Drawing.Color.Yellow;
            this.fc_AutoCalib2.SubFlowChart = null;
            this.fc_AutoCalib2.TabIndex = 48;
            this.fc_AutoCalib2.Text = "移动到拍照位";
            this.fc_AutoCalib2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_AutoCalib2.TimeOut = 10000;
            this.fc_AutoCalib2.TimeoutColor = System.Drawing.Color.Red;
            this.fc_AutoCalib2.tmrTimeOut = null;
            this.fc_AutoCalib2.FlowRun += new YHSDK.FlowRunEvent(this.fc_AutoCalib2_FlowRun);
            // 
            // fc_AutoCalib3
            // 
            this.fc_AutoCalib3.AlarmCode = "";
            this.fc_AutoCalib3.ArrowsWidth = 1;
            this.fc_AutoCalib3.BackColor = System.Drawing.Color.White;
            this.fc_AutoCalib3.bClearTrace = false;
            this.fc_AutoCalib3.bIsLastFlowChart = false;
            this.fc_AutoCalib3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_AutoCalib3.bSkip = false;
            this.fc_AutoCalib3.bStepByStepMode = false;
            this.fc_AutoCalib3.CASE1 = null;
            this.fc_AutoCalib3.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib3.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_AutoCalib3.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib3.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib3.CASE2 = null;
            this.fc_AutoCalib3.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib3.CASE2Color = System.Drawing.Color.Red;
            this.fc_AutoCalib3.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib3.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib3.CASE3 = null;
            this.fc_AutoCalib3.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib3.CASE3Color = System.Drawing.Color.Blue;
            this.fc_AutoCalib3.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib3.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib3.DefaultColor = System.Drawing.Color.White;
            this.fc_AutoCalib3.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_AutoCalib3.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_AutoCalib3.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_AutoCalib3.Location = new System.Drawing.Point(8, 141);
            this.fc_AutoCalib3.Margin = new System.Windows.Forms.Padding(8);
            this.fc_AutoCalib3.menuOpening = false;
            this.fc_AutoCalib3.Name = "fc_AutoCalib3";
            this.fc_AutoCalib3.NEXT = this.fc_AutoCalib4;
            this.fc_AutoCalib3.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib3.NEXTColor = System.Drawing.Color.Black;
            this.fc_AutoCalib3.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib3.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib3.Size = new System.Drawing.Size(185, 27);
            this.fc_AutoCalib3.SkipColor = System.Drawing.Color.Yellow;
            this.fc_AutoCalib3.SubFlowChart = null;
            this.fc_AutoCalib3.TabIndex = 49;
            this.fc_AutoCalib3.Text = "等待轴到达";
            this.fc_AutoCalib3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_AutoCalib3.TimeOut = 10000;
            this.fc_AutoCalib3.TimeoutColor = System.Drawing.Color.Red;
            this.fc_AutoCalib3.tmrTimeOut = null;
            this.fc_AutoCalib3.FlowRun += new YHSDK.FlowRunEvent(this.fc_AutoCalib3_FlowRun);
            // 
            // fc_AutoCalib4
            // 
            this.fc_AutoCalib4.AlarmCode = "";
            this.fc_AutoCalib4.ArrowsWidth = 1;
            this.fc_AutoCalib4.BackColor = System.Drawing.Color.White;
            this.fc_AutoCalib4.bClearTrace = false;
            this.fc_AutoCalib4.bIsLastFlowChart = false;
            this.fc_AutoCalib4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_AutoCalib4.bSkip = false;
            this.fc_AutoCalib4.bStepByStepMode = false;
            this.fc_AutoCalib4.CASE1 = null;
            this.fc_AutoCalib4.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib4.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_AutoCalib4.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib4.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib4.CASE2 = null;
            this.fc_AutoCalib4.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib4.CASE2Color = System.Drawing.Color.Red;
            this.fc_AutoCalib4.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib4.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib4.CASE3 = null;
            this.fc_AutoCalib4.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib4.CASE3Color = System.Drawing.Color.Blue;
            this.fc_AutoCalib4.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib4.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib4.DefaultColor = System.Drawing.Color.White;
            this.fc_AutoCalib4.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_AutoCalib4.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_AutoCalib4.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_AutoCalib4.Location = new System.Drawing.Point(8, 186);
            this.fc_AutoCalib4.Margin = new System.Windows.Forms.Padding(8);
            this.fc_AutoCalib4.menuOpening = false;
            this.fc_AutoCalib4.Name = "fc_AutoCalib4";
            this.fc_AutoCalib4.NEXT = this.fc_AutoCalib5;
            this.fc_AutoCalib4.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib4.NEXTColor = System.Drawing.Color.Black;
            this.fc_AutoCalib4.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib4.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib4.Size = new System.Drawing.Size(185, 27);
            this.fc_AutoCalib4.SkipColor = System.Drawing.Color.Yellow;
            this.fc_AutoCalib4.SubFlowChart = null;
            this.fc_AutoCalib4.TabIndex = 50;
            this.fc_AutoCalib4.Text = "发送拍照指令";
            this.fc_AutoCalib4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_AutoCalib4.TimeOut = 10000;
            this.fc_AutoCalib4.TimeoutColor = System.Drawing.Color.Red;
            this.fc_AutoCalib4.tmrTimeOut = null;
            this.fc_AutoCalib4.FlowRun += new YHSDK.FlowRunEvent(this.fc_AutoCalib4_FlowRun);
            // 
            // fc_AutoCalib5
            // 
            this.fc_AutoCalib5.AlarmCode = "";
            this.fc_AutoCalib5.ArrowsWidth = 1;
            this.fc_AutoCalib5.BackColor = System.Drawing.Color.White;
            this.fc_AutoCalib5.bClearTrace = false;
            this.fc_AutoCalib5.bIsLastFlowChart = false;
            this.fc_AutoCalib5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_AutoCalib5.bSkip = false;
            this.fc_AutoCalib5.bStepByStepMode = false;
            this.fc_AutoCalib5.CASE1 = null;
            this.fc_AutoCalib5.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib5.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_AutoCalib5.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib5.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib5.CASE2 = null;
            this.fc_AutoCalib5.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib5.CASE2Color = System.Drawing.Color.Red;
            this.fc_AutoCalib5.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib5.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib5.CASE3 = null;
            this.fc_AutoCalib5.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib5.CASE3Color = System.Drawing.Color.Blue;
            this.fc_AutoCalib5.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib5.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib5.DefaultColor = System.Drawing.Color.White;
            this.fc_AutoCalib5.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_AutoCalib5.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_AutoCalib5.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_AutoCalib5.Location = new System.Drawing.Point(8, 230);
            this.fc_AutoCalib5.Margin = new System.Windows.Forms.Padding(8);
            this.fc_AutoCalib5.menuOpening = false;
            this.fc_AutoCalib5.Name = "fc_AutoCalib5";
            this.fc_AutoCalib5.NEXT = this.fc_AutoCalib6;
            this.fc_AutoCalib5.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib5.NEXTColor = System.Drawing.Color.Black;
            this.fc_AutoCalib5.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib5.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib5.Size = new System.Drawing.Size(185, 27);
            this.fc_AutoCalib5.SkipColor = System.Drawing.Color.Yellow;
            this.fc_AutoCalib5.SubFlowChart = null;
            this.fc_AutoCalib5.TabIndex = 51;
            this.fc_AutoCalib5.Text = "等待结果反馈";
            this.fc_AutoCalib5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_AutoCalib5.TimeOut = 10000;
            this.fc_AutoCalib5.TimeoutColor = System.Drawing.Color.Red;
            this.fc_AutoCalib5.tmrTimeOut = null;
            this.fc_AutoCalib5.FlowRun += new YHSDK.FlowRunEvent(this.fc_AutoCalib5_FlowRun);
            // 
            // fc_AutoCalib1
            // 
            this.fc_AutoCalib1.AlarmCode = "";
            this.fc_AutoCalib1.ArrowsWidth = 1;
            this.fc_AutoCalib1.BackColor = System.Drawing.Color.White;
            this.fc_AutoCalib1.bClearTrace = false;
            this.fc_AutoCalib1.bIsLastFlowChart = false;
            this.fc_AutoCalib1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_AutoCalib1.bSkip = false;
            this.fc_AutoCalib1.bStepByStepMode = false;
            this.fc_AutoCalib1.CASE1 = null;
            this.fc_AutoCalib1.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib1.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_AutoCalib1.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib1.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib1.CASE2 = null;
            this.fc_AutoCalib1.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib1.CASE2Color = System.Drawing.Color.Red;
            this.fc_AutoCalib1.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib1.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib1.CASE3 = null;
            this.fc_AutoCalib1.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib1.CASE3Color = System.Drawing.Color.Blue;
            this.fc_AutoCalib1.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib1.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib1.DefaultColor = System.Drawing.Color.White;
            this.fc_AutoCalib1.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_AutoCalib1.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_AutoCalib1.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_AutoCalib1.Location = new System.Drawing.Point(8, 8);
            this.fc_AutoCalib1.Margin = new System.Windows.Forms.Padding(8);
            this.fc_AutoCalib1.menuOpening = false;
            this.fc_AutoCalib1.Name = "fc_AutoCalib1";
            this.fc_AutoCalib1.NEXT = this.npFlowChart1;
            this.fc_AutoCalib1.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_AutoCalib1.NEXTColor = System.Drawing.Color.Black;
            this.fc_AutoCalib1.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib1.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_AutoCalib1.Size = new System.Drawing.Size(185, 27);
            this.fc_AutoCalib1.SkipColor = System.Drawing.Color.Yellow;
            this.fc_AutoCalib1.SubFlowChart = null;
            this.fc_AutoCalib1.TabIndex = 47;
            this.fc_AutoCalib1.Text = "标定开始";
            this.fc_AutoCalib1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_AutoCalib1.TimeOut = 10000;
            this.fc_AutoCalib1.TimeoutColor = System.Drawing.Color.Red;
            this.fc_AutoCalib1.tmrTimeOut = null;
            this.fc_AutoCalib1.FlowRun += new YHSDK.FlowRunEvent(this.fc_AutoCalib1_FlowRun);
            // 
            // npFlowChart1
            // 
            this.npFlowChart1.AlarmCode = "";
            this.npFlowChart1.ArrowsWidth = 1;
            this.npFlowChart1.BackColor = System.Drawing.Color.White;
            this.npFlowChart1.bClearTrace = false;
            this.npFlowChart1.bIsLastFlowChart = false;
            this.npFlowChart1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.npFlowChart1.bSkip = false;
            this.npFlowChart1.bStepByStepMode = false;
            this.npFlowChart1.CASE1 = null;
            this.npFlowChart1.Case1ArrowType = YHSDK.ArrowTypes.Square;
            this.npFlowChart1.CASE1Color = System.Drawing.Color.LimeGreen;
            this.npFlowChart1.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.npFlowChart1.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.npFlowChart1.CASE2 = null;
            this.npFlowChart1.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart1.CASE2Color = System.Drawing.Color.Red;
            this.npFlowChart1.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.CASE3 = null;
            this.npFlowChart1.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart1.CASE3Color = System.Drawing.Color.Blue;
            this.npFlowChart1.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.DefaultColor = System.Drawing.Color.White;
            this.npFlowChart1.eSkipPath = YHSDK.FCResultType.NEXT;
            this.npFlowChart1.ExecutedColor = System.Drawing.Color.LightGray;
            this.npFlowChart1.ExecutingColor = System.Drawing.Color.Lime;
            this.npFlowChart1.Location = new System.Drawing.Point(8, 53);
            this.npFlowChart1.Margin = new System.Windows.Forms.Padding(8);
            this.npFlowChart1.menuOpening = false;
            this.npFlowChart1.Name = "npFlowChart1";
            this.npFlowChart1.NEXT = this.fc_AutoCalib2;
            this.npFlowChart1.NextArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart1.NEXTColor = System.Drawing.Color.Black;
            this.npFlowChart1.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.Size = new System.Drawing.Size(185, 27);
            this.npFlowChart1.SkipColor = System.Drawing.Color.Yellow;
            this.npFlowChart1.SubFlowChart = null;
            this.npFlowChart1.TabIndex = 58;
            this.npFlowChart1.Text = "UV气缸抬起";
            this.npFlowChart1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.npFlowChart1.TimeOut = 10000;
            this.npFlowChart1.TimeoutColor = System.Drawing.Color.Red;
            this.npFlowChart1.tmrTimeOut = null;
            this.npFlowChart1.FlowRun += new YHSDK.FlowRunEvent(this.npFlowChart1_FlowRun);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(267, 303);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(167, 42);
            this.button1.TabIndex = 59;
            this.button1.Text = "开始标定";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(262, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 27);
            this.label1.TabIndex = 60;
            this.label1.Text = "中心X：";
            // 
            // txt_X
            // 
            this.txt_X.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_X.Location = new System.Drawing.Point(353, 33);
            this.txt_X.Name = "txt_X";
            this.txt_X.Size = new System.Drawing.Size(81, 34);
            this.txt_X.TabIndex = 61;
            // 
            // txt_Y
            // 
            this.txt_Y.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_Y.Location = new System.Drawing.Point(352, 78);
            this.txt_Y.Name = "txt_Y";
            this.txt_Y.Size = new System.Drawing.Size(81, 34);
            this.txt_Y.TabIndex = 63;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label2.Location = new System.Drawing.Point(262, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 27);
            this.label2.TabIndex = 62;
            this.label2.Text = "中心Y：";
            // 
            // txt_Z
            // 
            this.txt_Z.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_Z.Location = new System.Drawing.Point(352, 131);
            this.txt_Z.Name = "txt_Z";
            this.txt_Z.Size = new System.Drawing.Size(81, 34);
            this.txt_Z.TabIndex = 65;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label3.Location = new System.Drawing.Point(262, 134);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 27);
            this.label3.TabIndex = 64;
            this.label3.Text = "中心Z：";
            // 
            // txt_offsetXYZ
            // 
            this.txt_offsetXYZ.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_offsetXYZ.Location = new System.Drawing.Point(352, 186);
            this.txt_offsetXYZ.Name = "txt_offsetXYZ";
            this.txt_offsetXYZ.Size = new System.Drawing.Size(81, 34);
            this.txt_offsetXYZ.TabIndex = 67;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(262, 189);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(92, 27);
            this.label4.TabIndex = 66;
            this.label4.Text = "寸动值：";
            // 
            // uc_AutoCabil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txt_offsetXYZ);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_Z);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_Y);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_X);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.npFlowChart1);
            this.Controls.Add(this.fc_AutoCalib7);
            this.Controls.Add(this.fc_AutoCalib6);
            this.Controls.Add(this.fc_AutoCalib5);
            this.Controls.Add(this.fc_AutoCalib4);
            this.Controls.Add(this.fc_AutoCalib3);
            this.Controls.Add(this.fc_AutoCalib2);
            this.Controls.Add(this.fc_AutoCalib1);
            this.Name = "uc_AutoCabil";
            this.Size = new System.Drawing.Size(534, 423);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private YHSDK.NPFlowChart fc_AutoCalib7;
        private YHSDK.NPFlowChart fc_AutoCalib6;
        private YHSDK.NPFlowChart fc_AutoCalib2;
        private YHSDK.NPFlowChart fc_AutoCalib3;
        private YHSDK.NPFlowChart fc_AutoCalib4;
        private YHSDK.NPFlowChart fc_AutoCalib5;
        private YHSDK.NPFlowChart fc_AutoCalib1;
        private YHSDK.NPFlowChart npFlowChart1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_X;
        private System.Windows.Forms.TextBox txt_Y;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_Z;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_offsetXYZ;
        private System.Windows.Forms.Label label4;
    }
}
