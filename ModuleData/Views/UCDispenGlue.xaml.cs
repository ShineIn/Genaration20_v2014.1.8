﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using G4.Motion;
using Googo.Manul;
using Googo.Manul.Views;
using Infrastructure;
using Microsoft.Win32;

namespace ModuleData
{
    /// <summary>
    /// Interaction logic for UCDispenGlue.xaml
    /// </summary>
    public partial class UCDispenGlue : UserControl
    {
        public UCDispenGlue()
        {
            InitializeComponent();
        }

        public static DependencyProperty SubModuleProperty = DependencyProperty.Register("SubModule", typeof(GlueModel), typeof(UCDispenGlue), new FrameworkPropertyMetadata(new GlueModel(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty XAxisNumProperty = DependencyProperty.Register("XAxisNum", typeof(EnumAxisType), typeof(UCDispenGlue), new FrameworkPropertyMetadata(EnumAxisType.X, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty YAxisNumProperty = DependencyProperty.Register("YAxisNum", typeof(EnumAxisType), typeof(UCDispenGlue), new FrameworkPropertyMetadata(EnumAxisType.Y, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));
        public static DependencyProperty ZAxisNumProperty = DependencyProperty.Register("ZAxisNum", typeof(EnumAxisType), typeof(UCDispenGlue), new FrameworkPropertyMetadata(EnumAxisType.Z, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public GlueModel SubModule
        {
            get { return (GlueModel)GetValue(SubModuleProperty); }
            set { SetValue(SubModuleProperty, value); }
        }

        public EnumAxisType XAxisNum
        {
            get { return (EnumAxisType)GetValue(XAxisNumProperty); }
            set { SetValue(XAxisNumProperty, value); }
        }

        public EnumAxisType YAxisNum
        {
            get { return (EnumAxisType)GetValue(YAxisNumProperty); }
            set { SetValue(YAxisNumProperty, value); }
        }

        public EnumAxisType ZAxisNum
        {
            get { return (EnumAxisType)GetValue(ZAxisNumProperty); }
            set { SetValue(ZAxisNumProperty, value); }
        }
    }

    public class SlotEnableConv : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (null == value || value == DependencyProperty.UnsetValue || !(value is SlotType slot)) return false;
            if (slot == SlotType.Three) return true;
            else if (slot == SlotType.Single) return false;
            else if (slot == SlotType.Double)
            {
                if (parameter == null) return true;
                else return false;
            }
            return true;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
