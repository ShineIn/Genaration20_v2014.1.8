﻿using G4.Motion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ModuleData
{
    /// <summary>
    /// UCSlot.xaml 的交互逻辑
    /// </summary>
    public partial class UCSlot : UserControl
    {
        public UCSlot()
        {
            InitializeComponent(); 
        }

        public static DependencyProperty SubSlotProperty = DependencyProperty.Register("SubSlot", typeof(SlotSignalInfor), typeof(UCSlot), new FrameworkPropertyMetadata(new SlotSignalInfor(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public SlotSignalInfor SubSlot
        {
            get { return (SlotSignalInfor)GetValue(SubSlotProperty); }
            set { SetValue(SubSlotProperty, value); }
        }
    }
}
