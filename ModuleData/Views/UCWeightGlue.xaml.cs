﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Ports;
using Googo.Manul.Views;

namespace ModuleData
{
    /// <summary>
    /// UCDropGlue.xaml 的交互逻辑
    /// </summary>
    public partial class UCWeightGlue : UserControl
    {
        public UCWeightGlue()
        {
            InitializeComponent();
        }


        public static DependencyProperty SubModuleProperty =
            DependencyProperty.Register("SubModule", typeof(WeightModel), typeof(UCWeightGlue), new FrameworkPropertyMetadata(new WeightModel(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public WeightModel SubModule
        {
            get { return (WeightModel)GetValue(SubModuleProperty); }
            set { SetValue(SubModuleProperty, value); }
        }

        public IEnumerable<string> Ports
        { 
            get { return SerialPort.GetPortNames(); }
        }

        public IEnumerable<int> Rates
        {
            get { return new int[] { 4800,9600,19200,38400}; }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
          
        }
    }
}
