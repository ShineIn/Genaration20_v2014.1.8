﻿using G4.GlueCore;
using G4.Motion;
using Googo.Manul;
using LogSv;
using SuperSimpleTcp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Xml.Serialization;
using YHSDK;

namespace ModuleData.Views
{
    [Serializable]
    public partial class uc_GlueInitProcess : ModuleBase
    {
        public uc_GlueInitProcess()
        {
            InitializeComponent();
        }

        private int GlueModuleError = 0;
        /// <summary>
        /// 点胶对象
        /// </summary>
        private GlueModule glueModule;

        #region 公有属性

        public InitialMode DataModel
        {
            get { return model; }
            set { model = value; OnPropertyChanged(); }
        }
        private InitialMode model = new InitialMode();

        /// <summary>
        /// 是否初始化模块
        /// </summary>
        public override bool IsInitialModule { get { return true; } }

        #endregion 公有属性

        #region 公有函数
        protected override void Run()
        {
            if (null == model) return;
            if (fc_GlueAuto1.WorkFlow == null)
                fc_GlueAuto1.TaskReset();
            fc_GlueAuto1.TaskRun();
        }

        public void RunReset()
        {
            StartChart = fc_GlueAuto1;
            fc_GlueAuto1.TaskReset();
        }

        #endregion 公有函数

        private YHSDK.FCResultType npFlowChart1_FlowRun(object sender, EventArgs e)
        {
            bool bret = model.Signal_R_Initialize;
            model.Signal_R_Initialize = bret;
            if (bret)
            {
                BInitIsOK = false;
                glueModule = new GlueModule(DataModel.AxisWorkModule, new GlueModuleParams());
                model.Signal_W_InitializeOK = false;
                model.Signal_W_Initializing = true;
                PrintLog(string.Format($"接收到初始化指令，开始初始化任务"), Color.Black);
                return FCResultType.NEXT;
            }
            return FCResultType.IDLE;
        }


        /// <summary>
        /// 复位点胶模组
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        private FCResultType npFlowChart8_FlowRun(object sender, EventArgs e)
        {
            BInitIsOK = true;
            model.Signal_W_InitializeOK = true;
            model.Signal_W_Initializing = false;//点胶任务接受
            return FCResultType.NEXT;
        }

        private FCResultType fc_GlueAuto6_FlowRun(object sender, EventArgs e)
        {
            if (0 == glueModule.ExecuteMoveToHome())
            {
                PrintLog("点胶模组回原成功", Color.Green);
                return FCResultType.NEXT;
            }
            else
            {
                GlueModuleError = 2000;
                PrintLog("点胶模组回原失败", Color.Red);
                return FCResultType.CASE1;
            }
        }

        private FCResultType exptionFC_FlowRun(object sender, EventArgs e)
        {
            model.Signal_W_InitializeOK= false;
            model.Signal_W_Initializing = false;
            //从配置表里拿数据进行异常处理
            GloMotionParame.WriteErrCode(StageIndex, (short)GlueModuleError);
            return FCResultType.NEXT;
        }

        private FCResultType UVup_FlowRun(object sender, EventArgs e)
        {
            if (!model.IsContainUV)
                return FCResultType.NEXT;
            MotionModel.Instance.SetDo(model.UVUpOut, true);
            MotionModel.Instance.SetDo(model.UVDownOut, false);
            bool uvDownIn = false;
            bool uvUpIn = false;
            int i = 0;
            while ((uvDownIn || !uvUpIn) && i < 10)
            {
                i++;
                MotionModel.Instance.GetDi(model.UVDownIn, out uvDownIn);
                MotionModel.Instance.GetDi(model.UVUpIn, out uvUpIn);
                Thread.Sleep(100);
            }
            if (!uvDownIn && uvUpIn)
            {
                PrintLog("UV气缸上升到位", Color.Black);
                return FCResultType.NEXT;
            }
            GlueModuleError = 2072;
            return FCResultType.CASE1;
        }
    }

}
