﻿namespace ModuleData.Views
{
    partial class uc_GlueExecuteWeighting
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.fc_GlueAuto7 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto1 = new YHSDK.NPFlowChart();
            this.npFlowChart1 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto2 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto3 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto4 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto5 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto6 = new YHSDK.NPFlowChart();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_weightometer = new System.Windows.Forms.Label();
            this.tb_number = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_glueTimer = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tb_IOVal = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chk_IsUseManual = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // fc_GlueAuto7
            // 
            this.fc_GlueAuto7.AlarmCode = "";
            this.fc_GlueAuto7.ArrowsWidth = 1;
            this.fc_GlueAuto7.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto7.bClearTrace = false;
            this.fc_GlueAuto7.bIsLastFlowChart = false;
            this.fc_GlueAuto7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto7.bSkip = false;
            this.fc_GlueAuto7.bStepByStepMode = false;
            this.fc_GlueAuto7.CASE1 = null;
            this.fc_GlueAuto7.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto7.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto7.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.CASE2 = null;
            this.fc_GlueAuto7.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto7.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto7.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.CASE3 = null;
            this.fc_GlueAuto7.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto7.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto7.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto7.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto7.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto7.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto7.Location = new System.Drawing.Point(19, 365);
            this.fc_GlueAuto7.Margin = new System.Windows.Forms.Padding(8);
            this.fc_GlueAuto7.menuOpening = false;
            this.fc_GlueAuto7.Name = "fc_GlueAuto7";
            this.fc_GlueAuto7.NEXT = this.fc_GlueAuto1;
            this.fc_GlueAuto7.NextArrowType = YHSDK.ArrowTypes.Square;
            this.fc_GlueAuto7.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto7.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.Size = new System.Drawing.Size(227, 27);
            this.fc_GlueAuto7.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto7.SubFlowChart = null;
            this.fc_GlueAuto7.TabIndex = 45;
            this.fc_GlueAuto7.Text = "复位PLC信号";
            this.fc_GlueAuto7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto7.TimeOut = 10000;
            this.fc_GlueAuto7.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto7.tmrTimeOut = null;
            this.fc_GlueAuto7.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto7_FlowRun);
            // 
            // fc_GlueAuto1
            // 
            this.fc_GlueAuto1.AlarmCode = "";
            this.fc_GlueAuto1.ArrowsWidth = 1;
            this.fc_GlueAuto1.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto1.bClearTrace = false;
            this.fc_GlueAuto1.bIsLastFlowChart = false;
            this.fc_GlueAuto1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto1.bSkip = false;
            this.fc_GlueAuto1.bStepByStepMode = false;
            this.fc_GlueAuto1.CASE1 = null;
            this.fc_GlueAuto1.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto1.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.CASE2 = null;
            this.fc_GlueAuto1.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto1.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.CASE3 = null;
            this.fc_GlueAuto1.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto1.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto1.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto1.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto1.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto1.Location = new System.Drawing.Point(19, 16);
            this.fc_GlueAuto1.Margin = new System.Windows.Forms.Padding(8);
            this.fc_GlueAuto1.menuOpening = false;
            this.fc_GlueAuto1.Name = "fc_GlueAuto1";
            this.fc_GlueAuto1.NEXT = this.npFlowChart1;
            this.fc_GlueAuto1.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto1.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Size = new System.Drawing.Size(227, 27);
            this.fc_GlueAuto1.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto1.SubFlowChart = null;
            this.fc_GlueAuto1.TabIndex = 44;
            this.fc_GlueAuto1.Text = "等待触发称重";
            this.fc_GlueAuto1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto1.TimeOut = 10000;
            this.fc_GlueAuto1.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto1.tmrTimeOut = null;
            this.fc_GlueAuto1.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto1_FlowRun);
            // 
            // npFlowChart1
            // 
            this.npFlowChart1.AlarmCode = "";
            this.npFlowChart1.ArrowsWidth = 1;
            this.npFlowChart1.BackColor = System.Drawing.Color.White;
            this.npFlowChart1.bClearTrace = false;
            this.npFlowChart1.bIsLastFlowChart = false;
            this.npFlowChart1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.npFlowChart1.bSkip = false;
            this.npFlowChart1.bStepByStepMode = false;
            this.npFlowChart1.CASE1 = null;
            this.npFlowChart1.Case1ArrowType = YHSDK.ArrowTypes.Square;
            this.npFlowChart1.CASE1Color = System.Drawing.Color.LimeGreen;
            this.npFlowChart1.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.npFlowChart1.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.npFlowChart1.CASE2 = null;
            this.npFlowChart1.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart1.CASE2Color = System.Drawing.Color.Red;
            this.npFlowChart1.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.CASE3 = null;
            this.npFlowChart1.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart1.CASE3Color = System.Drawing.Color.Blue;
            this.npFlowChart1.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.DefaultColor = System.Drawing.Color.White;
            this.npFlowChart1.eSkipPath = YHSDK.FCResultType.NEXT;
            this.npFlowChart1.ExecutedColor = System.Drawing.Color.LightGray;
            this.npFlowChart1.ExecutingColor = System.Drawing.Color.Lime;
            this.npFlowChart1.Location = new System.Drawing.Point(19, 66);
            this.npFlowChart1.Margin = new System.Windows.Forms.Padding(8);
            this.npFlowChart1.menuOpening = false;
            this.npFlowChart1.Name = "npFlowChart1";
            this.npFlowChart1.NEXT = this.fc_GlueAuto2;
            this.npFlowChart1.NextArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart1.NEXTColor = System.Drawing.Color.Black;
            this.npFlowChart1.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.Size = new System.Drawing.Size(227, 27);
            this.npFlowChart1.SkipColor = System.Drawing.Color.Yellow;
            this.npFlowChart1.SubFlowChart = null;
            this.npFlowChart1.TabIndex = 55;
            this.npFlowChart1.Text = "UV气缸抬起";
            this.npFlowChart1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.npFlowChart1.TimeOut = 10000;
            this.npFlowChart1.TimeoutColor = System.Drawing.Color.Red;
            this.npFlowChart1.tmrTimeOut = null;
            this.npFlowChart1.FlowRun += new YHSDK.FlowRunEvent(this.npFlowChart1_FlowRun);
            // 
            // fc_GlueAuto2
            // 
            this.fc_GlueAuto2.AlarmCode = "";
            this.fc_GlueAuto2.ArrowsWidth = 1;
            this.fc_GlueAuto2.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto2.bClearTrace = false;
            this.fc_GlueAuto2.bIsLastFlowChart = false;
            this.fc_GlueAuto2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto2.bSkip = false;
            this.fc_GlueAuto2.bStepByStepMode = false;
            this.fc_GlueAuto2.CASE1 = null;
            this.fc_GlueAuto2.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto2.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto2.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.CASE2 = null;
            this.fc_GlueAuto2.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto2.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto2.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.CASE3 = null;
            this.fc_GlueAuto2.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto2.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto2.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto2.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto2.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto2.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto2.Location = new System.Drawing.Point(19, 115);
            this.fc_GlueAuto2.Margin = new System.Windows.Forms.Padding(8);
            this.fc_GlueAuto2.menuOpening = false;
            this.fc_GlueAuto2.Name = "fc_GlueAuto2";
            this.fc_GlueAuto2.NEXT = this.fc_GlueAuto3;
            this.fc_GlueAuto2.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto2.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto2.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.Size = new System.Drawing.Size(227, 27);
            this.fc_GlueAuto2.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto2.SubFlowChart = null;
            this.fc_GlueAuto2.TabIndex = 49;
            this.fc_GlueAuto2.Text = "称重计去皮";
            this.fc_GlueAuto2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto2.TimeOut = 10000;
            this.fc_GlueAuto2.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto2.tmrTimeOut = null;
            this.fc_GlueAuto2.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto2_FlowRun);
            // 
            // fc_GlueAuto3
            // 
            this.fc_GlueAuto3.AlarmCode = "";
            this.fc_GlueAuto3.ArrowsWidth = 1;
            this.fc_GlueAuto3.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto3.bClearTrace = false;
            this.fc_GlueAuto3.bIsLastFlowChart = false;
            this.fc_GlueAuto3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto3.bSkip = false;
            this.fc_GlueAuto3.bStepByStepMode = false;
            this.fc_GlueAuto3.CASE1 = null;
            this.fc_GlueAuto3.Case1ArrowType = YHSDK.ArrowTypes.Square;
            this.fc_GlueAuto3.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto3.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.fc_GlueAuto3.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.fc_GlueAuto3.CASE2 = null;
            this.fc_GlueAuto3.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto3.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto3.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.CASE3 = null;
            this.fc_GlueAuto3.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto3.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto3.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto3.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto3.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto3.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto3.Location = new System.Drawing.Point(19, 165);
            this.fc_GlueAuto3.Margin = new System.Windows.Forms.Padding(8);
            this.fc_GlueAuto3.menuOpening = false;
            this.fc_GlueAuto3.Name = "fc_GlueAuto3";
            this.fc_GlueAuto3.NEXT = this.fc_GlueAuto4;
            this.fc_GlueAuto3.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto3.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto3.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.Size = new System.Drawing.Size(227, 27);
            this.fc_GlueAuto3.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto3.SubFlowChart = null;
            this.fc_GlueAuto3.TabIndex = 47;
            this.fc_GlueAuto3.Text = "开始排胶";
            this.fc_GlueAuto3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto3.TimeOut = 10000;
            this.fc_GlueAuto3.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto3.tmrTimeOut = null;
            this.fc_GlueAuto3.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto3_FlowRun);
            // 
            // fc_GlueAuto4
            // 
            this.fc_GlueAuto4.AlarmCode = "";
            this.fc_GlueAuto4.ArrowsWidth = 1;
            this.fc_GlueAuto4.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto4.bClearTrace = false;
            this.fc_GlueAuto4.bIsLastFlowChart = false;
            this.fc_GlueAuto4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto4.bSkip = false;
            this.fc_GlueAuto4.bStepByStepMode = false;
            this.fc_GlueAuto4.CASE1 = null;
            this.fc_GlueAuto4.Case1ArrowType = YHSDK.ArrowTypes.Square;
            this.fc_GlueAuto4.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto4.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.fc_GlueAuto4.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.fc_GlueAuto4.CASE2 = null;
            this.fc_GlueAuto4.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto4.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto4.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.CASE3 = null;
            this.fc_GlueAuto4.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto4.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto4.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto4.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto4.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto4.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto4.Location = new System.Drawing.Point(19, 215);
            this.fc_GlueAuto4.Margin = new System.Windows.Forms.Padding(8);
            this.fc_GlueAuto4.menuOpening = false;
            this.fc_GlueAuto4.Name = "fc_GlueAuto4";
            this.fc_GlueAuto4.NEXT = this.fc_GlueAuto5;
            this.fc_GlueAuto4.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto4.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto4.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.Size = new System.Drawing.Size(227, 27);
            this.fc_GlueAuto4.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto4.SubFlowChart = null;
            this.fc_GlueAuto4.TabIndex = 48;
            this.fc_GlueAuto4.Text = "读取重量";
            this.fc_GlueAuto4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto4.TimeOut = 10000;
            this.fc_GlueAuto4.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto4.tmrTimeOut = null;
            this.fc_GlueAuto4.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto4_FlowRun);
            // 
            // fc_GlueAuto5
            // 
            this.fc_GlueAuto5.AlarmCode = "";
            this.fc_GlueAuto5.ArrowsWidth = 1;
            this.fc_GlueAuto5.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto5.bClearTrace = false;
            this.fc_GlueAuto5.bIsLastFlowChart = false;
            this.fc_GlueAuto5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto5.bSkip = false;
            this.fc_GlueAuto5.bStepByStepMode = false;
            this.fc_GlueAuto5.CASE1 = null;
            this.fc_GlueAuto5.Case1ArrowType = YHSDK.ArrowTypes.Square;
            this.fc_GlueAuto5.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto5.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.fc_GlueAuto5.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.fc_GlueAuto5.CASE2 = null;
            this.fc_GlueAuto5.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto5.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto5.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto5.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto5.CASE3 = null;
            this.fc_GlueAuto5.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto5.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto5.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto5.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto5.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto5.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto5.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto5.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto5.Location = new System.Drawing.Point(19, 265);
            this.fc_GlueAuto5.Margin = new System.Windows.Forms.Padding(8);
            this.fc_GlueAuto5.menuOpening = false;
            this.fc_GlueAuto5.Name = "fc_GlueAuto5";
            this.fc_GlueAuto5.NEXT = this.fc_GlueAuto6;
            this.fc_GlueAuto5.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto5.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto5.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto5.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto5.Size = new System.Drawing.Size(227, 27);
            this.fc_GlueAuto5.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto5.SubFlowChart = null;
            this.fc_GlueAuto5.TabIndex = 50;
            this.fc_GlueAuto5.Text = "判断重量是否超限";
            this.fc_GlueAuto5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto5.TimeOut = 10000;
            this.fc_GlueAuto5.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto5.tmrTimeOut = null;
            this.fc_GlueAuto5.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto5_FlowRun);
            // 
            // fc_GlueAuto6
            // 
            this.fc_GlueAuto6.AlarmCode = "";
            this.fc_GlueAuto6.ArrowsWidth = 1;
            this.fc_GlueAuto6.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto6.bClearTrace = false;
            this.fc_GlueAuto6.bIsLastFlowChart = false;
            this.fc_GlueAuto6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto6.bSkip = false;
            this.fc_GlueAuto6.bStepByStepMode = false;
            this.fc_GlueAuto6.CASE1 = null;
            this.fc_GlueAuto6.Case1ArrowType = YHSDK.ArrowTypes.Square;
            this.fc_GlueAuto6.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto6.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.fc_GlueAuto6.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.fc_GlueAuto6.CASE2 = null;
            this.fc_GlueAuto6.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto6.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto6.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.CASE3 = null;
            this.fc_GlueAuto6.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto6.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto6.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto6.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto6.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto6.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto6.Location = new System.Drawing.Point(19, 315);
            this.fc_GlueAuto6.Margin = new System.Windows.Forms.Padding(8);
            this.fc_GlueAuto6.menuOpening = false;
            this.fc_GlueAuto6.Name = "fc_GlueAuto6";
            this.fc_GlueAuto6.NEXT = this.fc_GlueAuto7;
            this.fc_GlueAuto6.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto6.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto6.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.Size = new System.Drawing.Size(227, 27);
            this.fc_GlueAuto6.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto6.SubFlowChart = null;
            this.fc_GlueAuto6.TabIndex = 51;
            this.fc_GlueAuto6.Text = "移动到安全为位";
            this.fc_GlueAuto6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto6.TimeOut = 10000;
            this.fc_GlueAuto6.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto6.tmrTimeOut = null;
            this.fc_GlueAuto6.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto6_FlowRun);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label1.Location = new System.Drawing.Point(19, 418);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(206, 31);
            this.label1.TabIndex = 53;
            this.label1.Text = "称重计实时重量：";
            // 
            // lbl_weightometer
            // 
            this.lbl_weightometer.AutoSize = true;
            this.lbl_weightometer.Font = new System.Drawing.Font("微软雅黑", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_weightometer.Location = new System.Drawing.Point(221, 418);
            this.lbl_weightometer.Name = "lbl_weightometer";
            this.lbl_weightometer.Size = new System.Drawing.Size(62, 31);
            this.lbl_weightometer.TabIndex = 54;
            this.lbl_weightometer.Text = "0.00";
            // 
            // tb_number
            // 
            this.tb_number.Location = new System.Drawing.Point(381, 43);
            this.tb_number.Name = "tb_number";
            this.tb_number.Size = new System.Drawing.Size(100, 21);
            this.tb_number.TabIndex = 56;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(288, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 57;
            this.label2.Text = "出胶次数：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(271, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 12);
            this.label3.TabIndex = 58;
            this.label3.Text = "出胶时间（ms）：";
            // 
            // tb_glueTimer
            // 
            this.tb_glueTimer.Location = new System.Drawing.Point(381, 81);
            this.tb_glueTimer.Name = "tb_glueTimer";
            this.tb_glueTimer.Size = new System.Drawing.Size(100, 21);
            this.tb_glueTimer.TabIndex = 56;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(335, 203);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(106, 39);
            this.button1.TabIndex = 59;
            this.button1.Text = "测试出胶";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tb_IOVal
            // 
            this.tb_IOVal.Location = new System.Drawing.Point(381, 119);
            this.tb_IOVal.Name = "tb_IOVal";
            this.tb_IOVal.Size = new System.Drawing.Size(100, 21);
            this.tb_IOVal.TabIndex = 56;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(288, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 57;
            this.label4.Text = "出胶IO号：";
            // 
            // chk_IsUseManual
            // 
            this.chk_IsUseManual.AutoSize = true;
            this.chk_IsUseManual.Location = new System.Drawing.Point(381, 165);
            this.chk_IsUseManual.Name = "chk_IsUseManual";
            this.chk_IsUseManual.Size = new System.Drawing.Size(96, 16);
            this.chk_IsUseManual.TabIndex = 60;
            this.chk_IsUseManual.Text = "手动设置模式";
            this.chk_IsUseManual.UseVisualStyleBackColor = true;
            // 
            // uc_GlueExecuteWeighting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.chk_IsUseManual);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_IOVal);
            this.Controls.Add(this.tb_glueTimer);
            this.Controls.Add(this.tb_number);
            this.Controls.Add(this.npFlowChart1);
            this.Controls.Add(this.lbl_weightometer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.fc_GlueAuto6);
            this.Controls.Add(this.fc_GlueAuto5);
            this.Controls.Add(this.fc_GlueAuto2);
            this.Controls.Add(this.fc_GlueAuto3);
            this.Controls.Add(this.fc_GlueAuto4);
            this.Controls.Add(this.fc_GlueAuto1);
            this.Controls.Add(this.fc_GlueAuto7);
            this.Name = "uc_GlueExecuteWeighting";
            this.Size = new System.Drawing.Size(494, 536);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private YHSDK.NPFlowChart fc_GlueAuto7;
        private YHSDK.NPFlowChart fc_GlueAuto1;
        private YHSDK.NPFlowChart fc_GlueAuto2;
        private YHSDK.NPFlowChart fc_GlueAuto3;
        private YHSDK.NPFlowChart fc_GlueAuto4;
        private YHSDK.NPFlowChart fc_GlueAuto5;
        private YHSDK.NPFlowChart fc_GlueAuto6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_weightometer;
        private YHSDK.NPFlowChart npFlowChart1;
        private System.Windows.Forms.TextBox tb_number;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_glueTimer;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tb_IOVal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chk_IsUseManual;
    }
}
