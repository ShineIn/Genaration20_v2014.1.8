﻿using G4.GlueCore;
using G4.Motion;
using LogSv;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using YHSDK;

namespace ModuleData.Views
{
    [Serializable]
    public partial class uc_GlueExecuteWeighting : ModuleBase
    {
        public uc_GlueExecuteWeighting()
        {
            InitializeComponent();
        }

        private JTimer JM_weightometer = new JTimer();
        private double oldWeightometerValue1 = 0;
        private double newWeightometerValue1 = 0;
        private bool DispWeightometerValue1 = false;
        bool bResult = false;
        #region 公有属性
        public WeightModel DataModel
        {
            get { return model; }
            set { model = value; OnPropertyChanged(); }
        }
        private WeightModel model = new WeightModel();

        /// <summary>
        /// 自动对针的结果
        /// </summary>
        [Browsable(false)]
        public bool AutoNeedelResult
        {
            get { return gutoNeedelResult; }
            private set { gutoNeedelResult = value; OnPropertyChanged(); }
        }
        bool gutoNeedelResult = false;

        /// <summary>
        /// 对针中心的X
        /// </summary>
        [Browsable(false)]
        public double ExecuteAlignNeedle_X
        {
            get { return executeAlignNeedle_X; }
            private set { executeAlignNeedle_X = value; OnPropertyChanged(); }
        }
        double executeAlignNeedle_X = 0;
        /// <summary>
        /// 对针中心的Y
        /// </summary>
        [Browsable(false)]
        public double ExecuteAlignNeedle_Y
        {
            get { return executeAlignNeedle_Y; }
            private set { executeAlignNeedle_Y = value; OnPropertyChanged(); }
        }
        double executeAlignNeedle_Y = 0;
        /// <summary>
        /// 对针中心的Z
        /// </summary>
        [Browsable(false)]
        public double ExecuteAlignNeedle_Z
        {
            get { return executeAlignNeedle_Z; }
            private set { executeAlignNeedle_Z = value; OnPropertyChanged(); }
        }
        double executeAlignNeedle_Z = 0;

        /// <summary>
        /// 称重机对象
        /// </summary>
        public WeightometerModel Weightometer
        {
            get { return _weightometer; }
            set { _weightometer = value; }
        }
        private WeightometerModel _weightometer = new WeightometerModel();

        #endregion 公有属性

        #region 公有函数
        protected override void Run()
        {
            if (null == model) return;
            if (fc_GlueAuto1.WorkFlow == null)
                fc_GlueAuto1.TaskReset();
            fc_GlueAuto1.TaskRun();
            if (this.IsHandleCreated)
            {
                this.Invoke(new Action(() =>
                {
                    lbl_weightometer.Text = (Weightometer.RealValue*1000).ToString("f3") + "  mg";
                }));
            }
            //if (!IsWaitForSignal)
            //{
            //    model.ParentProject.PLC_StandbyPosition = false;
            //}
        }

        public override void ResetChart()
        {
            StartChart = fc_GlueAuto1;
            fc_GlueAuto1.TaskReset();
        }
        private int MoveTo(double X, double Y, double Z, bool IsSafety = true)
        {
            double safetyPos = 0;
            if (IsSafety)
            {
                var rtn = glueModule.ExecuteZMoveTo(safetyPos, 20, 20);
                if (rtn == 0)
                {
                    var rtnXY = glueModule.ExecuteXYMoveTo(X, Y, 20, 20);
                    if (0 == rtnXY)
                    {
                        return glueModule.ExecuteZMoveTo(Z, 20, 20);
                    }
                    else
                        return -1;
                }
                else
                    return -1;
            }
            else
            {
                var rtn = glueModule.ExecuteXYMoveTo(X, Y, 20, 20);
                if (0 == rtn)
                {
                    return glueModule.ExecuteZMoveTo(Z, 20, 20);
                }
                else
                    return -1;
            }
        }

        #endregion 公有函数

        private YHSDK.FCResultType fc_GlueAuto1_FlowRun(object sender, EventArgs e)
        {
            GlueProject project = model.ParentProject as GlueProject;
            if (null == project || null == project.ExcuteGlueModule) return FCResultType.IDLE;
            glueModule = project.ExcuteGlueModule;
            //model.ParentProject.PLC_StandbyPosition = true;
            bool bret = model.PLC_Sig_AutoWei;
            if (bret)
            {
                model.PLC_Sig_AutoWei = bret;
                model.SigAutoWeiOK = model.SigAutoWeiNG = false;
                model.SoftSigAutoWei = true;
                float tempMax = model.WeightometerValueMax;
                float tempMin = model.WeightometerValueMin;
                model.WeightometerValueMax = tempMax;
                model.WeightometerValueMin = tempMin;
                PrintLog(string.Format($"接收到称重指令，开始称重任务"), Color.Black);
                JM_weightometer.Restart();
                model.ParentProject.UpProcessAddre = model.ParentProject.ProcessAddre;

                return FCResultType.NEXT;
            }
            return FCResultType.IDLE;
        }

        private FCResultType fc_GlueAuto2_FlowRun(object sender, EventArgs e)
        {
            if (!Weightometer.IsConnected)
            {
                Weightometer.PortName = model.SeriPortName;
                Weightometer.BaudRate = model.BaudRate;

                Weightometer.Initialize();
            }
            Thread.Sleep(200);
            if (Weightometer.IsConnected)
            {
                DispWeightometerValue1 = true;
                if (Weightometer.IsStable)
                {
                    oldWeightometerValue1 = Weightometer.RealValue * 1000;
                    PrintLog("称重前获取重量OK", System.Drawing.Color.Black);
                    return FCResultType.NEXT;
                }
                else
                {
                    if (JM_weightometer.On(50000))
                    {
                        PrintLog("称重前获取重量超时", System.Drawing.Color.Red);
                    }
                }
            }
            else
            {
                PrintLog("称重计连接失败", System.Drawing.Color.Red);
            }
            return FCResultType.IDLE;
        }

        private FCResultType fc_GlueAuto3_FlowRun(object sender, EventArgs e)
        {
            //var rtn = glueModule.ExecuteWeighting(model.GlueModuleNum);
            //if (rtn == 0)
            //    bResult = true;
            //else
            //    bResult = false;
            var rtn = MoveTo(model.ParentProject.GluePram.WeightingLocation.X, model.ParentProject.GluePram.WeightingLocation.Y, model.ParentProject.GluePram.WeightingZPosition, true);
            if (rtn != 0)
            {
                PrintLog("点胶模组运动到安全位失败", Color.Red);
                return FCResultType.IDLE;
            }
            GlueProject project = model.ParentProject as GlueProject;
            bool iobool = int.TryParse(this.tb_IOVal.Text, out int val);
            try
            {
                if (chk_IsUseManual.Checked)
                {
                    if (int.TryParse(this.tb_number.Text, out int numVal) && int.TryParse(this.tb_glueTimer.Text, out int gluetime) && iobool)
                    {
                        for (int i = 0; i < numVal; i++)
                        {
                            Thread.Sleep(1000);
                            MotionModel.Instance.SetDo(val, true);
                            Thread.Sleep(gluetime);
                            MotionModel.Instance.SetDo(val, false);
                            Log.log.Write($"第{i+1}次称重结束", Color.Black);
                        }
                        bResult = true;
                        JM_weightometer.Restart();
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        bResult = false;
                        Log.log.Write("手动设置参数有误", Color.Red);
                        return FCResultType.IDLE;
                    }
                }
                else
                {
                    for (int i = 0; i < project.GlueExcuteModel.DispenseTimeList.Count; i++)
                    {
                        Thread.Sleep(1000);
                        MotionModel.Instance.SetDo(val, true);
                        Thread.Sleep((int)project.GlueExcuteModel.DispenseTimeList[i]);
                        MotionModel.Instance.SetDo(val, false);
                        Log.log.Write($"第{i + 1}次称重结束,用时{(int)project.GlueExcuteModel.DispenseTimeList[i]}", Color.Black);
                    }
                    bResult = true;
                    JM_weightometer.Restart();
                    return FCResultType.NEXT;
                }
                
            }
            catch (Exception)
            {
                bResult = false;
                Log.log.Write("称重点胶执行失败",Color.Red);
                return FCResultType.IDLE;
            }
            

        }
        private FCResultType fc_GlueAuto4_FlowRun(object sender, EventArgs e)
        {
            if (Weightometer.IsConnected)
            {
                if (Weightometer.IsStable)
                {
                    newWeightometerValue1 = Weightometer.RealValue * 1000;
                    DispWeightometerValue1 = false;
                    PrintLog("称重后获取重量OK", System.Drawing.Color.Black);
                    return FCResultType.NEXT;
                }
                else
                {
                    if (JM_weightometer.On(50000))
                    {
                        PrintLog("称重后获取重量超时", System.Drawing.Color.Red);
                    }
                }
            }
            else
            {
                PrintLog("称重器未连接", System.Drawing.Color.Red);
            }
            return FCResultType.IDLE;
        }

        private FCResultType fc_GlueAuto5_FlowRun(object sender, EventArgs e)
        {
            model.WeightometerValue = (float)(newWeightometerValue1 - oldWeightometerValue1);
            if (model.WeightometerValue < model.WeightometerValueMax && model.WeightometerValue > model.WeightometerValueMin)
            {
                bResult = true;
                PrintLog(string.Format("称重结果OK，数值：{0}", model.WeightometerValue.ToString("F3")), System.Drawing.Color.Green);
            }
            else
            {
                bResult = false;
                PrintLog(string.Format("称重结果超限，数值：{0}", model.WeightometerValue.ToString("F3")), System.Drawing.Color.Red);
            }
            //数据传输给PLC
            return FCResultType.NEXT;
        }

        private FCResultType fc_GlueAuto6_FlowRun(object sender, EventArgs e)
        {
            var rtn = MoveTo(model.ParentProject.GlueSafetyPos.XPos, model.ParentProject.GlueSafetyPos.YPos, model.ParentProject.GlueSafetyPos.ZPos, true);
            if (rtn != 0)
            {
                PrintLog("点胶模组运动到安全位失败", Color.Red);
                return FCResultType.IDLE;
            }
            return FCResultType.NEXT;
        }

        private FCResultType fc_GlueAuto7_FlowRun(object sender, EventArgs e)
        {
            if (bResult)
            {
                model.SigAutoWeiOK = bResult;
                model.SoftSigAutoWeiFin = true;
            }
            else
            {
                model.SigAutoWeiNG = bResult;
                model.SoftSigAutoWeiFin = true;
            }
            model.SigAutoWeiOK = bResult;
            model.SigAutoWeiNG = !bResult;
            Thread.Sleep(200);
            if (model.SigAutoWeiOK == bResult && model.SigAutoWeiNG == !bResult)
            {
                model.SoftSigAutoWeiFin = true;
                Thread.Sleep(200);
                if (model.SoftSigAutoWeiFin == true)
                {
                    Thread.Sleep(1000);
                    model.ParentProject.UpProcessAddre = 0;
                    return FCResultType.NEXT;
                }
                else
                {
                    return FCResultType.IDLE;
                }
            }
            else
            {
                return FCResultType.IDLE;
            }
        }

        private FCResultType npFlowChart1_FlowRun(object sender, EventArgs e)
        {
            GlueProject project = model.ParentProject as GlueProject;
            if (project == null) return FCResultType.IDLE;
            if (project.UVLightUp() || !project.IsContainUV)
            {
                return FCResultType.NEXT;
            }
            else
            {
                return FCResultType.IDLE;
            }
        }

        private GlueModule glueModule;

        private void button1_Click(object sender, EventArgs e)
        {
            bool iobool = int.TryParse(this.tb_IOVal.Text, out int val);
            if (int.TryParse(this.tb_number.Text, out int numVal) && int.TryParse(this.tb_glueTimer.Text, out int gluetime) && iobool)
            {
                for (int i = 0; i < numVal; i++)
                {
                    Thread.Sleep(1000);
                    MotionModel.Instance.SetDo(val, true);
                    Thread.Sleep(gluetime);
                    MotionModel.Instance.SetDo(val, false);
                    Log.log.Write("单次称重结束", Color.Black);

                }
            }
        }
    }
}
