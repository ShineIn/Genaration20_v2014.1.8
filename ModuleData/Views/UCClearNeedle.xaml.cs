﻿using Googo.Manul.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ModuleData
{
    /// <summary>
    /// UCDropGlue.xaml 的交互逻辑
    /// </summary>
    public partial class UCClearNeedle : UserControl
    {
        public UCClearNeedle()
        {
            InitializeComponent();
        }


        public static DependencyProperty SubModuleProperty = DependencyProperty.Register("SubModule", typeof(ClearNeedleModel), typeof(UCClearNeedle), new FrameworkPropertyMetadata(new ClearNeedleModel(),FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public ClearNeedleModel SubModule
        {
            get { return (ClearNeedleModel)GetValue(SubModuleProperty); }
            set { SetValue(SubModuleProperty, value); }
        }

    }
}
