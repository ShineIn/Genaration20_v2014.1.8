﻿using Googo.Manul;
using SuperSimpleTcp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ModuleData.Views
{
    /// <summary>
    /// UCKeySensorOperate.xaml 的交互逻辑
    /// </summary>
    public partial class UCKeySensorOperate : UserControl, INotifyPropertyChanged
    {
        public UCKeySensorOperate()
        {
            InitializeComponent();
            CommandBindings.Add(new CommandBinding(TestCommand, TestCommand_Excute, TestCommand_CanExcute));


        }
        public static RoutedCommand TestCommand = new RoutedCommand();

        public static DependencyProperty IsMultiSensorProperty = DependencyProperty.Register("IsMultiSensor", typeof(bool), typeof(UCKeySensorOperate), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public static DependencyProperty HeightSenClientProperty =
DependencyProperty.Register("HeightSenClient", typeof(SimpleTcpClient), typeof(UCKeySensorOperate), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));

        public double HeightVal
        {
            get { return _heightVal; }
            set { _heightVal = value; OnPropertyChanged(); }
        }
        private double _heightVal = 0;
     

        /// <summary>
        /// 测高传感器TCP对象
        /// </summary>
        /// <remarks>
        /// TCP对象，到最外层直接进行绑定
        /// </remarks>
        public SimpleTcpClient HeightSenClient
        {
            get { return (SimpleTcpClient)GetValue(HeightSenClientProperty); }
            set { SetValue(HeightSenClientProperty, value); }
        }


        /// <summary>
        /// 生产时间
        /// </summary>
        public ObservableCollection<SendorData> RawDataList
        {
            get { return rawDataList; }
            set { rawDataList = value; OnPropertyChanged(); }
        }
        private ObservableCollection<SendorData> rawDataList = new ObservableCollection<SendorData>();

    
       

        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        #endregion INotifyPropertyChanged

        private static void DepenProChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            UCKeySensorOperate flow = d as UCKeySensorOperate;
            if (null == flow) return;
            if (e.Property == HeightSenClientProperty)
            {
                SimpleTcpClient old = e.OldValue as SimpleTcpClient;
                if (null != old)
                {
                    old.Events.DataReceived -= flow.Alg_DataReceived;
                }
                SimpleTcpClient client = e.NewValue as SimpleTcpClient;
                if (null == client) return;
                client.Events.DataReceived += flow.Alg_DataReceived;
            }
        }


        private void Alg_DataReceived(object sender, SuperSimpleTcp.DataReceivedEventArgs e)
        {
            if (e.Data == null || null == e.Data.Array)
            {
                return;
            }
            string strRec = Encoding.UTF8.GetString(e.Data.Array);
            Debug.WriteLine($"Port={e.IpPort}+Data={strRec}");
            if (strRec.Contains("M0"))
            {
                Dispatcher.Invoke(() =>
                {
                    //RawDataList = new ObservableCollection<SendorData>();
                    var arr = strRec.Split(',');
                    if (arr == null || arr.Length == 0)
                    {
                        return;
                    }
                    //for (int i = 1; i < arr.Length; i++)
                    //{
                    //    RawDataList.Add(new SendorData { RawData = arr[i] });
                    //}
                    double.TryParse(arr[1], out double heiVal);
                    HeightVal = heiVal;
                });
            } 
        }

   



     

        private void TestCommand_Excute(object sender, ExecutedRoutedEventArgs e)
        {
            HeightSenClient.Send("M0\r\n");//接收传感器数据
        }



        private void TestCommand_CanExcute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = null != HeightSenClient && HeightSenClient.IsConnected;
            //e.CanExecute = true;
        }
       
    }

    public class SendorData : NotifyChangedBase
    {
        /// <summary>
        /// 生产时间
        /// </summary>
        public string RawData
        {
            get { return rawData; }
            set 
            { 
                rawData = value;
                OnPropertyChanged();
                if (!string.IsNullOrEmpty(value)&&double.TryParse(value, out double val))
                {
                    ConvHeight = val * 0.001;
                }
            }
        }
        private string rawData = string.Empty;

        /// <summary>
        /// 生产时间
        /// </summary>
        public double ConvHeight
        {
            get { return convHeight; }
            private set { convHeight = value; OnPropertyChanged(); }
        }
        private double convHeight = 0;
    }
}
