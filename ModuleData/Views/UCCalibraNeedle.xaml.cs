﻿using Googo.Manul.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ModuleData
{
    /// <summary>
    /// UCDropGlue.xaml 的交互逻辑
    /// </summary>
    public partial class UCCalibraNeedle : UserControl
    {
        public UCCalibraNeedle()
        {
            InitializeComponent();
        }


        public static DependencyProperty SubModuleProperty = DependencyProperty.Register("SubModule", typeof(AutoCalibraNeedle), typeof(UCCalibraNeedle), new FrameworkPropertyMetadata(new AutoCalibraNeedle(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public AutoCalibraNeedle SubModule
        {
            get { return (AutoCalibraNeedle)GetValue(SubModuleProperty); }
            set { SetValue(SubModuleProperty, value); }
        }
    }
}
