﻿using G4.GlueCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YHSDK;
using System.Threading;
using System.Windows.Markup;
using Googo.Manul;
using System.Xml.Serialization;
using SmartGlue.Infrastructure;
using SuperSimpleTcp;
using System.Diagnostics;
using G4.Motion;
using LogSv;
using Newtonsoft.Json;
using ShowAlarm;
using SmartGlue.Routine.ViewModels;
using System.Windows.Shapes;

namespace ModuleData
{
    [Serializable]
    public partial class uc_GlueFlow : ModuleBase
    {
        public uc_GlueFlow()
        {
            InitializeComponent();
        }
        #region 公有属性

        public GlueModel DataModel
        {
            get { return model; }
            set { model = value; OnPropertyChanged(); }
        }
        private GlueModel model = new GlueModel();

        /// <summary>
        /// 权限管理是否锁界面参数
        /// </summary>
        public bool IsLockParam
        {
            get { return isLockParam; }
            set { isLockParam = value; SetNudControlLock(value); OnPropertyChanged(); }
        }
        private bool isLockParam = false;
        /// <summary>
        /// 测高传感器TCP对象
        /// </summary>
        /// <remarks>
        /// TCP对象，到最外层直接进行绑定
        /// </remarks>
        [XmlIgnore]
        public SimpleTcpClient SensorClient
        {
            get { return _sensorClient; }
            set
            {
                if (_sensorClient != null && _sensorClient.Events != null)
                {
                    _sensorClient.Events.DataReceived -= sensorDataReceived;
                    _sensorClient.Events.Disconnected -= Events_Disconnected;

                }

                _sensorClient = value;
                if (value != null && value.Events != null)
                {
                    value.Events.DataReceived += sensorDataReceived;
                    value.Events.Disconnected += Events_Disconnected;
                }
                OnPropertyChanged();
            }
        }
        private SimpleTcpClient _sensorClient;

        /// <summary>
        /// UV灯TCP
        /// </summary>
        public SimpleTcpClient UVClient
        {
            get { return _uVClient; }
            set
            {
                if (_uVClient != null && _uVClient.Events != null)
                {
                    _uVClient.Events.DataReceived -= uvDataReceived;
                    _uVClient.Events.Disconnected -= uV_DisConnected;
                }
                _uVClient = value;
                if (value != null && value.Events != null)
                {
                    value.Events.DataReceived += uvDataReceived;
                    value.Events.Disconnected += uV_DisConnected;
                }
                OnPropertyChanged();
            }
        }

        //报警窗体
        public WinShowAlarm AlarmFrm
        {
            get { return alarmFrm; }
            set { alarmFrm = value; OnPropertyChanged(); }
        }
        private WinShowAlarm alarmFrm = null;


        private SimpleTcpClient _uVClient;

        /// <summary>
        /// 传感器TCP客户端接受到的最新信息
        /// </summary>
        /// <remarks>
        /// 获取之后马上清空信息
        /// </remarks>
        [XmlIgnore]
        private string SensorLastContent
        {
            get
            {
                string ret = sensorLastContent;
                sensorLastContent = string.Empty;
                return ret;
            }
            set { sensorLastContent = value; OnPropertyChanged(); }
        }
        private string sensorLastContent = string.Empty;
        #endregion 公有属性

        #region 公有函数
        protected override void Run()
        {
            if (null == model) return;
            glueProject = model.ParentProject as GlueProject;
            model = glueProject.GlueExcuteModel;
            if (null == glueProject) throw new NullReferenceException("模块的父点胶模组参数为空");
            if (glueProject.ExcuteGlueModule == null) throw new Exception("点胶参数为空，或者点胶文件路径不存在");
            if (DCTcpClient == null || !DCTcpClient.IsConnected) return;
            if (SensorClient == null || !SensorClient.IsConnected) return;
            if (fc_GlueAuto1.WorkFlow == null)
                fc_GlueAuto1.TaskReset();
            //CloseAlarmFrm();
            fc_GlueAuto1.TaskRun();
            //if (!IsWaitForSignal)
            //{
            //    model.ParentProject.PLC_StandbyPosition = false;
            //}
        }

        public override void ResetChart()
        {
            StartChart = fc_GlueAuto1;
            fc_GlueAuto1.TaskReset();
        }
        private int MoveTo(double X, double Y, double Z, bool IsSafety = true)
        {
            double safetyPos = 0;
            double posX = 999, posY = 999, posZ = 999;
            glueProject.ExcuteGlueModule.GetPosition(out posX, out posY, out posZ);
            var rtn = -1;
            if (IsSafety)
            {
                if (posZ > safetyPos)
                    rtn = glueProject.ExcuteGlueModule.ExecuteZMoveTo(safetyPos, model.GlueZAxisSpd, model.GlueZAxisAcc);
                else
                    rtn = 0; if (rtn == 0)
                {
                    var rtnXY = glueProject.ExcuteGlueModule.ExecuteXYMoveTo(X, Y, model.GlueXYAxisSpd, model.GlueXYAxisAcc);
                    if (0 == rtnXY)
                    {
                        return glueProject.ExcuteGlueModule.ExecuteZMoveTo(Z, model.GlueZAxisSpd, model.GlueZAxisAcc);
                    }
                    else
                        return -1;
                }
                else
                    return -1;
            }
            else
            {
                rtn = glueProject.ExcuteGlueModule.ExecuteXYMoveTo(X, Y, model.GlueXYAxisSpd, model.GlueXYAxisAcc);
                if (0 == rtn)
                {
                    return glueProject.ExcuteGlueModule.ExecuteZMoveTo(Z, model.GlueZAxisSpd, model.GlueZAxisAcc);
                }
                else
                    return -1;

            }
        }

        public override void Stop()
        {
            base.Stop();
            //if (model.SlotNum == SlotType.Single)
            //{
            //    SetStopSignal(model.FirstSlot);
            //}
            //else if (model.SlotNum == SlotType.Double)
            //{
            //    SetStopSignal(model.FirstSlot);
            //    SetStopSignal(model.SecSlot);
            //}
            //else if (model.SlotNum == SlotType.Three)
            //{
            //    SetStopSignal(model.FirstSlot);
            //    SetStopSignal(model.SecSlot);
            //    SetStopSignal(model.ThirldSlot);
            //}
            //model.WriteIndexAddre = 0;
        }

        private void SetStopSignal(SlotSignalInfor slot)
        {
            if (slot == null) return;
            slot.Signal_W_GlueFinished = true;
            slot.Signal_W_GlueFinishedNG = true;
            slot.Signal_W_GlueFinishedOK = false;
            slot.Signal_W_ReceiveProgram = false;
        }

        /// <summary>
        /// 显示报警窗口
        /// </summary>
        /// <param name="code"></param>
        private void ShowAlarmInfo(int code)
        {
            AlarmFrm.Alarm.Enqueue(code);
            this.Stop();
        }

        /// <summary>
        /// 关闭报警窗口
        /// </summary>
        private void CloseAlarmFrm()
        {
            AlarmFrm.CloseAlarmFrm();
        }

        private void SetNudControlLock(bool isLock)
        {
            foreach (Control ctl in this.Controls)
            {
                if (ctl is NumericUpDown || ctl is NPFlowChart)
                {
                    ctl.Enabled = !isLock;
                }
            }
        }
        #endregion 公有函数

        #region 私有函数

        /// <summary>
        /// 算法客户端接受信息事件
        /// </summary>
        /// <remarks>
        /// 可以重新该事件，在里面做自己想要的事件处理
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sensorDataReceived(object sender, SuperSimpleTcp.DataReceivedEventArgs e)
        {
            if (e.Data == null) return;
            string strRec = Encoding.UTF8.GetString(e.Data.Array);
            Debug.WriteLine($"Port={e.IpPort}+Data={strRec}");
            SensorLastContent = strRec;
        }

        private void Events_Disconnected(object sender, SuperSimpleTcp.ConnectionEventArgs e)
        {
            Debug.WriteLine($"Port={e.IpPort} Disconnected Reason={e.Reason}");
        }

        private YHSDK.FCResultType fc_GlueAuto1_FlowRun(object sender, EventArgs e)
        {
            _runSlot = null;
            model.ParentProject.UpProcessAddre = 0;

            //Debug.WriteLine(model.ParentProject.AxisWorkModule.ToString());
            if (model == null || model.ParentProject == null)
            {
                PrintLog($"绑定数据为空", Color.Red);
                return FCResultType.IDLE;
            }
            //model.ParentProject.PLC_StandbyPosition = true;

            if (model.IsHaveStartSignal(out SlotSignalInfor workSlot))
            {
                Debug.Assert(workSlot != null);
                _runSlot = workSlot;
                //_runSlot.StartButSignal = true;

                //_runSlot.Signal_W_GlueFinished = false;
                //_runSlot.Signal_W_GlueFinishedNG = false;
                //_runSlot.Signal_W_GlueFinishedOK = false;
                //_runSlot.Signal_W_ReceiveProgram = true;
                if (!_runSlot.WaitStartReset())
                {
                    PrintLog($"模组号:{StageIndex}PLC没有复位点胶{StageIndex}可点胶信号为false,检查PLC程序,或者程序的DB块有没有配置正确", Color.Red);
                }
                _runSlot.Signal_W_GlueReportStatus = (short)txb_GlueReport1.Value;
                PrintLog(string.Format($"接收到任务指令，点胶模组开始工作"), Color.Black);
                curesult = new ResultInfor();
                curesult.ProcessIndex = model.ParentProject.UpProcessAddre = model.ParentProject.ProcessAddre;
                if (null == model.GlueCheckResult) model.GlueCheckResult = new List<bool>();
                model.GlueCheckResult.Clear();
                if (!string.IsNullOrEmpty(model.ProductSN))
                    curesult.ProductSN = model.ProductSN;
                else curesult.ProductSN = "NoSN";
                Log.log.Write($"当前点胶产品SN为{curesult.ProductSN}", Color.Black);
                return FCResultType.NEXT;
            }
            return FCResultType.IDLE;
        }


        private FCResultType fc_GlueAuto4_FlowRun(object sender, EventArgs e)
        {

            Thread.Sleep(10);
            //通知德创视觉Location任务
            if (model.GlueCamPointsOrder == null || model.GlueCamPointsOrder.Count == 0) return FCResultType.NEXT;
            //if (null == DCTcpClient || !DCTcpClient.IsConnected)
            //{
            //    PrintLog($"DCCK TCP连接断开", System.Drawing.Color.Red);
            //    GlueModuleError = 1030;
            //    return FCResultType.CASE1;
            //}
            glueStrokeParames = new List<GlueBase>();
            for (int i = 0; i < model.GlueCamPointsOrder.Count; i++)
            {
                PositionInfor PosCom = model.GlueCamPointsOrder[i];
                if (PosCom == null || PosCom.ExtenCommand == null) continue;

                PosCom.ExtenCommand.DownLoad();
                //JM_GlueModule.Restart();
                PosCom.ExtenCommand.SN = curesult.ProductSN;
                //if (!MoveXYZAxis(PosCom.XPos + _runSlot.Offset.X, PosCom.YPos + _runSlot.Offset.Y, PosCom.ZPos, true))
                //{
                //    if (JM_GlueModule.On(100000))
                //    {
                //        GlueModuleError = -1;

                //        PrintLog("模组运动失败", System.Drawing.Color.Red);
                //        ShowAlarmInfo(1022);
                //        return FCResultType.IDLE;
                //    }
                //    return FCResultType.IDLE;
                //}
                var rtn = MoveTo(PosCom.XPos + _runSlot.Offset.X, PosCom.YPos + _runSlot.Offset.Y, PosCom.ZPos, true);
                if (0 != rtn)
                {
                    GlueModuleError = rtn;
                    curesult.ProNGCode = MotionNGCode;

                    PrintLog("模组运动失败", System.Drawing.Color.Red);
                    ShowAlarmInfo(1022);
                    return FCResultType.CASE1;
                }
                string mLocationBuffer = string.Empty;
                String StrCom = !string.IsNullOrEmpty(PosCom.StrCommand) ? PosCom.StrCommand : JsonConvert.SerializeObject(PosCom.ExtenCommand);
                model.VisionTaskNO = PosCom.ExtenCommand.TaskNo.ToString();
                DCTcpClient.Send(StrCom);
                if (i == model.GlueCamPointsOrder.Count - 1)
                {
                    int retryNum = 0;
                    while (string.IsNullOrEmpty(mLocationBuffer) && retryNum < 40)
                    {
                        Thread.Sleep(200);
                        mLocationBuffer = AlgLastContent;
                        retryNum++;
                    }
                    if (string.IsNullOrEmpty(mLocationBuffer))
                    {
                        curesult.ProNGCode = VisionNGCode;

                        PrintLog($"DCCK TCP接收信息超时", System.Drawing.Color.Red);
                        ShowAlarmInfo(1009);
                        return FCResultType.CASE1;
                    }
                    ReceiveData position = null;
                    try
                    {
                        position = JsonConvert.DeserializeObject<ReceiveData>(mLocationBuffer);
                    }
                    catch (Exception ex)
                    {
                        Debug.WriteLine(ex);
                    }
                    if (null != position)
                    {
                        var posX = position.ResultList.FirstOrDefault((tt) => { return tt.Name.Contains("PosX"); });
                        if (posX != null)
                        {
                            double tem = (double)posX.Value;
                            curesult.GlueOffsetX = model.GlueOffsetX = (float)tem;
                        }
                        var posY = position.ResultList.FirstOrDefault((tt) => { return tt.Name.Contains("PosY"); });
                        if (posY != null)
                        {
                            double tem = (double)posY.Value;
                            curesult.GlueOffsetY = model.GlueOffsetY = (float)tem;
                        }
                        var posR = position.ResultList.FirstOrDefault((tt) => { return tt.Name.Contains("PosR"); });
                        if (posR != null)
                        {
                            double tem = (double)posR.Value;
                            curesult.GlueOffsetA = model.GlueOffsetA = (float)tem;
                        }
                        if (position.Result != 1 || position.ResultList == null || position.ResultList.Count < 3)
                        {
                            curesult.ProNGCode = VisionNGCode;
                            PrintLog($"视觉软件发送位置信息错误 错误:{position.Result}", System.Drawing.Color.Red);
                            return FCResultType.CASE1;
                            //后续改成json传输数据
                        }
                        if (PosCom.Result != null)
                        {
                            PosCom.Result.Assign(position);
                            PosCom.Result.UpLoads();
                        }
                        foreach (var item in position.ResultList)
                        {
                            var match = model.VisionResult.FirstOrDefault(temp => { return temp.Name == item.Name; });
                            if (null == match)
                            {
                                match = item;
                                model.VisionResult.Add(item);
                            }
                            match.Value = item.Name;
                            match.UpLoads();
                        }
                        //curesult.AlarmCode = short.Parse(position.Result.ToString());
                        var GluePath = position.ResultList.FirstOrDefault((tt) => { return tt.Name.Contains("LinePaths"); });
                        if (GluePath != null)
                        {
                            string strPaths = GluePath.Value.ToString();
                            List<GlueLineSegment> lines = JsonConvert.DeserializeObject<List<GlueLineSegment>>(strPaths);
                            if (null != lines)
                            {
                                glueStrokeParames.AddRange(lines);
                                return FCResultType.NEXT;
                            }
                        }
                        GluePath = position.ResultList.FirstOrDefault((tt) => { return tt.Name.Contains("PointPaths"); });
                        if (GluePath != null)
                        {
                            string strPaths = GluePath.Value.ToString();
                            List<GlueStrokeParame> points = JsonConvert.DeserializeObject<List<GlueStrokeParame>>(strPaths);
                            if (null != points)
                            {
                                glueStrokeParames.AddRange(points);
                                return FCResultType.NEXT;
                            }
                        }
                        GluePath = position.ResultList.FirstOrDefault((tt) => { return tt.Name.Contains("PolyPaths"); });
                        if (GluePath != null)
                        {
                            string strPaths = GluePath.Value.ToString();
                            List<GluePolyLine> points = JsonConvert.DeserializeObject<List<GluePolyLine>>(strPaths);
                            if (null != points)
                            {
                                glueStrokeParames.AddRange(points);
                                return FCResultType.NEXT;
                            }
                        }
                    }

                    PrintLog($"德创TCP发送信息为非json格式", System.Drawing.Color.Black);
                    string[] reciveData = mLocationBuffer.Split(new char[] { ',', ':', '：', '，' });
                    if (!mLocationBuffer.Contains("ERROR"))
                    {
                        double.TryParse(reciveData[1], out double x);
                        double.TryParse(reciveData[2], out double y);
                        //double.TryParse(reciveData[3], out double z);
                        curesult.GlueOffsetX = model.GlueOffsetX = (float)x;
                        curesult.GlueOffsetY = model.GlueOffsetY = (float)y;
                        //curesult.GlueOffsetA = model.GlueOffsetA = (float)z;
                        PrintLog($"拍照定位成功", System.Drawing.Color.Black);
                        PrintLog(string.Format("offsetXYA:{0},{1},{2}", model.GlueOffsetX.ToString("F3"), model.GlueOffsetY.ToString("F3"), model.GlueOffsetA.ToString("F3")), System.Drawing.Color.Black);
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        curesult.ProNGCode = VisionNGCode;
                        Log.log.Write("点胶1视觉失败", Color.Red);
                        return FCResultType.IDLE;
                    }
                }
            }
            return FCResultType.IDLE;
        }

        private FCResultType fc_GlueAuto7_FlowRun(object sender, EventArgs e)
        {
            if (null == model.GlueHeightPoints || model.GlueHeightPoints.Count == 0)
            {
                PrintLog("测高点位为空", Color.Red);
                return FCResultType.NEXT;
            }
            model.GlueHeight.Clear();
            foreach (PositionInfor point in model.GlueHeightPoints)
            {
                if (null == point) continue;
                //bool ret = MoveXYZAxis(point.XPos + _runSlot.Offset.X, point.YPos + _runSlot.Offset.Y, point.ZPos, true);
                //if (!ret)
                //{
                //    if (JM_GlueModule.On(100000))
                //    {
                //        GlueModuleError = -1;

                //        PrintLog("模组运动失败", System.Drawing.Color.Red);
                //        ShowAlarmInfo(1022);
                //        JM_GlueModule.Restart();
                //        return FCResultType.IDLE;
                //    }
                //    return FCResultType.IDLE;
                //}
                int iret = MoveTo(point.XPos + _runSlot.Offset.X, point.YPos + _runSlot.Offset.Y, point.ZPos, true);
                if (0 != iret)
                {
                    curesult.ProNGCode = SensorNGCode;

                    PrintLog($"移动位置失败,错误码={iret}", Color.Red);
                    continue;
                }
                if (!GetSensorData())
                {
                    curesult.ProNGCode = SensorNGCode;

                    PrintLog($"位移传感器获取数据失败", Color.Red);
                    continue;
                }
            }
            return FCResultType.NEXT;
        }

        /// <summary>
        /// 获取高度信息
        /// </summary>
        /// <returns></returns>
        private bool GetSensorData()
        {

            if (null == SensorClient || !SensorClient.IsConnected)
            {
                return false;
            }
            //通知德创视觉Location任务
            SensorClient.Send("M0\r\n");
            string mLocationBuffer = string.Empty;
            int iretryCount = 0;
            while (string.IsNullOrEmpty(mLocationBuffer) && iretryCount < 50)
            {
                iretryCount++;
                Thread.Sleep(100);
                mLocationBuffer = SensorLastContent;
            }
            if (string.IsNullOrEmpty(mLocationBuffer))
            {
                return false;
            }
            var arr = mLocationBuffer.Split(',');
            if (arr == null || arr.Length < 2)
            {
                return false;
            }
            if (double.TryParse(arr[1], out double val))
            {
                if (-099999998 == val)
                    return false;
                Debug.WriteLine($"height={val * 0.001}");
                double heightOffset = val * 0.001 - model.GlueHeightBase;
                if (heightOffset > model.GlueHeightMax || heightOffset < model.GlueHeightMin)
                {
                    PrintLog(string.Format("位移传感器数据：{0}超限\r\n", heightOffset.ToString("f3")), System.Drawing.Color.Red);
                    return false;
                }
                PrintLog(string.Format("位移传感器的测量值：{0}", (val * 0.001).ToString("f3")), Color.Black);
                PrintLog(string.Format("位移传感器偏移值：{0}", heightOffset.ToString("f3")), System.Drawing.Color.Black);
                double AutoNeedelOffsetHeight = 0;
                if (null != glueProject && null != glueProject.AutoCalibra)
                    AutoNeedelOffsetHeight = glueProject.AutoCalibra.IPC_PLC_AutoNeedelOffsetZ;
                heightOffset = heightOffset + AutoNeedelOffsetHeight;

                model.GlueHeight.Add((float)heightOffset);
                return true;
            }
            return false;
        }

        private FCResultType fc_GlueAuto10_FlowRun(object sender, EventArgs e)
        {
            PathIndex = 1;
            curesult.GlueHeight = model.GlueHeight;
            if (model.GlueHeight != null && model.GlueHeight.Count == model.GlueHeightPoints.Count)
            {
                _runSlot.Signal_W_GlueReportStatus = (short)txb_GlueReport2.Value;
                return FCResultType.NEXT;
            }
            else
            {
                curesult.ProNGCode = SensorNGCode;
                ShowAlarmInfo(1029);
                Log.log.Write("测高数据量异常，请检查", Color.Red);
                return FCResultType.CASE1;
            }
        }
        private int PathIndex = 1;
        /// <summary>
        /// 点胶
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        private FCResultType fc_GlueAuto11_FlowRun(object sender, EventArgs e)
        {
            model.DispenseTimeList.Clear();
            double tempX = 0, tempY = 0;
            string heightOffset = string.Empty;
            if (null != glueProject && null != glueProject.AutoCalibra)
            {
                tempX = glueProject.AutoCalibra.IPC_PLC_AutoNeedelOffsetX + model.DispGlueXYOffset.X;
                tempY = glueProject.AutoCalibra.IPC_PLC_AutoNeedelOffsetY + model.DispGlueXYOffset.Y;
                glueProject.GlueExcuteModel.GuleNeedleOffset = tempX.ToString("f3") + "," + tempY.ToString("f3");
            }
            glueProject.GlueExcuteModel.GlueOffset = model.GlueOffsetX.ToString("f3") + "," + model.GlueOffsetY.ToString("f3");
            for (int i = 0; i < model.GlueHeight.Count; i++)
            {
                heightOffset += model.GlueHeight[i].ToString("f3") + ",";
            }
            glueProject.GlueExcuteModel.GluePanelOffset = heightOffset;
            //设定点胶空移速度
            model.ParentProject.GluePram.ZDefaultVelocity = model.GlueZAxisSpd;
            model.ParentProject.GluePram.ZDefaultAccVelocity = model.GlueZAxisAcc;
            model.ParentProject.GluePram.XYDefaultVelocity = model.GlueXYAxisSpd;
            model.ParentProject.GluePram.XYDefaultAccVelocity = model.GlueXYAxisAcc;
            glueProject.ExcuteGlueModule = new GlueModule(model.ParentProject.AxisWorkModule, model.ParentProject.GluePram);
            glueTotalTime = 0;
            foreach (var item in glueStrokeParames)
            {
                PathBaseModel match = null;
                if (item.GetType() == typeof(GlueLineSegment))//线段
                {
                    GlueLineSegment glue = (GlueLineSegment)item;
                    LinePathModel line = (LinePathModel)glueProject.ExcuteGlueModule.Routine.CurrentSubstrate.Paths.FirstOrDefault((tt) => { return tt.GetType() == typeof(LinePathModel); });
                    line.StartPoint = new System.Windows.Point(glue.StartPoint.X + tempX, glue.StartPoint.Y + tempY);
                    line.EndPoint = new System.Windows.Point(glue.EndPoint.X + tempX, glue.EndPoint.Y + tempY);
                    match = line;
                    match = match.Clone();
                }
                else if (item.GetType() == typeof(GlueStrokeParame))//点
                {
                    GlueStrokeParame glue = (GlueStrokeParame)item;
                    StrokePathModel stroke = (StrokePathModel)glueProject.ExcuteGlueModule.Routine.CurrentSubstrate.Paths.FirstOrDefault((tt) => { return tt.GetType() == typeof(StrokePathModel); });
                    stroke.StartPoint = new System.Windows.Point(glue.StartPoint.X + tempX, glue.StartPoint.Y + tempY);
                    match = stroke;
                    match = match.Clone();
                }
                else if (item.GetType() == typeof(GluePolyLine))//多段連綫路徑
                {
                    GluePolyLine polyLine = (GluePolyLine)item;
                    PolylinePathModel poly = (PolylinePathModel)glueProject.ExcuteGlueModule.Routine.CurrentSubstrate.Paths.FirstOrDefault((tt) => { return tt.GetType() == typeof(PolylinePathModel); });
                    poly.StartPoint = new System.Windows.Point(polyLine.StartPoint.X + tempX, polyLine.StartPoint.Y + tempY);
                    List<System.Windows.Point> temp = new List<System.Windows.Point>();
                    for (int i = 0; i < polyLine.EndPoint.Count; i++)
                    {
                        temp.Add(new System.Windows.Point(polyLine.EndPoint[i].X + tempX, polyLine.EndPoint[i].Y + tempY));
                    }
                    poly = (PolylinePathModel)poly.Clone();
                    poly.ExtendVertex = temp;
                    match = poly;
                }
                if (null == match) continue;

                match.ZPosition = match.ZPosition + model.GlueHeight.Average() + model.DispGlueZOffset;
                int rtn = glueProject.ExcuteGlueModule.ExecuteGlue(match);
                model.DispensingMoveSpeed = (float)match.Velocity;
                model.DispensingHeight = (float)match.ZPosition;
                model.CCDGluePosition = (float)tempX;
                if (rtn != 0)
                {
                    curesult.ProNGCode = MotionNGCode;

                    PrintLog("点胶失败", System.Drawing.Color.Red);
                    return FCResultType.CASE1;
                }
                else
                {
                    PrintLog("点胶完成", System.Drawing.Color.Red);
                    //PrintLog($"当前点胶高度:{match.ZPosition}mm", System.Drawing.Color.Red);

                    double time = match.CompareDuration;
                    model.DispenseTimeList.Add(time);
                    glueTotalTime += time;
                    Log.log.Write($"当前点胶出胶时间:{time}ms", Color.Black);
                    Log.log.Write($"{PathIndex++}当前点胶高度:{match.ZPosition}mm", Color.Black);

                    //return FCResultType.NEXT;
                }
            }
            curesult.GlueTotalTime = (float)glueTotalTime;
            model.DispenseTotalTime = (float)glueTotalTime;
            JM_GlueModule.Restart();
            return FCResultType.NEXT;
        }

        private FCResultType fc_GlueAuto13_FlowRun(object sender, EventArgs e)
        {
            GlueModuleError = 0;
            if (null == model.GlueCheckResult) model.GlueCheckResult = new List<bool>();
            model.GlueCheckResult.Clear();
            //if (null != DCTcpClient && !DCTcpClient.IsConnected)
            //{
            //    PrintLog($"DCCK TCP连接断开", System.Drawing.Color.Red);
            //    return FCResultType.IDLE;
            //}
            if (null == model.GlueCheckPointOrders || model.GlueCheckPointOrders.Count == 0)
            {
                curesult.ProNGCode = SensorNGCode;

                PrintLog("测高点位为空", Color.Red);
                return FCResultType.IDLE;
            }
            for (int i = 0; i < model.GlueCheckPointOrders.Count; i++)
            {
                PositionInfor point = model.GlueCheckPointOrders[i];
                if (null == point || point.ExtenCommand == null) continue;
                //if (!MoveXYZAxis(point.XPos + _runSlot.Offset.X, point.YPos + _runSlot.Offset.Y, point.ZPos))
                //{
                //    if (JM_GlueModule.On(100000))
                //    {
                //        GlueModuleError = -1;

                //        PrintLog("模组运动失败", System.Drawing.Color.Red);
                //        ShowAlarmInfo(1022);
                //        return FCResultType.IDLE;
                //    }
                //    return FCResultType.IDLE;
                //}
                int irtn = MoveTo(point.XPos + _runSlot.Offset.X, point.YPos + _runSlot.Offset.Y, point.ZPos);
                if (GlueModuleError != 0)
                {
                    curesult.ProNGCode = MotionNGCode;

                    PrintLog($"模组移动失败错误码={irtn}", System.Drawing.Color.Red);
                    Stop();
                    return FCResultType.IDLE;
                }
                byte[] dataRev = new byte[1024];
                //通知德创视觉Location任务
                string mLocationBuffer = AlgLastContent;
                point.ExtenCommand.DownLoad();
                point.ExtenCommand.SN = curesult.ProductSN;
                String strCommand = !string.IsNullOrEmpty(point.StrCommand) ? point.StrCommand : JsonConvert.SerializeObject(point.ExtenCommand);

                if (string.IsNullOrEmpty(strCommand)) continue;
                int reSendNum = 0;
            ReSendCommand:
                DCTcpClient.Send(strCommand);
                mLocationBuffer = string.Empty;
                int iretry = 0;
                while (string.IsNullOrEmpty(mLocationBuffer) && iretry < 50)
                {
                    System.Threading.Thread.Sleep(100);
                    mLocationBuffer = AlgLastContent;
                    iretry++;
                }
                if (string.IsNullOrEmpty(mLocationBuffer))
                {
                    if (DCTcpClient == null || !DCTcpClient.IsConnected)
                    {
                        curesult.ProNGCode = VisionNGCode;

                        PrintLog($"DCCK TCP连接断开", System.Drawing.Color.Red);
                        return FCResultType.CASE1;
                    }
                    reSendNum++;
                    if (reSendNum > 3) return FCResultType.CASE1;
                    goto ReSendCommand;
                }
                ReceiveData glueDisCheck = JsonConvert.DeserializeObject<ReceiveData>(mLocationBuffer);
                if (null == glueDisCheck || glueDisCheck.ResultList == null || glueDisCheck.ResultList.Count < 4)
                {
                    curesult.ProNGCode = VisionNGCode;

                    PrintLog($"视觉未发送点胶检测结果", System.Drawing.Color.Red);
                    return FCResultType.CASE1;
                }
                if (point.Result != null)
                {
                    point.Result.Assign(glueDisCheck);
                    point.Result.UpLoads();
                }
                foreach (var item in glueDisCheck.ResultList)
                {
                    var match = model.VisionResult.FirstOrDefault(temp => { return temp.Name == item.Name; });
                    if (null == match)
                    {
                        match = item;
                        model.VisionResult.Add(item);
                    }
                    match.Value = item.Name;
                    match.UpLoads();
                }
                curesult.ResultDetail = (bool)glueDisCheck.ResultList[3].Value ? "OK" : "NG";
                curesult.AlgImgPath = glueDisCheck.ResultList[0].Value.ToString();

                model.CCDGlueImagePath = curesult.AlgImgPath;
                model.CCDGlueArea = (float)6;
                if (glueDisCheck.Result == 1 && (bool)glueDisCheck.ResultList[3].Value)
                {
                    model.GlueCheckResult.Add(true);
                    continue;
                }
                model.GlueCheckResult.Add(false);
            }
            return FCResultType.NEXT;
        }

        private FCResultType fc_GlueAuto16_FlowRun(object sender, EventArgs e)
        {
            if (model.GlueCheckResult.Contains(false) || !model.ParentProject.IsContainUV)
                return FCResultType.CASE1;
            return FCResultType.NEXT;
        }

        private FCResultType fc_GlueAuto18_FlowRun(object sender, EventArgs e)
        {
            //if (!MoveXYZAxis(glueProject.UVPos.XPos + _runSlot.Offset.X, glueProject.UVPos.YPos + _runSlot.Offset.Y, glueProject.UVPos.ZPos, true))
            //{
            //    if (JM_GlueModule.On(100000))
            //    {
            //        GlueModuleError = -1;

            //        PrintLog("模组运动失败", System.Drawing.Color.Red);
            //        ShowAlarmInfo(1022);
            //        return FCResultType.IDLE;
            //    }
            //    return FCResultType.IDLE;
            //}
            //else
            //{
            //    JM_GlueModule.Restart();
            //    return FCResultType.NEXT;
            //}
            var rtn = MoveTo(glueProject.UVPos.XPos + _runSlot.Offset.X, glueProject.UVPos.YPos + _runSlot.Offset.Y, glueProject.UVPos.ZPos, true);
            if (0 == rtn)
            {
                JM_GlueModule.Restart();
                return FCResultType.NEXT;
            }
            else
            {
                curesult.ProNGCode = MotionNGCode;

                ShowAlarmInfo(1022);
                return FCResultType.CASE1;
            }

        }

        private FCResultType fc_GlueAuto19_FlowRun(object sender, EventArgs e)
        {
            return FCResultType.NEXT;
        }
        /// <summary>
        /// UV气缸下降
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        private FCResultType fc_GlueAuto20_FlowRun(object sender, EventArgs e)
        {
            if (!glueProject.IsContainUV) return FCResultType.IDLE;
            if (glueProject.UVLightDown())
            {
                return FCResultType.NEXT;
            }
            else
            {
                curesult.ProNGCode = MotionNGCode;

                ShowAlarmInfo(1035);
                PrintLog("UV气缸动作超时", Color.Red);
                return FCResultType.CASE1;
            }
        }

        private FCResultType fc_GlueAuto21_FlowRun(object sender, EventArgs e)
        {
            if (UVTcp == null || !UVTcp.Connected) return FCResultType.NEXT;
            //设置光照时间
            UVTcp.UVLight_SetTime(1, (double)glueProject.FirstUVTime);
            //设置功率10%-100%
            UVTcp.UVLight_SetPowerLevel(1, (int)glueProject.FirstUVPower);
            //设置功率范围
            UVTcp.UVLight_SetOpticalPowerRange(1, 0, 20000);
            //打开通道1 UV灯
            bool bOpen = UVTcp.UVLight_TurnOn(1);
            if (!bOpen && Debugger.IsAttached)
            {
                Debugger.Break();
            }
            int sleepTime = (int)(glueProject.FirstUVTime * 1000) - 1000;
            sleepTime = sleepTime > 0 ? sleepTime : 10;
            Thread.Sleep(sleepTime);
            //读通道1 UV灯功率
            int nReadPower = UVTcp.UVLight_GetOpticalPower(1);
            Log.log.Write($"UV灯功率:{nReadPower}", Color.Black);
            //读通道1 UV灯强度
            int nReadStrength = UVTcp.UVLight_GetStrength(1);
            Log.log.Write($"UV灯强度:{nReadStrength}", Color.Black);
            //读通道1 UV灯温度（光头温度）
            int nReadTemperature = UVTcp.UVLight_GetTemperature(1);
            Log.log.Write($"UV灯温度（光头温度）:{nReadTemperature}", Color.Black);
            //读取通道1光照时间
            curesult.FirstUVTime = UVTcp.UVLight_GetTime(1);
            Log.log.Write($"UV灯光照时间:{curesult.FirstUVTime}", Color.Black);
            curesult.FirstUVPower = (float)nReadStrength;
            DataModel.UVCurUseTime = curesult.FirstUVTime;
            DataModel.UVCuringLightPower = (float)nReadPower;
            //关闭通道1 UV灯
            //UVTcp.UVLight_TurnOff(1);
            Stopwatch sw = Stopwatch.StartNew();
            while (UVTcp.UVLight_GetUVState(1) == 1 && sw.ElapsedMilliseconds < 2000)
            {
                Thread.Sleep(50);
            }
            if (1 == UVTcp.UVLight_GetUVState(1))
            {
                curesult.ProNGCode = MotionNGCode;

                Log.log.Write("UV灯未及时关闭，请确认UV灯状态是否为自动", Color.Red);
            }
            return FCResultType.NEXT;
        }
        /// <summary>
        /// uv灯抬起
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <returns></returns>
        private FCResultType fc_GlueAuto22_FlowRun(object sender, EventArgs e)
        {
            if (glueProject == null || !glueProject.IsContainUV) return FCResultType.IDLE;
            if (glueProject.UVLightUp())
            {
                return FCResultType.NEXT;
            }
            else
            {
                curesult.ProNGCode = MotionNGCode;

                GlueModuleError = 2072;
                PrintLog("UV气缸动作超时", Color.Red);
                return FCResultType.CASE1;
            }
        }

        private FCResultType fc_GlueAuto23_FlowRun(object sender, EventArgs e)
        {
            //if (!MoveXYZAxis(model.AutoRunSafetyPos.XPos, model.AutoRunSafetyPos.YPos, model.AutoRunSafetyPos.ZPos, true))
            //{
            //    if (JM_GlueModule.On(100000))
            //    {
            //        GlueModuleError = -1;

            //        PrintLog("模组运动失败", System.Drawing.Color.Red);
            //        ShowAlarmInfo(1022);
            //        return FCResultType.IDLE;
            //    }
            //    return FCResultType.IDLE;
            //}
            //else
            //{
            //    return FCResultType.NEXT;
            //}
            var rtn = MoveTo(model.AutoRunSafetyPos.XPos, model.AutoRunSafetyPos.YPos, model.AutoRunSafetyPos.ZPos, true);
            if (rtn != 0)
            {
                curesult.ProNGCode = MotionNGCode;

                PrintLog($"模组移动失败,错误码={rtn}", System.Drawing.Color.Red);
                return FCResultType.IDLE;
            }
            else
            {
                return FCResultType.NEXT;
            }
        }


        private FCResultType fc_GlueAuto25_FlowRun(object sender, EventArgs e)
        {
            bool bres = false;
            if (!model.GlueCheckResult.Contains(false) && model.GlueCheckResult.Contains(true))
            {
                bres = true;
                PrintLog($"模组号:{StageIndex}+点胶结果OK", System.Drawing.Color.Green);
            }
            else
            {
                PrintLog($"模组号:{StageIndex}点胶结果NG", System.Drawing.Color.Red);
            }
            if (!_runSlot.WaitRuningReset(bres))
            {
                PrintLog($"模组号:{StageIndex}PLC没有复位点胶{StageIndex}点胶中信号为false,检查PLC程序,或者程序的DB块有没有配置正确", System.Drawing.Color.Red);
            }
            string path = @"D:\WorkLog\";
            if (StageIndex == 1) //左点胶
            {
                path += "LeftGlue";
                model.ScrewRotationSpeed = (float)250;
            }
            else if (StageIndex == 2) //右点胶
            {
                path += "RightGlue";
                model.ScrewRotationSpeed = (float)300;
            }
            else
            {
                path += "Glue";
            }
            if (!model.GlueCheckResult.Contains(false))
            {
                if (_runSlot.Signal_W_GlueFinishedNG == false && _runSlot.Signal_W_GlueFinishedOK == true)
                {
                    _runSlot.Signal_W_GlueFinished = true;
                    _runSlot.StartButSignal = false;
                    bool flag = model.ResetStartSignal();
                    if (_runSlot.Signal_W_GlueFinished == true && _runSlot.StartButSignal == false && flag == true)
                    {
                        _runSlot.Signal_W_ReceiveProgram = false;

                        model.ParentProject.UpProcessAddre = 0;
                        PrintLog("点胶结果OK", System.Drawing.Color.Green);
                        Thread.Sleep(500);
                        System.Windows.Application.Current.Dispatcher.Invoke(() => { model.DisGlueResults.Add(curesult); });
                        Helper.FileOperate.SaveResult(curesult, path);
                        curesult.Arquments = model.RunResult.Arquments;
                        curesult.UpLoads();
                        model.ParentProject.UpProcessAddre = 1;


                        //model.CCDGlueArea = 1;
                        //model.OverflowGlueArea = 1;
                        //model.DispensingMoveSpeed = 1;
                        //model.CCDGluePosition = 1;
                        //model.WeightActualValue = 1;


                        //glueProject.PLC_StandbyPosition = true;
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        return FCResultType.IDLE;
                    }
                }
                else
                {
                    return FCResultType.IDLE;
                }
            }
            else
            {
                if (_runSlot.Signal_W_GlueFinishedNG == true && _runSlot.Signal_W_GlueFinishedOK == false)
                {
                    _runSlot.Signal_W_GlueFinished = true;
                    _runSlot.StartButSignal = false;
                    bool flag = model.ResetStartSignal();
                    if (_runSlot.Signal_W_GlueFinished == true && _runSlot.StartButSignal == false && flag == true)
                    {
                        _runSlot.Signal_W_ReceiveProgram = false;
                        model.ParentProject.UpProcessAddre = 0;
                        PrintLog("点胶结果NG", System.Drawing.Color.Red);
                        Thread.Sleep(500);
                        System.Windows.Application.Current.Dispatcher.Invoke(() => { model.DisGlueResults.Add(curesult); });
                        Helper.FileOperate.SaveResult(curesult, path);
                        curesult.Arquments = model.RunResult.Arquments;
                        curesult.UpLoads();
                        model.ParentProject.UpProcessAddre = 0;
                        //glueProject.PLC_StandbyPosition = true;
                        return FCResultType.NEXT;
                    }
                    else
                    {
                        return FCResultType.IDLE;
                    }
                }
                else
                {
                    return FCResultType.IDLE;
                }
            }
        }

        /// <summary>
        /// 算法客户端接受信息事件
        /// </summary>
        /// <remarks>
        /// 可以重新该事件，在里面做自己想要的事件处理
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void uV_DisConnected(object sender, ConnectionEventArgs e)
        {
            if (e == null) return;
            Debug.WriteLine($"Port={e.IpPort}+disConnected={e.Reason}");
        }

        private void uvDataReceived(object sender, SuperSimpleTcp.DataReceivedEventArgs e)
        {
            if (e.Data == null) return;
            string strRec = Encoding.UTF8.GetString(e.Data.Array);
            Debug.WriteLine($"Port={e.IpPort}+Data={strRec}");
            uvLastContent = strRec;
        }
        private FCResultType fc_GlueAuto26_FlowRun(object sender, EventArgs e)
        {
            if (!UVTcp.Connected)
                UVTcp.Connect(glueProject.StrUVIP, glueProject.UVPortNum);
            Thread.Sleep(400);
            if (UVTcp.Connected)
            {
                PrintLog("UV灯TCP连接成功", Color.Black);

                JM_GlueModule.Restart();
                uvLightStatus = false;
                return FCResultType.NEXT;
            }
            else
            {
                curesult.ProNGCode = MotionNGCode;

                PrintLog("UV灯TCP连接失败", Color.Red);
                ShowAlarmInfo(1013);
                return FCResultType.CASE1;
            }
        }

        private FCResultType exptionFC_FlowRun(object sender, EventArgs e)
        {
            //从配置表里拿数据进行异常处理
            GloMotionParame.WriteErrCode(StageIndex, (short)GlueModuleError);
            model.GlueCheckResult.Add(false);
            //_runSlot.Signal_W_GlueFinished = true;
            //_runSlot.Signal_W_GlueFinishedNG = true;
            //_runSlot.Signal_W_GlueFinishedOK = false;
            //_runSlot.Signal_W_ReceiveProgram = false;
            //model.ParentProject.UpProcessAddre = 0;
            return FCResultType.NEXT;
        }
        #endregion 私有函数

        /// <summary>
        /// 点胶对象
        /// </summary>

        private JTimer JM_GlueModule = new JTimer();
        private int GlueModuleError = 0;
        private string uvLastContent = string.Empty;
        bool uvLightStatus = false;
        private List<GlueBase> glueStrokeParames = null;
        /// <summary>
        /// uv灯
        /// </summary>
        private readonly UVLight_ModbusTcp UVTcp = new UVLight_ModbusTcp();
        private SlotSignalInfor _runSlot = null;
        private GlueProject glueProject = null;
        private ResultInfor curesult = null;
        Task MotorTask;
        int MotorTaskRtn = 0;
        bool isMove = false;
        double glueTotalTime = 0;
        private string VisionNGCode = 1001.ToString();//MES上的ngcode
        private string SensorNGCode = 2001.ToString();
        private string MotionNGCode = 3001.ToString();


        private FCResultType UVup_FlowRun(object sender, EventArgs e)
        {
            if (glueProject == null) return FCResultType.IDLE;
            if (!glueProject.IsContainUV || glueProject.UVLightUp())
            {
                AlgLastContent = String.Empty;
                JM_GlueModule.Restart();
                return FCResultType.NEXT;
            }
            else
            {
                curesult.ProNGCode = MotionNGCode;
                ShowAlarmInfo(1034);
                return FCResultType.CASE1;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //glueProject = model.ParentProject as GlueProject;
            //if (!UVTcp.Connected)
            //    UVTcp.Connect(glueProject.StrUVIP, glueProject.UVPortNum);
            //UVTcp.UVLight_SetTime(1, (double)15);
            ////设置功率10%-100%
            //UVTcp.UVLight_SetPowerLevel(1, (int)20);
            ////设置功率范围
            //UVTcp.UVLight_SetOpticalPowerRange(1, 0, 20000);
            //uvLightStatus = UVTcp.UVLight_TurnOn(1);

        }


        #region 轴移动
        //private int MoveTo(double X, double Y, double Z, bool IsSafety = true)
        //{
        //    double safetyPos = 0;
        //    if (IsSafety)
        //    {
        //        var rtn = glueProject.ExcuteGlueModule.ExecuteZMoveTo(safetyPos, model.GlueZAxisSpd, model.GlueZAxisAcc);
        //        if (rtn == 0)
        //        {
        //            var rtnXY = glueProject.ExcuteGlueModule.ExecuteXYMoveTo(X, Y, model.GlueXYAxisSpd, model.GlueXYAxisAcc);
        //            if (0 == rtnXY)
        //            {
        //                return glueProject.ExcuteGlueModule.ExecuteZMoveTo(Z, model.GlueZAxisSpd, model.GlueZAxisAcc);
        //            }
        //            else
        //                return -1;
        //        }
        //        else
        //            return -1;
        //    }
        //    else
        //    {
        //        var rtn = glueProject.ExcuteGlueModule.ExecuteXYMoveTo(X, Y, model.GlueXYAxisSpd, model.GlueXYAxisAcc);
        //        if (0 == rtn)
        //        {
        //            return glueProject.ExcuteGlueModule.ExecuteZMoveTo(Z, model.GlueZAxisSpd, model.GlueZAxisAcc);
        //        }
        //        else
        //            return -1;

        //    }
        //}



        private bool MoveXYZAxis(double X, double Y, double Z, bool IsSafety = true)
        {
            double curPosX = 0;
            double curPosY = 0;
            double curPosZ = 0;
            glueProject.ExcuteGlueModule.GetPosition(out curPosX, out curPosY, out curPosZ);

            if (!isMove)
            {
                MotorTask = Task.Run(() =>
                {
                    var rtn = MoveTo(X, Y, Z, IsSafety);
                    MotorTaskRtn = rtn;
                });
                isMove = true;
                return false;
            }
            else
            {
                if (null != MotorTask && MotorTask.IsCompleted && curPosX == X && curPosY == Y && curPosZ == Z)
                {
                    if (MotorTaskRtn == 0)
                    {
                        isMove = false;
                        return true;
                    }
                    else
                    {
                        Log.log.Write("组装模组执行Move失败", Color.Red);
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion
    }

    public class WPoint
    {
        public WPoint()
        {
        }
        public WPoint(double temx, double temy)
        {

            X = temx;
            Y = temy;
        }
        public double X { get; set; }
        public double Y { get; set; }
    }

    public class GlueBase { }

    /// <summary>
    /// 单点路径
    /// </summary>
    public class GlueStrokeParame : GlueBase
    {

        public WPoint StartPoint { get; set; }
    }

    /// <summary>
    /// 线段
    /// </summary>
    public class GlueLineSegment : GlueBase
    {
        public WPoint StartPoint { get; set; }
        public WPoint EndPoint { get; set; }
    }


    /// <summary>
    /// 多线段連接
    /// </summary>
    public class GluePolyLine : GlueBase
    {
        public WPoint StartPoint { get; set; }
        public List<WPoint> EndPoint { get; set; }
    }
}
