﻿using G4.GlueCore;
using G4.Motion;
using LogSv;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using YHSDK;

namespace ModuleData.Views
{
    [Serializable]
    public partial class uc_GlueExecuteTapeClean : ModuleBase
    {
        public uc_GlueExecuteTapeClean()
        {
            InitializeComponent();
        }

        private JTimer JM_GlueModule = new JTimer();
        private int GlueModuleTaskRtn = 0;
        private Task GlueModuleTask = null;
        private bool bResult = false;
        #region 公有属性
        public ClearNeedleModel DataModel
        {
            get { return model; }
            set { model = value; OnPropertyChanged(); }
        }
        private ClearNeedleModel model = new ClearNeedleModel();

        /// <summary>
        /// 自动对针的结果
        /// </summary>
        [Browsable(false)]
        public bool AutoNeedelResult
        {
            get { return gutoNeedelResult; }
            private set { gutoNeedelResult = value; OnPropertyChanged(); }
        }
        bool gutoNeedelResult = false;

        /// <summary>
        /// 对针中心的X
        /// </summary>
        [Browsable(false)]
        public double ExecuteAlignNeedle_X
        {
            get { return executeAlignNeedle_X; }
            private set { executeAlignNeedle_X = value; OnPropertyChanged(); }
        }
        double executeAlignNeedle_X = 0;
        /// <summary>
        /// 对针中心的Y
        /// </summary>
        [Browsable(false)]
        public double ExecuteAlignNeedle_Y
        {
            get { return executeAlignNeedle_Y; }
            private set { executeAlignNeedle_Y = value; OnPropertyChanged(); }
        }
        double executeAlignNeedle_Y = 0;
        /// <summary>
        /// 对针中心的Z
        /// </summary>
        [Browsable(false)]
        public double ExecuteAlignNeedle_Z
        {
            get { return executeAlignNeedle_Z; }
            private set { executeAlignNeedle_Z = value; OnPropertyChanged(); }
        }
        double executeAlignNeedle_Z = 0;

        #endregion 公有属性
        #region 公有函数
        protected override void Run()
        {
            if (null == model) return;
            if (fc_GlueAuto1.WorkFlow == null)
                fc_GlueAuto1.TaskReset();
            fc_GlueAuto1.TaskRun();
            //if (!IsWaitForSignal)
            //{
            //    model.ParentProject.PLC_StandbyPosition = false;
            //}
        }

        public override void ResetChart()
        {
            StartChart = fc_GlueAuto1;
            fc_GlueAuto1.TaskReset();
        }
        private int MoveTo(double X, double Y, double Z, bool IsSafety = true)
        {
            double safetyPos = 0;
            if (IsSafety)
            {
                var rtn = glueModule.ExecuteZMoveTo(safetyPos, 20, 20);
                if (rtn == 0)
                {
                    var rtnXY = glueModule.ExecuteXYMoveTo(X, Y, 20, 20);
                    if (0 == rtnXY)
                    {
                        return glueModule.ExecuteZMoveTo(Z, 20, 20);
                    }
                    else
                        return -1;
                }
                else
                    return -1;
            }
            else
            {
                var rtn = glueModule.ExecuteXYMoveTo(X, Y, 20, 20);
                if (0 == rtn)
                {
                    return glueModule.ExecuteZMoveTo(Z, 20, 20);
                }
                else
                    return -1;

            }
        }

        #endregion 公有函数

        private YHSDK.FCResultType fc_Glueflow1_FlowRun(object sender, EventArgs e)
        {
            GlueProject project = model.ParentProject as GlueProject;
            if (null == project || project.ExcuteGlueModule == null) return FCResultType.IDLE;
            glueModule = project.ExcuteGlueModule;
            //model.ParentProject.PLC_StandbyPosition = true;
            bool bret = model.Signal_R_GuleClear;
            //Debug.WriteLine(model.ParentProject.AxisWorkModule.ToString());
            if (bret)
            {
                model.Signal_R_GuleClear = bret;
                model.Signal_W_GuleClearOK = model.Signal_W_GuleClearNG = false;
                model.Signal_W_GuleClearRun = true;
                PrintLog(string.Format($"接收到擦针指令，开始擦针任务"), Color.Black);

                model.ParentProject.UpProcessAddre = model.ParentProject.ProcessAddre;
                MotionModel.Instance.SetDo(model.ResetCleanGlueAlarm, true);            //擦针报警复位清除
                return FCResultType.NEXT;
            }
            return FCResultType.IDLE;
        }

        private FCResultType fc_GlueAuto22_FlowRun(object sender, EventArgs e)
        {
            GlueProject project = model.ParentProject as GlueProject;
            if (project == null) return FCResultType.IDLE;
            if (!project.IsContainUV) return FCResultType.NEXT;
            else if (project.UVLightUp())
            {
                return FCResultType.NEXT;
            }
            else
            {
                return FCResultType.IDLE;
            }
        }

        private FCResultType fc_GlueAuto2_FlowRun(object sender, EventArgs e)
        {
            MotionModel.Instance.SetDo(model.ResetCleanGlueAlarm, false);            //擦针报警复位清除
            var rtn = glueModule.ExecuteTapeClean();
            if (rtn == 0)
            {
                bResult = true;
                PrintLog(string.Format($"擦胶任务运行成功"), Color.Black);
            }
            else
            {
                bResult = false;
                PrintLog(string.Format($"擦胶任务运行失败"), Color.Red);
            }
            return FCResultType.NEXT;
        }

        private FCResultType fc_GlueAuto3_FlowRun(object sender, EventArgs e)
        {
            var rtn = MoveTo(model.ParentProject.GlueSafetyPos.XPos, model.ParentProject.GlueSafetyPos.YPos, model.ParentProject.GlueSafetyPos.ZPos, true);
            if (rtn == 0)
            {
                return FCResultType.NEXT;
            }
            else
            {
                PrintLog("点胶模组运动到安全位失败", Color.Red);
                return FCResultType.IDLE;
            }



        }

        private FCResultType fc_GlueAuto4_FlowRun(object sender, EventArgs e)
        {
            model.Signal_W_GuleClearOK = bResult;
            model.Signal_W_GuleClearNG = !bResult;
            Thread.Sleep(200);
            if (model.Signal_W_GuleClearOK == bResult && model.Signal_W_GuleClearNG == !bResult)
            {
                model.Signal_W_GuleClearFinish = true;
                Thread.Sleep(200);
                if (model.Signal_W_GuleClearFinish == true)
                {
                    model.Ref_W_GuleClearNum += 1;
                    Thread.Sleep(1000);
                    model.ParentProject.UpProcessAddre = 0;
                    return FCResultType.NEXT;
                }
                else
                {
                    return FCResultType.IDLE;
                }
            }
            else
            {
                return FCResultType.IDLE;
            }
        }

        private GlueModule glueModule;
    }
}
