﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ModuleData
{
    /// <summary>
    /// UCProjNormalPa.xaml 的交互逻辑
    /// </summary>
    public partial class UCProjNormalPa : UserControl
    {
        public UCProjNormalPa()
        {
            InitializeComponent();
        }

        public static DependencyProperty SubProjectProperty = DependencyProperty.Register("SubProject", typeof(GlueProject), typeof(UCProjNormalPa), new FrameworkPropertyMetadata(new GlueProject(), FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public GlueProject SubProject
        {
            get { return (GlueProject)GetValue(SubProjectProperty); }
            set { SetValue(SubProjectProperty, value); }
        }
    }
}
