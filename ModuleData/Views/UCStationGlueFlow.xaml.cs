﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Serialization;
using SuperSimpleTcp;
using Infrastructure;
using YHSDK;
using LogSv;
using ShowAlarm;
using System.Threading;
using Newtonsoft.Json.Linq;

namespace ModuleData.Views
{
    /// <summary>
    /// UCStationGlueFlow.xaml 的交互逻辑
    /// </summary>
    public partial class UCStationGlueFlow : UserControl, IModule
    {
        public UCStationGlueFlow()
        {
            InitializeComponent();
        }
        public static DependencyProperty DCClientProperty =
            DependencyProperty.Register("DCClient", typeof(SimpleTcpClient), typeof(UCStationGlueFlow), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));

        public static DependencyProperty HeightSenClientProperty =
    DependencyProperty.Register("HeightSenClient", typeof(SimpleTcpClient), typeof(UCStationGlueFlow), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));

        public static DependencyProperty ExcuteProjectProperty =
DependencyProperty.Register("ExcuteProject", typeof(GlueProject), typeof(UCStationGlueFlow), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));

        public static DependencyProperty StageIndexProperty =
DependencyProperty.Register("StageIndex", typeof(int), typeof(UCStationGlueFlow), new FrameworkPropertyMetadata(1, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));
        public static DependencyProperty AlarmFrmProperty =
DependencyProperty.Register("AlarmFrm", typeof(ShowAlarm.WinShowAlarm), typeof(UCStationGlueFlow), new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));

        public static DependencyProperty IsLockParamProperty =
DependencyProperty.Register("IsLockParam", typeof(bool), typeof(UCStationGlueFlow), new FrameworkPropertyMetadata(false, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, DepenProChanged));

        /// <summary>
        /// 德创TCP对象
        /// </summary>
        /// <remarks>
        /// TCP对象，到最外层直接进行绑定
        /// </remarks>
        public SimpleTcpClient DCClient
        {
            get { return (SimpleTcpClient)GetValue(DCClientProperty); }
            set { SetValue(DCClientProperty, value); }
        }



        /// <summary>
        /// 测高传感器TCP对象
        /// </summary>
        /// <remarks>
        /// TCP对象，到最外层直接进行绑定
        /// </remarks>
        public SimpleTcpClient HeightSenClient
        {
            get { return (SimpleTcpClient)GetValue(HeightSenClientProperty); }
            set { SetValue(HeightSenClientProperty, value); }
        }


        public GlueProject ExcuteProject
        {
            get { return (GlueProject)GetValue(ExcuteProjectProperty); }
            set { SetValue(ExcuteProjectProperty, value); }
        }

        public int StageIndex
        {
            get { return (int)GetValue(StageIndexProperty); }
            set { SetValue(StageIndexProperty, value); }
        }

        public bool IsLockParam
        {
            get { return (bool)GetValue(IsLockParamProperty); }
            set { SetValue(IsLockParamProperty, value); }
        }
        /// <summary>
        /// 报警提示窗
        /// </summary>
        public ShowAlarm.WinShowAlarm AlarmFrm
        {
            get { return (ShowAlarm.WinShowAlarm)GetValue(AlarmFrmProperty); }
            set { SetValue(AlarmFrmProperty, value); }
        }

        private static void DepenProChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            UCStationGlueFlow flow = d as UCStationGlueFlow;
            if (null == flow) return;
            if (e.Property == DCClientProperty)
            {
                SimpleTcpClient client = e.NewValue as SimpleTcpClient;
                if (null == client) return;
                flow.glue.DCTcpClient = client;
                flow.needle.DCTcpClient = client;
                flow.cle.DCTcpClient = client;
                flow.drop.DCTcpClient = client;
                flow.cal.DCTcpClient = client;
                flow.weight.DCTcpClient = client;
                flow.spotcheck.DCTcpClient= client; 

            }
            else if (e.Property == HeightSenClientProperty)
            {
                SimpleTcpClient client = e.NewValue as SimpleTcpClient;
                if (null == client) return;
                flow.glue.SensorClient = client;
                flow.spotcheck.SensorClient = client;
            }
            else if (e.Property == ExcuteProjectProperty)
            {
                GlueProject tempPro = e.NewValue as GlueProject;
                if (null == tempPro) return;
                flow.glue.DataModel = tempPro.GlueExcuteModel;
                flow.needle.DataModel = tempPro.AutoCalibra;
                flow.cle.DataModel = tempPro.ClearMode;
                flow.drop.DataModel = tempPro.DropData;
                flow.cal.DataModel = tempPro.GlueExcuteModel;
                flow.weight.DataModel = tempPro.WeightModel;
                flow.spotcheck.DataModel = tempPro.SpotCheckModel;
            }
            else if (e.Property == StageIndexProperty)
            {
                int tempIndex = (int)e.NewValue;
                flow.glue.StageIndex = tempIndex;
                flow.needle.StageIndex = tempIndex;
                flow.cle.StageIndex = tempIndex;
                flow.drop.StageIndex = tempIndex;
                flow.cal.StageIndex = tempIndex;
                flow.weight.StageIndex = tempIndex;
                flow.spotcheck.StageIndex = tempIndex;
            }
            else if (e.Property == AlarmFrmProperty)
            {
                WinShowAlarm tempFrm = (WinShowAlarm)e.NewValue;
                flow.glue.AlarmFrm = tempFrm;
                //flow.needle.AlarmFrm = tempFrm;
                //flow.cle.AlarmFrm = tempFrm;
                //flow.drop.AlarmFrm = tempFrm;
                //flow.cal.AlarmFrm = tempFrm;
                //flow.weight.AlarmFrm = tempFrm;
            }
            else if (e.Property == IsLockParamProperty)
            {
                bool tempIsLock = (bool)e.NewValue;
                flow.glue.IsLockParam = tempIsLock;
            }
        }
        private bool isScan = false;
        private Thread thread_RefreshUI;
        object obj = new object();
        public bool IsRun
        {
            get
            {
                if (null == task || task.IsCompleted) return false;
                return glue.IsRun || needle.IsRun || cle.IsRun || drop.IsRun || cal.IsRun || weight.IsRun || spotcheck.IsRun;
            }
        }
        void RefreshUI()
        {
            if (!isScan)
            {
                isScan = true;
                GlueProject tem = ExcuteProject;
                if (tem == null) return;
                if (thread_RefreshUI != null)
                {
                    if (thread_RefreshUI.IsAlive)
                        thread_RefreshUI.Abort();
                }
                thread_RefreshUI = new Thread(new ThreadStart(() =>
                {
                    while (true)
                    {
                        tem.StationStatus.IsDispGlue = glue.IsRun && !glue.IsWaitForSignal;
                        tem.StationStatus.IsCaliNeedle = needle.IsRun && !needle.IsWaitForSignal;
                        tem.StationStatus.IsWeightRun = weight.IsRun && !weight.IsWaitForSignal;
                        tem.StationStatus.IsDropGlueRun = drop.IsRun && !drop.IsWaitForSignal;
                        tem.StationStatus.IsTapeClean = cle.IsRun && !cle.IsWaitForSignal;
                        tem.StationStatus.IsSpotCheck = spotcheck.IsRun && !spotcheck.IsWaitForSignal;
                        if (tem.StationStatus.IsDispGlue || tem.StationStatus.IsCaliNeedle || tem.StationStatus.IsWeightRun
                        || tem.StationStatus.IsDropGlueRun || tem.StationStatus.IsTapeClean || tem.StationStatus.IsSpotCheck)
                        {
                            tem.GlueStandbyFlag = false;
                        }
                        else
                        {
                            tem.GlueStandbyFlag = true;
                        }
                        //if(glue.IsRun)
                        System.Threading.Thread.Sleep(200);
                    }
                }));
                thread_RefreshUI.IsBackground = true;
                thread_RefreshUI.Start();
                //Task.Run(() =>
                //{
                //    isScanStop = false;
                //    while (!isScanStop)
                //    {
                //        tem.StationStatus.IsDispGlue = glue.IsRun && !glue.IsWaitForSignal;
                //        tem.StationStatus.IsCaliNeedle = needle.IsRun && !needle.IsWaitForSignal;
                //        tem.StationStatus.IsWeightRun = weight.IsRun && !weight.IsWaitForSignal;
                //        tem.StationStatus.IsDropGlueRun = drop.IsRun && !drop.IsWaitForSignal;
                //        tem.StationStatus.IsTapeClean = cle.IsRun && !cle.IsWaitForSignal;
                //        //if(glue.IsRun)
                //        System.Threading.Thread.Sleep(200);
                //    }
                //});
            }

        }
        public void RunAsync()
        {
            GlueProject project = ExcuteProject;
            if (null == project) return;
            _IsStop = false;
            task = Task.Run(() =>
             {
                 bool bvalue = false;
                 //project.PLC_StandbyPosition = true;
                 //while (!bvalue)
                 //{
                 //    if (_IsStop) return;
                 //    bvalue = project.Auto_Program;
                 //    System.Threading.Thread.Sleep(200);
                 //}
                 if (_IsStop) return;
                 bvalue = project.Auto_Program;
                 System.Threading.Thread.Sleep(200);
                 project.SoftWareAuto = true;
                 glue.RunAsync();
                 needle.RunAsync();
                 cle.RunAsync();
                 drop.RunAsync();
                 cal.RunAsync();
                 weight.RunAsync();
                 spotcheck.RunAsync();
             });

            RefreshUI();
        }


        public void ResetChart()
        {
            glue.ResetChart();
            needle.ResetChart();
            cle.ResetChart();
            drop.ResetChart();
            cal.ResetChart();
            weight.ResetChart();
            spotcheck.ResetChart();
        }

        public void Stop()
        {
            _IsStop = true;
            ExcuteProject.SoftWareAuto = false;
            ExcuteProject.UpProcessAddre = 0;
            glue.Stop();
            needle.Stop();
            cle.Stop();
            drop.Stop();
            cal.Stop();
            weight.Stop();
            spotcheck.Stop();
        }

        private Task task;
        private bool _IsStop = false;
    }
    public class ModuleRunStatus : NotifyChangedBase
    {
        /// <summary>
        /// 点胶模块是否正在执行
        /// </summary>
        public bool IsDispGlue
        {
            get { return _isDispGlue; }
            set { _isDispGlue = value; OnPropertyChanged(); }
        }
        private bool _isDispGlue = false;

        /// <summary>
        /// 对针模块是否正在执行
        /// </summary>
        public bool IsCaliNeedle
        {
            get { return _sCaliNeedle; }
            set { _sCaliNeedle = value; OnPropertyChanged(); }
        }
        private bool _sCaliNeedle = false;

        /// <summary>
        /// 称重模块是否正在执行
        /// </summary>
        public bool IsWeightRun
        {
            get { return _isWeightRun; }
            set { _isWeightRun = value; OnPropertyChanged(); }
        }
        private bool _isWeightRun = false;
        /// <summary>
        /// 排胶模块是否正在执行
        /// </summary>
        public bool IsDropGlueRun
        {
            get { return _isDropGlueRun; }
            set { _isDropGlueRun = value; OnPropertyChanged(); }
        }
        private bool _isDropGlueRun = false;
        /// <summary>
        /// 擦针模块是否正在执行
        /// </summary>
        public bool IsTapeClean
        {
            get { return _isTapeClean; }
            set { _isTapeClean = value; OnPropertyChanged(); }
        }
        private bool _isTapeClean = false;

        /// <summary>
        /// 点检模块是否正在执行
        /// </summary>
        public bool IsSpotCheck
        {
            get { return _isSpotCheck; }
            set { _isSpotCheck = value; OnPropertyChanged(); }
        }
        private bool _isSpotCheck = false;
        /// <summary>
        /// 初始化模块是否正在执行
        /// </summary>
        public bool IsInitGlueRun
        {
            get { return _isInitGlueRun; }
            set { _isInitGlueRun = value; OnPropertyChanged(); }
        }
        private bool _isInitGlueRun = false;
        /// <summary>
        /// 自动标定模块是否正在执行
        /// </summary>
        public bool IsAutoCabilRun
        {
            get { return _isAutoCabilRun; }
            set { _isAutoCabilRun = value; OnPropertyChanged(); }
        }
        private bool _isAutoCabilRun = false;


    }
}
