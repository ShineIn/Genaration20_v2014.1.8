﻿namespace ModuleData
{
    partial class uc_GlueFlow
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.fc_GlueAuto13 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto16 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto23 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto25 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto1 = new YHSDK.NPFlowChart();
            this.UVup = new YHSDK.NPFlowChart();
            this.exptionFC = new YHSDK.NPFlowChart();
            this.fc_GlueAuto4 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto7 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto10 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto11 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto18 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto19 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto20 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto26 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto21 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto22 = new YHSDK.NPFlowChart();
            this.label2 = new System.Windows.Forms.Label();
            this.txb_GlueReport1 = new System.Windows.Forms.NumericUpDown();
            this.txb_GlueReport2 = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txb_GlueReport1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txb_GlueReport2)).BeginInit();
            this.SuspendLayout();
            // 
            // fc_GlueAuto13
            // 
            this.fc_GlueAuto13.AlarmCode = "";
            this.fc_GlueAuto13.ArrowsWidth = 1;
            this.fc_GlueAuto13.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto13.bClearTrace = false;
            this.fc_GlueAuto13.bIsLastFlowChart = false;
            this.fc_GlueAuto13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto13.bSkip = false;
            this.fc_GlueAuto13.bStepByStepMode = false;
            this.fc_GlueAuto13.CASE1 = this.exptionFC;
            this.fc_GlueAuto13.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto13.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto13.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto13.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.fc_GlueAuto13.CASE2 = null;
            this.fc_GlueAuto13.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto13.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto13.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto13.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto13.CASE3 = null;
            this.fc_GlueAuto13.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto13.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto13.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto13.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto13.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto13.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto13.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto13.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto13.Location = new System.Drawing.Point(497, 417);
            this.fc_GlueAuto13.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto13.menuOpening = false;
            this.fc_GlueAuto13.Name = "fc_GlueAuto13";
            this.fc_GlueAuto13.NEXT = this.fc_GlueAuto16;
            this.fc_GlueAuto13.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto13.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto13.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto13.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto13.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto13.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto13.SubFlowChart = null;
            this.fc_GlueAuto13.TabIndex = 69;
            this.fc_GlueAuto13.Text = "进行胶路检测";
            this.fc_GlueAuto13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto13.TimeOut = 10000;
            this.fc_GlueAuto13.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto13.tmrTimeOut = null;
            this.fc_GlueAuto13.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto13_FlowRun);
            // 
            // fc_GlueAuto16
            // 
            this.fc_GlueAuto16.AlarmCode = "";
            this.fc_GlueAuto16.ArrowsWidth = 1;
            this.fc_GlueAuto16.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto16.bClearTrace = false;
            this.fc_GlueAuto16.bIsLastFlowChart = false;
            this.fc_GlueAuto16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto16.bSkip = false;
            this.fc_GlueAuto16.bStepByStepMode = false;
            this.fc_GlueAuto16.CASE1 = this.fc_GlueAuto23;
            this.fc_GlueAuto16.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto16.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto16.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto16.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto16.CASE2 = null;
            this.fc_GlueAuto16.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto16.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto16.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto16.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto16.CASE3 = null;
            this.fc_GlueAuto16.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto16.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto16.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto16.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto16.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto16.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto16.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto16.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto16.Location = new System.Drawing.Point(497, 371);
            this.fc_GlueAuto16.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto16.menuOpening = false;
            this.fc_GlueAuto16.Name = "fc_GlueAuto16";
            this.fc_GlueAuto16.NEXT = this.fc_GlueAuto18;
            this.fc_GlueAuto16.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto16.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto16.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto16.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto16.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto16.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto16.SubFlowChart = null;
            this.fc_GlueAuto16.TabIndex = 70;
            this.fc_GlueAuto16.Text = "判断结果是否NG以及是否需要固化";
            this.fc_GlueAuto16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto16.TimeOut = 10000;
            this.fc_GlueAuto16.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto16.tmrTimeOut = null;
            this.fc_GlueAuto16.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto16_FlowRun);
            // 
            // fc_GlueAuto23
            // 
            this.fc_GlueAuto23.AlarmCode = "";
            this.fc_GlueAuto23.ArrowsWidth = 1;
            this.fc_GlueAuto23.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto23.bClearTrace = false;
            this.fc_GlueAuto23.bIsLastFlowChart = false;
            this.fc_GlueAuto23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto23.bSkip = false;
            this.fc_GlueAuto23.bStepByStepMode = false;
            this.fc_GlueAuto23.CASE1 = null;
            this.fc_GlueAuto23.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto23.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto23.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto23.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto23.CASE2 = null;
            this.fc_GlueAuto23.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto23.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto23.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto23.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto23.CASE3 = null;
            this.fc_GlueAuto23.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto23.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto23.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto23.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto23.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto23.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto23.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto23.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto23.Location = new System.Drawing.Point(497, 61);
            this.fc_GlueAuto23.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto23.menuOpening = false;
            this.fc_GlueAuto23.Name = "fc_GlueAuto23";
            this.fc_GlueAuto23.NEXT = this.fc_GlueAuto25;
            this.fc_GlueAuto23.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto23.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto23.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto23.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto23.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto23.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto23.SubFlowChart = null;
            this.fc_GlueAuto23.TabIndex = 71;
            this.fc_GlueAuto23.Text = "移动到待机位";
            this.fc_GlueAuto23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto23.TimeOut = 10000;
            this.fc_GlueAuto23.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto23.tmrTimeOut = null;
            this.fc_GlueAuto23.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto23_FlowRun);
            // 
            // fc_GlueAuto25
            // 
            this.fc_GlueAuto25.AlarmCode = "";
            this.fc_GlueAuto25.ArrowsWidth = 1;
            this.fc_GlueAuto25.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto25.bClearTrace = false;
            this.fc_GlueAuto25.bIsLastFlowChart = false;
            this.fc_GlueAuto25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto25.bSkip = false;
            this.fc_GlueAuto25.bStepByStepMode = false;
            this.fc_GlueAuto25.CASE1 = null;
            this.fc_GlueAuto25.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto25.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto25.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto25.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto25.CASE2 = null;
            this.fc_GlueAuto25.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto25.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto25.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto25.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto25.CASE3 = null;
            this.fc_GlueAuto25.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto25.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto25.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto25.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto25.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto25.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto25.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto25.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto25.Location = new System.Drawing.Point(497, 12);
            this.fc_GlueAuto25.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto25.menuOpening = false;
            this.fc_GlueAuto25.Name = "fc_GlueAuto25";
            this.fc_GlueAuto25.NEXT = this.fc_GlueAuto1;
            this.fc_GlueAuto25.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto25.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto25.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto25.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto25.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto25.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto25.SubFlowChart = null;
            this.fc_GlueAuto25.TabIndex = 66;
            this.fc_GlueAuto25.Text = "上传点胶数据";
            this.fc_GlueAuto25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto25.TimeOut = 10000;
            this.fc_GlueAuto25.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto25.tmrTimeOut = null;
            this.fc_GlueAuto25.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto25_FlowRun);
            // 
            // fc_GlueAuto1
            // 
            this.fc_GlueAuto1.AlarmCode = "";
            this.fc_GlueAuto1.ArrowsWidth = 1;
            this.fc_GlueAuto1.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto1.bClearTrace = false;
            this.fc_GlueAuto1.bIsLastFlowChart = false;
            this.fc_GlueAuto1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto1.bSkip = false;
            this.fc_GlueAuto1.bStepByStepMode = false;
            this.fc_GlueAuto1.CASE1 = null;
            this.fc_GlueAuto1.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto1.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.CASE2 = null;
            this.fc_GlueAuto1.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto1.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.CASE3 = null;
            this.fc_GlueAuto1.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto1.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto1.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto1.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto1.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto1.Location = new System.Drawing.Point(87, 12);
            this.fc_GlueAuto1.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto1.menuOpening = false;
            this.fc_GlueAuto1.Name = "fc_GlueAuto1";
            this.fc_GlueAuto1.NEXT = this.UVup;
            this.fc_GlueAuto1.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto1.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto1.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto1.SubFlowChart = null;
            this.fc_GlueAuto1.TabIndex = 55;
            this.fc_GlueAuto1.Text = "等待按钮触发信号";
            this.fc_GlueAuto1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto1.TimeOut = 10000;
            this.fc_GlueAuto1.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto1.tmrTimeOut = null;
            this.fc_GlueAuto1.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto1_FlowRun);
            // 
            // UVup
            // 
            this.UVup.AlarmCode = "";
            this.UVup.ArrowsWidth = 1;
            this.UVup.BackColor = System.Drawing.Color.White;
            this.UVup.bClearTrace = false;
            this.UVup.bIsLastFlowChart = false;
            this.UVup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UVup.bSkip = false;
            this.UVup.bStepByStepMode = false;
            this.UVup.CASE1 = this.exptionFC;
            this.UVup.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.UVup.CASE1Color = System.Drawing.Color.LimeGreen;
            this.UVup.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.UVup.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.UVup.CASE2 = null;
            this.UVup.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.UVup.CASE2Color = System.Drawing.Color.Red;
            this.UVup.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.UVup.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.UVup.CASE3 = null;
            this.UVup.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.UVup.CASE3Color = System.Drawing.Color.Blue;
            this.UVup.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.UVup.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.UVup.DefaultColor = System.Drawing.Color.White;
            this.UVup.eSkipPath = YHSDK.FCResultType.NEXT;
            this.UVup.ExecutedColor = System.Drawing.Color.LightGray;
            this.UVup.ExecutingColor = System.Drawing.Color.Lime;
            this.UVup.Location = new System.Drawing.Point(87, 93);
            this.UVup.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.UVup.menuOpening = false;
            this.UVup.Name = "UVup";
            this.UVup.NEXT = this.fc_GlueAuto4;
            this.UVup.NextArrowType = YHSDK.ArrowTypes.Line;
            this.UVup.NEXTColor = System.Drawing.Color.Black;
            this.UVup.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.UVup.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.UVup.Size = new System.Drawing.Size(217, 29);
            this.UVup.SkipColor = System.Drawing.Color.Yellow;
            this.UVup.SubFlowChart = null;
            this.UVup.TabIndex = 81;
            this.UVup.Text = "UV气缸抬起";
            this.UVup.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.UVup.TimeOut = 10000;
            this.UVup.TimeoutColor = System.Drawing.Color.Red;
            this.UVup.tmrTimeOut = null;
            this.UVup.FlowRun += new YHSDK.FlowRunEvent(this.UVup_FlowRun);
            // 
            // exptionFC
            // 
            this.exptionFC.AlarmCode = "";
            this.exptionFC.ArrowsWidth = 1;
            this.exptionFC.BackColor = System.Drawing.Color.White;
            this.exptionFC.bClearTrace = false;
            this.exptionFC.bIsLastFlowChart = false;
            this.exptionFC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.exptionFC.bSkip = false;
            this.exptionFC.bStepByStepMode = false;
            this.exptionFC.CASE1 = null;
            this.exptionFC.Case1ArrowType = YHSDK.ArrowTypes.Square;
            this.exptionFC.CASE1Color = System.Drawing.Color.LimeGreen;
            this.exptionFC.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.exptionFC.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.exptionFC.CASE2 = null;
            this.exptionFC.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.exptionFC.CASE2Color = System.Drawing.Color.Red;
            this.exptionFC.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.exptionFC.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.exptionFC.CASE3 = null;
            this.exptionFC.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.exptionFC.CASE3Color = System.Drawing.Color.Blue;
            this.exptionFC.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.exptionFC.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.exptionFC.DefaultColor = System.Drawing.Color.White;
            this.exptionFC.eSkipPath = YHSDK.FCResultType.NEXT;
            this.exptionFC.ExecutedColor = System.Drawing.Color.LightGray;
            this.exptionFC.ExecutingColor = System.Drawing.Color.Lime;
            this.exptionFC.Location = new System.Drawing.Point(400, 188);
            this.exptionFC.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.exptionFC.menuOpening = false;
            this.exptionFC.Name = "exptionFC";
            this.exptionFC.NEXT = this.fc_GlueAuto23;
            this.exptionFC.NextArrowType = YHSDK.ArrowTypes.Line;
            this.exptionFC.NEXTColor = System.Drawing.Color.Black;
            this.exptionFC.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.exptionFC.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.exptionFC.Size = new System.Drawing.Size(142, 29);
            this.exptionFC.SkipColor = System.Drawing.Color.Yellow;
            this.exptionFC.SubFlowChart = null;
            this.exptionFC.TabIndex = 73;
            this.exptionFC.Text = "异常处理";
            this.exptionFC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.exptionFC.TimeOut = 10000;
            this.exptionFC.TimeoutColor = System.Drawing.Color.Red;
            this.exptionFC.tmrTimeOut = null;
            this.exptionFC.FlowRun += new YHSDK.FlowRunEvent(this.exptionFC_FlowRun);
            // 
            // fc_GlueAuto4
            // 
            this.fc_GlueAuto4.AlarmCode = "";
            this.fc_GlueAuto4.ArrowsWidth = 1;
            this.fc_GlueAuto4.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto4.bClearTrace = false;
            this.fc_GlueAuto4.bIsLastFlowChart = false;
            this.fc_GlueAuto4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto4.bSkip = false;
            this.fc_GlueAuto4.bStepByStepMode = false;
            this.fc_GlueAuto4.CASE1 = this.exptionFC;
            this.fc_GlueAuto4.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto4.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto4.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.fc_GlueAuto4.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.fc_GlueAuto4.CASE2 = null;
            this.fc_GlueAuto4.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto4.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto4.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.CASE3 = null;
            this.fc_GlueAuto4.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto4.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto4.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto4.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto4.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto4.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto4.Location = new System.Drawing.Point(87, 174);
            this.fc_GlueAuto4.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto4.menuOpening = false;
            this.fc_GlueAuto4.Name = "fc_GlueAuto4";
            this.fc_GlueAuto4.NEXT = this.fc_GlueAuto7;
            this.fc_GlueAuto4.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto4.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto4.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto4.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto4.SubFlowChart = null;
            this.fc_GlueAuto4.TabIndex = 59;
            this.fc_GlueAuto4.Text = "触发德创拍照";
            this.fc_GlueAuto4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto4.TimeOut = 10000;
            this.fc_GlueAuto4.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto4.tmrTimeOut = null;
            this.fc_GlueAuto4.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto4_FlowRun);
            // 
            // fc_GlueAuto7
            // 
            this.fc_GlueAuto7.AlarmCode = "";
            this.fc_GlueAuto7.ArrowsWidth = 1;
            this.fc_GlueAuto7.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto7.bClearTrace = false;
            this.fc_GlueAuto7.bIsLastFlowChart = false;
            this.fc_GlueAuto7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto7.bSkip = false;
            this.fc_GlueAuto7.bStepByStepMode = false;
            this.fc_GlueAuto7.CASE1 = null;
            this.fc_GlueAuto7.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto7.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto7.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.CASE2 = null;
            this.fc_GlueAuto7.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto7.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto7.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.CASE3 = null;
            this.fc_GlueAuto7.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto7.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto7.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto7.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto7.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto7.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto7.Location = new System.Drawing.Point(87, 255);
            this.fc_GlueAuto7.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto7.menuOpening = false;
            this.fc_GlueAuto7.Name = "fc_GlueAuto7";
            this.fc_GlueAuto7.NEXT = this.fc_GlueAuto10;
            this.fc_GlueAuto7.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto7.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto7.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto7.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto7.SubFlowChart = null;
            this.fc_GlueAuto7.TabIndex = 60;
            this.fc_GlueAuto7.Text = "循环列表的点位移动并测高";
            this.fc_GlueAuto7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto7.TimeOut = 10000;
            this.fc_GlueAuto7.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto7.tmrTimeOut = null;
            this.fc_GlueAuto7.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto7_FlowRun);
            // 
            // fc_GlueAuto10
            // 
            this.fc_GlueAuto10.AlarmCode = "";
            this.fc_GlueAuto10.ArrowsWidth = 1;
            this.fc_GlueAuto10.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto10.bClearTrace = false;
            this.fc_GlueAuto10.bIsLastFlowChart = false;
            this.fc_GlueAuto10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto10.bSkip = false;
            this.fc_GlueAuto10.bStepByStepMode = false;
            this.fc_GlueAuto10.CASE1 = this.exptionFC;
            this.fc_GlueAuto10.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto10.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto10.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto10.Case1StartPoint = YHSDK.FlowchartSides.Bottom;
            this.fc_GlueAuto10.CASE2 = null;
            this.fc_GlueAuto10.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto10.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto10.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto10.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto10.CASE3 = null;
            this.fc_GlueAuto10.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto10.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto10.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto10.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto10.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto10.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto10.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto10.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto10.Location = new System.Drawing.Point(87, 337);
            this.fc_GlueAuto10.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto10.menuOpening = false;
            this.fc_GlueAuto10.Name = "fc_GlueAuto10";
            this.fc_GlueAuto10.NEXT = this.fc_GlueAuto11;
            this.fc_GlueAuto10.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto10.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto10.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto10.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto10.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto10.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto10.SubFlowChart = null;
            this.fc_GlueAuto10.TabIndex = 63;
            this.fc_GlueAuto10.Text = "判断测高数据";
            this.fc_GlueAuto10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto10.TimeOut = 10000;
            this.fc_GlueAuto10.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto10.tmrTimeOut = null;
            this.fc_GlueAuto10.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto10_FlowRun);
            // 
            // fc_GlueAuto11
            // 
            this.fc_GlueAuto11.AlarmCode = "";
            this.fc_GlueAuto11.ArrowsWidth = 1;
            this.fc_GlueAuto11.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto11.bClearTrace = false;
            this.fc_GlueAuto11.bIsLastFlowChart = false;
            this.fc_GlueAuto11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto11.bSkip = false;
            this.fc_GlueAuto11.bStepByStepMode = false;
            this.fc_GlueAuto11.CASE1 = null;
            this.fc_GlueAuto11.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto11.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto11.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto11.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto11.CASE2 = null;
            this.fc_GlueAuto11.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto11.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto11.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto11.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto11.CASE3 = null;
            this.fc_GlueAuto11.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto11.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto11.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto11.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto11.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto11.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto11.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto11.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto11.Location = new System.Drawing.Point(87, 417);
            this.fc_GlueAuto11.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto11.menuOpening = false;
            this.fc_GlueAuto11.Name = "fc_GlueAuto11";
            this.fc_GlueAuto11.NEXT = this.fc_GlueAuto13;
            this.fc_GlueAuto11.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto11.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto11.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto11.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto11.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto11.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto11.SubFlowChart = null;
            this.fc_GlueAuto11.TabIndex = 64;
            this.fc_GlueAuto11.Text = "开始点胶";
            this.fc_GlueAuto11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto11.TimeOut = 10000;
            this.fc_GlueAuto11.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto11.tmrTimeOut = null;
            this.fc_GlueAuto11.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto11_FlowRun);
            // 
            // fc_GlueAuto18
            // 
            this.fc_GlueAuto18.AlarmCode = "";
            this.fc_GlueAuto18.ArrowsWidth = 1;
            this.fc_GlueAuto18.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto18.bClearTrace = false;
            this.fc_GlueAuto18.bIsLastFlowChart = false;
            this.fc_GlueAuto18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto18.bSkip = false;
            this.fc_GlueAuto18.bStepByStepMode = false;
            this.fc_GlueAuto18.CASE1 = this.exptionFC;
            this.fc_GlueAuto18.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto18.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto18.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto18.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto18.CASE2 = null;
            this.fc_GlueAuto18.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto18.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto18.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto18.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto18.CASE3 = null;
            this.fc_GlueAuto18.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto18.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto18.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto18.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto18.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto18.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto18.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto18.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto18.Location = new System.Drawing.Point(626, 322);
            this.fc_GlueAuto18.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto18.menuOpening = false;
            this.fc_GlueAuto18.Name = "fc_GlueAuto18";
            this.fc_GlueAuto18.NEXT = this.fc_GlueAuto19;
            this.fc_GlueAuto18.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto18.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto18.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto18.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto18.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto18.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto18.SubFlowChart = null;
            this.fc_GlueAuto18.TabIndex = 78;
            this.fc_GlueAuto18.Text = "移动到固化位";
            this.fc_GlueAuto18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto18.TimeOut = 10000;
            this.fc_GlueAuto18.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto18.tmrTimeOut = null;
            this.fc_GlueAuto18.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto18_FlowRun);
            // 
            // fc_GlueAuto19
            // 
            this.fc_GlueAuto19.AlarmCode = "";
            this.fc_GlueAuto19.ArrowsWidth = 1;
            this.fc_GlueAuto19.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto19.bClearTrace = false;
            this.fc_GlueAuto19.bIsLastFlowChart = false;
            this.fc_GlueAuto19.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto19.bSkip = false;
            this.fc_GlueAuto19.bStepByStepMode = false;
            this.fc_GlueAuto19.CASE1 = null;
            this.fc_GlueAuto19.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto19.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto19.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto19.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto19.CASE2 = null;
            this.fc_GlueAuto19.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto19.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto19.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto19.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto19.CASE3 = null;
            this.fc_GlueAuto19.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto19.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto19.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto19.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto19.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto19.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto19.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto19.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto19.Location = new System.Drawing.Point(626, 279);
            this.fc_GlueAuto19.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto19.menuOpening = false;
            this.fc_GlueAuto19.Name = "fc_GlueAuto19";
            this.fc_GlueAuto19.NEXT = this.fc_GlueAuto20;
            this.fc_GlueAuto19.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto19.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto19.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto19.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto19.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto19.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto19.SubFlowChart = null;
            this.fc_GlueAuto19.TabIndex = 77;
            this.fc_GlueAuto19.Text = "等待到达";
            this.fc_GlueAuto19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto19.TimeOut = 10000;
            this.fc_GlueAuto19.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto19.tmrTimeOut = null;
            this.fc_GlueAuto19.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto19_FlowRun);
            // 
            // fc_GlueAuto20
            // 
            this.fc_GlueAuto20.AlarmCode = "";
            this.fc_GlueAuto20.ArrowsWidth = 1;
            this.fc_GlueAuto20.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto20.bClearTrace = false;
            this.fc_GlueAuto20.bIsLastFlowChart = false;
            this.fc_GlueAuto20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto20.bSkip = false;
            this.fc_GlueAuto20.bStepByStepMode = false;
            this.fc_GlueAuto20.CASE1 = this.exptionFC;
            this.fc_GlueAuto20.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto20.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto20.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto20.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto20.CASE2 = null;
            this.fc_GlueAuto20.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto20.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto20.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto20.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto20.CASE3 = null;
            this.fc_GlueAuto20.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto20.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto20.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto20.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto20.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto20.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto20.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto20.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto20.Location = new System.Drawing.Point(626, 233);
            this.fc_GlueAuto20.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto20.menuOpening = false;
            this.fc_GlueAuto20.Name = "fc_GlueAuto20";
            this.fc_GlueAuto20.NEXT = this.fc_GlueAuto26;
            this.fc_GlueAuto20.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto20.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto20.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto20.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto20.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto20.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto20.SubFlowChart = null;
            this.fc_GlueAuto20.TabIndex = 76;
            this.fc_GlueAuto20.Text = "UV气缸下压";
            this.fc_GlueAuto20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto20.TimeOut = 10000;
            this.fc_GlueAuto20.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto20.tmrTimeOut = null;
            this.fc_GlueAuto20.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto20_FlowRun);
            // 
            // fc_GlueAuto26
            // 
            this.fc_GlueAuto26.AlarmCode = "";
            this.fc_GlueAuto26.ArrowsWidth = 1;
            this.fc_GlueAuto26.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto26.bClearTrace = false;
            this.fc_GlueAuto26.bIsLastFlowChart = false;
            this.fc_GlueAuto26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto26.bSkip = false;
            this.fc_GlueAuto26.bStepByStepMode = false;
            this.fc_GlueAuto26.CASE1 = this.exptionFC;
            this.fc_GlueAuto26.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto26.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto26.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto26.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto26.CASE2 = null;
            this.fc_GlueAuto26.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto26.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto26.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto26.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto26.CASE3 = null;
            this.fc_GlueAuto26.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto26.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto26.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto26.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto26.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto26.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto26.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto26.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto26.Location = new System.Drawing.Point(626, 188);
            this.fc_GlueAuto26.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto26.menuOpening = false;
            this.fc_GlueAuto26.Name = "fc_GlueAuto26";
            this.fc_GlueAuto26.NEXT = this.fc_GlueAuto21;
            this.fc_GlueAuto26.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto26.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto26.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto26.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto26.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto26.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto26.SubFlowChart = null;
            this.fc_GlueAuto26.TabIndex = 80;
            this.fc_GlueAuto26.Text = "连接UV灯";
            this.fc_GlueAuto26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto26.TimeOut = 10000;
            this.fc_GlueAuto26.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto26.tmrTimeOut = null;
            this.fc_GlueAuto26.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto26_FlowRun);
            // 
            // fc_GlueAuto21
            // 
            this.fc_GlueAuto21.AlarmCode = "";
            this.fc_GlueAuto21.ArrowsWidth = 1;
            this.fc_GlueAuto21.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto21.bClearTrace = false;
            this.fc_GlueAuto21.bIsLastFlowChart = false;
            this.fc_GlueAuto21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto21.bSkip = false;
            this.fc_GlueAuto21.bStepByStepMode = false;
            this.fc_GlueAuto21.CASE1 = null;
            this.fc_GlueAuto21.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto21.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto21.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto21.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto21.CASE2 = null;
            this.fc_GlueAuto21.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto21.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto21.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto21.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto21.CASE3 = null;
            this.fc_GlueAuto21.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto21.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto21.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto21.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto21.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto21.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto21.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto21.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto21.Location = new System.Drawing.Point(626, 146);
            this.fc_GlueAuto21.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto21.menuOpening = false;
            this.fc_GlueAuto21.Name = "fc_GlueAuto21";
            this.fc_GlueAuto21.NEXT = this.fc_GlueAuto22;
            this.fc_GlueAuto21.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto21.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto21.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto21.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto21.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto21.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto21.SubFlowChart = null;
            this.fc_GlueAuto21.TabIndex = 75;
            this.fc_GlueAuto21.Text = "开始固化";
            this.fc_GlueAuto21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto21.TimeOut = 10000;
            this.fc_GlueAuto21.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto21.tmrTimeOut = null;
            this.fc_GlueAuto21.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto21_FlowRun);
            // 
            // fc_GlueAuto22
            // 
            this.fc_GlueAuto22.AlarmCode = "";
            this.fc_GlueAuto22.ArrowsWidth = 1;
            this.fc_GlueAuto22.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto22.bClearTrace = false;
            this.fc_GlueAuto22.bIsLastFlowChart = false;
            this.fc_GlueAuto22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto22.bSkip = false;
            this.fc_GlueAuto22.bStepByStepMode = false;
            this.fc_GlueAuto22.CASE1 = null;
            this.fc_GlueAuto22.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto22.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto22.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto22.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto22.CASE2 = null;
            this.fc_GlueAuto22.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto22.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto22.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto22.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto22.CASE3 = null;
            this.fc_GlueAuto22.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto22.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto22.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto22.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto22.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto22.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto22.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto22.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto22.Location = new System.Drawing.Point(626, 104);
            this.fc_GlueAuto22.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto22.menuOpening = false;
            this.fc_GlueAuto22.Name = "fc_GlueAuto22";
            this.fc_GlueAuto22.NEXT = this.fc_GlueAuto23;
            this.fc_GlueAuto22.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto22.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto22.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto22.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto22.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto22.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto22.SubFlowChart = null;
            this.fc_GlueAuto22.TabIndex = 79;
            this.fc_GlueAuto22.Text = "上升下压气缸";
            this.fc_GlueAuto22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto22.TimeOut = 10000;
            this.fc_GlueAuto22.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto22.tmrTimeOut = null;
            this.fc_GlueAuto22.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto22_FlowRun);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 267;
            this.label2.Text = "报工状态";
            // 
            // txb_GlueReport1
            // 
            this.txb_GlueReport1.Location = new System.Drawing.Point(87, 53);
            this.txb_GlueReport1.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.txb_GlueReport1.Name = "txb_GlueReport1";
            this.txb_GlueReport1.Size = new System.Drawing.Size(55, 20);
            this.txb_GlueReport1.TabIndex = 268;
            this.txb_GlueReport1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // txb_GlueReport2
            // 
            this.txb_GlueReport2.Location = new System.Drawing.Point(87, 382);
            this.txb_GlueReport2.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.txb_GlueReport2.Name = "txb_GlueReport2";
            this.txb_GlueReport2.Size = new System.Drawing.Size(55, 20);
            this.txb_GlueReport2.TabIndex = 270;
            this.txb_GlueReport2.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 386);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 269;
            this.label1.Text = "报工状态";
            // 
            // uc_GlueFlow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txb_GlueReport2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txb_GlueReport1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.UVup);
            this.Controls.Add(this.fc_GlueAuto26);
            this.Controls.Add(this.fc_GlueAuto22);
            this.Controls.Add(this.fc_GlueAuto18);
            this.Controls.Add(this.fc_GlueAuto19);
            this.Controls.Add(this.fc_GlueAuto20);
            this.Controls.Add(this.fc_GlueAuto21);
            this.Controls.Add(this.exptionFC);
            this.Controls.Add(this.fc_GlueAuto23);
            this.Controls.Add(this.fc_GlueAuto16);
            this.Controls.Add(this.fc_GlueAuto13);
            this.Controls.Add(this.fc_GlueAuto25);
            this.Controls.Add(this.fc_GlueAuto11);
            this.Controls.Add(this.fc_GlueAuto10);
            this.Controls.Add(this.fc_GlueAuto7);
            this.Controls.Add(this.fc_GlueAuto4);
            this.Controls.Add(this.fc_GlueAuto1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "uc_GlueFlow";
            this.Size = new System.Drawing.Size(875, 560);
            ((System.ComponentModel.ISupportInitialize)(this.txb_GlueReport1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txb_GlueReport2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private YHSDK.NPFlowChart fc_GlueAuto13;
        private YHSDK.NPFlowChart fc_GlueAuto16;
        private YHSDK.NPFlowChart fc_GlueAuto23;
        private YHSDK.NPFlowChart fc_GlueAuto25;
        private YHSDK.NPFlowChart fc_GlueAuto1;
        private YHSDK.NPFlowChart fc_GlueAuto4;
        private YHSDK.NPFlowChart fc_GlueAuto7;
        private YHSDK.NPFlowChart fc_GlueAuto10;
        private YHSDK.NPFlowChart fc_GlueAuto11;
        private YHSDK.NPFlowChart fc_GlueAuto21;
        private YHSDK.NPFlowChart fc_GlueAuto20;
        private YHSDK.NPFlowChart fc_GlueAuto19;
        private YHSDK.NPFlowChart fc_GlueAuto18;
        private YHSDK.NPFlowChart fc_GlueAuto22;
        private YHSDK.NPFlowChart fc_GlueAuto26;
        private YHSDK.NPFlowChart exptionFC;
        private YHSDK.NPFlowChart UVup;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown txb_GlueReport1;
        private System.Windows.Forms.NumericUpDown txb_GlueReport2;
        private System.Windows.Forms.Label label1;
    }
}
