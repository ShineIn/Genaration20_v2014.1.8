﻿namespace ModuleData.Views
{
    partial class uc_GlueExecuteTapeClean
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.fc_GlueAuto4 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto1 = new YHSDK.NPFlowChart();
            this.npFlowChart1 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto2 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto3 = new YHSDK.NPFlowChart();
            this.SuspendLayout();
            // 
            // fc_GlueAuto4
            // 
            this.fc_GlueAuto4.AlarmCode = "";
            this.fc_GlueAuto4.ArrowsWidth = 1;
            this.fc_GlueAuto4.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto4.bClearTrace = false;
            this.fc_GlueAuto4.bIsLastFlowChart = false;
            this.fc_GlueAuto4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto4.bSkip = false;
            this.fc_GlueAuto4.bStepByStepMode = false;
            this.fc_GlueAuto4.CASE1 = null;
            this.fc_GlueAuto4.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto4.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto4.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.CASE2 = null;
            this.fc_GlueAuto4.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto4.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto4.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.CASE3 = null;
            this.fc_GlueAuto4.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto4.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto4.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto4.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto4.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto4.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto4.Location = new System.Drawing.Point(19, 298);
            this.fc_GlueAuto4.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto4.menuOpening = false;
            this.fc_GlueAuto4.Name = "fc_GlueAuto4";
            this.fc_GlueAuto4.NEXT = this.fc_GlueAuto1;
            this.fc_GlueAuto4.NextArrowType = YHSDK.ArrowTypes.Square;
            this.fc_GlueAuto4.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto4.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto4.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto4.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto4.SubFlowChart = null;
            this.fc_GlueAuto4.TabIndex = 38;
            this.fc_GlueAuto4.Text = "复位PLC信号";
            this.fc_GlueAuto4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto4.TimeOut = 10000;
            this.fc_GlueAuto4.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto4.tmrTimeOut = null;
            this.fc_GlueAuto4.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto4_FlowRun);
            // 
            // fc_GlueAuto1
            // 
            this.fc_GlueAuto1.AlarmCode = "";
            this.fc_GlueAuto1.ArrowsWidth = 1;
            this.fc_GlueAuto1.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto1.bClearTrace = false;
            this.fc_GlueAuto1.bIsLastFlowChart = false;
            this.fc_GlueAuto1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto1.bSkip = false;
            this.fc_GlueAuto1.bStepByStepMode = false;
            this.fc_GlueAuto1.CASE1 = null;
            this.fc_GlueAuto1.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto1.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.CASE2 = null;
            this.fc_GlueAuto1.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto1.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.CASE3 = null;
            this.fc_GlueAuto1.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto1.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto1.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto1.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto1.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto1.Location = new System.Drawing.Point(19, 14);
            this.fc_GlueAuto1.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto1.menuOpening = false;
            this.fc_GlueAuto1.Name = "fc_GlueAuto1";
            this.fc_GlueAuto1.NEXT = this.npFlowChart1;
            this.fc_GlueAuto1.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto1.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto1.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto1.SubFlowChart = null;
            this.fc_GlueAuto1.TabIndex = 37;
            this.fc_GlueAuto1.Text = "等待触发信号";
            this.fc_GlueAuto1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto1.TimeOut = 10000;
            this.fc_GlueAuto1.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto1.tmrTimeOut = null;
            this.fc_GlueAuto1.FlowRun += new YHSDK.FlowRunEvent(this.fc_Glueflow1_FlowRun);
            // 
            // npFlowChart1
            // 
            this.npFlowChart1.AlarmCode = "";
            this.npFlowChart1.ArrowsWidth = 1;
            this.npFlowChart1.BackColor = System.Drawing.Color.White;
            this.npFlowChart1.bClearTrace = false;
            this.npFlowChart1.bIsLastFlowChart = false;
            this.npFlowChart1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.npFlowChart1.bSkip = false;
            this.npFlowChart1.bStepByStepMode = false;
            this.npFlowChart1.CASE1 = null;
            this.npFlowChart1.Case1ArrowType = YHSDK.ArrowTypes.Square;
            this.npFlowChart1.CASE1Color = System.Drawing.Color.LimeGreen;
            this.npFlowChart1.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.npFlowChart1.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.npFlowChart1.CASE2 = null;
            this.npFlowChart1.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart1.CASE2Color = System.Drawing.Color.Red;
            this.npFlowChart1.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.CASE3 = null;
            this.npFlowChart1.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart1.CASE3Color = System.Drawing.Color.Blue;
            this.npFlowChart1.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.DefaultColor = System.Drawing.Color.White;
            this.npFlowChart1.eSkipPath = YHSDK.FCResultType.NEXT;
            this.npFlowChart1.ExecutedColor = System.Drawing.Color.LightGray;
            this.npFlowChart1.ExecutingColor = System.Drawing.Color.Lime;
            this.npFlowChart1.Location = new System.Drawing.Point(19, 85);
            this.npFlowChart1.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.npFlowChart1.menuOpening = false;
            this.npFlowChart1.Name = "npFlowChart1";
            this.npFlowChart1.NEXT = this.fc_GlueAuto2;
            this.npFlowChart1.NextArrowType = YHSDK.ArrowTypes.Line;
            this.npFlowChart1.NEXTColor = System.Drawing.Color.Black;
            this.npFlowChart1.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.npFlowChart1.Size = new System.Drawing.Size(217, 29);
            this.npFlowChart1.SkipColor = System.Drawing.Color.Yellow;
            this.npFlowChart1.SubFlowChart = null;
            this.npFlowChart1.TabIndex = 40;
            this.npFlowChart1.Text = "UV气缸抬起";
            this.npFlowChart1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.npFlowChart1.TimeOut = 10000;
            this.npFlowChart1.TimeoutColor = System.Drawing.Color.Red;
            this.npFlowChart1.tmrTimeOut = null;
            this.npFlowChart1.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto22_FlowRun);
            // 
            // fc_GlueAuto2
            // 
            this.fc_GlueAuto2.AlarmCode = "";
            this.fc_GlueAuto2.ArrowsWidth = 1;
            this.fc_GlueAuto2.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto2.bClearTrace = false;
            this.fc_GlueAuto2.bIsLastFlowChart = false;
            this.fc_GlueAuto2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto2.bSkip = false;
            this.fc_GlueAuto2.bStepByStepMode = false;
            this.fc_GlueAuto2.CASE1 = null;
            this.fc_GlueAuto2.Case1ArrowType = YHSDK.ArrowTypes.Square;
            this.fc_GlueAuto2.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto2.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.fc_GlueAuto2.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.fc_GlueAuto2.CASE2 = null;
            this.fc_GlueAuto2.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto2.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto2.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.CASE3 = null;
            this.fc_GlueAuto2.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto2.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto2.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto2.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto2.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto2.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto2.Location = new System.Drawing.Point(19, 156);
            this.fc_GlueAuto2.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto2.menuOpening = false;
            this.fc_GlueAuto2.Name = "fc_GlueAuto2";
            this.fc_GlueAuto2.NEXT = this.fc_GlueAuto3;
            this.fc_GlueAuto2.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto2.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto2.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto2.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto2.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto2.SubFlowChart = null;
            this.fc_GlueAuto2.TabIndex = 40;
            this.fc_GlueAuto2.Text = "开始擦针";
            this.fc_GlueAuto2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto2.TimeOut = 10000;
            this.fc_GlueAuto2.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto2.tmrTimeOut = null;
            this.fc_GlueAuto2.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto2_FlowRun);
            // 
            // fc_GlueAuto3
            // 
            this.fc_GlueAuto3.AlarmCode = "";
            this.fc_GlueAuto3.ArrowsWidth = 1;
            this.fc_GlueAuto3.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto3.bClearTrace = false;
            this.fc_GlueAuto3.bIsLastFlowChart = false;
            this.fc_GlueAuto3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto3.bSkip = false;
            this.fc_GlueAuto3.bStepByStepMode = false;
            this.fc_GlueAuto3.CASE1 = null;
            this.fc_GlueAuto3.Case1ArrowType = YHSDK.ArrowTypes.Square;
            this.fc_GlueAuto3.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto3.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.fc_GlueAuto3.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.fc_GlueAuto3.CASE2 = null;
            this.fc_GlueAuto3.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto3.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto3.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.CASE3 = null;
            this.fc_GlueAuto3.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto3.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto3.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto3.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto3.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto3.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto3.Location = new System.Drawing.Point(19, 227);
            this.fc_GlueAuto3.Margin = new System.Windows.Forms.Padding(8, 9, 8, 9);
            this.fc_GlueAuto3.menuOpening = false;
            this.fc_GlueAuto3.Name = "fc_GlueAuto3";
            this.fc_GlueAuto3.NEXT = this.fc_GlueAuto4;
            this.fc_GlueAuto3.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto3.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto3.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto3.Size = new System.Drawing.Size(217, 29);
            this.fc_GlueAuto3.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto3.SubFlowChart = null;
            this.fc_GlueAuto3.TabIndex = 43;
            this.fc_GlueAuto3.Text = "移动轴到安全位";
            this.fc_GlueAuto3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto3.TimeOut = 10000;
            this.fc_GlueAuto3.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto3.tmrTimeOut = null;
            this.fc_GlueAuto3.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto3_FlowRun);
            // 
            // uc_GlueExecuteTapeClean
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.fc_GlueAuto3);
            this.Controls.Add(this.npFlowChart1);
            this.Controls.Add(this.fc_GlueAuto2);
            this.Controls.Add(this.fc_GlueAuto1);
            this.Controls.Add(this.fc_GlueAuto4);
            this.Name = "uc_GlueExecuteTapeClean";
            this.Size = new System.Drawing.Size(397, 488);
            this.ResumeLayout(false);

        }

        #endregion
        private YHSDK.NPFlowChart fc_GlueAuto4;
        private YHSDK.NPFlowChart fc_GlueAuto1;
        private YHSDK.NPFlowChart fc_GlueAuto2;
        private YHSDK.NPFlowChart fc_GlueAuto3;
        private YHSDK.NPFlowChart npFlowChart1;
    }
}
