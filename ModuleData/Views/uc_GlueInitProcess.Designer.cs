﻿namespace ModuleData.Views
{
    partial class uc_GlueInitProcess
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.UVup = new YHSDK.NPFlowChart();
            this.exptionFC = new YHSDK.NPFlowChart();
            this.fc_GlueAuto1 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto6 = new YHSDK.NPFlowChart();
            this.fc_GlueAuto7 = new YHSDK.NPFlowChart();
            this.SuspendLayout();
            // 
            // UVup
            // 
            this.UVup.AlarmCode = "";
            this.UVup.ArrowsWidth = 1;
            this.UVup.BackColor = System.Drawing.Color.White;
            this.UVup.bClearTrace = false;
            this.UVup.bIsLastFlowChart = false;
            this.UVup.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UVup.bSkip = false;
            this.UVup.bStepByStepMode = false;
            this.UVup.CASE1 = this.exptionFC;
            this.UVup.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.UVup.CASE1Color = System.Drawing.Color.LimeGreen;
            this.UVup.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.UVup.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.UVup.CASE2 = null;
            this.UVup.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.UVup.CASE2Color = System.Drawing.Color.Red;
            this.UVup.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.UVup.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.UVup.CASE3 = null;
            this.UVup.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.UVup.CASE3Color = System.Drawing.Color.Blue;
            this.UVup.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.UVup.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.UVup.DefaultColor = System.Drawing.Color.White;
            this.UVup.eSkipPath = YHSDK.FCResultType.NEXT;
            this.UVup.ExecutedColor = System.Drawing.Color.LightGray;
            this.UVup.ExecutingColor = System.Drawing.Color.Lime;
            this.UVup.Location = new System.Drawing.Point(11, 83);
            this.UVup.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.UVup.menuOpening = false;
            this.UVup.Name = "UVup";
            this.UVup.NEXT = this.fc_GlueAuto6;
            this.UVup.NextArrowType = YHSDK.ArrowTypes.Line;
            this.UVup.NEXTColor = System.Drawing.Color.Black;
            this.UVup.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.UVup.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.UVup.Size = new System.Drawing.Size(321, 33);
            this.UVup.SkipColor = System.Drawing.Color.Yellow;
            this.UVup.SubFlowChart = null;
            this.UVup.TabIndex = 82;
            this.UVup.Text = "UV气缸抬起";
            this.UVup.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.UVup.TimeOut = 10000;
            this.UVup.TimeoutColor = System.Drawing.Color.Red;
            this.UVup.tmrTimeOut = null;
            this.UVup.FlowRun += new YHSDK.FlowRunEvent(this.UVup_FlowRun);
            // 
            // exptionFC
            // 
            this.exptionFC.AlarmCode = "";
            this.exptionFC.ArrowsWidth = 1;
            this.exptionFC.BackColor = System.Drawing.Color.White;
            this.exptionFC.bClearTrace = false;
            this.exptionFC.bIsLastFlowChart = false;
            this.exptionFC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.exptionFC.bSkip = false;
            this.exptionFC.bStepByStepMode = false;
            this.exptionFC.CASE1 = null;
            this.exptionFC.Case1ArrowType = YHSDK.ArrowTypes.Square;
            this.exptionFC.CASE1Color = System.Drawing.Color.LimeGreen;
            this.exptionFC.CASE1EndPoint = YHSDK.FlowchartSides.Left;
            this.exptionFC.Case1StartPoint = YHSDK.FlowchartSides.Left;
            this.exptionFC.CASE2 = null;
            this.exptionFC.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.exptionFC.CASE2Color = System.Drawing.Color.Red;
            this.exptionFC.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.exptionFC.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.exptionFC.CASE3 = null;
            this.exptionFC.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.exptionFC.CASE3Color = System.Drawing.Color.Blue;
            this.exptionFC.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.exptionFC.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.exptionFC.DefaultColor = System.Drawing.Color.White;
            this.exptionFC.eSkipPath = YHSDK.FCResultType.NEXT;
            this.exptionFC.ExecutedColor = System.Drawing.Color.LightGray;
            this.exptionFC.ExecutingColor = System.Drawing.Color.Lime;
            this.exptionFC.Location = new System.Drawing.Point(461, 175);
            this.exptionFC.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.exptionFC.menuOpening = false;
            this.exptionFC.Name = "exptionFC";
            this.exptionFC.NEXT = this.fc_GlueAuto1;
            this.exptionFC.NextArrowType = YHSDK.ArrowTypes.Line;
            this.exptionFC.NEXTColor = System.Drawing.Color.Black;
            this.exptionFC.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.exptionFC.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.exptionFC.Size = new System.Drawing.Size(187, 33);
            this.exptionFC.SkipColor = System.Drawing.Color.Yellow;
            this.exptionFC.SubFlowChart = null;
            this.exptionFC.TabIndex = 74;
            this.exptionFC.Text = "异常处理";
            this.exptionFC.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.exptionFC.TimeOut = 10000;
            this.exptionFC.TimeoutColor = System.Drawing.Color.Red;
            this.exptionFC.tmrTimeOut = null;
            this.exptionFC.FlowRun += new YHSDK.FlowRunEvent(this.exptionFC_FlowRun);
            // 
            // fc_GlueAuto1
            // 
            this.fc_GlueAuto1.AlarmCode = "";
            this.fc_GlueAuto1.ArrowsWidth = 1;
            this.fc_GlueAuto1.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto1.bClearTrace = false;
            this.fc_GlueAuto1.bIsLastFlowChart = false;
            this.fc_GlueAuto1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto1.bSkip = false;
            this.fc_GlueAuto1.bStepByStepMode = false;
            this.fc_GlueAuto1.CASE1 = null;
            this.fc_GlueAuto1.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto1.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.CASE2 = null;
            this.fc_GlueAuto1.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto1.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.CASE3 = null;
            this.fc_GlueAuto1.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto1.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto1.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto1.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto1.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto1.Location = new System.Drawing.Point(11, 15);
            this.fc_GlueAuto1.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.fc_GlueAuto1.menuOpening = false;
            this.fc_GlueAuto1.Name = "fc_GlueAuto1";
            this.fc_GlueAuto1.NEXT = this.UVup;
            this.fc_GlueAuto1.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto1.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto1.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto1.Size = new System.Drawing.Size(321, 37);
            this.fc_GlueAuto1.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto1.SubFlowChart = null;
            this.fc_GlueAuto1.TabIndex = 0;
            this.fc_GlueAuto1.Text = "复位启动";
            this.fc_GlueAuto1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto1.TimeOut = 10000;
            this.fc_GlueAuto1.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto1.tmrTimeOut = null;
            this.fc_GlueAuto1.FlowRun += new YHSDK.FlowRunEvent(this.npFlowChart1_FlowRun);
            // 
            // fc_GlueAuto6
            // 
            this.fc_GlueAuto6.AlarmCode = "";
            this.fc_GlueAuto6.ArrowsWidth = 1;
            this.fc_GlueAuto6.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto6.bClearTrace = false;
            this.fc_GlueAuto6.bIsLastFlowChart = false;
            this.fc_GlueAuto6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto6.bSkip = false;
            this.fc_GlueAuto6.bStepByStepMode = false;
            this.fc_GlueAuto6.CASE1 = this.exptionFC;
            this.fc_GlueAuto6.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto6.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto6.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.CASE2 = null;
            this.fc_GlueAuto6.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto6.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto6.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.CASE3 = null;
            this.fc_GlueAuto6.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto6.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto6.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto6.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto6.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto6.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto6.Location = new System.Drawing.Point(11, 162);
            this.fc_GlueAuto6.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.fc_GlueAuto6.menuOpening = false;
            this.fc_GlueAuto6.Name = "fc_GlueAuto6";
            this.fc_GlueAuto6.NEXT = this.fc_GlueAuto7;
            this.fc_GlueAuto6.NextArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto6.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto6.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto6.Size = new System.Drawing.Size(321, 33);
            this.fc_GlueAuto6.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto6.SubFlowChart = null;
            this.fc_GlueAuto6.TabIndex = 37;
            this.fc_GlueAuto6.Text = "复位点胶模组";
            this.fc_GlueAuto6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto6.TimeOut = 10000;
            this.fc_GlueAuto6.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto6.tmrTimeOut = null;
            this.fc_GlueAuto6.FlowRun += new YHSDK.FlowRunEvent(this.fc_GlueAuto6_FlowRun);
            // 
            // fc_GlueAuto7
            // 
            this.fc_GlueAuto7.AlarmCode = "";
            this.fc_GlueAuto7.ArrowsWidth = 1;
            this.fc_GlueAuto7.BackColor = System.Drawing.Color.White;
            this.fc_GlueAuto7.bClearTrace = false;
            this.fc_GlueAuto7.bIsLastFlowChart = false;
            this.fc_GlueAuto7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fc_GlueAuto7.bSkip = false;
            this.fc_GlueAuto7.bStepByStepMode = false;
            this.fc_GlueAuto7.CASE1 = null;
            this.fc_GlueAuto7.Case1ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto7.CASE1Color = System.Drawing.Color.LimeGreen;
            this.fc_GlueAuto7.CASE1EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.Case1StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.CASE2 = null;
            this.fc_GlueAuto7.Case2ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto7.CASE2Color = System.Drawing.Color.Red;
            this.fc_GlueAuto7.CASE2EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.Case2StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.CASE3 = null;
            this.fc_GlueAuto7.Case3ArrowType = YHSDK.ArrowTypes.Line;
            this.fc_GlueAuto7.CASE3Color = System.Drawing.Color.Blue;
            this.fc_GlueAuto7.CASE3EndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.Case3StartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.DefaultColor = System.Drawing.Color.White;
            this.fc_GlueAuto7.eSkipPath = YHSDK.FCResultType.NEXT;
            this.fc_GlueAuto7.ExecutedColor = System.Drawing.Color.LightGray;
            this.fc_GlueAuto7.ExecutingColor = System.Drawing.Color.Lime;
            this.fc_GlueAuto7.Location = new System.Drawing.Point(11, 244);
            this.fc_GlueAuto7.Margin = new System.Windows.Forms.Padding(11, 10, 11, 10);
            this.fc_GlueAuto7.menuOpening = false;
            this.fc_GlueAuto7.Name = "fc_GlueAuto7";
            this.fc_GlueAuto7.NEXT = this.fc_GlueAuto1;
            this.fc_GlueAuto7.NextArrowType = YHSDK.ArrowTypes.Square;
            this.fc_GlueAuto7.NEXTColor = System.Drawing.Color.Black;
            this.fc_GlueAuto7.NEXTEndPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.NEXTStartPoint = YHSDK.FlowchartSides.Right;
            this.fc_GlueAuto7.Size = new System.Drawing.Size(321, 37);
            this.fc_GlueAuto7.SkipColor = System.Drawing.Color.Yellow;
            this.fc_GlueAuto7.SubFlowChart = null;
            this.fc_GlueAuto7.TabIndex = 0;
            this.fc_GlueAuto7.Text = "初始化完成";
            this.fc_GlueAuto7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.fc_GlueAuto7.TimeOut = 10000;
            this.fc_GlueAuto7.TimeoutColor = System.Drawing.Color.Red;
            this.fc_GlueAuto7.tmrTimeOut = null;
            this.fc_GlueAuto7.FlowRun += new YHSDK.FlowRunEvent(this.npFlowChart8_FlowRun);
            // 
            // uc_GlueInitProcess
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.UVup);
            this.Controls.Add(this.exptionFC);
            this.Controls.Add(this.fc_GlueAuto6);
            this.Controls.Add(this.fc_GlueAuto7);
            this.Controls.Add(this.fc_GlueAuto1);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "uc_GlueInitProcess";
            this.Size = new System.Drawing.Size(717, 334);
            this.ResumeLayout(false);

        }

        #endregion

        private YHSDK.NPFlowChart fc_GlueAuto1;
        private YHSDK.NPFlowChart fc_GlueAuto7;
        private YHSDK.NPFlowChart fc_GlueAuto6;
        private YHSDK.NPFlowChart exptionFC;
        private YHSDK.NPFlowChart UVup;
    }
}
