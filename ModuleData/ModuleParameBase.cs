﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace ModuleData
{
    public class ModuleParameBase : UserControl
    {

        public static DependencyProperty SubModuleProperty = DependencyProperty.Register("SubModule", typeof(IModule), typeof(ModuleParameBase), new PropertyMetadata(null));

        public IModule SubModule
        {
            get { return (IModule)GetValue(SubModuleProperty); }
            set { SetValue(SubModuleProperty, value); }
        }
    }
}
