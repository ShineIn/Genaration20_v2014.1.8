﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using YHSDK;
using System.Windows.Forms;
using LogSv;
using System.Xml.Serialization;
using System.Windows;
using Googo.Manul;
using System.Threading;

namespace ModuleData
{
    /// <summary>
    /// 模块基础控件
    /// </summary>
    [Serializable]
    public class ModuleBase : UserControl, IModule, INotifyPropertyChanged
    {
        protected ModuleBase()
        {
            AllModule.Add(this);
        }

        #region INotifyPropertyChanged
        [field: NonSerialized()]
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged([CallerMemberName] string PropertyName = null)
        {
            if (null != PropertyChanged)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(PropertyName));
            }
        }
        protected void PrintLog(string msg, System.Drawing.Color color)
        {
            if (PrintMsg != msg)
            {
                PrintMsg = msg;
                Log.log.Write(msg, color);
            }
        }
        #endregion INotifyPropertyChanged

        #region 公有属性
        [XmlIgnore]
        public bool IsRun
        {
            //get { return null != runtask && !runtask.IsCompleted; }
            get { return null != thread && thread.IsAlive; }
        }

        /// <summary>
        /// 德创TCP对象
        /// </summary>
        /// <remarks>
        /// TCP对象，到最外层直接进行绑定
        /// </remarks>
        [XmlIgnore]
        public SuperSimpleTcp.SimpleTcpClient DCTcpClient
        {
            get { return _dCTtcpClient; }
            set
            {
                if (_dCTtcpClient != null && _dCTtcpClient.Events != null)
                {
                    _dCTtcpClient.Events.DataReceived -= Alg_DataReceived;
                    _dCTtcpClient.Events.Disconnected -= Events_Disconnected;
                }
                _dCTtcpClient = value;
                if (value != null && value.Events != null)
                {
                    value.Events.DataReceived += Alg_DataReceived;
                    value.Events.Disconnected += Events_Disconnected;
                }
                OnPropertyChanged();
            }
        }
        private SuperSimpleTcp.SimpleTcpClient _dCTtcpClient;

        /// <summary>
        /// 算法TCP客户端接受到的最新信息
        /// </summary>
        /// <remarks>
        /// 获取之后马上清空信息
        /// </remarks>
        [XmlIgnore]
        protected string AlgLastContent
        {
            get
            {
                string ret = algLastContent;
                algLastContent = string.Empty;
                return ret;
            }
            set { algLastContent = value; OnPropertyChanged(); }
        }
        private string algLastContent = string.Empty;


        /// <summary>
        /// 初始化标志位
        /// </summary>
        public static bool BInitIsOK
        {
            get { return bInitIsOK; }
            set { bInitIsOK = value; GloMotionParame.FlowIsInitialized = value; }
        }
        static bool bInitIsOK = false;

        /// <summary>
        /// 是否初始化模块
        /// </summary>
        public virtual bool IsInitialModule { get { return false; } }

        /// <summary>
        /// 是否处于等待触发信号
        /// </summary>
        public virtual bool IsWaitForSignal
        {
            get
            {
                //if (null == runtask || runtask.IsCompleted) return true;
                return StartChart == null || StartChart.WorkFlow == null || StartChart.WorkFlow == StartChart;
            }
        }

        public int StageIndex { get; set; } = 1;

        public static List<ModuleBase> AllModule
        {
            get { return allModule; }
        }
        static readonly List<ModuleBase> allModule = new List<ModuleBase>();


        #endregion 公有属性

        #region 公有函数
        public void RunAsync()
        {
            if (!lockList.ContainsKey(StageIndex))
                lockList[StageIndex] = new Mutex();
            Mutex muxe = lockList[StageIndex];
            if (null == thread || !thread.IsAlive)
            {
                _isRun = true;
                thread = new System.Threading.Thread(() =>
                {
                    while (_isRun)
                    {
                        if (!IsCondiCanRun())
                        {
                            Thread.Sleep(50);
                            continue;
                        }
                        muxe.WaitOne();
                        try
                        {
                            if (!IsCondiCanRun())
                            {
                                Thread.Sleep(50);
                                continue;
                            }
                            Run();
                        }
                        catch (Exception ex)
                        {

                        }
                        finally
                        {
                            muxe.ReleaseMutex();
                        }
                        Thread.Sleep(50);
                    }
                });
                thread.IsBackground = true;
                thread.Start();
            }
        }

        protected virtual void Run() { }

        public virtual void ResetChart() { }

        public virtual void Stop()
        {
            _isRun = false;
        }

        public IModule Clone()
        {
            Debug.WriteLine(GetHashCode());
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Context = new StreamingContext(StreamingContextStates.Persistence | StreamingContextStates.Clone);
            formatter.Serialize(stream, this);
            stream.Position = 0L;
            IModule clone = (IModule)formatter.Deserialize(stream);
            Debug.WriteLine(clone.GetHashCode());
            return clone;
        }

        /// <summary>
        /// 算法客户端接受信息事件
        /// </summary>
        /// <remarks>
        /// 可以重新该事件，在里面做自己想要的事件处理
        /// </remarks>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void Alg_DataReceived(object sender, SuperSimpleTcp.DataReceivedEventArgs e)
        {
            if (e.Data == null || null == e.Data.Array)
                return;
            string strRec = Encoding.UTF8.GetString(e.Data.Array);
            Debug.WriteLine($"Port={e.IpPort}+Data={strRec}");
            AlgLastContent = strRec;
        }

        #endregion 公有函数

        #region private
        private void Events_Disconnected(object sender, SuperSimpleTcp.ConnectionEventArgs e)
        {
            Debug.WriteLine($"Port={e.IpPort} Disconnected Reason={e.Reason}");
        }

        /// <summary>
        /// 是否条件可以满足执行
        /// </summary>
        /// <returns></returns>
        private bool IsCondiCanRun()
        {
            IEnumerable<ModuleBase> modules = AllModule.Where((temp) => { return temp != null && temp.StageIndex == StageIndex && temp != this; });
            if (null != modules)
            {
                ModuleBase module = modules.FirstOrDefault((tt) => { return tt.IsRun && !tt.IsWaitForSignal; });
                return null == module;
            }
            return true;
        }

        private Task runtask = null;
        private System.Threading.Thread thread;
        private bool _isRun = false;
        private string PrintMsg = "";
        protected NPFlowChart StartChart = null;
        protected readonly static Dictionary<int, Mutex> lockList = new Dictionary<int, Mutex>();
        #endregion private
    }
}
