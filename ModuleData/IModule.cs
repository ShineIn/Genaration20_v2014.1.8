﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YHSDK;

namespace ModuleData
{
    /// <summary>
    /// 继承FlowChart
    /// </summary>
    public interface IModule : IChartRun
    {
        void ResetChart();
    }
}
